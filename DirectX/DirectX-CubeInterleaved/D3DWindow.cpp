#include <Windows.h>
#include<stdio.h>

#include<d3d11.h>
#include <d3dcompiler.h>
#include "WICTextureLoader.h"
#pragma warning( disable: 4838 )
#include "XNAMath\xnamath.h"

#pragma comment (lib, "d3d11.lib")
#pragma comment (lib,"D3dcompiler.lib")
#pragma comment (lib, "DirectXTK.lib")
#define WIN_WIDTH 800
#define WIN_HEIGHT 600

LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);

FILE *gpFile = NULL;
char gszLogFileName[] = "Log.txt";

HWND ghwnd = NULL;

DWORD dwStyle;
WINDOWPLACEMENT wpPrev = { sizeof(WINDOWPLACEMENT) };

bool gbActiveWindow = false;
bool gbEscapeKeyIsPressed = false;
bool gbFullscreen = false;

float gClearColor[4]; //RGBA
IDXGISwapChain *gpIDXGISwapChain = NULL;
ID3D11Device *gpID3D11Device = NULL;
ID3D11DeviceContext *gpID3D11DeviceContext = NULL;
ID3D11RenderTargetView *gpID3D11RenderTargetView = NULL;

ID3D11VertexShader *gpID3D11VertexShader = NULL;
ID3D11PixelShader *gpID3D11PixelShader = NULL;
ID3D11Buffer *gpID3D11Buffer_VertexBuffer_Cube_Position = NULL;
ID3D11ShaderResourceView *gpID3D11ShaderResourceView_Texture_Cube = NULL;
ID3D11SamplerState *gpID3D11Samplerstate_Texture_Cube = NULL;
ID3D11InputLayout *gpID3D11InputLayout = NULL;
ID3D11Buffer *gpID3D11Buffer_ConstantBuffer = NULL;
ID3D11RasterizerState *gpID3D11RasterizerState = NULL;
ID3D11DepthStencilView *gpID3D11DepthStencilView = NULL;

ID3D11SamplerState *gpID3D11SamplerState_Texture_Cube = NULL;
float light_ambient[] = { 0.0, 0.0, 0.0, 1.0 };
float light_diffuse[] = { 1.0, 1.0, 1.0, 1.0 };
float light_specular[] = { 1.0, 1.0, 1.0, 1.0 };
float material_ambient[] = { 0.0, 0.0, 0.0, 1.0 };
float material_diffuse[] = { 0.5, 0.5, 0.5, 1.0 };
float material_specular[] = { 0.5, 0.5, 0.5, 1.0 };
float material_shininess = 50.0;

float light_position[] = { 100.0, 100.0, -100.0, 1.0 };
float gfAnglePyramid = 0.0f;
float gfAngleCube = 0.0f;
struct CBUFFER {
	XMMATRIX WorldMatrix;
	XMMATRIX ViewMatrix;
	XMMATRIX ProjectionMatrix;
	XMVECTOR la;
	XMVECTOR ld;
	XMVECTOR ls;
	XMVECTOR lp;
	XMVECTOR ka;
	XMVECTOR kd;
	XMVECTOR ks;
	float materialShininess;
	UINT LKeyPressed;
};

bool gbLight;
bool gbAnimate;
XMMATRIX gPerspectiveProjectionMatrix;
// WinMain
int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpszCmdLine, int iCmdShow) {
	// Function declarations
	HRESULT initialize(void);
	void uninitialize(void);
	void update(void);
	void display(void);

	// variable declarations
	WNDCLASSEX wndclass;
	HWND hwnd;
	MSG msg;
	TCHAR szClassName[] = TEXT("Direct3D11");
	bool bDone = false;

	// code
	// create log file
	if (fopen_s(&gpFile, gszLogFileName, "w") != 0)
	{
		MessageBox(NULL, TEXT("Log File Can Not Be Created\nExitting ..."), TEXT("Error"), MB_OK | MB_TOPMOST | MB_ICONSTOP);
		exit(0);
	}
	else
	{
		fprintf_s(gpFile, "Log File Is Successfully Opened.\n");
		fclose(gpFile);
	}

	// initialize WNDCLASSEX structure
	wndclass.cbSize = sizeof(WNDCLASSEX);
	wndclass.style = CS_HREDRAW | CS_VREDRAW | CS_OWNDC;
	wndclass.cbClsExtra = 0;
	wndclass.cbWndExtra = 0;
	wndclass.lpfnWndProc = WndProc;
	wndclass.hbrBackground = (HBRUSH)GetStockObject(BLACK_BRUSH);
	wndclass.hInstance = hInstance;
	wndclass.hIcon = LoadIcon(NULL, IDI_APPLICATION);
	wndclass.hCursor = LoadCursor(NULL, IDC_ARROW);
	wndclass.lpszClassName = szClassName;
	wndclass.lpszMenuName = NULL;
	wndclass.hIconSm = LoadIcon(NULL, IDI_APPLICATION);

	// register WNDCLASSEX structure
	RegisterClassEx(&wndclass);

	// create window
	hwnd = CreateWindow(szClassName,
		TEXT("Direct3D11 Window"),
		WS_OVERLAPPEDWINDOW,
		100,
		100,
		WIN_WIDTH,
		WIN_HEIGHT,
		NULL,
		NULL,
		hInstance,
		NULL);

	ghwnd = hwnd;

	ShowWindow(hwnd, iCmdShow);
	SetForegroundWindow(hwnd);
	SetFocus(hwnd);

	// initialize D3D
	HRESULT hr;
	hr = initialize();
	if (FAILED(hr))
	{
		fopen_s(&gpFile, gszLogFileName, "a+");
		fprintf_s(gpFile, "initialize() Failed. Exitting Now ...\n");
		fclose(gpFile);
		DestroyWindow(hwnd);
		hwnd = NULL;
	}
	else
	{
		fopen_s(&gpFile, gszLogFileName, "a+");
		fprintf_s(gpFile, "initialize() Succeeded.\n");
		fclose(gpFile);
	}

	// message loop
	while (bDone == false)
	{
		if (PeekMessage(&msg, NULL, 0, 0, PM_REMOVE))
		{
			if (msg.message == WM_QUIT)
				bDone = true;
			else
			{
				TranslateMessage(&msg);
				DispatchMessage(&msg);
			}
		}
		else
		{
			// render
			display();
			update();
			if (gbActiveWindow == true)
			{
				if (gbEscapeKeyIsPressed == true)
					bDone = true;
			}
		}
	}

	// clean-up
	uninitialize();

	return((int)msg.wParam);
}

LRESULT CALLBACK WndProc(HWND hwnd, UINT iMsg, WPARAM wParam, LPARAM lParam) {

	HRESULT resize(int, int);
	void ToggleFullScreen(void);
	void uninitialize(void);

	HRESULT hr;

	switch (iMsg) {
	case WM_ACTIVATE:
		if (HIWORD(wParam) == 0)
			gbActiveWindow = true;
		else
			gbActiveWindow = false;
	case WM_ERASEBKGND:
		return 0;
	case WM_SIZE:
		if (gpID3D11DeviceContext) {
			hr = resize(LOWORD(lParam), HIWORD(lParam));
			if (FAILED(hr)) {
				fopen_s(&gpFile, gszLogFileName, "a+");
				fprintf_s(gpFile, "resize() failed.\n");
				fclose(gpFile);
				return(hr);
			}
			else {
				fopen_s(&gpFile, gszLogFileName, "a+");
				fprintf_s(gpFile, "resize() failed.\n");
				fclose(gpFile);
			}
		}
		break;
	case WM_KEYDOWN:
		switch (wParam) {
		case VK_ESCAPE:
			if (gbEscapeKeyIsPressed == false)
				gbEscapeKeyIsPressed = true;
			break;
		case 0x46:
			if (gbFullscreen == false) {
				ToggleFullScreen();
				gbFullscreen = true;
			}
			else {
				ToggleFullScreen();
				gbFullscreen = false;
			}
			break;
		case 0x41: // for 'A' or 'a'
			if (gbAnimate == false)
			{
				gbAnimate = true;

			}
			else
			{
				gbAnimate = false;
			}
			break;
		case 0x4C:
			if (gbLight == false)
			{
				gbLight = true;
				fopen_s(&gpFile, gszLogFileName, "a+");
				fprintf_s(gpFile, "L key is pressed.\n");
				fclose(gpFile);
			}
			else
			{
				gbLight = false;

			}
			break;
		default:
			break;
		}
		break;
	case WM_LBUTTONDOWN:
		break;
	case WM_CLOSE:
		uninitialize();
		break;
	case WM_DESTROY:
		PostQuitMessage(0);
		break;
	default:
		break;
	}
	return(DefWindowProc(hwnd, iMsg, wParam, lParam));
}

void ToggleFullScreen(void)
{
	// variable declarations
	MONITORINFO mi;

	// code
	if (gbFullscreen == false)
	{
		dwStyle = GetWindowLong(ghwnd, GWL_STYLE);
		if (dwStyle & WS_OVERLAPPEDWINDOW)
		{
			mi = { sizeof(MONITORINFO) };
			if (GetWindowPlacement(ghwnd, &wpPrev) && GetMonitorInfo(MonitorFromWindow(ghwnd, MONITORINFOF_PRIMARY), &mi))
			{
				SetWindowLong(ghwnd, GWL_STYLE, dwStyle & ~WS_OVERLAPPEDWINDOW);
				SetWindowPos(ghwnd, HWND_TOP, mi.rcMonitor.left, mi.rcMonitor.top, mi.rcMonitor.right - mi.rcMonitor.left, mi.rcMonitor.bottom - mi.rcMonitor.top, SWP_NOZORDER | SWP_FRAMECHANGED);
			}
		}
		ShowCursor(FALSE);
	}
	else
	{
		SetWindowLong(ghwnd, GWL_STYLE, dwStyle | WS_OVERLAPPEDWINDOW);
		SetWindowPlacement(ghwnd, &wpPrev);
		SetWindowPos(ghwnd, HWND_TOP, 0, 0, 0, 0, SWP_NOMOVE | SWP_NOSIZE | SWP_NOOWNERZORDER | SWP_NOZORDER | SWP_FRAMECHANGED);

		ShowCursor(TRUE);
	}
}

HRESULT initialize(void) {

	// function declarations
	void uninitialize(void);
	HRESULT resize(int, int);
	HRESULT Load3DTexture(const wchar_t *textureFileName, ID3D11ShaderResourceView **ppID3D11ShaderResourceView);
	// varible declarations
	HRESULT hr;
	D3D_DRIVER_TYPE d3dDriverType;
	D3D_DRIVER_TYPE d3dDriverTypes[] = { D3D_DRIVER_TYPE_HARDWARE, D3D_DRIVER_TYPE_WARP, D3D_DRIVER_TYPE_REFERENCE };
	D3D_FEATURE_LEVEL d3dFeatureLevel_required = D3D_FEATURE_LEVEL_11_0;
	D3D_FEATURE_LEVEL d3dFeatureLevel_acquired = D3D_FEATURE_LEVEL_10_0;
	UINT createDeviceFlags = 0;
	UINT numDriverTypes = 0;
	UINT numFeatureLevels = 1; // based upon d3dFeatureLevel_required

							   // code
	numDriverTypes = sizeof(d3dDriverTypes) / sizeof(d3dDriverTypes[0]); // calculating size of array

	DXGI_SWAP_CHAIN_DESC dxgiSwapChainDesc;
	ZeroMemory((void *)&dxgiSwapChainDesc, sizeof(DXGI_SWAP_CHAIN_DESC));
	dxgiSwapChainDesc.BufferCount = 1;
	dxgiSwapChainDesc.BufferDesc.Width = WIN_WIDTH;
	dxgiSwapChainDesc.BufferDesc.Height = WIN_HEIGHT;
	dxgiSwapChainDesc.BufferDesc.Format = DXGI_FORMAT_R8G8B8A8_UNORM;
	dxgiSwapChainDesc.BufferDesc.RefreshRate.Numerator = 60;
	dxgiSwapChainDesc.BufferDesc.RefreshRate.Denominator = 1;
	dxgiSwapChainDesc.BufferUsage = DXGI_USAGE_RENDER_TARGET_OUTPUT;
	dxgiSwapChainDesc.OutputWindow = ghwnd;
	dxgiSwapChainDesc.SampleDesc.Count = 1;
	dxgiSwapChainDesc.SampleDesc.Quality = 0;
	dxgiSwapChainDesc.Windowed = TRUE;

	for (UINT driverTypeIndex = 0; driverTypeIndex < numDriverTypes; driverTypeIndex++) {
		d3dDriverType = d3dDriverTypes[driverTypeIndex];
		hr = D3D11CreateDeviceAndSwapChain(
			NULL,						// Adapter
			d3dDriverType,				// Driver type
			NULL,						// Software
			createDeviceFlags,			// Flags
			&d3dFeatureLevel_required,	// Feature Levels
			numFeatureLevels,			// Num Features Levels
			D3D11_SDK_VERSION,			// SDK version
			&dxgiSwapChainDesc,			// swap chain desc
			&gpIDXGISwapChain,			// swap chain 
			&gpID3D11Device,			// device
			&d3dFeatureLevel_acquired,	// Feature level
			&gpID3D11DeviceContext		// Device context
		);
		if (SUCCEEDED(hr))
			break;
	}
	if (FAILED(hr)) {
		fopen_s(&gpFile, gszLogFileName, "a+");
		fprintf_s(gpFile, "D3D11CreateDeviceAndSwapChain() failed.\n");
		fclose(gpFile);
		return(hr);
	}
	else {
		fopen_s(&gpFile, gszLogFileName, "a+");
		fprintf_s(gpFile, "D3D11CreateDeviceAndSwapChain() succeeded.\n");
		fprintf_s(gpFile, "The chosen driver is of ");
		if (d3dDriverType == D3D_DRIVER_TYPE_HARDWARE) {
			fprintf_s(gpFile, "Hardware type.\n");
		}
		else if (d3dDriverType == D3D_DRIVER_TYPE_WARP) {
			fprintf_s(gpFile, "Warp type\n");
		}
		else if (d3dDriverType == D3D_DRIVER_TYPE_REFERENCE) {
			fprintf_s(gpFile, "Reference type\n");
		}
		else {
			fprintf_s(gpFile, "Unknown type.\n");
		}

		fprintf_s(gpFile, "The supported highest feature level is ");
		if (d3dFeatureLevel_acquired == D3D_FEATURE_LEVEL_11_0) {
			fprintf_s(gpFile, "11.0\n");
		}
		else if (d3dFeatureLevel_acquired == D3D_FEATURE_LEVEL_10_1) {
			fprintf_s(gpFile, "10.1\n");
		}
		else if (d3dFeatureLevel_acquired == D3D_FEATURE_LEVEL_10_0) {
			fprintf_s(gpFile, "10.0\n");
		}
		else {
			fprintf_s(gpFile, "Unknown.\n");
		}
		fclose(gpFile);
	}

	// initialize shader, input layouts, constant buffers etc.
	const char *vertexShaderSourceCode =
		"cbuffer ConstantBuffer"\
		"{"\
		"float4x4 worldMatrix; "\
		"float4x4 viewMatrix; "\
		"float4x4 projectionMatrix; "\
		"float4 la; "\
		"float4 ld; "\
		"float4 ls; "\
		"float4 ka; "\
		"float4 kd; "\
		"float4 ks; "\
		"float materialShininess;"\
		"float4 lp; "\
		"uint LKeyPressed;"\
		"}"\
		"struct VertexOut {"\
		"float4 position: SV_POSITION;"\
		"float2 texcoord: TEXCOORD;"\
		"float4 color: COLOR;"\
		"float3 transformed_normal: NORMAL0;"\
		"float3 light_direction: NORMAL1;"\
		"float3 viewer_vector: NORMAL2;"\
		"};"\
		"struct VertexIN{"\
		"float4 pos: POSITION;"\
		"float4 color: COLOR;"\
		"float2 texcoord: TEXCOORD;"\
		"float4 normal: NORMAL;"\
		"};"\
		"VertexOut main(VertexIN vin)"\
		"{"\
		"VertexOut output;"\
		"if(LKeyPressed==1)"\
		"{"\
		"float4 eyeCoordinates = mul(worldMatrix,vin.pos); "\
		"eyeCoordinates = mul(viewMatrix,eyeCoordinates); "\
		"output.transformed_normal = mul((float3x3)mul(worldMatrix,viewMatrix),(float3)vin.normal); "\
		"output.light_direction = (float3)(lp - eyeCoordinates); "\
		"output.viewer_vector = (-eyeCoordinates.xyz); "\
		"}"\
		"output.position = mul(worldMatrix,vin.pos); "\
		"output.position = mul(viewMatrix,output.position); "\
		"output.position = mul(projectionMatrix,output.position );"\
		"output.texcoord = vin.texcoord; "\
		"output.color = vin.color; "\
		"return output;"\
		"}";

	ID3DBlob *pID3DBlob_VertexShaderCode = NULL;
	ID3DBlob *pID3DBlob_Error = NULL;

	hr = D3DCompile(vertexShaderSourceCode,
		lstrlenA(vertexShaderSourceCode) + 1,
		"VS",
		NULL,
		D3D_COMPILE_STANDARD_FILE_INCLUDE,
		"main",
		"vs_5_0",
		0,
		0,
		&pID3DBlob_VertexShaderCode,
		&pID3DBlob_Error);

	if (FAILED(hr)) {
		if (pID3DBlob_Error != NULL) {
			fopen_s(&gpFile, gszLogFileName, "a+");
			fprintf(gpFile, "D3DCompile() Failed For Vertex Shader: %s.\n", (char *)pID3DBlob_Error->GetBufferPointer());
			fclose(gpFile);
			pID3DBlob_Error->Release();
			pID3DBlob_Error = NULL;
			return (hr);
		}
	}
	else {
		fopen_s(&gpFile, gszLogFileName, "a+");
		fprintf_s(gpFile, "D3DCompile() succeeded for vertex shader.\n");
		fclose(gpFile);
	}

	hr = gpID3D11Device->CreateVertexShader(pID3DBlob_VertexShaderCode->GetBufferPointer(), pID3DBlob_VertexShaderCode->GetBufferSize(), NULL, &gpID3D11VertexShader);
	if (FAILED(hr)) {
		fopen_s(&gpFile, gszLogFileName, "a+");
		fprintf_s(gpFile, "ID3D11Device::CreateVertexShader() Failed.\n");
		fclose(gpFile);
		return(hr);
	}
	else {
		fopen_s(&gpFile, gszLogFileName, "a+");
		fprintf_s(gpFile, "ID3D11Device::CreateVertexShader() Succeeded.\n");
		fclose(gpFile);
	}
	gpID3D11DeviceContext->VSSetShader(gpID3D11VertexShader, 0, 0);

	const char *pixelShaderSourceCode =
		"cbuffer ConstantBuffer"\
		"{"\
		"float4x4 worldMatrix; "\
		"float4x4 viewMatrix; "\
		"float4x4 projectionMatrix; "\
		"float4 la; "\
		"float4 ld; "\
		"float4 ls; "\
		"float4 ka; "\
		"float4 kd; "\
		"float4 ks; "\
		"float materialShininess;"\
		"float4 lp; "\
		"uint LKeyPressed;"\
		"}"\
		"struct VertexOut {"\
		"float4 position: SV_POSITION;"\
		"float2 texcoord: TEXCOORD;"\
		"float4 color: COLOR;"\
		"float3 transformed_normal: NORMAL0;"\
		"float3 light_direction: NORMAL1;"\
		"float3 viewer_vector: NORMAL2;"\
		"};"\
		"Texture2D myTexture2D;"\
		"SamplerState mySamplerState;"\
		"float4 main(VertexOut vIn) : SV_TARGET"\
		"{"\
		"float4 phong_ads_color;"\
		"if(LKeyPressed==1)"\
		"{"\
		"float3 normalized_tnorm = normalize(vIn.transformed_normal); "\
		"float3 normalized_light_direction = normalize(vIn.light_direction); "\
		"float3 normalized_viewer_vector = normalize(vIn.viewer_vector); "\
		"float tn_dot_ld = max(dot(normalized_tnorm,normalized_light_direction),0.0); "\
		"float3 ambient = la * ka ;"\
		"float3 diffuse = ld * kd * tn_dot_ld ;"\
		"float3 reflectionVector = reflect(-normalized_light_direction, normalized_tnorm); "\
		"float3 specular = ls * ks * pow(max(dot(reflectionVector,normalized_viewer_vector),0.0),materialShininess); "\
		"phong_ads_color = float4(ambient + diffuse ,1.0);"\
		"float4 txcolor = myTexture2D.Sample(mySamplerState,vIn.texcoord);"\
		"return phong_ads_color * vIn.color *  txcolor + float4(specular,1.0);"\
		"} else {"\
		"phong_ads_color = float4(1.0,1.0,1.0,1.0);"\
		"float4 txcolor = myTexture2D.Sample(mySamplerState,vIn.texcoord);"\
		"return phong_ads_color * vIn.color *  txcolor;"\
		"}"\
		"}";

	ID3DBlob *pID3DBlob_PixelShaderCode = NULL;
	pID3DBlob_Error = NULL;
	hr = D3DCompile(
		pixelShaderSourceCode,
		lstrlenA(pixelShaderSourceCode) + 1,
		"PS",
		NULL,
		D3D_COMPILE_STANDARD_FILE_INCLUDE,
		"main",
		"ps_5_0",
		0,
		0,
		&pID3DBlob_PixelShaderCode,
		&pID3DBlob_Error
	);

	if (FAILED(hr)) {
		if (pID3DBlob_Error != NULL) {
			fopen_s(&gpFile, gszLogFileName, "a+");
			fprintf_s(gpFile, "D3DCompile() Failed For Pixel Shader : %s.\n", (char *)pID3DBlob_Error->GetBufferPointer());
			fclose(gpFile);
			pID3DBlob_Error->Release();
			pID3DBlob_Error = NULL;
			return(hr);
		}
	}
	else {
		fopen_s(&gpFile, gszLogFileName, "a+");
		fprintf_s(gpFile, "D3DCompile() Succeeded For Pixel Shader.\n");
		fclose(gpFile);
	}

	hr = gpID3D11Device->CreatePixelShader(pID3DBlob_PixelShaderCode->GetBufferPointer(), pID3DBlob_PixelShaderCode->GetBufferSize(), NULL, &gpID3D11PixelShader);
	if (FAILED(hr)) {
		fopen_s(&gpFile, gszLogFileName, "a+");
		fprintf_s(gpFile, "ID3D11Device::CreatePixelShader() Failed.\n");
		fclose(gpFile);
		return(hr);
	}
	else {
		fopen_s(&gpFile, gszLogFileName, "a+");
		fprintf_s(gpFile, "ID3D11Device::CreatePixelShader() Succeeded.\n");
		fclose(gpFile);
	}
	gpID3D11DeviceContext->PSSetShader(gpID3D11PixelShader, 0, 0);
	pID3DBlob_PixelShaderCode->Release();
	pID3DBlob_PixelShaderCode = NULL;


	// code for square
	float cubeVertices[] = {
		// top
		// triangle 1
		-1.0f, +1.0f, +1.0f,    1.0f, 0.0f, 0.0f,	0.0f, +1.0f, 0.0f,	+0.0f, +0.0f,
		+1.0f, +1.0f, +1.0f,    1.0f, 0.0f, 0.0f,   0.0f, +1.0f, 0.0f,	+0.0f, +1.0f,
		-1.0f, +1.0f, -1.0f,    1.0f, 0.0f, 0.0f,   0.0f, +1.0f, 0.0f,	+1.0f, +0.0f,
		// triangle 2
		-1.0f, +1.0f, -1.0f,    1.0f, 0.0f, 0.0f,   0.0f, +1.0f, 0.0f,	+1.0f, +0.0f,
		+1.0f, +1.0f, +1.0f,    1.0f, 0.0f, 0.0f,   0.0f, +1.0f, 0.0f,	+0.0f, +1.0f,
		+1.0f, +1.0f, -1.0f,    1.0f, 0.0f, 0.0f,   0.0f, +1.0f, 0.0f,	+1.0f, +1.0f,

		//bottom
		// triangle 1
		+1.0f, -1.0f, -1.0f,	0.0f, 1.0f, 0.0f,   0.0f, -1.0f, 0.0f,	+0.0f, +0.0f,
		+1.0f, -1.0f, +1.0f,	0.0f, 1.0f, 0.0f,   0.0f, -1.0f, 0.0f,	+0.0f, +1.0f,
		-1.0f, -1.0f, -1.0f,	0.0f, 1.0f, 0.0f,   0.0f, -1.0f, 0.0f,	+1.0f, +0.0f,
		// triangle 2
		-1.0f, -1.0f, -1.0f,	0.0f, 1.0f, 0.0f,   0.0f, -1.0f, 0.0f,	+1.0f, +0.0f,
		+1.0f, -1.0f, +1.0f,	0.0f, 1.0f, 0.0f,   0.0f, -1.0f, 0.0f,	+0.0f, +1.0f,
		-1.0f, -1.0f, +1.0f,	0.0f, 1.0f, 0.0f,   0.0f, -1.0f, 0.0f,	+1.0f, +1.0f,

		//front
		// triangle 1
		-1.0f, +1.0f, -1.0f,	0.0f, 0.0f, 1.0f,	0.0f, 0.0f, -1.0f,	+0.0f, +0.0f,
		1.0f, 1.0f, -1.0f,   	0.0f, 0.0f, 1.0f,	0.0f, 0.0f, -1.0f,	+0.0f, +1.0f,
		-1.0f, -1.0f, -1.0f,	0.0f, 0.0f, 1.0f,	0.0f, 0.0f, -1.0f,	+1.0f, +0.0f,
		// triangle 2
		-1.0f, -1.0f, -1.0f,	0.0f, 0.0f, 1.0f,	0.0f, 0.0f, -1.0f,	+1.0f, +0.0f,
		1.0f, 1.0f, -1.0f,		0.0f, 0.0f, 1.0f,	0.0f, 0.0f, -1.0f,	+0.0f, +1.0f,
		+1.0f, -1.0f, -1.0f,	0.0f, 0.0f, 1.0f,	0.0f, 0.0f, -1.0f,	+1.0f, +1.0f,

		//back
		// triangle 1
		+1.0f, -1.0f, +1.0f,	0.0f, 1.0f, 1.0f,	0.0f, 0.0f, +1.0f,	+0.0f, +0.0f,
		+1.0f, +1.0f, +1.0f,	0.0f, 1.0f, 1.0f,	0.0f, 0.0f, +1.0f,	+0.0f, +1.0f,
		-1.0f, -1.0f, +1.0f,	0.0f, 1.0f, 1.0f,	0.0f, 0.0f, +1.0f,	+1.0f, +0.0f,
		// triangle 2
		-1.0f, -1.0f, +1.0f,	0.0f, 1.0f, 1.0f,	0.0f, 0.0f, +1.0f,	+1.0f, +0.0f,
		+1.0f, +1.0f, +1.0f,	0.0f, 1.0f, 1.0f,	0.0f, 0.0f, +1.0f,	+0.0f, +1.0f,
		-1.0f, +1.0f, +1.0f,	0.0f, 1.0f, 1.0f,	0.0f, 0.0f, +1.0f,	+1.0f, +1.0f,

		//left
		// triangle 1
		-1.0f, +1.0f, +1.0f,	1.0f, 0.0f, 1.0f,	1.0f, 0.0f, 1.0f,	+0.0f, +0.0f,
		-1.0f, +1.0f, -1.0f,	1.0f, 0.0f, 1.0f,	1.0f, 0.0f, 1.0f,	+0.0f, +1.0f,
		-1.0f, -1.0f, +1.0f,	1.0f, 0.0f, 1.0f,	1.0f, 0.0f, 1.0f,	+1.0f, +0.0f,
		// triangle 2
		-1.0f, -1.0f, +1.0f,	1.0f, 0.0f, 1.0f,	1.0f, 0.0f, 1.0f,	+1.0f, +0.0f,
		-1.0f, +1.0f, -1.0f,	1.0f, 0.0f, 1.0f,	1.0f, 0.0f, 1.0f,	+0.0f, +1.0f,
		-1.0f, -1.0f, -1.0f,	1.0f, 0.0f, 1.0f,	1.0f, 0.0f, 1.0f,	+1.0f, +1.0f,

		//right
		// triangle 1
		+1.0f, -1.0f, -1.0f,	1.0f, 1.0f, 0.0f,	+1.0f, 0.0f, 0.0f,	+0.0f, +0.0f,
		+1.0f, +1.0f, -1.0f,	1.0f, 1.0f, 0.0f,	+1.0f, 0.0f, 0.0f,	+0.0f, +1.0f,
		+1.0f, -1.0f, +1.0f,	1.0f, 1.0f, 0.0f,	+1.0f, 0.0f, 0.0f,	+1.0f, +0.0f,
		// triangle 2
		+1.0f, -1.0f, +1.0f,	1.0f, 1.0f, 0.0f,	+1.0f, 0.0f, 0.0f,	+1.0f, +0.0f,
		+1.0f, +1.0f, -1.0f,	1.0f, 1.0f, 0.0f,	+1.0f, 0.0f, 0.0f,	+0.0f, +1.0f,
		+1.0f, +1.0f, +1.0f,	1.0f, 1.0f, 0.0f,	+1.0f, 0.0f, 0.0f,	+1.0f, +1.0f
	};


	D3D11_BUFFER_DESC bufferDescCube;
	ZeroMemory(&bufferDescCube, sizeof(D3D11_BUFFER_DESC));
	bufferDescCube.Usage = D3D11_USAGE_DYNAMIC;
	bufferDescCube.ByteWidth = sizeof(float) * ARRAYSIZE(cubeVertices);
	bufferDescCube.BindFlags = D3D11_BIND_VERTEX_BUFFER;
	bufferDescCube.CPUAccessFlags = D3D11_CPU_ACCESS_WRITE;
	hr = gpID3D11Device->CreateBuffer(&bufferDescCube, NULL, &gpID3D11Buffer_VertexBuffer_Cube_Position);

	if (FAILED(hr))
	{
		fopen_s(&gpFile, gszLogFileName, "a+");
		fprintf_s(gpFile, "ID3D11Device::CreateBuffer() Failed For Vertex Buffer for Position of Square.\n");
		fclose(gpFile);
		return (hr);
	}
	else
	{
		fopen_s(&gpFile, gszLogFileName, "a+");
		fprintf_s(gpFile, "ID3D11Device::CreateBuffer() Succeeded For Vertex Buffer for position of square.\n");
		fclose(gpFile);
	}

	D3D11_MAPPED_SUBRESOURCE mappedSubresourceCube;
	ZeroMemory(&mappedSubresourceCube, sizeof(D3D11_MAPPED_SUBRESOURCE));
	gpID3D11DeviceContext->Map(gpID3D11Buffer_VertexBuffer_Cube_Position, NULL, D3D11_MAP_WRITE_DISCARD, NULL, &mappedSubresourceCube);
	memcpy(mappedSubresourceCube.pData, cubeVertices, sizeof(cubeVertices));
	gpID3D11DeviceContext->Unmap(gpID3D11Buffer_VertexBuffer_Cube_Position, NULL);

	// create and set input layout
	D3D11_INPUT_ELEMENT_DESC inputElementDescArr[4];

	inputElementDescArr[0].SemanticName = "POSITION";
	inputElementDescArr[0].SemanticIndex = 0;
	inputElementDescArr[0].Format = DXGI_FORMAT_R32G32B32_FLOAT;
	inputElementDescArr[0].InputSlot = 0;
	inputElementDescArr[0].AlignedByteOffset = 0;
	inputElementDescArr[0].InputSlotClass = D3D11_INPUT_PER_VERTEX_DATA;
	inputElementDescArr[0].InstanceDataStepRate = 0;


	inputElementDescArr[1].SemanticName = "COLOR";
	inputElementDescArr[1].SemanticIndex = 0;
	inputElementDescArr[1].Format = DXGI_FORMAT_R32G32B32_FLOAT;
	inputElementDescArr[1].InputSlot = 1;
	inputElementDescArr[1].AlignedByteOffset = 0;
	inputElementDescArr[1].InputSlotClass = D3D11_INPUT_PER_VERTEX_DATA;
	inputElementDescArr[1].InstanceDataStepRate = 0;

	inputElementDescArr[2].SemanticName = "NORMAL";
	inputElementDescArr[2].SemanticIndex = 0;
	inputElementDescArr[2].Format = DXGI_FORMAT_R32G32B32_FLOAT;
	inputElementDescArr[2].InputSlot = 2;
	inputElementDescArr[2].AlignedByteOffset = 0;
	inputElementDescArr[2].InputSlotClass = D3D11_INPUT_PER_VERTEX_DATA;
	inputElementDescArr[2].InstanceDataStepRate = 0;

	inputElementDescArr[3].SemanticName = "TEXCOORD";
	inputElementDescArr[3].SemanticIndex = 0;
	inputElementDescArr[3].Format = DXGI_FORMAT_R32G32_FLOAT;
	inputElementDescArr[3].InputSlot = 3;
	inputElementDescArr[3].AlignedByteOffset = D3D11_APPEND_ALIGNED_ELEMENT;
	inputElementDescArr[3].InputSlotClass = D3D11_INPUT_PER_VERTEX_DATA;
	inputElementDescArr[3].InstanceDataStepRate = 0;

	hr = gpID3D11Device->CreateInputLayout(inputElementDescArr, 4, pID3DBlob_VertexShaderCode->GetBufferPointer(), pID3DBlob_VertexShaderCode->GetBufferSize(), &gpID3D11InputLayout);
	if (FAILED(hr)) {
		fopen_s(&gpFile, gszLogFileName, "a+");
		fprintf_s(gpFile, "ID3D11Device::CreateInputLayout() Failed.\n");
		fclose(gpFile);
		return hr;
	}
	else {
		fopen_s(&gpFile, gszLogFileName, "a+");
		fprintf_s(gpFile, "ID3D11Device::CreateInputLayout() Succeeded.\n");
		fclose(gpFile);
	}
	gpID3D11DeviceContext->IASetInputLayout(gpID3D11InputLayout);
	pID3DBlob_VertexShaderCode->Release();
	pID3DBlob_VertexShaderCode = NULL;

	// define and set the constant buffer
	D3D11_BUFFER_DESC bufferDesc_ConstantBuffer;
	ZeroMemory(&bufferDesc_ConstantBuffer, sizeof(D3D11_BUFFER_DESC));
	bufferDesc_ConstantBuffer.Usage = D3D11_USAGE_DEFAULT;
	bufferDesc_ConstantBuffer.ByteWidth = sizeof(CBUFFER);
	bufferDesc_ConstantBuffer.BindFlags = D3D11_BIND_CONSTANT_BUFFER;
	hr = gpID3D11Device->CreateBuffer(&bufferDesc_ConstantBuffer, nullptr, &gpID3D11Buffer_ConstantBuffer);
	if (FAILED(hr)) {
		fopen_s(&gpFile, gszLogFileName, "a+");
		fprintf_s(gpFile, "ID3D11Device::CreateBuffer() Failed For Constant Buffer.\n");
		fclose(gpFile);
		return(hr);
	}
	else {
		fopen_s(&gpFile, gszLogFileName, "a+");
		fprintf_s(gpFile, "ID3D11Device::CreateBuffer() Succeeded For Constant Buffer.\n");
		fclose(gpFile);
	}
	gpID3D11DeviceContext->VSSetConstantBuffers(0, 1, &gpID3D11Buffer_ConstantBuffer);
	gpID3D11DeviceContext->PSSetConstantBuffers(0, 1, &gpID3D11Buffer_ConstantBuffer);

	// to disable back face culling
	D3D11_RASTERIZER_DESC rasterizerDesc;
	ZeroMemory(&rasterizerDesc, sizeof(D3D11_RASTERIZER_DESC));
	rasterizerDesc.AntialiasedLineEnable = FALSE;
	rasterizerDesc.MultisampleEnable = FALSE;
	rasterizerDesc.DepthBias = 0;
	rasterizerDesc.DepthBiasClamp = 0.0f;
	rasterizerDesc.SlopeScaledDepthBias = 0.0f;
	rasterizerDesc.CullMode = D3D11_CULL_NONE;
	rasterizerDesc.DepthClipEnable = TRUE;
	rasterizerDesc.FillMode = D3D11_FILL_SOLID;
	rasterizerDesc.FrontCounterClockwise = FALSE;
	rasterizerDesc.ScissorEnable = FALSE;

	hr = gpID3D11Device->CreateRasterizerState(&rasterizerDesc, &gpID3D11RasterizerState);
	if (FAILED(hr))
	{
		fopen_s(&gpFile, gszLogFileName, "a+");
		fprintf_s(gpFile, "ID3D11Device::CreateRasterizerState() Failed For Culling.\n");
		fclose(gpFile);
		return(hr);
	}
	else
	{
		fopen_s(&gpFile, gszLogFileName, "a+");
		fprintf_s(gpFile, "ID3D11Device::CreateRasterizerState() Succeeded For Culling.\n");
		fclose(gpFile);
	}
	gpID3D11DeviceContext->RSSetState(gpID3D11RasterizerState);

	// d3d clear color
	gClearColor[0] = 0.0f;
	gClearColor[1] = 0.1f;
	gClearColor[2] = 0.2f;
	gClearColor[3] = 0.3f;

	D3D11_SAMPLER_DESC samplerDesc;
	ZeroMemory(&samplerDesc, sizeof(D3D11_SAMPLER_DESC));
	samplerDesc.Filter = D3D11_FILTER_MIN_MAG_MIP_LINEAR;
	samplerDesc.AddressU = D3D11_TEXTURE_ADDRESS_WRAP;
	samplerDesc.AddressV = D3D11_TEXTURE_ADDRESS_WRAP;
	samplerDesc.AddressW = D3D11_TEXTURE_ADDRESS_WRAP;

	hr = gpID3D11Device->CreateSamplerState(&samplerDesc, &gpID3D11Samplerstate_Texture_Cube);
	if (FAILED(hr))
	{
		fopen_s(&gpFile, gszLogFileName, "a+");
		fprintf_s(gpFile, "ID3D11Device::CreateSamplerState() Failed For Pyramid Texture.\n");
		fclose(gpFile);
		return(hr);
	}
	else
	{
		fopen_s(&gpFile, gszLogFileName, "a+");
		fprintf_s(gpFile, "ID3D11Device::CreateSamplerState() Succeeded For Pyramid Texture.\n");
		fclose(gpFile);
	}

	hr = Load3DTexture(L"marble.bmp", &gpID3D11ShaderResourceView_Texture_Cube);
	if (FAILED(hr))
	{
		fopen_s(&gpFile, gszLogFileName, "a+");
		fprintf_s(gpFile, "ID3D11Device::Load3DTexture() Failed For Pyramid Texture.\n");
		fclose(gpFile);
		return(hr);
	}
	else
	{
		fopen_s(&gpFile, gszLogFileName, "a+");
		fprintf_s(gpFile, "ID3D11Device::Load3DTexture() Succeeded For Pyramid Texture.\n");
		fclose(gpFile);

	}
	// call resize for first time
	hr = resize(WIN_WIDTH, WIN_HEIGHT);
	if (FAILED(hr)) {
		fopen_s(&gpFile, gszLogFileName, "a+");
		fprintf_s(gpFile, "resize() failed.\n");
		fclose(gpFile);
		return(hr);
	}
	else {
		fopen_s(&gpFile, gszLogFileName, "a+");
		fprintf_s(gpFile, "resize() succeeded.\n");
		fclose(gpFile);
	}
	return (S_OK);

}

HRESULT Load3DTexture(const wchar_t *textureFileName, ID3D11ShaderResourceView **ppID3D11ShaderResourceView)
{
	HRESULT hr;
	hr = DirectX::CreateWICTextureFromFile(gpID3D11Device, gpID3D11DeviceContext, textureFileName, NULL, ppID3D11ShaderResourceView);
	if (FAILED(hr))
	{
		fopen_s(&gpFile, gszLogFileName, "a+");
		fprintf_s(gpFile, "CreateWICTextureFromFile() Failed for Texture Resource.\n");
		fclose(gpFile);
		return(hr);
	}
	else
	{
		fopen_s(&gpFile, gszLogFileName, "a+");
		fprintf_s(gpFile, "CreateWICTextureFromFile() Succeeded for Texture Resource.\n");
		fclose(gpFile);
	}

	return (hr);
}
HRESULT resize(int width, int height) {
	// code
	HRESULT hr = S_OK;

	if (gpID3D11RenderTargetView) {
		gpID3D11RenderTargetView->Release();
		gpID3D11RenderTargetView = NULL;
	}

	// resize swap chain buffers accordingly
	gpIDXGISwapChain->ResizeBuffers(1, width, height, DXGI_FORMAT_R8G8B8A8_UNORM, 0);

	// again get back buffer from swap chain
	ID3D11Texture2D *pID3D11Texture2D_BackBuffer;
	gpIDXGISwapChain->GetBuffer(0, __uuidof(ID3D11Texture2D), (LPVOID*)&pID3D11Texture2D_BackBuffer);

	// again get render target view from d3d11 device using above back buffer
	hr = gpID3D11Device->CreateRenderTargetView(pID3D11Texture2D_BackBuffer, NULL, &gpID3D11RenderTargetView);
	if (FAILED(hr)) {
		fopen_s(&gpFile, gszLogFileName, "a+");
		fprintf_s(gpFile, "ID3D11Device::CreateRenderTargetView() Failed.\n");
		fclose(gpFile);
		return(hr);
	}
	else {
		fopen_s(&gpFile, gszLogFileName, "a+");
		fprintf_s(gpFile, "ID3D11Device::CreateRenderTargetView() Succeeded.\n");
		fclose(gpFile);
	}
	pID3D11Texture2D_BackBuffer->Release();
	pID3D11Texture2D_BackBuffer = NULL;

	// set render target view as render target
	//gpID3D11DeviceContext->OMSetRenderTargets(1, &gpID3D11RenderTargetView, NULL);

	// enable depth
	D3D11_TEXTURE2D_DESC textureDesc;
	ZeroMemory(&textureDesc, sizeof(D3D11_TEXTURE2D_DESC));
	textureDesc.Width = (UINT)width;
	textureDesc.Height = (UINT)height;
	textureDesc.ArraySize = 1;
	textureDesc.MipLevels = 1;
	textureDesc.SampleDesc.Count = 1;
	textureDesc.SampleDesc.Quality = 0;
	textureDesc.Format = DXGI_FORMAT_D32_FLOAT;
	textureDesc.Usage = D3D11_USAGE_DEFAULT;
	textureDesc.BindFlags = D3D11_BIND_DEPTH_STENCIL;
	textureDesc.CPUAccessFlags = 0;
	textureDesc.MiscFlags = 0;

	ID3D11Texture2D *pID3D11Texture2D_DepthBuffer;
	gpID3D11Device->CreateTexture2D(&textureDesc, NULL, &pID3D11Texture2D_DepthBuffer);

	D3D11_DEPTH_STENCIL_VIEW_DESC depthStencilViewDesc;
	ZeroMemory(&depthStencilViewDesc, sizeof(D3D11_DEPTH_STENCIL_VIEW_DESC));
	depthStencilViewDesc.Format = DXGI_FORMAT_D32_FLOAT;
	depthStencilViewDesc.ViewDimension = D3D11_DSV_DIMENSION_TEXTURE2DMS;

	hr = gpID3D11Device->CreateDepthStencilView(pID3D11Texture2D_DepthBuffer, &depthStencilViewDesc, &gpID3D11DepthStencilView);
	if (FAILED(hr))
	{
		fopen_s(&gpFile, gszLogFileName, "a+");
		fprintf_s(gpFile, "ID3D11Device::CreateDepthStencilView() Failed.\n");
		fclose(gpFile);
		return (hr);
	}
	else
	{
		fopen_s(&gpFile, gszLogFileName, "a+");
		fprintf_s(gpFile, "ID3D11Device::CreateDepthStencilView() Succeeded.\n");
		fclose(gpFile);
	}

	pID3D11Texture2D_DepthBuffer->Release();
	pID3D11Texture2D_DepthBuffer = NULL;
	gpID3D11DeviceContext->OMSetRenderTargets(1, &gpID3D11RenderTargetView, gpID3D11DepthStencilView);

	// set viewport
	D3D11_VIEWPORT d3dViewPort;
	d3dViewPort.TopLeftX = 0;
	d3dViewPort.TopLeftY = 0;
	d3dViewPort.Width = (float)width;
	d3dViewPort.Height = (float)height;
	d3dViewPort.MinDepth = 0.0f;
	d3dViewPort.MaxDepth = 1.0f;
	gpID3D11DeviceContext->RSSetViewports(1, &d3dViewPort);

	gPerspectiveProjectionMatrix = XMMatrixPerspectiveFovLH(45.0f, (float)width / (float)height, 0.1f, 100.0f);
	return (hr);
}

void display(void) {
	// code
	// clear render target view to a chosen color
	gpID3D11DeviceContext->ClearRenderTargetView(gpID3D11RenderTargetView, gClearColor);
	gpID3D11DeviceContext->ClearDepthStencilView(gpID3D11DepthStencilView, D3D11_CLEAR_DEPTH, 1.0f, 0);
	fopen_s(&gpFile, gszLogFileName, "a+");
	fprintf_s(gpFile, "In display...\n");
	fclose(gpFile);
	// select which vertex buffer to display
	UINT stride = sizeof(float) * 11;
	UINT offset = 0;
	gpID3D11DeviceContext->IASetVertexBuffers(0, 1, &gpID3D11Buffer_VertexBuffer_Cube_Position, &stride, &offset);
	stride = sizeof(float) * 11;
	offset = sizeof(float) * 3;
	gpID3D11DeviceContext->IASetVertexBuffers(1, 1, &gpID3D11Buffer_VertexBuffer_Cube_Position, &stride, &offset);

	stride = sizeof(float) * 11;
	offset = sizeof(float) * 6;;
	gpID3D11DeviceContext->IASetVertexBuffers(2, 1, &gpID3D11Buffer_VertexBuffer_Cube_Position, &stride, &offset);

	stride = sizeof(float) * 11;
	offset = sizeof(float) * 9;
	gpID3D11DeviceContext->IASetVertexBuffers(3, 1, &gpID3D11Buffer_VertexBuffer_Cube_Position, &stride, &offset);

	//bind texture and sampler as pixel shader resource
	gpID3D11DeviceContext->PSSetShaderResources(0, 1, &gpID3D11ShaderResourceView_Texture_Cube);
	gpID3D11DeviceContext->PSSetSamplers(0, 1, &gpID3D11SamplerState_Texture_Cube);
	// select geometry primitive
	gpID3D11DeviceContext->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST);

	// translation is concerned with world matrix transformation
	XMMATRIX worldMatrix = XMMatrixIdentity();
	XMMATRIX viewMatrix = XMMatrixIdentity();
	XMMATRIX rotationMatrix = XMMatrixIdentity();

	worldMatrix = XMMatrixTranslation(0.0f, 0.0f, 6.0f);
	XMMATRIX x = XMMatrixRotationX(XMConvertToRadians(gfAngleCube));
	XMMATRIX y = XMMatrixRotationY(XMConvertToRadians(gfAngleCube));
	XMMATRIX z = XMMatrixRotationZ(XMConvertToRadians(gfAngleCube));
	rotationMatrix = x * y * z;

	XMMATRIX scaleMatrix = XMMatrixScaling(0.75f, 0.75f, 0.75f);
	worldMatrix = scaleMatrix * rotationMatrix * worldMatrix;
	worldMatrix = rotationMatrix * worldMatrix;

	// load the data into the constant buffer
	CBUFFER constantBuffer;
	constantBuffer.WorldMatrix = worldMatrix;
	constantBuffer.ViewMatrix = viewMatrix;
	constantBuffer.ProjectionMatrix = gPerspectiveProjectionMatrix;
	if (gbLight == 1) {
		constantBuffer.LKeyPressed = 1;
		constantBuffer.la = XMVectorSet(light_ambient[0], light_ambient[1], light_ambient[2], light_ambient[3]);
		constantBuffer.ld = XMVectorSet(light_diffuse[0], light_diffuse[1], light_diffuse[2], light_diffuse[3]);
		constantBuffer.ls = XMVectorSet(light_specular[0], light_specular[1], light_specular[2], light_specular[3]);
		constantBuffer.lp = XMVectorSet(light_position[0], light_position[1], light_position[2], light_position[3]);
		constantBuffer.ka = XMVectorSet(material_ambient[0], material_ambient[1], material_ambient[2], material_ambient[3]);
		constantBuffer.kd = XMVectorSet(material_diffuse[0], material_diffuse[1], material_diffuse[2], material_diffuse[3]);
		constantBuffer.ks = XMVectorSet(material_specular[0], material_specular[1], material_specular[2], material_specular[3]);
		constantBuffer.materialShininess = material_shininess;
	}
	gpID3D11DeviceContext->UpdateSubresource(gpID3D11Buffer_ConstantBuffer, 0, NULL, &constantBuffer, 0, 0);

	// draw vertex buffer to render target
	gpID3D11DeviceContext->Draw(6, 0);
	gpID3D11DeviceContext->Draw(6, 6);
	gpID3D11DeviceContext->Draw(6, 12);
	gpID3D11DeviceContext->Draw(6, 18);
	gpID3D11DeviceContext->Draw(6, 24);
	gpID3D11DeviceContext->Draw(6, 36);
	// switch between front and back buffers
	gpIDXGISwapChain->Present(0, 0);
}

void update(void)
{
	if (gbAnimate) {
		gfAngleCube = gfAngleCube - 0.1f;
		if (gfAngleCube <= -360.0f)
			gfAngleCube = 0.0f;
	}

}
void uninitialize(void)
{
	// code
	if (gpID3D11SamplerState_Texture_Cube)
	{
		gpID3D11SamplerState_Texture_Cube->Release();
		gpID3D11SamplerState_Texture_Cube = NULL;
	}
	if (gpID3D11ShaderResourceView_Texture_Cube)
	{
		gpID3D11ShaderResourceView_Texture_Cube->Release();
		gpID3D11ShaderResourceView_Texture_Cube = NULL;
	}
	if (gpID3D11Buffer_ConstantBuffer)
	{
		gpID3D11Buffer_ConstantBuffer->Release();
		gpID3D11Buffer_ConstantBuffer = NULL;
	}
	if (gpID3D11RenderTargetView) {
		gpID3D11RenderTargetView->Release();
		gpID3D11RenderTargetView = NULL;
	}

	if (gpID3D11InputLayout)
	{
		gpID3D11InputLayout->Release();
		gpID3D11InputLayout = NULL;
	}

	if (gpID3D11Buffer_VertexBuffer_Cube_Position)
	{
		gpID3D11Buffer_VertexBuffer_Cube_Position->Release();
		gpID3D11Buffer_VertexBuffer_Cube_Position = NULL;
	}

	if (gpID3D11Buffer_VertexBuffer_Cube_Position)
	{
		gpID3D11Buffer_VertexBuffer_Cube_Position->Release();
		gpID3D11Buffer_VertexBuffer_Cube_Position = NULL;
	}

	if (gpIDXGISwapChain) {
		gpIDXGISwapChain->Release();
		gpIDXGISwapChain = NULL;
	}
	if (gpID3D11DeviceContext) {
		gpID3D11DeviceContext->Release();
		gpID3D11DeviceContext = NULL;
	}

	if (gpID3D11Device) {
		gpID3D11Device->Release();
		gpID3D11Device = NULL;
	}

	if (gpFile) {
		fopen_s(&gpFile, gszLogFileName, "a+");
		fprintf_s(gpFile, "uninitialize() Succeeded\n");
		fprintf_s(gpFile, "Log File Is Successfully Closed.\n");
		fclose(gpFile);
	}
}