#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#include "list_aux.h"

node_t* get_node(data_t data){
	node_t *newNode = (node_t *)malloc(sizeof(node_t));
	newNode->data = (char *)malloc(BUFFER_SIZE);
	strcpy(newNode->data, data);
	newNode->next = NULL;
	return newNode;
}

node_t* get_last_node(list_t *list){
	while(list->next != NULL)
		list = list->next;
	return list;
}

node_t* get_prev_node(list_t *list , node_t *e_node){
	while(list != NULL){
		if(list->next != NULL && list->next == e_node)
			return list;
		list = list->next;
	}
	return list;
}

void generic_insert(node_t *prev, node_t *target){
	target->next = prev->next;
	prev->next = target;
}

void generic_delete(node_t *prev , node_t *target){
	prev->next = target->next;
	free(target);
}

node_t *search_node(list_t *list , data_t data){
	while(list != NULL){
		if(strcmp(list->data, data) == STRING_EQUAL){
			return list;
		}
		list = list->next;
	}
	return list;
}

node_t *search_prev_node(list_t *list, data_t data){
	while(list != NULL){
		if(list->next != NULL && strcmp(list->next->data, data) == STRING_EQUAL){
			return list;
		}
		list = list->next;
	}
	return list;
}

void* xcalloc( int number_of_elements, int size_of_elements){
	
}


