#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#include "list_aux.h"

node_t* get_node(data_t data){
	node_t *newNode = (node_t *)malloc(sizeof(node_t));
	newNode->data = (char *)malloc(BUFFER_SIZE);
	strcpy(newNode->data, data);
	newNode->next = NULL;
	return newNode;
}

node_t* get_last_node(list_t *list){
	node_t *trackPtr = list;
	do	
		trackPtr = trackPtr->next;		
	while(trackPtr->next != list);

	return trackPtr;
}

node_t* get_prev_node(list_t *list , node_t *e_node){
	node_t *trackPtr = list;
	do{
		if(list->next != NULL && list->next == e_node)
			return list;
		list = list->next;
	}
	while(trackPtr->next != list);
	return NULL;
}

void generic_insert(node_t *prev, node_t *target){
	target->next = prev->next;
	prev->next = target;
}

void generic_delete(node_t *prev , node_t *target){
	prev->next = target->next;
	free(target);
}

node_t *search_node(list_t *list , data_t data){
	node_t *trackPtr = list;
	do{
		if(strcmp(trackPtr->data, data) == STRING_EQUAL){
			return trackPtr;
		}
		trackPtr = trackPtr->next;
	}
	while(trackPtr->next != list);
	return NULL;
}

node_t *search_prev_node(list_t *list, data_t data){
	node_t *trackPtr = list;
	do{
		if(strcmp(trackPtr->next->data, data) == STRING_EQUAL){
			return trackPtr;
		}
		trackPtr = trackPtr->next;
	}
	while(trackPtr->next != list);
	return NULL;
}

void* xcalloc( int number_of_elements, int size_of_elements){
	
}


