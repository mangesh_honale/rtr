#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#include "list.h"
#include "list_aux.h"


list_t* create_list(){
	list_t *listHead = NULL;
	return listHead;
}

result_t display_list(list_t  *list){
	node_t *track = list;
	do{		
		printf("\n%s", track->data);		
		track = track->next;
	}
	while(track != list);
}

result_t insert_beg(list_t **list , data_t ndata){
	if(!is_empty(*list)){
		node_t *trackPtr = get_last_node(*list);
		node_t *node = get_node(ndata);	
		node->next = *list;
		(*list) = node;
		trackPtr->next = *list;
		return 1;
	}
	(*list) = get_node(ndata);
	(*list)->next = *list;
	return 1;
}

result_t insert_end(list_t **list , data_t ndata){
	
	if(*list != NULL){
		node_t *node = get_node(ndata);
		node_t *trackPtr = get_last_node(*list);	
		generic_insert(trackPtr, node);
		node->next = *list;
	}else{
		(*list) = get_node(ndata);
		(*list)->next = *list;
	}
	return 1;
}

result_t insert_after_data(list_t **list ,data_t edata, data_t ndata){
	node_t *trackPtr = search_node(*list, edata);
		
	if(trackPtr != NULL){
		node_t *node = get_node(ndata);
		generic_insert(trackPtr, node);
		return 1;
	}
	return 0;
}

result_t insert_before_data(list_t **list , data_t edata , data_t ndata){
	node_t *trackPtr;
	trackPtr = search_prev_node(*list, edata);
	if(trackPtr != NULL){
		node_t *node = get_node(ndata);
		generic_insert(trackPtr, node);
		return 1;
	}
	return 0;
}

result_t delete_end(list_t **list){
	
	node_t *trackPtr = get_prev_node(*list, get_last_node(*list));
	if(trackPtr != NULL){
		generic_delete(trackPtr, trackPtr->next);
		trackPtr->next = *list;
		return 1;
	}
	return 0;
}

result_t delete_beg(list_t **list){
	node_t *delNode = *list;
	*list = (*list)->next;
	node_t *lastNode = get_last_node(*list);
	lastNode->next = *list;
	free(delNode);
	return 1;
}

result_t delete_data(list_t **list , data_t edata){
	node_t *trackPtr = *list;
	if(strcmp(trackPtr->data, edata) == STRING_EQUAL)
		return delete_beg(list);
	trackPtr = search_prev_node(*list, edata);
	if(trackPtr != NULL){
		generic_delete(trackPtr, trackPtr->next);
		return 1;
	}
	return 0;
}

result_t search_list(list_t *list , data_t sdata){
	node_t *node = search_node(list, sdata);
	if(node != NULL)
		return 1;
	return 0;
}

result_t find_n_replace(list_t *list , data_t edata , data_t ndata){
	node_t *node = search_node(list, edata);
	if(node != NULL){
		strcpy(node->data, ndata);
		return 1;
	}
	return 0;
}

result_t destroy_list(list_t **p_list){
	node_t *delNode;
	while((*p_list)->next != *p_list){
		delNode = (*p_list);
		(*p_list) = (*p_list)->next;
		free(delNode);
	} 
	*p_list = NULL;
	return 1;
}

result_t is_empty(list_t *list){
	if(list == NULL)
		return 1;
	else
		return 0;
}

len_t length_of_list(list_t *my_list){
	if(!is_empty(my_list)){
		int listLength = 0;
		while(my_list != NULL){
			listLength++;
			my_list = my_list->next;
		}
		
		return listLength;
	}
	return 0;
}

result_t reverse_list(list_t **list ){
	if(length_of_list((*list)) > 1){
		
		node_t *preTrackPtr = (*list), *trackPtr = (*list), *curTrackPtr = (*list)->next;
		trackPtr->next = NULL;
		while(curTrackPtr != NULL){
			preTrackPtr = curTrackPtr;
			curTrackPtr = curTrackPtr->next;
			preTrackPtr->next = trackPtr;
			trackPtr = preTrackPtr;
		}
		*list = trackPtr;
		
		return 1;
	}
	return 0;
}

result_t sort_list(list_t *my_list){
	if(length_of_list((my_list)) > 1){
		node_t *firstTrackPtr = my_list;
		char tempData[255];
		while(firstTrackPtr->next != my_list){
			node_t *secondTrackPtr = firstTrackPtr->next;		
			while(secondTrackPtr->next != my_list){
				if(strcmp(secondTrackPtr->data, firstTrackPtr->data) < 0){
					strcpy(tempData, firstTrackPtr->data);
					strcpy(firstTrackPtr->data, secondTrackPtr->data);
					strcpy(secondTrackPtr->data, tempData);	
				}
				secondTrackPtr = secondTrackPtr->next;
			}
			firstTrackPtr = firstTrackPtr->next;
		}
		return 1;		
	}
	return 0;
}

result_t merge_list (list_t *src_list, list_t *dest_list){
	node_t *trackPtr = src_list;
	if(!is_empty(src_list) && !is_empty(dest_list)){
		while(trackPtr->next != src_list)
			trackPtr = trackPtr->next;
	trackPtr->next = dest_list;
	node_t *lastNode = get_last_node(dest_list);
	lastNode->next = src_list;
	return 1;
	}
	return 0;
}
