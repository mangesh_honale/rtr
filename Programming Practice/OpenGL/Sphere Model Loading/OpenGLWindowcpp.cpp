//Windows headers
#define _CRT_SECURE_NO_WARNINGS 
#include <windows.h>

//OpenGL headers
#include <gl/GL.h>
#include <gl/GLU.h>
#include "Header.h"
//C headers
#include<stdio.h>
#include<stdlib.h>

//c++ headers
#include<vector>

//symbolice constants
#define TRUE   1 
#define FALSE  0

#define BUFFER_SIZE   256 //maximum length of line in mesh file
#define S_EQUAL       0   //return value of strcmp when strings are equal

#define WIN_INIT_X   100 //X-coordinate to top-right corner
#define WIN_INIT_Y   100 //Y-coordinate to top left corner

#define WIN_WIDTH    800
#define WIN_HEIGHT   600

#define NR_POINT_COORDS 3   // Number of point coordinates
#define NR_TEXTURE_COORDS 2 // Number of texture coordinates
#define NR_NORMAL_COORDS 3 // Number of normal coordinates
#define NR_FACE_TOKENS   3 // Minimum number of entries in face data
#define FOY_ANGLE         45 // Field of view in Y direction
#define ZNEAR			0.1
#define ZFAR			200.0

#define VIEWPORT_BOTTOMLEFT_X	0
#define VIEWPORT_BOTTOMLEFT_Y	0

#define MONKEYHEAD_X_TRANSLATE 0.0f   // X-translation of monkeyhead
#define MONKEYHEAD_Y_TRANSLATE 0.0f   // Y-translation of monkeyhead
#define MONKEYHEAD_Z_TRANSLATE -5.0f  // Z-translation of monkeyhead

#define MONKEYHEAD_X_SCALE_FACTOR    1.5f // X-scale factor of monkeyhead
#define MONKEYHEAD_Y_SCALE_FACTOR    1.5f // Y-scale factor of monkeyhead
#define MONKEYHEAD_Z_SCALE_FACTOR    1.5f // Z-scale factor of monkeyhead

#define START_ANGLE_POS              0.0f  // Marks beginging angle position of rotation
#define END_ANGLE_POS                360.0f  // Marks terminating angle position rotation
#define MONKEYHEAD_ANGLE_INCREMENT   1.0f // Increment angle for monkeyhead

#pragma comment(lib, "user32.lib")
#pragma comment(lib, "gdi32.lib")
#pragma comment(lib, "kernel32.lib")
#pragma comment(lib, "opengl32.lib")
#pragma comment(lib, "glu32.lib")



// Global function declaration
LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);

// Global variable declaration
HWND ghwnd = NULL;
HDC ghdc = NULL;
HGLRC ghrc = NULL;



DWORD dwStyle;
WINDOWPLACEMENT wpPrev = { sizeof(WINDOWPLACEMENT) };

int giScreenWidth;
int giScreenHeight;

bool gbActiveWindow = false;
bool gbEscapeKeyIsPressed = false;
bool gbFullScreen = false;
GLfloat g_rotate = 0.0f;
GLuint texture_earth;
// Vector of vectors of floats to hold vertex data
std::vector<std::vector<float>> g_vertices;

// Vector of vectors of float to hold texture data
std::vector<std::vector<float>> g_texture;

// Vecctor of vectors of float to hold normal data
std::vector<std::vector<float>> g_normal;

// Vector of vectors of int to hold index data in g_vertices
std::vector<std::vector<int>> g_face_tri, g_face_texture, g_face_normals;

// Handle to mesh file
FILE *g_fp_meshfile = NULL;

// Handle to log file
FILE *g_file_logfile = NULL;

// Hold line in a file
char line[BUFFER_SIZE];

// Light properties
GLfloat light_ambient[] = { 0.0f, 0.0f, 0.0f, 0.0f };
GLfloat light_defuse[] = { 0.69f, 0.69f, 0.58f, 1.0f };
GLfloat light_specular[] = { 0.65f, 0.69f, 0.58f, 1.0f };
GLfloat light_position[] = { 3.0f, 0.0f, 3.0f, 0.0f };

// Material properties
GLfloat material_specular[] = { 1.0f, 1.0f, 1.0f, 1.0f };
GLfloat material_shininess[] = { 50.0f };

// Track global light status
BOOL gLightOn = FALSE;

// main()
int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpszCmdLine, int iCmdShow) {
	// Function prototype
	void Initialize(void);
	void Uninitialize(void);
	void Display(void);
	void Update(void);

	// Variable declaration
	WNDCLASSEX wndClass;
	HWND hwnd;
	MSG msg;
	TCHAR szClassName[] = TEXT("RTROGL");
	bool bDone = false;
	giScreenWidth = GetSystemMetrics(SM_CXSCREEN);
	giScreenHeight = GetSystemMetrics(SM_CYSCREEN);


	// Code
	// Initializing members of struct WNDCLASSEX
	wndClass.cbSize = sizeof(WNDCLASSEX);
	wndClass.style = CS_HREDRAW | CS_VREDRAW | CS_OWNDC;
	wndClass.cbClsExtra = 0;
	wndClass.cbWndExtra = 0;
	wndClass.hInstance = hInstance;
	wndClass.hbrBackground = (HBRUSH)GetStockObject(BLACK_BRUSH);
	wndClass.hIcon = LoadIcon(NULL, IDI_APPLICATION);
	wndClass.hCursor = LoadCursor(NULL, IDC_ARROW);
	wndClass.hIconSm = LoadIcon(NULL, IDI_APPLICATION);
	wndClass.lpfnWndProc = WndProc;
	wndClass.lpszClassName = szClassName;
	wndClass.lpszMenuName = NULL;

	// Registering class
	RegisterClassEx(&wndClass);

	// Create window
	hwnd = CreateWindowEx(WS_EX_APPWINDOW,
		szClassName,
		TEXT("Blender Model Loading: Venus Planet"),
		WS_OVERLAPPEDWINDOW | WS_CLIPCHILDREN | WS_CLIPSIBLINGS | WS_VISIBLE,
		(giScreenWidth / 2) - 400,
		(giScreenHeight / 2) - 300,
		WIN_WIDTH,
		WIN_HEIGHT,
		NULL,
		NULL,
		hInstance,
		NULL);
	ghwnd = hwnd;

	// Initialize
	Initialize();

	ShowWindow(hwnd, SW_SHOW);
	SetForegroundWindow(hwnd);
	SetFocus(hwnd);

	// Message loop
	while (bDone == false)
	{
		if (PeekMessage(&msg, NULL, 0, 0, PM_REMOVE))
		{
			if (msg.message == WM_QUIT)
				bDone = true;
			else
			{
				TranslateMessage(&msg);
				DispatchMessage(&msg);
			}
		}
		else
		{
			if (gbActiveWindow == true)
			{
				if (gbEscapeKeyIsPressed == true)
					bDone = true;
				Update();
				Display();
			}
		}
	}
	Uninitialize();
	return((int)msg.wParam);
}

// WndProc()
LRESULT CALLBACK WndProc(HWND hwnd, UINT iMsg, WPARAM wParam, LPARAM lParam)
{
	// Function prototype
	void Resize(int, int);
	void ToggleFullscreen(void);
	void Uninitialize(void);

	// Code
	switch (iMsg)
	{
	case WM_ACTIVATE:
		if (HIWORD(wParam) == 0)
			gbActiveWindow = true;
		else
			gbActiveWindow = false;
		break;
	case WM_SIZE:
		Resize(LOWORD(lParam), HIWORD(lParam));
		break;
	case WM_KEYDOWN:
		switch (wParam)
		{
		case 0x4C:
			if (gLightOn) {
				glDisable(GL_LIGHTING);
				gLightOn = FALSE;
			}
			else {
				glEnable(GL_LIGHTING);
				gLightOn = TRUE;
			}

			break;
		case VK_ESCAPE:
			gbEscapeKeyIsPressed = true;
			break;
		case 0x46:
			if (gbFullScreen == false)
			{
				ToggleFullscreen();
				gbFullScreen = true;
			}
			else
			{
				ToggleFullscreen();
				gbFullScreen = false;
			}
			break;
		}
		break;
	case WM_DESTROY:
		PostQuitMessage(0);
		break;
	default:
		break;
	}
	return(DefWindowProc(hwnd, iMsg, wParam, lParam));
}

void ToggleFullscreen(void)
{
	// Variable declaration
	MONITORINFO  mi;
	int x, y;

	// Code
	if (gbFullScreen == false)
	{
		dwStyle = GetWindowLong(ghwnd, GWL_STYLE);
		if (dwStyle & WS_OVERLAPPEDWINDOW)
		{
			mi = { sizeof(MONITORINFO) };
			if (GetWindowPlacement(ghwnd, &wpPrev) && GetMonitorInfo(MonitorFromWindow(ghwnd, MONITORINFOF_PRIMARY), &mi))
			{
				SetWindowLong(ghwnd, GWL_STYLE, dwStyle & ~WS_OVERLAPPEDWINDOW);
				SetWindowPos(ghwnd, HWND_TOP, mi.rcMonitor.left, mi.rcMonitor.top, mi.rcMonitor.right - mi.rcMonitor.left, mi.rcMonitor.bottom - mi.rcMonitor.top, SWP_NOZORDER | SWP_FRAMECHANGED);
			}
		}
		ShowCursor(FALSE);
	}
	else
	{
		// Code
		x = (giScreenWidth / 2) - ((wpPrev.rcNormalPosition.right - wpPrev.rcNormalPosition.left) / 2);
		y = (giScreenHeight / 2) - ((wpPrev.rcNormalPosition.bottom - wpPrev.rcNormalPosition.top) / 2);
		SetWindowLong(ghwnd, GWL_STYLE, dwStyle | WS_OVERLAPPEDWINDOW);
		SetWindowPlacement(ghwnd, &wpPrev);
		SetWindowPos(ghwnd, HWND_TOP, x, y, 0, 0, SWP_NOSIZE | SWP_NOOWNERZORDER | SWP_NOZORDER | SWP_FRAMECHANGED);
		ShowCursor(TRUE);
	}
}

void Initialize(void)
{
	// Function declarations
	void Resize(int, int);
	void Uninitialize(void);
	void LoadMeshData(void);
	int LoadGLTextures(GLuint *, TCHAR[]);

	fopen_s(&g_file_logfile, "MONKEYHEADLOADER.LOG", "w");
	if (!g_file_logfile)
		Uninitialize();
	PIXELFORMATDESCRIPTOR pfd;
	int iPixelFormatIndex = -1;

	ZeroMemory((void*)&pfd, sizeof(PIXELFORMATDESCRIPTOR));

	pfd.nSize = sizeof(PIXELFORMATDESCRIPTOR);

	pfd.nVersion = 1;
	pfd.dwFlags = PFD_DRAW_TO_WINDOW | PFD_SUPPORT_OPENGL | PFD_DOUBLEBUFFER;
	pfd.iPixelType = PFD_TYPE_RGBA;
	pfd.cColorBits = 32;
	pfd.cRedBits = 8;
	pfd.cGreenBits = 8;
	pfd.cBlueBits = 8;
	pfd.cAlphaBits = 8;
	pfd.cDepthBits = 24;

	ghdc = GetDC(ghwnd);

	iPixelFormatIndex = ChoosePixelFormat(ghdc, &pfd);
	if (iPixelFormatIndex == 0)
	{
		ReleaseDC(ghwnd, ghdc);
		ghdc = NULL;
	}
	if (SetPixelFormat(ghdc, iPixelFormatIndex, &pfd) == false)
	{
		ReleaseDC(ghwnd, ghdc);
		ghdc = NULL;
	}

	ghrc = wglCreateContext(ghdc);
	if (ghrc == NULL)
	{
		ReleaseDC(ghwnd, ghdc);
		ghdc = NULL;
	}

	if (wglMakeCurrent(ghdc, ghrc) == false)
	{
		wglDeleteContext(ghrc);
		ghrc == NULL;
		ReleaseDC(ghwnd, ghdc);
		ghrc = NULL;
	}

	glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
	glEnable(GL_DEPTH_TEST);
	glDepthFunc(GL_LEQUAL);
	glClearDepth(1.0f);
	glShadeModel(GL_SMOOTH);
	glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST);

	glLightfv(GL_LIGHT0, GL_SPECULAR, light_specular);
	glLightfv(GL_LIGHT0, GL_AMBIENT, light_ambient);
	glLightfv(GL_LIGHT0, GL_DIFFUSE, light_defuse);
	glLightfv(GL_LIGHT0, GL_POSITION, light_position);
	glMaterialfv(GL_FRONT, GL_SPECULAR, material_specular);
	glMaterialfv(GL_FRONT, GL_SHININESS, material_shininess);
	glEnable(GL_LIGHT0);

	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);

	glEnable(GL_TEXTURE_2D);
	LoadGLTextures(&texture_earth, MAKEINTRESOURCE(IDBITMAP_EARTH));
	LoadMeshData();
	Resize(WIN_WIDTH, WIN_HEIGHT);
}

int LoadGLTextures(GLuint *texture, TCHAR imageResourceId[]) {
	HBITMAP hBitmap = NULL;
	BITMAP bmp;
	int iStatus = FALSE;
	hBitmap = (HBITMAP)LoadImage(GetModuleHandle(NULL), imageResourceId, IMAGE_BITMAP, 0, 0, LR_CREATEDIBSECTION);
	if (hBitmap) {
		iStatus = TRUE;

		GetObject(hBitmap, sizeof(bmp), &bmp);
		glGenTextures(1, texture);
		glBindTexture(GL_TEXTURE_2D, *texture);
		glPixelStorei(GL_UNPACK_ALIGNMENT, 4);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
		gluBuild2DMipmaps(GL_TEXTURE_2D, 3, bmp.bmWidth, bmp.bmHeight, GL_BGR_EXT, GL_UNSIGNED_BYTE, bmp.bmBits);
		DeleteObject(hBitmap);
	}

	return (iStatus);
}

void LoadMeshData() {
	void Uninitialize();

	// Open mesh file
	fopen_s(&g_fp_meshfile, ("Planet_Sphere.OBJ"), "r");
	if (!g_fp_meshfile)
		Uninitialize();

	// Separate strings 
	// String holding space separator for strtok
	char *sep_space = " ";
	// String holding forward slash separator for strtok
	char *sep_fslash = "/";

	// Token pointers
	// Character pointer for holding first word in a line
	char *first_token = NULL;
	// Character pointer for holding next word separated by
	// specified separtor to strtok
	char *token = NULL;

	// Array of character pointers to hold strings of face entries
	// Face entries can be variable. In some files they are three
	//  and in some files they are four
	char *face_tokens[NR_FACE_TOKENS];
	// no of non-null tokens in above vector
	int nr_tokens;

	// Character pointer for holding string associated with vertex index
	char *token_vertex_index = NULL;
	// Character pointer for holding string associated with texture index
	char *token_texture_index = NULL;
	// Character pointer for holding string associated with normal index
	char *token_normal_index = NULL;

	// While there is line in a file
	while (fgets(line, BUFFER_SIZE, g_fp_meshfile) != NULL) {

		// Bind line to a separator and get first token
		first_token = strtok(line, sep_space);

		// If first token indicates vertex data
		if (strcmp(first_token, "v") == S_EQUAL) {
			// Create a vector of NR_POINT_COORDS number of floats to hold coordinates
			std::vector<float> vec_point_coord(NR_POINT_COORDS);

			for (int i = 0; i != NR_POINT_COORDS; i++)
				vec_point_coord[i] = atof(strtok(NULL, sep_space));
			g_vertices.push_back(vec_point_coord);
		}

		// If first token indicates texture data
		if (strcmp(first_token, "vt") == S_EQUAL) {
			std::vector<float> vec_texture_coord(NR_TEXTURE_COORDS);

			for (int i = 0; i != NR_TEXTURE_COORDS; i++)
				vec_texture_coord[i] = atof(strtok(NULL, sep_space));
			g_texture.push_back(vec_texture_coord);
		}

		// If first token indicates normal data
		if (strcmp(first_token, "vn") == S_EQUAL) {
			std::vector<float> vec_normal_coord(NR_NORMAL_COORDS);
			for (int i = 0; i != NR_NORMAL_COORDS; i++)
				vec_normal_coord[i] = atof(strtok(NULL, sep_space));
			g_normal.push_back(vec_normal_coord);
		}

		// If first token indicates face data
		else if (strcmp(first_token, "f") == S_EQUAL) {
			// Define three vectors of integers with length 3 to hold indices of triangle's
			// positional coordinates, texture coordinates, and normal coordinates in g_vertices,
			// g_textures and g_normal resp.

			std::vector<int> triangle_vertex_indices(3), texture_vertex_indices(3), normal_vertex_indices(3);

			// Initialize all char pointers in face_tokens to NULL
			memset((void*)face_tokens, 0, NR_FACE_TOKENS);

			// Extract three fields of information in face_tokens and increment nr_tokens accordingly
			nr_tokens = 0;
			while (token = strtok(NULL, sep_space)) {
				if (strlen(token) < 3) {
					break;
				}
				face_tokens[nr_tokens] = token;
				nr_tokens++;
			}

			// Every face data entry is going to have minimum three fields
			// therefore, construct a triangle out of it with  
			// S1:   triangle coordinates data and
			// S2: texture coordinates index data
			// S3: normal coodinate index data
			// S4: Put that data in triangle_vertex_indices, texture_vertex_indices,
			//	   normal_vertex_indices. Vectors will be constructed at the end of the loop

			for (int i = 0; i != NR_FACE_TOKENS; ++i) {
				token_vertex_index = strtok(face_tokens[i], sep_fslash);
				token_texture_index = strtok(NULL, sep_fslash);
				token_normal_index = strtok(NULL, sep_fslash);
				triangle_vertex_indices[i] = atoi(token_vertex_index);
				texture_vertex_indices[i] = atoi(token_texture_index);
				normal_vertex_indices[i] = atoi(token_normal_index);
			}

			// Add constructed vectors to global face vectors
			g_face_tri.push_back(triangle_vertex_indices);
			g_face_texture.push_back(texture_vertex_indices);
			g_face_normals.push_back(normal_vertex_indices);


		}
		// Initialize line buffer to NULL
		memset((void *)line, (int)'\0', BUFFER_SIZE);
	}

	fclose(g_fp_meshfile);
	g_fp_meshfile = NULL;

	// Log vertex, texture and face data in log file
	fprintf(g_file_logfile, "g_vertices: %llu g_texture: %llu g_normals: %llu g_face_tri: %llu\n", g_vertices.size(), g_texture.size(), g_normal.size(),
		g_face_tri.size());

}

void Display(void)
{

	// Code
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

	glTranslatef(0.0f, 0.0f, -10.0f);
	glRotatef(g_rotate, 0.0f, 1.0f, 0.0f);
	glScalef(MONKEYHEAD_X_SCALE_FACTOR, MONKEYHEAD_Y_SCALE_FACTOR, MONKEYHEAD_Z_SCALE_FACTOR);

	//Keep counter-clockwise winding of vertices of geometry
	glFrontFace(GL_CCW);
	//glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);

	glBindTexture(GL_TEXTURE_2D, texture_earth);
	for (int i = 0; i != g_face_tri.size(); i++) {

		glBegin(GL_TRIANGLES);

		for (int j = 0; j != g_face_tri[i].size(); j++) {
			int vi = g_face_tri[i][j] - 1;
			int ni = g_face_normals[i][j] - 1;
			int ti = g_face_texture[i][j] - 1;
			glNormal3f(g_normal[ni][0], g_normal[ni][1], g_normal[ni][2]);
			glTexCoord2f(g_texture[ti][0], g_texture[ti][1]);
			glVertex3f(g_vertices[vi][0], g_vertices[vi][1], g_vertices[vi][2]);
		}
		glEnd();
	}
	SwapBuffers(ghdc);
}

void Resize(int width, int height)
{
	// Code
	if (height == 0)
	{
		height = 1;
	}

	glViewport(VIEWPORT_BOTTOMLEFT_X, VIEWPORT_BOTTOMLEFT_Y, (GLsizei)width, (GLsizei)height);

	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	gluPerspective(FOY_ANGLE, (GLfloat)width / (GLfloat)height, ZNEAR, ZFAR);
}

void Uninitialize(void)
{
	// Uninitialization code

	if (gbFullScreen == true)
	{
		dwStyle = GetWindowLong(ghwnd, GWL_STYLE);
		SetWindowLong(ghwnd, GWL_STYLE, dwStyle | WS_OVERLAPPEDWINDOW);
		SetWindowPlacement(ghwnd, &wpPrev);
		SetWindowPos(ghwnd, HWND_TOP, 0, 0, 0, 0, SWP_NOMOVE | SWP_NOSIZE | SWP_NOZORDER | SWP_NOOWNERZORDER | SWP_FRAMECHANGED);
		ShowCursor(TRUE);
	}

	wglMakeCurrent(NULL, NULL);

	wglDeleteContext(ghrc);
	ghrc = NULL;

	ReleaseDC(ghwnd, ghdc);
	ghdc = NULL;

	fclose(g_file_logfile);
	g_file_logfile = NULL;
	DestroyWindow(ghwnd);
	ghwnd = NULL;
}


void Update() {
	//g_rotate = g_rotate + MONKEYHEAD_ANGLE_INCREMENT;

	/*if (g_rotate >= END_ANGLE_POS)
	g_rotate = START_ANGLE_POS;*/
}