#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#include "list.h"

int main() {

	int operation = 0, result;
	list_t *list = create_list();
	list_t *list_2 = create_list();
	char data[BUFFER_SIZE], pivot[BUFFER_SIZE];
	while (operation != 15) {
		
		printf("\n1. Insert at begining\n2. Insert at end \n3. Insert after data\n4. Insert before data\n5. Delete from end\n6. Delete from begining\n7. Delete data\n8. Search data\n9. Find and replace data\n10. Print List\n11. Destroy list\n12. Reverse list\n13. Sort list\n14. Merge lists\n15. Exit\nEnter your choice:");

		scanf("%d", &operation);

		switch (operation) {

		case 1:
			printf("Enter name:");
	    	        scanf("%s", data);
			result = insert_beg(&list, data);
			if(result)
				printf("Inserted successfully.");
			break;

		case 2: printf("Enter name:");
	    	        scanf("%s", data);
			result = insert_end(&list, data);
			if(result)
				printf("Inserted successfully.");
			else
				printf("Error: Operation failed. Could not allocate memory.");
			break;

		case 3: printf("Enter name:");
	    	        scanf("%s", data);
			printf("\nInsert after:");
			scanf("%s", pivot);
			result = insert_after_data(&list, pivot, data);
			if(result)
				printf("Inserted successfully.");
			else
				printf("Error: Operation failed. Data not found!");
			break;
		case 4: printf("Enter name:");
	    	        scanf("%s", data);
			printf("\nInsert before:");
			scanf("%s", pivot);
			result = insert_before_data(&list, pivot, data);
			if(result)
				printf("Inserted successfully.");
			else
				printf("Error: Operation failed. Data not found.");
			break;	
		case 5: result = delete_end(&list);
			if(result)
				printf("Deleted successfully.");
			break;	
		case 6: result = delete_beg(&list);
			break;	
		case 7: printf("Enter data to be deleted:");
			scanf("%s", data);
			result = delete_data(&list, data);
			if(result)
				printf("Data deleted successfully.");
			break;
		case 8: printf("Enter data to be searched:");
			scanf("%s", data);
			result = search_list(list, data);
			if(result)
				printf("Data found.");
			else
				printf("Data not found.");
			break;
		case 9: printf("Enter data to be replaced:");
			scanf("%s", pivot);
			printf("Enter data to be replaced by:");
			scanf("%s", data);
			result = find_n_replace(list, pivot, data);
			if(result)
				printf("Data found and replaced.");
			else
				printf("Data not found.");
			break;
		case 10: display_list(list);
			break;
		case 11: result = destroy_list(&list);
			 if(result)
				printf("List destroyed.");
			break;
		case 12: result = reverse_list(&list);
			 if(result)
				printf("Reversed the list successfully.");
			 else
				printf("Error: Something went wrong.");
			 break;	
		case 13: result = sort_list(list);
			 if(result)
				printf("Sorted the list successfully.");
			 else
				printf("Error: Something went wrong.");
			 break;		
		case 14: 
			 strcpy(data, "List 2 Node 1");
			 insert_beg(&list_2, data);
			 strcpy(data, "List 2 Node 2");
			 insert_end(&list_2, data);
			 strcpy(data, "List 2 Node 3");
			 insert_end(&list_2, data);
			result = merge_list(list, list_2);
			if(result)
				printf("Merged the lists successfully.");
			 else
				printf("Error: Something went wrong.");
			break;
		case 15:exit(0);
		default:
			printf("Wrong choice.");
			break;



		}

	}

	
return 0;
}


