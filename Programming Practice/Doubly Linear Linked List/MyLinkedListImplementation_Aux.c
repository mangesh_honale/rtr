#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#include "list_aux.h"

node_t* get_node(data_t data){
	printf("Entered get_node (aux).\n");
	node_t *newNode = (node_t *)malloc(sizeof(node_t));
	if(newNode == NULL)
		return NULL;
	newNode->data = (char *)malloc(BUFFER_SIZE);
	if(newNode->data == NULL)
		return NULL;
	strcpy(newNode->data, data);
	newNode->next = NULL;
	newNode->previous = NULL;
	printf("Exiting get_node (aux).\n");
	return newNode;
}

node_t* get_last_node(list_t *list){
	printf("Entered get_last_node (aux).\n");
	while(list->next != NULL)
		list = list->next;
	printf("Exiting get_last_node (aux).\n");
	return list;
}

node_t* get_prev_node(list_t *list , node_t *e_node){
	printf("Entered get_prev_node (aux).\n");
	while(list != NULL){
		if(list == e_node)
			return list->previous;
		list = list->next;
	}
	printf("Exiting get_prev_node (aux).\n");
	return list;
}

void generic_insert(node_t *prev, node_t *target){
	printf("Entered generic_insert (aux).\n");
	target->next = prev->next;
	target->previous = prev;
	if(prev->next != NULL)
		prev->next->previous = target;
	prev->next = target;
	printf("Exiting generic_insert (aux).\n");
}

void generic_delete(node_t *prev , node_t *target){
	printf("Entered generic_delete (aux).\n");
	prev->next = target->next;
	if(target->next != NULL){
		target->next->previous = prev;
	}
	free(target);
	printf("Exiting generic_delete (aux).\n");
}

node_t *search_node(list_t *list , data_t data){
	printf("Entered search_node (aux).\n");
	while(list != NULL){
		if(strcmp(list->data, data) == STRING_EQUAL){
			return list;
		}
		list = list->next;
	}
	printf("Exiting search_node (aux).\n");
	return list;
}

node_t *search_prev_node(list_t *list, data_t data){
	printf("Entered search_prev_node (aux).\n");
	while(list != NULL){
		if(strcmp(list->data, data) == STRING_EQUAL){
			return list->previous;
		}
		list = list->next;
	}
	printf("Exiting search_prev_node (aux).\n");
	return list;
}

void* xcalloc( int number_of_elements, int size_of_elements){
	
}


