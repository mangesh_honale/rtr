package com.astromedicomp.glesview;


import android.content.Context; // for drawing context related
import android.opengl.GLSurfaceView; // for OpenGL Surface view and all related
import javax.microedition.khronos.opengles.GL10; // for OpenGLES 1.0 needed as param type GL10
import javax.microedition.khronos.egl.EGLConfig; // for EGLConfig needed as param type EGLConfig
import android.opengl.GLES32; // for OpenGLES 3.2
import android.view.Window;  // for "Window" class
import android.view.WindowManager; // for "WindowManager" class
import android.content.pm.ActivityInfo; // for "ActivityInfo" class
import android.graphics.Color; // for "Color" class
import android.widget.TextView; // for "TextView" class
import android.view.Gravity; // for "Gravity" class
import android.view.MotionEvent; // for "MotionEvent" class
import android.view.GestureDetector;
import android.view.GestureDetector.OnGestureListener; // for OnGestureListener
import android.view.GestureDetector.OnDoubleTapListener; // for OnDoubleTapListener

// A view of OpenGLES3 graphics which also receives touch events
public class GLESView extends GLSurfaceView implements GLSurfaceView.Renderer, OnGestureListener, OnDoubleTapListener{
	private final Context context;
	private GestureDetector gestureDetector;
	
	public GLESView(Context drawingContext){
		super(drawingContext);
		
		context = drawingContext;
		// accordingly set EGLContext to current supported version of OpenGL-ES
		setEGLContextClientVersion(3);
		
		// set Renderer for drawing on the GLSurfaceView
		setRenderer(this);
		
		// Render the view only when there is a change in the drawing data
		setRenderMode(GLSurfaceView.RENDERMODE_WHEN_DIRTY);
		
		gestureDetector = new GestureDetector(context, this, null, false); // this means 'handler' i.e. who is going to handle	
		gestureDetector.setOnDoubleTapListener(this); // this means 'handler' who is going to handle
	}
	// Override method of GLSurfaceView.Renderer (Init code)
	@Override
	public void onSurfaceCreated(GL10 gl, EGLConfig config){
		// OpenGL-ES version check
		String version = gl.glGetString(GL10.GL_VERSION);
		System.out.println("VDG: " + version);
		
		initialize(gl);
	}
	
	// override method of GLSurfaceView.Renderer (Change size code)
	@Override
	public void onSurfaceChanged(GL10 unused, int width, int height){
		resize(width, height);
	}
	
	@Override
	public void onDrawFrame(GL10 unused){
		draw();
	}
	
	// Handling 'onTouchEvent' is the most important because it triggers all gestureand tap events
	@Override
	public boolean onTouchEvent(MotionEvent event){
		// code
		int eventaction = event.getAction();
		if(!gestureDetector.onTouchEvent(event))
			super.onTouchEvent(event);
		return(true);
	}
    
    // abstract method from OnDoubleTapListener so must be implemented
    @Override
    public boolean onDoubleTap(MotionEvent e){
        System.out.println("VGD: Double Tap");
        return(true);
    }
    
    // abstract method from OnDoubleTapListener so must be implemented
    @Override
    public boolean onDoubleTapEvent(MotionEvent e){
        // Do Not Write Any code Here Because Already Written 'onDoubleTap'
        return(true);
    }
    
    // abstract method from OnDoubleTapListener so must be implemented
    @Override
    public boolean onSingleTapConfirmed(MotionEvent e){
        System.out.println("VDG: Single Tap");
        return(true);
    }
    
    // abstract method from OnGestureListener so must be implemented
    @Override
    public boolean onDown(MotionEvent e){
        // Do Not Write Any code Here Because Already Written 'onSingleTapConfirmed'
        return(true);
    }
    
    // abstract method from OnGestureListener so must be implemented
    @Override
    public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX, float velocityY){
        return(true);
    }
    
    // abstract method from OnGestureListener so must be implemented
    @Override
    public void onLongPress(MotionEvent e){
        System.out.println("VDG: Long Press");
    }
    
    // abstract method from OnGestureListener so must be implemented
    @Override
    public boolean onScroll(MotionEvent e1, MotionEvent e2, float distanceX, float distanceY){
		System.out.println("VDG: Scroll");
        System.exit(0);
		return (true);
    }
    
    // abstract method from OnGestureListener so must be implemented
    @Override
    public void onShowPress(MotionEvent e){
    }
    
    // abstract method from OnGestureListener so must be implemented
    @Override
    public boolean onSingleTapUp(MotionEvent e){
        return(true);
    }
	
	private void initialize(GL10 gl){
		//Set the background frame color
		GLES32.glClearColor(0.0f, 0.0f, 1.0f, 1.0f);
	}
	
	private void resize(int width, int height){
		// Adjust the viewport based on geometry changes, such as screen rotation
		GLES32.glViewport(0, 0, width, height);
	}
	
	public void draw(){
		// Draw background color
		GLES32.glClear(GLES32.GL_COLOR_BUFFER_BIT | GLES32.GL_DEPTH_BUFFER_BIT);
	}
}
