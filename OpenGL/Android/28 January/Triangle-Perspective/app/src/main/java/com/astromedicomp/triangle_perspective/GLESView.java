package com.astromedicomp.triangle_perspective;


import android.content.Context; // for drawing context related
import android.opengl.GLSurfaceView; // for OpenGL Surface view and all related
import javax.microedition.khronos.opengles.GL10; // for OpenGLES 1.0 needed as param type GL10
import javax.microedition.khronos.egl.EGLConfig; // for EGLConfig needed as param type EGLConfig
import android.opengl.GLES32; // for OpenGLES 3.2
import android.view.Window;  // for "Window" class
import android.view.WindowManager; // for "WindowManager" class
import android.content.pm.ActivityInfo; // for "ActivityInfo" class
import android.graphics.Color; // for "Color" class
import android.widget.TextView; // for "TextView" class
import android.view.Gravity; // for "Gravity" class
import android.view.MotionEvent; // for "MotionEvent" class
import android.view.GestureDetector;
import android.view.GestureDetector.OnGestureListener; // for OnGestureListener
import android.view.GestureDetector.OnDoubleTapListener; // for OnDoubleTapListener

// for vbo
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.FloatBuffer;

import android.opengl.Matrix; // for matrix math

// A view of OpenGLES3 graphics which also receives touch events
public class GLESView extends GLSurfaceView implements GLSurfaceView.Renderer, OnGestureListener, OnDoubleTapListener{
	private final Context context;
	private GestureDetector gestureDetector;
	
	private int vertexShaderObject;
	private int fragmentShaderObject;
	private int shaderProgramObject;
	
	private int[] vao = new int[1];
	private int[] vbo = new int[1];
	private int mvpUniform;
	
	private float perspectiveProjectionMatrix[] = new float[16]; // 4x4 matrix

	public GLESView(Context drawingContext){
		super(drawingContext);
		
		context = drawingContext;
		// accordingly set EGLContext to current supported version of OpenGL-ES
		setEGLContextClientVersion(3);
		
		// set Renderer for drawing on the GLSurfaceView
		setRenderer(this);
		
		// Render the view only when there is a change in the drawing data
		setRenderMode(GLSurfaceView.RENDERMODE_WHEN_DIRTY);
		
		gestureDetector = new GestureDetector(context, this, null, false); // this means 'handler' i.e. who is going to handle	
		gestureDetector.setOnDoubleTapListener(this); // this means 'handler' who is going to handle
	}
	// Override method of GLSurfaceView.Renderer (Init code)
	@Override
	public void onSurfaceCreated(GL10 gl, EGLConfig config){
		// OpenGL-ES version check
		String glesVersion = gl.glGetString(GL10.GL_VERSION);
		System.out.println("VDG: " + glesVersion );
		
		// Get GLSL version
		String glslVersion = gl.glGetString(GLES32.GL_SHADING_LANGUAGE_VERSION);
		System.out.println("VDG: GLSL Version= " + glslVersion);
		initialize(gl);
	}
	
	// override method of GLSurfaceView.Renderer (Change size code)
	@Override
	public void onSurfaceChanged(GL10 unused, int width, int height){
		resize(width, height);
	}
	
	@Override
	public void onDrawFrame(GL10 unused){
		display();
	}
	
	// Handling 'onTouchEvent' is the most important because it triggers all gestureand tap events
	@Override
	public boolean onTouchEvent(MotionEvent event){
		// code
		int eventaction = event.getAction();
		if(!gestureDetector.onTouchEvent(event))
			super.onTouchEvent(event);
		return(true);
	}
    
    // abstract method from OnDoubleTapListener so must be implemented
    @Override
    public boolean onDoubleTap(MotionEvent e){
        System.out.println("VDG: Double Tap");
        return(true);
    }
    
    // abstract method from OnDoubleTapListener so must be implemented
    @Override
    public boolean onDoubleTapEvent(MotionEvent e){
        // Do Not Write Any code Here Because Already Written 'onDoubleTap'
        return(true);
    }
    
    // abstract method from OnDoubleTapListener so must be implemented
    @Override
    public boolean onSingleTapConfirmed(MotionEvent e){
        System.out.println("VDG: Single Tap");
        return(true);
    }
    
    // abstract method from OnGestureListener so must be implemented
    @Override
    public boolean onDown(MotionEvent e){
        // Do Not Write Any code Here Because Already Written 'onSingleTapConfirmed'
        return(true);
    }
    
    // abstract method from OnGestureListener so must be implemented
    @Override
    public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX, float velocityY){
        return(true);
    }
    
    // abstract method from OnGestureListener so must be implemented
    @Override
    public void onLongPress(MotionEvent e){
        System.out.println("VDG: Long Press");
    }
    
    // abstract method from OnGestureListener so must be implemented
    @Override
    public boolean onScroll(MotionEvent e1, MotionEvent e2, float distanceX, float distanceY){
		System.out.println("VDG: Scroll");
        System.exit(0);
		return (true);
    }
    
    // abstract method from OnGestureListener so must be implemented
    @Override
    public void onShowPress(MotionEvent e){
    }
    
    // abstract method from OnGestureListener so must be implemented
    @Override
    public boolean onSingleTapUp(MotionEvent e){
        return(true);
    }
	
	private void initialize(GL10 gl){
		// Create vertex shader
		vertexShaderObject = GLES32.glCreateShader(GLES32.GL_VERTEX_SHADER);
		final String vertexShaderSourceObject = String.format(
"#version 320 es"+
"\n"+
"in vec4 vPosition;"+
"uniform mat4 u_mvp_matrix;"+
"void main(void)"+
"{"+
"gl_Position = u_mvp_matrix * vPosition;"+
"}"
);

// provide source code to shader
GLES32.glShaderSource(vertexShaderObject, vertexShaderSourceObject);

// Compile shader & check for errors
GLES32.glCompileShader(vertexShaderObject);
int[] iShaderCompiledStatus = new int[1];
int[] iInfoLogLength = new int[1];
String szInfoLog = null;

GLES32.glGetShaderiv(vertexShaderObject, GLES32.GL_COMPILE_STATUS, iShaderCompiledStatus, 0);
if(iShaderCompiledStatus[0] == GLES32.GL_FALSE){
	GLES32.glGetShaderiv(vertexShaderObject, GLES32.GL_INFO_LOG_LENGTH, iInfoLogLength, 0);
	if(iInfoLogLength[0] > 0){
		szInfoLog = GLES32.glGetShaderInfoLog(vertexShaderObject);
		System.out.println("VDG: Vertex shader compilation log = " + szInfoLog);
		uninitialize();
		System.exit(0);
	}
}

// Fragment shader
fragmentShaderObject = GLES32.glCreateShader(GLES32.GL_FRAGMENT_SHADER);

// fragment shader source code
final String fragmentShaderSourceCode = String.format
(
"#version 320 es"+
"\n"+
"precision highp float;"+
"out vec4 FragColor;"+
"void main(void)"+
"{"+
"FragColor = vec4(1.0, 1.0, 1.0, 1.0);"+
"}"
);

// Provide source code to shader
GLES32.glShaderSource(fragmentShaderObject, fragmentShaderSourceCode);

// Compile shader and check for errors
GLES32.glCompileShader(fragmentShaderObject);
iShaderCompiledStatus[0] = 0; //re-initialize
iInfoLogLength[0] = 0; // re-initialize
szInfoLog = null; // re-initialize

GLES32.glGetShaderiv(fragmentShaderObject, GLES32.GL_COMPILE_STATUS, iShaderCompiledStatus, 0);
if(iShaderCompiledStatus[0] == GLES32.GL_FALSE){
	GLES32.glGetShaderiv(fragmentShaderObject, GLES32.GL_INFO_LOG_LENGTH,iInfoLogLength, 0);
	if(iInfoLogLength[0] > 0){
		szInfoLog = GLES32.glGetShaderInfoLog(fragmentShaderObject);
		System.out.println("VDG: Fragment shader compilation log = " + szInfoLog);
		uninitialize();
		System.exit(0);
	}
}

// Create shader program
shaderProgramObject = GLES32.glCreateProgram();

// attach vertex shader to shader program
GLES32.glAttachShader(shaderProgramObject, vertexShaderObject);

// attach fragment shader to shader program 
GLES32.glAttachShader(shaderProgramObject, fragmentShaderObject);

// pre-link bidning of shader program object with vertex shader attributes
GLES32.glBindAttribLocation(shaderProgramObject, GLESMacros.VDG_ATTRIBUTE_VERTEX, "vPosition");

// Link the two shaders together to shader program //object
GLES32.glLinkProgram(shaderProgramObject);
int[] iShaderProgramLinkStatus = new int[1];
iInfoLogLength[0] = 0;
szInfoLog = null;
GLES32.glGetProgramiv(shaderProgramObject, GLES32.GL_LINK_STATUS, iShaderProgramLinkStatus, 0);
if(iShaderProgramLinkStatus[0] == GLES32.GL_FALSE){
	GLES32.glGetProgramiv(shaderProgramObject, GLES32.GL_INFO_LOG_LENGTH, iInfoLogLength, 0);
	if(iInfoLogLength[0] > 0){
		szInfoLog = GLES32.glGetProgramInfoLog(shaderProgramObject);
		System.out.println("VDG: Shader program link log=" + szInfoLog);
		uninitialize();
		System.exit(0);
	}
}

// get MVP uniform location
mvpUniform = GLES32.glGetUniformLocation(shaderProgramObject, "u_mvp_matrix");

// *** vertices, colors, shader attributes, vbo, vao // intitalizations

final float triangleVertices[] = new float[]{
	0.0f, 1.0f, 0.0f, //apex
	-1.0f, -1.0f, 0.0f, //left bottom
	1.0f, -1.0f, 0.0f //right bottom
};

GLES32.glGenVertexArrays(1, vao, 0);
GLES32.glBindVertexArray(vao[0]);

GLES32.glGenBuffers(1, vbo, 0);
GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, vbo[0]);

ByteBuffer byteBuffer = ByteBuffer.allocateDirect(triangleVertices.length *  4);
byteBuffer.order(ByteOrder.nativeOrder());
FloatBuffer verticesBuffer = byteBuffer.asFloatBuffer();
verticesBuffer.put(triangleVertices);
verticesBuffer.position(0);

GLES32.glBufferData(GLES32.GL_ARRAY_BUFFER, triangleVertices.length * 4, verticesBuffer,
GLES32.GL_STATIC_DRAW);

GLES32.glVertexAttribPointer(GLESMacros.VDG_ATTRIBUTE_VERTEX, 3, GLES32.GL_FLOAT, false, 0, 0);

GLES32.glEnableVertexAttribArray(GLESMacros.VDG_ATTRIBUTE_VERTEX);

GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, 0);
GLES32.glBindVertexArray(0);

// enable depth testing
GLES32.glEnable(GLES32.GL_DEPTH_TEST);

// depth test to do
GLES32.glDepthFunc(GLES32.GL_LEQUAL);

// We will always cull back faces for better //performace
GLES32.glEnable(GLES32.GL_CULL_FACE);
		//Set the background frame color
		GLES32.glClearColor(0.0f, 0.0f, 1.0f, 1.0f);
		
		// set projection matrix to identity matrix
		Matrix.setIdentityM(perspectiveProjectionMatrix, 0);
	}
	
	private void resize(int width, int height){
		// Adjust the viewport based on geometry changes, such as screen rotation
		GLES32.glViewport(0, 0, width, height);
		Matrix.perspectiveM(perspectiveProjectionMatrix, 0, (float)45.0, (float)width/(float)height, (float)0.1, (float)100.0); 

	}
	public void display(){
		// Draw background color
		GLES32.glClear(GLES32.GL_COLOR_BUFFER_BIT | GLES32.GL_DEPTH_BUFFER_BIT);
		// use shader program
		GLES32.glUseProgram(shaderProgramObject);
		// OpenGL-ES drawing
		float modelViewMatrix[] = new float[16];
		float modelViewProjectionMatrix[] = new float[16];
		
		// set modelview & modelviewprojection matrices to 			//identity
		Matrix.setIdentityM(modelViewMatrix, 0);
		Matrix.setIdentityM(modelViewProjectionMatrix, 0);
		
		Matrix.translateM(modelViewMatrix, 0, 0.0f, 0.0f, -3.0f);
		// Multiply the modelview and projection matrix to get 		//modelviewprojection matrix
		Matrix.multiplyMM(modelViewProjectionMatrix, 0, perspectiveProjectionMatrix, 0, modelViewMatrix, 0);
		// pass above modelviewprojection matrix to the vertex 		//shader in 'u_mvp_matrix' whose position value we 			//already calculated in initWithFrame() by using 				//glGetUniformLocation()	
		GLES32.glUniformMatrix4fv(mvpUniform, 1, false, modelViewProjectionMatrix, 0);
		
		// bind vao
		GLES32.glBindVertexArray(vao[0]);
		
		// draw, either by glDrawTriangles() or glDrawArrays() 		//or glDrawElements()
		GLES32.glDrawArrays(GLES32.GL_TRIANGLES, 0, 3);
		
		// unbind vao
		GLES32.glBindVertexArray(0);
		
		// un-use shader program
		GLES32.glUseProgram(0);
		
		//render/flush
		requestRender();
	
	}

void uninitialize(){
	// destroy vao
	if(vao[0] != 0){
		GLES32.glDeleteVertexArrays(1, vao, 0);
		vao[0] = 0;
	}

	// destroy vbo
	if(vbo[0] != 0){
		GLES32.glDeleteBuffers(1, vbo, 0);
		vbo[0] = 0;
	}
	
	if(shaderProgramObject != 0){
		if(vertexShaderObject != 0){
			// detach vertex shader from shader program 			//object
			GLES32.glDetachShader(shaderProgramObject, vertexShaderObject);
			// delete vertex shader object
			GLES32.glDeleteShader(vertexShaderObject);
			vertexShaderObject = 0;
		}
		
		if(fragmentShaderObject != 0){
			// detach fragment  shader from shader program 				//object
                GLES32.glDetachShader(shaderProgramObject, fragmentShaderObject);
                // delete fragment shader object
                GLES32.glDeleteShader(fragmentShaderObject);
                fragmentShaderObject = 0;

		}
	}
		// delete shader program object
        	if(shaderProgramObject != 0)
        	{
           	GLES32.glDeleteProgram(shaderProgramObject);
           	shaderProgramObject = 0;
        	}

}
}