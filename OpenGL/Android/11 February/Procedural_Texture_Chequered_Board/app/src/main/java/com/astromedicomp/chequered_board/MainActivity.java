package com.astromedicomp.chequered_board;

// default supplied packages by android SDK
import android.app.Activity;
import android.os.Bundle;

// later added packages
import android.view.Window;  // for "Window" class
import android.view.WindowManager; // for "WindowManager" class
import android.content.pm.ActivityInfo; // for "ActivityInfo" class
import android.widget.TextView; // for "TextView" class
import android.graphics.Color; // for "Color" class

public class MainActivity extends Activity {
	private GLESView myView;
	
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        
		// this is to get rid of Action bar
		this.requestWindowFeature(Window.FEATURE_NO_TITLE);
		
		// this is done to make fullscreen
		this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
		
		// force activity window orientation to landscape
		MainActivity.this.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
		
		// set black background color
		this.getWindow().getDecorView().setBackgroundColor(Color.rgb(0, 0, 0));
		
		myView = new GLESView(this);
		setContentView(myView);
		
    }
	
	@Override
    protected void onPause()
    {
        super.onPause();
    }
    
    @Override
    protected void onResume()
    {
        super.onResume();
    }

}
