package com.astromedicomp.hello_1;

// default supplied packages by android SDK
import android.app.Activity;
import android.os.Bundle;

// later added packages
import android.view.Window;  // for "Window" class
import android.view.WindowManager; // for "WindowManager" class
import android.content.pm.ActivityInfo; // for "ActivityInfo" class
import android.graphics.Color; // for "Color" class
import android.widget.TextView; // for "TextView" class
import android.view.Gravity; // for "Gravity" class

public class MainActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        
		// this is to get rid of Action bar
		this.requestWindowFeature(Window.FEATURE_NO_TITLE);
		
		// this is done to make fullscreen
		this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
		
		// force activity window orientation to landscape
		MainActivity.this.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
		
		// set black background color
		this.getWindow().getDecorView().setBackgroundColor(Color.rgb(0, 0, 0));
		
		// display green colored hello world text
		TextView myTextview = new TextView(this);
		myTextview.setText("Hello World!!!");
		myTextview.setTextSize(60);
		myTextview.setTextColor(Color.GREEN);
		myTextview.setGravity(Gravity.CENTER);
		setContentView(myTextview);
		
    }
}
