package com.astromedicomp.hello_2;


import android.content.Context; // for drawing context related
import android.view.Window;  // for "Window" class
import android.view.WindowManager; // for "WindowManager" class
import android.content.pm.ActivityInfo; // for "ActivityInfo" class
import android.graphics.Color; // for "Color" class
import android.widget.TextView; // for "TextView" class
import android.view.Gravity; // for "Gravity" class

public class MyView extends TextView {

	MyView(Context context){
		super(context);
		setTextSize(60);
		setTextColor(Color.rgb(0, 255, 0));
		setText("Hello World!!!");
		setGravity(Gravity.CENTER);
	}
}
