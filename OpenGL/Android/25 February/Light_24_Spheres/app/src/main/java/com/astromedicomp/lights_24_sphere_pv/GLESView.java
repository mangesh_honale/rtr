package com.astromedicomp.lights_24_sphere_pv;


import android.content.Context; // for drawing context related
import android.opengl.GLSurfaceView; // for OpenGL Surface view and all related
import javax.microedition.khronos.opengles.GL10; // for OpenGLES 1.0 needed as param type GL10
import javax.microedition.khronos.egl.EGLConfig; // for EGLConfig needed as param type EGLConfig
import android.opengl.GLES32; // for OpenGLES 3.2
import android.view.Window;  // for "Window" class
import android.view.WindowManager; // for "WindowManager" class
import android.content.pm.ActivityInfo; // for "ActivityInfo" class
import android.graphics.Color; // for "Color" class
import android.widget.TextView; // for "TextView" class
import android.view.Gravity; // for "Gravity" class
import android.view.MotionEvent; // for "MotionEvent" class
import android.view.GestureDetector;
import android.view.GestureDetector.OnGestureListener; // for OnGestureListener
import android.view.GestureDetector.OnDoubleTapListener; // for OnDoubleTapListener

// for vbo
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.FloatBuffer;
import java.nio.ShortBuffer;
import android.opengl.Matrix; // for matrix math

// A view of OpenGLES3 graphics which also receives touch events
public class GLESView extends GLSurfaceView implements GLSurfaceView.Renderer, OnGestureListener, OnDoubleTapListener{
	private final Context context;
	private GestureDetector gestureDetector;
	
	private int vertexShaderObject;
	private int fragmentShaderObject;
	private int shaderProgramObject;
	private int numElements;
    private int numVertices;
	
	private int[] vao_sphere = new int[1];
    private int[] vbo_sphere_position = new int[1];
    private int[] vbo_sphere_normal = new int[1];
    private int[] vbo_sphere_element = new int[1];

    private float light_ambient[] = {0.0f,0.0f,0.0f,1.0f};
    private float light_diffuse[] = {1.0f,1.0f,1.0f,1.0f};
    private float light_specular[] = {1.0f,1.0f,1.0f,1.0f};
    private float light_position[] = { 0.0f, 0.0f, 1.0f, 0.0f };
    
    private float material_ambient[] = {0.0f,0.0f,0.0f,1.0f};
    private float material_diffuse[] = {1.0f,1.0f,1.0f,1.0f};
    private float material_specular[] = {1.0f,1.0f,1.0f,1.0f};
    private float material_shininess = 50.0f;
    
	//11
private float material_emerald_ambient[] = { 0.0215f, 0.1745f, 0.0215f, 1.0f };
private float material_emerald_diffuse[] = { 0.07568f, 0.61424f, 0.07568f, 1.0f };
private float material_emerald_specular[] = { 0.633f, 0.727811f, 0.633f, 1.0f };
private float material_emerald_shininess = 0.6f * 128;

//12
private float material_jade_ambient[] = { 0.135f, 0.225f, 0.1575f, 1.0f };
private float material_jade_diffuse[] = { 0.54f, 0.89f, 0.63f, 1.0f };
private float material_jade_specular[] = { 0.316228f, 0.316228f, 0.316228f, 1.0f };
private float material_jade_shininess = 0.1f * 128;

//13
private float material_obsidian_ambient[] = { 0.05375f, 0.05f, 0.06625f, 1.0f };
private float material_obsidian_diffuse[] = { 0.18275f, 0.17f, 0.22525f, 1.0f };
private float material_obsidian_specular[] = { 0.332741f, 0.328634f, 0.346435f, 1.0f };
private float material_obsidian_shininess = 0.3f * 128;

//14
private float material_pearl_ambient[] = { 0.25f, 0.20725f, 0.20725f, 1.0f };
private float material_pearl_diffuse[] = { 1.0f, 0.829f, 0.829f, 1.0f };
private float material_pearl_specular[] = { 0.296648f, 0.296648f, 0.296648f, 1.0f };
private float material_pearl_shininess = 0.088f * 128;

//15
private float material_ruby_ambient[] = { 0.1745f, 0.01175f, 0.01175f, 1.0f };
private float material_ruby_diffuse[] = { 0.61424f, 0.04136f, 0.04136f, 1.0f };
private float material_ruby_specular[] = { 0.727811f, 0.626959f, 0.626959f, 1.0f };
private float material_ruby_shininess = 0.6f * 128;

//16
private float material_turquoise_ambient[] = { 0.1f, 0.18725f, 0.1745f, 1.0f };
private float material_turquoise_diffuse[] = { 0.396f, 0.74151f, 0.69102f, 1.0f };
private float material_turquoise_specular[] = { 0.297254f, 0.30829f, 0.306678f, 1.0f };
private float material_turquoise_shininess = 0.1f * 128;

//21
private float material_brass_ambient[] = { 0.329412f, 0.223529f, 0.027451f, 1.0f };
private float material_brass_diffuse[] = { 0.780392f, 0.568627f, 0.113725f, 1.0f };
private float material_brass_specular[] = { 0.992157f, 0.941176f, 0.807843f, 1.0f };
private float material_brass_shininess = 0.21794872f * 128;

//22
private float material_bronze_ambient[] = { 0.2125f, 0.1275f, 0.054f, 1.0f };
private float material_bronze_diffuse[] = { 0.714f, 0.4284f, 0.18144f, 1.0f };
private float material_bronze_specular[] = { 0.393548f, 0.271906f, 0.166721f, 1.0f };
private float material_bronze_shininess = 0.2f * 128;

//23
private float material_chrome_ambient[] = { 0.25f, 0.25f, 0.25f, 1.0f };
private float material_chrome_diffuse[] = { 0.4f, 0.4f, 0.4f, 1.0f };
private float material_chrome_specular[] = { 0.774597f, 0.774597f, 0.774597f, 1.0f };
private float material_chrome_shininess = 0.6f * 128;

//24
private float material_copper_ambient[] = { 0.19125f, 0.0735f, 0.0225f, 1.0f };
private float material_copper_diffuse[] = { 0.7038f, 0.27048f, 0.0828f, 1.0f };
private float material_copper_specular[] = { 0.256777f, 0.137622f, 0.86014f, 1.0f };
private float material_copper_shininess = 0.1f * 128;

//25
private float material_gold_ambient[] = { 0.24725f, 0.1995f, 0.0745f, 1.0f };
private float material_gold_diffuse[] = { 0.75164f, 0.61648f, 0.22648f, 1.0f };
private float material_gold_specular[] = { 0.628281f, 0.555802f, 0.366065f, 1.0f };
private float material_gold_shininess = 0.4f * 128;

//26
private float material_silver_ambient[] = { 0.19225f, 0.19225f, 0.19225f, 1.0f };
private float material_silver_diffuse[] = { 0.50754f, 0.50754f, 0.50754f, 1.0f };
private float material_silver_specular[] = { 0.508273f, 0.508273f, 0.508273f, 1.0f };
private float material_silver_shininess = 0.4f * 128;

//31
private float material_black_ambient[] = { 0.0f, 0.0f, 0.0f, 1.0f };
private float material_black_diffuse[] = { 0.01f, 0.01f, 0.01f, 1.0f };
private float material_black_specular[] = { 0.50f, 0.50f, 0.50f, 1.0f };
private float material_black_shininess = 0.25f * 128;

//32
private float material_cyan_ambient[] = { 0.0f, 0.1f, 0.06f, 1.0f };
private float material_cyan_diffuse[] = { 0.0f, 0.50980392f, 0.50980932f, 1.0f };
private float material_cyan_specular[] = { 0.50196078f, 0.50196078f, 0.50196078f, 1.0f };
private float material_cyan_shininess = 0.25f * 128;

//33
private float material_green_ambient[] = { 0.0f, 0.0f, 0.0f, 1.0f };
private float material_green_diffuse[] = { 0.1f, 0.35f, 0.1f, 1.0f };
private float material_green_specular[] = { 0.45f, 0.45f, 0.45f, 1.0f };
private float material_green_shininess = 0.25f * 128;

//34
private float material_red_ambient[] = { 0.0f, 0.0f, 0.0f, 1.0f };
private float material_red_diffuse[] = { 0.5f, 0.0f, 0.0f, 1.0f };
private float material_red_specular[] = { 0.7f, 0.6f, 0.6f, 1.0f };
private float material_red_shininess = 0.25f * 128;

//35
private float material_white_ambient[] = { 0.0f, 0.0f, 0.0f, 1.0f };
private float material_white_diffuse[] = { 0.55f, 0.55f, 0.55f, 1.0f };
private float material_white_specular[] = { 0.70f, 0.70f, 0.70f, 1.0f };
private float material_white_shininess = 0.25f * 128;

//36
private float material_yellow_ambient[] = { 0.0f, 0.0f, 0.0f, 1.0f };
private float material_yellow_diffuse[] = { 0.5f, 0.5f, 0.0f, 1.0f };
private float material_yellow_specular[] = { 0.60f, 0.60f, 0.50f, 1.0f };
private float material_yellow_shininess = 0.25f * 128;

//41
private float material_black2_ambient[] = { 0.02f, 0.02f, 0.02f, 1.0f };
private float material_black2_diffuse[] = { 0.01f, 0.01f, 0.01f, 1.0f };
private float material_black2_specular[] = { 0.4f, 0.4f, 0.4f, 1.0f };
private float material_black2_shininess = 0.078125f * 128;

//42
private float material_cyan2_ambient[] = { 0.0f, 0.05f, 0.05f, 1.0f };
private float material_cyan2_diffuse[] = { 0.4f, 0.5f, 0.0f, 1.0f };
private float material_cyan2_specular[] = { 0.04f, 0.7f, 0.7f, 1.0f };
private float material_cyan2_shininess = 0.078125f * 128;

//43
private float material_green2_ambient[] = { 0.0f, 0.05f, 0.0f, 1.0f };
private float material_green2_diffuse[] = { 0.4f, 0.5f, 0.4f, 1.0f };
private float material_green2_specular[] = { 0.04f, 0.7f, 0.04f, 1.0f };
private float material_green2_shininess = 0.078125f * 128;

//44
private float material_red2_ambient[] = { 0.05f, 0.0f, 0.0f, 1.0f };
private float material_red2_diffuse[] = { 0.5f, 0.4f, 0.4f, 1.0f };
private float material_red2_specular[] = { 0.7f, 0.04f, 0.04f, 1.0f };
private float material_red2_shininess = 0.078125f * 128;

//45
private float material_white2_ambient[] = { 0.05f, 0.05f, 0.05f, 1.0f };
private float material_white2_diffuse[] = { 0.5f, 0.5f, 0.5f, 1.0f };
private float material_white2_specular[] = { 0.7f, 0.7f, 0.7f, 1.0f };
private float material_white2_shininess = 0.078125f * 128;

//46
private float material_yellow2_ambient[] = { 0.05f, 0.05f, 0.0f, 1.0f };
private float material_yellow2_diffuse[] = { 0.5f, 0.5f, 0.4f, 1.0f };
private float material_yellow2_specular[] = { 0.7f, 0.7f, 0.04f, 1.0f };
private float material_yellow2_shininess = 0.078125f * 128;

    private int  modelMatrixUniform, viewMatrixUniform, projectionMatrixUniform;
    private int  laUniform, ldUniform, lsUniform, lightPositionUniform;
    private int  kaUniform, kdUniform, ksUniform, materialShininessUniform;

    private int doubleTapUniform;

    private float perspectiveProjectionMatrix[]=new float[16]; // 4x4 matrix
    
    private int doubleTap; // for lights
	
	public GLESView(Context drawingContext){
		super(drawingContext);
		
		context = drawingContext;
		// accordingly set EGLContext to current supported version of OpenGL-ES
		setEGLContextClientVersion(3);
		
		// set Renderer for drawing on the GLSurfaceView
		setRenderer(this);
		
		// Render the view only when there is a change in the drawing data
		setRenderMode(GLSurfaceView.RENDERMODE_WHEN_DIRTY);
		
		gestureDetector = new GestureDetector(context, this, null, false); // this means 'handler' i.e. who is going to handle	
		gestureDetector.setOnDoubleTapListener(this); // this means 'handler' who is going to handle
	}
	// Override method of GLSurfaceView.Renderer (Init code)
	@Override
	public void onSurfaceCreated(GL10 gl, EGLConfig config){
		// OpenGL-ES version check
		String glesVersion = gl.glGetString(GL10.GL_VERSION);
		System.out.println("VDG: " + glesVersion );
		
		// Get GLSL version
		String glslVersion = gl.glGetString(GLES32.GL_SHADING_LANGUAGE_VERSION);
		System.out.println("VDG: GLSL Version= " + glslVersion);
		initialize(gl);
	}
	
	// override method of GLSurfaceView.Renderer (Change size code)
	@Override
	public void onSurfaceChanged(GL10 unused, int width, int height){
		resize(width, height);
	}
	
	@Override
	public void onDrawFrame(GL10 unused){
		
		display();
	}
	
	// Handling 'onTouchEvent' is the most important because it triggers all gestureand tap events
	@Override
	public boolean onTouchEvent(MotionEvent event){
		// code
		int eventaction = event.getAction();
		if(!gestureDetector.onTouchEvent(event))
			super.onTouchEvent(event);
		return(true);
	}
    
    // abstract method from OnDoubleTapListener so must be implemented
    @Override
    public boolean onDoubleTap(MotionEvent e){
        doubleTap++;
        if(doubleTap > 1)
            doubleTap=0;
        return(true);
    }
    
    // abstract method from OnDoubleTapListener so must be implemented
    @Override
    public boolean onDoubleTapEvent(MotionEvent e){
        // Do Not Write Any code Here Because Already Written 'onDoubleTap'
        return(true);
    }
    
    // abstract method from OnDoubleTapListener so must be implemented
    @Override
    public boolean onSingleTapConfirmed(MotionEvent e){
        
        return(true);
    }
    
    // abstract method from OnGestureListener so must be implemented
    @Override
    public boolean onDown(MotionEvent e){
        // Do Not Write Any code Here Because Already Written 'onSingleTapConfirmed'
        return(true);
    }
    
    // abstract method from OnGestureListener so must be implemented
    @Override
    public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX, float velocityY){
        return(true);
    }
    
    // abstract method from OnGestureListener so must be implemented
    @Override
    public void onLongPress(MotionEvent e){
        System.out.println("VDG: Long Press");
    }
    
    // abstract method from OnGestureListener so must be implemented
    @Override
    public boolean onScroll(MotionEvent e1, MotionEvent e2, float distanceX, float distanceY){
		System.out.println("VDG: Scroll");
        System.exit(0);
		return (true);
    }
    
    // abstract method from OnGestureListener so must be implemented
    @Override
    public void onShowPress(MotionEvent e){
    }
    
    // abstract method from OnGestureListener so must be implemented
    @Override
    public boolean onSingleTapUp(MotionEvent e){
        return(true);
    }
	
	private void initialize(GL10 gl){
		// Create vertex shader
		vertexShaderObject = GLES32.glCreateShader(GLES32.GL_VERTEX_SHADER);
		final String vertexShaderSourceObject = String.format(
         "#version 320 es"+
         "\n"+
         "in vec4 vPosition;"+
         "in vec3 vNormal;"+
         "uniform mat4 u_model_matrix;"+
         "uniform mat4 u_view_matrix;"+
         "uniform mat4 u_projection_matrix;"+
         "uniform int u_double_tap;"+
         "uniform vec3 u_La;"+
         "uniform vec3 u_Ld;"+
         "uniform vec3 u_Ls;"+
         "uniform vec4 u_light_position;"+
         "uniform vec3 u_Ka;"+
         "uniform vec3 u_Kd;"+
         "uniform vec3 u_Ks;"+
         "uniform float u_material_shininess;"+
         "out vec3 phong_ads_color;"+
         "void main(void)"+
         "{"+
         "if (u_double_tap == 1)"+
         "{"+
         "vec4 eye_coordinates=u_view_matrix * u_model_matrix * vPosition;"+
         "vec3 transformed_normals=normalize(mat3(u_view_matrix * u_model_matrix) * vNormal);"+
         "vec3 light_direction = normalize(vec3(u_light_position) - eye_coordinates.xyz);"+
         "float tn_dot_ld = max(dot(transformed_normals, light_direction),0.0);"+
         "vec3 ambient = u_La * u_Ka;"+
         "vec3 diffuse = u_Ld * u_Kd * tn_dot_ld;"+
         "vec3 reflection_vector = reflect(-light_direction, transformed_normals);"+
         "vec3 viewer_vector = normalize(-eye_coordinates.xyz);"+
         "vec3 specular = u_Ls * u_Ks * pow(max(dot(reflection_vector, viewer_vector), 0.0), u_material_shininess);"+
         "phong_ads_color=ambient + diffuse + specular;"+
         "}"+
         "else"+
         "{"+
         "phong_ads_color = vec3(1.0, 1.0, 1.0);"+
         "}"+
         "gl_Position=u_projection_matrix * u_view_matrix * u_model_matrix * vPosition;"+
         "}"
        );

// provide source code to shader
GLES32.glShaderSource(vertexShaderObject, vertexShaderSourceObject);

// Compile shader & check for errors
GLES32.glCompileShader(vertexShaderObject);
int[] iShaderCompiledStatus = new int[1];
int[] iInfoLogLength = new int[1];
String szInfoLog = null;

GLES32.glGetShaderiv(vertexShaderObject, GLES32.GL_COMPILE_STATUS, iShaderCompiledStatus, 0);
if(iShaderCompiledStatus[0] == GLES32.GL_FALSE){
	GLES32.glGetShaderiv(vertexShaderObject, GLES32.GL_INFO_LOG_LENGTH, iInfoLogLength, 0);
	if(iInfoLogLength[0] > 0){
		szInfoLog = GLES32.glGetShaderInfoLog(vertexShaderObject);
		System.out.println("VDG: Vertex shader compilation log = " + szInfoLog);
		uninitialize();
		System.exit(0);
	}
}

// Fragment shader
fragmentShaderObject = GLES32.glCreateShader(GLES32.GL_FRAGMENT_SHADER);

// fragment shader source code
final String fragmentShaderSourceCode = String.format
(
		 "#version 320 es"+
         "\n"+
         "precision highp float;"+
         "in vec3 phong_ads_color;"+
         "out vec4 FragColor;"+
         "void main(void)"+
         "{"+
         "FragColor = vec4(phong_ads_color, 1.0);"+
         "}"
);


// Provide source code to shader
GLES32.glShaderSource(fragmentShaderObject, fragmentShaderSourceCode);

// Compile shader and check for errors
GLES32.glCompileShader(fragmentShaderObject);
iShaderCompiledStatus[0] = 0; //re-initialize
iInfoLogLength[0] = 0; // re-initialize
szInfoLog = null; // re-initialize

GLES32.glGetShaderiv(fragmentShaderObject, GLES32.GL_COMPILE_STATUS, iShaderCompiledStatus, 0);
if(iShaderCompiledStatus[0] == GLES32.GL_FALSE){
	GLES32.glGetShaderiv(fragmentShaderObject, GLES32.GL_INFO_LOG_LENGTH,iInfoLogLength, 0);
	if(iInfoLogLength[0] > 0){
		szInfoLog = GLES32.glGetShaderInfoLog(fragmentShaderObject);
		System.out.println("VDG: Fragment shader compilation log = " + szInfoLog);
		uninitialize();
		System.exit(0);
	}
}

// Create shader program
shaderProgramObject = GLES32.glCreateProgram();

// attach vertex shader to shader program
GLES32.glAttachShader(shaderProgramObject, vertexShaderObject);

// attach fragment shader to shader program 
GLES32.glAttachShader(shaderProgramObject, fragmentShaderObject);

// pre-link bidning of shader program object with vertex shader attributes
GLES32.glBindAttribLocation(shaderProgramObject, GLESMacros.VDG_ATTRIBUTE_VERTEX, "vPosition");
GLES32.glBindAttribLocation(shaderProgramObject,GLESMacros.VDG_ATTRIBUTE_NORMAL,"vNormal");

// Link the two shaders together to shader program //object
GLES32.glLinkProgram(shaderProgramObject);
int[] iShaderProgramLinkStatus = new int[1];
iInfoLogLength[0] = 0;
szInfoLog = null;
GLES32.glGetProgramiv(shaderProgramObject, GLES32.GL_LINK_STATUS, iShaderProgramLinkStatus, 0);
if(iShaderProgramLinkStatus[0] == GLES32.GL_FALSE){
	GLES32.glGetProgramiv(shaderProgramObject, GLES32.GL_INFO_LOG_LENGTH, iInfoLogLength, 0);
	if(iInfoLogLength[0] > 0){
		szInfoLog = GLES32.glGetProgramInfoLog(shaderProgramObject);
		System.out.println("VDG: Shader program link log=" + szInfoLog);
		uninitialize();
		System.exit(0);
	}
}

// get MVP uniform location
modelMatrixUniform = GLES32.glGetUniformLocation(shaderProgramObject, "u_model_matrix");
viewMatrixUniform = GLES32.glGetUniformLocation(shaderProgramObject, "u_view_matrix");
projectionMatrixUniform = GLES32.glGetUniformLocation(shaderProgramObject, "u_projection_matrix");

doubleTapUniform = GLES32.glGetUniformLocation(shaderProgramObject, "u_double_tap");

laUniform = GLES32.glGetUniformLocation(shaderProgramObject, "u_La");
ldUniform = GLES32.glGetUniformLocation(shaderProgramObject, "u_Ld");
lsUniform = GLES32.glGetUniformLocation(shaderProgramObject, "u_Ls");
lightPositionUniform = GLES32.glGetUniformLocation(shaderProgramObject, "u_light_position");;

kaUniform = GLES32.glGetUniformLocation(shaderProgramObject, "u_Ka");
kdUniform = GLES32.glGetUniformLocation(shaderProgramObject, "u_Kd");
ksUniform = GLES32.glGetUniformLocation(shaderProgramObject, "u_Ks");
materialShininessUniform = GLES32.glGetUniformLocation(shaderProgramObject, "u_material_shininess");;


// *** vertices, colors, shader attributes, vbo, vao // intitalizations
Sphere sphere=new Sphere();
        float sphere_vertices[]=new float[1146];
        float sphere_normals[]=new float[1146];
        float sphere_textures[]=new float[764];
        short sphere_elements[]=new short[2280];
        sphere.getSphereVertexData(sphere_vertices, sphere_normals, sphere_textures, sphere_elements);
        numVertices = sphere.getNumberOfSphereVertices();
        numElements = sphere.getNumberOfSphereElements();

        // vao
        GLES32.glGenVertexArrays(1,vao_sphere,0);
        GLES32.glBindVertexArray(vao_sphere[0]);
        
        // position vbo
        GLES32.glGenBuffers(1,vbo_sphere_position,0);
        GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER,vbo_sphere_position[0]);
        
        ByteBuffer byteBuffer=ByteBuffer.allocateDirect(sphere_vertices.length * 4);
        byteBuffer.order(ByteOrder.nativeOrder());
        FloatBuffer verticesBuffer=byteBuffer.asFloatBuffer();
        verticesBuffer.put(sphere_vertices);
        verticesBuffer.position(0);
        
        GLES32.glBufferData(GLES32.GL_ARRAY_BUFFER,
                            sphere_vertices.length * 4,
                            verticesBuffer,
                            GLES32.GL_STATIC_DRAW);
        
        GLES32.glVertexAttribPointer(GLESMacros.VDG_ATTRIBUTE_VERTEX,
                                     3,
                                     GLES32.GL_FLOAT,
                                     false,0,0);
        
        GLES32.glEnableVertexAttribArray(GLESMacros.VDG_ATTRIBUTE_VERTEX);
        
        GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER,0);
        
        // normal vbo
        GLES32.glGenBuffers(1,vbo_sphere_normal,0);
        GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER,vbo_sphere_normal[0]);
        
        byteBuffer=ByteBuffer.allocateDirect(sphere_normals.length * 4);
        byteBuffer.order(ByteOrder.nativeOrder());
        verticesBuffer=byteBuffer.asFloatBuffer();
        verticesBuffer.put(sphere_normals);
        verticesBuffer.position(0);
        
        GLES32.glBufferData(GLES32.GL_ARRAY_BUFFER,
                            sphere_normals.length * 4,
                            verticesBuffer,
                            GLES32.GL_STATIC_DRAW);
        
        GLES32.glVertexAttribPointer(GLESMacros.VDG_ATTRIBUTE_NORMAL,
                                     3,
                                     GLES32.GL_FLOAT,
                                     false,0,0);
        
        GLES32.glEnableVertexAttribArray(GLESMacros.VDG_ATTRIBUTE_NORMAL);
        
        GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER,0);
        
        // element vbo
        GLES32.glGenBuffers(1,vbo_sphere_element,0);
        GLES32.glBindBuffer(GLES32.GL_ELEMENT_ARRAY_BUFFER,vbo_sphere_element[0]);
        
        byteBuffer=ByteBuffer.allocateDirect(sphere_elements.length * 2);
        byteBuffer.order(ByteOrder.nativeOrder());
        ShortBuffer elementsBuffer=byteBuffer.asShortBuffer();
        elementsBuffer.put(sphere_elements);
        elementsBuffer.position(0);
        
        GLES32.glBufferData(GLES32.GL_ELEMENT_ARRAY_BUFFER,
                            sphere_elements.length * 2,
                            elementsBuffer,
                            GLES32.GL_STATIC_DRAW);
        
        GLES32.glBindBuffer(GLES32.GL_ELEMENT_ARRAY_BUFFER,0);

        GLES32.glBindVertexArray(0);
// enable depth testing
GLES32.glEnable(GLES32.GL_DEPTH_TEST);

// depth test to do
GLES32.glDepthFunc(GLES32.GL_LEQUAL);

// We will always cull back faces for better //performace
GLES32.glEnable(GLES32.GL_CULL_FACE);
		//Set the background frame color
		GLES32.glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
		doubleTap = 0;
		// set projection matrix to identity matrix
		Matrix.setIdentityM(perspectiveProjectionMatrix, 0);
	}
	
	private void resize(int width, int height){
		// Adjust the viewport based on geometry changes, such as screen rotation
		GLES32.glViewport(0, 0, width, height);
		Matrix.perspectiveM(perspectiveProjectionMatrix, 0, (float)45.0, (float)width/(float)height, (float)0.1, (float)100.0); 

	}
	public void display(){
		// Draw background color
		GLES32.glClear(GLES32.GL_COLOR_BUFFER_BIT | GLES32.GL_DEPTH_BUFFER_BIT);
		// use shader program
		GLES32.glUseProgram(shaderProgramObject);
		if(doubleTap==1)
        {
            GLES32.glUniform1i(doubleTapUniform, 1);
            
            // setting light's properties
            GLES32.glUniform3fv(laUniform, 1, light_ambient, 0);
            GLES32.glUniform3fv(ldUniform, 1, light_diffuse, 0);
            GLES32.glUniform3fv(lsUniform, 1, light_specular, 0);
            GLES32.glUniform4fv(lightPositionUniform, 1, light_position, 0);
            
            // setting material's properties
            GLES32.glUniform3fv(kaUniform, 1, material_ambient, 0);
            GLES32.glUniform3fv(kdUniform, 1, material_diffuse, 0);
            GLES32.glUniform3fv(ksUniform, 1, material_specular, 0);
            GLES32.glUniform1f(materialShininessUniform, material_shininess);
        }
        else
        {
            GLES32.glUniform1i(doubleTapUniform, 0);
        }
        
        // OpenGL-ES drawing
        float modelMatrix[]=new float[16];
        float viewMatrix[]=new float[16];
        
        // set modelMatrix and viewMatrix matrices to identity matrix
        Matrix.setIdentityM(modelMatrix,0);
        Matrix.setIdentityM(viewMatrix,0);

        // apply z axis translation to go deep into the screen by -1.5,
        // so that pyramid with same fullscreen co-ordinates, but due to above translation will look small
        Matrix.translateM(modelMatrix,0,-3.5f,1.8f,-6.0f);
        GLES32.glUniformMatrix4fv(modelMatrixUniform,1,false,modelMatrix,0);
        GLES32.glUniformMatrix4fv(viewMatrixUniform,1,false,viewMatrix,0);
        GLES32.glUniformMatrix4fv(projectionMatrixUniform,1,false,perspectiveProjectionMatrix,0);
        
        // bind vao
        GLES32.glBindVertexArray(vao_sphere[0]);
		
		GLES32.glUniform3fv(kaUniform, 1, material_emerald_ambient, 0);
		GLES32.glUniform3fv(kdUniform, 1, material_emerald_diffuse, 0);
		GLES32.glUniform3fv(ksUniform, 1, material_emerald_specular, 0);
		GLES32.glUniform1f(materialShininessUniform, material_emerald_shininess);
        // 11
        // *** draw, either by glDrawTriangles() or glDrawArrays() or glDrawElements()
        GLES32.glBindBuffer(GLES32.GL_ELEMENT_ARRAY_BUFFER, vbo_sphere_element[0]);
        GLES32.glDrawElements(GLES32.GL_TRIANGLES, numElements, GLES32.GL_UNSIGNED_SHORT, 0);
        
		// 12
		Matrix.setIdentityM(modelMatrix,0);
        Matrix.setIdentityM(viewMatrix,0);
		
		GLES32.glUniform3fv(kaUniform, 1, material_jade_ambient, 0);
		GLES32.glUniform3fv(kdUniform, 1, material_jade_diffuse, 0);
		GLES32.glUniform3fv(ksUniform, 1, material_jade_specular, 0);
		GLES32.glUniform1f(materialShininessUniform, material_jade_shininess);
        // apply z axis translation to go deep into the screen by -1.5,
        // so that pyramid with same fullscreen co-ordinates, but due to above translation will look small
        Matrix.translateM(modelMatrix,0,-2.1f,1.8f,-6.0f);
        GLES32.glUniformMatrix4fv(modelMatrixUniform,1,false,modelMatrix,0);
        GLES32.glUniformMatrix4fv(viewMatrixUniform,1,false,viewMatrix,0);
        GLES32.glUniformMatrix4fv(projectionMatrixUniform,1,false,perspectiveProjectionMatrix,0);
		GLES32.glBindBuffer(GLES32.GL_ELEMENT_ARRAY_BUFFER, vbo_sphere_element[0]);
        GLES32.glDrawElements(GLES32.GL_TRIANGLES, numElements, GLES32.GL_UNSIGNED_SHORT, 0);
		
		// 13
		Matrix.setIdentityM(modelMatrix,0);
        Matrix.setIdentityM(viewMatrix,0);

		GLES32.glUniform3fv(kaUniform, 1, material_obsidian_ambient, 0);
		GLES32.glUniform3fv(kdUniform, 1, material_obsidian_diffuse, 0);
		GLES32.glUniform3fv(ksUniform, 1, material_obsidian_specular, 0);
		GLES32.glUniform1f(materialShininessUniform, material_obsidian_shininess);
        // apply z axis translation to go deep into the screen by -1.5,
        // so that pyramid with same fullscreen co-ordinates, but due to above translation will look small
        Matrix.translateM(modelMatrix,0,-0.7f,1.8f,-6.0f);
        GLES32.glUniformMatrix4fv(modelMatrixUniform,1,false,modelMatrix,0);
        GLES32.glUniformMatrix4fv(viewMatrixUniform,1,false,viewMatrix,0);
        GLES32.glUniformMatrix4fv(projectionMatrixUniform,1,false,perspectiveProjectionMatrix,0);
		GLES32.glBindBuffer(GLES32.GL_ELEMENT_ARRAY_BUFFER, vbo_sphere_element[0]);
        GLES32.glDrawElements(GLES32.GL_TRIANGLES, numElements, GLES32.GL_UNSIGNED_SHORT, 0);
		
		// 14
		Matrix.setIdentityM(modelMatrix,0);
        Matrix.setIdentityM(viewMatrix,0);

		GLES32.glUniform3fv(kaUniform, 1, material_pearl_ambient, 0);
		GLES32.glUniform3fv(kdUniform, 1, material_pearl_diffuse, 0);
		GLES32.glUniform3fv(ksUniform, 1, material_pearl_specular, 0);
		GLES32.glUniform1f(materialShininessUniform, material_pearl_shininess);
        // apply z axis translation to go deep into the screen by -1.5,
        // so that pyramid with same fullscreen co-ordinates, but due to above translation will look small
        Matrix.translateM(modelMatrix,0,0.7f,1.8f,-6.0f);
        GLES32.glUniformMatrix4fv(modelMatrixUniform,1,false,modelMatrix,0);
        GLES32.glUniformMatrix4fv(viewMatrixUniform,1,false,viewMatrix,0);
        GLES32.glUniformMatrix4fv(projectionMatrixUniform,1,false,perspectiveProjectionMatrix,0);
		GLES32.glBindBuffer(GLES32.GL_ELEMENT_ARRAY_BUFFER, vbo_sphere_element[0]);
        GLES32.glDrawElements(GLES32.GL_TRIANGLES, numElements, GLES32.GL_UNSIGNED_SHORT, 0);
		
		// 15
		Matrix.setIdentityM(modelMatrix,0);
        Matrix.setIdentityM(viewMatrix,0);

		GLES32.glUniform3fv(kaUniform, 1, material_ruby_ambient, 0);
		GLES32.glUniform3fv(kdUniform, 1, material_ruby_diffuse, 0);
		GLES32.glUniform3fv(ksUniform, 1, material_ruby_specular, 0);
		GLES32.glUniform1f(materialShininessUniform, material_ruby_shininess);
        // apply z axis translation to go deep into the screen by -1.5,
        // so that pyramid with same fullscreen co-ordinates, but due to above translation will look small
        Matrix.translateM(modelMatrix,0,2.1f,1.8f,-6.0f);
        GLES32.glUniformMatrix4fv(modelMatrixUniform,1,false,modelMatrix,0);
        GLES32.glUniformMatrix4fv(viewMatrixUniform,1,false,viewMatrix,0);
        GLES32.glUniformMatrix4fv(projectionMatrixUniform,1,false,perspectiveProjectionMatrix,0);
		GLES32.glBindBuffer(GLES32.GL_ELEMENT_ARRAY_BUFFER, vbo_sphere_element[0]);
        GLES32.glDrawElements(GLES32.GL_TRIANGLES, numElements, GLES32.GL_UNSIGNED_SHORT, 0);
		
		// 16
		Matrix.setIdentityM(modelMatrix,0);
        Matrix.setIdentityM(viewMatrix,0);

		GLES32.glUniform3fv(kaUniform, 1, material_turquoise_ambient, 0);
		GLES32.glUniform3fv(kdUniform, 1, material_turquoise_diffuse, 0);
		GLES32.glUniform3fv(ksUniform, 1, material_turquoise_specular, 0);
		GLES32.glUniform1f(materialShininessUniform, material_turquoise_shininess);
        // apply z axis translation to go deep into the screen by -1.5,
        // so that pyramid with same fullscreen co-ordinates, but due to above translation will look small
        Matrix.translateM(modelMatrix,0, 3.5f,1.8f,-6.0f);
        GLES32.glUniformMatrix4fv(modelMatrixUniform,1,false,modelMatrix,0);
        GLES32.glUniformMatrix4fv(viewMatrixUniform,1,false,viewMatrix,0);
        GLES32.glUniformMatrix4fv(projectionMatrixUniform,1,false,perspectiveProjectionMatrix,0);
		GLES32.glBindBuffer(GLES32.GL_ELEMENT_ARRAY_BUFFER, vbo_sphere_element[0]);
        GLES32.glDrawElements(GLES32.GL_TRIANGLES, numElements, GLES32.GL_UNSIGNED_SHORT, 0);
		
		// 21
		Matrix.setIdentityM(modelMatrix,0);
        Matrix.setIdentityM(viewMatrix,0);

		GLES32.glUniform3fv(kaUniform, 1, material_brass_ambient, 0);
		GLES32.glUniform3fv(kdUniform, 1, material_brass_diffuse, 0);
		GLES32.glUniform3fv(ksUniform, 1, material_brass_specular, 0);
		GLES32.glUniform1f(materialShininessUniform, material_brass_shininess);
        // apply z axis translation to go deep into the screen by -1.5,
        // so that pyramid with same fullscreen co-ordinates, but due to above translation will look small
        Matrix.translateM(modelMatrix,0,-3.5f,0.6f,-6.0f);
        GLES32.glUniformMatrix4fv(modelMatrixUniform,1,false,modelMatrix,0);
        GLES32.glUniformMatrix4fv(viewMatrixUniform,1,false,viewMatrix,0);
        GLES32.glUniformMatrix4fv(projectionMatrixUniform,1,false,perspectiveProjectionMatrix,0);
		GLES32.glBindBuffer(GLES32.GL_ELEMENT_ARRAY_BUFFER, vbo_sphere_element[0]);
        GLES32.glDrawElements(GLES32.GL_TRIANGLES, numElements, GLES32.GL_UNSIGNED_SHORT, 0);
		
		// 22
		Matrix.setIdentityM(modelMatrix,0);
        Matrix.setIdentityM(viewMatrix,0);

		GLES32.glUniform3fv(kaUniform, 1, material_bronze_ambient, 0);
		GLES32.glUniform3fv(kdUniform, 1, material_bronze_diffuse, 0);
		GLES32.glUniform3fv(ksUniform, 1, material_bronze_specular, 0);
		GLES32.glUniform1f(materialShininessUniform, material_bronze_shininess);
        // apply z axis translation to go deep into the screen by -1.5,
        // so that pyramid with same fullscreen co-ordinates, but due to above translation will look small
        Matrix.translateM(modelMatrix,0,-2.1f,0.6f,-6.0f);
        GLES32.glUniformMatrix4fv(modelMatrixUniform,1,false,modelMatrix,0);
        GLES32.glUniformMatrix4fv(viewMatrixUniform,1,false,viewMatrix,0);
        GLES32.glUniformMatrix4fv(projectionMatrixUniform,1,false,perspectiveProjectionMatrix,0);
		GLES32.glBindBuffer(GLES32.GL_ELEMENT_ARRAY_BUFFER, vbo_sphere_element[0]);
        GLES32.glDrawElements(GLES32.GL_TRIANGLES, numElements, GLES32.GL_UNSIGNED_SHORT, 0);
		
		// 23
		Matrix.setIdentityM(modelMatrix,0);
        Matrix.setIdentityM(viewMatrix,0);

		GLES32.glUniform3fv(kaUniform, 1, material_chrome_ambient, 0);
		GLES32.glUniform3fv(kdUniform, 1, material_chrome_diffuse, 0);
		GLES32.glUniform3fv(ksUniform, 1, material_chrome_specular, 0);
		GLES32.glUniform1f(materialShininessUniform, material_chrome_shininess);
        // apply z axis translation to go deep into the screen by -1.5,
        // so that pyramid with same fullscreen co-ordinates, but due to above translation will look small
        Matrix.translateM(modelMatrix,0,-0.7f,0.6f,-6.0f);
        GLES32.glUniformMatrix4fv(modelMatrixUniform,1,false,modelMatrix,0);
        GLES32.glUniformMatrix4fv(viewMatrixUniform,1,false,viewMatrix,0);
        GLES32.glUniformMatrix4fv(projectionMatrixUniform,1,false,perspectiveProjectionMatrix,0);
		GLES32.glBindBuffer(GLES32.GL_ELEMENT_ARRAY_BUFFER, vbo_sphere_element[0]);
        GLES32.glDrawElements(GLES32.GL_TRIANGLES, numElements, GLES32.GL_UNSIGNED_SHORT, 0);
		
		// 24
		Matrix.setIdentityM(modelMatrix,0);
        Matrix.setIdentityM(viewMatrix,0);

		GLES32.glUniform3fv(kaUniform, 1, material_copper_ambient, 0);
		GLES32.glUniform3fv(kdUniform, 1, material_copper_diffuse, 0);
		GLES32.glUniform3fv(ksUniform, 1, material_copper_specular, 0);
		GLES32.glUniform1f(materialShininessUniform, material_copper_shininess);
        // apply z axis translation to go deep into the screen by -1.5,
        // so that pyramid with same fullscreen co-ordinates, but due to above translation will look small
        Matrix.translateM(modelMatrix,0,0.7f,0.6f,-6.0f);
        GLES32.glUniformMatrix4fv(modelMatrixUniform,1,false,modelMatrix,0);
        GLES32.glUniformMatrix4fv(viewMatrixUniform,1,false,viewMatrix,0);
        GLES32.glUniformMatrix4fv(projectionMatrixUniform,1,false,perspectiveProjectionMatrix,0);
		GLES32.glBindBuffer(GLES32.GL_ELEMENT_ARRAY_BUFFER, vbo_sphere_element[0]);
        GLES32.glDrawElements(GLES32.GL_TRIANGLES, numElements, GLES32.GL_UNSIGNED_SHORT, 0);
		
		// 25
		Matrix.setIdentityM(modelMatrix,0);
        Matrix.setIdentityM(viewMatrix,0);

		GLES32.glUniform3fv(kaUniform, 1, material_gold_ambient, 0);
		GLES32.glUniform3fv(kdUniform, 1, material_gold_diffuse, 0);
		GLES32.glUniform3fv(ksUniform, 1, material_gold_specular, 0);
		GLES32.glUniform1f(materialShininessUniform, material_gold_shininess);
        // apply z axis translation to go deep into the screen by -1.5,
        // so that pyramid with same fullscreen co-ordinates, but due to above translation will look small
        Matrix.translateM(modelMatrix,0,2.1f,0.6f,-6.0f);
        GLES32.glUniformMatrix4fv(modelMatrixUniform,1,false,modelMatrix,0);
        GLES32.glUniformMatrix4fv(viewMatrixUniform,1,false,viewMatrix,0);
        GLES32.glUniformMatrix4fv(projectionMatrixUniform,1,false,perspectiveProjectionMatrix,0);
		GLES32.glBindBuffer(GLES32.GL_ELEMENT_ARRAY_BUFFER, vbo_sphere_element[0]);
        GLES32.glDrawElements(GLES32.GL_TRIANGLES, numElements, GLES32.GL_UNSIGNED_SHORT, 0);
		
		// 26
		Matrix.setIdentityM(modelMatrix,0);
        Matrix.setIdentityM(viewMatrix,0);

		GLES32.glUniform3fv(kaUniform, 1, material_silver_ambient, 0);
		GLES32.glUniform3fv(kdUniform, 1, material_silver_diffuse, 0);
		GLES32.glUniform3fv(ksUniform, 1, material_silver_specular, 0);
		GLES32.glUniform1f(materialShininessUniform, material_silver_shininess);
        // apply z axis translation to go deep into the screen by -1.5,
        // so that pyramid with same fullscreen co-ordinates, but due to above translation will look small
        Matrix.translateM(modelMatrix,0,3.5f,0.6f,-6.0f);
        GLES32.glUniformMatrix4fv(modelMatrixUniform,1,false,modelMatrix,0);
        GLES32.glUniformMatrix4fv(viewMatrixUniform,1,false,viewMatrix,0);
        GLES32.glUniformMatrix4fv(projectionMatrixUniform,1,false,perspectiveProjectionMatrix,0);
		GLES32.glBindBuffer(GLES32.GL_ELEMENT_ARRAY_BUFFER, vbo_sphere_element[0]);
        GLES32.glDrawElements(GLES32.GL_TRIANGLES, numElements, GLES32.GL_UNSIGNED_SHORT, 0);
		// 31
		Matrix.setIdentityM(modelMatrix,0);
        Matrix.setIdentityM(viewMatrix,0);

		GLES32.glUniform3fv(kaUniform, 1, material_black_ambient, 0);
		GLES32.glUniform3fv(kdUniform, 1, material_black_diffuse, 0);
		GLES32.glUniform3fv(ksUniform, 1, material_black_specular, 0);
		GLES32.glUniform1f(materialShininessUniform, material_black_shininess);
        // apply z axis translation to go deep into the screen by -1.5,
        // so that pyramid with same fullscreen co-ordinates, but due to above translation will look small
        Matrix.translateM(modelMatrix,0,-3.5f,-0.6f,-6.0f);
        GLES32.glUniformMatrix4fv(modelMatrixUniform,1,false,modelMatrix,0);
        GLES32.glUniformMatrix4fv(viewMatrixUniform,1,false,viewMatrix,0);
        GLES32.glUniformMatrix4fv(projectionMatrixUniform,1,false,perspectiveProjectionMatrix,0);
		GLES32.glBindBuffer(GLES32.GL_ELEMENT_ARRAY_BUFFER, vbo_sphere_element[0]);
        GLES32.glDrawElements(GLES32.GL_TRIANGLES, numElements, GLES32.GL_UNSIGNED_SHORT, 0);
		
		// 32
		Matrix.setIdentityM(modelMatrix,0);
        Matrix.setIdentityM(viewMatrix,0);

		GLES32.glUniform3fv(kaUniform, 1, material_cyan_ambient, 0);
		GLES32.glUniform3fv(kdUniform, 1, material_cyan_diffuse, 0);
		GLES32.glUniform3fv(ksUniform, 1, material_cyan_specular, 0);
		GLES32.glUniform1f(materialShininessUniform, material_cyan_shininess);
        // apply z axis translation to go deep into the screen by -1.5,
        // so that pyramid with same fullscreen co-ordinates, but due to above translation will look small
        Matrix.translateM(modelMatrix,0,-2.1f,-0.6f,-6.0f);
        GLES32.glUniformMatrix4fv(modelMatrixUniform,1,false,modelMatrix,0);
        GLES32.glUniformMatrix4fv(viewMatrixUniform,1,false,viewMatrix,0);
        GLES32.glUniformMatrix4fv(projectionMatrixUniform,1,false,perspectiveProjectionMatrix,0);
		GLES32.glBindBuffer(GLES32.GL_ELEMENT_ARRAY_BUFFER, vbo_sphere_element[0]);
        GLES32.glDrawElements(GLES32.GL_TRIANGLES, numElements, GLES32.GL_UNSIGNED_SHORT, 0);
		
		// 33
		Matrix.setIdentityM(modelMatrix,0);
        Matrix.setIdentityM(viewMatrix,0);

		GLES32.glUniform3fv(kaUniform, 1, material_green_ambient, 0);
		GLES32.glUniform3fv(kdUniform, 1, material_green_diffuse, 0);
		GLES32.glUniform3fv(ksUniform, 1, material_green_specular, 0);
		GLES32.glUniform1f(materialShininessUniform, material_green_shininess);
        // apply z axis translation to go deep into the screen by -1.5,
        // so that pyramid with same fullscreen co-ordinates, but due to above translation will look small
        Matrix.translateM(modelMatrix,0,-0.7f,-0.6f,-6.0f);
        GLES32.glUniformMatrix4fv(modelMatrixUniform,1,false,modelMatrix,0);
        GLES32.glUniformMatrix4fv(viewMatrixUniform,1,false,viewMatrix,0);
        GLES32.glUniformMatrix4fv(projectionMatrixUniform,1,false,perspectiveProjectionMatrix,0);
		GLES32.glBindBuffer(GLES32.GL_ELEMENT_ARRAY_BUFFER, vbo_sphere_element[0]);
        GLES32.glDrawElements(GLES32.GL_TRIANGLES, numElements, GLES32.GL_UNSIGNED_SHORT, 0);
		
		// 34
		Matrix.setIdentityM(modelMatrix,0);
        Matrix.setIdentityM(viewMatrix,0);

		GLES32.glUniform3fv(kaUniform, 1, material_red_ambient, 0);
		GLES32.glUniform3fv(kdUniform, 1, material_red_diffuse, 0);
		GLES32.glUniform3fv(ksUniform, 1, material_red_specular, 0);
		GLES32.glUniform1f(materialShininessUniform, material_red_shininess);
        // apply z axis translation to go deep into the screen by -1.5,
        // so that pyramid with same fullscreen co-ordinates, but due to above translation will look small
        Matrix.translateM(modelMatrix,0,0.7f,-0.6f,-6.0f);
        GLES32.glUniformMatrix4fv(modelMatrixUniform,1,false,modelMatrix,0);
        GLES32.glUniformMatrix4fv(viewMatrixUniform,1,false,viewMatrix,0);
        GLES32.glUniformMatrix4fv(projectionMatrixUniform,1,false,perspectiveProjectionMatrix,0);
		GLES32.glBindBuffer(GLES32.GL_ELEMENT_ARRAY_BUFFER, vbo_sphere_element[0]);
        GLES32.glDrawElements(GLES32.GL_TRIANGLES, numElements, GLES32.GL_UNSIGNED_SHORT, 0);
		
		// 35
		Matrix.setIdentityM(modelMatrix,0);
        Matrix.setIdentityM(viewMatrix,0);

		GLES32.glUniform3fv(kaUniform, 1, material_white_ambient, 0);
		GLES32.glUniform3fv(kdUniform, 1, material_white_diffuse, 0);
		GLES32.glUniform3fv(ksUniform, 1, material_white_specular, 0);
		GLES32.glUniform1f(materialShininessUniform, material_white_shininess);
        // apply z axis translation to go deep into the screen by -1.5,
        // so that pyramid with same fullscreen co-ordinates, but due to above translation will look small
        Matrix.translateM(modelMatrix,0,2.1f,-0.6f,-6.0f);
        GLES32.glUniformMatrix4fv(modelMatrixUniform,1,false,modelMatrix,0);
        GLES32.glUniformMatrix4fv(viewMatrixUniform,1,false,viewMatrix,0);
        GLES32.glUniformMatrix4fv(projectionMatrixUniform,1,false,perspectiveProjectionMatrix,0);
		GLES32.glBindBuffer(GLES32.GL_ELEMENT_ARRAY_BUFFER, vbo_sphere_element[0]);
        GLES32.glDrawElements(GLES32.GL_TRIANGLES, numElements, GLES32.GL_UNSIGNED_SHORT, 0);
		
		// 36
		Matrix.setIdentityM(modelMatrix,0);
        Matrix.setIdentityM(viewMatrix,0);

		GLES32.glUniform3fv(kaUniform, 1, material_yellow_ambient, 0);
		GLES32.glUniform3fv(kdUniform, 1, material_yellow_diffuse, 0);
		GLES32.glUniform3fv(ksUniform, 1, material_yellow_specular, 0);
		GLES32.glUniform1f(materialShininessUniform, material_yellow_shininess);
        // apply z axis translation to go deep into the screen by -1.5,
        // so that pyramid with same fullscreen co-ordinates, but due to above translation will look small
        Matrix.translateM(modelMatrix,0,3.5f,-0.6f,-6.0f);
        GLES32.glUniformMatrix4fv(modelMatrixUniform,1,false,modelMatrix,0);
        GLES32.glUniformMatrix4fv(viewMatrixUniform,1,false,viewMatrix,0);
        GLES32.glUniformMatrix4fv(projectionMatrixUniform,1,false,perspectiveProjectionMatrix,0);
		GLES32.glBindBuffer(GLES32.GL_ELEMENT_ARRAY_BUFFER, vbo_sphere_element[0]);
        GLES32.glDrawElements(GLES32.GL_TRIANGLES, numElements, GLES32.GL_UNSIGNED_SHORT, 0);
		
		
		// 41
		Matrix.setIdentityM(modelMatrix,0);
        Matrix.setIdentityM(viewMatrix,0);

		GLES32.glUniform3fv(kaUniform, 1, material_black2_ambient, 0);
		GLES32.glUniform3fv(kdUniform, 1, material_black2_diffuse, 0);
		GLES32.glUniform3fv(ksUniform, 1, material_black2_specular, 0);
		GLES32.glUniform1f(materialShininessUniform, material_black2_shininess);
        // apply z axis translation to go deep into the screen by -1.5,
        // so that pyramid with same fullscreen co-ordinates, but due to above translation will look small
        Matrix.translateM(modelMatrix,0,-3.5f,-1.8f,-6.0f);
        GLES32.glUniformMatrix4fv(modelMatrixUniform,1,false,modelMatrix,0);
        GLES32.glUniformMatrix4fv(viewMatrixUniform,1,false,viewMatrix,0);
        GLES32.glUniformMatrix4fv(projectionMatrixUniform,1,false,perspectiveProjectionMatrix,0);
		GLES32.glBindBuffer(GLES32.GL_ELEMENT_ARRAY_BUFFER, vbo_sphere_element[0]);
        GLES32.glDrawElements(GLES32.GL_TRIANGLES, numElements, GLES32.GL_UNSIGNED_SHORT, 0);
		
		// 42
		Matrix.setIdentityM(modelMatrix,0);
        Matrix.setIdentityM(viewMatrix,0);

        // apply z axis translation to go deep into the screen by -1.5,
		GLES32.glUniform3fv(kaUniform, 1, material_cyan2_ambient, 0);
		GLES32.glUniform3fv(kdUniform, 1, material_cyan2_diffuse, 0);
		GLES32.glUniform3fv(ksUniform, 1, material_cyan2_specular, 0);
		GLES32.glUniform1f(materialShininessUniform, material_cyan2_shininess);
        // so that pyramid with same fullscreen co-ordinates, but due to above translation will look small
        Matrix.translateM(modelMatrix,0,-2.1f,-1.8f,-6.0f);
        GLES32.glUniformMatrix4fv(modelMatrixUniform,1,false,modelMatrix,0);
        GLES32.glUniformMatrix4fv(viewMatrixUniform,1,false,viewMatrix,0);
        GLES32.glUniformMatrix4fv(projectionMatrixUniform,1,false,perspectiveProjectionMatrix,0);
		GLES32.glBindBuffer(GLES32.GL_ELEMENT_ARRAY_BUFFER, vbo_sphere_element[0]);
        GLES32.glDrawElements(GLES32.GL_TRIANGLES, numElements, GLES32.GL_UNSIGNED_SHORT, 0);
		
		// 43
		Matrix.setIdentityM(modelMatrix,0);
        Matrix.setIdentityM(viewMatrix,0);

		GLES32.glUniform3fv(kaUniform, 1, material_green2_ambient, 0);
		GLES32.glUniform3fv(kdUniform, 1, material_green2_diffuse, 0);
		GLES32.glUniform3fv(ksUniform, 1, material_green2_specular, 0);
		GLES32.glUniform1f(materialShininessUniform, material_green2_shininess);
        // apply z axis translation to go deep into the screen by -1.5,
        // so that pyramid with same fullscreen co-ordinates, but due to above translation will look small
        Matrix.translateM(modelMatrix,0,-0.7f,-1.8f,-6.0f);
        GLES32.glUniformMatrix4fv(modelMatrixUniform,1,false,modelMatrix,0);
        GLES32.glUniformMatrix4fv(viewMatrixUniform,1,false,viewMatrix,0);
        GLES32.glUniformMatrix4fv(projectionMatrixUniform,1,false,perspectiveProjectionMatrix,0);
		GLES32.glBindBuffer(GLES32.GL_ELEMENT_ARRAY_BUFFER, vbo_sphere_element[0]);
        GLES32.glDrawElements(GLES32.GL_TRIANGLES, numElements, GLES32.GL_UNSIGNED_SHORT, 0);
        
		// 44
		Matrix.setIdentityM(modelMatrix,0);
        Matrix.setIdentityM(viewMatrix,0);

		GLES32.glUniform3fv(kaUniform, 1, material_red2_ambient, 0);
		GLES32.glUniform3fv(kdUniform, 1, material_red2_diffuse, 0);
		GLES32.glUniform3fv(ksUniform, 1, material_red2_specular, 0);
		GLES32.glUniform1f(materialShininessUniform, material_red2_shininess);
        // apply z axis translation to go deep into the screen by -1.5,
        // so that pyramid with same fullscreen co-ordinates, but due to above translation will look small
        Matrix.translateM(modelMatrix,0,0.7f,-1.8f,-6.0f);
        GLES32.glUniformMatrix4fv(modelMatrixUniform,1,false,modelMatrix,0);
        GLES32.glUniformMatrix4fv(viewMatrixUniform,1,false,viewMatrix,0);
        GLES32.glUniformMatrix4fv(projectionMatrixUniform,1,false,perspectiveProjectionMatrix,0);
		GLES32.glBindBuffer(GLES32.GL_ELEMENT_ARRAY_BUFFER, vbo_sphere_element[0]);
        GLES32.glDrawElements(GLES32.GL_TRIANGLES, numElements, GLES32.GL_UNSIGNED_SHORT, 0);
		
		// 45
		Matrix.setIdentityM(modelMatrix,0);
        Matrix.setIdentityM(viewMatrix,0);

		GLES32.glUniform3fv(kaUniform, 1, material_white2_ambient, 0);
		GLES32.glUniform3fv(kdUniform, 1, material_white2_diffuse, 0);
		GLES32.glUniform3fv(ksUniform, 1, material_white2_specular, 0);
		GLES32.glUniform1f(materialShininessUniform, material_white2_shininess);
        // apply z axis translation to go deep into the screen by -1.5,
        // so that pyramid with same fullscreen co-ordinates, but due to above translation will look small
        Matrix.translateM(modelMatrix,0,2.1f,-1.8f,-6.0f);
        GLES32.glUniformMatrix4fv(modelMatrixUniform,1,false,modelMatrix,0);
        GLES32.glUniformMatrix4fv(viewMatrixUniform,1,false,viewMatrix,0);
        GLES32.glUniformMatrix4fv(projectionMatrixUniform,1,false,perspectiveProjectionMatrix,0);
		GLES32.glBindBuffer(GLES32.GL_ELEMENT_ARRAY_BUFFER, vbo_sphere_element[0]);
        GLES32.glDrawElements(GLES32.GL_TRIANGLES, numElements, GLES32.GL_UNSIGNED_SHORT, 0);
		
		// 46
		Matrix.setIdentityM(modelMatrix,0);
        Matrix.setIdentityM(viewMatrix,0);

		GLES32.glUniform3fv(kaUniform, 1, material_yellow2_ambient, 0);
		GLES32.glUniform3fv(kdUniform, 1, material_yellow2_diffuse, 0);
		GLES32.glUniform3fv(ksUniform, 1, material_yellow2_specular, 0);
		GLES32.glUniform1f(materialShininessUniform, material_yellow2_shininess);
        // apply z axis translation to go deep into the screen by -1.5,
        // so that pyramid with same fullscreen co-ordinates, but due to above translation will look small
        Matrix.translateM(modelMatrix,0,3.5f,-1.8f,-6.0f);
        GLES32.glUniformMatrix4fv(modelMatrixUniform,1,false,modelMatrix,0);
        GLES32.glUniformMatrix4fv(viewMatrixUniform,1,false,viewMatrix,0);
        GLES32.glUniformMatrix4fv(projectionMatrixUniform,1,false,perspectiveProjectionMatrix,0);
		GLES32.glBindBuffer(GLES32.GL_ELEMENT_ARRAY_BUFFER, vbo_sphere_element[0]);
        GLES32.glDrawElements(GLES32.GL_TRIANGLES, numElements, GLES32.GL_UNSIGNED_SHORT, 0);
		// unbind vao
		GLES32.glBindVertexArray(0);
		
		// un-use shader program
		GLES32.glUseProgram(0);
		
		//render/flush
		requestRender();
	
	}


void uninitialize(){
	// destroy vao

	
	if(vao_sphere[0] != 0)
        {
            GLES32.glDeleteVertexArrays(1, vao_sphere, 0);
            vao_sphere[0]=0;
        }
        
        // destroy position vbo
        if(vbo_sphere_position[0] != 0)
        {
            GLES32.glDeleteBuffers(1, vbo_sphere_position, 0);
            vbo_sphere_position[0]=0;
        }
        
        // destroy normal vbo
        if(vbo_sphere_normal[0] != 0)
        {
            GLES32.glDeleteBuffers(1, vbo_sphere_normal, 0);
            vbo_sphere_normal[0]=0;
        }
        
        // destroy element vbo
        if(vbo_sphere_element[0] != 0)
        {
            GLES32.glDeleteBuffers(1, vbo_sphere_element, 0);
            vbo_sphere_element[0]=0;
        }

	
	if(shaderProgramObject != 0){
		if(vertexShaderObject != 0){
			// detach vertex shader from shader program 			//object
			GLES32.glDetachShader(shaderProgramObject, vertexShaderObject);
			// delete vertex shader object
			GLES32.glDeleteShader(vertexShaderObject);
			vertexShaderObject = 0;
		}
		
		if(fragmentShaderObject != 0){
			// detach fragment  shader from shader program 				//object
                GLES32.glDetachShader(shaderProgramObject, fragmentShaderObject);
                // delete fragment shader object
                GLES32.glDeleteShader(fragmentShaderObject);
                fragmentShaderObject = 0;

		}
	}
		// delete shader program object
        	if(shaderProgramObject != 0)
        	{
           	GLES32.glDeleteProgram(shaderProgramObject);
           	shaderProgramObject = 0;
        	}

}
}