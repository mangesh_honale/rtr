package com.astromedicomp.lights_sphere_pv_pf;


import android.content.Context; // for drawing context related
import android.opengl.GLSurfaceView; // for OpenGL Surface view and all related
import javax.microedition.khronos.opengles.GL10; // for OpenGLES 1.0 needed as param type GL10
import javax.microedition.khronos.egl.EGLConfig; // for EGLConfig needed as param type EGLConfig
import android.opengl.GLES32; // for OpenGLES 3.2
import android.view.Window;  // for "Window" class
import android.view.WindowManager; // for "WindowManager" class
import android.content.pm.ActivityInfo; // for "ActivityInfo" class
import android.graphics.Color; // for "Color" class
import android.widget.TextView; // for "TextView" class
import android.view.Gravity; // for "Gravity" class
import android.view.MotionEvent; // for "MotionEvent" class
import android.view.GestureDetector;
import android.view.GestureDetector.OnGestureListener; // for OnGestureListener
import android.view.GestureDetector.OnDoubleTapListener; // for OnDoubleTapListener

// for vbo
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.FloatBuffer;
import java.nio.ShortBuffer;
import android.opengl.Matrix; // for matrix math

// A view of OpenGLES3 graphics which also receives touch events
public class GLESView extends GLSurfaceView implements GLSurfaceView.Renderer, OnGestureListener, OnDoubleTapListener{
	private final Context context;
	private GestureDetector gestureDetector;
	
	private int vertexShaderObject_perVer;
	private int fragmentShaderObject_perVer;
	private int shaderProgramObject_perVer;
	private int vertexShaderObject_perFrag;
	private int fragmentShaderObject_perFrag;
	private int shaderProgramObject_perFrag;
	private int numElements;
    private int numVertices;
	
	private int[] vao_sphere = new int[1];
    private int[] vbo_sphere_position = new int[1];
    private int[] vbo_sphere_normal = new int[1];
    private int[] vbo_sphere_element = new int[1];

    private float light_ambient[] = {0.0f,0.0f,0.0f,1.0f};
    private float light_diffuse[] = {1.0f,1.0f,1.0f,1.0f};
    private float light_specular[] = {1.0f,1.0f,1.0f,1.0f};
    private float light_position[] = { 100.0f,100.0f,100.0f,1.0f };
    
    private float material_ambient[] = {0.0f,0.0f,0.0f,1.0f};
    private float material_diffuse[] = {1.0f,1.0f,1.0f,1.0f};
    private float material_specular[] = {1.0f,1.0f,1.0f,1.0f};
    private float material_shininess = 50.0f;
    
    private int  modelMatrixUniform_perVer, viewMatrixUniform_perVer, projectionMatrixUniform_perVer;
    private int  laUniform_perVer, ldUniform_perVer, lsUniform_perVer, lightPositionUniform_perVer;
    private int  kaUniform_perVer, kdUniform_perVer, ksUniform_perVer, materialShininessUniform_perVer;

	private int  modelMatrixUniform_perFrag, viewMatrixUniform_perFrag, projectionMatrixUniform_perFrag;
    private int  laUniform_perFrag, ldUniform_perFrag, lsUniform_perFrag, lightPositionUniform_perFrag;
    private int  kaUniform_perFrag, kdUniform_perFrag, ksUniform_perFrag, materialShininessUniform_perFrag;

    private int doubleTapUniform_perVer, doubleTapUniform_perFrag;

    private float perspectiveProjectionMatrix[]=new float[16]; // 4x4 matrix
    
    private int doubleTap; // for lights
	private int singleTap = 1;
	public GLESView(Context drawingContext){
		super(drawingContext);
		
		context = drawingContext;
		// accordingly set EGLContext to current supported version of OpenGL-ES
		setEGLContextClientVersion(3);
		
		// set Renderer for drawing on the GLSurfaceView
		setRenderer(this);
		
		// Render the view only when there is a change in the drawing data
		setRenderMode(GLSurfaceView.RENDERMODE_WHEN_DIRTY);
		
		gestureDetector = new GestureDetector(context, this, null, false); // this means 'handler' i.e. who is going to handle	
		gestureDetector.setOnDoubleTapListener(this); // this means 'handler' who is going to handle
	}
	// Override method of GLSurfaceView.Renderer (Init code)
	@Override
	public void onSurfaceCreated(GL10 gl, EGLConfig config){
		// OpenGL-ES version check
		String glesVersion = gl.glGetString(GL10.GL_VERSION);
		System.out.println("VDG: " + glesVersion );
		
		// Get GLSL version
		String glslVersion = gl.glGetString(GLES32.GL_SHADING_LANGUAGE_VERSION);
		System.out.println("VDG: GLSL Version= " + glslVersion);
		initialize(gl);
	}
	
	// override method of GLSurfaceView.Renderer (Change size code)
	@Override
	public void onSurfaceChanged(GL10 unused, int width, int height){
		resize(width, height);
	}
	
	@Override
	public void onDrawFrame(GL10 unused){
		
		display();
	}
	
	// Handling 'onTouchEvent' is the most important because it triggers all gestureand tap events
	@Override
	public boolean onTouchEvent(MotionEvent event){
		// code
		int eventaction = event.getAction();
		if(!gestureDetector.onTouchEvent(event))
			super.onTouchEvent(event);
		return(true);
	}
    
    // abstract method from OnDoubleTapListener so must be implemented
    @Override
    public boolean onDoubleTap(MotionEvent e){
        doubleTap++;
        if(doubleTap > 1)
            doubleTap=0;
        return(true);
    }
    
    // abstract method from OnDoubleTapListener so must be implemented
    @Override
    public boolean onDoubleTapEvent(MotionEvent e){
        // Do Not Write Any code Here Because Already Written 'onDoubleTap'
        return(true);
    }
    
    // abstract method from OnDoubleTapListener so must be implemented
    @Override
    public boolean onSingleTapConfirmed(MotionEvent e){
        singleTap++;
        return(true);
    }
    
    // abstract method from OnGestureListener so must be implemented
    @Override
    public boolean onDown(MotionEvent e){
        // Do Not Write Any code Here Because Already Written 'onSingleTapConfirmed'
        return(true);
    }
    
    // abstract method from OnGestureListener so must be implemented
    @Override
    public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX, float velocityY){
        return(true);
    }
    
    // abstract method from OnGestureListener so must be implemented
    @Override
    public void onLongPress(MotionEvent e){
        System.out.println("VDG: Long Press");
    }
    
    // abstract method from OnGestureListener so must be implemented
    @Override
    public boolean onScroll(MotionEvent e1, MotionEvent e2, float distanceX, float distanceY){
		System.out.println("VDG: Scroll");
        System.exit(0);
		return (true);
    }
    
    // abstract method from OnGestureListener so must be implemented
    @Override
    public void onShowPress(MotionEvent e){
    }
    
    // abstract method from OnGestureListener so must be implemented
    @Override
    public boolean onSingleTapUp(MotionEvent e){
        return(true);
    }
	
	private void initialize(GL10 gl){
		// Create vertex shader
		vertexShaderObject_perVer = GLES32.glCreateShader(GLES32.GL_VERTEX_SHADER);
		String vertexShaderSourceObject = String.format(
         "#version 320 es"+
         "\n"+
         "in vec4 vPosition;"+
         "in vec3 vNormal;"+
         "uniform mat4 u_model_matrix;"+
         "uniform mat4 u_view_matrix;"+
         "uniform mat4 u_projection_matrix;"+
         "uniform int u_double_tap;"+
         "uniform vec3 u_La;"+
         "uniform vec3 u_Ld;"+
         "uniform vec3 u_Ls;"+
         "uniform vec4 u_light_position;"+
         "uniform vec3 u_Ka;"+
         "uniform vec3 u_Kd;"+
         "uniform vec3 u_Ks;"+
         "uniform float u_material_shininess;"+
         "out vec3 phong_ads_color;"+
         "void main(void)"+
         "{"+
         "if (u_double_tap == 1)"+
         "{"+
         "vec4 eye_coordinates=u_view_matrix * u_model_matrix * vPosition;"+
         "vec3 transformed_normals=normalize(mat3(u_view_matrix * u_model_matrix) * vNormal);"+
         "vec3 light_direction = normalize(vec3(u_light_position) - eye_coordinates.xyz);"+
         "float tn_dot_ld = max(dot(transformed_normals, light_direction),0.0);"+
         "vec3 ambient = u_La * u_Ka;"+
         "vec3 diffuse = u_Ld * u_Kd * tn_dot_ld;"+
         "vec3 reflection_vector = reflect(-light_direction, transformed_normals);"+
         "vec3 viewer_vector = normalize(-eye_coordinates.xyz);"+
         "vec3 specular = u_Ls * u_Ks * pow(max(dot(reflection_vector, viewer_vector), 0.0), u_material_shininess);"+
         "phong_ads_color=ambient + diffuse + specular;"+
         "}"+
         "else"+
         "{"+
         "phong_ads_color = vec3(1.0, 1.0, 1.0);"+
         "}"+
         "gl_Position=u_projection_matrix * u_view_matrix * u_model_matrix * vPosition;"+
         "}"
        );

// provide source code to shader
GLES32.glShaderSource(vertexShaderObject_perVer, vertexShaderSourceObject);

// Compile shader & check for errors
GLES32.glCompileShader(vertexShaderObject_perVer);
int[] iShaderCompiledStatus = new int[1];
int[] iInfoLogLength = new int[1];
String szInfoLog = null;

GLES32.glGetShaderiv(vertexShaderObject_perVer, GLES32.GL_COMPILE_STATUS, iShaderCompiledStatus, 0);
if(iShaderCompiledStatus[0] == GLES32.GL_FALSE){
	GLES32.glGetShaderiv(vertexShaderObject_perVer, GLES32.GL_INFO_LOG_LENGTH, iInfoLogLength, 0);
	if(iInfoLogLength[0] > 0){
		szInfoLog = GLES32.glGetShaderInfoLog(vertexShaderObject_perVer);
		System.out.println("VDG: Vertex shader compilation log = " + szInfoLog);
		uninitialize();
		System.exit(0);
	}
}

// Fragment shader
fragmentShaderObject_perVer = GLES32.glCreateShader(GLES32.GL_FRAGMENT_SHADER);

// fragment shader source code
String fragmentShaderSourceCode = String.format
(
		 "#version 320 es"+
         "\n"+
         "precision highp float;"+
         "in vec3 phong_ads_color;"+
         "out vec4 FragColor;"+
         "void main(void)"+
         "{"+
         "FragColor = vec4(phong_ads_color, 1.0);"+
         "}"
);


// Provide source code to shader
GLES32.glShaderSource(fragmentShaderObject_perVer, fragmentShaderSourceCode);

// Compile shader and check for errors
GLES32.glCompileShader(fragmentShaderObject_perVer);
iShaderCompiledStatus[0] = 0; //re-initialize
iInfoLogLength[0] = 0; // re-initialize
szInfoLog = null; // re-initialize

GLES32.glGetShaderiv(fragmentShaderObject_perVer, GLES32.GL_COMPILE_STATUS, iShaderCompiledStatus, 0);
if(iShaderCompiledStatus[0] == GLES32.GL_FALSE){
	GLES32.glGetShaderiv(fragmentShaderObject_perVer, GLES32.GL_INFO_LOG_LENGTH,iInfoLogLength, 0);
	if(iInfoLogLength[0] > 0){
		szInfoLog = GLES32.glGetShaderInfoLog(fragmentShaderObject_perVer);
		System.out.println("VDG: Fragment shader compilation log = " + szInfoLog);
		uninitialize();
		System.exit(0);
	}
}

// Create shader program
shaderProgramObject_perVer = GLES32.glCreateProgram();

// attach vertex shader to shader program
GLES32.glAttachShader(shaderProgramObject_perVer, vertexShaderObject_perVer);

// attach fragment shader to shader program 
GLES32.glAttachShader(shaderProgramObject_perVer, fragmentShaderObject_perVer);

// pre-link bidning of shader program object with vertex shader attributes
GLES32.glBindAttribLocation(shaderProgramObject_perVer, GLESMacros.VDG_ATTRIBUTE_VERTEX, "vPosition");
GLES32.glBindAttribLocation(shaderProgramObject_perVer,GLESMacros.VDG_ATTRIBUTE_NORMAL,"vNormal");

// Link the two shaders together to shader program //object
GLES32.glLinkProgram(shaderProgramObject_perVer);
int[] iShaderProgramLinkStatus = new int[1];
iInfoLogLength[0] = 0;
szInfoLog = null;
GLES32.glGetProgramiv(shaderProgramObject_perVer, GLES32.GL_LINK_STATUS, iShaderProgramLinkStatus, 0);
if(iShaderProgramLinkStatus[0] == GLES32.GL_FALSE){
	GLES32.glGetProgramiv(shaderProgramObject_perVer, GLES32.GL_INFO_LOG_LENGTH, iInfoLogLength, 0);
	if(iInfoLogLength[0] > 0){
		szInfoLog = GLES32.glGetProgramInfoLog(shaderProgramObject_perVer);
		System.out.println("VDG: Shader program link log=" + szInfoLog);
		uninitialize();
		System.exit(0);
	}
}

// get MVP uniform location
modelMatrixUniform_perVer = GLES32.glGetUniformLocation(shaderProgramObject_perVer, "u_model_matrix");
viewMatrixUniform_perVer = GLES32.glGetUniformLocation(shaderProgramObject_perVer, "u_view_matrix");
projectionMatrixUniform_perVer = GLES32.glGetUniformLocation(shaderProgramObject_perVer, "u_projection_matrix");

doubleTapUniform_perVer = GLES32.glGetUniformLocation(shaderProgramObject_perVer, "u_double_tap");

laUniform_perVer = GLES32.glGetUniformLocation(shaderProgramObject_perVer, "u_La");
ldUniform_perVer = GLES32.glGetUniformLocation(shaderProgramObject_perVer, "u_Ld");
lsUniform_perVer = GLES32.glGetUniformLocation(shaderProgramObject_perVer, "u_Ls");
lightPositionUniform_perVer = GLES32.glGetUniformLocation(shaderProgramObject_perVer, "u_light_position");;

kaUniform_perVer = GLES32.glGetUniformLocation(shaderProgramObject_perVer, "u_Ka");
kdUniform_perVer = GLES32.glGetUniformLocation(shaderProgramObject_perVer, "u_Kd");
ksUniform_perVer = GLES32.glGetUniformLocation(shaderProgramObject_perVer, "u_Ks");
materialShininessUniform_perVer = GLES32.glGetUniformLocation(shaderProgramObject_perVer, "u_material_shininess");;

// per fragment shader code
vertexShaderObject_perFrag = GLES32.glCreateShader(GLES32.GL_VERTEX_SHADER);
		vertexShaderSourceObject = String.format(
         "#version 320 es"+
         "\n"+
         "in vec4 vPosition;"+
         "in vec3 vNormal;"+
         "uniform mat4 u_model_matrix;"+
         "uniform mat4 u_view_matrix;"+
         "uniform mat4 u_projection_matrix;"+
         "uniform mediump int u_double_tap;"+
         "uniform vec4 u_light_position;"+
         "out vec3 transformed_normals;"+
         "out vec3 light_direction;"+
         "out vec3 viewer_vector;"+
         "void main(void)"+
         "{"+
         "if (u_double_tap == 1)"+
         "{"+
         "vec4 eye_coordinates=u_view_matrix * u_model_matrix * vPosition;"+
         "transformed_normals=mat3(u_view_matrix * u_model_matrix) * vNormal;"+
         "light_direction = vec3(u_light_position) - eye_coordinates.xyz;"+
         "viewer_vector = -eye_coordinates.xyz;"+
         "}"+
         "gl_Position=u_projection_matrix * u_view_matrix * u_model_matrix * vPosition;"+
         "}"
        );
 

// provide source code to shader
GLES32.glShaderSource(vertexShaderObject_perFrag, vertexShaderSourceObject);

// Compile shader & check for errors
GLES32.glCompileShader(vertexShaderObject_perFrag);
iShaderCompiledStatus = new int[1];
iInfoLogLength = new int[1];
szInfoLog = null;

GLES32.glGetShaderiv(vertexShaderObject_perFrag, GLES32.GL_COMPILE_STATUS, iShaderCompiledStatus, 0);
if(iShaderCompiledStatus[0] == GLES32.GL_FALSE){
	GLES32.glGetShaderiv(vertexShaderObject_perFrag, GLES32.GL_INFO_LOG_LENGTH, iInfoLogLength, 0);
	if(iInfoLogLength[0] > 0){
		szInfoLog = GLES32.glGetShaderInfoLog(vertexShaderObject_perFrag);
		System.out.println("VDG: Vertex shader compilation log = " + szInfoLog);
		uninitialize();
		System.exit(0);
	}
}

// Fragment shader
fragmentShaderObject_perFrag = GLES32.glCreateShader(GLES32.GL_FRAGMENT_SHADER);

// fragment shader source code
fragmentShaderSourceCode = String.format
(
         "#version 320 es"+
         "\n"+
         "precision highp float;"+
         "in vec3 transformed_normals;"+
         "in vec3 light_direction;"+
         "in vec3 viewer_vector;"+
         "out vec4 FragColor;"+
         "uniform vec3 u_La;"+
         "uniform vec3 u_Ld;"+
         "uniform vec3 u_Ls;"+
         "uniform vec3 u_Ka;"+
         "uniform vec3 u_Kd;"+
         "uniform vec3 u_Ks;"+
         "uniform float u_material_shininess;"+
         "uniform int u_double_tap;"+
         "void main(void)"+
         "{"+
         "vec3 phong_ads_color;"+
         "if(u_double_tap==1)"+
         "{"+
         "vec3 normalized_transformed_normals=normalize(transformed_normals);"+
         "vec3 normalized_light_direction=normalize(light_direction);"+
         "vec3 normalized_viewer_vector=normalize(viewer_vector);"+
         "vec3 ambient = u_La * u_Ka;"+
         "float tn_dot_ld = max(dot(normalized_transformed_normals, normalized_light_direction),0.0);"+
         "vec3 diffuse = u_Ld * u_Kd * tn_dot_ld;"+
         "vec3 reflection_vector = reflect(-normalized_light_direction, normalized_transformed_normals);"+
         "vec3 specular = u_Ls * u_Ks * pow(max(dot(reflection_vector, normalized_viewer_vector), 0.0), u_material_shininess);"+
         "phong_ads_color=ambient + diffuse + specular;"+
         "}"+
         "else"+
         "{"+
         "phong_ads_color = vec3(1.0, 1.0, 1.0);"+
         "}"+
         "FragColor = vec4(phong_ads_color, 1.0);"+
         "}"
        );



// Provide source code to shader
GLES32.glShaderSource(fragmentShaderObject_perFrag, fragmentShaderSourceCode);

// Compile shader and check for errors
GLES32.glCompileShader(fragmentShaderObject_perFrag);
iShaderCompiledStatus[0] = 0; //re-initialize
iInfoLogLength[0] = 0; // re-initialize
szInfoLog = null; // re-initialize

GLES32.glGetShaderiv(fragmentShaderObject_perFrag, GLES32.GL_COMPILE_STATUS, iShaderCompiledStatus, 0);
if(iShaderCompiledStatus[0] == GLES32.GL_FALSE){
	GLES32.glGetShaderiv(fragmentShaderObject_perFrag, GLES32.GL_INFO_LOG_LENGTH,iInfoLogLength, 0);
	if(iInfoLogLength[0] > 0){
		szInfoLog = GLES32.glGetShaderInfoLog(fragmentShaderObject_perFrag);
		System.out.println("VDG: Fragment shader compilation log = " + szInfoLog);
		uninitialize();
		System.exit(0);
	}
}

// Create shader program
shaderProgramObject_perFrag = GLES32.glCreateProgram();

// attach vertex shader to shader program
GLES32.glAttachShader(shaderProgramObject_perFrag, vertexShaderObject_perFrag);

// attach fragment shader to shader program 
GLES32.glAttachShader(shaderProgramObject_perFrag, fragmentShaderObject_perFrag);

// pre-link bidning of shader program object with vertex shader attributes
GLES32.glBindAttribLocation(shaderProgramObject_perFrag, GLESMacros.VDG_ATTRIBUTE_VERTEX, "vPosition");
GLES32.glBindAttribLocation(shaderProgramObject_perFrag,GLESMacros.VDG_ATTRIBUTE_NORMAL,"vNormal");

// Link the two shaders together to shader program //object
GLES32.glLinkProgram(shaderProgramObject_perFrag);
iShaderProgramLinkStatus = new int[1];
iInfoLogLength[0] = 0;
szInfoLog = null;
GLES32.glGetProgramiv(shaderProgramObject_perFrag, GLES32.GL_LINK_STATUS, iShaderProgramLinkStatus, 0);
if(iShaderProgramLinkStatus[0] == GLES32.GL_FALSE){
	GLES32.glGetProgramiv(shaderProgramObject_perFrag, GLES32.GL_INFO_LOG_LENGTH, iInfoLogLength, 0);
	if(iInfoLogLength[0] > 0){
		szInfoLog = GLES32.glGetProgramInfoLog(shaderProgramObject_perFrag);
		System.out.println("VDG: Shader program link log=" + szInfoLog);
		uninitialize();
		System.exit(0);
	}
}

// get MVP uniform location
modelMatrixUniform_perFrag = GLES32.glGetUniformLocation(shaderProgramObject_perFrag, "u_model_matrix");
viewMatrixUniform_perFrag = GLES32.glGetUniformLocation(shaderProgramObject_perFrag, "u_view_matrix");
projectionMatrixUniform_perFrag = GLES32.glGetUniformLocation(shaderProgramObject_perFrag, "u_projection_matrix");

doubleTapUniform_perFrag = GLES32.glGetUniformLocation(shaderProgramObject_perFrag, "u_double_tap");

laUniform_perFrag = GLES32.glGetUniformLocation(shaderProgramObject_perFrag, "u_La");
ldUniform_perFrag = GLES32.glGetUniformLocation(shaderProgramObject_perFrag, "u_Ld");
lsUniform_perFrag = GLES32.glGetUniformLocation(shaderProgramObject_perFrag, "u_Ls");
lightPositionUniform_perFrag = GLES32.glGetUniformLocation(shaderProgramObject_perFrag, "u_light_position");;

kaUniform_perFrag = GLES32.glGetUniformLocation(shaderProgramObject_perFrag, "u_Ka");
kdUniform_perFrag = GLES32.glGetUniformLocation(shaderProgramObject_perFrag, "u_Kd");
ksUniform_perFrag = GLES32.glGetUniformLocation(shaderProgramObject_perFrag, "u_Ks");
materialShininessUniform_perFrag = GLES32.glGetUniformLocation(shaderProgramObject_perFrag, "u_material_shininess");;


// *** vertices, colors, shader attributes, vbo, vao // intitalizations
Sphere sphere=new Sphere();
        float sphere_vertices[]=new float[1146];
        float sphere_normals[]=new float[1146];
        float sphere_textures[]=new float[764];
        short sphere_elements[]=new short[2280];
        sphere.getSphereVertexData(sphere_vertices, sphere_normals, sphere_textures, sphere_elements);
        numVertices = sphere.getNumberOfSphereVertices();
        numElements = sphere.getNumberOfSphereElements();

        // vao
        GLES32.glGenVertexArrays(1,vao_sphere,0);
        GLES32.glBindVertexArray(vao_sphere[0]);
        
        // position vbo
        GLES32.glGenBuffers(1,vbo_sphere_position,0);
        GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER,vbo_sphere_position[0]);
        
        ByteBuffer byteBuffer=ByteBuffer.allocateDirect(sphere_vertices.length * 4);
        byteBuffer.order(ByteOrder.nativeOrder());
        FloatBuffer verticesBuffer=byteBuffer.asFloatBuffer();
        verticesBuffer.put(sphere_vertices);
        verticesBuffer.position(0);
        
        GLES32.glBufferData(GLES32.GL_ARRAY_BUFFER,
                            sphere_vertices.length * 4,
                            verticesBuffer,
                            GLES32.GL_STATIC_DRAW);
        
        GLES32.glVertexAttribPointer(GLESMacros.VDG_ATTRIBUTE_VERTEX,
                                     3,
                                     GLES32.GL_FLOAT,
                                     false,0,0);
        
        GLES32.glEnableVertexAttribArray(GLESMacros.VDG_ATTRIBUTE_VERTEX);
        
        GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER,0);
        
        // normal vbo
        GLES32.glGenBuffers(1,vbo_sphere_normal,0);
        GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER,vbo_sphere_normal[0]);
        
        byteBuffer=ByteBuffer.allocateDirect(sphere_normals.length * 4);
        byteBuffer.order(ByteOrder.nativeOrder());
        verticesBuffer=byteBuffer.asFloatBuffer();
        verticesBuffer.put(sphere_normals);
        verticesBuffer.position(0);
        
        GLES32.glBufferData(GLES32.GL_ARRAY_BUFFER,
                            sphere_normals.length * 4,
                            verticesBuffer,
                            GLES32.GL_STATIC_DRAW);
        
        GLES32.glVertexAttribPointer(GLESMacros.VDG_ATTRIBUTE_NORMAL,
                                     3,
                                     GLES32.GL_FLOAT,
                                     false,0,0);
        
        GLES32.glEnableVertexAttribArray(GLESMacros.VDG_ATTRIBUTE_NORMAL);
        
        GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER,0);
        
        // element vbo
        GLES32.glGenBuffers(1,vbo_sphere_element,0);
        GLES32.glBindBuffer(GLES32.GL_ELEMENT_ARRAY_BUFFER,vbo_sphere_element[0]);
        
        byteBuffer=ByteBuffer.allocateDirect(sphere_elements.length * 2);
        byteBuffer.order(ByteOrder.nativeOrder());
        ShortBuffer elementsBuffer=byteBuffer.asShortBuffer();
        elementsBuffer.put(sphere_elements);
        elementsBuffer.position(0);
        
        GLES32.glBufferData(GLES32.GL_ELEMENT_ARRAY_BUFFER,
                            sphere_elements.length * 2,
                            elementsBuffer,
                            GLES32.GL_STATIC_DRAW);
        
        GLES32.glBindBuffer(GLES32.GL_ELEMENT_ARRAY_BUFFER,0);

        GLES32.glBindVertexArray(0);
// enable depth testing
GLES32.glEnable(GLES32.GL_DEPTH_TEST);

// depth test to do
GLES32.glDepthFunc(GLES32.GL_LEQUAL);

// We will always cull back faces for better //performace
GLES32.glEnable(GLES32.GL_CULL_FACE);
		//Set the background frame color
		GLES32.glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
		doubleTap = 0;
		// set projection matrix to identity matrix
		Matrix.setIdentityM(perspectiveProjectionMatrix, 0);
	}
	
	private void resize(int width, int height){
		// Adjust the viewport based on geometry changes, such as screen rotation
		GLES32.glViewport(0, 0, width, height);
		Matrix.perspectiveM(perspectiveProjectionMatrix, 0, (float)45.0, (float)width/(float)height, (float)0.1, (float)100.0); 

	}
	public void display(){
		// Draw background color
		GLES32.glClear(GLES32.GL_COLOR_BUFFER_BIT | GLES32.GL_DEPTH_BUFFER_BIT);
		// use shader program
		if(singleTap % 2 == 1){
	
			GLES32.glUseProgram(shaderProgramObject_perVer);
			if(doubleTap==1)
			{
				GLES32.glUniform1i(doubleTapUniform_perVer, 1);
				
				// setting light's properties
				GLES32.glUniform3fv(laUniform_perVer, 1, light_ambient, 0);
				GLES32.glUniform3fv(ldUniform_perVer, 1, light_diffuse, 0);
				GLES32.glUniform3fv(lsUniform_perVer, 1, light_specular, 0);
				GLES32.glUniform4fv(lightPositionUniform_perVer, 1, light_position, 0);
				
				// setting material's properties
				GLES32.glUniform3fv(kaUniform_perVer, 1, material_ambient, 0);
				GLES32.glUniform3fv(kdUniform_perVer, 1, material_diffuse, 0);
				GLES32.glUniform3fv(ksUniform_perVer, 1, material_specular, 0);
				GLES32.glUniform1f(materialShininessUniform_perVer, material_shininess);
			}
			else
			{
				GLES32.glUniform1i(doubleTapUniform_perVer, 0);
			}
			
			// OpenGL-ES drawing
			float modelMatrix[]=new float[16];
			float viewMatrix[]=new float[16];
			
			// set modelMatrix and viewMatrix matrices to identity matrix
			Matrix.setIdentityM(modelMatrix,0);
			Matrix.setIdentityM(viewMatrix,0);

			// apply z axis translation to go deep into the screen by -1.5,
			// so that pyramid with same fullscreen co-ordinates, but due to above translation will look small
			Matrix.translateM(modelMatrix,0,0.0f,0.0f,-1.5f);
			
			GLES32.glUniformMatrix4fv(modelMatrixUniform_perVer,1,false,modelMatrix,0);
			GLES32.glUniformMatrix4fv(viewMatrixUniform_perVer,1,false,viewMatrix,0);
			GLES32.glUniformMatrix4fv(projectionMatrixUniform_perVer,1,false,perspectiveProjectionMatrix,0);
			
			// bind vao
			GLES32.glBindVertexArray(vao_sphere[0]);
			
			// *** draw, either by glDrawTriangles() or glDrawArrays() or glDrawElements()
			GLES32.glBindBuffer(GLES32.GL_ELEMENT_ARRAY_BUFFER, vbo_sphere_element[0]);
			GLES32.glDrawElements(GLES32.GL_TRIANGLES, numElements, GLES32.GL_UNSIGNED_SHORT, 0);
			
			// unbind vao
			GLES32.glBindVertexArray(0);
		}
		else{
			GLES32.glUseProgram(shaderProgramObject_perFrag);
			if(doubleTap==1)
			{
				GLES32.glUniform1i(doubleTapUniform_perFrag, 1);
				
				// setting light's properties
				GLES32.glUniform3fv(laUniform_perFrag, 1, light_ambient, 0);
				GLES32.glUniform3fv(ldUniform_perFrag, 1, light_diffuse, 0);
				GLES32.glUniform3fv(lsUniform_perFrag, 1, light_specular, 0);
				GLES32.glUniform4fv(lightPositionUniform_perFrag, 1, light_position, 0);
				
				// setting material's properties
				GLES32.glUniform3fv(kaUniform_perFrag, 1, material_ambient, 0);
				GLES32.glUniform3fv(kdUniform_perFrag, 1, material_diffuse, 0);
				GLES32.glUniform3fv(ksUniform_perFrag, 1, material_specular, 0);
				GLES32.glUniform1f(materialShininessUniform_perFrag, material_shininess);
			}
			else
			{
				GLES32.glUniform1i(doubleTapUniform_perFrag, 0);
			}
			
			// OpenGL-ES drawing
			float modelMatrix[]=new float[16];
			float viewMatrix[]=new float[16];
			
			// set modelMatrix and viewMatrix matrices to identity matrix
			Matrix.setIdentityM(modelMatrix,0);
			Matrix.setIdentityM(viewMatrix,0);

			// apply z axis translation to go deep into the screen by -1.5,
			// so that pyramid with same fullscreen co-ordinates, but due to above translation will look small
			Matrix.translateM(modelMatrix,0,0.0f,0.0f,-1.5f);
			
			GLES32.glUniformMatrix4fv(modelMatrixUniform_perFrag,1,false,modelMatrix,0);
			GLES32.glUniformMatrix4fv(viewMatrixUniform_perFrag,1,false,viewMatrix,0);
			GLES32.glUniformMatrix4fv(projectionMatrixUniform_perFrag,1,false,perspectiveProjectionMatrix,0);
			
			// bind vao
			GLES32.glBindVertexArray(vao_sphere[0]);
			
			// *** draw, either by glDrawTriangles() or glDrawArrays() or glDrawElements()
			GLES32.glBindBuffer(GLES32.GL_ELEMENT_ARRAY_BUFFER, vbo_sphere_element[0]);
			GLES32.glDrawElements(GLES32.GL_TRIANGLES, numElements, GLES32.GL_UNSIGNED_SHORT, 0);
			
			// unbind vao
			GLES32.glBindVertexArray(0);
		}
		
		// un-use shader program
		GLES32.glUseProgram(0);
		
		//render/flush
		requestRender();
	
	}


void uninitialize(){
	// destroy vao

	
	if(vao_sphere[0] != 0)
        {
            GLES32.glDeleteVertexArrays(1, vao_sphere, 0);
            vao_sphere[0]=0;
        }
        
        // destroy position vbo
        if(vbo_sphere_position[0] != 0)
        {
            GLES32.glDeleteBuffers(1, vbo_sphere_position, 0);
            vbo_sphere_position[0]=0;
        }
        
        // destroy normal vbo
        if(vbo_sphere_normal[0] != 0)
        {
            GLES32.glDeleteBuffers(1, vbo_sphere_normal, 0);
            vbo_sphere_normal[0]=0;
        }
        
        // destroy element vbo
        if(vbo_sphere_element[0] != 0)
        {
            GLES32.glDeleteBuffers(1, vbo_sphere_element, 0);
            vbo_sphere_element[0]=0;
        }

	
	if(shaderProgramObject_perVer != 0){
		if(vertexShaderObject_perVer != 0){
			// detach vertex shader from shader program 			//object
			GLES32.glDetachShader(shaderProgramObject_perVer, vertexShaderObject_perVer);
			// delete vertex shader object
			GLES32.glDeleteShader(vertexShaderObject_perVer);
			vertexShaderObject_perVer = 0;
		}
		
		if(fragmentShaderObject_perVer != 0){
			// detach fragment  shader from shader program 				//object
                GLES32.glDetachShader(shaderProgramObject_perVer, fragmentShaderObject_perVer);
                // delete fragment shader object
                GLES32.glDeleteShader(fragmentShaderObject_perVer);
                fragmentShaderObject_perVer = 0;

		}
	}
		// delete shader program object
        	if(shaderProgramObject_perVer != 0)
        	{
           	GLES32.glDeleteProgram(shaderProgramObject_perVer);
           	shaderProgramObject_perVer = 0;
        	}

}
}