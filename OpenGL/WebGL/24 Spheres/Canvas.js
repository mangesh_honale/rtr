// global variables
var canvas=null;
var gl=null; // webgl context
var bFullscreen=false;
var canvas_original_width;
var canvas_original_height;

const WebGLMacros= // when whole 'WebGLMacros' is 'const', all inside it are automatically 'const'
{
VDG_ATTRIBUTE_VERTEX:0,
VDG_ATTRIBUTE_COLOR:1,
VDG_ATTRIBUTE_NORMAL:2,
VDG_ATTRIBUTE_TEXTURE0:3,
};

var gVertexShaderObject;
var gFragmentShaderObject;
var gShaderProgramObject;

var axisIdentifier = 0;
var light_ambient=[0.0,0.0,0.0];
var light_diffuse=[1.0,1.0,1.0];
var light_specular=[1.0,1.0,1.0];
var light_position=[0.0,0.0,-6.0,1.0];

//11
var material_emerald_ambient = [ 0.0215, 0.1745, 0.0215];
var material_emerald_diffuse = [ 0.07568, 0.61424, 0.07568];
var material_emerald_specular = [ 0.633, 0.727811, 0.633];
var material_emerald_shininess = 0.6 * 128;

//12
var material_jade_ambient = [ 0.135, 0.225, 0.1575];
var material_jade_diffuse = [ 0.54, 0.89, 0.63];
var material_jade_specular = [ 0.316228, 0.316228, 0.316228];
var material_jade_shininess = 0.1 * 128;

//13
var material_obsidian_ambient = [ 0.05375, 0.05, 0.06625];
var material_obsidian_diffuse = [ 0.18275, 0.17, 0.22525];
var material_obsidian_specular = [ 0.332741, 0.328634, 0.346435];
var material_obsidian_shininess = 0.3 * 128;

//14
var material_pearl_ambient = [ 0.25, 0.20725, 0.20725];
var material_pearl_diffuse = [ 1.0, 0.829, 0.829];
var material_pearl_specular = [ 0.296648, 0.296648, 0.296648];
var material_pearl_shininess = 0.088 * 128;

//15
var material_ruby_ambient = [ 0.1745, 0.01175, 0.01175];
var material_ruby_diffuse = [ 0.61424, 0.04136, 0.04136];
var material_ruby_specular = [ 0.727811, 0.626959, 0.626959];
var material_ruby_shininess = 0.6 * 128;

//16
var material_turquoise_ambient = [ 0.1, 0.18725, 0.1745];
var material_turquoise_diffuse = [ 0.396, 0.74151, 0.69102];
var material_turquoise_specular = [ 0.297254, 0.30829, 0.306678];
var material_turquoise_shininess = 0.1 * 128;

//21
var material_brass_ambient = [ 0.329412, 0.223529, 0.027451];
var material_brass_diffuse = [ 0.780392, 0.568627, 0.113725];
var material_brass_specular = [ 0.992157, 0.941176, 0.807843];
var material_brass_shininess = 0.21794872 * 128;

//22
var material_bronze_ambient = [ 0.2125, 0.1275, 0.054];
var material_bronze_diffuse = [ 0.714, 0.4284, 0.18144];
var material_bronze_specular = [ 0.393548, 0.271906, 0.166721];
var material_bronze_shininess = 0.2 * 128;

//23
var material_chrome_ambient = [ 0.25, 0.25, 0.25];
var material_chrome_diffuse = [ 0.4, 0.4, 0.4];
var material_chrome_specular = [ 0.774597, 0.774597, 0.774597];
var material_chrome_shininess = 0.6 * 128;

//24
var material_copper_ambient = [ 0.19125, 0.0735, 0.0225];
var material_copper_diffuse = [ 0.7038, 0.27048, 0.0828];
var material_copper_specular = [ 0.256777, 0.137622, 0.86014];
var material_copper_shininess = 0.1 * 128;

//25
var material_gold_ambient = [ 0.24725, 0.1995, 0.0745];
var material_gold_diffuse = [ 0.75164, 0.61648, 0.22648];
var material_gold_specular = [ 0.628281, 0.555802, 0.366065];
var material_gold_shininess = 0.4 * 128;

//26
var material_silver_ambient = [ 0.19225, 0.19225, 0.19225];
var material_silver_diffuse = [ 0.50754, 0.50754, 0.50754];
var material_silver_specular = [ 0.508273, 0.508273, 0.508273];
var material_silver_shininess = 0.4 * 128;

//31
var material_black_ambient = [ 0.0, 0.0, 0.0];
var material_black_diffuse = [ 0.01, 0.01, 0.01];
var material_black_specular = [ 0.50, 0.50, 0.50];
var material_black_shininess = 0.25 * 128;

//32
var material_cyan_ambient = [ 0.0, 0.1, 0.06];
var material_cyan_diffuse = [ 0.0, 0.50980392, 0.50980932];
var material_cyan_specular = [ 0.50196078, 0.50196078, 0.50196078];
var material_cyan_shininess = 0.25 * 128;

//33
var material_green_ambient = [ 0.0, 0.0, 0.0];
var material_green_diffuse = [ 0.1, 0.35, 0.1];
var material_green_specular = [ 0.45, 0.45, 0.45];
var material_green_shininess = 0.25 * 128;

//34
var material_red_ambient = [ 0.0, 0.0, 0.0];
var material_red_diffuse = [ 0.5, 0.0, 0.0];
var material_red_specular = [ 0.7, 0.6, 0.6];
var material_red_shininess = 0.25 * 128;

//35
var material_white_ambient = [ 0.0, 0.0, 0.0];
var material_white_diffuse = [ 0.55, 0.55, 0.55];
var material_white_specular = [ 0.70, 0.70, 0.70];
var material_white_shininess = 0.25 * 128;

//36
var material_yellow_ambient = [ 0.0, 0.0, 0.0];
var material_yellow_diffuse = [ 0.5, 0.5, 0.0];
var material_yellow_specular = [ 0.60, 0.60, 0.50];
var material_yellow_shininess = 0.25 * 128;

//41
var material_black2_ambient = [ 0.02, 0.02, 0.02];
var material_black2_diffuse = [ 0.01, 0.01, 0.01];
var material_black2_specular = [ 0.4, 0.4, 0.4];
var material_black2_shininess = 0.078125 * 128;

//42
var material_cyan2_ambient = [ 0.0, 0.05, 0.05];
var material_cyan2_diffuse = [ 0.4, 0.5, 0.0];
var material_cyan2_specular = [ 0.04, 0.7, 0.7];
var material_cyan2_shininess = 0.078125 * 128;

//43
var material_green2_ambient = [ 0.0, 0.05, 0.0];
var material_green2_diffuse = [ 0.4, 0.5, 0.4];
var material_green2_specular = [ 0.04, 0.7, 0.04];
var material_green2_shininess = 0.078125 * 128;

//44
var material_red2_ambient = [ 0.05, 0.0, 0.0];
var material_red2_diffuse = [ 0.5, 0.4, 0.4];
var material_red2_specular = [ 0.7, 0.04, 0.04];
var material_red2_shininess = 0.078125 * 128;

//45
var material_white2_ambient = [ 0.05, 0.05, 0.05];
var material_white2_diffuse = [ 0.5, 0.5, 0.5];
var material_white2_specular = [ 0.7, 0.7, 0.7];
var material_white2_shininess = 0.078125 * 128;

//46
var material_yellow2_ambient = [ 0.05, 0.05, 0.0];
var material_yellow2_diffuse = [ 0.5, 0.5, 0.4];
var material_yellow2_specular = [ 0.7, 0.7, 0.04];
var material_yellow2_shininess = 0.078125 * 128;


var material_ambient = [ 0.0,0.0,0.0];
var material_diffuse = [ 1.0,1.0,1.0];
var material_specular = [ 1.0,1.0,1.0];
var material_shininess = 50.0;
var sphere=null;

var perspectiveProjectionMatrix;

var model_matrix_uniform, view_matrix_uniform, projection_matrix_uniform;
var L_KeyPressed_uniform, La_uniform, Ld_uniform, Ls_uniform;
var light_position_uniform, Ka_uniform, Kd_uniform, Ks_uniform;
var material_shininess_uniform;

var bLKeyPressed=false;
var lightAngle = 0.0;

// To start animation : To have requestAnimationFrame() to be called "cross-browser" compatible
var requestAnimationFrame =
window.requestAnimationFrame ||
window.webkitRequestAnimationFrame ||
window.mozRequestAnimationFrame ||
window.oRequestAnimationFrame ||
window.msRequestAnimationFrame;

// To stop animation : To have cancelAnimationFrame() to be called "cross-browser" compatible
var cancelAnimationFrame =
window.cancelAnimationFrame ||
window.webkitCancelRequestAnimationFrame || window.webkitCancelAnimationFrame ||
window.mozCancelRequestAnimationFrame || window.mozCancelAnimationFrame ||
window.oCancelRequestAnimationFrame || window.oCancelAnimationFrame ||
window.msCancelRequestAnimationFrame || window.msCancelAnimationFrame;

// onload function
function main()
{
    // get <canvas> element
    canvas = document.getElementById("AMC");
    if(!canvas)
        console.log("Obtaining Canvas Failed\n");
    else
        console.log("Obtaining Canvas Succeeded\n");
    canvas_original_width=canvas.width;
    canvas_original_height=canvas.height;
    
    // register keyboard's keydown event handler
    window.addEventListener("keydown", keyDown, false);
    window.addEventListener("click", mouseDown, false);
    window.addEventListener("resize", resize, false);

    // initialize WebGL
    init();
    
    // start drawing here as warming-up
    resize();
    draw();
}

function toggleFullScreen()
{
    // code
    var fullscreen_element =
    document.fullscreenElement ||
    document.webkitFullscreenElement ||
    document.mozFullScreenElement ||
    document.msFullscreenElement ||
    null;

    // if not fullscreen
    if(fullscreen_element==null)
    {
        if(canvas.requestFullscreen)
            canvas.requestFullscreen();
        else if(canvas.mozRequestFullScreen)
            canvas.mozRequestFullScreen();
        else if(canvas.webkitRequestFullscreen)
            canvas.webkitRequestFullscreen();
        else if(canvas.msRequestFullscreen)
            canvas.msRequestFullscreen();
        bFullscreen=true;
    }
    else // if already fullscreen
    {
        if(document.exitFullscreen)
            document.exitFullscreen();
        else if(document.mozCancelFullScreen)
            document.mozCancelFullScreen();
        else if(document.webkitExitFullscreen)
            document.webkitExitFullscreen();
        else if(document.msExitFullscreen)
            document.msExitFullscreen();
        bFullscreen=false;
    }
}

function init()
{
    // code
    // get WebGL 2.0 context
    gl = canvas.getContext("webgl2");
    if(gl==null) // failed to get context
    {
        console.log("Failed to get the rendering context for WebGL");
        return;
    }
    gl.viewportWidth = canvas.width;
    gl.viewportHeight = canvas.height;
   
    // vertex shader
    var vertexShaderSourceCode=
    "#version 300 es"+
    "\n"+
    "in vec4 vPosition;"+
    "in vec3 vNormal;"+
    "uniform mat4 u_model_matrix;"+
    "uniform mat4 u_view_matrix;"+
    "uniform mat4 u_projection_matrix;"+
    "uniform mediump int u_LKeyPressed;"+
    "uniform vec4 u_light_position;"+
    "out vec3 transformed_normals;"+
    "out vec3 light_direction;"+
    "out vec3 viewer_vector;"+
    "void main(void)"+
    "{"+
    "if(u_LKeyPressed == 1)"+
    "{"+
    "vec4 eye_coordinates=u_view_matrix * u_model_matrix * vPosition;"+
    "transformed_normals=mat3(u_view_matrix * u_model_matrix) * vNormal;"+
    "light_direction = vec3(u_light_position) - eye_coordinates.xyz;"+
    "viewer_vector = -eye_coordinates.xyz;"+
    "}"+
    "gl_Position=u_projection_matrix * u_view_matrix * u_model_matrix * vPosition;"+
    "}";

    gVertexShaderObject=gl.createShader(gl.VERTEX_SHADER);
    gl.shaderSource(gVertexShaderObject,vertexShaderSourceCode);
    gl.compileShader(gVertexShaderObject);
    if(gl.getShaderParameter(gVertexShaderObject,gl.COMPILE_STATUS)==false)
    {
        var error=gl.getShaderInfoLog(gVertexShaderObject);
        if(error.length > 0)
        {
            alert(error);
            uninitialize();
        }
    }
    
    // fragment shader
    var fragmentShaderSourceCode=
    "#version 300 es"+
    "\n"+
    "precision highp float;"+
    "in vec3 transformed_normals;"+
    "in vec3 light_direction;"+
    "in vec3 viewer_vector;"+
    "out vec4 FragColor;"+
    "uniform vec3 u_La;"+
    "uniform vec3 u_Ld;"+
    "uniform vec3 u_Ls;"+
    "uniform vec3 u_Ka;"+
    "uniform vec3 u_Kd;"+
    "uniform vec3 u_Ks;"+
    "uniform float u_material_shininess;"+
    "uniform int u_LKeyPressed;"+
    "void main(void)"+
    "{"+
    "vec3 phong_ads_color;"+
    "if(u_LKeyPressed == 1)"+
    "{"+
    "vec3 normalized_transformed_normals=normalize(transformed_normals);"+
    "vec3 normalized_light_direction=normalize(light_direction);"+
    "vec3 normalized_viewer_vector=normalize(viewer_vector);"+
    "vec3 ambient = u_La * u_Ka;"+
    "float tn_dot_ld = max(dot(normalized_transformed_normals, normalized_light_direction),0.0);"+
    "vec3 diffuse = u_Ld * u_Kd * tn_dot_ld;"+
    "vec3 reflection_vector = reflect(-normalized_light_direction, normalized_transformed_normals);"+
    "vec3 specular = u_Ls * u_Ks * pow(max(dot(reflection_vector, normalized_viewer_vector), 0.0), u_material_shininess);"+
    "phong_ads_color=ambient + diffuse + specular;"+
    "}"+
    "else"+
    "{"+
    "phong_ads_color = vec3(1.0, 1.0, 1.0);"+
    "}"+
    "FragColor = vec4(phong_ads_color, 1.0);"+
    "}"
    
    gFragmentShaderObject=gl.createShader(gl.FRAGMENT_SHADER);
    gl.shaderSource(gFragmentShaderObject,fragmentShaderSourceCode);
    gl.compileShader(gFragmentShaderObject);
    if(gl.getShaderParameter(gFragmentShaderObject,gl.COMPILE_STATUS)==false)
    {
        var error=gl.getShaderInfoLog(gFragmentShaderObject);
        if(error.length > 0)
        {
            alert(error);
            uninitialize();
        }
    }
    
    // shader program
    gShaderProgramObject=gl.createProgram();
    gl.attachShader(gShaderProgramObject,gVertexShaderObject);
    gl.attachShader(gShaderProgramObject,gFragmentShaderObject);
    
    // pre-link binding of shader program object with vertex shader attributes
    gl.bindAttribLocation(gShaderProgramObject,WebGLMacros.VDG_ATTRIBUTE_VERTEX,"vPosition");
    gl.bindAttribLocation(gShaderProgramObject,WebGLMacros.VDG_ATTRIBUTE_NORMAL,"vNormal");

    // linking
    gl.linkProgram(gShaderProgramObject);
    if (!gl.getProgramParameter(gShaderProgramObject, gl.LINK_STATUS))
    {
        var error=gl.getProgramInfoLog(gShaderProgramObject);
        if(error.length > 0)
        {
            alert(error);
            uninitialize();
        }
    }

    // get Model Matrix uniform location
    model_matrix_uniform=gl.getUniformLocation(gShaderProgramObject,"u_model_matrix");
    // get View Matrix uniform location
    view_matrix_uniform=gl.getUniformLocation(gShaderProgramObject,"u_view_matrix");
    // get Projection Matrix uniform location
    projection_matrix_uniform=gl.getUniformLocation(gShaderProgramObject,"u_projection_matrix");
    
    // get single tap detecting uniform
    L_KeyPressed_uniform=gl.getUniformLocation(gShaderProgramObject,"u_LKeyPressed");
    
    // ambient color intensity of light
    La_uniform=gl.getUniformLocation(gShaderProgramObject,"u_La");
    // diffuse color intensity of light
    Ld_uniform=gl.getUniformLocation(gShaderProgramObject,"u_Ld");
    // specular color intensity of light
    Ls_uniform=gl.getUniformLocation(gShaderProgramObject,"u_Ls");
    // position of light
    light_position_uniform =gl.getUniformLocation(gShaderProgramObject,"u_light_position");
    
    // ambient reflective color intensity of material
    Ka_uniform=gl.getUniformLocation(gShaderProgramObject,"u_Ka");
    // diffuse reflective color intensity of material
    Kd_uniform=gl.getUniformLocation(gShaderProgramObject,"u_Kd");
    // specular reflective color intensity of material
    Ks_uniform=gl.getUniformLocation(gShaderProgramObject,"u_Ks");
    // shininess of material ( value is conventionally between 1 to 200 )
    material_shininess_uniform=gl.getUniformLocation(gShaderProgramObject,"u_material_shininess");
    
    // *** vertices, colors, shader attribs, vbo, vao initializations ***
    sphere=new Mesh();
    makeSphere(sphere,2.0,30,30);

    // Depth test will always be enabled
    gl.enable(gl.DEPTH_TEST);
    
    // depth test to do
    gl.depthFunc(gl.LEQUAL);
    
    // We will always cull back faces for better performance
    gl.enable(gl.CULL_FACE);
    
    // set clear color
    gl.clearColor(0.25, 0.25, 0.25, 1.0); // black
    
    // initialize projection matrix
    perspectiveProjectionMatrix=mat4.create();
}

function resize()
{
    // code
    if(bFullscreen==true)
    {
        canvas.width=window.innerWidth;
        canvas.height=window.innerHeight;
    }
    else
    {
        canvas.width=canvas_original_width;
        canvas.height=canvas_original_height;
    }
   
    // set the viewport to match
    gl.viewport(0, 0, canvas.width, canvas.height);
    
    mat4.perspective(perspectiveProjectionMatrix, 45.0, parseFloat(canvas.width)/parseFloat(canvas.height), 0.1, 100.0);
}

function draw()
{
    // code
    gl.clear(gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT);
    
    gl.useProgram(gShaderProgramObject);
    
    if(bLKeyPressed==true)
    {
        gl.uniform1i(L_KeyPressed_uniform, 1);
        
        // setting light properties
        gl.uniform3fv(La_uniform, light_ambient); // ambient intensity of light
        gl.uniform3fv(Ld_uniform, light_diffuse); // diffuse intensity of light
        gl.uniform3fv(Ls_uniform, light_specular); // specular intensity of light
        if (axisIdentifier == 0) {
            light_position[1] = Math.sin(lightAngle);
            light_position[2] = Math.cos(lightAngle) - 4;
        }
        if (axisIdentifier == 1) {
            light_position[1] = 0.0;    
            light_position[0] = Math.sin(lightAngle);
            light_position[2] = Math.cos(lightAngle) - 4;
        }
        if (axisIdentifier == 2) {
            light_position[2] = -6.0;
            light_position[0] = parseFloat(3) * Math.cos(lightAngle) - parseFloat(3) * Math.sin(lightAngle);
            light_position[1] = parseFloat(3) * Math.sin(lightAngle) + parseFloat(3) * Math.cos(lightAngle);
			//light_position[0] = Math.cos(lightAngle) - 4;
			//light_position[1] = Math.sin(lightAngle) ;
        }
        gl.uniform4fv(light_position_uniform, light_position); // light position
        
        // setting material properties
        gl.uniform3fv(Ka_uniform, material_ambient); // ambient reflectivity of material
        gl.uniform3fv(Kd_uniform, material_diffuse); // diffuse reflectivity of material
        gl.uniform3fv(Ks_uniform, material_specular); // specular reflectivity of material
        gl.uniform1f(material_shininess_uniform, material_shininess); // material shininess
    }
    else
    {
        gl.uniform1i(L_KeyPressed_uniform, 0);
    }
    
    var modelMatrix=mat4.create();
    var viewMatrix=mat4.create();

    mat4.translate(modelMatrix, modelMatrix, [0.0,0.0,-6.0]);
    
    gl.uniformMatrix4fv(model_matrix_uniform,false,modelMatrix);
    gl.uniformMatrix4fv(view_matrix_uniform,false,viewMatrix);
    gl.uniformMatrix4fv(projection_matrix_uniform,false,perspectiveProjectionMatrix);
    
    //11
    gl.viewport(canvas.width/6, 5*canvas.height/6, canvas.width / 6, canvas.height / 6);
    mat4.perspective(perspectiveProjectionMatrix, 45.0, parseFloat(canvas.width/6)/parseFloat(canvas.height/6), 0.1, 100.0);
    sphere.draw();
   
    //12
    if(bLKeyPressed==true){
        gl.uniform3fv(Ka_uniform, material_jade_ambient); // ambient reflectivity of material
        gl.uniform3fv(Kd_uniform, material_jade_diffuse); // diffuse reflectivity of material
        gl.uniform3fv(Ks_uniform, material_jade_specular); // specular reflectivity of material
        gl.uniform1f(material_shininess_uniform, material_jade_shininess ); // material shininess
        
    }
    modelMatrix=mat4.create();
    viewMatrix=mat4.create();
    mat4.translate(modelMatrix, modelMatrix, [0.0,0.0,-6.0]);
    gl.uniformMatrix4fv(model_matrix_uniform,false,modelMatrix);
    gl.uniformMatrix4fv(view_matrix_uniform,false,viewMatrix);
    gl.uniformMatrix4fv(projection_matrix_uniform,false,perspectiveProjectionMatrix);
    gl.viewport(canvas.width/6, 4*canvas.height/6, canvas.width / 6, canvas.height / 6);
    mat4.perspective(perspectiveProjectionMatrix, 45.0, parseFloat(canvas.width/6)/parseFloat(canvas.height/6), 0.1, 100.0);
    sphere.draw();
    
    //13
    if(bLKeyPressed==true){
        gl.uniform3fv(Ka_uniform, material_obsidian_ambient); // ambient reflectivity of material
        gl.uniform3fv(Kd_uniform, material_obsidian_diffuse); // diffuse reflectivity of material
        gl.uniform3fv(Ks_uniform, material_obsidian_specular); // specular reflectivity of material
        gl.uniform1f(material_shininess_uniform, material_obsidian_shininess ); // material shininess
        
    }
    modelMatrix=mat4.create();
    viewMatrix=mat4.create();
    mat4.translate(modelMatrix, modelMatrix, [0.0,0.0,-6.0]);
    gl.uniformMatrix4fv(model_matrix_uniform,false,modelMatrix);
    gl.uniformMatrix4fv(view_matrix_uniform,false,viewMatrix);
    gl.uniformMatrix4fv(projection_matrix_uniform,false,perspectiveProjectionMatrix);
    gl.viewport(canvas.width/6, 3*canvas.height/6, canvas.width / 6, canvas.height / 6);
    mat4.perspective(perspectiveProjectionMatrix, 45.0, parseFloat(canvas.width/6)/parseFloat(canvas.height/6), 0.1, 100.0);
    sphere.draw();
    
    //14
    if(bLKeyPressed==true){
        gl.uniform3fv(Ka_uniform, material_pearl_ambient); // ambient reflectivity of material
        gl.uniform3fv(Kd_uniform, material_pearl_diffuse); // diffuse reflectivity of material
        gl.uniform3fv(Ks_uniform, material_pearl_specular); // specular reflectivity of material
        gl.uniform1f(material_shininess_uniform, material_pearl_shininess ); // material shininess
        
    }
    modelMatrix=mat4.create();
    viewMatrix=mat4.create();
    mat4.translate(modelMatrix, modelMatrix, [0.0,0.0,-6.0]);
    gl.uniformMatrix4fv(model_matrix_uniform,false,modelMatrix);
    gl.uniformMatrix4fv(view_matrix_uniform,false,viewMatrix);
    gl.uniformMatrix4fv(projection_matrix_uniform,false,perspectiveProjectionMatrix);
    gl.viewport(canvas.width/6, 2*canvas.height/6, canvas.width / 6, canvas.height / 6);
    mat4.perspective(perspectiveProjectionMatrix, 45.0, parseFloat(canvas.width/6)/parseFloat(canvas.height/6), 0.1, 100.0);
    sphere.draw();
    
    //15
    if(bLKeyPressed==true){
        gl.uniform3fv(Ka_uniform, material_ruby_ambient); // ambient reflectivity of material
        gl.uniform3fv(Kd_uniform, material_ruby_diffuse); // diffuse reflectivity of material
        gl.uniform3fv(Ks_uniform, material_ruby_specular); // specular reflectivity of material
        gl.uniform1f(material_shininess_uniform, material_ruby_shininess ); // material shininess
        
    }
    modelMatrix=mat4.create();
    viewMatrix=mat4.create();
    mat4.translate(modelMatrix, modelMatrix, [0.0,0.0,-6.0]);
    gl.uniformMatrix4fv(model_matrix_uniform,false,modelMatrix);
    gl.uniformMatrix4fv(view_matrix_uniform,false,viewMatrix);
    gl.uniformMatrix4fv(projection_matrix_uniform,false,perspectiveProjectionMatrix);
    gl.viewport(canvas.width/6, canvas.height/6, canvas.width / 6, canvas.height / 6);
    mat4.perspective(perspectiveProjectionMatrix, 45.0, parseFloat(canvas.width/6)/parseFloat(canvas.height/6), 0.1, 100.0);
    sphere.draw();
    
    //16
    if(bLKeyPressed==true){
        gl.uniform3fv(Ka_uniform, material_turquoise_ambient); // ambient reflectivity of material
        gl.uniform3fv(Kd_uniform, material_turquoise_diffuse); // diffuse reflectivity of material
        gl.uniform3fv(Ks_uniform, material_turquoise_specular); // specular reflectivity of material
        gl.uniform1f(material_shininess_uniform, material_turquoise_shininess ); // material shininess
        
    }
    modelMatrix=mat4.create();
    viewMatrix=mat4.create();
    mat4.translate(modelMatrix, modelMatrix, [0.0,0.0,-6.0]);
    gl.uniformMatrix4fv(model_matrix_uniform,false,modelMatrix);
    gl.uniformMatrix4fv(view_matrix_uniform,false,viewMatrix);
    gl.uniformMatrix4fv(projection_matrix_uniform,false,perspectiveProjectionMatrix);
    gl.viewport(canvas.width/6, 0, canvas.width / 6, canvas.height / 6);
    mat4.perspective(perspectiveProjectionMatrix, 45.0, parseFloat(canvas.width/6)/parseFloat(canvas.height/6), 0.1, 100.0);
    sphere.draw();
    
    //21
    if(bLKeyPressed==true){
        gl.uniform3fv(Ka_uniform, material_brass_ambient); // ambient reflectivity of material
        gl.uniform3fv(Kd_uniform, material_brass_diffuse); // diffuse reflectivity of material
        gl.uniform3fv(Ks_uniform, material_brass_specular); // specular reflectivity of material
        gl.uniform1f(material_shininess_uniform, material_brass_shininess ); // material shininess
        
    }
    modelMatrix=mat4.create();
    viewMatrix=mat4.create();
    mat4.translate(modelMatrix, modelMatrix, [0.0,0.0,-6.0]);
    gl.uniformMatrix4fv(model_matrix_uniform,false,modelMatrix);
    gl.uniformMatrix4fv(view_matrix_uniform,false,viewMatrix);
    gl.uniformMatrix4fv(projection_matrix_uniform,false,perspectiveProjectionMatrix);
    gl.viewport(2*canvas.width/6, 5*canvas.height/6, canvas.width / 6, canvas.height / 6);
    mat4.perspective(perspectiveProjectionMatrix, 45.0, parseFloat(canvas.width/6)/parseFloat(canvas.height/6), 0.1, 100.0);
    sphere.draw();
    
    //22
    if(bLKeyPressed==true){
        gl.uniform3fv(Ka_uniform, material_bronze_ambient); // ambient reflectivity of material
        gl.uniform3fv(Kd_uniform, material_bronze_diffuse); // diffuse reflectivity of material
        gl.uniform3fv(Ks_uniform, material_bronze_specular); // specular reflectivity of material
        gl.uniform1f(material_shininess_uniform, material_bronze_shininess ); // material shininess
        
    }
    modelMatrix=mat4.create();
    viewMatrix=mat4.create();
    mat4.translate(modelMatrix, modelMatrix, [0.0,0.0,-6.0]);
    gl.uniformMatrix4fv(model_matrix_uniform,false,modelMatrix);
    gl.uniformMatrix4fv(view_matrix_uniform,false,viewMatrix);
    gl.uniformMatrix4fv(projection_matrix_uniform,false,perspectiveProjectionMatrix);
    gl.viewport(2*canvas.width/6, 4*canvas.height/6, canvas.width / 6, canvas.height / 6);
    mat4.perspective(perspectiveProjectionMatrix, 45.0, parseFloat(canvas.width/6)/parseFloat(canvas.height/6), 0.1, 100.0);
    sphere.draw();
    
    //23
    if(bLKeyPressed==true){
        gl.uniform3fv(Ka_uniform, material_chrome_ambient); // ambient reflectivity of material
        gl.uniform3fv(Kd_uniform, material_chrome_diffuse); // diffuse reflectivity of material
        gl.uniform3fv(Ks_uniform, material_chrome_specular); // specular reflectivity of material
        gl.uniform1f(material_shininess_uniform, material_chrome_shininess ); // material shininess
        
    }
    modelMatrix=mat4.create();
    viewMatrix=mat4.create();
    mat4.translate(modelMatrix, modelMatrix, [0.0,0.0,-6.0]);
    gl.uniformMatrix4fv(model_matrix_uniform,false,modelMatrix);
    gl.uniformMatrix4fv(view_matrix_uniform,false,viewMatrix);
    gl.uniformMatrix4fv(projection_matrix_uniform,false,perspectiveProjectionMatrix);
    gl.viewport(2*canvas.width/6, 3*canvas.height/6, canvas.width / 6, canvas.height / 6);
    mat4.perspective(perspectiveProjectionMatrix, 45.0, parseFloat(canvas.width/6)/parseFloat(canvas.height/6), 0.1, 100.0);
    sphere.draw();
    
    //24
    if(bLKeyPressed==true){
        gl.uniform3fv(Ka_uniform, material_copper_ambient); // ambient reflectivity of material
        gl.uniform3fv(Kd_uniform, material_copper_diffuse); // diffuse reflectivity of material
        gl.uniform3fv(Ks_uniform, material_copper_specular); // specular reflectivity of material
        gl.uniform1f(material_shininess_uniform, material_copper_shininess ); // material shininess
        
    }
    modelMatrix=mat4.create();
    viewMatrix=mat4.create();
    mat4.translate(modelMatrix, modelMatrix, [0.0,0.0,-6.0]);
    gl.uniformMatrix4fv(model_matrix_uniform,false,modelMatrix);
    gl.uniformMatrix4fv(view_matrix_uniform,false,viewMatrix);
    gl.uniformMatrix4fv(projection_matrix_uniform,false,perspectiveProjectionMatrix);
    gl.viewport(2*canvas.width/6, 2*canvas.height/6, canvas.width / 6, canvas.height / 6);
    mat4.perspective(perspectiveProjectionMatrix, 45.0, parseFloat(canvas.width/6)/parseFloat(canvas.height/6), 0.1, 100.0);
    sphere.draw();
    
    //25
    if(bLKeyPressed==true){
        gl.uniform3fv(Ka_uniform, material_gold_ambient); // ambient reflectivity of material
        gl.uniform3fv(Kd_uniform, material_gold_diffuse); // diffuse reflectivity of material
        gl.uniform3fv(Ks_uniform, material_gold_specular); // specular reflectivity of material
        gl.uniform1f(material_shininess_uniform, material_gold_shininess ); // material shininess
        
    }
    modelMatrix=mat4.create();
    viewMatrix=mat4.create();
    mat4.translate(modelMatrix, modelMatrix, [0.0,0.0,-6.0]);
    gl.uniformMatrix4fv(model_matrix_uniform,false,modelMatrix);
    gl.uniformMatrix4fv(view_matrix_uniform,false,viewMatrix);
    gl.uniformMatrix4fv(projection_matrix_uniform,false,perspectiveProjectionMatrix);
    gl.viewport(2*canvas.width/6, canvas.height/6, canvas.width / 6, canvas.height / 6);
    mat4.perspective(perspectiveProjectionMatrix, 45.0, parseFloat(canvas.width/6)/parseFloat(canvas.height/6), 0.1, 100.0);
    sphere.draw();
    
    //26
    if(bLKeyPressed==true){
        gl.uniform3fv(Ka_uniform, material_silver_ambient); // ambient reflectivity of material
        gl.uniform3fv(Kd_uniform, material_silver_diffuse); // diffuse reflectivity of material
        gl.uniform3fv(Ks_uniform, material_silver_specular); // specular reflectivity of material
        gl.uniform1f(material_shininess_uniform, material_silver_shininess ); // material shininess
        
    }
    modelMatrix=mat4.create();
    viewMatrix=mat4.create();
    mat4.translate(modelMatrix, modelMatrix, [0.0,0.0,-6.0]);
    gl.uniformMatrix4fv(model_matrix_uniform,false,modelMatrix);
    gl.uniformMatrix4fv(view_matrix_uniform,false,viewMatrix);
    gl.uniformMatrix4fv(projection_matrix_uniform,false,perspectiveProjectionMatrix);
    gl.viewport(2*canvas.width/6, 0, canvas.width / 6, canvas.height / 6);
    mat4.perspective(perspectiveProjectionMatrix, 45.0, parseFloat(canvas.width/6)/parseFloat(canvas.height/6), 0.1, 100.0);
    sphere.draw();
    
    //31
    if(bLKeyPressed==true){
        gl.uniform3fv(Ka_uniform, material_black_ambient); // ambient reflectivity of material
        gl.uniform3fv(Kd_uniform, material_black_diffuse); // diffuse reflectivity of material
        gl.uniform3fv(Ks_uniform, material_black_specular); // specular reflectivity of material
        gl.uniform1f(material_shininess_uniform, material_black_shininess ); // material shininess
        
    }
    modelMatrix=mat4.create();
    viewMatrix=mat4.create();
    mat4.translate(modelMatrix, modelMatrix, [0.0,0.0,-6.0]);
    gl.uniformMatrix4fv(model_matrix_uniform,false,modelMatrix);
    gl.uniformMatrix4fv(view_matrix_uniform,false,viewMatrix);
    gl.uniformMatrix4fv(projection_matrix_uniform,false,perspectiveProjectionMatrix);
    gl.viewport(3*canvas.width/6, 5*canvas.height/6, canvas.width / 6, canvas.height / 6);
    mat4.perspective(perspectiveProjectionMatrix, 45.0, parseFloat(canvas.width/6)/parseFloat(canvas.height/6), 0.1, 100.0);
    sphere.draw();
    
    //32
    if(bLKeyPressed==true){
        gl.uniform3fv(Ka_uniform, material_cyan_ambient); // ambient reflectivity of material
        gl.uniform3fv(Kd_uniform, material_cyan_diffuse); // diffuse reflectivity of material
        gl.uniform3fv(Ks_uniform, material_cyan_specular); // specular reflectivity of material
        gl.uniform1f(material_shininess_uniform, material_cyan_shininess ); // material shininess
        
    }
    modelMatrix=mat4.create();
    viewMatrix=mat4.create();
    mat4.translate(modelMatrix, modelMatrix, [0.0,0.0,-6.0]);
    gl.uniformMatrix4fv(model_matrix_uniform,false,modelMatrix);
    gl.uniformMatrix4fv(view_matrix_uniform,false,viewMatrix);
    gl.uniformMatrix4fv(projection_matrix_uniform,false,perspectiveProjectionMatrix);
    gl.viewport(3*canvas.width/6, 4*canvas.height/6, canvas.width / 6, canvas.height / 6);
    mat4.perspective(perspectiveProjectionMatrix, 45.0, parseFloat(canvas.width/6)/parseFloat(canvas.height/6), 0.1, 100.0);
    sphere.draw();
    
    //33
    if(bLKeyPressed==true){
        gl.uniform3fv(Ka_uniform, material_green_ambient); // ambient reflectivity of material
        gl.uniform3fv(Kd_uniform, material_green_diffuse); // diffuse reflectivity of material
        gl.uniform3fv(Ks_uniform, material_green_specular); // specular reflectivity of material
        gl.uniform1f(material_shininess_uniform, material_green_shininess ); // material shininess
        
    }
    modelMatrix=mat4.create();
    viewMatrix=mat4.create();
    mat4.translate(modelMatrix, modelMatrix, [0.0,0.0,-6.0]);
    gl.uniformMatrix4fv(model_matrix_uniform,false,modelMatrix);
    gl.uniformMatrix4fv(view_matrix_uniform,false,viewMatrix);
    gl.uniformMatrix4fv(projection_matrix_uniform,false,perspectiveProjectionMatrix);
    gl.viewport(3*canvas.width/6, 3*canvas.height/6, canvas.width / 6, canvas.height / 6);
    mat4.perspective(perspectiveProjectionMatrix, 45.0, parseFloat(canvas.width/6)/parseFloat(canvas.height/6), 0.1, 100.0);
    sphere.draw();
    
    //34
    if(bLKeyPressed==true){
        gl.uniform3fv(Ka_uniform, material_red_ambient); // ambient reflectivity of material
        gl.uniform3fv(Kd_uniform, material_red_diffuse); // diffuse reflectivity of material
        gl.uniform3fv(Ks_uniform, material_red_specular); // specular reflectivity of material
        gl.uniform1f(material_shininess_uniform, material_red_shininess ); // material shininess
        
    }
    modelMatrix=mat4.create();
    viewMatrix=mat4.create();
    mat4.translate(modelMatrix, modelMatrix, [0.0,0.0,-6.0]);
    gl.uniformMatrix4fv(model_matrix_uniform,false,modelMatrix);
    gl.uniformMatrix4fv(view_matrix_uniform,false,viewMatrix);
    gl.uniformMatrix4fv(projection_matrix_uniform,false,perspectiveProjectionMatrix);
    gl.viewport(3*canvas.width/6, 2*canvas.height/6, canvas.width / 6, canvas.height / 6);
    mat4.perspective(perspectiveProjectionMatrix, 45.0, parseFloat(canvas.width/6)/parseFloat(canvas.height/6), 0.1, 100.0);
    sphere.draw();
    
    //35
    if(bLKeyPressed==true){
        gl.uniform3fv(Ka_uniform, material_white_ambient); // ambient reflectivity of material
        gl.uniform3fv(Kd_uniform, material_white_diffuse); // diffuse reflectivity of material
        gl.uniform3fv(Ks_uniform, material_white_specular); // specular reflectivity of material
        gl.uniform1f(material_shininess_uniform, material_white_shininess ); // material shininess
        
    }
    modelMatrix=mat4.create();
    viewMatrix=mat4.create();
    mat4.translate(modelMatrix, modelMatrix, [0.0,0.0,-6.0]);
    gl.uniformMatrix4fv(model_matrix_uniform,false,modelMatrix);
    gl.uniformMatrix4fv(view_matrix_uniform,false,viewMatrix);
    gl.uniformMatrix4fv(projection_matrix_uniform,false,perspectiveProjectionMatrix);
    gl.viewport(3*canvas.width/6, canvas.height/6, canvas.width / 6, canvas.height / 6);
    mat4.perspective(perspectiveProjectionMatrix, 45.0, parseFloat(canvas.width/6)/parseFloat(canvas.height/6), 0.1, 100.0);
    sphere.draw();
    
    //36
    if(bLKeyPressed==true){
        gl.uniform3fv(Ka_uniform, material_yellow_ambient); // ambient reflectivity of material
        gl.uniform3fv(Kd_uniform, material_yellow_diffuse); // diffuse reflectivity of material
        gl.uniform3fv(Ks_uniform, material_yellow_specular); // specular reflectivity of material
        gl.uniform1f(material_shininess_uniform, material_yellow_shininess ); // material shininess
        
    }
    modelMatrix=mat4.create();
    viewMatrix=mat4.create();
    mat4.translate(modelMatrix, modelMatrix, [0.0,0.0,-6.0]);
    gl.uniformMatrix4fv(model_matrix_uniform,false,modelMatrix);
    gl.uniformMatrix4fv(view_matrix_uniform,false,viewMatrix);
    gl.uniformMatrix4fv(projection_matrix_uniform,false,perspectiveProjectionMatrix);
    gl.viewport(3*canvas.width/6, 0, canvas.width / 6, canvas.height / 6);
    mat4.perspective(perspectiveProjectionMatrix, 45.0, parseFloat(canvas.width/6)/parseFloat(canvas.height/6), 0.1, 100.0);
    sphere.draw();
    
    //41
    if(bLKeyPressed==true){
        gl.uniform3fv(Ka_uniform, material_black2_ambient); // ambient reflectivity of material
        gl.uniform3fv(Kd_uniform, material_black2_diffuse); // diffuse reflectivity of material
        gl.uniform3fv(Ks_uniform, material_black2_specular); // specular reflectivity of material
        gl.uniform1f(material_shininess_uniform, material_black2_shininess ); // material shininess
        
    }
    modelMatrix=mat4.create();
    viewMatrix=mat4.create();
    mat4.translate(modelMatrix, modelMatrix, [0.0,0.0,-6.0]);
    gl.uniformMatrix4fv(model_matrix_uniform,false,modelMatrix);
    gl.uniformMatrix4fv(view_matrix_uniform,false,viewMatrix);
    gl.uniformMatrix4fv(projection_matrix_uniform,false,perspectiveProjectionMatrix);
    gl.viewport(4*canvas.width/6, 5*canvas.height/6, canvas.width / 6, canvas.height / 6);
    mat4.perspective(perspectiveProjectionMatrix, 45.0, parseFloat(canvas.width/6)/parseFloat(canvas.height/6), 0.1, 100.0);
    sphere.draw();
    
    //42
    if(bLKeyPressed==true){
        gl.uniform3fv(Ka_uniform, material_cyan2_ambient); // ambient reflectivity of material
        gl.uniform3fv(Kd_uniform, material_cyan2_diffuse); // diffuse reflectivity of material
        gl.uniform3fv(Ks_uniform, material_cyan2_specular); // specular reflectivity of material
        gl.uniform1f(material_shininess_uniform, material_cyan2_shininess ); // material shininess
        
    }
    modelMatrix=mat4.create();
    viewMatrix=mat4.create();
    mat4.translate(modelMatrix, modelMatrix, [0.0,0.0,-6.0]);
    gl.uniformMatrix4fv(model_matrix_uniform,false,modelMatrix);
    gl.uniformMatrix4fv(view_matrix_uniform,false,viewMatrix);
    gl.uniformMatrix4fv(projection_matrix_uniform,false,perspectiveProjectionMatrix);
    gl.viewport(4*canvas.width/6, 4*canvas.height/6, canvas.width / 6, canvas.height / 6);
    mat4.perspective(perspectiveProjectionMatrix, 45.0, parseFloat(canvas.width/6)/parseFloat(canvas.height/6), 0.1, 100.0);
    sphere.draw();
    
    //43
    if(bLKeyPressed==true){
        gl.uniform3fv(Ka_uniform, material_green2_ambient); // ambient reflectivity of material
        gl.uniform3fv(Kd_uniform, material_green2_diffuse); // diffuse reflectivity of material
        gl.uniform3fv(Ks_uniform, material_green2_specular); // specular reflectivity of material
        gl.uniform1f(material_shininess_uniform, material_green2_shininess ); // material shininess
        
    }
    modelMatrix=mat4.create();
    viewMatrix=mat4.create();
    mat4.translate(modelMatrix, modelMatrix, [0.0,0.0,-6.0]);
    gl.uniformMatrix4fv(model_matrix_uniform,false,modelMatrix);
    gl.uniformMatrix4fv(view_matrix_uniform,false,viewMatrix);
    gl.uniformMatrix4fv(projection_matrix_uniform,false,perspectiveProjectionMatrix);
    gl.viewport(4*canvas.width/6, 3*canvas.height/6, canvas.width / 6, canvas.height / 6);
    mat4.perspective(perspectiveProjectionMatrix, 45.0, parseFloat(canvas.width/6)/parseFloat(canvas.height/6), 0.1, 100.0);
    sphere.draw();
    
    //44
    if(bLKeyPressed==true){
        gl.uniform3fv(Ka_uniform, material_red2_ambient); // ambient reflectivity of material
        gl.uniform3fv(Kd_uniform, material_red2_diffuse); // diffuse reflectivity of material
        gl.uniform3fv(Ks_uniform, material_red2_specular); // specular reflectivity of material
        gl.uniform1f(material_shininess_uniform, material_red2_shininess ); // material shininess
        
    }
    modelMatrix=mat4.create();
    viewMatrix=mat4.create();
    mat4.translate(modelMatrix, modelMatrix, [0.0,0.0,-6.0]);
    gl.uniformMatrix4fv(model_matrix_uniform,false,modelMatrix);
    gl.uniformMatrix4fv(view_matrix_uniform,false,viewMatrix);
    gl.uniformMatrix4fv(projection_matrix_uniform,false,perspectiveProjectionMatrix);
    gl.viewport(4*canvas.width/6, 2*canvas.height/6, canvas.width / 6, canvas.height / 6);
    mat4.perspective(perspectiveProjectionMatrix, 45.0, parseFloat(canvas.width/6)/parseFloat(canvas.height/6), 0.1, 100.0);
    sphere.draw();
    
    //45
    if(bLKeyPressed==true){
        gl.uniform3fv(Ka_uniform, material_white2_ambient); // ambient reflectivity of material
        gl.uniform3fv(Kd_uniform, material_white2_diffuse); // diffuse reflectivity of material
        gl.uniform3fv(Ks_uniform, material_white2_specular); // specular reflectivity of material
        gl.uniform1f(material_shininess_uniform, material_white2_shininess ); // material shininess
        
    }
    modelMatrix=mat4.create();
    viewMatrix=mat4.create();
    mat4.translate(modelMatrix, modelMatrix, [0.0,0.0,-6.0]);
    gl.uniformMatrix4fv(model_matrix_uniform,false,modelMatrix);
    gl.uniformMatrix4fv(view_matrix_uniform,false,viewMatrix);
    gl.uniformMatrix4fv(projection_matrix_uniform,false,perspectiveProjectionMatrix);
    gl.viewport(4*canvas.width/6, canvas.height/6, canvas.width / 6, canvas.height / 6);
    mat4.perspective(perspectiveProjectionMatrix, 45.0, parseFloat(canvas.width/6)/parseFloat(canvas.height/6), 0.1, 100.0);
    sphere.draw();
    
    //46
    if(bLKeyPressed==true){
        gl.uniform3fv(Ka_uniform, material_yellow2_ambient); // ambient reflectivity of material
        gl.uniform3fv(Kd_uniform, material_yellow2_diffuse); // diffuse reflectivity of material
        gl.uniform3fv(Ks_uniform, material_yellow2_specular); // specular reflectivity of material
        gl.uniform1f(material_shininess_uniform, material_yellow2_shininess ); // material shininess
        
    }
    modelMatrix=mat4.create();
    viewMatrix=mat4.create();
    mat4.translate(modelMatrix, modelMatrix, [0.0,0.0,-6.0]);
    gl.uniformMatrix4fv(model_matrix_uniform,false,modelMatrix);
    gl.uniformMatrix4fv(view_matrix_uniform,false,viewMatrix);
    gl.uniformMatrix4fv(projection_matrix_uniform,false,perspectiveProjectionMatrix);
    gl.viewport(4*canvas.width/6, 0, canvas.width / 6, canvas.height / 6);
    mat4.perspective(perspectiveProjectionMatrix, 45.0, parseFloat(canvas.width/6)/parseFloat(canvas.height/6), 0.1, 100.0);
    sphere.draw();
    gl.useProgram(null);
    update();
    // animation loop
    requestAnimationFrame(draw, canvas);
}

function update() {
    lightAngle = (lightAngle + 0.2);
    if (lightAngle >= 360.0)
        lightAngle = 0.0;
}

function uninitialize()
{
    // code
   if(sphere)
    {
        sphere.deallocate();
        sphere=null;
    }
    
    if(gShaderProgramObject)
    {
        if(perFragment_FragmentShaderObject)
        {
            gl.detachShader(gShaderProgramObject,perFragment_FragmentShaderObject);
            gl.deleteShader(perFragment_FragmentShaderObject);
            perFragment_FragmentShaderObject=null;
        }
        
        if(perFragment_VertexShaderObject)
        {
            gl.detachShader(gShaderProgramObject,perFragment_VertexShaderObject);
            gl.deleteShader(perFragment_VertexShaderObject);
            perFragment_VertexShaderObject=null;
        }
        
        gl.deleteProgram(gShaderProgramObject);
        gShaderProgramObject=null;
    }
}

function keyDown(event)
{
    // code
    switch(event.keyCode)
    {
        case 27: // Escape
            // uninitialize
            uninitialize();
            // close our application's tab
            window.close(); // may not work in Firefox but works in Safari and chrome
            break;
        case 76: // for 'L' or 'l'
            if(bLKeyPressed==false)
                bLKeyPressed=true;
            else
                bLKeyPressed=false;
            break;
        case 70: // for 'F' or 'f'
            toggleFullScreen();
            break;
        case 88: axisIdentifier = 0;
            light_position[0] = 0.0;
            light_position[1] = 0.0;
            light_position[2] = -6.0;
            lightAngle = 0.0;
            break;
        case 89:
            axisIdentifier = 1;
            light_position[0] = 0.0;
            light_position[1] = 0.0;
            light_position[2] = -6.0;
            lightAngle = 0.0;
            break;
        case 90:
            axisIdentifier = 2;
            light_position[0] = 0.0;
            light_position[1] = 0.0;
            light_position[2] = 0.0;
            lightAngle = 0.0;
            break;
        default:
            break;
    }
}

function mouseDown()
{
    // code
}
