// global variables
var canvas=null;
var gl=null; // webgl context
var bFullscreen=false;
var canvas_original_width;
var canvas_original_height;

const WebGLMacros= // when whole 'WebGLMacros' is 'const', all inside it are automatically 'const'
{
VDG_ATTRIBUTE_VERTEX:0,
VDG_ATTRIBUTE_COLOR:1,
VDG_ATTRIBUTE_NORMAL:2,
VDG_ATTRIBUTE_TEXTURE0:3,
};

var perVertex_VertexShaderObject;
var perVertex_FragmentShaderObject;
var perVertex_ShaderProgramObject;

var perFragment_VertexShaderObject;
var perFragment_FragmentShaderObject;
var perFragment_ShaderProgramObject;

var light_ambient=[0.0,0.0,0.0];
var light_diffuse=[1.0,1.0,1.0];
var light_specular=[1.0,1.0,1.0];
var light_position=[100.0,100.0,100.0,1.0];

var material_ambient= [0.0,0.0,0.0];
var material_diffuse= [1.0,1.0,1.0];
var material_specular= [1.0,1.0,1.0];
var material_shininess= 50.0;

var sphere=null;
var shaderMode = 0;
var perspectiveProjectionMatrix;

var perVertex_ModelMatrixUniform, perVertex_ViewMatrixUniform, perVertex_ProjectionMatrixUniform;
var perVertex_LaUniform, perVertex_LdUniform, perVertex_LsUniform, perVertex_LightPositionUniform;
var perVertex_KaUniform, perVertex_KdUniform, perVertex_KsUniform, perVertex_MaterialShininessUniform;
var perVertex_LKeyPressedUniform;

var perFragment_ModelMatrixUniform, perFragment_ViewMatrixUniform, perFragment_ProjectionMatrixUniform;
var perFragment_LaUniform, perFragment_LdUniform, perFragment_LsUniform, perFragment_LightPositionUniform;
var perFragment_KaUniform, perFragment_KdUniform, perFragment_KsUniform, perFragment_MaterialShininessUniform;
var perFragment_LKeyPressedUniform;

var bLKeyPressed=false;

// To start animation : To have requestAnimationFrame() to be called "cross-browser" compatible
var requestAnimationFrame =
window.requestAnimationFrame ||
window.webkitRequestAnimationFrame ||
window.mozRequestAnimationFrame ||
window.oRequestAnimationFrame ||
window.msRequestAnimationFrame;

// To stop animation : To have cancelAnimationFrame() to be called "cross-browser" compatible
var cancelAnimationFrame =
window.cancelAnimationFrame ||
window.webkitCancelRequestAnimationFrame || window.webkitCancelAnimationFrame ||
window.mozCancelRequestAnimationFrame || window.mozCancelAnimationFrame ||
window.oCancelRequestAnimationFrame || window.oCancelAnimationFrame ||
window.msCancelRequestAnimationFrame || window.msCancelAnimationFrame;

// onload function
function main()
{
    // get <canvas> element
    canvas = document.getElementById("AMC");
    if(!canvas)
        console.log("Obtaining Canvas Failed\n");
    else
        console.log("Obtaining Canvas Succeeded\n");
    canvas_original_width=canvas.width;
    canvas_original_height=canvas.height;
    
    // register keyboard's keydown event handler
    window.addEventListener("keydown", keyDown, false);
    window.addEventListener("click", mouseDown, false);
    window.addEventListener("resize", resize, false);

    // initialize WebGL
    init();
    
    // start drawing here as warming-up
    resize();
    draw();
}

function toggleFullScreen()
{
    // code
    var fullscreen_element =
    document.fullscreenElement ||
    document.webkitFullscreenElement ||
    document.mozFullScreenElement ||
    document.msFullscreenElement ||
    null;

    // if not fullscreen
    if(fullscreen_element==null)
    {
        if(canvas.requestFullscreen)
            canvas.requestFullscreen();
        else if(canvas.mozRequestFullScreen)
            canvas.mozRequestFullScreen();
        else if(canvas.webkitRequestFullscreen)
            canvas.webkitRequestFullscreen();
        else if(canvas.msRequestFullscreen)
            canvas.msRequestFullscreen();
        bFullscreen=true;
    }
    else // if already fullscreen
    {
        if(document.exitFullscreen)
            document.exitFullscreen();
        else if(document.mozCancelFullScreen)
            document.mozCancelFullScreen();
        else if(document.webkitExitFullscreen)
            document.webkitExitFullscreen();
        else if(document.msExitFullscreen)
            document.msExitFullscreen();
        bFullscreen=false;
    }
}

function init()
{
    // code
    // get WebGL 2.0 context
    gl = canvas.getContext("webgl2");
    if(gl==null) // failed to get context
    {
        console.log("Failed to get the rendering context for WebGL");
        return;
    }
    gl.viewportWidth = canvas.width;
    gl.viewportHeight = canvas.height;
   
    // vertex shader
    var vertexShaderSourceCode=
    "#version 300 es"+
    "\n"+
    "in vec4 vPosition;"+
    "in vec3 vNormal;"+
    "uniform mat4 u_model_matrix;"+
    "uniform mat4 u_view_matrix;"+
    "uniform mat4 u_projection_matrix;"+
    "uniform mediump int u_LKeyPressed;"+
    "uniform vec3 u_La;"+
    "uniform vec3 u_Ld;"+
    "uniform vec3 u_Ls;"+
    "uniform vec4 u_light_position;"+
    "uniform vec3 u_Ka;"+
    "uniform vec3 u_Kd;"+
    "uniform vec3 u_Ks;"+
    "uniform float u_material_shininess;"+
    "out vec3 phong_ads_color;"+
    "void main(void)"+
    "{"+
    "if(u_LKeyPressed == 1)"+
    "{"+
    "vec4 eye_coordinates=u_view_matrix * u_model_matrix * vPosition;"+
    "vec3 transformed_normals=normalize(mat3(u_view_matrix * u_model_matrix) * vNormal);"+
    "vec3 light_direction = normalize(vec3(u_light_position) - eye_coordinates.xyz);"+
    "float tn_dot_ld = max(dot(transformed_normals, light_direction),0.0);"+
    "vec3 ambient = u_La * u_Ka;"+
    "vec3 diffuse = u_Ld * u_Kd * tn_dot_ld;"+
    "vec3 reflection_vector = reflect(-light_direction, transformed_normals);"+
    "vec3 viewer_vector = normalize(-eye_coordinates.xyz);"+
    "vec3 specular = u_Ls * u_Ks * pow(max(dot(reflection_vector, viewer_vector), 0.0), u_material_shininess);"+
    "phong_ads_color=ambient + diffuse + specular;"+
    "}"+
    "else"+
    "{"+
    "phong_ads_color = vec3(1.0, 1.0, 1.0);"+
    "}"+
    "gl_Position=u_projection_matrix * u_view_matrix * u_model_matrix * vPosition;"+
    "}";

    perVertex_VertexShaderObject=gl.createShader(gl.VERTEX_SHADER);
    gl.shaderSource(perVertex_VertexShaderObject,vertexShaderSourceCode);
    gl.compileShader(perVertex_VertexShaderObject);
    if(gl.getShaderParameter(perVertex_VertexShaderObject,gl.COMPILE_STATUS)==false)
    {
        var error=gl.getShaderInfoLog(perVertex_VertexShaderObject);
        if(error.length > 0)
        {
            alert(error);
            uninitialize();
        }
    }
    
    // fragment shader
    var fragmentShaderSourceCode=
    "#version 300 es"+
    "\n"+
    "precision highp float;"+
    "in vec3 phong_ads_color;"+
    "out vec4 FragColor;"+
    "void main(void)"+
    "{"+
    "FragColor = vec4(phong_ads_color, 1.0);"+
    "}";
    
    perVertex_FragmentShaderObject=gl.createShader(gl.FRAGMENT_SHADER);
    gl.shaderSource(perVertex_FragmentShaderObject,fragmentShaderSourceCode);
    gl.compileShader(perVertex_FragmentShaderObject);
    if(gl.getShaderParameter(perVertex_FragmentShaderObject,gl.COMPILE_STATUS)==false)
    {
        var error=gl.getShaderInfoLog(perVertex_FragmentShaderObject);
        if(error.length > 0)
        {
            alert(error);
            uninitialize();
        }
    }
    
    // shader program
    perVertex_ShaderProgramObject=gl.createProgram();
    gl.attachShader(perVertex_ShaderProgramObject,perVertex_VertexShaderObject);
    gl.attachShader(perVertex_ShaderProgramObject,perVertex_FragmentShaderObject);
    
    // pre-link binding of shader program object with vertex shader attributes
    gl.bindAttribLocation(perVertex_ShaderProgramObject,WebGLMacros.VDG_ATTRIBUTE_VERTEX,"vPosition");
    gl.bindAttribLocation(perVertex_ShaderProgramObject,WebGLMacros.VDG_ATTRIBUTE_NORMAL,"vNormal");

    // linking
    gl.linkProgram(perVertex_ShaderProgramObject);
    if (!gl.getProgramParameter(perVertex_ShaderProgramObject, gl.LINK_STATUS))
    {
        var error=gl.getProgramInfoLog(perVertex_ShaderProgramObject);
        if(error.length > 0)
        {
            alert(error);
            uninitialize();
        }
    }

    // get Model Matrix uniform location
    perVertex_ModelMatrixUniform=gl.getUniformLocation(perVertex_ShaderProgramObject,"u_model_matrix");
    // get View Matrix uniform location
    perVertex_ViewMatrixUniform=gl.getUniformLocation(perVertex_ShaderProgramObject,"u_view_matrix");
    // get Projection Matrix uniform location
    perVertex_ProjectionMatrixUniform=gl.getUniformLocation(perVertex_ShaderProgramObject,"u_projection_matrix");
    
    // get single tap detecting uniform
    perVertex_LKeyPressedUniform=gl.getUniformLocation(perVertex_ShaderProgramObject,"u_LKeyPressed");
    
    // ambient color intensity of light
    perVertex_LaUniform=gl.getUniformLocation(perVertex_ShaderProgramObject,"u_La");
    // diffuse color intensity of light
    perVertex_LdUniform=gl.getUniformLocation(perVertex_ShaderProgramObject,"u_Ld");
    // specular color intensity of light
    perVertex_LsUniform=gl.getUniformLocation(perVertex_ShaderProgramObject,"u_Ls");
    // position of light
    perVertex_LightPositionUniform=gl.getUniformLocation(perVertex_ShaderProgramObject,"u_light_position");
    
    // ambient reflective color intensity of material
    perVertex_KaUniform=gl.getUniformLocation(perVertex_ShaderProgramObject,"u_Ka");
    // diffuse reflective color intensity of material
    perVertex_KdUniform=gl.getUniformLocation(perVertex_ShaderProgramObject,"u_Kd");
    // specular reflective color intensity of material
    perVertex_KsUniform=gl.getUniformLocation(perVertex_ShaderProgramObject,"u_Ks");
    // shininess of material ( value is conventionally between 1 to 200 )
    perVertex_MaterialShininessUniform=gl.getUniformLocation(perVertex_ShaderProgramObject,"u_material_shininess");
	
	/****************************************** per fragment ******************************************/
	var vertexShaderSourceCode=
    "#version 300 es"+
    "\n"+
    "in vec4 vPosition;"+
    "in vec3 vNormal;"+
    "uniform mat4 u_model_matrix;"+
    "uniform mat4 u_view_matrix;"+
    "uniform mat4 u_projection_matrix;"+
    "uniform mediump int u_LKeyPressed;"+
    "uniform vec4 u_light_position;"+
    "out vec3 transformed_normals;"+
    "out vec3 light_direction;"+
    "out vec3 viewer_vector;"+
    "void main(void)"+
    "{"+
    "if(u_LKeyPressed == 1)"+
    "{"+
    "vec4 eye_coordinates=u_view_matrix * u_model_matrix * vPosition;"+
    "transformed_normals=mat3(u_view_matrix * u_model_matrix) * vNormal;"+
    "light_direction = vec3(u_light_position) - eye_coordinates.xyz;"+
    "viewer_vector = -eye_coordinates.xyz;"+
    "}"+
    "gl_Position=u_projection_matrix * u_view_matrix * u_model_matrix * vPosition;"+
    "}";

    perFragment_VertexShaderObject=gl.createShader(gl.VERTEX_SHADER);
    gl.shaderSource(perFragment_VertexShaderObject,vertexShaderSourceCode);
    gl.compileShader(perFragment_VertexShaderObject);
    if(gl.getShaderParameter(perFragment_VertexShaderObject,gl.COMPILE_STATUS)==false)
    {
        var error=gl.getShaderInfoLog(perFragment_VertexShaderObject);
        if(error.length > 0)
        {
            alert(error);
            uninitialize();
        }
    }
    
    // fragment shader
    var fragmentShaderSourceCode=
    "#version 300 es"+
    "\n"+
    "precision highp float;"+
    "in vec3 transformed_normals;"+
    "in vec3 light_direction;"+
    "in vec3 viewer_vector;"+
    "out vec4 FragColor;"+
    "uniform vec3 u_La;"+
    "uniform vec3 u_Ld;"+
    "uniform vec3 u_Ls;"+
    "uniform vec3 u_Ka;"+
    "uniform vec3 u_Kd;"+
    "uniform vec3 u_Ks;"+
    "uniform float u_material_shininess;"+
    "uniform int u_LKeyPressed;"+
    "void main(void)"+
    "{"+
    "vec3 phong_ads_color;"+
    "if(u_LKeyPressed == 1)"+
    "{"+
    "vec3 normalized_transformed_normals=normalize(transformed_normals);"+
    "vec3 normalized_light_direction=normalize(light_direction);"+
    "vec3 normalized_viewer_vector=normalize(viewer_vector);"+
    "vec3 ambient = u_La * u_Ka;"+
    "float tn_dot_ld = max(dot(normalized_transformed_normals, normalized_light_direction),0.0);"+
    "vec3 diffuse = u_Ld * u_Kd * tn_dot_ld;"+
    "vec3 reflection_vector = reflect(-normalized_light_direction, normalized_transformed_normals);"+
    "vec3 specular = u_Ls * u_Ks * pow(max(dot(reflection_vector, normalized_viewer_vector), 0.0), u_material_shininess);"+
    "phong_ads_color=ambient + diffuse + specular;"+
    "}"+
    "else"+
    "{"+
    "phong_ads_color = vec3(1.0, 1.0, 1.0);"+
    "}"+
    "FragColor = vec4(phong_ads_color, 1.0);"+
    "}"
    
    perFragment_FragmentShaderObject=gl.createShader(gl.FRAGMENT_SHADER);
    gl.shaderSource(perFragment_FragmentShaderObject,fragmentShaderSourceCode);
    gl.compileShader(perFragment_FragmentShaderObject);
    if(gl.getShaderParameter(perFragment_FragmentShaderObject,gl.COMPILE_STATUS)==false)
    {
        var error=gl.getShaderInfoLog(perFragment_FragmentShaderObject);
        if(error.length > 0)
        {
            alert(error);
            uninitialize();
        }
    }
    
    // shader program
    perFragment_ShaderProgramObject=gl.createProgram();
    gl.attachShader(perFragment_ShaderProgramObject,perFragment_VertexShaderObject);
    gl.attachShader(perFragment_ShaderProgramObject,perFragment_FragmentShaderObject);
    
    // pre-link binding of shader program object with vertex shader attributes
    gl.bindAttribLocation(perFragment_ShaderProgramObject,WebGLMacros.VDG_ATTRIBUTE_VERTEX,"vPosition");
    gl.bindAttribLocation(perFragment_ShaderProgramObject,WebGLMacros.VDG_ATTRIBUTE_NORMAL,"vNormal");

    // linking
    gl.linkProgram(perFragment_ShaderProgramObject);
    if (!gl.getProgramParameter(perFragment_ShaderProgramObject, gl.LINK_STATUS))
    {
        var error=gl.getProgramInfoLog(perFragment_ShaderProgramObject);
        if(error.length > 0)
        {
            alert(error);
            uninitialize();
        }
    }

    // get Model Matrix uniform location
    perFragment_ModelMatrixUniform=gl.getUniformLocation(perFragment_ShaderProgramObject,"u_model_matrix");
    // get View Matrix uniform location
    perFragment_ViewMatrixUniform=gl.getUniformLocation(perFragment_ShaderProgramObject,"u_view_matrix");
    // get Projection Matrix uniform location
    perFragment_ProjectionMatrixUniform=gl.getUniformLocation(perFragment_ShaderProgramObject,"u_projection_matrix");
    
    // get single tap detecting uniform
    perFragment_LKeyPressedUniform=gl.getUniformLocation(perFragment_ShaderProgramObject,"u_LKeyPressed");
    
    // ambient color intensity of light
    perFragment_LaUniform=gl.getUniformLocation(perFragment_ShaderProgramObject,"u_La");
    // diffuse color intensity of light
    perFragment_LdUniform=gl.getUniformLocation(perFragment_ShaderProgramObject,"u_Ld");
    // specular color intensity of light
    perFragment_LsUniform=gl.getUniformLocation(perFragment_ShaderProgramObject,"u_Ls");
    // position of light
    perFragment_LightPositionUniform=gl.getUniformLocation(perFragment_ShaderProgramObject,"u_light_position");
    
    // ambient reflective color intensity of material
    perFragment_KaUniform=gl.getUniformLocation(perFragment_ShaderProgramObject,"u_Ka");
    // diffuse reflective color intensity of material
    perFragment_KdUniform=gl.getUniformLocation(perFragment_ShaderProgramObject,"u_Kd");
    // specular reflective color intensity of material
    perFragment_KsUniform=gl.getUniformLocation(perFragment_ShaderProgramObject,"u_Ks");
    // shininess of material ( value is conventionally between 1 to 200 )
    perFragment_MaterialShininessUniform=gl.getUniformLocation(perFragment_ShaderProgramObject,"u_material_shininess");

    // linking
    gl.linkProgram(perFragment_ShaderProgramObject);
    if (!gl.getProgramParameter(perFragment_ShaderProgramObject, gl.LINK_STATUS))
    {
        var error=gl.getProgramInfoLog(perFragment_ShaderProgramObject);
        if(error.length > 0)
        {
            alert(error);
            uninitialize();
        }
    }

    // get Model Matrix uniform location
    perFragment_ModelMatrixUniform=gl.getUniformLocation(perFragment_ShaderProgramObject,"u_model_matrix");
    // get View Matrix uniform location
    perFragment_ViewMatrixUniform=gl.getUniformLocation(perFragment_ShaderProgramObject,"u_view_matrix");
    // get Projection Matrix uniform location
    perFragment_ProjectionMatrixUniform=gl.getUniformLocation(perFragment_ShaderProgramObject,"u_projection_matrix");
    
    // get single tap detecting uniform
    perFragment_LKeyPressedUniform=gl.getUniformLocation(perFragment_ShaderProgramObject,"u_LKeyPressed");
    
    // ambient color intensity of light
    perFragment_LaUniform=gl.getUniformLocation(perFragment_ShaderProgramObject,"u_La");
    // diffuse color intensity of light
    perFragment_LdUniform=gl.getUniformLocation(perFragment_ShaderProgramObject,"u_Ld");
    // specular color intensity of light
    perFragment_LsUniform=gl.getUniformLocation(perFragment_ShaderProgramObject,"u_Ls");
    // position of light
    perFragment_LightPositionUniform=gl.getUniformLocation(perFragment_ShaderProgramObject,"u_light_position");
    
    // ambient reflective color intensity of material
    perFragment_KaUniform=gl.getUniformLocation(perFragment_ShaderProgramObject,"u_Ka");
    // diffuse reflective color intensity of material
    perFragment_KdUniform=gl.getUniformLocation(perFragment_ShaderProgramObject,"u_Kd");
    // specular reflective color intensity of material
    perFragment_KsUniform=gl.getUniformLocation(perFragment_ShaderProgramObject,"u_Ks");
    // shininess of material ( value is conventionally between 1 to 200 )
    perFragment_MaterialShininessUniform=gl.getUniformLocation(perFragment_ShaderProgramObject,"u_material_shininess");
    
    // *** vertices, colors, shader attribs, vbo, vao initializations ***
    sphere=new Mesh();
    makeSphere(sphere,2.0,30,30);

    // Depth test will always be enabled
    gl.enable(gl.DEPTH_TEST);
    
    // depth test to do
    gl.depthFunc(gl.LEQUAL);
    
    // We will always cull back faces for better performance
    gl.enable(gl.CULL_FACE);
    
    // set clear color
    gl.clearColor(0.0, 0.0, 0.0, 1.0); // black
    
    // initialize projection matrix
    perspectiveProjectionMatrix=mat4.create();
}

function resize()
{
    // code
    if(bFullscreen==true)
    {
        canvas.width=window.innerWidth;
        canvas.height=window.innerHeight;
    }
    else
    {
        canvas.width=canvas_original_width;
        canvas.height=canvas_original_height;
    }
   
    // set the viewport to match
    gl.viewport(0, 0, canvas.width, canvas.height);
    
    mat4.perspective(perspectiveProjectionMatrix, 45.0, parseFloat(canvas.width)/parseFloat(canvas.height), 0.1, 100.0);
}

function draw()
{
    // code
    gl.clear(gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT);
    
    if(shaderMode == 1){
		 gl.useProgram(perVertex_ShaderProgramObject);
    
    if(bLKeyPressed==true)
    {
        gl.uniform1i(perVertex_LKeyPressedUniform, 1);
        
        // setting light properties
        gl.uniform3fv(perVertex_LaUniform, light_ambient); // ambient intensity of light
        gl.uniform3fv(perVertex_LdUniform, light_diffuse); // diffuse intensity of light
        gl.uniform3fv(perVertex_LsUniform, light_specular); // specular intensity of light
        gl.uniform4fv(perVertex_LightPositionUniform, light_position); // light position
        
        // setting material properties
        gl.uniform3fv(perVertex_KaUniform, material_ambient); // ambient reflectivity of material
        gl.uniform3fv(perVertex_KdUniform, material_diffuse); // diffuse reflectivity of material
        gl.uniform3fv(perVertex_KsUniform, material_specular); // specular reflectivity of material
        gl.uniform1f(perVertex_MaterialShininessUniform, material_shininess); // material shininess
    }
    else
    {
        gl.uniform1i(perVertex_LKeyPressedUniform, 0);
    }
    
    var modelMatrix=mat4.create();
    var viewMatrix=mat4.create();

    mat4.translate(modelMatrix, modelMatrix, [0.0,0.0,-6.0]);
    
    gl.uniformMatrix4fv(perVertex_ModelMatrixUniform,false,modelMatrix);
    gl.uniformMatrix4fv(perVertex_ViewMatrixUniform,false,viewMatrix);
    gl.uniformMatrix4fv(perVertex_ProjectionMatrixUniform,false,perspectiveProjectionMatrix);
    
    sphere.draw();
	}else{
		gl.useProgram(perFragment_ShaderProgramObject);
    
    if(bLKeyPressed==true)
    {
        gl.uniform1i(perFragment_LKeyPressedUniform, 1);
        
        // setting light properties
        gl.uniform3fv(perFragment_LaUniform, light_ambient); // ambient intensity of light
        gl.uniform3fv(perFragment_LdUniform, light_diffuse); // diffuse intensity of light
        gl.uniform3fv(perFragment_LsUniform, light_specular); // specular intensity of light
        gl.uniform4fv(perFragment_LightPositionUniform, light_position); // light position
        
        // setting material properties
        gl.uniform3fv(perFragment_KaUniform, material_ambient); // ambient reflectivity of material
        gl.uniform3fv(perFragment_KdUniform, material_diffuse); // diffuse reflectivity of material
        gl.uniform3fv(perFragment_KsUniform, material_specular); // specular reflectivity of material
        gl.uniform1f(perFragment_MaterialShininessUniform, material_shininess); // material shininess
    }
    else
    {
        gl.uniform1i(perFragment_LKeyPressedUniform, 0);
    }
    
    var modelMatrix=mat4.create();
    var viewMatrix=mat4.create();

    mat4.translate(modelMatrix, modelMatrix, [0.0,0.0,-6.0]);
    
    gl.uniformMatrix4fv(perFragment_ModelMatrixUniform,false,modelMatrix);
    gl.uniformMatrix4fv(perFragment_ViewMatrixUniform,false,viewMatrix);
    gl.uniformMatrix4fv(perFragment_ProjectionMatrixUniform,false,perspectiveProjectionMatrix);
    
    sphere.draw();
	}
   
    gl.useProgram(null);
    
    // animation loop
    requestAnimationFrame(draw, canvas);
}

function uninitialize()
{
    // code
    if(sphere)
    {
        sphere.deallocate();
        sphere=null;
    }
    
    if(perVertex_ShaderProgramObject)
    {
        if(perVertex_FragmentShaderObject)
        {
            gl.detachShader(perVertex_ShaderProgramObject,perVertex_FragmentShaderObject);
            gl.deleteShader(perVertex_FragmentShaderObject);
            perVertex_FragmentShaderObject=null;
        }
        
        if(perVertex_VertexShaderObject)
        {
            gl.detachShader(perVertex_ShaderProgramObject,perVertex_VertexShaderObject);
            gl.deleteShader(perVertex_VertexShaderObject);
            perVertex_VertexShaderObject=null;
        }
        
        gl.deleteProgram(perVertex_ShaderProgramObject);
        perVertex_ShaderProgramObject=null;
    }
}

function keyDown(event)
{
	
    // code
    switch(event.keyCode)
    {
        case 27: // Escape
            toggleFullScreen();
			break;
        case 76: // for 'L' or 'l'
            if(bLKeyPressed==false)
                bLKeyPressed=true;
            else
                bLKeyPressed=false;
            break;
        case 70: // for 'F' or 'f'
            shaderMode = 2;
            break;
		case 86:
			shaderMode = 1;
		break;
		case 113:
		// uninitialize
            uninitialize();
            // close our application's tab
            window.close(); // may not work in Firefox but works in Safari and chrome
            break;
    }
}

function mouseDown()
{
    // code
}
