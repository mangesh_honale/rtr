// global variables
var canvas = null;
var gl = null; // Webgl context
var canvas_original_width;
var canvas_original_height;
var bFullscreen = false;

// To start animation : to have requestAnimationFrame() to be called "cross-browser" compatible
var requestAnimationFrame = window.requestAnimationFrame || 
window.webkitRequestAnimationFrame ||
window.mozRequestAnimationFrame || 
window.oRequestAnimationFrame || 
window.msRequestAnimationFrame;

// To stop animation : To have cancelAnimationFrame() to be called "cross-browser" compatible
var cancelAnimationFrame = window.cancelAnimationFrame ||
window.webkitCancelRequestAnimationFrame ||
window.webkitCancelAnimationFrame ||
window.mozCancelRequestAnimationFrame ||
window.mozCancelAnimationFrame ||
window.oCancelRequestAnimationFrame ||
window.oCancelAnimationFrame ||
window.msCancelRequestAnimationFrame ||
window.msCancelAnimationFrame;

// onload function
function main(){
	// get canvas element
	canvas = document.getElementById("AMC");
	if(!canvas){
		console.log("Obtaining canvas failed.");
	}else{
		console.log("Canvas obtained successfully!");
		canvas_original_width = canvas.width;
		canvas_original_height = canvas.height;
		
		// register keyboard's keydown event handler
		window.addEventListener("keydown", keyDown, false);
		window.addEventListener("click", mouseDown, false);
		window.addEventListener("resize", resize, false);
		
		// initialize Webgl
		init();
		
		// start drawing here as warming-updateCommands
		resize();
		draw();
	}
}

function toggleFullScreen(){
	var fullscreen_element = 
	document.fullscreenElement ||
	document.webkitFullscreenElement ||
	document.mozFullScreenElement ||
	document.msFullscreenElement ||
	null;
	
	// if not fullscreen
	if(!fullscreen_element){
		if(canvas.requestFullscreen)
			canvas.requestFullscreen();
		else if(canvas.mozRequestFullScreen)
			canvas.mozRequestFullScreen();
		else if(canvas.webkitRequestFullscreen)
			canvas.webkitRequestFullscreen();
		else if(canvas.msRequestFullscreen)
			canvas.msRequestFullscreen();
		bFullscreen = true;
	}else{
		// if already fullscreen
		if(document.exitFullscreen)
			document.exitFullscreen();
		else if(document.mozCancelFullScreen)
			document.mozCancelFullScreen();
		else if(document.webkitExitFullscreen)
			document.webkitExitFullscreen();
		else if(document.msExitFullscreen)
			document.msExitFullscreen();
		bFullscreen = false;
	}
}

function init(){
	// get WebGL 2.0 context
	gl = canvas.getContext("webgl2");
	if(!gl){
		console.log("Failed to get rendering context for WebGL");
		return;
	}
	gl.viewportWidth = canvas.width;
	gl.viewportHeight = canvas.height;
	
	// set clear color
	gl.clearColor(0.0, 0.0, 1.0, 1.0);
}

function resize(){
	if(bFullscreen == true){
		canvas.width = window.innerWidth;
		canvas.height = window.innerHeight;
	}else{
		canvas.width = canvas_original_width;
		canvas.height = canvas_original_height;
	}
	
	// set viewport to match canvas dimensions
	gl.viewport(0, 0, canvas.width, canvas.height);
}

function draw(){
	gl.clear(gl.COLOR_BUFFER_BIT);
	
	//animation loop
	requestAnimationFrame(draw, canvas);
}

function keyDown(event){
	// code
	switch(event.key){
		case 'f':
		case 'F': toggleFullScreen();
				  break;
	}
}

function mouseDown(){
	// code
	//alert("Mouse is clicked");
}