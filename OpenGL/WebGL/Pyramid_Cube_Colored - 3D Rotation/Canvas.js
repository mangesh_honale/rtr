// global variables
var canvas = null;
var gl = null; // Webgl context
var canvas_original_width;
var canvas_original_height;
var bFullscreen = false;

const WebGLMacros = {
	VDG_ATTRIBUTE_VERTEX:0,
	VDG_ATTRIBUTE_COLOR:1,
	VDG_ATTRIBUTE_NORMAL:2,
	VDG_ATTRIBUTE_TEXTURE0:3
};

var vertexShaderObject;
var fragmentShaderObject;
var shaderProgramObject;

var vao_pyramid;
var vao_cube;
var vbo;
var mvpUniform;
var pyramidAngle = 0.0, cubeAngle = 0.0;
var perspectiveProjectionMatrix;

// To start animation : to have requestAnimationFrame() to be called "cross-browser" compatible
var requestAnimationFrame = window.requestAnimationFrame || 
window.webkitRequestAnimationFrame ||
window.mozRequestAnimationFrame || 
window.oRequestAnimationFrame || 
window.msRequestAnimationFrame;

// To stop animation : To have cancelAnimationFrame() to be called "cross-browser" compatible
var cancelAnimationFrame = window.cancelAnimationFrame ||
window.webkitCancelRequestAnimationFrame ||
window.webkitCancelAnimationFrame ||
window.mozCancelRequestAnimationFrame ||
window.mozCancelAnimationFrame ||
window.oCancelRequestAnimationFrame ||
window.oCancelAnimationFrame ||
window.msCancelRequestAnimationFrame ||
window.msCancelAnimationFrame;

// onload function
function main(){
	// get canvas element
	canvas = document.getElementById("AMC");
	if(!canvas){
		console.log("Obtaining canvas failed.");
	}else{
		console.log("Canvas obtained successfully!");
		canvas_original_width = canvas.width;
		canvas_original_height = canvas.height;
		
		// register keyboard's keydown event handler
		window.addEventListener("keydown", keyDown, false);
		window.addEventListener("click", mouseDown, false);
		window.addEventListener("resize", resize, false);
		
		// initialize Webgl
		init();
		
		// start drawing here as warming-updateCommands
		resize();
		draw();
	}
}

function toggleFullScreen(){
	var fullscreen_element = 
	document.fullscreenElement ||
	document.webkitFullscreenElement ||
	document.mozFullScreenElement ||
	document.msFullscreenElement ||
	null;
	
	// if not fullscreen
	if(!fullscreen_element){
		if(canvas.requestFullscreen)
			canvas.requestFullscreen();
		else if(canvas.mozRequestFullScreen)
			canvas.mozRequestFullScreen();
		else if(canvas.webkitRequestFullscreen)
			canvas.webkitRequestFullscreen();
		else if(canvas.msRequestFullscreen)
			canvas.msRequestFullscreen();
		bFullscreen = true;
	}else{
		// if already fullscreen
		if(document.exitFullscreen)
			document.exitFullscreen();
		else if(document.mozCancelFullScreen)
			document.mozCancelFullScreen();
		else if(document.webkitExitFullscreen)
			document.webkitExitFullscreen();
		else if(document.msExitFullscreen)
			document.msExitFullscreen();
		bFullscreen = false;
	}
}

function init(){
	// get WebGL 2.0 context
	gl = canvas.getContext("webgl2");
	if(!gl){
		console.log("Failed to get rendering context for WebGL");
		return;
	}
	gl.viewportWidth = canvas.width;
	gl.viewportHeight = canvas.height;
	
	// vertex shader
	var vertexShaderSourceCode = 
	"#version 300 es"+
    "\n" +
	"in vec4 vPosition;" +
	"in vec4 vColor;" +
	"out vec4 out_vColor;" +
	"uniform mat4 u_mvp_matrix;" +
	"void main(void)" +
	"{" +
	"gl_Position = u_mvp_matrix * vPosition;" +
	"out_vColor = vColor;" +
	"}";


	vertexShaderObject = gl.createShader(gl.VERTEX_SHADER);
	
	gl.shaderSource(vertexShaderObject, vertexShaderSourceCode);
	gl.compileShader(vertexShaderObject);
	
	if(gl.getShaderParameter(vertexShaderObject, gl.COMPILE_STATUS) == false){
		var error = gl.getShaderInfoLog(vertexShaderObject);
		if(error.length > 0){
			alert(error);
			uninitialize();
		}
	}
	
	// fragment shader
	var fragmentShaderSourceCode = 
	"#version 300 es"+
    "\n"+
    "precision highp float;"+
    "in vec4 out_vColor;" +
	"out vec4 FragColor;" +
	"void main(void)" +
	"{" +
	"FragColor = out_vColor;" +
	"}";

	
	fragmentShaderObject = gl.createShader(gl.FRAGMENT_SHADER);
	gl.shaderSource(fragmentShaderObject, fragmentShaderSourceCode);
	gl.compileShader(fragmentShaderObject);
	
	if(gl.getShaderParameter(fragmentShaderObject, gl.COMPILE_STATUS) == false){
		var error = gl.getShaderInfoLog(fragmentShaderObject);
		if(error.length > 0){
			alert(error);
			uninitialize();
		}
	}
	
	shaderProgramObject = gl.createProgram();
	gl.attachShader(shaderProgramObject, vertexShaderObject);
	gl.attachShader(shaderProgramObject, fragmentShaderObject);
	
	// pre-link binding of shader program object with vertex shader attributes
	gl.bindAttribLocation(shaderProgramObject, WebGLMacros.VDG_ATTRIBUTE_VERTEX, "vPosition");
	gl.bindAttribLocation(shaderProgramObject, WebGLMacros.VDG_ATTRIBUTE_COLOR, "vColor");
	
	// linking
	gl.linkProgram(shaderProgramObject);
	if(!gl.getProgramParameter(shaderProgramObject, gl.LINK_STATUS)){
		var error = gl.getProgramInfoLog(shaderProgramObject);
		if(error.length > 0){
			alert(error);
			uninitialize();
		}
	}
	
	// get MVP uniform location
	mvpUniform = gl.getUniformLocation(shaderProgramObject, "u_mvp_matrix");
	
	// vertices, colors, shader attributes, vao_pyramid, vbo_position initialization
	var pyramidVertices = new Float32Array([
		0, 1, 0,    // front-top
		-1, -1, 1,  // front-left
		1, -1, 1,   // front-right

		0, 1, 0,    // right-top
		1, -1, 1,   // right-left
		1, -1, -1,  // right-right

		0, 1, 0,    // back-top
		1, -1, -1,  // back-left
		-1, -1, -1, // back-right

		0, 1, 0,    // left-top
		-1, -1, -1, // left-left
		-1, -1, 1   // left-right

	]);
	
	var pyramidColor = new Float32Array([
		1.0, 0.0, 0.0,
		0.0, 1.0, 0.0,
		0.0, 0.0, 1.0,
		1.0, 0.0, 0.0,
		0.0, 0.0, 1.0,
		0.0, 1.0, 0.0,
		1.0, 0.0, 0.0,
		0.0, 1.0, 0.0,
		0.0, 0.0, 1.0,
		1.0, 0.0, 0.0,
		0.0, 0.0, 1.0,
		0.0, 1.0, 0.0
	]);
	
	var cubeVertices = new Float32Array([
		1.0, 1.0, -1.0,  // top-right of top
		-1.0, 1.0, -1.0, // top-left of top
		-1.0, 1.0, 1.0, // bottom-left of top
		1.0, 1.0, 1.0,  // bottom-right of top

						   // bottom surface
		1.0, -1.0, 1.0,  // top-right of bottom
		-1.0, -1.0, 1.0, // top-left of bottom
		-1.0, -1.0, -1.0, // bottom-left of bottom
		1.0, -1.0, -1.0,  // bottom-right of bottom

							 // front surface
		1.0, 1.0, 1.0,  // top-right of front
		-1.0, 1.0, 1.0, // top-left of front
		-1.0, -1.0, 1.0, // bottom-left of front
		1.0, -1.0, 1.0,  // bottom-right of front

							// back surface
		1.0, -1.0, -1.0,  // top-right of back
		-1.0, -1.0, -1.0, // top-left of back
		-1.0, 1.0, -1.0, // bottom-left of back
		1.0, 1.0, -1.0,  // bottom-right of back

							// left surface
		-1.0, 1.0, 1.0, // top-right of left
		-1.0, 1.0, -1.0, // top-left of left
		-1.0, -1.0, -1.0, // bottom-left of left
		-1.0, -1.0, 1.0, // bottom-right of left

							// right surface
		1.0, 1.0, -1.0,  // top-right of right
		1.0, 1.0, 1.0,  // top-left of right
		1.0, -1.0, 1.0,  // bottom-left of right
		1.0, -1.0, -1.0  // bottom-right of right

	]);
	var i;
	for (i = 0; i<72; i++)
	{
		if (cubeVertices[i]<0.0)
			cubeVertices[i] = cubeVertices[i] + 0.25;
		else if (cubeVertices[i]>0.0)
			cubeVertices[i] = cubeVertices[i] - 0.25;
		else
			cubeVertices[i] = cubeVertices[i]; // no change
	}
	
	var cubeColors = new Float32Array([
		1.0, 0.0, 0.0,
		1.0, 0.0, 0.0,
		1.0, 0.0, 0.0,
		1.0, 0.0, 0.0,
		
		0.0, 1.0, 0.0,
		0.0, 1.0, 0.0,
		0.0, 1.0, 0.0,
		0.0, 1.0, 0.0,
		
		0.0, 0.0, 1.0,
		0.0, 0.0, 1.0,
		0.0, 0.0, 1.0,
		0.0, 0.0, 1.0,

		1.0, 0.0, 0.0,
		1.0, 0.0, 0.0,
		1.0, 0.0, 0.0,
		1.0, 0.0, 0.0,
		
		0.0, 1.0, 0.0,
		0.0, 1.0, 0.0,
		0.0, 1.0, 0.0,
		0.0, 1.0, 0.0,
		
		0.0, 0.0, 1.0,
		0.0, 0.0, 1.0,
		0.0, 0.0, 1.0,
		0.0, 0.0, 1.0

	]);

	
	vao_pyramid = gl.createVertexArray();
	gl.bindVertexArray(vao_pyramid);
	
	vbo = gl.createBuffer();
	gl.bindBuffer(gl.ARRAY_BUFFER, vbo);
	gl.bufferData(gl.ARRAY_BUFFER, pyramidVertices, gl.STATIC_DRAW);
	gl.vertexAttribPointer(WebGLMacros.VDG_ATTRIBUTE_VERTEX, 3, gl.FLOAT, false, 0, 0);
	gl.enableVertexAttribArray(WebGLMacros.VDG_ATTRIBUTE_VERTEX);
	gl.bindBuffer(gl.ARRAY_BUFFER, null);
	
	vbo = gl.createBuffer();
	gl.bindBuffer(gl.ARRAY_BUFFER, vbo);
	gl.bufferData(gl.ARRAY_BUFFER, pyramidColor, gl.STATIC_DRAW);
	gl.vertexAttribPointer(WebGLMacros.VDG_ATTRIBUTE_COLOR, 3, gl.FLOAT, false, 0, 0);
	gl.enableVertexAttribArray(WebGLMacros.VDG_ATTRIBUTE_COLOR);
	gl.bindBuffer(gl.ARRAY_BUFFER, null);
	
	gl.bindVertexArray(null);
	
	vao_cube = gl.createVertexArray();
	gl.bindVertexArray(vao_cube);
	
	vbo = gl.createBuffer();
	gl.bindBuffer(gl.ARRAY_BUFFER, vbo);
	gl.bufferData(gl.ARRAY_BUFFER, cubeVertices, gl.STATIC_DRAW);
	gl.vertexAttribPointer(WebGLMacros.VDG_ATTRIBUTE_VERTEX, 3, gl.FLOAT, false, 0, 0);
	gl.enableVertexAttribArray(WebGLMacros.VDG_ATTRIBUTE_VERTEX);
	
	vbo = gl.createBuffer();
	gl.bindBuffer(gl.ARRAY_BUFFER, vbo);
	gl.bufferData(gl.ARRAY_BUFFER, cubeColors, gl.STATIC_DRAW);
	gl.vertexAttribPointer(WebGLMacros.VDG_ATTRIBUTE_COLOR, 3, gl.FLOAT, false, 0, 0);
	gl.enableVertexAttribArray(WebGLMacros.VDG_ATTRIBUTE_COLOR);
	gl.bindBuffer(gl.ARRAY_BUFFER, null);
	
	gl.bindVertexArray(null);
	// set clear color
	gl.clearColor(0.0, 0.0, 0.0, 1.0);
	
	// initialize projection matrix
	perspectiveProjectionMatrix = mat4.create();
}

function resize(){
	if(bFullscreen == true){
		canvas.width = window.innerWidth;
		canvas.height = window.innerHeight;
	}else{
		canvas.width = canvas_original_width;
		canvas.height = canvas_original_height;
	}
	
	// set viewport to match canvas dimensions
	gl.viewport(0, 0, canvas.width, canvas.height);
	
	// perspective projection 
	
	mat4.perspective(perspectiveProjectionMatrix, 45.0, parseFloat(canvas.width/canvas.height), 0.1, 100.0);
	
}

function draw(){
	gl.clear(gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT);
	
	gl.useProgram(shaderProgramObject);
	var modelViewMatrix = mat4.create();
	var modelViewProjectionMatrix = mat4.create();
	
	mat4.translate(modelViewMatrix, modelViewMatrix,[-1.5, 0.0, -6.0]);
	mat4.rotateY(modelViewMatrix, modelViewMatrix, degToRad(pyramidAngle));
	mat4.multiply(modelViewProjectionMatrix, perspectiveProjectionMatrix, modelViewMatrix);
	
	gl.uniformMatrix4fv(mvpUniform, false, modelViewProjectionMatrix);
	
	gl.bindVertexArray(vao_pyramid);
	gl.drawArrays(gl.TRIANGLES, 0, 12);
	//gl.bindVeretxArray(null);
	
	modelViewMatrix = mat4.create();
	modelViewProjectionMatrix = mat4.create();
	mat4.translate(modelViewMatrix, modelViewMatrix,[1.5, 0.0, -6.0]);
	mat4.rotateZ(modelViewMatrix, modelViewMatrix, degToRad(cubeAngle));
	mat4.multiply(modelViewProjectionMatrix, perspectiveProjectionMatrix, modelViewMatrix);
	
	gl.uniformMatrix4fv(mvpUniform, false, modelViewProjectionMatrix);
	
	gl.bindVertexArray(vao_cube);
	gl.drawArrays(gl.TRIANGLE_FAN, 0, 4);
	gl.drawArrays(gl.TRIANGLE_FAN, 4, 4);
	gl.drawArrays(gl.TRIANGLE_FAN, 8, 4);
	gl.drawArrays(gl.TRIANGLE_FAN, 12, 4);
	gl.drawArrays(gl.TRIANGLE_FAN, 16, 4);
	gl.drawArrays(gl.TRIANGLE_FAN, 20, 4);
	//gl.bindVeretxArray(null);
	gl.useProgram(null);
	//animation loop
	update();
	requestAnimationFrame(draw, canvas);
}

function update(){
	pyramidAngle = (pyramidAngle + 1.0);
	if (pyramidAngle >= 360.0)
		pyramidAngle = 0.0;
	
	cubeAngle = (cubeAngle + 1.0);
	if (cubeAngle >= 360.0)
		cubeAngle = 0.0;
	
	
}
function degToRad(degree){
	return (degree * Math.PI/180.0);
}

function uninitialize(){
	if(vao_pyramid){
		gl.deleteVertexArray(vao_pyramid);
		vao_pyramid = null;
	}
	if(vbo){
		gl.deleteBuffer(vbo);
		vbo = null;
	}
	if(shaderProgramObject){
		if(fragmentShaderObject){
			gl.detachShader(shaderProgramObject, fragmentShaderObject);
			gl.deleteShder(fragmentShaderObject);
			fragmentShaderObject = null;
		}
		if(vertexShaderObject){
			gl.detachShader(shaderProgramObject, vertexShaderObject);
			gl.deleteShader(vertexShaderObject);
			vertexShaderObject = null;
		}
		gl.deleteProgram(shaderProgramObject);
		shaderProgramObject = null;
	}
}

function keyDown(event){
	// code
	switch(event.key){
		case 'f':
		case 'F': toggleFullScreen();
				  break;
	}
	switch(event.keyCode){
		case 27: // handle escape key
				 uninitialize();
				 window.close();
				 break;
	}
}

function mouseDown(){
	// code
	//alert("Mouse is clicked");
}