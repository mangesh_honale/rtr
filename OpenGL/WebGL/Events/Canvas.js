// global variables
var canvas = null;
var context = null;

// onload function
function main(){
	// get canvas element
	var canvas = document.getElementById("AMC");
	if(!canvas){
		console.log("Obtaining canvas failed.");
	}else{
		console.log("Canvas obtained successfully!");
		// print canvas width and height on console
		console.log("Canvas width: " + canvas.width + "And canvas height:" + canvas.height);
		
		// get 2D context
		var context = canvas.getContext("2d");
		if(!context){
			console.log("Obtaining 2d context failed.");
		}else{
			console.log("Obtained 2d context successfully.");
			
			// fill canvas with black color
			context.fillStyle = "black";
			context.fillRect(0, 0, canvas.width, canvas.height);
			
			// center the text
			context.textAlign = "center";
			context.textBaseline ="middle";
			
			// text
			var str = "Hello World!";
			
			// text font
			context.font = "48px sans-serif";
			
			// text color
			context.fillStyle = "white";
			
			// display the text in center
			context.fillText(str, canvas.width/2, canvas.height/2);
			
			// register keyboard's keydown event handler
			window.addEventListener("keydown", keyDown, false);
			window.addEventListener("click", mouseDown, false);
		
		}
	}
}

function keyDown(event){
	// code
	alert(event.key + " key pressed.");
}

function mouseDown(){
	// code
	alert("Mouse is clicked");
}