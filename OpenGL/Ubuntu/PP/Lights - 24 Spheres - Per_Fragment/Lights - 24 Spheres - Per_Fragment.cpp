#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <memory.h>
#include <X11/Xlib.h>
#include <X11/Xutil.h>
#include <X11/XKBlib.h>
#include <X11/keysym.h>

#include <GL/glew.h>
#include <GL/gl.h>
#include <GL/glx.h>
#include "Sphere.h"
#include "vmath.h"
#pragma comment(lib, "Sphere.lib")
// Namespace
using namespace std;
using namespace vmath;

enum
{
	VDG_ATTRIBUTE_VERTEX = 0,
	VDG_ATTRIBUTE_COLOR,
	VDG_ATTRIBUTE_NORMAL,
	VDG_ATTRIBUTE_TEXTURE0,
};

bool bFullScreen = false;
Display *gpDisplay = NULL;
XVisualInfo *gpXVisualInfo = NULL;
Colormap gColormap;
Window gWindow;
int giWindowWidth = 800;
int giWindowHeight = 600;

GLXContext gGLXContext;

FILE *gpFile = NULL;
int winWidth, winHeight;
GLuint gVertexShaderObject;
GLuint gFragmentShaderObject;
GLuint gShaderProgramObject;

GLuint gNumElements;
GLuint gNumVertices;
float sphere_vertices[1146];
float sphere_normals[1146];
float sphere_textures[764];
unsigned short sphere_elements[2280];


GLuint gVao_sphere;
GLuint gVbo_sphere_position;
GLuint gVbo_sphere_normal;
GLuint gVbo_sphere_element;
GLuint model_matrix_uniform, view_matrix_uniform, projection_matrix_uniform;
GLuint L_KeyPressed_uniform;
GLuint La_uniform;
GLuint Ld_uniform;
GLuint Ls_uniform;
GLuint light_position_uniform;

GLuint Ka_uniform;
GLuint Kd_uniform;
GLuint Ks_uniform;
GLuint material_shininess_uniform;

mat4 gPerspectiveProjectionMatrix;

bool gbLight;

GLfloat lightAmbient[] = { 0.0f, 0.0f, 0.0f, 1.0f };
GLfloat lightDiffuse[] = { 1.0f, 1.0f, 1.0f, 1.0f };
GLfloat lightSpecular[] = { 1.0f, 1.0f, 1.0f, 1.0f };
GLfloat lightPosition[] = { 0.0f, 0.0f, 0.0f, 0.0f };

//11
GLfloat material_emerald_ambient[] = { 0.0215f, 0.1745f, 0.0215f, 1.0f };
GLfloat material_emerald_diffuse[] = { 0.07568f, 0.61424f, 0.07568f, 1.0f };
GLfloat material_emerald_specular[] = { 0.633f, 0.727811f, 0.633f, 1.0f };
GLfloat material_emerald_shininess = 0.6f * 128;

//12
GLfloat material_jade_ambient[] = { 0.135f, 0.225f, 0.1575f, 1.0f };
GLfloat material_jade_diffuse[] = { 0.54f, 0.89f, 0.63f, 1.0f };
GLfloat material_jade_specular[] = { 0.316228f, 0.316228f, 0.316228f, 1.0f };
GLfloat material_jade_shininess = 0.1f * 128;

//13
GLfloat material_obsidian_ambient[] = { 0.05375f, 0.05f, 0.06625f, 1.0f };
GLfloat material_obsidian_diffuse[] = { 0.18275f, 0.17f, 0.22525f, 1.0f };
GLfloat material_obsidian_specular[] = { 0.332741f, 0.328634f, 0.346435f, 1.0f };
GLfloat material_obsidian_shininess = 0.3f * 128;

//14
GLfloat material_pearl_ambient[] = { 0.25f, 0.20725f, 0.20725f, 1.0f };
GLfloat material_pearl_diffuse[] = { 1.0f, 0.829f, 0.829f, 1.0f };
GLfloat material_pearl_specular[] = { 0.296648f, 0.296648f, 0.296648f, 1.0f };
GLfloat material_pearl_shininess = 0.088f * 128;

//15
GLfloat material_ruby_ambient[] = { 0.1745f, 0.01175f, 0.01175f, 1.0f };
GLfloat material_ruby_diffuse[] = { 0.61424f, 0.04136f, 0.04136f, 1.0f };
GLfloat material_ruby_specular[] = { 0.727811f, 0.626959f, 0.626959f, 1.0f };
GLfloat material_ruby_shininess = 0.6f * 128;

//16
GLfloat material_turquoise_ambient[] = { 0.1f, 0.18725f, 0.1745f, 1.0f };
GLfloat material_turquoise_diffuse[] = { 0.396f, 0.74151f, 0.69102f, 1.0f };
GLfloat material_turquoise_specular[] = { 0.297254f, 0.30829f, 0.306678f, 1.0f };
GLfloat material_turquoise_shininess = 0.1 * 128;

//21
GLfloat material_brass_ambient[] = { 0.329412f, 0.223529f, 0.027451f, 1.0f };
GLfloat material_brass_diffuse[] = { 0.780392f, 0.568627f, 0.113725f, 1.0f };
GLfloat material_brass_specular[] = { 0.992157f, 0.941176f, 0.807843f, 1.0f };
GLfloat material_brass_shininess = 0.21794872f * 128;

//22
GLfloat material_bronze_ambient[] = { 0.2125f, 0.1275, 0.054f, 1.0f };
GLfloat material_bronze_diffuse[] = { 0.714f, 0.4284f, 0.18144f, 1.0f };
GLfloat material_bronze_specular[] = { 0.393548f, 0.271906f, 0.166721f, 1.0f };
GLfloat material_bronze_shininess = 0.2f * 128;

//23
GLfloat material_chrome_ambient[] = { 0.25f, 0.25f, 0.25f, 1.0f };
GLfloat material_chrome_diffuse[] = { 0.4f, 0.4f, 0.4f, 1.0f };
GLfloat material_chrome_specular[] = { 0.774597f, 0.774597f, 0.774597f, 1.0f };
GLfloat material_chrome_shininess = 0.6f * 128;

//24
GLfloat material_copper_ambient[] = { 0.19125f, 0.0735f, 0.0225f, 1.0f };
GLfloat material_copper_diffuse[] = { 0.7038f, 0.27048f, 0.0828f, 1.0f };
GLfloat material_copper_specular[] = { 0.256777f, 0.137622f, 0.86014f, 1.0f };
GLfloat material_copper_shininess = 0.1f * 128;

//25
GLfloat material_gold_ambient[] = { 0.24725f, 0.1995, 0.0745f, 1.0f };
GLfloat material_gold_diffuse[] = { 0.75164f, 0.61648f, 0.22648f, 1.0f };
GLfloat material_gold_specular[] = { 0.628281f, 0.555802f, 0.366065f, 1.0f };
GLfloat material_gold_shininess = 0.4f * 128;

//26
GLfloat material_silver_ambient[] = { 0.19225f, 0.19225f, 0.19225f, 1.0f };
GLfloat material_silver_diffuse[] = { 0.50754f, 0.50754f, 0.50754f, 1.0f };
GLfloat material_silver_specular[] = { 0.508273f, 0.508273f, 0.508273f, 1.0f };
GLfloat material_silver_shininess = 0.4f * 128;

//31
GLfloat material_black_ambient[] = { 0.0f, 0.0f, 0.0f, 1.0f };
GLfloat material_black_diffuse[] = { 0.01f, 0.01f, 0.01f, 1.0f };
GLfloat material_black_specular[] = { 0.50f, 0.50f, 0.50f, 1.0f };
GLfloat material_black_shininess = 0.25f * 128;

//32
GLfloat material_cyan_ambient[] = { 0.0f, 0.1f, 0.06f, 1.0f };
GLfloat material_cyan_diffuse[] = { 0.0f, 0.50980392f, 0.50980932f, 1.0f };
GLfloat material_cyan_specular[] = { 0.50196078f, 0.50196078f, 0.50196078f, 1.0f };
GLfloat material_cyan_shininess = 0.25f * 128;

//33
GLfloat material_green_ambient[] = { 0.0f, 0.0f, 0.0f, 1.0f };
GLfloat material_green_diffuse[] = { 0.1f, 0.35f, 0.1f, 1.0f };
GLfloat material_green_specular[] = { 0.45f, 0.45f, 0.45f, 1.0f };
GLfloat material_green_shininess = 0.25f * 128;

//34
GLfloat material_red_ambient[] = { 0.0f, 0.0f, 0.0f, 1.0f };
GLfloat material_red_diffuse[] = { 0.5f, 0.0f, 0.0f, 1.0f };
GLfloat material_red_specular[] = { 0.7f, 0.6f, 0.6f, 1.0f };
GLfloat material_red_shininess = 0.25f * 128;

//35
GLfloat material_white_ambient[] = { 0.0f, 0.0f, 0.0f, 1.0f };
GLfloat material_white_diffuse[] = { 0.55f, 0.55f, 0.55f, 1.0f };
GLfloat material_white_specular[] = { 0.70f, 0.70f, 0.70f, 1.0f };
GLfloat material_white_shininess = 0.25f * 128;

//36
GLfloat material_yellow_ambient[] = { 0.0f, 0.0f, 0.0f, 1.0f };
GLfloat material_yellow_diffuse[] = { 0.5f, 0.5f, 0.0f, 1.0f };
GLfloat material_yellow_specular[] = { 0.60f, 0.60f, 0.50f, 1.0f };
GLfloat material_yellow_shininess = 0.25f * 128;

//41
GLfloat material_black2_ambient[] = { 0.02f, 0.02f, 0.02f, 1.0f };
GLfloat material_black2_diffuse[] = { 0.01f, 0.01f, 0.01f, 1.0f };
GLfloat material_black2_specular[] = { 0.4f, 0.4f, 0.4f, 1.0f };
GLfloat material_black2_shininess = 0.078125f * 128;

//42
GLfloat material_cyan2_ambient[] = { 0.0f, 0.05f, 0.05f, 1.0f };
GLfloat material_cyan2_diffuse[] = { 0.4f, 0.5f, 0.0f, 1.0f };
GLfloat material_cyan2_specular[] = { 0.04f, 0.7f, 0.7f, 1.0f };
GLfloat material_cyan2_shininess = 0.078125f * 128;

//43
GLfloat material_green2_ambient[] = { 0.0f, 0.05f, 0.0f, 1.0f };
GLfloat material_green2_diffuse[] = { 0.4f, 0.5f, 0.4f, 1.0f };
GLfloat material_green2_specular[] = { 0.04f, 0.7f, 0.04f, 1.0f };
GLfloat material_green2_shininess = 0.078125f * 128;

//44
GLfloat material_red2_ambient[] = { 0.05f, 0.0f, 0.0f, 1.0f };
GLfloat material_red2_diffuse[] = { 0.5f, 0.4f, 0.4f, 1.0f };
GLfloat material_red2_specular[] = { 0.7f, 0.04f, 0.04f, 1.0f };
GLfloat material_red2_shininess = 0.078125f * 128;

//45
GLfloat material_white2_ambient[] = { 0.05f, 0.05f, 0.05f, 1.0f };
GLfloat material_white2_diffuse[] = { 0.5f, 0.5f, 0.5f, 1.0f };
GLfloat material_white2_specular[] = { 0.7f, 0.7f, 0.7f, 1.0f };
GLfloat material_white2_shininess = 0.078125f * 128;

//46
GLfloat material_yellow2_ambient[] = { 0.05f, 0.05f, 0.0f, 1.0f };
GLfloat material_yellow2_diffuse[] = { 0.5f, 0.5f, 0.4f, 1.0f };
GLfloat material_yellow2_specular[] = { 0.7f, 0.7f, 0.04f, 1.0f };
GLfloat material_yellow2_shininess = 0.078125f * 128;


GLfloat material_ambient[] = { 0.0f,0.0f,0.0f,1.0f };
GLfloat material_diffuse[] = { 1.0f,1.0f,1.0f,1.0f };
GLfloat material_specular[] = { 1.0f,1.0f,1.0f,1.0f };
GLfloat material_shininess = 50.0f;

GLfloat gfLightAngle;
int axisIdentifier = 0;



int main(void){
	//function declarations
	void CreateWindow(void);
	void ToggleFullScreen(void);
	void initialize(void);
	void display(void);
	void uninitialize();
	void resize(int, int);	
	void update(void);
	//variable declarations
	static bool bIsLKeyPressed = false;
	bool bDone = false;
	gpFile = fopen("Log.txt", "w");
	if (gpFile == 0)
	{
		printf("Log file can not be created.\n Exitting...");
		exit(0);
	}
	else
	{
		fprintf(gpFile, "Log file successfully Opened.\n");
	}
	CreateWindow();
	initialize();

	//message loop
	XEvent event;
	KeySym keysym;
	
	while(bDone == false){
		while(XPending(gpDisplay)){
			XNextEvent(gpDisplay, &event);
			switch(event.type){
				case MapNotify:
					break;
				case KeyPress:
					keysym = XkbKeycodeToKeysym(gpDisplay, event.xkey.keycode, 0, 0);
					switch(keysym){
						case XK_Escape:
							bDone = true;
							break;
						case XK_F:
						case XK_f:
							if(bFullScreen == false){
								ToggleFullScreen();
								bFullScreen = true;
							}else{
								ToggleFullScreen();
								bFullScreen = false;
							}
							break;
						case XK_l:
						case XK_L:
							if (bIsLKeyPressed == false)
							{
								gbLight = true;
								bIsLKeyPressed = true;
							}
							else
							{
								gbLight = false;
								bIsLKeyPressed = false;
							}
							break;
						case XK_x:
						case XK_X:
							axisIdentifier = 0;
							lightPosition[0] = 0.0f;
							lightPosition[1] = 0.0f;
							lightPosition[2] = 0.0f;
							gfLightAngle = 0.0f;
							break;
						case XK_y:
						case XK_Y:
							axisIdentifier = 1;
							lightPosition[0] = 0.0f;
							lightPosition[1] = 0.0f;
							lightPosition[2] = 0.0f;
							gfLightAngle = 0.0f;
							break;
						case XK_z:
						case XK_Z:
							axisIdentifier = 2;
							lightPosition[0] = 0.0f;
							lightPosition[1] = 0.0f;
							lightPosition[2] = 0.0f;
							gfLightAngle = 0.0f;
							break;
						default:
							break;

					}
					break;
				case ButtonPress:
					switch(event.xbutton.button){
						case 1: 
							break;
						case 2:
							break;
						case 3:
							break;
						default:
							break;
					}
					break;
				case MotionNotify:
					break;
				case ConfigureNotify:
					winWidth = event.xconfigure.width;
					winHeight = event.xconfigure.height;
					resize(winWidth, winHeight);
					break;
				case Expose:
					break;
				case DestroyNotify:
					break;
				case 33:
					bDone = true;
					break;
				default:
					break;
			}
		}
		update();
		display();
	}
	uninitialize();
	return (0);
}

void CreateWindow(void){
	//function prototypes
	void uninitialize(void);
	
	//variable declarations
	XSetWindowAttributes winAttribs;
	int defaultScreen;
	int defaultDepth;
	int styleMask;
	
	static int frameBufferAttributes[] = {
		GLX_RGBA,
		GLX_RED_SIZE, 8,
		GLX_GREEN_SIZE, 8,
		GLX_BLUE_SIZE, 8,
		GLX_ALPHA_SIZE, 8,
		GLX_DOUBLEBUFFER, True,
		GLX_DEPTH_SIZE, 24,
		None
	};

	//code
	gpDisplay = XOpenDisplay(NULL);
	if(gpDisplay == NULL){
		printf("Erroe: Unable to open X Display.\n Exiting now...\n");
		uninitialize();
		exit(1);
	}

	defaultScreen = XDefaultScreen(gpDisplay);
	

	gpXVisualInfo = glXChooseVisual(gpDisplay, defaultScreen, frameBufferAttributes);
	if (gpXVisualInfo == NULL) {
		printf("Error: Unable to allocate memory for Visual info.\nExitting Now.\n");
		uninitialize();
        	exit(1);
	}
	XMatchVisualInfo(gpDisplay, defaultScreen, defaultDepth, TrueColor, gpXVisualInfo);
	if(gpXVisualInfo == NULL) {
	    printf("Error: Unable to get VisualInfo. \nExitting...\n");
	    uninitialize();
	    exit(1);
	}
	winAttribs.border_pixmap = 0;
	winAttribs.border_pixel = 0;
	winAttribs.background_pixmap = 0;
	winAttribs.colormap = XCreateColormap(gpDisplay, RootWindow(gpDisplay, gpXVisualInfo->screen), gpXVisualInfo->visual, AllocNone);
        gColormap =  winAttribs.colormap;
	winAttribs.background_pixel =  BlackPixel(gpDisplay, defaultScreen);
	winAttribs.event_mask = ExposureMask | VisibilityChangeMask | ButtonPressMask | KeyPressMask | PointerMotionMask | StructureNotifyMask;

	styleMask =  CWBorderPixel | CWBackPixel | CWEventMask | CWColormap;

	gWindow = XCreateWindow(gpDisplay,
	RootWindow(gpDisplay, gpXVisualInfo->screen),
	0,  // X co-ordinate distance
	0,  // Y co-ordinate distance
	giWindowWidth, // Window width
	giWindowHeight, // Window height
	0, // Border Thickness, '0' is default
	gpXVisualInfo->depth,
	InputOutput,
	gpXVisualInfo->visual,
	styleMask,
	&winAttribs
	);

	if(!gWindow) {

	printf("Error: Failed to create main window.\nExitting... \n");
	uninitialize();
	exit(1);
	}
	
	XStoreName(gpDisplay, gWindow, "First OpenGL Window");
	
	Atom windowManagerDelete = XInternAtom(gpDisplay, "WM_DELETE_WINDOW", True);
	XSetWMProtocols(gpDisplay, gWindow, &windowManagerDelete, 1);
	XMapWindow(gpDisplay, gWindow);
}

void ToggleFullScreen(){

	Atom wm_state;
	Atom fullscreen;
	XEvent xev={0};

	//code
	wm_state = XInternAtom(gpDisplay, "_NET_WM_STATE", False);
	memset(&xev, 0, sizeof(xev));
	
	xev.type = ClientMessage;
	xev.xclient.window = gWindow;
	xev.xclient.message_type = wm_state;
	xev.xclient.format = 32;
	xev.xclient.data.l[0] = bFullScreen ? 0: 1;
	
	fullscreen = XInternAtom(gpDisplay, "_NET_WM_STATE_FULLSCREEN", False);
	xev.xclient.data.l[1] = fullscreen;
		
	XSendEvent(gpDisplay, RootWindow(gpDisplay, gpXVisualInfo->screen),
		  False,
		  StructureNotifyMask,
		  &xev);
}

void initialize(void){
	//function prototypes
	void resize(int, int);
	void uninitialize(void);
	void LoadGLTexture();
	gGLXContext = glXCreateContext(gpDisplay, gpXVisualInfo, NULL, GL_TRUE);
	glXMakeCurrent(gpDisplay, gWindow, gGLXContext);
	glewInit();
	gVertexShaderObject = glCreateShader(GL_VERTEX_SHADER);

	// provide source code to shader
	const GLchar *vertexShaderSourceCode =
		"#version 130" \
		"\n" \
		"in vec4 vPosition;" \
		"in vec3 vNormal;" \
		"uniform mat4 u_model_matrix;" \
		"uniform mat4 u_view_matrix;" \
		"uniform mat4 u_projection_matrix;" \
		"uniform vec4 u_light_position;" \
		"uniform int u_lighting_enabled;" \
		"out vec3 transformed_normals;" \
		"out vec3 light_direction;" \
		"out vec3 viewer_vector;" \
		"void main(void)" \
		"{" \
		"if(u_lighting_enabled==1)" \
		"{" \
		"vec4 eye_coordinates=u_view_matrix * u_model_matrix * vPosition;" \
		"transformed_normals=mat3(u_view_matrix * u_model_matrix) * vNormal;" \
		"light_direction = vec3(u_light_position) - eye_coordinates.xyz;" \
		"viewer_vector = -eye_coordinates.xyz;" \
		"}" \
		"gl_Position=u_projection_matrix * u_view_matrix * u_model_matrix * vPosition;" \
		"}";



	glShaderSource(gVertexShaderObject, 1, (const GLchar **)&vertexShaderSourceCode, NULL);

	// compile shader
	glCompileShader(gVertexShaderObject);
	GLint iInfoLogLength = 0;
	GLint iShaderCompiledStatus = 0;
	char *szInfoLog = NULL;
	glGetShaderiv(gVertexShaderObject, GL_COMPILE_STATUS, &iShaderCompiledStatus);
	if (iShaderCompiledStatus == GL_FALSE)
	{
		glGetShaderiv(gVertexShaderObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
		if (iInfoLogLength > 0)
		{
			szInfoLog = (char *)malloc(iInfoLogLength);
			if (szInfoLog != NULL)
			{
				GLsizei written;
				glGetShaderInfoLog(gVertexShaderObject, iInfoLogLength, &written, szInfoLog);
				fprintf(gpFile, "Vertex Shader Compilation Log : %s\n", szInfoLog);
				free(szInfoLog);
				uninitialize();
				exit(0);
			}
		}
	}

	// *** FRAGMENT SHADER ***
	// create shader
	gFragmentShaderObject = glCreateShader(GL_FRAGMENT_SHADER);

	// provide source code to shader
	const GLchar *fragmentShaderSourceCode =
		"#version 130" \
		"\n" \
		"in vec3 transformed_normals;" \
		"in vec3 light_direction;" \
		"in vec3 viewer_vector;" \
		"out vec4 FragColor;" \
		"uniform vec3 u_La;" \
		"uniform vec3 u_Ld;" \
		"uniform vec3 u_Ls;" \
		"uniform vec3 u_Ka;" \
		"uniform vec3 u_Kd;" \
		"uniform vec3 u_Ks;" \
		"uniform float u_material_shininess;" \
		"uniform int u_lighting_enabled;" \
		"void main(void)" \
		"{" \
		"vec3 phong_ads_color;" \
		"if(u_lighting_enabled==1)" \
		"{" \
		"vec3 normalized_transformed_normals=normalize(transformed_normals);" \
		"vec3 normalized_light_direction=normalize(light_direction);" \
		"vec3 normalized_viewer_vector=normalize(viewer_vector);" \
		"vec3 ambient = u_La * u_Ka;" \
		"float tn_dot_ld = max(dot(normalized_transformed_normals, normalized_light_direction),0.0);" \
		"vec3 diffuse = u_Ld * u_Kd * tn_dot_ld;" \
		"vec3 reflection_vector = reflect(-normalized_light_direction, normalized_transformed_normals);" \
		"vec3 specular = u_Ls * u_Ks * pow(max(dot(reflection_vector, normalized_viewer_vector), 0.0), u_material_shininess);" \
		"phong_ads_color=ambient + diffuse + specular;" \
		"}" \
		"else" \
		"{" \
		"phong_ads_color = vec3(1.0, 1.0, 1.0);" \
		"}" \
		"FragColor = vec4(phong_ads_color, 1.0);" \
		"}";

	glShaderSource(gFragmentShaderObject, 1, (const GLchar **)&fragmentShaderSourceCode, NULL);

	// compile shader
	glCompileShader(gFragmentShaderObject);
	glGetShaderiv(gFragmentShaderObject, GL_COMPILE_STATUS, &iShaderCompiledStatus);
	if (iShaderCompiledStatus == GL_FALSE)
	{
		glGetShaderiv(gFragmentShaderObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
		if (iInfoLogLength > 0)
		{
			szInfoLog = (char *)malloc(iInfoLogLength);
			if (szInfoLog != NULL)
			{
				GLsizei written;
				glGetShaderInfoLog(gFragmentShaderObject, iInfoLogLength, &written, szInfoLog);
				fprintf(gpFile, "Fragment Shader Compilation Log : %s\n", szInfoLog);
				free(szInfoLog);
				uninitialize();
				exit(0);
			}
		}
	}

	// *** SHADER PROGRAM ***
	// create
	gShaderProgramObject = glCreateProgram();

	// attach vertex shader to shader program
	glAttachShader(gShaderProgramObject, gVertexShaderObject);

	// attach fragment shader to shader program
	glAttachShader(gShaderProgramObject, gFragmentShaderObject);

	// pre-link binding of shader program object with vertex shader position attribute
	glBindAttribLocation(gShaderProgramObject, VDG_ATTRIBUTE_VERTEX, "vPosition");
	glBindAttribLocation(gShaderProgramObject, VDG_ATTRIBUTE_NORMAL, "vNormal");

	// link shader
	glLinkProgram(gShaderProgramObject);
	GLint iShaderProgramLinkStatus = 0;
	glGetProgramiv(gShaderProgramObject, GL_LINK_STATUS, &iShaderProgramLinkStatus);
	if (iShaderProgramLinkStatus == GL_FALSE)
	{
		glGetProgramiv(gShaderProgramObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
		if (iInfoLogLength>0)
		{
			szInfoLog = (char *)malloc(iInfoLogLength);
			if (szInfoLog != NULL)
			{
				GLsizei written;
				glGetProgramInfoLog(gShaderProgramObject, iInfoLogLength, &written, szInfoLog);
				fprintf(gpFile, "Shader Program Link Log : %s\n", szInfoLog);
				free(szInfoLog);
				uninitialize();
				exit(0);
			}
		}
	}

	// get uniform locations
	model_matrix_uniform = glGetUniformLocation(gShaderProgramObject, "u_model_matrix");
	view_matrix_uniform = glGetUniformLocation(gShaderProgramObject, "u_view_matrix");
	projection_matrix_uniform = glGetUniformLocation(gShaderProgramObject, "u_projection_matrix");


	L_KeyPressed_uniform = glGetUniformLocation(gShaderProgramObject, "u_lighting_enabled");

	// Ambient color intensity of light
	La_uniform = glGetUniformLocation(gShaderProgramObject, "u_La");
	// Diffuse color intensity of light
	Ld_uniform = glGetUniformLocation(gShaderProgramObject, "u_Ld");
	// Specular color intensity of light
	Ls_uniform = glGetUniformLocation(gShaderProgramObject, "u_Ls");

	// Position of light
	light_position_uniform = glGetUniformLocation(gShaderProgramObject, "u_light_position");;

	// Ambient color reflective intensity of material
	Ka_uniform = glGetUniformLocation(gShaderProgramObject, "u_Ka");
	// diffuse reflective color intensity of material
	Kd_uniform = glGetUniformLocation(gShaderProgramObject, "u_Kd");
	// specular reflective color intensity of material
	Ks_uniform = glGetUniformLocation(gShaderProgramObject, "u_Ks");
	// shininess of material ( value is conventionally between 1 to 200 )
	material_shininess_uniform = glGetUniformLocation(gShaderProgramObject, "u_material_shininess");;

	// *** vertices, colors, shader attribs, vbo, vao initializations ***
	getSphereVertexData(sphere_vertices, sphere_normals, sphere_textures, sphere_elements);

	gNumVertices = getNumberOfSphereVertices();
	gNumElements = getNumberOfSphereElements();
	// *** vertices, colors, shader attribs, vbo, vao initializations ***


	// CUBE CODE
	// vao
	glGenVertexArrays(1, &gVao_sphere);
	glBindVertexArray(gVao_sphere);

	// position vbo
	glGenBuffers(1, &gVbo_sphere_position);
	glBindBuffer(GL_ARRAY_BUFFER, gVbo_sphere_position);
	glBufferData(GL_ARRAY_BUFFER, sizeof(sphere_vertices), sphere_vertices, GL_STATIC_DRAW);

	glVertexAttribPointer(VDG_ATTRIBUTE_VERTEX, 3, GL_FLOAT, GL_FALSE, 0, NULL);

	glEnableVertexAttribArray(VDG_ATTRIBUTE_VERTEX);

	glBindBuffer(GL_ARRAY_BUFFER, 0);

	// normal vbo
	glGenBuffers(1, &gVbo_sphere_normal);
	glBindBuffer(GL_ARRAY_BUFFER, gVbo_sphere_normal);
	glBufferData(GL_ARRAY_BUFFER, sizeof(sphere_normals), sphere_normals, GL_STATIC_DRAW);

	glVertexAttribPointer(VDG_ATTRIBUTE_NORMAL, 3, GL_FLOAT, GL_FALSE, 0, NULL);

	glEnableVertexAttribArray(VDG_ATTRIBUTE_NORMAL);

	glBindBuffer(GL_ARRAY_BUFFER, 0);

	// element vbo
	glGenBuffers(1, &gVbo_sphere_element);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, gVbo_sphere_element);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(sphere_elements), sphere_elements, GL_STATIC_DRAW);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);

	glBindVertexArray(0);

	glShadeModel(GL_SMOOTH);
	// set-up depth buffer
	glClearDepth(1.0f);
	// enable depth testing
	glEnable(GL_DEPTH_TEST);
	// depth test to do
	glDepthFunc(GL_LEQUAL);
	// set really nice percpective calculations ?
	glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST);
	// We will always cull back faces for better performance
	glEnable(GL_CULL_FACE);

	// set background color
	glClearColor(0.25f, 0.25f, 0.25f, 0.0f); //grey

										  // set perspective matrix to identitu matrix
	gPerspectiveProjectionMatrix = mat4::identity();

	gbLight = false;

	resize(giWindowWidth, giWindowHeight);
}





void display(){
	void MapViewport(int x, int y, int width, int height);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	// Start using OpenGL program object
	glUseProgram(gShaderProgramObject);

	if (gbLight == true)
	{
		fprintf(gpFile, "true :\n");
		// set 'u_lighting_enabled' uniform
		glUniform1i(L_KeyPressed_uniform, 1);

		// setting light's properties
		glUniform3fv(La_uniform, 1, lightAmbient);
		glUniform3fv(Ld_uniform, 1, lightDiffuse);
		glUniform3fv(Ls_uniform, 1, lightSpecular);
		if (axisIdentifier == 0) {
			lightPosition[1] = sinf(gfLightAngle);
			lightPosition[2] = cosf(gfLightAngle) - 1;
		}
		if (axisIdentifier == 1) {

			lightPosition[0] = sinf(gfLightAngle);
			lightPosition[2] = cosf(gfLightAngle) - 1;
		}
		if (axisIdentifier == 2) {
			lightPosition[2] = -2.0f;
			lightPosition[0] = cosf(gfLightAngle);
			lightPosition[1] = sinf(gfLightAngle);
		}
		glUniform4fv(light_position_uniform, 1, lightPosition);

		// setting material's properties
		glUniform3fv(Ka_uniform, 1, material_ambient);
		glUniform3fv(Kd_uniform, 1, material_diffuse);
		glUniform3fv(Ks_uniform, 1, material_specular);
		glUniform1f(material_shininess_uniform, material_shininess);
	}
	else
	{
		// set 'u_lighting_enabled' uniform
		fprintf(gpFile, "false :\n");
		glUniform1i(L_KeyPressed_uniform, 0);
	}

	// OpenGL Drawing
	mat4 modelMatrix = mat4::identity();
	mat4 viewMatrix = mat4::identity();

	MapViewport(winWidth/6, 5*winHeight/6,winWidth / 6, winHeight / 6);
	// apply z axis translation to go deep into the screen by -2.0,
	// so that triangle with same fullscreen co-ordinates, but due to above translation will look small
	if (gbLight == true) {
		glUniform3fv(Ka_uniform, 1, material_emerald_ambient);
		glUniform3fv(Kd_uniform, 1, material_emerald_diffuse);
		glUniform3fv(Ks_uniform, 1, material_emerald_specular);
		glUniform1f(material_shininess_uniform, material_emerald_shininess);
	}
	modelMatrix = translate(0.0f, 0.0f, -2.0f);
	
	//modelMatrix = modelMatrix * scale(0.3f, 0.3f, 0.3f);
	glUniformMatrix4fv(model_matrix_uniform, 1, GL_FALSE, modelMatrix);
	glUniformMatrix4fv(view_matrix_uniform, 1, GL_FALSE, viewMatrix);
	glUniformMatrix4fv(projection_matrix_uniform, 1, GL_FALSE, gPerspectiveProjectionMatrix);

	// *** bind vao ***
	glBindVertexArray(gVao_sphere);

	// *** draw, either by glDrawTriangles() or glDrawArrays() or glDrawElements()
	// 11
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, gVbo_sphere_element);
	glDrawElements(GL_TRIANGLES, gNumElements, GL_UNSIGNED_SHORT, 0);

	MapViewport(winWidth / 6, 4 * winHeight / 6, winWidth / 6, winHeight / 6);
	// 12
	if (gbLight == true) {
	glUniform3fv(Ka_uniform, 1, material_jade_ambient);
	glUniform3fv(Kd_uniform, 1, material_jade_diffuse);
	glUniform3fv(Ks_uniform, 1, material_jade_specular);
	glUniform1f(material_shininess_uniform, material_jade_shininess);
	}
	modelMatrix = mat4::identity();
	modelMatrix = translate(0.0f, 0.0f, -2.0f);
	//modelMatrix = modelMatrix * scale(0.3f, 0.3f, 0.3f);
	glUniformMatrix4fv(model_matrix_uniform, 1, GL_FALSE, modelMatrix);
	glUniformMatrix4fv(view_matrix_uniform, 1, GL_FALSE, viewMatrix);
	glUniformMatrix4fv(projection_matrix_uniform, 1, GL_FALSE, gPerspectiveProjectionMatrix);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, gVbo_sphere_element);
	glDrawElements(GL_TRIANGLES, gNumElements, GL_UNSIGNED_SHORT, 0);

	MapViewport(winWidth / 6,   winHeight / 2, winWidth / 6, winHeight / 6);
	// 13
	if (gbLight == true) {
	glUniform3fv(Ka_uniform, 1, material_obsidian_ambient);
	glUniform3fv(Kd_uniform, 1, material_obsidian_diffuse);
	glUniform3fv(Ks_uniform, 1, material_obsidian_specular);
	glUniform1f(material_shininess_uniform, material_obsidian_shininess);
	}
	modelMatrix = mat4::identity();
	modelMatrix = translate(0.0f, 0.0f, -2.0f);
	
	glUniformMatrix4fv(model_matrix_uniform, 1, GL_FALSE, modelMatrix);
	glUniformMatrix4fv(view_matrix_uniform, 1, GL_FALSE, viewMatrix);
	glUniformMatrix4fv(projection_matrix_uniform, 1, GL_FALSE, gPerspectiveProjectionMatrix);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, gVbo_sphere_element);
	glDrawElements(GL_TRIANGLES, gNumElements, GL_UNSIGNED_SHORT, 0);

	MapViewport(winWidth / 6, winHeight / 3, winWidth / 6, winHeight / 6);
	// 14
	if (gbLight == true) {
	glUniform3fv(Ka_uniform, 1, material_pearl_ambient);
	glUniform3fv(Kd_uniform, 1, material_pearl_diffuse);
	glUniform3fv(Ks_uniform, 1, material_pearl_specular);
	glUniform1f(material_shininess_uniform, material_pearl_shininess);
	}
	modelMatrix = mat4::identity();
	modelMatrix = translate(0.0f, 0.0f, -2.0f);
	glUniformMatrix4fv(model_matrix_uniform, 1, GL_FALSE, modelMatrix);
	glUniformMatrix4fv(view_matrix_uniform, 1, GL_FALSE, viewMatrix);
	glUniformMatrix4fv(projection_matrix_uniform, 1, GL_FALSE, gPerspectiveProjectionMatrix);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, gVbo_sphere_element);
	glDrawElements(GL_TRIANGLES, gNumElements, GL_UNSIGNED_SHORT, 0);

	MapViewport(winWidth / 6, winHeight / 6, winWidth / 6, winHeight / 6);
	// 15
	if (gbLight == true) {
	glUniform3fv(Ka_uniform, 1, material_ruby_ambient);
	glUniform3fv(Kd_uniform, 1, material_ruby_diffuse);
	glUniform3fv(Ks_uniform, 1, material_ruby_specular);
	glUniform1f(material_shininess_uniform, material_ruby_shininess);
	}
	modelMatrix = mat4::identity();
	modelMatrix = translate(0.0f, 0.0f, -2.0f);
	glUniformMatrix4fv(model_matrix_uniform, 1, GL_FALSE, modelMatrix);
	glUniformMatrix4fv(view_matrix_uniform, 1, GL_FALSE, viewMatrix);
	glUniformMatrix4fv(projection_matrix_uniform, 1, GL_FALSE, gPerspectiveProjectionMatrix);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, gVbo_sphere_element);
	glDrawElements(GL_TRIANGLES, gNumElements, GL_UNSIGNED_SHORT, 0);

	MapViewport(winWidth / 6, 0, winWidth / 6, winHeight / 6);
	// 16
	if (gbLight == true) {
	glUniform3fv(Ka_uniform, 1, material_turquoise_ambient);
	glUniform3fv(Kd_uniform, 1, material_turquoise_diffuse);
	glUniform3fv(Ks_uniform, 1, material_turquoise_specular);
	glUniform1f(material_shininess_uniform, material_turquoise_shininess);
	}
	modelMatrix = mat4::identity();
	modelMatrix = translate(0.0f, 0.0f, -2.0f);
	glUniformMatrix4fv(model_matrix_uniform, 1, GL_FALSE, modelMatrix);
	glUniformMatrix4fv(view_matrix_uniform, 1, GL_FALSE, viewMatrix);
	glUniformMatrix4fv(projection_matrix_uniform, 1, GL_FALSE, gPerspectiveProjectionMatrix);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, gVbo_sphere_element);
	glDrawElements(GL_TRIANGLES, gNumElements, GL_UNSIGNED_SHORT, 0);

	MapViewport(2*winWidth / 6, 5*winHeight / 6, winWidth / 6, winHeight / 6);
	// 21
	if (gbLight == true) {
	glUniform3fv(Ka_uniform, 1, material_brass_ambient);
	glUniform3fv(Kd_uniform, 1, material_brass_diffuse);
	glUniform3fv(Ks_uniform, 1, material_brass_specular);
	glUniform1f(material_shininess_uniform, material_brass_shininess);
	}
	modelMatrix = mat4::identity();
	modelMatrix = translate(0.0f, 0.0f, -2.0f);
	glUniformMatrix4fv(model_matrix_uniform, 1, GL_FALSE, modelMatrix);
	glUniformMatrix4fv(view_matrix_uniform, 1, GL_FALSE, viewMatrix);
	glUniformMatrix4fv(projection_matrix_uniform, 1, GL_FALSE, gPerspectiveProjectionMatrix);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, gVbo_sphere_element);
	glDrawElements(GL_TRIANGLES, gNumElements, GL_UNSIGNED_SHORT, 0);

	MapViewport(2 * winWidth / 6, 4 * winHeight / 6, winWidth / 6, winHeight / 6);
	// 22
	if (gbLight == true) {
	glUniform3fv(Ka_uniform, 1, material_bronze_ambient);
	glUniform3fv(Kd_uniform, 1, material_bronze_diffuse);
	glUniform3fv(Ks_uniform, 1, material_bronze_specular);
	glUniform1f(material_shininess_uniform, material_bronze_shininess);
	}
	modelMatrix = mat4::identity();
	modelMatrix = translate(0.0f, 0.0f, -2.0f);
	glUniformMatrix4fv(model_matrix_uniform, 1, GL_FALSE, modelMatrix);
	glUniformMatrix4fv(view_matrix_uniform, 1, GL_FALSE, viewMatrix);
	glUniformMatrix4fv(projection_matrix_uniform, 1, GL_FALSE, gPerspectiveProjectionMatrix);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, gVbo_sphere_element);
	glDrawElements(GL_TRIANGLES, gNumElements, GL_UNSIGNED_SHORT, 0);

	MapViewport(2 * winWidth / 6, 3 * winHeight / 6, winWidth / 6, winHeight / 6);
	// 23
	if (gbLight == true) {
	glUniform3fv(Ka_uniform, 1, material_chrome_ambient);
	glUniform3fv(Kd_uniform, 1, material_chrome_diffuse);
	glUniform3fv(Ks_uniform, 1, material_chrome_specular);
	glUniform1f(material_shininess_uniform, material_chrome_shininess);
	}
	modelMatrix = mat4::identity();
	modelMatrix = translate(0.0f, 0.0f, -2.0f);
	glUniformMatrix4fv(model_matrix_uniform, 1, GL_FALSE, modelMatrix);
	glUniformMatrix4fv(view_matrix_uniform, 1, GL_FALSE, viewMatrix);
	glUniformMatrix4fv(projection_matrix_uniform, 1, GL_FALSE, gPerspectiveProjectionMatrix);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, gVbo_sphere_element);
	glDrawElements(GL_TRIANGLES, gNumElements, GL_UNSIGNED_SHORT, 0);
	
	MapViewport(2 * winWidth / 6, 2 * winHeight / 6, winWidth / 6, winHeight / 6);
	// 24
	if (gbLight == true) {
	glUniform3fv(Ka_uniform, 1, material_copper_ambient);
	glUniform3fv(Kd_uniform, 1, material_copper_diffuse);
	glUniform3fv(Ks_uniform, 1, material_copper_specular);
	glUniform1f(material_shininess_uniform, material_copper_shininess);
	}
	modelMatrix = mat4::identity();
	modelMatrix = translate(0.256f, 0.22f, -2.0f);
	modelMatrix = translate(0.0f, 0.0f, -2.0f);
	glUniformMatrix4fv(model_matrix_uniform, 1, GL_FALSE, modelMatrix);
	glUniformMatrix4fv(view_matrix_uniform, 1, GL_FALSE, viewMatrix);
	glUniformMatrix4fv(projection_matrix_uniform, 1, GL_FALSE, gPerspectiveProjectionMatrix);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, gVbo_sphere_element);
	glDrawElements(GL_TRIANGLES, gNumElements, GL_UNSIGNED_SHORT, 0);

	MapViewport(2 * winWidth / 6,  winHeight / 6, winWidth / 6, winHeight / 6);
	// 25
	if (gbLight == true) {
	glUniform3fv(Ka_uniform, 1, material_gold_ambient);
	glUniform3fv(Kd_uniform, 1, material_gold_diffuse);
	glUniform3fv(Ks_uniform, 1, material_gold_specular);
	glUniform1f(material_shininess_uniform, material_gold_shininess);
	}
	modelMatrix = mat4::identity();
	modelMatrix = translate(0.0f, 0.0f, -2.0f);
	glUniformMatrix4fv(model_matrix_uniform, 1, GL_FALSE, modelMatrix);
	glUniformMatrix4fv(view_matrix_uniform, 1, GL_FALSE, viewMatrix);
	glUniformMatrix4fv(projection_matrix_uniform, 1, GL_FALSE, gPerspectiveProjectionMatrix);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, gVbo_sphere_element);
	glDrawElements(GL_TRIANGLES, gNumElements, GL_UNSIGNED_SHORT, 0);

	MapViewport(2 * winWidth / 6,  0, winWidth / 6, winHeight / 6);
	// 26
	if (gbLight == true) {
	glUniform3fv(Ka_uniform, 1, material_silver_ambient);
	glUniform3fv(Kd_uniform, 1, material_silver_diffuse);
	glUniform3fv(Ks_uniform, 1, material_silver_specular);
	glUniform1f(material_shininess_uniform, material_silver_shininess);
	}
	modelMatrix = mat4::identity();
	modelMatrix = translate(0.0f, 0.0f, -2.0f);
	glUniformMatrix4fv(model_matrix_uniform, 1, GL_FALSE, modelMatrix);
	glUniformMatrix4fv(view_matrix_uniform, 1, GL_FALSE, viewMatrix);
	glUniformMatrix4fv(projection_matrix_uniform, 1, GL_FALSE, gPerspectiveProjectionMatrix);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, gVbo_sphere_element);
	glDrawElements(GL_TRIANGLES, gNumElements, GL_UNSIGNED_SHORT, 0);
	
	MapViewport(3 * winWidth / 6, 5*winHeight / 6, winWidth / 6, winHeight / 6);
	// 31
	if (gbLight == true) {
	glUniform3fv(Ka_uniform, 1, material_black_ambient);
	glUniform3fv(Kd_uniform, 1, material_black_diffuse);
	glUniform3fv(Ks_uniform, 1, material_black_specular);
	glUniform1f(material_shininess_uniform, material_black_shininess);
	}
	modelMatrix = mat4::identity();
	modelMatrix = translate(0.0f, 0.0f, -2.0f);
	glUniformMatrix4fv(model_matrix_uniform, 1, GL_FALSE, modelMatrix);
	glUniformMatrix4fv(view_matrix_uniform, 1, GL_FALSE, viewMatrix);
	glUniformMatrix4fv(projection_matrix_uniform, 1, GL_FALSE, gPerspectiveProjectionMatrix);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, gVbo_sphere_element);
	glDrawElements(GL_TRIANGLES, gNumElements, GL_UNSIGNED_SHORT, 0);
	
	MapViewport(3 * winWidth / 6, 4 * winHeight / 6, winWidth / 6, winHeight / 6);
	// 32
	if (gbLight == true) {
	glUniform3fv(Ka_uniform, 1, material_cyan_ambient);
	glUniform3fv(Kd_uniform, 1, material_cyan_diffuse);
	glUniform3fv(Ks_uniform, 1, material_cyan_specular);
	glUniform1f(material_shininess_uniform, material_cyan_shininess);
	}
	modelMatrix = mat4::identity();
	modelMatrix = translate(0.0f, 0.0f, -2.0f);
	glUniformMatrix4fv(model_matrix_uniform, 1, GL_FALSE, modelMatrix);
	glUniformMatrix4fv(view_matrix_uniform, 1, GL_FALSE, viewMatrix);
	glUniformMatrix4fv(projection_matrix_uniform, 1, GL_FALSE, gPerspectiveProjectionMatrix);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, gVbo_sphere_element);
	glDrawElements(GL_TRIANGLES, gNumElements, GL_UNSIGNED_SHORT, 0);

	MapViewport(3 * winWidth / 6, 3 * winHeight / 6, winWidth / 6, winHeight / 6);
	// 33
	if (gbLight == true) {
	glUniform3fv(Ka_uniform, 1, material_green_ambient);
	glUniform3fv(Kd_uniform, 1, material_green_diffuse);
	glUniform3fv(Ks_uniform, 1, material_green_specular);
	glUniform1f(material_shininess_uniform, material_green_shininess);
	}
	modelMatrix = mat4::identity();
	modelMatrix = translate(0.0f, 0.0f, -2.0f);
	glUniformMatrix4fv(model_matrix_uniform, 1, GL_FALSE, modelMatrix);
	glUniformMatrix4fv(view_matrix_uniform, 1, GL_FALSE, viewMatrix);
	glUniformMatrix4fv(projection_matrix_uniform, 1, GL_FALSE, gPerspectiveProjectionMatrix);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, gVbo_sphere_element);
	glDrawElements(GL_TRIANGLES, gNumElements, GL_UNSIGNED_SHORT, 0);

	MapViewport(3 * winWidth / 6, 2 * winHeight / 6, winWidth / 6, winHeight / 6);
	// 34
	if (gbLight == true) {
	glUniform3fv(Ka_uniform, 1, material_red_ambient);
	glUniform3fv(Kd_uniform, 1, material_red_diffuse);
	glUniform3fv(Ks_uniform, 1, material_red_specular);
	glUniform1f(material_shininess_uniform, material_red_shininess);
	}
	modelMatrix = mat4::identity();
	modelMatrix = translate(0.0f, 0.0f, -2.0f);
	glUniformMatrix4fv(model_matrix_uniform, 1, GL_FALSE, modelMatrix);
	glUniformMatrix4fv(view_matrix_uniform, 1, GL_FALSE, viewMatrix);
	glUniformMatrix4fv(projection_matrix_uniform, 1, GL_FALSE, gPerspectiveProjectionMatrix);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, gVbo_sphere_element);
	glDrawElements(GL_TRIANGLES, gNumElements, GL_UNSIGNED_SHORT, 0);

	MapViewport(3 * winWidth / 6, winHeight / 6, winWidth / 6, winHeight / 6);
	// 35
	if (gbLight == true) {
	glUniform3fv(Ka_uniform, 1, material_white_ambient);
	glUniform3fv(Kd_uniform, 1, material_white_diffuse);
	glUniform3fv(Ks_uniform, 1, material_white_specular);
	glUniform1f(material_shininess_uniform, material_white_shininess);
	}
	modelMatrix = mat4::identity();
	modelMatrix = translate(0.0f, 0.0f, -2.0f);
	glUniformMatrix4fv(model_matrix_uniform, 1, GL_FALSE, modelMatrix);
	glUniformMatrix4fv(view_matrix_uniform, 1, GL_FALSE, viewMatrix);
	glUniformMatrix4fv(projection_matrix_uniform, 1, GL_FALSE, gPerspectiveProjectionMatrix);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, gVbo_sphere_element);
	glDrawElements(GL_TRIANGLES, gNumElements, GL_UNSIGNED_SHORT, 0);

	MapViewport(3 * winWidth / 6, 0, winWidth / 6, winHeight / 6);
	// 36
	if (gbLight == true) {
	glUniform3fv(Ka_uniform, 1, material_yellow_ambient);
	glUniform3fv(Kd_uniform, 1, material_yellow_diffuse);
	glUniform3fv(Ks_uniform, 1, material_yellow_specular);
	glUniform1f(material_shininess_uniform, material_yellow_shininess);
	}
	modelMatrix = mat4::identity();
	modelMatrix = translate(0.0f, 0.0f, -2.0f);
	glUniformMatrix4fv(model_matrix_uniform, 1, GL_FALSE, modelMatrix);
	glUniformMatrix4fv(view_matrix_uniform, 1, GL_FALSE, viewMatrix);
	glUniformMatrix4fv(projection_matrix_uniform, 1, GL_FALSE, gPerspectiveProjectionMatrix);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, gVbo_sphere_element);
	glDrawElements(GL_TRIANGLES, gNumElements, GL_UNSIGNED_SHORT, 0);

	MapViewport(4 * winWidth / 6, 5*winHeight / 6, winWidth / 6, winHeight / 6);
	// 41
	if (gbLight == true) {
	glUniform3fv(Ka_uniform, 1, material_black2_ambient);
	glUniform3fv(Kd_uniform, 1, material_black2_diffuse);
	glUniform3fv(Ks_uniform, 1, material_black2_specular);
	glUniform1f(material_shininess_uniform, material_black2_shininess);
	}
	modelMatrix = mat4::identity();
	modelMatrix = translate(0.0f, 0.0f, -2.0f);
	glUniformMatrix4fv(model_matrix_uniform, 1, GL_FALSE, modelMatrix);
	glUniformMatrix4fv(view_matrix_uniform, 1, GL_FALSE, viewMatrix);
	glUniformMatrix4fv(projection_matrix_uniform, 1, GL_FALSE, gPerspectiveProjectionMatrix);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, gVbo_sphere_element);
	glDrawElements(GL_TRIANGLES, gNumElements, GL_UNSIGNED_SHORT, 0);

	MapViewport(4 * winWidth / 6, 4 * winHeight / 6, winWidth / 6, winHeight / 6);
	// 42
	if (gbLight == true) {
	glUniform3fv(Ka_uniform, 1, material_cyan2_ambient);
	glUniform3fv(Kd_uniform, 1, material_cyan2_diffuse);
	glUniform3fv(Ks_uniform, 1, material_cyan2_specular);
	glUniform1f(material_shininess_uniform, material_cyan2_shininess);
	}
	modelMatrix = mat4::identity();
	modelMatrix = translate(0.0f, 0.0f, -2.0f);
	glUniformMatrix4fv(model_matrix_uniform, 1, GL_FALSE, modelMatrix);
	glUniformMatrix4fv(view_matrix_uniform, 1, GL_FALSE, viewMatrix);
	glUniformMatrix4fv(projection_matrix_uniform, 1, GL_FALSE, gPerspectiveProjectionMatrix);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, gVbo_sphere_element);
	glDrawElements(GL_TRIANGLES, gNumElements, GL_UNSIGNED_SHORT, 0);

	MapViewport(4 * winWidth / 6, 3 * winHeight / 6, winWidth / 6, winHeight / 6);
	// 43
	if (gbLight == true) {
	glUniform3fv(Ka_uniform, 1, material_green2_ambient);
	glUniform3fv(Kd_uniform, 1, material_green2_diffuse);
	glUniform3fv(Ks_uniform, 1, material_green2_specular);
	glUniform1f(material_shininess_uniform, material_green2_shininess);
	}
	modelMatrix = mat4::identity();
	modelMatrix = translate(0.0f, 0.0f, -2.0f);
	glUniformMatrix4fv(model_matrix_uniform, 1, GL_FALSE, modelMatrix);
	glUniformMatrix4fv(view_matrix_uniform, 1, GL_FALSE, viewMatrix);
	glUniformMatrix4fv(projection_matrix_uniform, 1, GL_FALSE, gPerspectiveProjectionMatrix);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, gVbo_sphere_element);
	glDrawElements(GL_TRIANGLES, gNumElements, GL_UNSIGNED_SHORT, 0);

	MapViewport(4 * winWidth / 6, 2 * winHeight / 6, winWidth / 6, winHeight / 6);
	// 44
	if (gbLight == true) {
	glUniform3fv(Ka_uniform, 1, material_red2_ambient);
	glUniform3fv(Kd_uniform, 1, material_red2_diffuse);
	glUniform3fv(Ks_uniform, 1, material_red2_specular);
	glUniform1f(material_shininess_uniform, material_red2_shininess);
	}
	modelMatrix = mat4::identity();
	modelMatrix = translate(0.0f, 0.0f, -2.0f);
	glUniformMatrix4fv(model_matrix_uniform, 1, GL_FALSE, modelMatrix);
	glUniformMatrix4fv(view_matrix_uniform, 1, GL_FALSE, viewMatrix);
	glUniformMatrix4fv(projection_matrix_uniform, 1, GL_FALSE, gPerspectiveProjectionMatrix);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, gVbo_sphere_element);
	glDrawElements(GL_TRIANGLES, gNumElements, GL_UNSIGNED_SHORT, 0);

	MapViewport(4 * winWidth / 6, winHeight / 6, winWidth / 6, winHeight / 6);
	// 45
	if (gbLight == true) {
	glUniform3fv(Ka_uniform, 1, material_white2_ambient);
	glUniform3fv(Kd_uniform, 1, material_white2_diffuse);
	glUniform3fv(Ks_uniform, 1, material_white2_specular);
	glUniform1f(material_shininess_uniform, material_white2_shininess);
	}
	modelMatrix = mat4::identity();
	modelMatrix = translate(0.0f, 0.0f, -2.0f);
	glUniformMatrix4fv(model_matrix_uniform, 1, GL_FALSE, modelMatrix);
	glUniformMatrix4fv(view_matrix_uniform, 1, GL_FALSE, viewMatrix);
	glUniformMatrix4fv(projection_matrix_uniform, 1, GL_FALSE, gPerspectiveProjectionMatrix);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, gVbo_sphere_element);
	glDrawElements(GL_TRIANGLES, gNumElements, GL_UNSIGNED_SHORT, 0);

	MapViewport(4 * winWidth / 6, 0, winWidth / 6, winHeight / 6);
	// 46
	if (gbLight == true) {
	glUniform3fv(Ka_uniform, 1, material_yellow2_ambient);
	glUniform3fv(Kd_uniform, 1, material_yellow2_diffuse);
	glUniform3fv(Ks_uniform, 1, material_yellow2_specular);
	glUniform1f(material_shininess_uniform, material_yellow2_shininess);
	}
	modelMatrix = mat4::identity();
	modelMatrix = translate(0.0f, 0.0f, -2.0f);
	glUniformMatrix4fv(model_matrix_uniform, 1, GL_FALSE, modelMatrix);
	glUniformMatrix4fv(view_matrix_uniform, 1, GL_FALSE, viewMatrix);
	glUniformMatrix4fv(projection_matrix_uniform, 1, GL_FALSE, gPerspectiveProjectionMatrix);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, gVbo_sphere_element);
	glDrawElements(GL_TRIANGLES, gNumElements, GL_UNSIGNED_SHORT, 0);

	// *** unbind vao ***
	glBindVertexArray(0);


	glUseProgram(0);
	glXSwapBuffers(gpDisplay, gWindow);
}

void MapViewport(int x, int y, int width, int height) {

	//code
	if (height == 0)
		height = 1;
	glViewport(x, y, (GLsizei)width, (GLsizei)height);
	gPerspectiveProjectionMatrix = perspective(45.0f, (GLfloat)width / (GLfloat)height, 0.1f, 100.0f);
}
void update() {
	gfLightAngle = (gfLightAngle + 0.01f);
	if (gfLightAngle >= 360.0f)
		gfLightAngle = 0.0f;
}


void resize(int width, int height){
	//code
	if(height == 0)
		height = 1;
	glViewport(0, 0, (GLsizei)width, (GLsizei)height);
	if (width <= height)
	{
		gPerspectiveProjectionMatrix = vmath::perspective(60.0f,(GLfloat)height/(GLfloat)width, 0.1f, 30.0f);
	}
	else
	{
		gPerspectiveProjectionMatrix = vmath::perspective(60.0f, (GLfloat)width / (GLfloat)height, 0.1f, 30.0f);
	}
	
}


void uninitialize(void){
	if (gVao_sphere)
	{
		glDeleteVertexArrays(1, &gVao_sphere);
		gVao_sphere = 0;
	}



	// destroy vbo
	if (gVbo_sphere_position)
	{
		glDeleteBuffers(1, &gVbo_sphere_position);
		gVbo_sphere_position = 0;
	}
	if (gVbo_sphere_normal)
	{
		glDeleteBuffers(1, &gVbo_sphere_normal);
		gVbo_sphere_normal = 0;
	}
	if (gVbo_sphere_element)
	{
		glDeleteBuffers(1, &gVbo_sphere_element);
		gVbo_sphere_element = 0;
	}
	// Detach vertex shader from shader program object
	glDetachShader(gShaderProgramObject, gVertexShaderObject);
	// Detach fragment shader from shader program object
	glDetachShader(gShaderProgramObject, gVertexShaderObject);

	// Delete vertex shader object
	glDeleteShader(gVertexShaderObject);
	gVertexShaderObject = 0;
	// Delte fragment shader object
	glDeleteShader(gFragmentShaderObject);
	gFragmentShaderObject = 0;

	// Delete shader program object
	glDeleteProgram(gShaderProgramObject);
	gShaderProgramObject = 0;

	// Unlink shader program
	glUseProgram(0);

	GLXContext currentGLXContext;
	currentGLXContext = glXGetCurrentContext();
	if(currentGLXContext != NULL &&currentGLXContext== gGLXContext)
	{
		glXMakeCurrent(gpDisplay, 0,0);
	}
	
	if(gGLXContext)
	{
		glXDestroyContext(gpDisplay, gGLXContext);
	}
	if(gWindow)
	{
		XDestroyWindow(gpDisplay, gWindow);
	}
	if(gColormap)
	{
		XFreeColormap(gpDisplay, gColormap);
	}
	if(gpXVisualInfo)
	{
		free(gpXVisualInfo);
		gpXVisualInfo == NULL;
	}
	if(gpDisplay)
	{
		XCloseDisplay(gpDisplay);
		gpDisplay == NULL;
	}	
}

