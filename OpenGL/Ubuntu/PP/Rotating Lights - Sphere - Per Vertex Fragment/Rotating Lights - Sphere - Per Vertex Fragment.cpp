#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <memory.h>
#include <X11/Xlib.h>
#include <X11/Xutil.h>
#include <X11/XKBlib.h>
#include <X11/keysym.h>

#include <GL/glew.h>
#include <GL/gl.h>
#include <GL/glx.h>
#include "Sphere.h"
#include "vmath.h"
#pragma comment(lib, "Sphere.lib")
// Namespace
using namespace std;
using namespace vmath;

enum
{
	VDG_ATTRIBUTE_VERTEX = 0,
	VDG_ATTRIBUTE_COLOR,
	VDG_ATTRIBUTE_NORMAL,
	VDG_ATTRIBUTE_TEXTURE0,
};

bool bFullScreen = false;
Display *gpDisplay = NULL;
XVisualInfo *gpXVisualInfo = NULL;
Colormap gColormap;
Window gWindow;
int giWindowWidth = 800;
int giWindowHeight = 600;

GLXContext gGLXContext;

FILE *gpFile = NULL;

GLuint gPerVertex_VertexShaderObject = false;
GLuint gPerVertex_FragmentShaderObject = false;
GLuint gPerVertexShaderProgramObject = false;

GLuint gPerFragment_VertexShaderObject = false;
GLuint gPerFragment_FragmentShaderObject = false;
GLuint gPerFragmentShaderProgramObject = false;
int gbPerVertexMode = 1;
GLuint gNumElements;
GLuint gNumVertices;
float sphere_vertices[1146];
float sphere_normals[1146];
float sphere_textures[764];
unsigned short sphere_elements[2280];

GLuint gVao_sphere;
GLuint gVbo_sphere_position;
GLuint gVbo_sphere_normal;
GLuint gVbo_sphere_element;
GLuint model_matrix_uniform_perVer, view_matrix_uniform_perVer, projection_matrix_uniform_perVer;
GLuint L_KeyPressed_uniform_perVer;


GLuint model_matrix_uniform_perFrag, view_matrix_uniform_perFrag, projection_matrix_uniform_perFrag;
GLuint L_KeyPressed_uniform_perFrag;

GLuint red_La_uniform_perVer;
GLuint red_Ld_uniform_perVer;
GLuint red_Ls_uniform_perVer;
GLuint red_light_position_uniform_perVer;

GLuint green_La_uniform_perVer;
GLuint green_Ld_uniform_perVer;
GLuint green_Ls_uniform_perVer;
GLuint green_light_position_uniform_perVer;

GLuint blue_La_uniform_perVer;
GLuint blue_Ld_uniform_perVer;
GLuint blue_Ls_uniform_perVer;
GLuint blue_light_position_uniform_perVer;


GLuint Ka_uniform_perVer;
GLuint Kd_uniform_perVer;
GLuint Ks_uniform_perVer;
GLuint material_shininess_uniform_perVer;

/****************Per fragment uniforms ************************/
GLuint red_La_uniform_perFrag;
GLuint red_Ld_uniform_perFrag;
GLuint red_Ls_uniform_perFrag;
GLuint red_light_position_uniform_perFrag;

GLuint green_La_uniform_perFrag;
GLuint green_Ld_uniform_perFrag;
GLuint green_Ls_uniform_perFrag;
GLuint green_light_position_uniform_perFrag;

GLuint blue_La_uniform_perFrag;
GLuint blue_Ld_uniform_perFrag;
GLuint blue_Ls_uniform_perFrag;
GLuint blue_light_position_uniform_perFrag;


GLuint Ka_uniform_perFrag;
GLuint Kd_uniform_perFrag;
GLuint Ks_uniform_perFrag;
GLuint material_shininess_uniform_perFrag;

mat4 gPerspectiveProjectionMatrix;
GLfloat gfRedLightAngle = 0.0f, gfGreenLightAngle = 0.0f, gfBlueLightAngle = 0.0f;
bool gbLight;


GLfloat red_lightAmbient[] = { 0.0f, 0.0f, 0.0f, 0.0f };
GLfloat red_lightDiffuse[] = {1.0f, 0.0f, 0.0f, 0.0f };
GLfloat red_lightSpecular[] = { 1.0f, 0.0f, 0.0f, 0.0f };
GLfloat red_lightPosition[] = { 0.0f, 0.0f, 0.0f, 0.0f };

GLfloat green_lightAmbient[] = { 0.0f, 0.0f, 0.0f, 0.0f };
GLfloat green_lightDiffuse[] = { 0.0f, 1.0f, 0.0f, 0.0f };
GLfloat green_lightSpecular[] = { 0.0f, 1.0f, 0.0f, 0.0f };
GLfloat green_lightPosition[] = { 0.0f, 0.0f, 0.0f, 0.0f };

GLfloat blue_lightAmbient[] = { 0.0f, 0.0f, 0.0f, 0.0f };
GLfloat blue_lightDiffuse[] = { 0.0f, 0.0f, 1.0f, 0.0f };
GLfloat blue_lightSpecular[] = { 0.0f, 0.0f, 1.0f, 0.0f };
GLfloat blue_lightPosition[] = { 0.0f, 0.0f, 0.0f, 0.0f };

GLfloat material_ambient[] = { 0.0f,0.0f,0.0f,1.0f };
GLfloat material_diffuse[] = { 1.0f,1.0f,1.0f,1.0f };
GLfloat material_specular[] = { 1.0f,1.0f,1.0f,1.0f };
GLfloat material_shininess = 50.0f;



int main(void){
	//function declarations
	void CreateWindow(void);
	void ToggleFullScreen(void);
	void initialize(void);
	void display(void);
	void uninitialize();
	void resize(int, int);	
	void update(void);
	//variable declarations
	int winWidth = giWindowWidth;
	int winHeight = giWindowHeight;
	static bool bIsLKeyPressed = false;
	bool bDone = false;
	gpFile = fopen("Log.txt", "w");
	if (gpFile == 0)
	{
		printf("Log file can not be created.\n Exitting...");
		exit(0);
	}
	else
	{
		fprintf(gpFile, "Log file successfully Opened.\n");
	}
	CreateWindow();
	initialize();

	//message loop
	XEvent event;
	KeySym keysym;
	
	while(bDone == false){
		while(XPending(gpDisplay)){
			XNextEvent(gpDisplay, &event);
			switch(event.type){
				case MapNotify:
					break;
				case KeyPress:
					keysym = XkbKeycodeToKeysym(gpDisplay, event.xkey.keycode, 0, 0);
					switch(keysym){
						case XK_Escape:
							if(bFullScreen == false){
								ToggleFullScreen();
								bFullScreen = true;
							}else{
								ToggleFullScreen();
								bFullScreen = false;
							}
							break;
						case XK_q:
						case XK_Q:
							bDone = true;
							break;
						case XK_F:
						case XK_f:
							gbPerVertexMode = 0; //per fragment
							break;
						case XK_v:
						case XK_V:
							gbPerVertexMode = 1; //PerVertex
							break;
						case XK_l:
						case XK_L:
							if (bIsLKeyPressed == false)
							{
								gbLight = true;
								bIsLKeyPressed = true;
							}
							else
							{
								gbLight = false;
								bIsLKeyPressed = false;
							}
							break;
						default:
							break;

					}
					break;
				case ButtonPress:
					switch(event.xbutton.button){
						case 1: 
							break;
						case 2:
							break;
						case 3:
							break;
						default:
							break;
					}
					break;
				case MotionNotify:
					break;
				case ConfigureNotify:
					winWidth = event.xconfigure.width;
					winHeight = event.xconfigure.height;
					resize(winWidth, winHeight);
					break;
				case Expose:
					break;
				case DestroyNotify:
					break;
				case 33:
					bDone = true;
					break;
				default:
					break;
			}
		}
		update();
		display();
	}
	uninitialize();
	return (0);
}

void CreateWindow(void){
	//function prototypes
	void uninitialize(void);
	
	//variable declarations
	XSetWindowAttributes winAttribs;
	int defaultScreen;
	int defaultDepth;
	int styleMask;
	
	static int frameBufferAttributes[] = {
		GLX_RGBA,
		GLX_RED_SIZE, 8,
		GLX_GREEN_SIZE, 8,
		GLX_BLUE_SIZE, 8,
		GLX_ALPHA_SIZE, 8,
		GLX_DOUBLEBUFFER, True,
		GLX_DEPTH_SIZE, 24,
		None
	};

	//code
	gpDisplay = XOpenDisplay(NULL);
	if(gpDisplay == NULL){
		printf("Erroe: Unable to open X Display.\n Exiting now...\n");
		uninitialize();
		exit(1);
	}

	defaultScreen = XDefaultScreen(gpDisplay);
	

	gpXVisualInfo = glXChooseVisual(gpDisplay, defaultScreen, frameBufferAttributes);
	if (gpXVisualInfo == NULL) {
		printf("Error: Unable to allocate memory for Visual info.\nExitting Now.\n");
		uninitialize();
        	exit(1);
	}
	XMatchVisualInfo(gpDisplay, defaultScreen, defaultDepth, TrueColor, gpXVisualInfo);
	if(gpXVisualInfo == NULL) {
	    printf("Error: Unable to get VisualInfo. \nExitting...\n");
	    uninitialize();
	    exit(1);
	}
	winAttribs.border_pixmap = 0;
	winAttribs.border_pixel = 0;
	winAttribs.background_pixmap = 0;
	winAttribs.colormap = XCreateColormap(gpDisplay, RootWindow(gpDisplay, gpXVisualInfo->screen), gpXVisualInfo->visual, AllocNone);
        gColormap =  winAttribs.colormap;
	winAttribs.background_pixel =  BlackPixel(gpDisplay, defaultScreen);
	winAttribs.event_mask = ExposureMask | VisibilityChangeMask | ButtonPressMask | KeyPressMask | PointerMotionMask | StructureNotifyMask;

	styleMask =  CWBorderPixel | CWBackPixel | CWEventMask | CWColormap;

	gWindow = XCreateWindow(gpDisplay,
	RootWindow(gpDisplay, gpXVisualInfo->screen),
	0,  // X co-ordinate distance
	0,  // Y co-ordinate distance
	giWindowWidth, // Window width
	giWindowHeight, // Window height
	0, // Border Thickness, '0' is default
	gpXVisualInfo->depth,
	InputOutput,
	gpXVisualInfo->visual,
	styleMask,
	&winAttribs
	);

	if(!gWindow) {

	printf("Error: Failed to create main window.\nExitting... \n");
	uninitialize();
	exit(1);
	}
	
	XStoreName(gpDisplay, gWindow, "First OpenGL Window");
	
	Atom windowManagerDelete = XInternAtom(gpDisplay, "WM_DELETE_WINDOW", True);
	XSetWMProtocols(gpDisplay, gWindow, &windowManagerDelete, 1);
	XMapWindow(gpDisplay, gWindow);
}

void ToggleFullScreen(){

	Atom wm_state;
	Atom fullscreen;
	XEvent xev={0};

	//code
	wm_state = XInternAtom(gpDisplay, "_NET_WM_STATE", False);
	memset(&xev, 0, sizeof(xev));
	
	xev.type = ClientMessage;
	xev.xclient.window = gWindow;
	xev.xclient.message_type = wm_state;
	xev.xclient.format = 32;
	xev.xclient.data.l[0] = bFullScreen ? 0: 1;
	
	fullscreen = XInternAtom(gpDisplay, "_NET_WM_STATE_FULLSCREEN", False);
	xev.xclient.data.l[1] = fullscreen;
		
	XSendEvent(gpDisplay, RootWindow(gpDisplay, gpXVisualInfo->screen),
		  False,
		  StructureNotifyMask,
		  &xev);
}

void initialize(void){
	//function prototypes
	void resize(int, int);
	void uninitialize(void);
	void LoadGLTexture();
	gGLXContext = glXCreateContext(gpDisplay, gpXVisualInfo, NULL, GL_TRUE);
	glXMakeCurrent(gpDisplay, gWindow, gGLXContext);
	glewInit();
	gPerVertex_VertexShaderObject = glCreateShader(GL_VERTEX_SHADER);

	// provide source code to shader
	const GLchar *vertexShaderSourceCode =
		"#version 130" \
		"\n" \
		"in vec4 vPosition;" \
		"in vec3 vNormal;" \
		"uniform mat4 u_model_matrix;" \
		"uniform mat4 u_view_matrix;" \
		"uniform mat4 u_projection_matrix;" \
		"uniform int u_lighting_enabled;"\
		"uniform vec3 u_La_red;"\
		"uniform vec3 u_Ld_red;"\
		"uniform vec3 u_Ls_red;" \
		"uniform vec4 u_light_position_red;"\
		"uniform vec3 u_La_blue;"\
		"uniform vec3 u_Ld_blue;"\
		"uniform vec3 u_Ls_blue;" \
		"uniform vec4 u_light_position_green;"\
		"uniform vec3 u_La_green;"\
		"uniform vec3 u_Ld_green;"\
		"uniform vec3 u_Ls_green;" \
		"uniform vec4 u_light_position_blue;"\
		"uniform vec3 u_Ka;"\
		"uniform vec3 u_Ks;"\
		"uniform vec3 u_Kd;" \
		"uniform float u_material_shininess;" \
		"out vec3 phong_ads_color;" \
		"void main(void)" \
		"{" \
		"if (u_lighting_enabled == 1)" \
		"{" \
		"vec3 red_light;" \
		"vec3 blue_light;" \
		"vec3 green_light;" \
		"vec4 eye_coordinates=u_view_matrix * u_model_matrix * vPosition;" \
		"vec3 transformed_normals=normalize(mat3(u_view_matrix * u_model_matrix) * vNormal);" \
		"vec3 light_direction = normalize(vec3(u_light_position_red) - eye_coordinates.xyz);" \
		"float tn_dot_ld = max(dot(transformed_normals, light_direction),0.0);" \
		"vec3 ambient = u_La_red * u_Ka;" \
		"vec3 diffuse = u_Ld_red * u_Kd * tn_dot_ld;" \
		"vec3 reflection_vector = reflect(-light_direction, transformed_normals);" \
		"vec3 viewer_vector = normalize(-eye_coordinates.xyz);" \
		"vec3 specular = u_Ls_red * u_Ks * pow(max(dot(reflection_vector, viewer_vector), 0.0), u_material_shininess);" \
		"red_light=ambient + diffuse + specular;" \

		"light_direction = normalize(vec3(u_light_position_green) - eye_coordinates.xyz);" \
		"tn_dot_ld = max(dot(transformed_normals, light_direction),0.0);" \
		"ambient = u_La_green * u_Ka;" \
		"diffuse = u_Ld_green * u_Kd * tn_dot_ld;" \
		"reflection_vector = reflect(-light_direction, transformed_normals);" \
		"viewer_vector = normalize(-eye_coordinates.xyz);" \
		"specular = u_Ls_green * u_Ks * pow(max(dot(reflection_vector, viewer_vector), 0.0), u_material_shininess);" \
		"green_light = ambient + diffuse + specular;" \

		"light_direction = normalize(vec3(u_light_position_blue) - eye_coordinates.xyz);" \
		"tn_dot_ld = max(dot(transformed_normals, light_direction),0.0);" \
		"ambient = u_La_blue * u_Ka;" \
		"diffuse = u_Ld_blue * u_Kd * tn_dot_ld;" \
		"reflection_vector = reflect(-light_direction, transformed_normals);" \
		"viewer_vector = normalize(-eye_coordinates.xyz);" \
		"specular = u_Ls_blue * u_Ks * pow(max(dot(reflection_vector, viewer_vector), 0.0), u_material_shininess);" \
		"blue_light = ambient + diffuse + specular;" \
		"phong_ads_color=red_light + green_light + blue_light;" \

		"}" \
		"else" \
		"{" \
		"phong_ads_color= vec3(1.0, 1.0, 1.0);" \
		"}" \
		"gl_Position=u_projection_matrix * u_view_matrix * u_model_matrix * vPosition;" \
		"}";



	glShaderSource(gPerVertex_VertexShaderObject, 1, (const GLchar **)&vertexShaderSourceCode, NULL);

	// compile shader
	glCompileShader(gPerVertex_VertexShaderObject);
	GLint iInfoLogLength = 0;
	GLint iShaderCompiledStatus = 0;
	char *szInfoLog = NULL;
	glGetShaderiv(gPerVertex_VertexShaderObject, GL_COMPILE_STATUS, &iShaderCompiledStatus);
	if (iShaderCompiledStatus == GL_FALSE)
	{
		glGetShaderiv(gPerVertex_VertexShaderObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
		if (iInfoLogLength > 0)
		{
			szInfoLog = (char *)malloc(iInfoLogLength);
			if (szInfoLog != NULL)
			{
				GLsizei written;
				glGetShaderInfoLog(gPerVertex_VertexShaderObject, iInfoLogLength, &written, szInfoLog);
				fprintf(gpFile, "Vertex Shader Compilation Log : %s\n", szInfoLog);
				free(szInfoLog);
				uninitialize();
				exit(0);
			}
		}
	}

	// *** FRAGMENT SHADER ***
	// create shader
	gPerVertex_FragmentShaderObject = glCreateShader(GL_FRAGMENT_SHADER);

	// provide source code to shader
	const GLchar *fragmentShaderSourceCode =
		"#version 130" \
		"\n" \
		"in vec3 phong_ads_color;" \
		"out vec4 FragColor;" \
		"void main(void)" \
		"{" \
		"FragColor = vec4(phong_ads_color, 1.0);" \
		"}";

	glShaderSource(gPerVertex_FragmentShaderObject, 1, (const GLchar **)&fragmentShaderSourceCode, NULL);

	// compile shader
	glCompileShader(gPerVertex_FragmentShaderObject);
	glGetShaderiv(gPerVertex_FragmentShaderObject, GL_COMPILE_STATUS, &iShaderCompiledStatus);
	if (iShaderCompiledStatus == GL_FALSE)
	{
		glGetShaderiv(gPerVertex_FragmentShaderObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
		if (iInfoLogLength > 0)
		{
			szInfoLog = (char *)malloc(iInfoLogLength);
			if (szInfoLog != NULL)
			{
				GLsizei written;
				glGetShaderInfoLog(gPerVertex_FragmentShaderObject, iInfoLogLength, &written, szInfoLog);
				fprintf(gpFile, "Fragment Shader Compilation Log : %s\n", szInfoLog);
				free(szInfoLog);
				uninitialize();
				exit(0);
			}
		}
	}

	// *** SHADER PROGRAM ***
	// create
	gPerVertexShaderProgramObject = glCreateProgram();

	// attach vertex shader to shader program
	glAttachShader(gPerVertexShaderProgramObject, gPerVertex_VertexShaderObject);

	// attach fragment shader to shader program
	glAttachShader(gPerVertexShaderProgramObject, gPerVertex_FragmentShaderObject);

	// pre-link binding of shader program object with vertex shader position attribute
	glBindAttribLocation(gPerVertexShaderProgramObject, VDG_ATTRIBUTE_VERTEX, "vPosition");
	glBindAttribLocation(gPerVertexShaderProgramObject, VDG_ATTRIBUTE_NORMAL, "vNormal");

	// link shader
	glLinkProgram(gPerVertexShaderProgramObject);
	GLint iShaderProgramLinkStatus = 0;
	glGetProgramiv(gPerVertexShaderProgramObject, GL_LINK_STATUS, &iShaderProgramLinkStatus);
	if (iShaderProgramLinkStatus == GL_FALSE)
	{
		glGetProgramiv(gPerVertexShaderProgramObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
		if (iInfoLogLength>0)
		{
			szInfoLog = (char *)malloc(iInfoLogLength);
			if (szInfoLog != NULL)
			{
				GLsizei written;
				glGetProgramInfoLog(gPerVertexShaderProgramObject, iInfoLogLength, &written, szInfoLog);
				fprintf(gpFile, "Shader Program Link Log : %s\n", szInfoLog);
				free(szInfoLog);
				uninitialize();
				exit(0);
			}
		}
	}

	// get uniform locations
	model_matrix_uniform_perVer = glGetUniformLocation(gPerVertexShaderProgramObject, "u_model_matrix");
	view_matrix_uniform_perVer = glGetUniformLocation(gPerVertexShaderProgramObject, "u_view_matrix");
	projection_matrix_uniform_perVer = glGetUniformLocation(gPerVertexShaderProgramObject, "u_projection_matrix");


	L_KeyPressed_uniform_perVer = glGetUniformLocation(gPerVertexShaderProgramObject, "u_lighting_enabled");
	
	/*******************RED light***************************/
	// Ambient color intensity of light
	red_La_uniform_perVer = glGetUniformLocation(gPerVertexShaderProgramObject, "u_La_red");
	// Diffuse color intensity of light
	red_Ld_uniform_perVer = glGetUniformLocation(gPerVertexShaderProgramObject, "u_Ld_red");
	// Specular color intensity of light
	red_Ls_uniform_perVer = glGetUniformLocation(gPerVertexShaderProgramObject, "u_Ls_red");

	// Position of light
	red_light_position_uniform_perVer = glGetUniformLocation(gPerVertexShaderProgramObject, "u_light_position_red");

	/**************************GREEN light***************************/
	// Ambient color intensity of light
	green_La_uniform_perVer = glGetUniformLocation(gPerVertexShaderProgramObject, "u_La_green");
	// Diffuse color intensity of light
	green_Ld_uniform_perVer = glGetUniformLocation(gPerVertexShaderProgramObject, "u_Ld_green");
	// Specular color intensity of light
	green_Ls_uniform_perVer = glGetUniformLocation(gPerVertexShaderProgramObject, "u_Ls_green");

	// Position of light
	green_light_position_uniform_perVer = glGetUniformLocation(gPerVertexShaderProgramObject, "u_light_position_green");


	/**************************BLUE light***************************/
	// Ambient color intensity of light
	blue_La_uniform_perVer = glGetUniformLocation(gPerVertexShaderProgramObject, "u_La_blue");
	// Diffuse color intensity of light
	blue_Ld_uniform_perVer = glGetUniformLocation(gPerVertexShaderProgramObject, "u_Ld_blue");
	// Specular color intensity of light
	blue_Ls_uniform_perVer = glGetUniformLocation(gPerVertexShaderProgramObject, "u_Ls_blue");

	// Position of light
	blue_light_position_uniform_perVer = glGetUniformLocation(gPerVertexShaderProgramObject, "u_light_position_blue");

	// Ambient color reflective intensity of material
	Ka_uniform_perVer = glGetUniformLocation(gPerVertexShaderProgramObject, "u_Ka");
	// diffuse reflective color intensity of material
	Kd_uniform_perVer = glGetUniformLocation(gPerVertexShaderProgramObject, "u_Kd");
	// specular reflective color intensity of material
	Ks_uniform_perVer = glGetUniformLocation(gPerVertexShaderProgramObject, "u_Ks");
	// shininess of material ( value is conventionally between 1 to 200 )
	material_shininess_uniform_perVer = glGetUniformLocation(gPerVertexShaderProgramObject, "u_material_shininess");;


	// Per fragment 
	gPerFragment_VertexShaderObject = glCreateShader(GL_VERTEX_SHADER);

	// provide source code to shader
	vertexShaderSourceCode =
		"#version 130" \
		"\n" \
		"in vec4 vPosition;" \
		"in vec3 vNormal;" \
		"uniform mat4 u_model_matrix;" \
		"uniform mat4 u_view_matrix;" \
		"uniform mat4 u_projection_matrix;" \
		"uniform vec4 u_light_position_red;" \
		"uniform vec4 u_light_position_green;" \
		"uniform vec4 u_light_position_blue;" \
		"uniform int u_lighting_enabled;" \
		"out vec3 transformed_normals;" \
		"out vec3 light_direction_red;" \
		"out vec3 light_direction_green;" \
		"out vec3 light_direction_blue;" \
		"out vec3 viewer_vector;" \
		"void main(void)" \
		"{" \
		"if(u_lighting_enabled==1)" \
		"{" \
		"vec4 eye_coordinates=u_view_matrix * u_model_matrix * vPosition;" \
		"transformed_normals=mat3(u_view_matrix * u_model_matrix) * vNormal;" \
		"light_direction_red = vec3(u_light_position_red) - eye_coordinates.xyz;" \
		"light_direction_green = vec3(u_light_position_green) - eye_coordinates.xyz;" \
		"light_direction_blue = vec3(u_light_position_blue) - eye_coordinates.xyz;" \
		"viewer_vector = -eye_coordinates.xyz;" \
		"}" \
		"gl_Position=u_projection_matrix * u_view_matrix * u_model_matrix * vPosition;" \
		"}";


	glShaderSource(gPerFragment_VertexShaderObject, 1, (const GLchar **)&vertexShaderSourceCode, NULL);

	// compile shader
	glCompileShader(gPerFragment_VertexShaderObject);
	iInfoLogLength = 0;
	iShaderCompiledStatus = 0;
	szInfoLog = NULL;
	glGetShaderiv(gPerFragment_VertexShaderObject, GL_COMPILE_STATUS, &iShaderCompiledStatus);
	if (iShaderCompiledStatus == GL_FALSE)
	{
		glGetShaderiv(gPerFragment_VertexShaderObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
		if (iInfoLogLength > 0)
		{
			szInfoLog = (char *)malloc(iInfoLogLength);
			if (szInfoLog != NULL)
			{
				GLsizei written;
				glGetShaderInfoLog(gPerFragment_VertexShaderObject, iInfoLogLength, &written, szInfoLog);
				fprintf(gpFile, "Vertex Shader Compilation Log : %s\n", szInfoLog);
				free(szInfoLog);
				uninitialize();
				exit(0);
			}
		}
	}

	// *** FRAGMENT SHADER ***
	// create shader
	gPerFragment_FragmentShaderObject = glCreateShader(GL_FRAGMENT_SHADER);

	// provide source code to shader
	fragmentShaderSourceCode =
		"#version 130" \
		"\n" \
		"in vec3 transformed_normals;" \
		"in vec3 light_direction_red;" \
		"in vec3 light_direction_green;" \
		"in vec3 light_direction_blue;" \
		"in vec3 viewer_vector;" \
		"out vec4 FragColor;" \
		"uniform vec3 u_La_red;" \
		"uniform vec3 u_Ld_red;" \
		"uniform vec3 u_Ls_red;" \

		"uniform vec3 u_La_green;" \
		"uniform vec3 u_Ld_green;" \
		"uniform vec3 u_Ls_green;" \

		"uniform vec3 u_La_blue;" \
		"uniform vec3 u_Ld_blue;" \
		"uniform vec3 u_Ls_blue;" \
		"uniform vec3 u_Ka;" \
		"uniform vec3 u_Kd;" \
		"uniform vec3 u_Ks;" \
		"uniform float u_material_shininess;" \
		"uniform int u_lighting_enabled;" \
		"void main(void)" \
		"{" \
		"vec3 phong_ads_color;" \
		"vec3 red_light;" \
		"vec3 green_light;" \
		"vec3 blue_light;" \
		"if(u_lighting_enabled==1)" \
		"{" \

		"vec3 normalized_transformed_normals=normalize(transformed_normals);" \
		"vec3 normalized_light_direction=normalize(light_direction_red);" \
		"vec3 normalized_viewer_vector=normalize(viewer_vector);" \
		"vec3 ambient = u_La_red * u_Ka;" \
		"float tn_dot_ld = max(dot(normalized_transformed_normals, normalized_light_direction),0.0);" \
		"vec3 diffuse = u_Ld_red * u_Kd * tn_dot_ld;" \
		"vec3 reflection_vector = reflect(-normalized_light_direction, 				normalized_transformed_normals);" \
		"vec3 specular = u_Ls_red * u_Ks * pow(max(dot(reflection_vector, normalized_viewer_vector), 0.0), u_material_shininess);" \
		"red_light=ambient + diffuse + specular;" \

		"normalized_transformed_normals=normalize(transformed_normals);" \
		"normalized_light_direction=normalize(light_direction_green);" \
		"normalized_viewer_vector=normalize(viewer_vector);" \
		"ambient = u_La_green * u_Ka;" \
		"tn_dot_ld = max(dot(normalized_transformed_normals, normalized_light_direction),0.0);" \
		"diffuse = u_Ld_green * u_Kd * tn_dot_ld;" \
		"reflection_vector = reflect(-normalized_light_direction, normalized_transformed_normals);" \
		"specular = u_Ls_green * u_Ks * pow(max(dot(reflection_vector, normalized_viewer_vector), 0.0), u_material_shininess);" \
		"green_light=ambient + diffuse + specular;" \

		"normalized_transformed_normals=normalize(transformed_normals);" \
		"normalized_light_direction=normalize(light_direction_blue);" \
		"normalized_viewer_vector=normalize(viewer_vector);" \
		"ambient = u_La_blue * u_Ka;" \
		"tn_dot_ld = max(dot(normalized_transformed_normals, normalized_light_direction),0.0);" \
		"diffuse = u_Ld_blue * u_Kd * tn_dot_ld;" \
		"reflection_vector = reflect(-normalized_light_direction, normalized_transformed_normals);" \
		"specular = u_Ls_blue * u_Ks * pow(max(dot(reflection_vector, normalized_viewer_vector), 0.0), u_material_shininess);" \
		"blue_light=ambient + diffuse + specular;" \

		"phong_ads_color=red_light + green_light + blue_light;" \
		"}" \
		"else" \
		"{" \
		"phong_ads_color = vec3(1.0, 1.0, 1.0);" \
		"}" \
		"FragColor = vec4(phong_ads_color, 1.0);" \
		"}";

	glShaderSource(gPerFragment_FragmentShaderObject, 1, (const GLchar **)&fragmentShaderSourceCode, NULL);

	// compile shader
	glCompileShader(gPerFragment_FragmentShaderObject);
	glGetShaderiv(gPerFragment_FragmentShaderObject, GL_COMPILE_STATUS, &iShaderCompiledStatus);
	if (iShaderCompiledStatus == GL_FALSE)
	{
		glGetShaderiv(gPerFragment_FragmentShaderObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
		if (iInfoLogLength > 0)
		{
			szInfoLog = (char *)malloc(iInfoLogLength);
			if (szInfoLog != NULL)
			{
				GLsizei written;
				glGetShaderInfoLog(gPerFragment_FragmentShaderObject, iInfoLogLength, &written, szInfoLog);
				fprintf(gpFile, "Fragment Shader Compilation Log : %s\n", szInfoLog);
				free(szInfoLog);
				uninitialize();
				exit(0);
			}
		}
	}

	// *** SHADER PROGRAM ***
	// create
	gPerFragmentShaderProgramObject = glCreateProgram();

	// attach vertex shader to shader program
	glAttachShader(gPerFragmentShaderProgramObject, gPerFragment_VertexShaderObject);

	// attach fragment shader to shader program
	glAttachShader(gPerFragmentShaderProgramObject, gPerFragment_FragmentShaderObject);

	// pre-link binding of shader program object with vertex shader position attribute
	glBindAttribLocation(gPerFragmentShaderProgramObject, VDG_ATTRIBUTE_VERTEX, "vPosition");
	glBindAttribLocation(gPerFragmentShaderProgramObject, VDG_ATTRIBUTE_NORMAL, "vNormal");

	// link shader
	glLinkProgram(gPerFragmentShaderProgramObject);
	iShaderProgramLinkStatus = 0;
	glGetProgramiv(gPerFragmentShaderProgramObject, GL_LINK_STATUS, &iShaderProgramLinkStatus);
	if (iShaderProgramLinkStatus == GL_FALSE)
	{
		glGetProgramiv(gPerFragmentShaderProgramObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
		if (iInfoLogLength>0)
		{
			szInfoLog = (char *)malloc(iInfoLogLength);
			if (szInfoLog != NULL)
			{
				GLsizei written;
				glGetProgramInfoLog(gPerFragmentShaderProgramObject, iInfoLogLength, &written, szInfoLog);
				fprintf(gpFile, "Shader Program Link Log : %s\n", szInfoLog);
				free(szInfoLog);
				uninitialize();
				exit(0);
			}
		}
	}

	// get uniform locations
	model_matrix_uniform_perFrag = glGetUniformLocation(gPerFragmentShaderProgramObject, "u_model_matrix");
	view_matrix_uniform_perFrag = glGetUniformLocation(gPerFragmentShaderProgramObject, "u_view_matrix");
	projection_matrix_uniform_perFrag = glGetUniformLocation(gPerFragmentShaderProgramObject, "u_projection_matrix");


	L_KeyPressed_uniform_perFrag = glGetUniformLocation(gPerFragmentShaderProgramObject, "u_lighting_enabled");

	// Ambient color intensity of light
	red_La_uniform_perFrag = glGetUniformLocation(gPerFragmentShaderProgramObject, "u_La_red");
	// Diffuse color intensity of light
	red_Ld_uniform_perFrag = glGetUniformLocation(gPerFragmentShaderProgramObject, "u_Ld_red");
	// Specular color intensity of light
	red_Ls_uniform_perFrag = glGetUniformLocation(gPerFragmentShaderProgramObject, "u_Ls_red");

	// Position of light
	red_light_position_uniform_perFrag = glGetUniformLocation(gPerFragmentShaderProgramObject, "u_light_position_red");

green_La_uniform_perFrag = glGetUniformLocation(gPerFragmentShaderProgramObject, "u_La_green");
	// Diffuse color intensity of light
	green_Ld_uniform_perFrag = glGetUniformLocation(gPerFragmentShaderProgramObject, "u_Ld_green");
	// Specular color intensity of light
	green_Ls_uniform_perFrag = glGetUniformLocation(gPerFragmentShaderProgramObject, "u_Ls_green");

	// Position of light
	green_light_position_uniform_perFrag = glGetUniformLocation(gPerFragmentShaderProgramObject, "u_light_position_green");

blue_La_uniform_perFrag = glGetUniformLocation(gPerFragmentShaderProgramObject, "u_La_blue");
	// Diffuse color intensity of light
	blue_Ld_uniform_perFrag = glGetUniformLocation(gPerFragmentShaderProgramObject, "u_Ld_blue");
	// Specular color intensity of light
	blue_Ls_uniform_perFrag = glGetUniformLocation(gPerFragmentShaderProgramObject, "u_Ls_blue");

	// Position of light
	blue_light_position_uniform_perFrag = glGetUniformLocation(gPerFragmentShaderProgramObject, "u_light_position_blue");

	// Ambient color reflective intensity of material
	Ka_uniform_perFrag = glGetUniformLocation(gPerFragmentShaderProgramObject, "u_Ka");
	// diffuse reflective color intensity of material
	Kd_uniform_perFrag = glGetUniformLocation(gPerFragmentShaderProgramObject, "u_Kd");
	// specular reflective color intensity of material
	Ks_uniform_perFrag = glGetUniformLocation(gPerFragmentShaderProgramObject, "u_Ks");
	// shininess of material ( value is conventionally between 1 to 200 )
	material_shininess_uniform_perFrag = glGetUniformLocation(gPerFragmentShaderProgramObject, "u_material_shininess");;


	// *** vertices, colors, shader attribs, vbo, vao initializations ***
	getSphereVertexData(sphere_vertices, sphere_normals, sphere_textures, sphere_elements);

	gNumVertices = getNumberOfSphereVertices();
	gNumElements = getNumberOfSphereElements();
	// *** vertices, colors, shader attribs, vbo, vao initializations ***


	// CUBE CODE
	// vao
	glGenVertexArrays(1, &gVao_sphere);
	glBindVertexArray(gVao_sphere);

	// position vbo
	glGenBuffers(1, &gVbo_sphere_position);
	glBindBuffer(GL_ARRAY_BUFFER, gVbo_sphere_position);
	glBufferData(GL_ARRAY_BUFFER, sizeof(sphere_vertices), sphere_vertices, GL_STATIC_DRAW);

	glVertexAttribPointer(VDG_ATTRIBUTE_VERTEX, 3, GL_FLOAT, GL_FALSE, 0, NULL);

	glEnableVertexAttribArray(VDG_ATTRIBUTE_VERTEX);

	glBindBuffer(GL_ARRAY_BUFFER, 0);

	// normal vbo
	glGenBuffers(1, &gVbo_sphere_normal);
	glBindBuffer(GL_ARRAY_BUFFER, gVbo_sphere_normal);
	glBufferData(GL_ARRAY_BUFFER, sizeof(sphere_normals), sphere_normals, GL_STATIC_DRAW);

	glVertexAttribPointer(VDG_ATTRIBUTE_NORMAL, 3, GL_FLOAT, GL_FALSE, 0, NULL);

	glEnableVertexAttribArray(VDG_ATTRIBUTE_NORMAL);

	glBindBuffer(GL_ARRAY_BUFFER, 0);

	// element vbo
	glGenBuffers(1, &gVbo_sphere_element);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, gVbo_sphere_element);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(sphere_elements), sphere_elements, GL_STATIC_DRAW);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);

	glBindVertexArray(0);

	glShadeModel(GL_SMOOTH);
	// set-up depth buffer
	glClearDepth(1.0f);
	// enable depth testing
	glEnable(GL_DEPTH_TEST);
	// depth test to do
	glDepthFunc(GL_LEQUAL);
	// set really nice percpective calculations ?
	glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST);
	// We will always cull back faces for better performance
	glEnable(GL_CULL_FACE);

	// set background color
	glClearColor(0.0f, 0.0f, 0.0f, 0.0f); // black

										  // set perspective matrix to identitu matrix
	gPerspectiveProjectionMatrix = mat4::identity();

	gbLight = false;


	resize(giWindowWidth, giWindowHeight);
}





void display(){
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	// Start using OpenGL program object
	if (gbPerVertexMode == 1) {
		glUseProgram(gPerVertexShaderProgramObject);
		if (gbLight == true)
		{

			// set 'u_lighting_enabled' uniform
			glUniform1i(L_KeyPressed_uniform_perVer, 1);

			// setting light's properties
			glUniform3fv(red_La_uniform_perVer, 1, red_lightAmbient);
			glUniform3fv(red_Ld_uniform_perVer, 1, red_lightDiffuse);
			glUniform3fv(red_Ls_uniform_perVer, 1, red_lightSpecular);
			red_lightPosition[1] = sinf(gfRedLightAngle);
			red_lightPosition[2] = cosf(gfRedLightAngle) - 2;
			glUniform4fv(red_light_position_uniform_perVer, 1, red_lightPosition);

			glUniform3fv(blue_La_uniform_perVer, 1, blue_lightAmbient);
			glUniform3fv(blue_Ld_uniform_perVer, 1, blue_lightDiffuse);
			glUniform3fv(blue_Ls_uniform_perVer, 1, blue_lightSpecular);
			blue_lightPosition[0] = sinf(gfBlueLightAngle);
			blue_lightPosition[1] = cosf(gfBlueLightAngle);
			glUniform4fv(blue_light_position_uniform_perVer, 1, blue_lightPosition);

			glUniform3fv(green_La_uniform_perVer, 1, green_lightAmbient);
			glUniform3fv(green_Ld_uniform_perVer, 1, green_lightDiffuse);
			glUniform3fv(green_Ls_uniform_perVer, 1, green_lightSpecular);
			green_lightPosition[0] = sinf(gfGreenLightAngle);
			green_lightPosition[2] = cosf(gfGreenLightAngle) - 2;
			glUniform4fv(green_light_position_uniform_perVer, 1, green_lightPosition);

			// setting material's properties
			glUniform3fv(Ka_uniform_perVer, 1, material_ambient);
			glUniform3fv(Kd_uniform_perVer, 1, material_diffuse);
			glUniform3fv(Ks_uniform_perVer, 1, material_specular);
			glUniform1f(material_shininess_uniform_perVer, material_shininess);

		}
		else
		{
			// set 'u_lighting_enabled' uniform

			glUniform1i(L_KeyPressed_uniform_perVer, 0);
		}

		// OpenGL Drawing
		// set all matrices to identity
		mat4 modelMatrix = mat4::identity();
		mat4 viewMatrix = mat4::identity();

		// apply z axis translation to go deep into the screen by -2.0,
		// so that triangle with same fullscreen co-ordinates, but due to above translation will look small
		modelMatrix = translate(0.0f, 0.0f, -2.0f);

		glUniformMatrix4fv(model_matrix_uniform_perVer, 1, GL_FALSE, modelMatrix);
		glUniformMatrix4fv(view_matrix_uniform_perVer, 1, GL_FALSE, viewMatrix);
		glUniformMatrix4fv(projection_matrix_uniform_perVer, 1, GL_FALSE, gPerspectiveProjectionMatrix);

		// *** bind vao ***
		glBindVertexArray(gVao_sphere);

		// *** draw, either by glDrawTriangles() or glDrawArrays() or glDrawElements()
		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, gVbo_sphere_element);
		glDrawElements(GL_TRIANGLES, gNumElements, GL_UNSIGNED_SHORT, 0);
	}
	else {
		glUseProgram(gPerFragmentShaderProgramObject);
		if (gbLight == true)
		{

			// set 'u_lighting_enabled' uniform
			glUniform1i(L_KeyPressed_uniform_perFrag, 1);

			glUniform3fv(red_La_uniform_perFrag, 1, red_lightAmbient);
			glUniform3fv(red_Ld_uniform_perFrag, 1, red_lightDiffuse);
			glUniform3fv(red_Ls_uniform_perFrag, 1, red_lightSpecular);
			red_lightPosition[1] = sinf(gfRedLightAngle);
			red_lightPosition[2] = cosf(gfRedLightAngle) - 2;
			glUniform4fv(red_light_position_uniform_perFrag, 1, red_lightPosition);

			glUniform3fv(blue_La_uniform_perFrag, 1, blue_lightAmbient);
			glUniform3fv(blue_Ld_uniform_perFrag, 1, blue_lightDiffuse);
			glUniform3fv(blue_Ls_uniform_perFrag, 1, blue_lightSpecular);
			blue_lightPosition[0] = sinf(gfBlueLightAngle);
			blue_lightPosition[1] = cosf(gfBlueLightAngle);
			glUniform4fv(blue_light_position_uniform_perFrag, 1, blue_lightPosition);

			glUniform3fv(green_La_uniform_perFrag, 1, green_lightAmbient);
			glUniform3fv(green_Ld_uniform_perFrag, 1, green_lightDiffuse);
			glUniform3fv(green_Ls_uniform_perFrag, 1, green_lightSpecular);
			green_lightPosition[0] = sinf(gfGreenLightAngle);
			green_lightPosition[2] = cosf(gfGreenLightAngle) - 2;
			glUniform4fv(green_light_position_uniform_perFrag, 1, green_lightPosition);

			// setting material's properties
			glUniform3fv(Ka_uniform_perFrag, 1, material_ambient);
			glUniform3fv(Kd_uniform_perFrag, 1, material_diffuse);
			glUniform3fv(Ks_uniform_perFrag, 1, material_specular);
			glUniform1f(material_shininess_uniform_perFrag, material_shininess);
		}
		else
		{
			// set 'u_lighting_enabled' uniform

			glUniform1i(L_KeyPressed_uniform_perFrag, 0);
		}

		// OpenGL Drawing
		// set all matrices to identity
		mat4 modelMatrix = mat4::identity();
		mat4 viewMatrix = mat4::identity();

		// apply z axis translation to go deep into the screen by -2.0,
		// so that triangle with same fullscreen co-ordinates, but due to above translation will look small
		modelMatrix = translate(0.0f, 0.0f, -2.0f);

		glUniformMatrix4fv(model_matrix_uniform_perFrag, 1, GL_FALSE, modelMatrix);
		glUniformMatrix4fv(view_matrix_uniform_perFrag, 1, GL_FALSE, viewMatrix);
		glUniformMatrix4fv(projection_matrix_uniform_perFrag, 1, GL_FALSE, gPerspectiveProjectionMatrix);

		// *** bind vao ***
		glBindVertexArray(gVao_sphere);

		// *** draw, either by glDrawTriangles() or glDrawArrays() or glDrawElements()
		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, gVbo_sphere_element);
		glDrawElements(GL_TRIANGLES, gNumElements, GL_UNSIGNED_SHORT, 0);
	}



	// *** unbind vao ***
	glBindVertexArray(0);

	glUseProgram(0);
	glXSwapBuffers(gpDisplay, gWindow);
}
void update() {
	gfRedLightAngle = (gfRedLightAngle + 1.0f);
	if (gfRedLightAngle >= 360.0f)
		gfRedLightAngle = 0.0f;

	gfBlueLightAngle = (gfBlueLightAngle + 1.0f);
	if (gfBlueLightAngle >= 360.0f)
		gfBlueLightAngle = 0.0f;


	gfGreenLightAngle = (gfGreenLightAngle + 1.0f);
	if (gfGreenLightAngle >= 360.0f)
		gfGreenLightAngle = 0.0f;
}

void resize(int width, int height){
	//code
	if(height == 0)
		height = 1;
	glViewport(0, 0, (GLsizei)width, (GLsizei)height);
	if (width <= height)
	{
		gPerspectiveProjectionMatrix = vmath::perspective(45.0f,(GLfloat)height/(GLfloat)width, 0.1f, 100.0f);
	}
	else
	{
		gPerspectiveProjectionMatrix = vmath::perspective(45.0f, (GLfloat)width / (GLfloat)height, 0.1f, 100.0f);
	}
	
}


void uninitialize(void){
	if (gVao_sphere)
	{
		glDeleteVertexArrays(1, &gVao_sphere);
		gVao_sphere = 0;
	}



	// destroy vbo
	if (gVbo_sphere_position)
	{
		glDeleteBuffers(1, &gVbo_sphere_position);
		gVbo_sphere_position = 0;
	}
	if (gVbo_sphere_normal)
	{
		glDeleteBuffers(1, &gVbo_sphere_normal);
		gVbo_sphere_normal = 0;
	}
	if (gVbo_sphere_element)
	{
		glDeleteBuffers(1, &gVbo_sphere_element);
		gVbo_sphere_element = 0;
	}
	// Detach vertex shader from shader program object
	glDetachShader(gPerVertexShaderProgramObject, gPerVertex_VertexShaderObject);

	// Detach fragment shader from shader program object
	glDetachShader(gPerVertexShaderProgramObject, gPerVertex_FragmentShaderObject);

	// Delete vertex shader object
	glDeleteShader(gPerVertex_VertexShaderObject);
	gPerVertex_VertexShaderObject = 0;

	// Delete fragment shader object
	glDeleteShader(gPerVertex_FragmentShaderObject);
	gPerVertex_FragmentShaderObject = 0;

	// Delete shader program object
	glDeleteProgram(gPerVertexShaderProgramObject);
	gPerVertexShaderProgramObject = 0;

	glDetachShader(gPerFragmentShaderProgramObject, gPerFragment_VertexShaderObject);

	// Detach fragment shader from shader program object
	glDetachShader(gPerFragmentShaderProgramObject, gPerFragment_FragmentShaderObject);

	// Delete vertex shader object
	glDeleteShader(gPerFragment_VertexShaderObject);
	gPerFragment_VertexShaderObject = 0;

	// Delete fragment shader object
	glDeleteShader(gPerFragment_FragmentShaderObject);
	gPerFragment_FragmentShaderObject = 0;

	// Delete shader program object
	glDeleteProgram(gPerFragmentShaderProgramObject);
	gPerFragmentShaderProgramObject = 0;

	// Unlink shader program
	glUseProgram(0);

	GLXContext currentGLXContext;
	currentGLXContext = glXGetCurrentContext();
	if(currentGLXContext != NULL &&currentGLXContext== gGLXContext)
	{
		glXMakeCurrent(gpDisplay, 0,0);
	}
	
	if(gGLXContext)
	{
		glXDestroyContext(gpDisplay, gGLXContext);
	}
	if(gWindow)
	{
		XDestroyWindow(gpDisplay, gWindow);
	}
	if(gColormap)
	{
		XFreeColormap(gpDisplay, gColormap);
	}
	if(gpXVisualInfo)
	{
		free(gpXVisualInfo);
		gpXVisualInfo == NULL;
	}
	if(gpDisplay)
	{
		XCloseDisplay(gpDisplay);
		gpDisplay == NULL;
	}	
}

