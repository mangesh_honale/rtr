#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <memory.h>
#include <X11/Xlib.h>
#include <X11/Xutil.h>
#include <X11/XKBlib.h>
#include <X11/keysym.h>

#include <GL/glew.h>
#include <GL/gl.h>
#include <GL/glx.h>
#include "Sphere.h"
#include "vmath.h"
#pragma comment(lib, "Sphere.lib")
// Namespace
using namespace std;
using namespace vmath;

enum
{
	VDG_ATTRIBUTE_VERTEX = 0,
	VDG_ATTRIBUTE_COLOR,
	VDG_ATTRIBUTE_NORMAL,
	VDG_ATTRIBUTE_TEXTURE0,
};

bool bFullScreen = false;
Display *gpDisplay = NULL;
XVisualInfo *gpXVisualInfo = NULL;
Colormap gColormap;
Window gWindow;
int giWindowWidth = 800;
int giWindowHeight = 600;

GLXContext gGLXContext;

FILE *gpFile = NULL;

GLuint gVertexShaderObject;
GLuint gFragmentShaderObject;
GLuint gShaderProgramObject;

GLfloat gfPyramidAngle = 0.0f;


GLuint gVao_pyramid;
GLuint gVbo_pyramid_position;
GLuint gVbo_pyramid_normal;
GLuint gVbo_pyramid_element;
GLuint model_matrix_uniform, view_matrix_uniform, rotation_matrix_uniform, projection_matrix_uniform;
GLuint L_KeyPressed_uniform;
// Red light uniforms
GLuint Red_La_uniform;
GLuint Red_Ld_uniform;
GLuint Red_Ls_uniform;
GLuint red_light_position_uniform;

// Blue light uniforms
GLuint Blue_La_uniform;
GLuint Blue_Ld_uniform;
GLuint Blue_Ls_uniform;
GLuint blue_light_position_uniform;

GLuint Ka_uniform;
GLuint Kd_uniform;
GLuint Ks_uniform;
GLuint material_shininess_uniform;

mat4 gPerspectiveProjectionMatrix;

bool gbAnimate;
bool gbLight;


//red light arrays
GLfloat red_light_ambient[] = { 0.0f, 0.0f, 0.0f, 1.0f };
GLfloat red_light_diffuse[] = { 1.0f, 0.0f, 0.0f, 1.0f };
GLfloat red_light_specular[] = { 1.0f, 0.0f, 0.0f, 1.0f };
GLfloat red_light_position[] = { -2.0f, 0.0f, 0.0f, 1.0f };

//blue light arrays
GLfloat blue_light_ambient[] = { 0.0f, 0.0f, 0.0f, 1.0f };
GLfloat blue_light_diffuse[] = { 0.0f, 0.0f, 1.0f, 1.0f };
GLfloat blue_light_specular[] = { 0.0f, 0.0f, 1.0f, 1.0f };
GLfloat blue_light_position[] = { 2.0f, 0.0f, 0.0f, 1.0f };

//material arrays
GLfloat material_ambient[] = { 0.0f, 0.0f, 0.0f, 1.0f };
GLfloat material_diffuse[] = { 1.0f, 1.0f, 1.0f, 1.0f };
GLfloat material_specular[] = { 1.0f, 1.0f, 1.0f, 1.0f };
GLfloat material_shininess = 50.0f;


int main(void){
	//function declarations
	void CreateWindow(void);
	void ToggleFullScreen(void);
	void initialize(void);
	void display(void);
	void uninitialize();
	void resize(int, int);	
	void update();
	//variable declarations
	int winWidth = giWindowWidth;
	int winHeight = giWindowHeight;
	static bool bIsLKeyPressed = false;
	bool bDone = false;
	gpFile = fopen("Log.txt", "w");
	if (gpFile == 0)
	{
		printf("Log file can not be created.\n Exitting...");
		exit(0);
	}
	else
	{
		fprintf(gpFile, "Log file successfully Opened.\n");
	}
	CreateWindow();
	initialize();

	//message loop
	XEvent event;
	KeySym keysym;
	
	while(bDone == false){
		while(XPending(gpDisplay)){
			XNextEvent(gpDisplay, &event);
			switch(event.type){
				case MapNotify:
					break;
				case KeyPress:
					keysym = XkbKeycodeToKeysym(gpDisplay, event.xkey.keycode, 0, 0);
					switch(keysym){
						case XK_Escape:
							bDone = true;
							break;
						case XK_F:
						case XK_f:
							if(bFullScreen == false){
								ToggleFullScreen();
								bFullScreen = true;
							}else{
								ToggleFullScreen();
								bFullScreen = false;
							}
							break;
						case XK_l:
						case XK_L:
							if (bIsLKeyPressed == false)
							{
								gbLight = true;
								bIsLKeyPressed = true;
							}
							else
							{
								gbLight = false;
								bIsLKeyPressed = false;
							}
							break;
						default:
							break;

					}
					break;
				case ButtonPress:
					switch(event.xbutton.button){
						case 1: 
							break;
						case 2:
							break;
						case 3:
							break;
						default:
							break;
					}
					break;
				case MotionNotify:
					break;
				case ConfigureNotify:
					winWidth = event.xconfigure.width;
					winHeight = event.xconfigure.height;
					resize(winWidth, winHeight);
					break;
				case Expose:
					break;
				case DestroyNotify:
					break;
				case 33:
					bDone = true;
					break;
				default:
					break;
			}
		}
		update();
		display();
	}
	uninitialize();
	return (0);
}

void update() {
	gfPyramidAngle = (gfPyramidAngle + 1.0f);
	if (gfPyramidAngle >= 360.0f)
		gfPyramidAngle = 0.0f;
}

void CreateWindow(void){
	//function prototypes
	void uninitialize(void);
	
	//variable declarations
	XSetWindowAttributes winAttribs;
	int defaultScreen;
	int defaultDepth;
	int styleMask;
	
	static int frameBufferAttributes[] = {
		GLX_RGBA,
		GLX_RED_SIZE, 8,
		GLX_GREEN_SIZE, 8,
		GLX_BLUE_SIZE, 8,
		GLX_ALPHA_SIZE, 8,
		GLX_DOUBLEBUFFER, True,
		GLX_DEPTH_SIZE, 24,
		None
	};

	//code
	gpDisplay = XOpenDisplay(NULL);
	if(gpDisplay == NULL){
		printf("Erroe: Unable to open X Display.\n Exiting now...\n");
		uninitialize();
		exit(1);
	}

	defaultScreen = XDefaultScreen(gpDisplay);
	

	gpXVisualInfo = glXChooseVisual(gpDisplay, defaultScreen, frameBufferAttributes);
	if (gpXVisualInfo == NULL) {
		printf("Error: Unable to allocate memory for Visual info.\nExitting Now.\n");
		uninitialize();
        	exit(1);
	}
	XMatchVisualInfo(gpDisplay, defaultScreen, defaultDepth, TrueColor, gpXVisualInfo);
	if(gpXVisualInfo == NULL) {
	    printf("Error: Unable to get VisualInfo. \nExitting...\n");
	    uninitialize();
	    exit(1);
	}
	winAttribs.border_pixmap = 0;
	winAttribs.border_pixel = 0;
	winAttribs.background_pixmap = 0;
	winAttribs.colormap = XCreateColormap(gpDisplay, RootWindow(gpDisplay, gpXVisualInfo->screen), gpXVisualInfo->visual, AllocNone);
        gColormap =  winAttribs.colormap;
	winAttribs.background_pixel =  BlackPixel(gpDisplay, defaultScreen);
	winAttribs.event_mask = ExposureMask | VisibilityChangeMask | ButtonPressMask | KeyPressMask | PointerMotionMask | StructureNotifyMask;

	styleMask =  CWBorderPixel | CWBackPixel | CWEventMask | CWColormap;

	gWindow = XCreateWindow(gpDisplay,
	RootWindow(gpDisplay, gpXVisualInfo->screen),
	0,  // X co-ordinate distance
	0,  // Y co-ordinate distance
	giWindowWidth, // Window width
	giWindowHeight, // Window height
	0, // Border Thickness, '0' is default
	gpXVisualInfo->depth,
	InputOutput,
	gpXVisualInfo->visual,
	styleMask,
	&winAttribs
	);

	if(!gWindow) {

	printf("Error: Failed to create main window.\nExitting... \n");
	uninitialize();
	exit(1);
	}
	
	XStoreName(gpDisplay, gWindow, "First OpenGL Window");
	
	Atom windowManagerDelete = XInternAtom(gpDisplay, "WM_DELETE_WINDOW", True);
	XSetWMProtocols(gpDisplay, gWindow, &windowManagerDelete, 1);
	XMapWindow(gpDisplay, gWindow);
}

void ToggleFullScreen(){

	Atom wm_state;
	Atom fullscreen;
	XEvent xev={0};

	//code
	wm_state = XInternAtom(gpDisplay, "_NET_WM_STATE", False);
	memset(&xev, 0, sizeof(xev));
	
	xev.type = ClientMessage;
	xev.xclient.window = gWindow;
	xev.xclient.message_type = wm_state;
	xev.xclient.format = 32;
	xev.xclient.data.l[0] = bFullScreen ? 0: 1;
	
	fullscreen = XInternAtom(gpDisplay, "_NET_WM_STATE_FULLSCREEN", False);
	xev.xclient.data.l[1] = fullscreen;
		
	XSendEvent(gpDisplay, RootWindow(gpDisplay, gpXVisualInfo->screen),
		  False,
		  StructureNotifyMask,
		  &xev);
}

void initialize(void){
	//function prototypes
	void resize(int, int);
	void uninitialize(void);
	void LoadGLTexture();
	gGLXContext = glXCreateContext(gpDisplay, gpXVisualInfo, NULL, GL_TRUE);
	glXMakeCurrent(gpDisplay, gWindow, gGLXContext);
	glewInit();
	gVertexShaderObject = glCreateShader(GL_VERTEX_SHADER);

	// provide source code to shader
	const GLchar *vertexShaderSourceCode =
		"#version 130" \
		"\n" \
		"in vec4 vPosition;" \
		"in vec3 vNormal;" \
		"uniform mat4 u_model_matrix;" \
		"uniform mat4 u_view_matrix;" \
		"uniform mat4 u_rotation_matrix;" \
		"uniform mat4 u_projection_matrix;" \
		"uniform int u_lighting_enabled;"\
		"uniform vec3 u_red_La;"\
		"uniform vec3 u_red_Ld;"\
		"uniform vec3 u_red_Ls;" \
		"uniform vec3 u_blue_La;"\
		"uniform vec3 u_blue_Ld;"\
		"uniform vec3 u_blue_Ls;" \
		"uniform vec4 u_red_light_position;"\
		"uniform vec4 u_blue_light_position;"\
		"uniform vec3 u_Ka;"\
		"uniform vec3 u_Ks;"\
		"uniform vec3 u_Kd;" \
		"uniform float u_material_shininess;" \
		"vec3 blue_color;" \
		"vec3 red_color;" \
		"out vec3 phong_ads_color;" \
		"void main(void)" \
		"{" \
		"if (u_lighting_enabled == 1)" \
		"{" \
		"vec4 eye_coordinates=u_view_matrix * u_model_matrix * vPosition;" \
		"vec3 transformed_normals=normalize(mat3(u_view_matrix * u_model_matrix) * vNormal);" \

		"vec3 red_light_direction = normalize(vec3(u_red_light_position) - eye_coordinates.xyz);" \
		"float tn_dot_red_ld = max(dot(transformed_normals, red_light_direction),0.0);" \
		"vec3 red_ambient = u_red_La * u_Ka;" \
		"vec3 red_diffuse = u_red_Ld * u_Kd * tn_dot_red_ld;" \
		"vec3 reflection_vector = reflect(-red_light_direction, transformed_normals);" \
		"vec3 viewer_vector = normalize(-eye_coordinates.xyz);" \
		"vec3 red_specular = u_red_Ls * u_Ks * pow(max(dot(reflection_vector, viewer_vector), 0.0), u_material_shininess);" \
		"red_color=red_ambient + red_diffuse + red_specular;" \

		"vec3 blue_light_direction = normalize(vec3(u_blue_light_position) - eye_coordinates.xyz);" \
		"float tn_dot_blue_ld = max(dot(transformed_normals, blue_light_direction),0.0);" \
		"vec3 blue_ambient = u_blue_La * u_Ka;" \
		"vec3 blue_diffuse = u_blue_Ld * u_Kd * tn_dot_blue_ld;" \
		"reflection_vector = reflect(-blue_light_direction, transformed_normals);" \
		"viewer_vector = normalize(-eye_coordinates.xyz);" \
		"vec3 blue_specular = u_blue_Ls * u_Ks * pow(max(dot(reflection_vector, viewer_vector), 0.0), u_material_shininess);" \
		"blue_color = blue_ambient + blue_diffuse + blue_specular;" \
		"phong_ads_color = red_color + blue_color;" \
		"}" \
		"else" \
		"{" \
		"phong_ads_color = vec3(1.0, 1.0, 1.0);" \
		"}" \
		"gl_Position=u_projection_matrix * u_view_matrix * u_model_matrix * u_rotation_matrix * vPosition;" \
		"}";


	glShaderSource(gVertexShaderObject, 1, (const GLchar **)&vertexShaderSourceCode, NULL);

	// compile shader
	glCompileShader(gVertexShaderObject);
	GLint iInfoLogLength = 0;
	GLint iShaderCompiledStatus = 0;
	char *szInfoLog = NULL;
	glGetShaderiv(gVertexShaderObject, GL_COMPILE_STATUS, &iShaderCompiledStatus);
	if (iShaderCompiledStatus == GL_FALSE)
	{
		glGetShaderiv(gVertexShaderObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
		if (iInfoLogLength > 0)
		{
			szInfoLog = (char *)malloc(iInfoLogLength);
			if (szInfoLog != NULL)
			{
				GLsizei written;
				glGetShaderInfoLog(gVertexShaderObject, iInfoLogLength, &written, szInfoLog);
				fprintf(gpFile, "Vertex Shader Compilation Log : %s\n", szInfoLog);
				free(szInfoLog);
				uninitialize();
				exit(0);
			}
		}
	}

	// *** FRAGMENT SHADER ***
	// create shader
	gFragmentShaderObject = glCreateShader(GL_FRAGMENT_SHADER);

	// provide source code to shader
	const GLchar *fragmentShaderSourceCode =
		"#version 130" \
		"\n" \
		"in vec3 phong_ads_color;" \
		"out vec4 FragColor;" \
		"void main(void)" \
		"{" \
		"FragColor = vec4(phong_ads_color, 1.0);" \
		"}";

	glShaderSource(gFragmentShaderObject, 1, (const GLchar **)&fragmentShaderSourceCode, NULL);

	// compile shader
	glCompileShader(gFragmentShaderObject);
	glGetShaderiv(gFragmentShaderObject, GL_COMPILE_STATUS, &iShaderCompiledStatus);
	if (iShaderCompiledStatus == GL_FALSE)
	{
		glGetShaderiv(gFragmentShaderObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
		if (iInfoLogLength > 0)
		{
			szInfoLog = (char *)malloc(iInfoLogLength);
			if (szInfoLog != NULL)
			{
				GLsizei written;
				glGetShaderInfoLog(gFragmentShaderObject, iInfoLogLength, &written, szInfoLog);
				fprintf(gpFile, "Fragment Shader Compilation Log : %s\n", szInfoLog);
				free(szInfoLog);
				uninitialize();
				exit(0);
			}
		}
	}

	// *** SHADER PROGRAM ***
	// create
	gShaderProgramObject = glCreateProgram();

	// attach vertex shader to shader program
	glAttachShader(gShaderProgramObject, gVertexShaderObject);

	// attach fragment shader to shader program
	glAttachShader(gShaderProgramObject, gFragmentShaderObject);

	// pre-link binding of shader program object with vertex shader position attribute
	glBindAttribLocation(gShaderProgramObject, VDG_ATTRIBUTE_VERTEX, "vPosition");
	glBindAttribLocation(gShaderProgramObject, VDG_ATTRIBUTE_NORMAL, "vNormal");

	// link shader
	glLinkProgram(gShaderProgramObject);
	GLint iShaderProgramLinkStatus = 0;
	glGetProgramiv(gShaderProgramObject, GL_LINK_STATUS, &iShaderProgramLinkStatus);
	if (iShaderProgramLinkStatus == GL_FALSE)
	{
		glGetProgramiv(gShaderProgramObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
		if (iInfoLogLength>0)
		{
			szInfoLog = (char *)malloc(iInfoLogLength);
			if (szInfoLog != NULL)
			{
				GLsizei written;
				glGetProgramInfoLog(gShaderProgramObject, iInfoLogLength, &written, szInfoLog);
				fprintf(gpFile, "Shader Program Link Log : %s\n", szInfoLog);
				free(szInfoLog);
				uninitialize();
				exit(0);
			}
		}
	}

	// get uniform locations
	model_matrix_uniform = glGetUniformLocation(gShaderProgramObject, "u_model_matrix");
	view_matrix_uniform = glGetUniformLocation(gShaderProgramObject, "u_view_matrix");
	projection_matrix_uniform = glGetUniformLocation(gShaderProgramObject, "u_projection_matrix");
	rotation_matrix_uniform = glGetUniformLocation(gShaderProgramObject, "u_rotation_matrix");
	L_KeyPressed_uniform = glGetUniformLocation(gShaderProgramObject, "u_lighting_enabled");

	// Ambient color intensity of light
	Red_La_uniform = glGetUniformLocation(gShaderProgramObject, "u_red_La");
	// Diffuse color intensity of light
	Red_Ld_uniform = glGetUniformLocation(gShaderProgramObject, "u_red_Ld");
	// Specular color intensity of light
	Red_Ls_uniform = glGetUniformLocation(gShaderProgramObject, "u_red_Ls");

	// Position of light
	red_light_position_uniform = glGetUniformLocation(gShaderProgramObject, "u_red_light_position");

	Blue_La_uniform = glGetUniformLocation(gShaderProgramObject, "u_blue_La");
	// Diffuse color intensity of light
	Blue_Ld_uniform = glGetUniformLocation(gShaderProgramObject, "u_blue_Ld");
	// Specular color intensity of light
	Blue_Ls_uniform = glGetUniformLocation(gShaderProgramObject, "u_blue_Ls");

	// Position of light
	blue_light_position_uniform = glGetUniformLocation(gShaderProgramObject, "u_blue_light_position");

	// Ambient color reflective intensity of material
	Ka_uniform = glGetUniformLocation(gShaderProgramObject, "u_Ka");
	// diffuse reflective color intensity of material
	Kd_uniform = glGetUniformLocation(gShaderProgramObject, "u_Kd");
	// specular reflective color intensity of material
	Ks_uniform = glGetUniformLocation(gShaderProgramObject, "u_Ks");
	// shininess of material ( value is conventionally between 1 to 200 )
	material_shininess_uniform = glGetUniformLocation(gShaderProgramObject, "u_material_shininess");

	// *** vertices, colors, shader attribs, vbo, vao initializations ***
	const GLfloat pyramidVertices[] = {
		0.0f, 1.0f, 0.0f,
		-1.0f, -1.0f, -1.0f,
		1.0f, -1.0f, -1.0f,

		0.0f,1.0f,0.0f,
		1.0f,-1.0f,-1.0f,
		1.0f,-1.0f,1.0,

		0.0f,1.0f,0.0f,
		1.0f,-1.0f,1.0f,
		-1.0f,-1.0f,1.0f,

		0.0f,1.0f,0.0f,
		-1.0f,-1.0f,1.0f,
		-1.0f,-1.0f,-1.0f
	};

	const GLfloat pyramidNormals[] = {
		0.0f,1.0f,-1.0f,
		-1.0f, -1.0f, -2.0f,
		1.0f, -1.0f, -2.0f,

		1.0f,1.0f,0.0f,
		2.0f,-1.0f,-1.0f,
		2.0f,-1.0f,1.0,

		0.0f,1.0f,1.0f,
		1.0f,-1.0f,2.0f,
		-1.0f,-1.0f,2.0f,

		-1.0f,1.0f,0.0f,
		-2.0f,-1.0f,1.0f,
		-2.0f,-1.0f,-1.0f
	};
	// *** vertices, colors, shader attribs, vbo, vao initializations ***


	// CUBE CODE
	// vao
	glGenVertexArrays(1, &gVao_pyramid);
	glBindVertexArray(gVao_pyramid);

	// position vbo
	glGenBuffers(1, &gVbo_pyramid_position);
	glBindBuffer(GL_ARRAY_BUFFER, gVbo_pyramid_position);
	glBufferData(GL_ARRAY_BUFFER, sizeof(pyramidVertices), pyramidVertices, GL_STATIC_DRAW);

	glVertexAttribPointer(VDG_ATTRIBUTE_VERTEX, 3, GL_FLOAT, GL_FALSE, 0, NULL);

	glEnableVertexAttribArray(VDG_ATTRIBUTE_VERTEX);

	glBindBuffer(GL_ARRAY_BUFFER, 0);

	// normal vbo
	glGenBuffers(1, &gVbo_pyramid_normal);
	glBindBuffer(GL_ARRAY_BUFFER, gVbo_pyramid_normal);
	glBufferData(GL_ARRAY_BUFFER, sizeof(pyramidNormals), pyramidNormals, GL_STATIC_DRAW);
	glVertexAttribPointer(VDG_ATTRIBUTE_NORMAL, 3, GL_FLOAT, GL_FALSE, 0, NULL);
	glEnableVertexAttribArray(VDG_ATTRIBUTE_NORMAL);

	glBindBuffer(GL_ARRAY_BUFFER, 0);

	glBindVertexArray(0);

	glShadeModel(GL_SMOOTH);
	// set-up depth buffer
	glClearDepth(1.0f);
	// enable depth testing
	glEnable(GL_DEPTH_TEST);
	// depth test to do
	glDepthFunc(GL_LEQUAL);
	// set really nice percpective calculations ?
	glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST);
	// We will always cull back faces for better performance
	//glEnable(GL_CULL_FACE);

	// set background color
	glClearColor(0.0f, 0.0f, 0.0f, 0.0f); // black

										  // set perspective matrix to identitu matrix
	gPerspectiveProjectionMatrix = mat4::identity();

	gbLight = false;
	resize(giWindowWidth, giWindowHeight);
}





void display(){
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	// Start using OpenGL program object
	glUseProgram(gShaderProgramObject);

	if (gbLight == true)
	{
		// set 'u_lighting_enabled' uniform
		glUniform1i(L_KeyPressed_uniform, 1);
		// setting light's properties
		glUniform3fv(Red_La_uniform, 1, red_light_ambient);
		glUniform3fv(Red_Ld_uniform, 1, red_light_diffuse);
		glUniform3fv(Red_Ls_uniform, 1, red_light_specular);
		glUniform4fv(red_light_position_uniform, 1, red_light_position);

		glUniform3fv(Blue_La_uniform, 1, blue_light_ambient);
		glUniform3fv(Blue_Ld_uniform, 1, blue_light_diffuse);
		glUniform3fv(Blue_Ls_uniform, 1, blue_light_specular);
		glUniform4fv(blue_light_position_uniform, 1, blue_light_position);

		// setting material's properties
		glUniform3fv(Ka_uniform, 1, material_ambient);
		glUniform3fv(Kd_uniform, 1, material_diffuse);
		glUniform3fv(Ks_uniform, 1, material_specular);
		glUniform1f(material_shininess_uniform, material_shininess);
	}
	else
	{
		// set 'u_lighting_enabled' uniform
		glUniform1i(L_KeyPressed_uniform, 0);
	}

	// OpenGL Drawing
	// set all matrices to identity
	mat4 modelMatrix = mat4::identity();
	mat4 viewMatrix = mat4::identity();
	mat4 rotationMatrix = mat4::identity();

	// apply z axis translation to go deep into the screen by -2.0,
	// so that triangle with same fullscreen co-ordinates, but due to above translation will look small
	modelMatrix = translate(0.0f, 0.0f, -6.0f);
	rotationMatrix = rotate(0.0f, gfPyramidAngle, 0.0f);
	glUniformMatrix4fv(model_matrix_uniform, 1, GL_FALSE, modelMatrix);
	glUniformMatrix4fv(view_matrix_uniform, 1, GL_FALSE, viewMatrix);
	glUniformMatrix4fv(rotation_matrix_uniform, 1, GL_FALSE, rotationMatrix);
	glUniformMatrix4fv(projection_matrix_uniform, 1, GL_FALSE, gPerspectiveProjectionMatrix);

	// *** bind vao ***
	glBindVertexArray(gVao_pyramid);

	// *** draw, either by glDrawTriangles() or glDrawArrays() or glDrawElements()
	glDrawArrays(GL_TRIANGLES, 0, 12);

	// *** unbind vao ***
	glBindVertexArray(0);


	glUseProgram(0);
	glXSwapBuffers(gpDisplay, gWindow);
}


void resize(int width, int height){
	//code
	if(height == 0)
		height = 1;
	glViewport(0, 0, (GLsizei)width, (GLsizei)height);
	if (width <= height)
	{
		gPerspectiveProjectionMatrix = vmath::perspective(60.0f,(GLfloat)height/(GLfloat)width, 0.1f, 30.0f);
	}
	else
	{
		gPerspectiveProjectionMatrix = vmath::perspective(60.0f, (GLfloat)width / (GLfloat)height, 0.1f, 30.0f);
	}
	
}


void uninitialize(void){
	// destroy vao of square
	if (gVao_pyramid)
	{
		glDeleteVertexArrays(1, &gVao_pyramid);
		gVao_pyramid = 0;
	}

	// destroy vbo
	if (gVbo_pyramid_position)
	{
		glDeleteBuffers(1, &gVbo_pyramid_position);
		gVbo_pyramid_position = 0;
	}
	if (gVbo_pyramid_normal)
	{
		glDeleteBuffers(1, &gVbo_pyramid_normal);
		gVbo_pyramid_normal = 0;
	}
	// Detach vertex shader from shader program object
	glDetachShader(gShaderProgramObject, gVertexShaderObject);
	// Detach fragment shader from shader program object
	glDetachShader(gShaderProgramObject, gVertexShaderObject);

	// Delete vertex shader object
	glDeleteShader(gVertexShaderObject);
	gVertexShaderObject = 0;
	// Delte fragment shader object
	glDeleteShader(gFragmentShaderObject);
	gFragmentShaderObject = 0;

	// Delete shader program object
	glDeleteProgram(gShaderProgramObject);
	gShaderProgramObject = 0;

	// Unlink shader program
	glUseProgram(0);

	GLXContext currentGLXContext;
	currentGLXContext = glXGetCurrentContext();
	if(currentGLXContext != NULL &&currentGLXContext== gGLXContext)
	{
		glXMakeCurrent(gpDisplay, 0,0);
	}
	
	if(gGLXContext)
	{
		glXDestroyContext(gpDisplay, gGLXContext);
	}
	if(gWindow)
	{
		XDestroyWindow(gpDisplay, gWindow);
	}
	if(gColormap)
	{
		XFreeColormap(gpDisplay, gColormap);
	}
	if(gpXVisualInfo)
	{
		free(gpXVisualInfo);
		gpXVisualInfo == NULL;
	}
	if(gpDisplay)
	{
		XCloseDisplay(gpDisplay);
		gpDisplay == NULL;
	}	
}

