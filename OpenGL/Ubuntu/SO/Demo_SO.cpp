#include <stdlib.h>
#include <stdio.h>
#include <dlfcn.h>
#include <X11/Xlib.h>
#include <X11/Xutil.h>
#include <X11/XKBlib.h>

int main(){
	void *handle;
	char *error;
	typedef int(*pFunctionPointer)(int);
	pFunctionPointer pfn = NULL;
	handle = dlopen("/home/mangesh/Documents/RTR/SO/Demo.so", RTLD_LAZY);
	if (!handle) {
            fputs (dlerror(), stderr);
            exit(1);
        }
	pfn = (pFunctionPointer)dlsym(handle, "demoExportFunc");
	if ((error = dlerror()) != NULL)  {
	    fputs(error, stderr);
	    exit(1);
	}
	pfn(4);
	dlclose(handle);		
}
