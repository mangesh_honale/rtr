#include<stdio.h>

using namespace std;

union Student{
	int id;
	float age;
};

int main(){
	Student mangesh, rohit;
	mangesh.id = 1;
	printf("Id of Mangesh is %d and size of Mangesh union is %lu\n", mangesh.id, sizeof(mangesh));	
	rohit.age = 25.3;
	printf("Age of Rohit is %f and size of Rohit union is %lu\n", rohit.age, sizeof(rohit));
	return 0;
}
