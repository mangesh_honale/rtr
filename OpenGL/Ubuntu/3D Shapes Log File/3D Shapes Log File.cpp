#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <memory.h>

#include <X11/Xlib.h>
#include <X11/Xutil.h>
#include <X11/XKBlib.h>
#include <GL/gl.h>
#include <GL/glx.h>
#include <GL/glu.h>

using namespace std;

bool bFullScreen = false;
Display *gpDisplay = NULL;
XVisualInfo *gpXVisualInfo = NULL;
Colormap gColormap;
Window gWindow;
int giWindowWidth = 800;
int giWindowHeight = 600;
GLfloat gfTriRotationAngle, gfSquareRotationAngle = 360.0f;
GLXContext gGLXContext;
FILE *gpFile = NULL;

int main(void){
	//function declarations
	void CreateWindow(void);
	void ToggleFullScreen(void);
	void initialize(void);
	void display(void);
	void uninitialize();
	void resize(int, int);	
	void update(void);

	//variable declarations
	int winWidth = giWindowWidth;
	int winHeight = giWindowHeight;
	bool bDone = false;
	
	//open log file
	gpFile = fopen("Log.txt", "w");
	if(gpFile == NULL){
		printf("Error: Can not open log file.");
		uninitialize();
		exit(1);
	}
	CreateWindow();
	initialize();

	//message loop
	XEvent event;
	KeySym keysym;
	
	while(bDone == false){
		while(XPending(gpDisplay)){
			XNextEvent(gpDisplay, &event);
			switch(event.type){
				case MapNotify:
					break;
				case KeyPress:
					keysym = XkbKeycodeToKeysym(gpDisplay, event.xkey.keycode, 0, 0);
					switch(keysym){
						case XK_Escape:
							bDone = true;
							break;
						case XK_F:
						case XK_f:
							if(bFullScreen == false){
								ToggleFullScreen();
								bFullScreen = true;
							}else{
								ToggleFullScreen();
								bFullScreen = false;
							}
							break;
						default:
							break;

					}
					break;
				case ButtonPress:
					switch(event.xbutton.button){
						case 1: 
							break;
						case 2:
							break;
						case 3:
							break;
						default:
							break;
					}
					break;
				case MotionNotify:
					break;
				case ConfigureNotify:
					winWidth = event.xconfigure.width;
					winHeight = event.xconfigure.height;
					resize(winWidth, winHeight);
					break;
				case Expose:
					break;
				case DestroyNotify:
					break;
				case 33:
					bDone = true;
					break;
				default:
					break;
			}
		}
		update();
		display();
	}
	uninitialize();
	return (0);
}

void CreateWindow(void){
	fprintf(gpFile, "Entered CreateWindow() function.\n");
	//function prototypes
	void uninitialize(void);
	
	//variable declarations
	XSetWindowAttributes winAttribs;
	int defaultScreen;
	int defaultDepth;
	int styleMask;
	
	static int frameBufferAttributes[] = {
		GLX_RGBA,
		GLX_RED_SIZE, 8,
		GLX_GREEN_SIZE, 8,
		GLX_BLUE_SIZE, 8,
		GLX_ALPHA_SIZE, 8,
		GLX_DOUBLEBUFFER, True,
		GLX_DEPTH_SIZE, 24,
		None
	};

	//code
	gpDisplay = XOpenDisplay(NULL);
	if(gpDisplay == NULL){
		printf("Erroe: Unable to open X Display.\n Exiting now...\n");
		uninitialize();
		exit(1);
	}

	defaultScreen = XDefaultScreen(gpDisplay);
	

	gpXVisualInfo = glXChooseVisual(gpDisplay, defaultScreen, frameBufferAttributes);
	if (gpXVisualInfo == NULL) {
		printf("Error: Unable to allocate memory for Visual info.\nExitting Now.\n");
		uninitialize();
        	exit(1);
	}
	XMatchVisualInfo(gpDisplay, defaultScreen, defaultDepth, TrueColor, gpXVisualInfo);
	if(gpXVisualInfo == NULL) {
	    printf("Error: Unable to get VisualInfo. \nExitting...\n");
	    uninitialize();
	    exit(1);
	}
	winAttribs.border_pixmap = 0;
	winAttribs.border_pixel = 0;
	winAttribs.background_pixmap = 0;
	winAttribs.colormap = XCreateColormap(gpDisplay, RootWindow(gpDisplay, gpXVisualInfo->screen), gpXVisualInfo->visual, AllocNone);
        gColormap =  winAttribs.colormap;
	winAttribs.background_pixel =  BlackPixel(gpDisplay, defaultScreen);
	winAttribs.event_mask = ExposureMask | VisibilityChangeMask | ButtonPressMask | KeyPressMask | PointerMotionMask | StructureNotifyMask;

	styleMask =  CWBorderPixel | CWBackPixel | CWEventMask | CWColormap;

	gWindow = XCreateWindow(gpDisplay,
	RootWindow(gpDisplay, gpXVisualInfo->screen),
	0,  // X co-ordinate distance
	0,  // Y co-ordinate distance
	giWindowWidth, // Window width
	giWindowHeight, // Window height
	0, // Border Thickness, '0' is default
	gpXVisualInfo->depth,
	InputOutput,
	gpXVisualInfo->visual,
	styleMask,
	&winAttribs
	);

	if(!gWindow) {

	printf("Error: Failed to create main window.\nExitting... \n");
	uninitialize();
	exit(1);
	}
	
	XStoreName(gpDisplay, gWindow, "First OpenGL Window");
	
	Atom windowManagerDelete = XInternAtom(gpDisplay, "WM_DELETE_WINDOW", True);
	XSetWMProtocols(gpDisplay, gWindow, &windowManagerDelete, 1);
	XMapWindow(gpDisplay, gWindow);
	fprintf(gpFile, "Exiting CreateWindow() function.\n");
}

void ToggleFullScreen(){
	fprintf(gpFile, "Entered ToggleFullScreen() function.\n");
	Atom wm_state;
	Atom fullscreen;
	XEvent xev={0};

	//code
	wm_state = XInternAtom(gpDisplay, "_NET_WM_STATE", False);
	memset(&xev, 0, sizeof(xev));
	
	xev.type = ClientMessage;
	xev.xclient.window = gWindow;
	xev.xclient.message_type = wm_state;
	xev.xclient.format = 32;
	xev.xclient.data.l[0] = bFullScreen ? 0: 1;
	
	fullscreen = XInternAtom(gpDisplay, "_NET_WM_STATE_FULLSCREEN", False);
	xev.xclient.data.l[1] = fullscreen;
		
	XSendEvent(gpDisplay, RootWindow(gpDisplay, gpXVisualInfo->screen),
		  False,
		  StructureNotifyMask,
		  &xev);
	fprintf(gpFile, "Exiting ToggleFullScreen() function.\n");
}

void initialize(void){
	fprintf(gpFile, "Entered initialize() function.\n");
	//function prototypes
	void resize(int, int);
	
	gGLXContext = glXCreateContext(gpDisplay, gpXVisualInfo, NULL, GL_TRUE);
	
	glXMakeCurrent(gpDisplay, gWindow, gGLXContext);
	glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
	glClearDepth(1.0f);
	glEnable(GL_DEPTH_TEST);
	glDepthFunc(GL_LEQUAL);
	glShadeModel(GL_SMOOTH);
	glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST);
	resize(giWindowWidth, giWindowHeight);
	fprintf(gpFile, "Exiting initialize() function.\n");
}

void display(){
	fprintf(gpFile, "Entered display() function.\n");
	void drawPyramid();
	void drawCube();

	//code
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

	//draw triangle
	glTranslatef(-3.0f, 0.0f, -12.0f);
	glRotatef(gfTriRotationAngle, 0.0f, 1.0f, 0.0f);
	drawPyramid();
	
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	
	//draw square
	glTranslatef(3.0f, 0.0f, -12.0f);
	glRotatef(gfSquareRotationAngle, 1.0f, 1.0f, 1.0f);
	drawCube();
	glXSwapBuffers(gpDisplay,gWindow);
	fprintf(gpFile, "Exited display() function.\n");
}

void drawPyramid(){
	fprintf(gpFile, "Entered drawPyramid() function.\n");
	glBegin(GL_TRIANGLES);
		//Front face
	glColor3f(1.0f, 0.0f, 0.0f);
	glVertex3f(0.0f, 1.0f, 0.0f); 
	glColor3f(0.0f, 1.0f, 0.0f);
	glVertex3f(-1.0f, -1.0f, 1.0f);
	glColor3f(0.0f, 0.0f, 1.0f);
	glVertex3f(1.0f, -1.0f, 1.0f);

	//left face
	glColor3f(1.0f, 0.0f, 0.0f);
	glVertex3f(0.0f, 1.0f, 0.0f);
	glColor3f(0.0f, 0.0f, 1.0f);
	glVertex3f(1.0f, -1.0f, 1.0f);
	glColor3f(0.0f, 1.0f, 0.0f);
	glVertex3f(1.0f, -1.0f, -1.0f);

	//back face
	glColor3f(1.0f, 0.0f, 0.0f);
	glVertex3f(0.0f, 1.0f, 0.0f);
	glColor3f(0.0f, 1.0f, 0.0f);
	glVertex3f(1.0f, -1.0f, -1.0f);
	glColor3f(0.0f, 0.0f, 1.0f);
	glVertex3f(-1.0f, -1.0f, -1.0f);

	//right face
	glColor3f(1.0f, 0.0f, 0.0f);
	glVertex3f(0.0f, 1.0f, 0.0f); 
	glColor3f(0.0f, 0.0f, 1.0f);
	glVertex3f(-1.0f, -1.0f, -1.0f);
	glColor3f(0.0f, 1.0f, 0.0f);
	glVertex3f(-1.0f, -1.0f, 1.0f);
	glEnd();
	fprintf(gpFile, "Exiting drawPyramid() function.\n");
}

void drawCube(){
	fprintf(gpFile, "Entered drawCube() function.\n");
	glBegin(GL_QUADS);

	//Front face
	glColor3f(1.0f, 0.0f, 0.0f);
	glVertex3f(1.0f, 1.0f, 1.0f);
	glVertex3f(-1.0f, 1.0f, 1.0f);
	glVertex3f(-1.0f, -1.0f, 1.0f);
	glVertex3f(1.0f, -1.0f, 1.0f);

	//left face
	glColor3f(0.0f, 1.0f, 0.0f);
	glVertex3f(1.0f, 1.0f, -1.0f);
	glVertex3f(1.0f, 1.0f, 1.0f);
	glVertex3f(1.0f, -1.0f, 1.0f);
	glVertex3f(1.0f, -1.0f, -1.0f);

	//back face
	glColor3f(0.0f, 0.0f, 1.0f);
	glVertex3f(-1.0f, 1.0f, -1.0f);
	glVertex3f(1.0f, 1.0f, -1.0f);
	glVertex3f(1.0f, -1.0f, -1.0f);
	glVertex3f(-1.0f, -1.0f, -1.0f);

	//right face
	glColor3f(1.0f, 1.0f, 0.0f);
	glVertex3f(-1.0f, 1.0f, 1.0f);
	glVertex3f(-1.0f, 1.0f, -1.0f);
	glVertex3f(-1.0f, -1.0f, -1.0f);
	glVertex3f(-1.0f, -1.0f, 1.0f);

	//top face
	glColor3f(0.0f, 1.0f, 1.0f);
	glVertex3f(-1.0f, 1.0f, 1.0f);
	glVertex3f(1.0f, 1.0f, 1.0f);
	glVertex3f(1.0f, 1.0f, -1.0f);
	glVertex3f(-1.0f, 1.0f, -1.0f);

	//bottom face
	glColor3f(1.0f, 0.0f, 1.0f);
	glVertex3f(1.0f, -1.0f, 1.0f);
	glVertex3f(-1.0f, -1.0f, 1.0f);
	glVertex3f(-1.0f, -1.0f, -1.0f);
	glVertex3f(1.0f, -1.0f, -1.0f);
	glEnd();
	fprintf(gpFile, "Exiting drawCube() function.\n");
}
void resize(int width, int height){
	fprintf(gpFile, "Entered resize() function.\n");
	//code
	if(height == 0)
		height = 1;
	glViewport(0, 0, (GLsizei)width, (GLsizei)height);
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	gluPerspective(45.0f, (GLfloat)width/(GLfloat)height, 0.1f, 100.0f);
	fprintf(gpFile, "Exiting resize() function.\n");
}

void update(){
	fprintf(gpFile, "Entered update() function.\n");
	gfTriRotationAngle += 0.1f;
	if(gfTriRotationAngle >= 360.0f)
		gfTriRotationAngle = 0.0f;
	
	gfSquareRotationAngle -= 0.1f;
	if(gfSquareRotationAngle <= 0.0f)
		gfSquareRotationAngle = 360.0f; 
	fprintf(gpFile, "Exiting update() function.\n");
}
void uninitialize(void){
	fprintf(gpFile, "Entered initialize() function.\n");
	GLXContext currentGLXContext;
	currentGLXContext = glXGetCurrentContext();
	
	if(currentGLXContext != NULL && currentGLXContext == gGLXContext){
		glXMakeCurrent(gpDisplay, 0, 0);
	}

	if(gGLXContext)
		glXDestroyContext(gpDisplay, gGLXContext);

	if(gWindow)
		XDestroyWindow(gpDisplay, gWindow);
	if(gColormap)
		XFreeColormap(gpDisplay, gColormap);
	if(gpXVisualInfo){
		free(gpXVisualInfo);
		gpXVisualInfo = NULL;
	}
	if(gpDisplay){
		XCloseDisplay(gpDisplay);
		gpDisplay = NULL;
	}	
	if(gpFile){
		fprintf(gpFile, "Exiting program");
		fclose(gpFile);
		gpFile = NULL;
	}
}
