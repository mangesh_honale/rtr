#include <windows.h>
#include <stdio.h> 

#include <gl\glew.h>
#include <gl\GL.h>
#include "vmath.h"
#include "Sphere.h"
#pragma comment(lib,"glew32.lib")
#pragma comment(lib,"opengl32.lib")
#pragma comment(lib, "Sphere.lib")
#define WIN_WIDTH 800
#define WIN_HEIGHT 600

using namespace vmath;

enum {
	VDG_ATTRIBUTE_VERTEX = 0,
	VDG_ATTRIBUTE_COLOR,
	VDG_ATTRIBUTE_NORMAL,
	VDG_ATTRIBUTE_TEXTURE0,
};



mat4 gPerspectiveProjectionMatrix;
//Prototype Of WndProc() declared Globally
LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);

//Global variable declarations
FILE *gpFile = NULL;

HWND ghwnd = NULL;
HDC ghdc = NULL;
HGLRC ghrc = NULL;

DWORD dwStyle;
WINDOWPLACEMENT wpPrev = { sizeof(WINDOWPLACEMENT) };

bool gbActiveWindow = false;
bool gbEscapeKeyIsPressed = false;
bool gbFullscreen = false;
int gbPerVertexMode = 1;

GLuint gPerVertex_VertexShaderObject = false;
GLuint gPerVertex_FragmentShaderObject = false;
GLuint gPerVertexShaderProgramObject = false;

GLuint gPerFragment_VertexShaderObject = false;
GLuint gPerFragment_FragmentShaderObject = false;
GLuint gPerFragmentShaderProgramObject = false;

GLuint gNumElements;
GLuint gNumVertices;
float sphere_vertices[1146];
float sphere_normals[1146];
float sphere_textures[764];
unsigned short sphere_elements[2280];

GLuint gVao_sphere;
GLuint gVbo_sphere_position;
GLuint gVbo_sphere_normal;
GLuint gVbo_sphere_element;
GLuint model_matrix_uniform_perVer, view_matrix_uniform_perVer, projection_matrix_uniform_perVer;
GLuint L_KeyPressed_uniform_perVer;
GLuint La_uniform_perVer;
GLuint Ld_uniform_perVer;
GLuint Ls_uniform_perVer;
GLuint light_position_uniform_perVer;

GLuint Ka_uniform_perVer;
GLuint Kd_uniform_perVer;
GLuint Ks_uniform_perVer;
GLuint material_shininess_uniform_perVer;

GLuint model_matrix_uniform_perFrag, view_matrix_uniform_perFrag, projection_matrix_uniform_perFrag;
GLuint L_KeyPressed_uniform_perFrag;
GLuint La_uniform_perFrag;
GLuint Ld_uniform_perFrag;
GLuint Ls_uniform_perFrag;
GLuint light_position_uniform_perFrag;

GLuint Ka_uniform_perFrag;
GLuint Kd_uniform_perFrag;
GLuint Ks_uniform_perFrag;
GLuint material_shininess_uniform_perFrag;

mat4 gPrespectiveProjectionMatrix;

bool gbLight;


GLfloat lightAmbient[] = { 0.0f,0.0f,0.0f,1.0f };
GLfloat lightDiffuse[] = { 1.0f,1.0f,1.0f,1.0f };
GLfloat lightSpecular[] = { 1.0f,1.0f,1.0f,1.0f };
GLfloat lightPosition[] = { 100.0f,100.0f,100.0f,1.0f };

GLfloat material_ambient[] = { 0.0f,0.0f,0.0f,1.0f };
GLfloat material_diffuse[] = { 1.0f,1.0f,1.0f,1.0f };
GLfloat material_specular[] = { 1.0f,1.0f,1.0f,1.0f };
GLfloat material_shininess = 50.0f;
//main()
int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpszCmdLine, int iCmdShow)
{
	//function prototype
	void initialize(void);
	void uninitialize(void);
	void display(void);
	void Update();

	//variable declaration
	WNDCLASSEX wndclass;
	HWND hwnd;
	MSG msg;
	TCHAR szClassName[] = TEXT("OpenGLPP");
	bool bDone = false;

	//code
	if (fopen_s(&gpFile, "Log.txt", "w") != 0)
	{
		MessageBox(NULL, TEXT("Log File Can Not Be Created\nExitting ..."), TEXT("Error"), MB_OK | MB_TOPMOST | MB_ICONSTOP);
		exit(0);
	}
	else
	{
		fprintf(gpFile, "Log File Is Successfully Opened.\n");
	}

	wndclass.cbSize = sizeof(WNDCLASSEX);
	wndclass.style = CS_HREDRAW | CS_VREDRAW | CS_OWNDC;
	wndclass.cbClsExtra = 0;
	wndclass.cbWndExtra = 0;
	wndclass.hInstance = hInstance;
	wndclass.hbrBackground = (HBRUSH)GetStockObject(BLACK_BRUSH);
	wndclass.hIcon = LoadIcon(NULL, IDI_APPLICATION);
	wndclass.hCursor = LoadCursor(NULL, IDC_ARROW);
	wndclass.hIconSm = LoadIcon(NULL, IDI_APPLICATION);
	wndclass.lpfnWndProc = WndProc;
	wndclass.lpszClassName = szClassName;
	wndclass.lpszMenuName = NULL;

	RegisterClassEx(&wndclass);

	hwnd = CreateWindowEx(WS_EX_APPWINDOW,
		szClassName,
		TEXT("OpenGL Programmable Pipeline : Window"),
		WS_OVERLAPPEDWINDOW | WS_CLIPCHILDREN | WS_CLIPSIBLINGS | WS_VISIBLE,
		100,
		100,
		WIN_WIDTH,
		WIN_HEIGHT,
		NULL,
		NULL,
		hInstance,
		NULL);

	ghwnd = hwnd;

	ShowWindow(hwnd, iCmdShow);
	SetForegroundWindow(hwnd);
	SetFocus(hwnd);

	initialize();

	while (bDone == false)
	{
		if (PeekMessage(&msg, NULL, 0, 0, PM_REMOVE))
		{
			if (msg.message == WM_QUIT)
				bDone = true;
			else
			{
				TranslateMessage(&msg);
				DispatchMessage(&msg);
			}
		}
		else
		{

			display();

			if (gbActiveWindow == true)
			{
				if (gbEscapeKeyIsPressed == true)
					bDone = true;
			}
		}
	}

	uninitialize();

	return((int)msg.wParam);
}

//WndProc()
LRESULT CALLBACK WndProc(HWND hwnd, UINT iMsg, WPARAM wParam, LPARAM lParam)
{
	//function prototype
	void resize(int, int);
	void ToggleFullscreen(void);
	void uninitialize(void);

	// static variable declarations
	static bool bIsAKeyPressed = false;
	static bool bIsLKeyPressed = false;
	//code
	switch (iMsg)
	{
	case WM_ACTIVATE:
		if (HIWORD(wParam) == 0)
			gbActiveWindow = true;
		else
			gbActiveWindow = false;
		break;
	case WM_ERASEBKGND:
		return(0);
	case WM_SIZE:
		resize(LOWORD(lParam), HIWORD(lParam));
		break;
	case WM_KEYDOWN:
		switch (wParam)
		{
			// Quit the application
		case 0x51:
			gbEscapeKeyIsPressed = true;
			break;
			// toggle fullscreen
		case VK_ESCAPE:
			if (gbFullscreen == false)
			{
				ToggleFullscreen();
				gbFullscreen = true;
			}
			else
			{
				ToggleFullscreen();
				gbFullscreen = false;
			}
			break;
			// toggle between per vertex and per fragment mode
		case 0x46:
			gbPerVertexMode = 0;
			break;
		case 0x56: // 'V' key
			gbPerVertexMode = 1; //PerVertex
			break;
		case 0x4C: // for 'L' or 'l'
			if (bIsLKeyPressed == false)
			{
				gbLight = true;
				bIsLKeyPressed = true;
			}
			else
			{
				gbLight = false;
				bIsLKeyPressed = false;
			}
			break;
		default:
			break;
		}
		break;
	case WM_LBUTTONDOWN:
		break;
	case WM_CLOSE:
		uninitialize();
		break;
	case WM_DESTROY:
		PostQuitMessage(0);
		break;
	default:
		break;
	}
	return(DefWindowProc(hwnd, iMsg, wParam, lParam));
}

void ToggleFullscreen(void)
{
	//variable declarations
	MONITORINFO mi;

	//code
	if (gbFullscreen == false)
	{
		dwStyle = GetWindowLong(ghwnd, GWL_STYLE);
		if (dwStyle & WS_OVERLAPPEDWINDOW)
		{
			mi = { sizeof(MONITORINFO) };
			if (GetWindowPlacement(ghwnd, &wpPrev) && GetMonitorInfo(MonitorFromWindow(ghwnd, MONITORINFOF_PRIMARY), &mi))
			{
				SetWindowLong(ghwnd, GWL_STYLE, dwStyle & ~WS_OVERLAPPEDWINDOW);
				SetWindowPos(ghwnd, HWND_TOP, mi.rcMonitor.left, mi.rcMonitor.top, mi.rcMonitor.right - mi.rcMonitor.left, mi.rcMonitor.bottom - mi.rcMonitor.top, SWP_NOZORDER | SWP_FRAMECHANGED);
			}
		}
		ShowCursor(FALSE);
	}

	else
	{
		//code
		SetWindowLong(ghwnd, GWL_STYLE, dwStyle | WS_OVERLAPPEDWINDOW);
		SetWindowPlacement(ghwnd, &wpPrev);
		SetWindowPos(ghwnd, HWND_TOP, 0, 0, 0, 0, SWP_NOMOVE | SWP_NOSIZE | SWP_NOOWNERZORDER | SWP_NOZORDER | SWP_FRAMECHANGED);

		ShowCursor(TRUE);
	}
}

void initialize(void)
{
	//function prototypes
	void uninitialize(void);
	void resize(int, int);

	//variable declarations
	PIXELFORMATDESCRIPTOR pfd;
	int iPixelFormatIndex;

	//code
	ZeroMemory(&pfd, sizeof(PIXELFORMATDESCRIPTOR));

	pfd.nSize = sizeof(PIXELFORMATDESCRIPTOR);
	pfd.nVersion = 1;
	pfd.dwFlags = PFD_DRAW_TO_WINDOW | PFD_SUPPORT_OPENGL | PFD_DOUBLEBUFFER;
	pfd.iPixelType = PFD_TYPE_RGBA;
	pfd.cColorBits = 32;
	pfd.cRedBits = 8;
	pfd.cGreenBits = 8;
	pfd.cBlueBits = 8;
	pfd.cAlphaBits = 8;
	pfd.cDepthBits = 32;

	ghdc = GetDC(ghwnd);

	iPixelFormatIndex = ChoosePixelFormat(ghdc, &pfd);
	if (iPixelFormatIndex == 0)
	{
		ReleaseDC(ghwnd, ghdc);
		ghdc = NULL;
	}

	if (SetPixelFormat(ghdc, iPixelFormatIndex, &pfd) == false)
	{
		ReleaseDC(ghwnd, ghdc);
		ghdc = NULL;
	}

	ghrc = wglCreateContext(ghdc);
	if (ghrc == NULL)
	{
		ReleaseDC(ghwnd, ghdc);
		ghdc = NULL;
	}

	if (wglMakeCurrent(ghdc, ghrc) == false)
	{
		wglDeleteContext(ghrc);
		ghrc = NULL;
		ReleaseDC(ghwnd, ghdc);
		ghdc = NULL;
	}

	GLenum glew_error = glewInit();
	if (glew_error != GLEW_OK)
	{
		wglDeleteContext(ghrc);
		ghrc = NULL;
		ReleaseDC(ghwnd, ghdc);
		ghdc = NULL;
	}

	// ***Per vertex VERTEX SHADER ***
	// create shader
	gPerVertex_VertexShaderObject = glCreateShader(GL_VERTEX_SHADER);

	// provide source code to shader
	const GLchar *vertexShaderSourceCode =
		"#version 130" \
		"\n" \
		"in vec4 vPosition;" \
		"in vec3 vNormal;" \
		"uniform mat4 u_model_matrix;" \
		"uniform mat4 u_view_matrix;" \
		"uniform mat4 u_projection_matrix;" \
		"uniform int u_lighting_enabled;"\
		"uniform vec3 u_La;"\
		"uniform vec3 u_Ld;"\
		"uniform vec3 u_Ls;" \
		"uniform vec4 u_light_position;"\
		"uniform vec3 u_Ka;"\
		"uniform vec3 u_Ks;"\
		"uniform vec3 u_Kd;" \
		"uniform float u_material_shininess;" \
		"out vec3 phong_ads_color;" \
		"void main(void)" \
		"{" \
		"if (u_lighting_enabled == 1)" \
		"{" \
		"vec4 eye_coordinates=u_view_matrix * u_model_matrix * vPosition;" \
		"vec3 transformed_normals=normalize(mat3(u_view_matrix * u_model_matrix) * vNormal);" \
		"vec3 light_direction = normalize(vec3(u_light_position) - eye_coordinates.xyz);" \
		"float tn_dot_ld = max(dot(transformed_normals, light_direction),0.0);" \
		"vec3 ambient = u_La * u_Ka;" \
		"vec3 diffuse = u_Ld * u_Kd * tn_dot_ld;" \
		"vec3 reflection_vector = reflect(-light_direction, transformed_normals);" \
		"vec3 viewer_vector = normalize(-eye_coordinates.xyz);" \
		"vec3 specular = u_Ls * u_Ks * pow(max(dot(reflection_vector, viewer_vector), 0.0), u_material_shininess);" \
		"phong_ads_color=ambient + diffuse + specular;" \
		"}" \
		"else" \
		"{" \
		"phong_ads_color = vec3(1.0, 1.0, 1.0);" \
		"}" \
		"gl_Position=u_projection_matrix * u_view_matrix * u_model_matrix * vPosition;" \
		"}";


	glShaderSource(gPerVertex_VertexShaderObject, 1, (const GLchar **)&vertexShaderSourceCode, NULL);

	// compile shader
	glCompileShader(gPerVertex_VertexShaderObject);
	GLint iInfoLogLength = 0;
	GLint iShaderCompiledStatus = 0;
	char *szInfoLog = NULL;
	glGetShaderiv(gPerVertex_VertexShaderObject, GL_COMPILE_STATUS, &iShaderCompiledStatus);
	if (iShaderCompiledStatus == GL_FALSE)
	{
		glGetShaderiv(gPerVertex_VertexShaderObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
		if (iInfoLogLength > 0)
		{
			szInfoLog = (char *)malloc(iInfoLogLength);
			if (szInfoLog != NULL)
			{
				GLsizei written;
				glGetShaderInfoLog(gPerVertex_VertexShaderObject, iInfoLogLength, &written, szInfoLog);
				fprintf(gpFile, "Vertex Shader Compilation Log : %s\n", szInfoLog);
				free(szInfoLog);
				uninitialize();
				exit(0);
			}
		}
	}

	// *** FRAGMENT SHADER ***
	// create shader
	gPerVertex_FragmentShaderObject = glCreateShader(GL_FRAGMENT_SHADER);

	// provide source code to shader
	const GLchar *fragmentShaderSourceCode =
		"#version 130" \
		"\n" \
		"in vec3 phong_ads_color;" \
		"out vec4 FragColor;" \
		"void main(void)" \
		"{" \
		"FragColor = vec4(phong_ads_color, 1.0);" \
		"}";

	glShaderSource(gPerVertex_FragmentShaderObject, 1, (const GLchar **)&fragmentShaderSourceCode, NULL);

	// compile shader
	glCompileShader(gPerVertex_FragmentShaderObject);
	glGetShaderiv(gPerVertex_FragmentShaderObject, GL_COMPILE_STATUS, &iShaderCompiledStatus);
	if (iShaderCompiledStatus == GL_FALSE)
	{
		glGetShaderiv(gPerVertex_FragmentShaderObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
		if (iInfoLogLength > 0)
		{
			szInfoLog = (char *)malloc(iInfoLogLength);
			if (szInfoLog != NULL)
			{
				GLsizei written;
				glGetShaderInfoLog(gPerVertex_FragmentShaderObject, iInfoLogLength, &written, szInfoLog);
				fprintf(gpFile, "Fragment Shader Compilation Log : %s\n", szInfoLog);
				free(szInfoLog);
				uninitialize();
				exit(0);
			}
		}
	}

	// *** SHADER PROGRAM ***
	// create
	gPerVertexShaderProgramObject = glCreateProgram();

	// attach vertex shader to shader program
	glAttachShader(gPerVertexShaderProgramObject, gPerVertex_VertexShaderObject);

	// attach fragment shader to shader program
	glAttachShader(gPerVertexShaderProgramObject, gPerVertex_FragmentShaderObject);

	// pre-link binding of shader program object with vertex shader position attribute
	glBindAttribLocation(gPerVertexShaderProgramObject, VDG_ATTRIBUTE_VERTEX, "vPosition");
	glBindAttribLocation(gPerVertexShaderProgramObject, VDG_ATTRIBUTE_NORMAL, "vNormal");

	// link shader
	glLinkProgram(gPerVertexShaderProgramObject);
	GLint iShaderProgramLinkStatus = 0;
	glGetProgramiv(gPerVertexShaderProgramObject, GL_LINK_STATUS, &iShaderProgramLinkStatus);
	if (iShaderProgramLinkStatus == GL_FALSE)
	{
		glGetProgramiv(gPerVertexShaderProgramObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
		if (iInfoLogLength>0)
		{
			szInfoLog = (char *)malloc(iInfoLogLength);
			if (szInfoLog != NULL)
			{
				GLsizei written;
				glGetProgramInfoLog(gPerVertexShaderProgramObject, iInfoLogLength, &written, szInfoLog);
				fprintf(gpFile, "Shader Program Link Log : %s\n", szInfoLog);
				free(szInfoLog);
				uninitialize();
				exit(0);
			}
		}
	}

	// get uniform locations
	model_matrix_uniform_perVer = glGetUniformLocation(gPerVertexShaderProgramObject, "u_model_matrix");
	view_matrix_uniform_perVer = glGetUniformLocation(gPerVertexShaderProgramObject, "u_view_matrix");
	projection_matrix_uniform_perVer = glGetUniformLocation(gPerVertexShaderProgramObject, "u_projection_matrix");


	L_KeyPressed_uniform_perVer = glGetUniformLocation(gPerVertexShaderProgramObject, "u_lighting_enabled");

	// Ambient color intensity of light
	La_uniform_perVer = glGetUniformLocation(gPerVertexShaderProgramObject, "u_La");
	// Diffuse color intensity of light
	Ld_uniform_perVer = glGetUniformLocation(gPerVertexShaderProgramObject, "u_Ld");
	// Specular color intensity of light
	Ls_uniform_perVer = glGetUniformLocation(gPerVertexShaderProgramObject, "u_Ls");

	// Position of light
	light_position_uniform_perVer = glGetUniformLocation(gPerVertexShaderProgramObject, "u_light_position");;

	// Ambient color reflective intensity of material
	Ka_uniform_perVer = glGetUniformLocation(gPerVertexShaderProgramObject, "u_Ka");
	// diffuse reflective color intensity of material
	Kd_uniform_perVer = glGetUniformLocation(gPerVertexShaderProgramObject, "u_Kd");
	// specular reflective color intensity of material
	Ks_uniform_perVer = glGetUniformLocation(gPerVertexShaderProgramObject, "u_Ks");
	// shininess of material ( value is conventionally between 1 to 200 )
	material_shininess_uniform_perVer = glGetUniformLocation(gPerVertexShaderProgramObject, "u_material_shininess");;


	// Per fragment 
	gPerFragment_VertexShaderObject = glCreateShader(GL_VERTEX_SHADER);

	// provide source code to shader
	vertexShaderSourceCode =
		"#version 130" \
		"\n" \
		"in vec4 vPosition;" \
		"in vec3 vNormal;" \
		"uniform mat4 u_model_matrix;" \
		"uniform mat4 u_view_matrix;" \
		"uniform mat4 u_projection_matrix;" \
		"uniform vec4 u_light_position;" \
		"uniform int u_lighting_enabled;" \
		"out vec3 transformed_normals;" \
		"out vec3 light_direction;" \
		"out vec3 viewer_vector;" \
		"void main(void)" \
		"{" \
		"if(u_lighting_enabled==1)" \
		"{" \
		"vec4 eye_coordinates=u_view_matrix * u_model_matrix * vPosition;" \
		"transformed_normals=mat3(u_view_matrix * u_model_matrix) * vNormal;" \
		"light_direction = vec3(u_light_position) - eye_coordinates.xyz;" \
		"viewer_vector = -eye_coordinates.xyz;" \
		"}" \
		"gl_Position=u_projection_matrix * u_view_matrix * u_model_matrix * vPosition;" \
		"}";


	glShaderSource(gPerFragment_VertexShaderObject, 1, (const GLchar **)&vertexShaderSourceCode, NULL);

	// compile shader
	glCompileShader(gPerFragment_VertexShaderObject);
	iInfoLogLength = 0;
	iShaderCompiledStatus = 0;
	szInfoLog = NULL;
	glGetShaderiv(gPerFragment_VertexShaderObject, GL_COMPILE_STATUS, &iShaderCompiledStatus);
	if (iShaderCompiledStatus == GL_FALSE)
	{
		glGetShaderiv(gPerFragment_VertexShaderObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
		if (iInfoLogLength > 0)
		{
			szInfoLog = (char *)malloc(iInfoLogLength);
			if (szInfoLog != NULL)
			{
				GLsizei written;
				glGetShaderInfoLog(gPerFragment_VertexShaderObject, iInfoLogLength, &written, szInfoLog);
				fprintf(gpFile, "Vertex Shader Compilation Log : %s\n", szInfoLog);
				free(szInfoLog);
				uninitialize();
				exit(0);
			}
		}
	}

	// *** FRAGMENT SHADER ***
	// create shader
	gPerFragment_FragmentShaderObject = glCreateShader(GL_FRAGMENT_SHADER);

	// provide source code to shader
	fragmentShaderSourceCode =
		"#version 130" \
		"\n" \
		"in vec3 transformed_normals;" \
		"in vec3 light_direction;" \
		"in vec3 viewer_vector;" \
		"out vec4 FragColor;" \
		"uniform vec3 u_La;" \
		"uniform vec3 u_Ld;" \
		"uniform vec3 u_Ls;" \
		"uniform vec3 u_Ka;" \
		"uniform vec3 u_Kd;" \
		"uniform vec3 u_Ks;" \
		"uniform float u_material_shininess;" \
		"uniform int u_lighting_enabled;" \
		"void main(void)" \
		"{" \
		"vec3 phong_ads_color;" \
		"if(u_lighting_enabled==1)" \
		"{" \
		"vec3 normalized_transformed_normals=normalize(transformed_normals);" \
		"vec3 normalized_light_direction=normalize(light_direction);" \
		"vec3 normalized_viewer_vector=normalize(viewer_vector);" \
		"vec3 ambient = u_La * u_Ka;" \
		"float tn_dot_ld = max(dot(normalized_transformed_normals, normalized_light_direction),0.0);" \
		"vec3 diffuse = u_Ld * u_Kd * tn_dot_ld;" \
		"vec3 reflection_vector = reflect(-normalized_light_direction, normalized_transformed_normals);" \
		"vec3 specular = u_Ls * u_Ks * pow(max(dot(reflection_vector, normalized_viewer_vector), 0.0), u_material_shininess);" \
		"phong_ads_color=ambient + diffuse + specular;" \
		"}" \
		"else" \
		"{" \
		"phong_ads_color = vec3(1.0, 1.0, 1.0);" \
		"}" \
		"FragColor = vec4(phong_ads_color, 1.0);" \
		"}";

	glShaderSource(gPerFragment_FragmentShaderObject, 1, (const GLchar **)&fragmentShaderSourceCode, NULL);

	// compile shader
	glCompileShader(gPerFragment_FragmentShaderObject);
	glGetShaderiv(gPerFragment_FragmentShaderObject, GL_COMPILE_STATUS, &iShaderCompiledStatus);
	if (iShaderCompiledStatus == GL_FALSE)
	{
		glGetShaderiv(gPerFragment_FragmentShaderObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
		if (iInfoLogLength > 0)
		{
			szInfoLog = (char *)malloc(iInfoLogLength);
			if (szInfoLog != NULL)
			{
				GLsizei written;
				glGetShaderInfoLog(gPerFragment_FragmentShaderObject, iInfoLogLength, &written, szInfoLog);
				fprintf(gpFile, "Fragment Shader Compilation Log : %s\n", szInfoLog);
				free(szInfoLog);
				uninitialize();
				exit(0);
			}
		}
	}

	// *** SHADER PROGRAM ***
	// create
	gPerFragmentShaderProgramObject = glCreateProgram();

	// attach vertex shader to shader program
	glAttachShader(gPerFragmentShaderProgramObject, gPerFragment_VertexShaderObject);

	// attach fragment shader to shader program
	glAttachShader(gPerFragmentShaderProgramObject, gPerFragment_FragmentShaderObject);

	// pre-link binding of shader program object with vertex shader position attribute
	glBindAttribLocation(gPerFragmentShaderProgramObject, VDG_ATTRIBUTE_VERTEX, "vPosition");
	glBindAttribLocation(gPerFragmentShaderProgramObject, VDG_ATTRIBUTE_NORMAL, "vNormal");

	// link shader
	glLinkProgram(gPerFragmentShaderProgramObject);
	iShaderProgramLinkStatus = 0;
	glGetProgramiv(gPerFragmentShaderProgramObject, GL_LINK_STATUS, &iShaderProgramLinkStatus);
	if (iShaderProgramLinkStatus == GL_FALSE)
	{
		glGetProgramiv(gPerFragmentShaderProgramObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
		if (iInfoLogLength>0)
		{
			szInfoLog = (char *)malloc(iInfoLogLength);
			if (szInfoLog != NULL)
			{
				GLsizei written;
				glGetProgramInfoLog(gPerFragmentShaderProgramObject, iInfoLogLength, &written, szInfoLog);
				fprintf(gpFile, "Shader Program Link Log : %s\n", szInfoLog);
				free(szInfoLog);
				uninitialize();
				exit(0);
			}
		}
	}

	// get uniform locations
	model_matrix_uniform_perFrag = glGetUniformLocation(gPerFragmentShaderProgramObject, "u_model_matrix");
	view_matrix_uniform_perFrag = glGetUniformLocation(gPerFragmentShaderProgramObject, "u_view_matrix");
	projection_matrix_uniform_perFrag = glGetUniformLocation(gPerFragmentShaderProgramObject, "u_projection_matrix");


	L_KeyPressed_uniform_perFrag = glGetUniformLocation(gPerFragmentShaderProgramObject, "u_lighting_enabled");

	// Ambient color intensity of light
	La_uniform_perFrag = glGetUniformLocation(gPerFragmentShaderProgramObject, "u_La");
	// Diffuse color intensity of light
	Ld_uniform_perFrag = glGetUniformLocation(gPerFragmentShaderProgramObject, "u_Ld");
	// Specular color intensity of light
	Ls_uniform_perFrag = glGetUniformLocation(gPerFragmentShaderProgramObject, "u_Ls");

	// Position of light
	light_position_uniform_perFrag = glGetUniformLocation(gPerFragmentShaderProgramObject, "u_light_position");;

	// Ambient color reflective intensity of material
	Ka_uniform_perFrag = glGetUniformLocation(gPerFragmentShaderProgramObject, "u_Ka");
	// diffuse reflective color intensity of material
	Kd_uniform_perFrag = glGetUniformLocation(gPerFragmentShaderProgramObject, "u_Kd");
	// specular reflective color intensity of material
	Ks_uniform_perFrag = glGetUniformLocation(gPerFragmentShaderProgramObject, "u_Ks");
	// shininess of material ( value is conventionally between 1 to 200 )
	material_shininess_uniform_perFrag = glGetUniformLocation(gPerFragmentShaderProgramObject, "u_material_shininess");;


	// *** vertices, colors, shader attribs, vbo, vao initializations ***
	getSphereVertexData(sphere_vertices, sphere_normals, sphere_textures, sphere_elements);

	gNumVertices = getNumberOfSphereVertices();
	gNumElements = getNumberOfSphereElements();
	// *** vertices, colors, shader attribs, vbo, vao initializations ***


	// CUBE CODE
	// vao
	glGenVertexArrays(1, &gVao_sphere);
	glBindVertexArray(gVao_sphere);

	// position vbo
	glGenBuffers(1, &gVbo_sphere_position);
	glBindBuffer(GL_ARRAY_BUFFER, gVbo_sphere_position);
	glBufferData(GL_ARRAY_BUFFER, sizeof(sphere_vertices), sphere_vertices, GL_STATIC_DRAW);

	glVertexAttribPointer(VDG_ATTRIBUTE_VERTEX, 3, GL_FLOAT, GL_FALSE, 0, NULL);

	glEnableVertexAttribArray(VDG_ATTRIBUTE_VERTEX);

	glBindBuffer(GL_ARRAY_BUFFER, 0);

	// normal vbo
	glGenBuffers(1, &gVbo_sphere_normal);
	glBindBuffer(GL_ARRAY_BUFFER, gVbo_sphere_normal);
	glBufferData(GL_ARRAY_BUFFER, sizeof(sphere_normals), sphere_normals, GL_STATIC_DRAW);

	glVertexAttribPointer(VDG_ATTRIBUTE_NORMAL, 3, GL_FLOAT, GL_FALSE, 0, NULL);

	glEnableVertexAttribArray(VDG_ATTRIBUTE_NORMAL);

	glBindBuffer(GL_ARRAY_BUFFER, 0);

	// element vbo
	glGenBuffers(1, &gVbo_sphere_element);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, gVbo_sphere_element);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(sphere_elements), sphere_elements, GL_STATIC_DRAW);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);

	glBindVertexArray(0);

	glShadeModel(GL_SMOOTH);
	// set-up depth buffer
	glClearDepth(1.0f);
	// enable depth testing
	glEnable(GL_DEPTH_TEST);
	// depth test to do
	glDepthFunc(GL_LEQUAL);
	// set really nice percpective calculations ?
	glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST);
	// We will always cull back faces for better performance
	glEnable(GL_CULL_FACE);

	// set background color
	glClearColor(0.0f, 0.0f, 0.0f, 0.0f); // black

										  // set perspective matrix to identitu matrix
	gPerspectiveProjectionMatrix = mat4::identity();

	gbLight = false;

	// resize
	resize(WIN_WIDTH, WIN_HEIGHT);

}

void display(void)
{
	//code
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	// start using OpenGL program object
	if (gbPerVertexMode == 1) {
		glUseProgram(gPerVertexShaderProgramObject);
		if (gbLight == true)
		{

			// set 'u_lighting_enabled' uniform
			glUniform1i(L_KeyPressed_uniform_perVer, 1);

			// setting light's properties
			glUniform3fv(La_uniform_perVer, 1, lightAmbient);
			glUniform3fv(Ld_uniform_perVer, 1, lightDiffuse);
			glUniform3fv(Ls_uniform_perVer, 1, lightSpecular);
			glUniform4fv(light_position_uniform_perVer, 1, lightPosition);

			// setting material's properties
			glUniform3fv(Ka_uniform_perVer, 1, material_ambient);
			glUniform3fv(Kd_uniform_perVer, 1, material_diffuse);
			glUniform3fv(Ks_uniform_perVer, 1, material_specular);
			glUniform1f(material_shininess_uniform_perVer, material_shininess);
		}
		else
		{
			// set 'u_lighting_enabled' uniform

			glUniform1i(L_KeyPressed_uniform_perVer, 0);
		}

		// OpenGL Drawing
		// set all matrices to identity
		mat4 modelMatrix = mat4::identity();
		mat4 viewMatrix = mat4::identity();

		// apply z axis translation to go deep into the screen by -2.0,
		// so that triangle with same fullscreen co-ordinates, but due to above translation will look small
		modelMatrix = translate(0.0f, 0.0f, -2.0f);

		glUniformMatrix4fv(model_matrix_uniform_perVer, 1, GL_FALSE, modelMatrix);
		glUniformMatrix4fv(view_matrix_uniform_perVer, 1, GL_FALSE, viewMatrix);
		glUniformMatrix4fv(projection_matrix_uniform_perVer, 1, GL_FALSE, gPerspectiveProjectionMatrix);

		// *** bind vao ***
		glBindVertexArray(gVao_sphere);

		// *** draw, either by glDrawTriangles() or glDrawArrays() or glDrawElements()
		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, gVbo_sphere_element);
		glDrawElements(GL_TRIANGLES, gNumElements, GL_UNSIGNED_SHORT, 0);
	}
	else {
		glUseProgram(gPerFragmentShaderProgramObject);
		if (gbLight == true)
		{

			// set 'u_lighting_enabled' uniform
			glUniform1i(L_KeyPressed_uniform_perFrag, 1);

			// setting light's properties
			glUniform3fv(La_uniform_perFrag, 1, lightAmbient);
			glUniform3fv(Ld_uniform_perFrag, 1, lightDiffuse);
			glUniform3fv(Ls_uniform_perFrag, 1, lightSpecular);
			glUniform4fv(light_position_uniform_perFrag, 1, lightPosition);

			// setting material's properties
			glUniform3fv(Ka_uniform_perFrag, 1, material_ambient);
			glUniform3fv(Kd_uniform_perFrag, 1, material_diffuse);
			glUniform3fv(Ks_uniform_perFrag, 1, material_specular);
			glUniform1f(material_shininess_uniform_perFrag, material_shininess);
		}
		else
		{
			// set 'u_lighting_enabled' uniform

			glUniform1i(L_KeyPressed_uniform_perFrag, 0);
		}

		// OpenGL Drawing
		// set all matrices to identity
		mat4 modelMatrix = mat4::identity();
		mat4 viewMatrix = mat4::identity();

		// apply z axis translation to go deep into the screen by -2.0,
		// so that triangle with same fullscreen co-ordinates, but due to above translation will look small
		modelMatrix = translate(0.0f, 0.0f, -2.0f);

		glUniformMatrix4fv(model_matrix_uniform_perFrag, 1, GL_FALSE, modelMatrix);
		glUniformMatrix4fv(view_matrix_uniform_perFrag, 1, GL_FALSE, viewMatrix);
		glUniformMatrix4fv(projection_matrix_uniform_perFrag, 1, GL_FALSE, gPerspectiveProjectionMatrix);

		// *** bind vao ***
		glBindVertexArray(gVao_sphere);

		// *** draw, either by glDrawTriangles() or glDrawArrays() or glDrawElements()
		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, gVbo_sphere_element);
		glDrawElements(GL_TRIANGLES, gNumElements, GL_UNSIGNED_SHORT, 0);
	}



	// *** unbind vao ***
	glBindVertexArray(0);

	// stop using OpenGL program object
	glUseProgram(0);

	SwapBuffers(ghdc);
}

void resize(int width, int height)
{
	//code
	if (height == 0)
		height = 1;
	glViewport(0, 0, (GLsizei)width, (GLsizei)height);
	gPerspectiveProjectionMatrix = perspective(45.0f, (GLfloat)width / (GLfloat)height, 0.1f, 100.0f);
}


void uninitialize(void)
{
	//code
	if (gbFullscreen == true)
	{
		dwStyle = GetWindowLong(ghwnd, GWL_STYLE);
		SetWindowLong(ghwnd, GWL_STYLE, dwStyle | WS_OVERLAPPEDWINDOW);
		SetWindowPlacement(ghwnd, &wpPrev);
		SetWindowPos(ghwnd, HWND_TOP, 0, 0, 0, 0, SWP_NOMOVE | SWP_NOSIZE | SWP_NOOWNERZORDER | SWP_NOZORDER | SWP_FRAMECHANGED);

		ShowCursor(TRUE);

	}


	// destroy vao of square
	if (gVao_sphere)
	{
		glDeleteVertexArrays(1, &gVao_sphere);
		gVao_sphere = 0;
	}



	// destroy vbo
	if (gVbo_sphere_position)
	{
		glDeleteBuffers(1, &gVbo_sphere_position);
		gVbo_sphere_position = 0;
	}
	if (gVbo_sphere_normal)
	{
		glDeleteBuffers(1, &gVbo_sphere_normal);
		gVbo_sphere_normal = 0;
	}
	if (gVbo_sphere_element)
	{
		glDeleteBuffers(1, &gVbo_sphere_element);
		gVbo_sphere_element = 0;
	}
	// Detach vertex shader from shader program object
	glDetachShader(gPerVertexShaderProgramObject, gPerVertex_VertexShaderObject);

	// Detach fragment shader from shader program object
	glDetachShader(gPerVertexShaderProgramObject, gPerVertex_FragmentShaderObject);

	// Delete vertex shader object
	glDeleteShader(gPerVertex_VertexShaderObject);
	gPerVertex_VertexShaderObject = 0;

	// Delete fragment shader object
	glDeleteShader(gPerVertex_FragmentShaderObject);
	gPerVertex_FragmentShaderObject = 0;

	// Delete shader program object
	glDeleteProgram(gPerVertexShaderProgramObject);
	gPerVertexShaderProgramObject = 0;

	glDetachShader(gPerFragmentShaderProgramObject, gPerFragment_VertexShaderObject);

	// Detach fragment shader from shader program object
	glDetachShader(gPerFragmentShaderProgramObject, gPerFragment_FragmentShaderObject);

	// Delete vertex shader object
	glDeleteShader(gPerFragment_VertexShaderObject);
	gPerFragment_VertexShaderObject = 0;

	// Delete fragment shader object
	glDeleteShader(gPerFragment_FragmentShaderObject);
	gPerFragment_FragmentShaderObject = 0;

	// Delete shader program object
	glDeleteProgram(gPerFragmentShaderProgramObject);
	gPerFragmentShaderProgramObject = 0;

	// Unlink shader program
	glUseProgram(0);
	wglMakeCurrent(NULL, NULL);

	wglDeleteContext(ghrc);
	ghrc = NULL;

	ReleaseDC(ghwnd, ghdc);
	ghdc = NULL;

	if (gpFile)
	{
		fprintf(gpFile, "Log File Is Successfully Closed.\n");
		fclose(gpFile);
		gpFile = NULL;
	}
}
