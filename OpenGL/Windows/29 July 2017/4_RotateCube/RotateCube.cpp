/*
Do the single revolving cube application without using translation, scaling and rotation library functions.
Use glLoadMatrix and glMultMatrix instead.
*/
#include <windows.h>
#include <gl/GL.h>
#include <gl/GLU.h>
#include <math.h>

#define WIN_WIDTH 800
#define WIN_HEIGHT 600
#define PI 3.1415926535898f

#pragma comment(lib,"opengl32.lib")
#pragma comment(lib,"glu32.lib")


// Global function declaration
LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);

// Global variable declaration
HWND ghwnd = NULL;
HDC ghdc = NULL;
HGLRC ghrc = NULL;
//matrices declaration
GLfloat gfIdentityMatrix[16], gfTranslationMatrix[16], gfScaleMatrix[16],
gfXRotationMatrix[16], gfYRotationMatrix[16], gfZRotationMatrix[16];

DWORD dwStyle;
WINDOWPLACEMENT wpPrev = { sizeof(WINDOWPLACEMENT) };

int giScreenWidth;
int giScreenHeight;

bool gbActiveWindow = false;
bool gbEscapeKeyIsPressed = false;
bool gbFullScreen = false;
GLfloat gfCubeDegreeAngle = 0.0f, gfCubeRadianAngle = 0.0f;
// main()
int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpszCmdLine, int iCmdShow) {
	// Function prototype
	void Initialize(void);
	void Uninitialize(void);
	void Display(void);
	void Update(void);

	// Variable declaration
	WNDCLASSEX wndClass;
	HWND hwnd;
	MSG msg;
	TCHAR szClassName[] = TEXT("RTROGL");
	bool bDone = false;
	giScreenWidth = GetSystemMetrics(SM_CXSCREEN);
	giScreenHeight = GetSystemMetrics(SM_CYSCREEN);


	// Code
	// Initializing members of struct WNDCLASSEX
	wndClass.cbSize = sizeof(WNDCLASSEX);
	wndClass.style = CS_HREDRAW | CS_VREDRAW | CS_OWNDC;
	wndClass.cbClsExtra = 0;
	wndClass.cbWndExtra = 0;
	wndClass.hInstance = hInstance;
	wndClass.hbrBackground = (HBRUSH)GetStockObject(BLACK_BRUSH);
	wndClass.hIcon = LoadIcon(NULL, IDI_APPLICATION);
	wndClass.hCursor = LoadCursor(NULL, IDC_ARROW);
	wndClass.hIconSm = LoadIcon(NULL, IDI_APPLICATION);
	wndClass.lpfnWndProc = WndProc;
	wndClass.lpszClassName = szClassName;
	wndClass.lpszMenuName = NULL;

	// Registering class
	RegisterClassEx(&wndClass);

	// Create window
	hwnd = CreateWindowEx(WS_EX_APPWINDOW,
		szClassName,
		TEXT("OpenGL Fixed Function Pipeline Using Native Windowing: Double Buffer Window"),
		WS_OVERLAPPEDWINDOW | WS_CLIPCHILDREN | WS_CLIPSIBLINGS | WS_VISIBLE,
		(giScreenWidth / 2) - 400,
		(giScreenHeight / 2) - 300,
		WIN_WIDTH,
		WIN_HEIGHT,
		NULL,
		NULL,
		hInstance,
		NULL);
	ghwnd = hwnd;

	// Initialize
	Initialize();

	ShowWindow(hwnd, SW_SHOW);
	SetForegroundWindow(hwnd);
	SetFocus(hwnd);

	// Message loop
	while (bDone == false)
	{
		if (PeekMessage(&msg, NULL, 0, 0, PM_REMOVE))
		{
			if (msg.message == WM_QUIT)
				bDone = true;
			else
			{
				TranslateMessage(&msg);
				DispatchMessage(&msg);
			}
		}
		else
		{
			if (gbActiveWindow == true)
			{
				if (gbEscapeKeyIsPressed == true)
					bDone = true;
				Update();
				Display();
			}
		}
	}
	Uninitialize();
	return((int)msg.wParam);
}

// WndProc()
LRESULT CALLBACK WndProc(HWND hwnd, UINT iMsg, WPARAM wParam, LPARAM lParam)
{
	// Function prototype
	void Resize(int, int);
	void ToggleFullscreen(void);
	void Uninitialize(void);

	// Code
	switch (iMsg)
	{
	case WM_ACTIVATE:
		if (HIWORD(wParam) == 0)
			gbActiveWindow = true;
		else
			gbActiveWindow = false;
		break;
	case WM_SIZE:
		Resize(LOWORD(lParam), HIWORD(lParam));
		break;
	case WM_KEYDOWN:
		switch (wParam)
		{
		case VK_ESCAPE:
			gbEscapeKeyIsPressed = true;
			break;
		case 0x46:
			if (gbFullScreen == false)
			{
				ToggleFullscreen();
				gbFullScreen = true;
			}
			else
			{
				ToggleFullscreen();
				gbFullScreen = false;
			}
			break;
		}
		break;
	case WM_DESTROY:
		PostQuitMessage(0);
		break;
	default:
		break;
	}
	return(DefWindowProc(hwnd, iMsg, wParam, lParam));
}

void ToggleFullscreen(void)
{
	// Variable declaration
	MONITORINFO  mi;
	int x, y;

	// Code
	if (gbFullScreen == false)
	{
		dwStyle = GetWindowLong(ghwnd, GWL_STYLE);
		if (dwStyle & WS_OVERLAPPEDWINDOW)
		{
			mi = { sizeof(MONITORINFO) };
			if (GetWindowPlacement(ghwnd, &wpPrev) && GetMonitorInfo(MonitorFromWindow(ghwnd, MONITORINFOF_PRIMARY), &mi))
			{
				SetWindowLong(ghwnd, GWL_STYLE, dwStyle & ~WS_OVERLAPPEDWINDOW);
				SetWindowPos(ghwnd, HWND_TOP, mi.rcMonitor.left, mi.rcMonitor.top, mi.rcMonitor.right - mi.rcMonitor.left, mi.rcMonitor.bottom - mi.rcMonitor.top, SWP_NOZORDER | SWP_FRAMECHANGED);
			}
		}
		ShowCursor(FALSE);
	}
	else
	{
		// Code
		x = (giScreenWidth / 2) - ((wpPrev.rcNormalPosition.right - wpPrev.rcNormalPosition.left) / 2);
		y = (giScreenHeight / 2) - ((wpPrev.rcNormalPosition.bottom - wpPrev.rcNormalPosition.top) / 2);
		SetWindowLong(ghwnd, GWL_STYLE, dwStyle | WS_OVERLAPPEDWINDOW);
		SetWindowPlacement(ghwnd, &wpPrev);
		SetWindowPos(ghwnd, HWND_TOP, x, y, 0, 0, SWP_NOSIZE | SWP_NOOWNERZORDER | SWP_NOZORDER | SWP_FRAMECHANGED);
		ShowCursor(TRUE);
	}
}

void Initialize(void)
{
	// Function prototypes
	void Resize(int, int);
	void initializeIdentityeMatrix(void);
	void initializeTranslationMatrix(void);
	void initializeScaleMatrix(void);

	// Variable Declarations
	PIXELFORMATDESCRIPTOR pfd;
	int iPixelFormatIndex;

	// Code
	ZeroMemory(&pfd, sizeof(PIXELFORMATDESCRIPTOR));

	// Initialization of structure 'PIXELFORMATDESCRIPTOR'
	pfd.nSize = sizeof(PIXELFORMATDESCRIPTOR);
	pfd.nVersion = 1;
	pfd.dwFlags = PFD_DRAW_TO_WINDOW | PFD_SUPPORT_OPENGL | PFD_DOUBLEBUFFER;
	pfd.iPixelType = PFD_TYPE_RGBA;
	pfd.cColorBits = 32;
	pfd.cRedBits = 8;
	pfd.cGreenBits = 8;
	pfd.cBlueBits = 8;
	pfd.cAlphaBits = 8;
	pfd.cDepthBits = 32;
	ghdc = GetDC(ghwnd);

	iPixelFormatIndex = ChoosePixelFormat(ghdc, &pfd);
	if (iPixelFormatIndex == 0)
	{
		ReleaseDC(ghwnd, ghdc);
		ghdc = NULL;
	}

	if (SetPixelFormat(ghdc, iPixelFormatIndex, &pfd) == FALSE)
	{
		ReleaseDC(ghwnd, ghdc);
		ghdc = NULL;
	}

	ghrc = wglCreateContext(ghdc);
	if (ghrc == NULL)
	{
		ReleaseDC(ghwnd, ghdc);
		ghdc = NULL;
	}

	if (wglMakeCurrent(ghdc, ghrc) == FALSE)
	{
		wglDeleteContext(ghrc);
		ghrc = NULL;
		ReleaseDC(ghwnd, ghdc);
		ghdc = NULL;
	}

	glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
	glClearDepth(1.0f);
	glEnable(GL_DEPTH_TEST);
	glDepthFunc(GL_LEQUAL);
	glShadeModel(GL_SMOOTH);
	glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST);
	Resize(WIN_WIDTH, WIN_HEIGHT);

	//Initialize  identity matrix
	initializeIdentityeMatrix();

	//initialize translation matrix
	initializeTranslationMatrix();

	//initialize scale matrix
	initializeScaleMatrix();
}

void initializeIdentityeMatrix(void) {
	gfIdentityMatrix[0] = 1.0f;
	gfIdentityMatrix[1] = 0.0f;
	gfIdentityMatrix[2] = 0.0f;
	gfIdentityMatrix[3] = 0.0f;
	gfIdentityMatrix[4] = 0.0f;
	gfIdentityMatrix[5] = 1.0f;
	gfIdentityMatrix[6] = 0.0f;
	gfIdentityMatrix[7] = 0.0f;
	gfIdentityMatrix[8] = 0.0f;
	gfIdentityMatrix[9] = 0.0f;
	gfIdentityMatrix[10] = 1.0f;
	gfIdentityMatrix[11] = 0.0f;
	gfIdentityMatrix[12] = 0.0f;
	gfIdentityMatrix[13] = 0.0f;
	gfIdentityMatrix[14] = 0.0f;
	gfIdentityMatrix[15] = 1.0f;
}

void initializeTranslationMatrix(void) {
	gfTranslationMatrix[0] = 1.0f;
	gfTranslationMatrix[1] = 0.0f;
	gfTranslationMatrix[2] = 0.0f;
	gfTranslationMatrix[3] = 0.0f;
	gfTranslationMatrix[4] = 0.0f;
	gfTranslationMatrix[5] = 1.0f;
	gfTranslationMatrix[6] = 0.0f;
	gfTranslationMatrix[7] = 0.0f;
	gfTranslationMatrix[8] = 0.0f;
	gfTranslationMatrix[9] = 0.0f;
	gfTranslationMatrix[10] = 1.0f;
	gfTranslationMatrix[11] = 0.0f;
	gfTranslationMatrix[12] = 0.0f; //tx
	gfTranslationMatrix[13] = 0.0f; //ty
	gfTranslationMatrix[14] = -4.0f; //tz
	gfTranslationMatrix[15] = 1.0f;
}

void initializeScaleMatrix(void) {
	gfScaleMatrix[0] = 0.75f; //sx
	gfScaleMatrix[1] = 0.0f;
	gfScaleMatrix[2] = 0.0f;
	gfScaleMatrix[3] = 0.0f;
	gfScaleMatrix[4] = 0.0f;
	gfScaleMatrix[5] = 0.75f; //sy
	gfScaleMatrix[6] = 0.0f;
	gfScaleMatrix[7] = 0.0f;
	gfScaleMatrix[8] = 0.0f;
	gfScaleMatrix[9] = 0.0f;
	gfScaleMatrix[10] = 0.75f; //sy
	gfScaleMatrix[11] = 0.0f;
	gfScaleMatrix[12] = 0.0f;
	gfScaleMatrix[13] = 0.0f;
	gfScaleMatrix[14] = 0.0f;
	gfScaleMatrix[15] = 1.0f;
}

void initializeXRotationMatrix(void) {
	gfXRotationMatrix[0] = 1.0f;
	gfXRotationMatrix[1] = 0.0f;
	gfXRotationMatrix[2] = 0.0f;
	gfXRotationMatrix[3] = 0.0f;
	gfXRotationMatrix[4] = 0.0f;
	gfXRotationMatrix[5] = cos(gfCubeRadianAngle);
	gfXRotationMatrix[6] = sin(gfCubeRadianAngle);
	gfXRotationMatrix[7] = 0.0f;
	gfXRotationMatrix[8] = 0.0f;
	gfXRotationMatrix[9] = -sin(gfCubeRadianAngle);
	gfXRotationMatrix[10] = cos(gfCubeRadianAngle);
	gfXRotationMatrix[11] = 0.0f;
	gfXRotationMatrix[12] = 0.0f;
	gfXRotationMatrix[13] = 0.0f;
	gfXRotationMatrix[14] = 0.0f;
	gfXRotationMatrix[15] = 1.0f;
}

void initializeYRotationMatrix(void) {
	gfYRotationMatrix[0] = cos(gfCubeRadianAngle);
	gfYRotationMatrix[1] = 0.0f;
	gfYRotationMatrix[2] = -sin(gfCubeRadianAngle);
	gfYRotationMatrix[3] = 0.0f;
	gfYRotationMatrix[4] = 0.0f;
	gfYRotationMatrix[5] = 1.0f;
	gfYRotationMatrix[6] = 0.0f;
	gfYRotationMatrix[7] = 0.0f;
	gfYRotationMatrix[8] = sin(gfCubeRadianAngle);
	gfYRotationMatrix[9] = 0.0f;
	gfYRotationMatrix[10] = cos(gfCubeRadianAngle);
	gfYRotationMatrix[11] = 0.0f;
	gfYRotationMatrix[12] = 0.0f;
	gfYRotationMatrix[13] = 0.0f;
	gfYRotationMatrix[14] = 0.0f;
	gfYRotationMatrix[15] = 1.0f;
}

void initializeZRotationMatrix(void) {
	gfZRotationMatrix[0] = cos(gfCubeRadianAngle);
	gfZRotationMatrix[1] = sin(gfCubeRadianAngle);
	gfZRotationMatrix[2] = 0.0f;
	gfZRotationMatrix[3] = 0.0f;
	gfZRotationMatrix[4] = -sin(gfCubeRadianAngle);
	gfZRotationMatrix[5] = cos(gfCubeRadianAngle);
	gfZRotationMatrix[6] = 0.0f;
	gfZRotationMatrix[7] = 0.0f;
	gfZRotationMatrix[8] = 0.0f;
	gfZRotationMatrix[9] = 0.0f;
	gfZRotationMatrix[10] = 1.0f;
	gfZRotationMatrix[11] = 0.0f;
	gfZRotationMatrix[12] = 0.0f;
	gfZRotationMatrix[13] = 0.0f;
	gfZRotationMatrix[14] = 0.0f;
	gfZRotationMatrix[15] = 1.0f;
}

void Display(void)
{
	// Function declaration
	void drawCube();
	
	// Code
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glMatrixMode(GL_MODELVIEW);
	//glLoadIdentity();
	glLoadMatrixf(gfIdentityMatrix);
	drawCube();
	SwapBuffers(ghdc);
}

void Resize(int width, int height)
{
	// Code
	if (height == 0)
		height = 1;
	glViewport(0, 0, (GLsizei)width, (GLsizei)height);
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	gluPerspective(45.0f, (GLfloat)width /  (GLfloat)height, 0.1f, 100.0f);
}

void Uninitialize(void)
{
	// Uninitialization code

	if (gbFullScreen == true)
	{
		dwStyle = GetWindowLong(ghwnd, GWL_STYLE);
		SetWindowLong(ghwnd, GWL_STYLE, dwStyle | WS_OVERLAPPEDWINDOW);
		SetWindowPlacement(ghwnd, &wpPrev);
		SetWindowPos(ghwnd, HWND_TOP, 0, 0, 0, 0, SWP_NOMOVE | SWP_NOSIZE | SWP_NOZORDER | SWP_NOOWNERZORDER | SWP_FRAMECHANGED);
		ShowCursor(TRUE);
	}

	wglMakeCurrent(NULL, NULL);

	wglDeleteContext(ghrc);
	ghrc = NULL;

	ReleaseDC(ghwnd, ghdc);
	ghdc = NULL;

	DestroyWindow(ghwnd);
	ghwnd = NULL;
}

void drawCube()
{	
	void initializeXRotationMatrix(void);
	void initializeYRotationMatrix(void);
	void initializeZRotationMatrix(void);
	//glTranslatef(0.0f, 0.0f, -4.0f);
	glMultMatrixf(gfTranslationMatrix);
	glMultMatrixf(gfScaleMatrix);
	gfCubeRadianAngle = gfCubeDegreeAngle * (PI / 180.0f);

	//initialize X axis rotation matrix
	initializeXRotationMatrix();
	glMultMatrixf(gfXRotationMatrix);

	//initialize Y axis rotation matrix
	initializeYRotationMatrix();
	glMultMatrixf(gfYRotationMatrix);

	//initialize Z axis rotation matrix
	initializeZRotationMatrix();
	glMultMatrixf(gfZRotationMatrix);
	//glRotatef(gfCubeDegreeAngle, 1.0f, 1.0f, 1.0f);

	glBegin(GL_QUADS);

	//Front face
	glColor3f(1.0f, 0.0f, 0.0f);
	glVertex3f(1.0f, 1.0f, 1.0f); 
	glVertex3f(-1.0f, 1.0f, 1.0f);
	glVertex3f(-1.0f, -1.0f, 1.0f);
	glVertex3f(1.0f, -1.0f, 1.0f);

	//left face
	glColor3f(0.0f, 1.0f, 0.0f);
	glVertex3f(1.0f, 1.0f, -1.0f);
	glVertex3f(1.0f, 1.0f, 1.0f);
	glVertex3f(1.0f, -1.0f, 1.0f);
	glVertex3f(1.0f, -1.0f, -1.0f);

	//back face
	glColor3f(0.0f, 0.0f, 1.0f);
	glVertex3f(-1.0f, 1.0f, -1.0f); 
	glVertex3f(1.0f, 1.0f, -1.0f);
	glVertex3f(1.0f, -1.0f, -1.0f);
	glVertex3f(-1.0f, -1.0f, -1.0f);

	//right face
	glColor3f(1.0f, 1.0f, 0.0f);
	glVertex3f(-1.0f, 1.0f, 1.0f); 
	glVertex3f(-1.0f, 1.0f, -1.0f);
	glVertex3f(-1.0f, -1.0f, -1.0f);
	glVertex3f(-1.0f, -1.0f, 1.0f);

	//top face
	glColor3f(0.0f, 1.0f, 1.0f);
	glVertex3f(-1.0f, 1.0f, 1.0f);
	glVertex3f(1.0f, 1.0f, 1.0f);
	glVertex3f(1.0f, 1.0f, -1.0f);
	glVertex3f(-1.0f, 1.0f, -1.0f);

	//bottom face
	glColor3f(1.0f, 0.0f, 1.0f);
	glVertex3f(1.0f, -1.0f, 1.0f);
	glVertex3f(-1.0f, -1.0f, 1.0f);
	glVertex3f(-1.0f, -1.0f, -1.0f);
	glVertex3f(1.0f, -1.0f, -1.0f);
	glEnd();
	
}

void Update() {
	gfCubeDegreeAngle = (gfCubeDegreeAngle + 0.1f);
	if (gfCubeDegreeAngle >= 360.0f)
		gfCubeDegreeAngle = 0.0f;
}