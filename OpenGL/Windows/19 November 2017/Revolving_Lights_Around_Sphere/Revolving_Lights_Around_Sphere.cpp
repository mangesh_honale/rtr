#include <windows.h>
#include <gl/GL.h>
#include <gl/GLU.h>

#define WIN_WIDTH 800
#define WIN_HEIGHT 600

#pragma comment(lib,"opengl32.lib")
#pragma comment(lib,"glu32.lib")


// Global function declaration
LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);

// Global variable declaration
HWND ghwnd = NULL;
HDC ghdc = NULL;
HGLRC ghrc = NULL;



DWORD dwStyle;
WINDOWPLACEMENT wpPrev = { sizeof(WINDOWPLACEMENT) };

int giScreenWidth;
int giScreenHeight;
GLUquadric *sphere = NULL;
bool gbActiveWindow = false;
bool gbEscapeKeyIsPressed = false;
bool gbFullScreen = false;
GLfloat gfRedLightAngle = 0.0f, gfGreenLightAngle = 0.0f, gfBlueLightAngle = 0.0f;


//red light arrays
GLfloat red_light_ambient[] = {0.0f, 0.0f, 0.0f, 0.0f};
GLfloat red_light_diffuse[] = {1.0f, 0.0f, 0.0f, 0.0f};
GLfloat red_light_specular[] = {1.0f, 0.0f, 0.0f, 0.0f};
GLfloat red_light_position[] = {0.0f, 0.0f, 0.0f, 0.0f};

//green light arrays
GLfloat green_light_ambient[] = { 0.0f, 0.0f, 0.0f, 0.0f };
GLfloat green_light_diffuse[] = {0.0f, 1.0f, 0.0f, 0.0f};
GLfloat green_light_specular[] = { 0.0f, 1.0f, 0.0f, 0.0f };
GLfloat green_light_position[] = { 0.0f, 0.0f, 0.0f, 0.0f };

//blue light arrays
GLfloat blue_light_ambient[] = {0.0f, 0.0f, 0.0f, 0.0f};
GLfloat blue_light_diffuse[] = {0.0f, 0.0f, 1.0f, 0.0f};
GLfloat blue_light_specular[] = {0.0f, 0.0f, 1.0f, 0.0f};
GLfloat blue_light_position[] = {0.0f, 0.0f, 0.0f, 0.0f};

//material arrays
GLfloat material_ambient[] = {0.0f, 0.0f, 0.0f, 0.0f};
GLfloat material_diffuse[] = {1.0f, 1.0f, 1.0f, 1.0f};
GLfloat material_specular[] = {1.0f, 1.0f, 1.0f, 1.0f};
GLfloat material_shininess = 50.0f;

BOOLEAN isLightOn = FALSE;
// main()
int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpszCmdLine, int iCmdShow) {
	// Function prototype
	void Initialize(void);
	void Uninitialize(void);
	void Display(void);
	void Update(void);

	// Variable declaration
	WNDCLASSEX wndClass;
	HWND hwnd;
	MSG msg;
	TCHAR szClassName[] = TEXT("RTROGL");
	bool bDone = false;
	giScreenWidth = GetSystemMetrics(SM_CXSCREEN);
	giScreenHeight = GetSystemMetrics(SM_CYSCREEN);


	// Code
	// Initializing members of struct WNDCLASSEX
	wndClass.cbSize = sizeof(WNDCLASSEX);
	wndClass.style = CS_HREDRAW | CS_VREDRAW | CS_OWNDC;
	wndClass.cbClsExtra = 0;
	wndClass.cbWndExtra = 0;
	wndClass.hInstance = hInstance;
	wndClass.hbrBackground = (HBRUSH)GetStockObject(BLACK_BRUSH);
	wndClass.hIcon = LoadIcon(NULL, IDI_APPLICATION);
	wndClass.hCursor = LoadCursor(NULL, IDC_ARROW);
	wndClass.hIconSm = LoadIcon(NULL, IDI_APPLICATION);
	wndClass.lpfnWndProc = WndProc;
	wndClass.lpszClassName = szClassName;
	wndClass.lpszMenuName = NULL;

	// Registering class
	RegisterClassEx(&wndClass);

	// Create window
	hwnd = CreateWindowEx(WS_EX_APPWINDOW,
		szClassName,
		TEXT("OpenGL Fixed Function Pipeline Using Native Windowing: Double Buffer Window"),
		WS_OVERLAPPEDWINDOW | WS_CLIPCHILDREN | WS_CLIPSIBLINGS | WS_VISIBLE,
		(giScreenWidth / 2) - 400,
		(giScreenHeight / 2) - 300,
		WIN_WIDTH,
		WIN_HEIGHT,
		NULL,
		NULL,
		hInstance,
		NULL);
	ghwnd = hwnd;

	// Initialize
	Initialize();

	ShowWindow(hwnd, SW_SHOW);
	SetForegroundWindow(hwnd);
	SetFocus(hwnd);

	// Message loop
	while (bDone == false)
	{
		if (PeekMessage(&msg, NULL, 0, 0, PM_REMOVE))
		{
			if (msg.message == WM_QUIT)
				bDone = true;
			else
			{
				TranslateMessage(&msg);
				DispatchMessage(&msg);
			}
		}
		else
		{
			if (gbActiveWindow == true)
			{
				if (gbEscapeKeyIsPressed == true)
					bDone = true;
				Update();
				Display();
			}
		}
	}
	Uninitialize();
	return((int)msg.wParam);
}

// WndProc()
LRESULT CALLBACK WndProc(HWND hwnd, UINT iMsg, WPARAM wParam, LPARAM lParam)
{
	// Function prototype
	void Resize(int, int);
	void ToggleFullscreen(void);
	void Uninitialize(void);

	// Code
	switch (iMsg)
	{
	case WM_ACTIVATE:
		if (HIWORD(wParam) == 0)
			gbActiveWindow = true;
		else
			gbActiveWindow = false;
		break;
	case WM_SIZE:
		Resize(LOWORD(lParam), HIWORD(lParam));
		break;
	case WM_KEYDOWN:
		switch (wParam)
		{
		
		case 0x4C:
			if (isLightOn) {
				glDisable(GL_LIGHTING);
				isLightOn = FALSE;
			}
			else {
				glEnable(GL_LIGHTING);
				isLightOn = TRUE;
			}
			break;
		case VK_ESCAPE:
			gbEscapeKeyIsPressed = true;
			break;
		case 0x46:
			if (gbFullScreen == false)
			{
				ToggleFullscreen();
				gbFullScreen = true;
			}
			else
			{
				ToggleFullscreen();
				gbFullScreen = false;
			}
			break;
		}
		break;
	case WM_DESTROY:
		PostQuitMessage(0);
		break;
	default:
		break;
	}
	return(DefWindowProc(hwnd, iMsg, wParam, lParam));
}

void ToggleFullscreen(void)
{
	// Variable declaration
	MONITORINFO  mi;
	int x, y;

	// Code
	if (gbFullScreen == false)
	{
		dwStyle = GetWindowLong(ghwnd, GWL_STYLE);
		if (dwStyle & WS_OVERLAPPEDWINDOW)
		{
			mi = { sizeof(MONITORINFO) };
			if (GetWindowPlacement(ghwnd, &wpPrev) && GetMonitorInfo(MonitorFromWindow(ghwnd, MONITORINFOF_PRIMARY), &mi))
			{
				SetWindowLong(ghwnd, GWL_STYLE, dwStyle & ~WS_OVERLAPPEDWINDOW);
				SetWindowPos(ghwnd, HWND_TOP, mi.rcMonitor.left, mi.rcMonitor.top, mi.rcMonitor.right - mi.rcMonitor.left, mi.rcMonitor.bottom - mi.rcMonitor.top, SWP_NOZORDER | SWP_FRAMECHANGED);
			}
		}
		ShowCursor(FALSE);
	}
	else
	{
		// Code
		x = (giScreenWidth / 2) - ((wpPrev.rcNormalPosition.right - wpPrev.rcNormalPosition.left) / 2);
		y = (giScreenHeight / 2) - ((wpPrev.rcNormalPosition.bottom - wpPrev.rcNormalPosition.top) / 2);
		SetWindowLong(ghwnd, GWL_STYLE, dwStyle | WS_OVERLAPPEDWINDOW);
		SetWindowPlacement(ghwnd, &wpPrev);
		SetWindowPos(ghwnd, HWND_TOP, x, y, 0, 0, SWP_NOSIZE | SWP_NOOWNERZORDER | SWP_NOZORDER | SWP_FRAMECHANGED);
		ShowCursor(TRUE);
	}
}

void Initialize(void)
{
	// Function prototypes
	void Resize(int, int);
	// Variable Declarations
	PIXELFORMATDESCRIPTOR pfd;
	int iPixelFormatIndex;

	// Code
	ZeroMemory(&pfd, sizeof(PIXELFORMATDESCRIPTOR));

	// Initialization of structure 'PIXELFORMATDESCRIPTOR'
	pfd.nSize = sizeof(PIXELFORMATDESCRIPTOR);
	pfd.nVersion = 1;
	pfd.dwFlags = PFD_DRAW_TO_WINDOW | PFD_SUPPORT_OPENGL | PFD_DOUBLEBUFFER;
	pfd.iPixelType = PFD_TYPE_RGBA;
	pfd.cColorBits = 32;
	pfd.cRedBits = 8;
	pfd.cGreenBits = 8;
	pfd.cBlueBits = 8;
	pfd.cAlphaBits = 8;
	pfd.cDepthBits = 32;
	ghdc = GetDC(ghwnd);

	iPixelFormatIndex = ChoosePixelFormat(ghdc, &pfd);
	if (iPixelFormatIndex == 0)
	{
		ReleaseDC(ghwnd, ghdc);
		ghdc = NULL;
	}

	if (SetPixelFormat(ghdc, iPixelFormatIndex, &pfd) == FALSE)
	{
		ReleaseDC(ghwnd, ghdc);
		ghdc = NULL;
	}

	ghrc = wglCreateContext(ghdc);
	if (ghrc == NULL)
	{
		ReleaseDC(ghwnd, ghdc);
		ghdc = NULL;
	}

	if (wglMakeCurrent(ghdc, ghrc) == FALSE)
	{
		wglDeleteContext(ghrc);
		ghrc = NULL;
		ReleaseDC(ghwnd, ghdc);
		ghdc = NULL;
	}

	glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
	glClearDepth(1.0f);
	glEnable(GL_DEPTH_TEST);
	glDepthFunc(GL_LEQUAL);
	glShadeModel(GL_SMOOTH);
	glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST);

	//red light config
	glLightfv(GL_LIGHT0, GL_AMBIENT, red_light_ambient);
	glLightfv(GL_LIGHT0, GL_DIFFUSE, red_light_diffuse);
	glLightfv(GL_LIGHT0, GL_SPECULAR, red_light_specular);
	glEnable(GL_LIGHT0);

	//green light config
	glLightfv(GL_LIGHT1, GL_AMBIENT, green_light_ambient);
	glLightfv(GL_LIGHT1, GL_DIFFUSE, green_light_diffuse);
	glLightfv(GL_LIGHT1, GL_SPECULAR, green_light_specular);
	glEnable(GL_LIGHT1);

	//blue light config
	glLightfv(GL_LIGHT2, GL_AMBIENT, blue_light_ambient);
	glLightfv(GL_LIGHT2, GL_DIFFUSE, blue_light_diffuse);
	glLightfv(GL_LIGHT2, GL_SPECULAR, blue_light_specular);
	glEnable(GL_LIGHT2);

	//material config
	glMaterialfv(GL_FRONT, GL_AMBIENT, material_ambient);
	glMaterialfv(GL_FRONT, GL_DIFFUSE, material_diffuse);
	glMaterialfv(GL_FRONT, GL_SPECULAR, material_specular);
	glMaterialf(GL_FRONT, GL_SHININESS, material_shininess);
	Resize(WIN_WIDTH, WIN_HEIGHT);
}

void Display(void)
{
	// Function declaration
	void drawSphere();
	// Code
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	drawSphere();
	SwapBuffers(ghdc);
}

void drawSphere() {
	glPushMatrix();
	gluLookAt(0.0f, 0.0f, 0.1f, 0.0f, 0.0f, 0.0f, 0, 1, 0);
	glPushMatrix();
	glRotatef(gfRedLightAngle, 1.0f, 0.0f, 0.0f);
	red_light_position[1] = gfRedLightAngle;
	glLightfv(GL_LIGHT0, GL_POSITION, red_light_position);
	glPopMatrix();

	glPushMatrix();
	glRotatef(gfGreenLightAngle, 0.0f, 1.0f, 0.0f);
	green_light_position[0] = gfGreenLightAngle;
	glLightfv(GL_LIGHT1, GL_POSITION, green_light_position);
	glPopMatrix();

	glPushMatrix();
	glRotatef(gfBlueLightAngle, 0.0f, 0.0f, 1.0f);
	blue_light_position[0] = gfBlueLightAngle;
	glLightfv(GL_LIGHT2, GL_POSITION, blue_light_position);
	glPopMatrix();

	glPushMatrix();
	glTranslatef(0.0f, 0.0f, -3.0f);
	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
	sphere = gluNewQuadric();
	gluSphere(sphere, 0.75, 30, 30);
	glPopMatrix();

	glPopMatrix();
	

}

void Resize(int width, int height)
{
	// Code
	if (height == 0)
		height = 1;
	glViewport(0, 0, (GLsizei)width, (GLsizei)height);
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	gluPerspective(45.0f, (GLfloat)width /  (GLfloat)height, 0.1f, 100.0f);
}

void Uninitialize(void)
{
	// Uninitialization code
	if (sphere) {
		gluDeleteQuadric(sphere);
		sphere = NULL;
	}
		
	if (gbFullScreen == true)
	{
		dwStyle = GetWindowLong(ghwnd, GWL_STYLE);
		SetWindowLong(ghwnd, GWL_STYLE, dwStyle | WS_OVERLAPPEDWINDOW);
		SetWindowPlacement(ghwnd, &wpPrev);
		SetWindowPos(ghwnd, HWND_TOP, 0, 0, 0, 0, SWP_NOMOVE | SWP_NOSIZE | SWP_NOZORDER | SWP_NOOWNERZORDER | SWP_FRAMECHANGED);
		ShowCursor(TRUE);
	}

	wglMakeCurrent(NULL, NULL);

	wglDeleteContext(ghrc);
	ghrc = NULL;

	ReleaseDC(ghwnd, ghdc);
	ghdc = NULL;

	DestroyWindow(ghwnd);
	ghwnd = NULL;
}




void Update() {
	gfRedLightAngle = (gfRedLightAngle + 1.0f);
	if (gfRedLightAngle >= 360.0f)
		gfRedLightAngle = 0.0f;

	gfGreenLightAngle = (gfGreenLightAngle + 0.1f);
	if (gfGreenLightAngle >= 360.0f)
		gfGreenLightAngle = 0.0f;

	gfBlueLightAngle = (gfBlueLightAngle + 0.1f);
	if (gfBlueLightAngle >= 360.0f)
		gfBlueLightAngle = 0.0f;
}