#include <GL/freeglut.h>

//global variable declaration
bool bFullscreen = false; //variable to toggle for fullscreen

int main(int argc, char** argv)
{
	//function prototypes
	void display(void);
	void resize(int, int);
	void keyboard(unsigned char, int, int);
	void mouse(int, int, int, int);
	void initialize(void);
	void uninitialize(void);

	//code
	glutInit(&argc, argv);

	glutInitDisplayMode(GLUT_SINGLE | GLUT_RGB);

	glutInitWindowSize(800, 600); //to declare initial window size
	int giX = GetSystemMetrics(SM_CXFULLSCREEN) / 2 - 400;
	int giY = GetSystemMetrics(SM_CYFULLSCREEN) / 2 - 300;
	glutInitWindowPosition(giX, giY); //to declare initial window position
	
	glutCreateWindow("OpenGL First Window : Hello World !!!"); //open the window with "OpenGL First Window : Hello World" in the title bar
	
	initialize();
	
	glutDisplayFunc(display);
	glutReshapeFunc(resize);
	glutKeyboardFunc(keyboard);
	glutMouseFunc(mouse);
	glutCloseFunc(uninitialize);
	
	glutMainLoop();

	//	return(0); 
}

void display(void)
{

	//code

	//to clear all pixels
	glClear(GL_COLOR_BUFFER_BIT);
	
	//to process buffered OpenGL Routines
	glFlush();
}

void initialize(void)
{
	//code
	//to select clearing (background) clear
	glClearColor(0.0f, 0.0f, 1.0f, 0.0f); //blue 
}

void keyboard(unsigned char key, int x, int y)
{
	TCHAR str[255];
	//code
	switch (key)
	{
	case 27: // Escape
		glutLeaveMainLoop();
		break;
	case 'a':
	case 'A':
		wsprintf(str, TEXT("Key pressed: A"));
		MessageBox(NULL, str, TEXT("Key Press"), MB_OK);
		break;
	case 'b':
	case 'B':
		glClearColor(0.0f, 0.0f, 1.0f, 0.0f); //blue 
		glutPostRedisplay();
		break;
	case 'F':
	case 'f':
		if (bFullscreen == false)
		{
			glutFullScreen();
			bFullscreen = true;
		}
		else
		{
			glutLeaveFullScreen();
			bFullscreen = false;
		}
	
		break;
	case 'c':
	case 'C':
		glClearColor(0.0f, 1.0f, 1.0f, 0.0f); //blue 
		glutPostRedisplay();
		break;
	case 'r':
	case 'R':
		glClearColor(1.0f, 1.0f, 1.0f, 0.0f); //blue 
		glutPostRedisplay();
		break;
	case 'g':
	case 'G':
		glClearColor(0.0f, 1.0f, 0.0f, 0.0f);
		glutPostRedisplay();
		break;
	case 'k':
	case 'K':
		glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
		glutPostRedisplay();
		break;
	case 'm':
	case 'M':
		glClearColor(1.0f, 0.0f, 1.0f, 0.0f);
		glutPostRedisplay();
		break;
	case 'y':
	case 'Y':
		glClearColor(1.0f, 1.0f, 0.0f, 0.0f);
		glutPostRedisplay();
		break;
	default:
		
		wsprintf(str, TEXT("Invalid key pressed!"));
		MessageBox(NULL, str, TEXT("Key Press"), MB_OK);
		break;
	}
}

void mouse(int button, int state, int x, int y)
{
	//code
	switch (button)
	{
	case GLUT_LEFT_BUTTON:
		TCHAR str[255];
		wsprintf(str, TEXT("Left mouse button clicked!"));
		MessageBox(NULL, str, TEXT("Mouse clicks"), MB_OK);
		
		break;
	default:
		break;
	}
}

void resize(int width, int height)
{
	// code
}

void uninitialize(void)
{
	// code
}

