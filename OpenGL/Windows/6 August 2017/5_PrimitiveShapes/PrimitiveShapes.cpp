#include <windows.h>
#include <gl/GL.h>
#include <gl/GLU.h>

#define WIN_WIDTH 800
#define WIN_HEIGHT 600

#pragma comment(lib,"opengl32.lib")
#pragma comment(lib,"glu32.lib")


// Global function declaration
LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);

// Global variable declaration
HWND ghwnd = NULL;
HDC ghdc = NULL;
HGLRC ghrc = NULL;



DWORD dwStyle;
WINDOWPLACEMENT wpPrev = { sizeof(WINDOWPLACEMENT) };

int giScreenWidth;
int giScreenHeight;

bool gbActiveWindow = false;
bool gbEscapeKeyIsPressed = false;
bool gbFullScreen = false;
GLfloat gfCubeAngle = 0.0f;
// main()
int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpszCmdLine, int iCmdShow) {
	// Function prototype
	void Initialize(void);
	void Uninitialize(void);
	void Display(void);

	// Variable declaration
	WNDCLASSEX wndClass;
	HWND hwnd;
	MSG msg;
	TCHAR szClassName[] = TEXT("RTROGL");
	bool bDone = false;
	giScreenWidth = GetSystemMetrics(SM_CXSCREEN);
	giScreenHeight = GetSystemMetrics(SM_CYSCREEN);


	// Code
	// Initializing members of struct WNDCLASSEX
	wndClass.cbSize = sizeof(WNDCLASSEX);
	wndClass.style = CS_HREDRAW | CS_VREDRAW | CS_OWNDC;
	wndClass.cbClsExtra = 0;
	wndClass.cbWndExtra = 0;
	wndClass.hInstance = hInstance;
	wndClass.hbrBackground = (HBRUSH)GetStockObject(BLACK_BRUSH);
	wndClass.hIcon = LoadIcon(NULL, IDI_APPLICATION);
	wndClass.hCursor = LoadCursor(NULL, IDC_ARROW);
	wndClass.hIconSm = LoadIcon(NULL, IDI_APPLICATION);
	wndClass.lpfnWndProc = WndProc;
	wndClass.lpszClassName = szClassName;
	wndClass.lpszMenuName = NULL;

	// Registering class
	RegisterClassEx(&wndClass);

	// Create window
	hwnd = CreateWindowEx(WS_EX_APPWINDOW,
		szClassName,
		TEXT("OpenGL Fixed Function Pipeline Using Native Windowing: Double Buffer Window"),
		WS_OVERLAPPEDWINDOW | WS_CLIPCHILDREN | WS_CLIPSIBLINGS | WS_VISIBLE,
		(giScreenWidth / 2) - 400,
		(giScreenHeight / 2) - 300,
		WIN_WIDTH,
		WIN_HEIGHT,
		NULL,
		NULL,
		hInstance,
		NULL);
	ghwnd = hwnd;

	// Initialize
	Initialize();

	ShowWindow(hwnd, SW_SHOW);
	SetForegroundWindow(hwnd);
	SetFocus(hwnd);

	// Message loop
	while (bDone == false)
	{
		if (PeekMessage(&msg, NULL, 0, 0, PM_REMOVE))
		{
			if (msg.message == WM_QUIT)
				bDone = true;
			else
			{
				TranslateMessage(&msg);
				DispatchMessage(&msg);
			}
		}
		else
		{
			if (gbActiveWindow == true)
			{
				if (gbEscapeKeyIsPressed == true)
					bDone = true;
				Display();
			}
		}
	}
	Uninitialize();
	return((int)msg.wParam);
}

// WndProc()
LRESULT CALLBACK WndProc(HWND hwnd, UINT iMsg, WPARAM wParam, LPARAM lParam)
{
	// Function prototype
	void Resize(int, int);
	void ToggleFullscreen(void);
	void Uninitialize(void);

	// Code
	switch (iMsg)
	{
	case WM_ACTIVATE:
		if (HIWORD(wParam) == 0)
			gbActiveWindow = true;
		else
			gbActiveWindow = false;
		break;
	case WM_SIZE:
		Resize(LOWORD(lParam), HIWORD(lParam));
		break;
	case WM_KEYDOWN:
		switch (wParam)
		{
		case VK_ESCAPE:
			gbEscapeKeyIsPressed = true;
			break;
		case 0x46:
			if (gbFullScreen == false)
			{
				ToggleFullscreen();
				gbFullScreen = true;
			}
			else
			{
				ToggleFullscreen();
				gbFullScreen = false;
			}
			break;
		}
		break;
	case WM_DESTROY:
		PostQuitMessage(0);
		break;
	default:
		break;
	}
	return(DefWindowProc(hwnd, iMsg, wParam, lParam));
}

void ToggleFullscreen(void)
{
	// Variable declaration
	MONITORINFO  mi;
	int x, y;

	// Code
	if (gbFullScreen == false)
	{
		dwStyle = GetWindowLong(ghwnd, GWL_STYLE);
		if (dwStyle & WS_OVERLAPPEDWINDOW)
		{
			mi = { sizeof(MONITORINFO) };
			if (GetWindowPlacement(ghwnd, &wpPrev) && GetMonitorInfo(MonitorFromWindow(ghwnd, MONITORINFOF_PRIMARY), &mi))
			{
				SetWindowLong(ghwnd, GWL_STYLE, dwStyle & ~WS_OVERLAPPEDWINDOW);
				SetWindowPos(ghwnd, HWND_TOP, mi.rcMonitor.left, mi.rcMonitor.top, mi.rcMonitor.right - mi.rcMonitor.left, mi.rcMonitor.bottom - mi.rcMonitor.top, SWP_NOZORDER | SWP_FRAMECHANGED);
			}
		}
		ShowCursor(FALSE);
	}
	else
	{
		// Code
		x = (giScreenWidth / 2) - ((wpPrev.rcNormalPosition.right - wpPrev.rcNormalPosition.left) / 2);
		y = (giScreenHeight / 2) - ((wpPrev.rcNormalPosition.bottom - wpPrev.rcNormalPosition.top) / 2);
		SetWindowLong(ghwnd, GWL_STYLE, dwStyle | WS_OVERLAPPEDWINDOW);
		SetWindowPlacement(ghwnd, &wpPrev);
		SetWindowPos(ghwnd, HWND_TOP, x, y, 0, 0, SWP_NOSIZE | SWP_NOOWNERZORDER | SWP_NOZORDER | SWP_FRAMECHANGED);
		ShowCursor(TRUE);
	}
}

void Initialize(void)
{
	// Function prototypes
	void Resize(int, int);
	// Variable Declarations
	PIXELFORMATDESCRIPTOR pfd;
	int iPixelFormatIndex;

	// Code
	ZeroMemory(&pfd, sizeof(PIXELFORMATDESCRIPTOR));

	// Initialization of structure 'PIXELFORMATDESCRIPTOR'
	pfd.nSize = sizeof(PIXELFORMATDESCRIPTOR);
	pfd.nVersion = 1;
	pfd.dwFlags = PFD_DRAW_TO_WINDOW | PFD_SUPPORT_OPENGL | PFD_DOUBLEBUFFER;
	pfd.iPixelType = PFD_TYPE_RGBA;
	pfd.cColorBits = 32;
	pfd.cRedBits = 8;
	pfd.cGreenBits = 8;
	pfd.cBlueBits = 8;
	pfd.cAlphaBits = 8;
	pfd.cDepthBits = 32;
	ghdc = GetDC(ghwnd);

	iPixelFormatIndex = ChoosePixelFormat(ghdc, &pfd);
	if (iPixelFormatIndex == 0)
	{
		ReleaseDC(ghwnd, ghdc);
		ghdc = NULL;
	}

	if (SetPixelFormat(ghdc, iPixelFormatIndex, &pfd) == FALSE)
	{
		ReleaseDC(ghwnd, ghdc);
		ghdc = NULL;
	}

	ghrc = wglCreateContext(ghdc);
	if (ghrc == NULL)
	{
		ReleaseDC(ghwnd, ghdc);
		ghdc = NULL;
	}

	if (wglMakeCurrent(ghdc, ghrc) == FALSE)
	{
		wglDeleteContext(ghrc);
		ghrc = NULL;
		ReleaseDC(ghwnd, ghdc);
		ghdc = NULL;
	}

	glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
	glClearDepth(1.0f);
	glEnable(GL_DEPTH_TEST);
	glDepthFunc(GL_LEQUAL);
	glShadeModel(GL_SMOOTH);
	glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST);
	Resize(WIN_WIDTH, WIN_HEIGHT);
}

void Display(void)
{
	// Function declaration
	void drawCube();
	void drawRectPoints();
	void drawRect1();
	void drawRect2();
	void drawRect3();
	void drawRect4();
	void drawRect5();
	// Code
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(0.0f, 0.0f, -4.0f);
	glPushMatrix();

	//point grid
	drawRectPoints();
	drawRect1();
	drawRect2();
	drawRect3();
	glPopMatrix();
	glPushMatrix();
	drawRect4();
	glPopMatrix();
	drawRect5();
	SwapBuffers(ghdc);
}

void Resize(int width, int height)
{
	// Code
	if (height == 0)
		height = 1;
	glViewport(0, 0, (GLsizei)width, (GLsizei)height);
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	gluPerspective(45.0f, (GLfloat)width /  (GLfloat)height, 0.1f, 100.0f);
}

void Uninitialize(void)
{
	// Uninitialization code

	if (gbFullScreen == true)
	{
		dwStyle = GetWindowLong(ghwnd, GWL_STYLE);
		SetWindowLong(ghwnd, GWL_STYLE, dwStyle | WS_OVERLAPPEDWINDOW);
		SetWindowPlacement(ghwnd, &wpPrev);
		SetWindowPos(ghwnd, HWND_TOP, 0, 0, 0, 0, SWP_NOMOVE | SWP_NOSIZE | SWP_NOZORDER | SWP_NOOWNERZORDER | SWP_FRAMECHANGED);
		ShowCursor(TRUE);
	}

	wglMakeCurrent(NULL, NULL);

	wglDeleteContext(ghrc);
	ghrc = NULL;

	ReleaseDC(ghwnd, ghdc);
	ghdc = NULL;

	DestroyWindow(ghwnd);
	ghwnd = NULL;
}

void drawRectPoints() {
	glColor3f(1.0f, 1.0f, 1.0f);
	glPointSize(2.0f);
	glBegin(GL_POINTS);
	for (GLfloat rowX = -2.0f; rowX <= -1.2f; rowX = rowX + 0.2f)
		for (GLfloat rowY = 1.0f; rowY >= 0.4f; rowY = rowY - 0.2f)
			glVertex3f(rowX, rowY, 0.0f);
	glVertex3f(0.0f, 0.0f, 0.0f);
	glEnd();
}

void drawRect1() {
	glColor3f(1.0f, 1.0f, 1.0f);
	glPointSize(2.0f);
	glBegin(GL_LINES);
	glVertex3f(-0.4f, 1.0f, 0.0f);
	glVertex3f(0.2f, 1.0f, 0.0f);

	glVertex3f(-0.4f, 1.0f, 0.0f);
	glVertex3f(-0.4f, 0.4f, 0.0f);

	glVertex3f(0.2f, 1.0f, 0.0f);
	glVertex3f(-0.4f, 0.4f, 0.0f);

	glVertex3f(-0.2f, 1.0f, 0.0f);
	glVertex3f(-0.2f, 0.4f, 0.0f);

	glVertex3f(0.0f, 1.0f, 0.0f);
	glVertex3f(0.0f, 0.4f, 0.0f);

	glVertex3f(0.0f, 1.0f, 0.0f);
	glVertex3f(0.0f, 0.4f, 0.0f);

	glVertex3f(-0.4f, 0.6f, 0.0f);
	glVertex3f(0.2f, 0.6f, 0.0f);

	glVertex3f(-0.4f, 0.8f, 0.0f);
	glVertex3f(0.2f, 0.8f, 0.0f);

	glVertex3f(0.0f, 0.4f, 0.0f);
	glVertex3f(0.2f, 0.6f, 0.0f);

	glVertex3f(-0.4f, 0.8f, 0.0f);
	glVertex3f(-0.2f, 1.0f, 0.0f);

	glVertex3f(-0.2f, 0.4f, 0.0f);
	glVertex3f(0.2f, 0.8f, 0.0f);

	glVertex3f(-0.4f, 0.6f, 0.0f);
	glVertex3f(0.0f, 1.0f, 0.0f);
	glEnd();
}

void drawRect2() {
	glColor3f(1.0f, 1.0f, 1.0f);
	glLineWidth(2.0f);
	glBegin(GL_LINE_LOOP);
	glVertex3f(1.4f, 1.0f, 0.0f);
	glVertex3f(2.0f, 1.0f, 0.0f);
	glVertex3f(2.0f, 0.4f, 0.0f);
	glVertex3f(1.4f, 0.4f, 0.0f);
	glEnd();

	glBegin(GL_LINES);
	glVertex3f(1.6f, 1.0f, 0.0);
	glVertex3f(1.6f, 0.4f, 0.0f);

	glVertex3f(1.8f, 1.0f, 0.0);
	glVertex3f(1.8f, 0.4f, 0.0f);

	glVertex3f(1.4f, 0.6f, 0.0);
	glVertex3f(2.0f, 0.6f, 0.0f);

	glVertex3f(1.4f, 0.8f, 0.0);
	glVertex3f(2.0f, 0.8f, 0.0f);
	glEnd();

}

void drawRect3() {
	glColor3f(1.0f, 1.0f, 1.0f);
	glTranslatef(-1.6f, -1.0f, 0.0);
	glPointSize(2.0f);
	glBegin(GL_LINES);
	glVertex3f(-0.4f, 1.0f, 0.0f);
	glVertex3f(0.2f, 1.0f, 0.0f);

	glVertex3f(-0.4f, 1.0f, 0.0f);
	glVertex3f(-0.4f, 0.4f, 0.0f);

	glVertex3f(0.2f, 1.0f, 0.0f);
	glVertex3f(-0.4f, 0.4f, 0.0f);

	glVertex3f(-0.2f, 1.0f, 0.0f);
	glVertex3f(-0.2f, 0.4f, 0.0f);

	glVertex3f(0.0f, 1.0f, 0.0f);
	glVertex3f(0.0f, 0.4f, 0.0f);

	glVertex3f(0.0f, 1.0f, 0.0f);
	glVertex3f(0.0f, 0.4f, 0.0f);

	glVertex3f(-0.4f, 0.6f, 0.0f);
	glVertex3f(0.2f, 0.6f, 0.0f);

	glVertex3f(-0.4f, 0.8f, 0.0f);
	glVertex3f(0.2f, 0.8f, 0.0f);

	glVertex3f(0.0f, 0.4f, 0.0f);
	glVertex3f(0.2f, 0.6f, 0.0f);

	glVertex3f(-0.4f, 0.8f, 0.0f);
	glVertex3f(-0.2f, 1.0f, 0.0f);

	glVertex3f(-0.2f, 0.4f, 0.0f);
	glVertex3f(0.2f, 0.8f, 0.0f);

	glVertex3f(-0.4f, 0.6f, 0.0f);
	glVertex3f(0.0f, 1.0f, 0.0f);

	glVertex3f(-0.4f, 0.4f, 0.0f);
	glVertex3f(0.2f, 0.4f, 0.0f);

	glVertex3f(0.2f, 0.4f, 0.0f);
	glVertex3f(0.2f, 1.0f, 0.0f);
	glEnd();
}

void drawRect4() {
	glColor3f(1.0f, 1.0f, 1.0f);
	glTranslatef(0.0f, -1.0f, 0.0f);
	glLineWidth(2.0f);
	glBegin(GL_LINES);
	glVertex3f(-0.4f, 1.0f, 0.0f);
	glVertex3f(0.2f, 1.0f, 0.0f);

	glVertex3f(-0.4f, 1.0f, 0.0f);
	glVertex3f(-0.4f, 0.4f, 0.0f);

	glVertex3f(0.2f, 0.4f, 0.0f);
	glVertex3f(-0.4f, 0.4f, 0.0f);

	glVertex3f(0.2f, 1.0f, 0.0f);
	glVertex3f(0.2f, 0.4f, 0.0f);

	glVertex3f(-0.4f, 1.0f, 0.0f);
	glVertex3f(-0.2f, 0.4f, 0.0f);

	glVertex3f(-0.4f, 1.0f, 0.0f);
	glVertex3f(0.0f, 0.4f, 0.0f);

	glVertex3f(-0.4f, 1.0f, 0.0f);
	glVertex3f(0.2f, 0.4f, 0.0f);

	glVertex3f(-0.4f, 1.0f, 0.0f);
	glVertex3f(0.2f, 0.6f, 0.0f);

	glVertex3f(-0.4f, 1.0f, 0.0f);
	glVertex3f(0.2f, 0.8f, 0.0f);
	glEnd();
}

void drawRect5() {
	glColor3f(1.0f, 0.0f, 0.0f);
	glBegin(GL_QUADS);
	glVertex3f(1.4f, 0.0f, 0.0f);
	glVertex3f(1.6f, 0.0f, 0.0f);
	glVertex3f(1.6f, -0.6f, 0.0f);
	glVertex3f(1.4f, -0.6f, 0.0f);
	glEnd();

	glColor3f(0.0f, 1.0f, 0.0f);
	glBegin(GL_QUADS);
	glVertex3f(1.6f, 0.0f, 0.0f);
	glVertex3f(1.8f, 0.0f, 0.0f);
	glVertex3f(1.8f, -0.6f, 0.0f);
	glVertex3f(1.6f, -0.6f, 0.0f);
	glEnd();

	glLineWidth(1.0f);
	glColor3f(0.0f, 0.0f, 1.0f);
	glBegin(GL_QUADS);
	glVertex3f(1.8f, 0.0f, 0.0f);
	glVertex3f(2.0f, 0.0f, 0.0f);
	glVertex3f(2.0f, -0.6f, 0.0f);
	glVertex3f(1.8f, -0.6f, 0.0f);
	glEnd();

	glColor3f(1.0f, 1.0f, 1.0f);
	glBegin(GL_LINES);
	glVertex3f(1.6f, 0.0f, 0.0f);
	glVertex3f(1.6f, -0.6f, 0.0f);

	glVertex3f(1.8f, 0.0f, 0.0f);
	glVertex3f(1.8f, -0.6f, 0.0f);

	glVertex3f(1.4f, -0.2f, 0.0f);
	glVertex3f(2.0f, -0.2f, 0.0f);

	glVertex3f(1.4f, -0.4f, 0.0f);
	glVertex3f(2.0f, -0.4f, 0.0f);
	glEnd();
}


