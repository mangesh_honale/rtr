#include <windows.h>
#include <gl/GL.h>
#include <gl/GLU.h>

#define WIN_WIDTH 1000
#define WIN_HEIGHT 600

#pragma comment(lib,"opengl32.lib")
#pragma comment(lib,"glu32.lib")


// Global function declaration
LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);

// Global variable declaration
HWND ghwnd = NULL;
HDC ghdc = NULL;
HGLRC ghrc = NULL;



DWORD dwStyle;
WINDOWPLACEMENT wpPrev = { sizeof(WINDOWPLACEMENT) };

int giScreenWidth;
int giScreenHeight;

bool gbActiveWindow = false;
bool gbEscapeKeyIsPressed = false;
bool gbFullScreen = false;
GLfloat firstIXCoordinate = -2.7f, nTopYCoordinate = 4.8f, nBottomYCoordinate = 1.8f, saffronShadeRed = 0.0f, saffronShadeGreen = 0.0f, saffronShadeBlue = 0.0f,
greenShadeRed = 0.0f, greenShadeGreen = 0.0f, greenShadeBlue = 0.0f, iTopYCoordinate = -1.8f, iBottomYCoordinate = -4.8f, aTopYCoordindate, aXCoordinate = 2.85f, aYCoordinate = -1.8f, leftHorizontalX = -2.7f, rightHorizontalX = -2.7f;
// main()
int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpszCmdLine, int iCmdShow) {
	// Function prototype
	void Initialize(void);
	void Uninitialize(void);
	void Display(void);
	void update(void);

	// Variable declaration
	WNDCLASSEX wndClass;
	HWND hwnd;
	MSG msg;
	TCHAR szClassName[] = TEXT("RTROGL");
	bool bDone = false;
	giScreenWidth = GetSystemMetrics(SM_CXSCREEN);
	giScreenHeight = GetSystemMetrics(SM_CYSCREEN);


	// Code
	// Initializing members of struct WNDCLASSEX
	wndClass.cbSize = sizeof(WNDCLASSEX);
	wndClass.style = CS_HREDRAW | CS_VREDRAW | CS_OWNDC;
	wndClass.cbClsExtra = 0;
	wndClass.cbWndExtra = 0;
	wndClass.hInstance = hInstance;
	wndClass.hbrBackground = (HBRUSH)GetStockObject(BLACK_BRUSH);
	wndClass.hIcon = LoadIcon(NULL, IDI_APPLICATION);
	wndClass.hCursor = LoadCursor(NULL, IDC_ARROW);
	wndClass.hIconSm = LoadIcon(NULL, IDI_APPLICATION);
	wndClass.lpfnWndProc = WndProc;
	wndClass.lpszClassName = szClassName;
	wndClass.lpszMenuName = NULL;

	// Registering class
	RegisterClassEx(&wndClass);

	// Create window
	hwnd = CreateWindowEx(WS_EX_APPWINDOW,
		szClassName,
		TEXT("OpenGL Fixed Function Pipeline Using Native Windowing: Double Buffer Window"),
		WS_OVERLAPPEDWINDOW | WS_CLIPCHILDREN | WS_CLIPSIBLINGS | WS_VISIBLE,
		(giScreenWidth / 2) - 400,
		(giScreenHeight / 2) - 300,
		WIN_WIDTH,
		WIN_HEIGHT,
		NULL,
		NULL,
		hInstance,
		NULL);
	ghwnd = hwnd;

	// Initialize
	Initialize();

	ShowWindow(hwnd, SW_SHOW);
	SetForegroundWindow(hwnd);
	SetFocus(hwnd);

	// Message loop
	while (bDone == false)
	{
		if (PeekMessage(&msg, NULL, 0, 0, PM_REMOVE))
		{
			if (msg.message == WM_QUIT)
				bDone = true;
			else
			{
				TranslateMessage(&msg);
				DispatchMessage(&msg);
			}
		}
		else
		{
			if (gbActiveWindow == true)
			{
				if (gbEscapeKeyIsPressed == true)
					bDone = true;
				update();
				Display();
			}
		}
	}
	Uninitialize();
	return((int)msg.wParam);
}

// WndProc()
LRESULT CALLBACK WndProc(HWND hwnd, UINT iMsg, WPARAM wParam, LPARAM lParam)
{
	// Function prototype
	void Resize(int, int);
	void ToggleFullscreen(void);
	void Uninitialize(void);

	// Code
	switch (iMsg)
	{
	case WM_ACTIVATE:
		if (HIWORD(wParam) == 0)
			gbActiveWindow = true;
		else
			gbActiveWindow = false;
		break;
	case WM_SIZE:
		Resize(LOWORD(lParam), HIWORD(lParam));
		break;
	case WM_KEYDOWN:
		switch (wParam)
		{
		case VK_ESCAPE:
			gbEscapeKeyIsPressed = true;
			break;
		case 0x46:
			if (gbFullScreen == false)
			{
				ToggleFullscreen();
				gbFullScreen = true;
			}
			else
			{
				ToggleFullscreen();
				gbFullScreen = false;
			}
			break;
		}
		break;
	case WM_DESTROY:
		PostQuitMessage(0);
		break;
	default:
		break;
	}
	return(DefWindowProc(hwnd, iMsg, wParam, lParam));
}

void ToggleFullscreen(void)
{
	// Variable declaration
	MONITORINFO  mi;
	int x, y;

	// Code
	if (gbFullScreen == false)
	{
		dwStyle = GetWindowLong(ghwnd, GWL_STYLE);
		if (dwStyle & WS_OVERLAPPEDWINDOW)
		{
			mi = { sizeof(MONITORINFO) };
			if (GetWindowPlacement(ghwnd, &wpPrev) && GetMonitorInfo(MonitorFromWindow(ghwnd, MONITORINFOF_PRIMARY), &mi))
			{
				SetWindowLong(ghwnd, GWL_STYLE, dwStyle & ~WS_OVERLAPPEDWINDOW);
				SetWindowPos(ghwnd, HWND_TOP, mi.rcMonitor.left, mi.rcMonitor.top, mi.rcMonitor.right - mi.rcMonitor.left, mi.rcMonitor.bottom - mi.rcMonitor.top, SWP_NOZORDER | SWP_FRAMECHANGED);
			}
		}
		ShowCursor(FALSE);
	}
	else
	{
		// Code
		x = (giScreenWidth / 2) - ((wpPrev.rcNormalPosition.right - wpPrev.rcNormalPosition.left) / 2);
		y = (giScreenHeight / 2) - ((wpPrev.rcNormalPosition.bottom - wpPrev.rcNormalPosition.top) / 2);
		SetWindowLong(ghwnd, GWL_STYLE, dwStyle | WS_OVERLAPPEDWINDOW);
		SetWindowPlacement(ghwnd, &wpPrev);
		SetWindowPos(ghwnd, HWND_TOP, x, y, 0, 0, SWP_NOSIZE | SWP_NOOWNERZORDER | SWP_NOZORDER | SWP_FRAMECHANGED);
		ShowCursor(TRUE);
	}
}

void Initialize(void)
{
	// Function prototypes
	void Resize(int, int);
	// Variable Declarations
	PIXELFORMATDESCRIPTOR pfd;
	int iPixelFormatIndex;

	// Code
	ZeroMemory(&pfd, sizeof(PIXELFORMATDESCRIPTOR));

	// Initialization of structure 'PIXELFORMATDESCRIPTOR'
	pfd.nSize = sizeof(PIXELFORMATDESCRIPTOR);
	pfd.nVersion = 1;
	pfd.dwFlags = PFD_DRAW_TO_WINDOW | PFD_SUPPORT_OPENGL | PFD_DOUBLEBUFFER;
	pfd.iPixelType = PFD_TYPE_RGBA;
	pfd.cColorBits = 32;
	pfd.cRedBits = 8;
	pfd.cGreenBits = 8;
	pfd.cBlueBits = 8;
	pfd.cAlphaBits = 8;
	pfd.cDepthBits = 32;
	ghdc = GetDC(ghwnd);

	iPixelFormatIndex = ChoosePixelFormat(ghdc, &pfd);
	if (iPixelFormatIndex == 0)
	{
		ReleaseDC(ghwnd, ghdc);
		ghdc = NULL;
	}

	if (SetPixelFormat(ghdc, iPixelFormatIndex, &pfd) == FALSE)
	{
		ReleaseDC(ghwnd, ghdc);
		ghdc = NULL;
	}

	ghrc = wglCreateContext(ghdc);
	if (ghrc == NULL)
	{
		ReleaseDC(ghwnd, ghdc);
		ghdc = NULL;
	}

	if (wglMakeCurrent(ghdc, ghrc) == FALSE)
	{
		wglDeleteContext(ghrc);
		ghrc = NULL;
		ReleaseDC(ghwnd, ghdc);
		ghdc = NULL;
	}

	glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
	glClearDepth(1.0f);
	glEnable(GL_DEPTH_TEST);
	glEnable(GL_BLEND);
	glDepthFunc(GL_LEQUAL);
	glShadeModel(GL_SMOOTH);
	glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST);
	Resize(WIN_WIDTH, WIN_HEIGHT);
}

void Display(void)
{
	// Function declaration
	void drawI();
	void drawN();
	void drawD();
	void drawI2();
	void drawA();

	// Code
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(0.0f, 0.0f, -4.3f);
	glLineWidth(5.0f);
	glBegin(GL_LINES);
	drawI();
	drawN();
	drawD();
	drawI2();
	drawA();
	glEnd();
	SwapBuffers(ghdc);
}

void drawI() {
	
	glColor3f(1.0f, 0.6f, 0.2f);
	glVertex3f(firstIXCoordinate, 1.5f, 0.0f);
	glColor3f(0.0745f, 0.53f, 0.031f);
	glVertex3f(firstIXCoordinate, -1.5f, 0.0f);
	
}

void drawN() {
	//left line
	glColor3f(1.0f, 0.6f, 0.2f);
	glVertex3f(-1.8f, nTopYCoordinate, 0.0f);
	glColor3f(0.0745f, 0.53f, 0.031f);
	glVertex3f(-1.8f, nBottomYCoordinate, 0.0f);

	//cross line
	glColor3f(1.0f, 0.6f, 0.2f);
	glVertex3f(-1.8f, nTopYCoordinate, 0.0f);
	glColor3f(0.0745f, 0.53f, 0.031f);
	glVertex3f(-1.1f, nBottomYCoordinate, 0.0f);

	//right line
	glColor3f(1.0f, 0.6f, 0.2f);
	glVertex3f(-1.1f, nTopYCoordinate, 0.0f);
	glColor3f(0.0745f, 0.53f, 0.031f);
	glVertex3f(-1.1f, nBottomYCoordinate, 0.0f);
}

void drawD() {
	//left line
	glColor3f(saffronShadeRed, saffronShadeGreen, saffronShadeBlue);
	glVertex3f(-0.6f, 1.5f, 0.0f);
	glColor3f(greenShadeRed, greenShadeGreen, greenShadeBlue);
	glVertex3f(-0.6f, -1.5f, 0.0f);

	//right line
	glColor3f(saffronShadeRed, saffronShadeGreen, saffronShadeBlue);
	glVertex3f(0.1f, 1.5f, 0.0f);
	glColor3f(greenShadeRed, greenShadeGreen, greenShadeBlue);
	glVertex3f(0.1f, -1.5f, 0.0f);

	//top horizontal line
	glColor3f(saffronShadeRed, saffronShadeGreen, saffronShadeBlue);
	glVertex3f(-0.65f, 1.5f, 0.0f);
	glVertex3f(0.1f, 1.5f, 0.0f);

	//bottom horizontal line
	glColor3f(greenShadeRed, greenShadeGreen, greenShadeBlue);
	glVertex3f(-0.65f, -1.5f, 0.0f);
	glVertex3f(0.1f, -1.5f, 0.0f);
}

void drawI2() {

	glColor3f(1.0f, 0.6f, 0.2f);
	glVertex3f(0.6f, iTopYCoordinate, 0.0f);
	glColor3f(0.0745f, 0.53f, 0.031f);
	glVertex3f(0.6f, iBottomYCoordinate, 0.0f);

}

void drawA() {
	//left inclined line
	glColor3f(1.0f, 0.6f, 0.2f);
	glVertex3f(aXCoordinate, aYCoordinate, 0.0f);
	glColor3f(0.0745f, 0.53f, 0.031f);
	glVertex3f(aXCoordinate - 0.7f, aYCoordinate - 3.0f, 0.0f); 

	//right inclined line
	glColor3f(1.0f, 0.6f, 0.2f);
	glVertex3f(aXCoordinate, aYCoordinate, 0.0f);
	glColor3f(0.0745f, 0.53f, 0.031f);
	glVertex3f(aXCoordinate + 0.7f, aYCoordinate - 3.0f, 0.0f);

	//horizonal lines
	glColor3f(1.0f, 0.6f, 0.2f);
	glVertex3f(leftHorizontalX, 0.0f, 0.0f);
	glVertex3f(rightHorizontalX, 0.0f, 0.0f);

	glColor3f(1.0f, 1.0f, 1.0f);
	glVertex3f(leftHorizontalX, -0.005f, 0.0f);
	glVertex3f(rightHorizontalX, -0.005f, 0.0f);

	glColor3f(0.0745f, 0.53f, 0.031f);
	glVertex3f(leftHorizontalX, -0.01f, 0.0f);
	glVertex3f(rightHorizontalX, -0.01f, 0.0f);

}

void Resize(int width, int height)
{
	// Code
	if (height == 0)
		height = 1;
	glViewport(0, 0, (GLsizei)width, (GLsizei)height);
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	gluPerspective(45.0f, (GLfloat)width /  (GLfloat)height, 0.1f, 100.0f);
}

void Uninitialize(void)
{
	// Uninitialization code

	if (gbFullScreen == true)
	{
		dwStyle = GetWindowLong(ghwnd, GWL_STYLE);
		SetWindowLong(ghwnd, GWL_STYLE, dwStyle | WS_OVERLAPPEDWINDOW);
		SetWindowPlacement(ghwnd, &wpPrev);
		SetWindowPos(ghwnd, HWND_TOP, 0, 0, 0, 0, SWP_NOMOVE | SWP_NOSIZE | SWP_NOZORDER | SWP_NOOWNERZORDER | SWP_FRAMECHANGED);
		ShowCursor(TRUE);
	}

	wglMakeCurrent(NULL, NULL);

	wglDeleteContext(ghrc);
	ghrc = NULL;

	ReleaseDC(ghwnd, ghdc);
	ghdc = NULL;

	DestroyWindow(ghwnd);
	ghwnd = NULL;
}

void update() {
	//move first I along X axis from left to right
	if(firstIXCoordinate <= -2.3f)
		firstIXCoordinate += 0.0001f;

	//move N along Y axis top down
	if (nTopYCoordinate >= 1.5f && firstIXCoordinate >= -2.3f)
		nTopYCoordinate -= 0.0001f;
	if (nBottomYCoordinate >= -1.5f && firstIXCoordinate >= -2.3f)
		nBottomYCoordinate -= 0.0001f;
	
	//D animation
	if (nBottomYCoordinate <= -1.5f) {
		if(saffronShadeRed <= 1.0f)
			saffronShadeRed += 0.0001f;
		if (saffronShadeGreen <= 0.6f)
			saffronShadeGreen += 0.0006f;
		if (saffronShadeBlue <= 0.2)
			saffronShadeBlue += 0.0002f;
		if (greenShadeRed <= 0.0745f)
			greenShadeRed += 0.0000745f;
		if (greenShadeGreen <= 0.53f)
			greenShadeGreen += 0.00053f;
		if (greenShadeBlue <= 0.031f)
			greenShadeBlue += 0.000031f;
	}

	//I animation
	if (saffronShadeRed >= 1.0f && iTopYCoordinate <= 1.5f)
		iTopYCoordinate += 0.0001f;
	if (saffronShadeRed >= 1.0f && iBottomYCoordinate <= -1.5f)
		iBottomYCoordinate += 0.0001f;

	//A animation
	if (iBottomYCoordinate >= -1.5f && aYCoordinate <= 1.5f)
		aYCoordinate += 0.0001f;
	if (aYCoordinate >= 1.5f && aXCoordinate >= 1.8f)
		aXCoordinate -= 0.0001f;

	//horizontal line animation
	if (aXCoordinate <= 1.8f && rightHorizontalX <= 2.15f)
		rightHorizontalX += 0.0001f;
	if (rightHorizontalX >= 2.15f && leftHorizontalX <= 1.45f)
		leftHorizontalX += 0.0001f;
}

