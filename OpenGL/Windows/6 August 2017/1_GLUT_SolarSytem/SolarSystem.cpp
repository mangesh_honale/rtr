#include <GL/freeglut.h>

//global variable declaration
bool bFullscreen = false; //variable to toggle for fullscreen
static int year = 0, day = 0;
int main(int argc, char** argv)
{
	//function prototypes
	void display(void);
	void resize(int, int);
	void keyboard(unsigned char, int, int);
	void mouse(int, int, int, int);
	void initialize(void);
	void uninitialize(void);

	//code
	glutInit(&argc, argv);

	glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGB);

	glutInitWindowSize(800, 600); //to declare initial window size
	int giX = GetSystemMetrics(SM_CXFULLSCREEN) / 2 - 400;
	int giY = GetSystemMetrics(SM_CYFULLSCREEN) / 2 - 300;
	glutInitWindowPosition(giX, giY); //to declare initial window position

	glutCreateWindow("GLUT: Solar System"); //open the window with "OpenGL First Window : Hello World" in the title bar

	initialize();

	glutDisplayFunc(display);
	glutReshapeFunc(resize);
	glutKeyboardFunc(keyboard);
	glutMouseFunc(mouse);
	glutCloseFunc(uninitialize);

	glutMainLoop();

	//	return(0); 
}

void display(void)
{

	//code

	//to clear all pixels
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	gluLookAt(0.0f, 0.0f, 5.0f, 0.0f, 0.0f, 0.0f, 0.0f, 1.0f, 0.0f);
	glColor3f(1.0f, 1.0f, 0.0f);
	glPushMatrix();
	glutWireSphere(1.0, 20, 16);
	glRotatef((GLfloat)year, 0.0f, 1.0f, 0.0f);
	glTranslatef(2.0, 0.0, 0.0);
	glRotatef((GLfloat)day, 0.0, 1.0, 0.0);
	glColor3f(0.0f, 0.0f, 1.0f);
	glutWireSphere(0.2, 10, 8);
	glPopMatrix();
	//to process buffered OpenGL Routines
	glutSwapBuffers();
}

void initialize(void)
{
	//code
	//to select clearing (background) clear
	glClearColor(0.0f, 0.0f, 0.0f, 0.0f); //blue 
	glShadeModel(GL_FLAT);
}

void keyboard(unsigned char key, int x, int y)
{
	TCHAR str[255];
	//code
	switch (key)
	{
	case 27: // Escape
		glutLeaveMainLoop();
		break;
	case 'D':
		day = (day + 6) % 360;
		glutPostRedisplay();
		break;
	case 'd':
		day = (day - 6) % 360;
		glutPostRedisplay();
		break;
	case 'Y':
		year = (year + 3) % 360;
		glutPostRedisplay();
		break;
	case 'y':
		year = (year - 3) % 360;
		glutPostRedisplay();
		break;
	case 'F':
	case 'f':
		if (bFullscreen == false)
		{
			glutFullScreen();
			bFullscreen = true;
		}
		else
		{
			glutLeaveFullScreen();
			bFullscreen = false;
		}

		break;
	case 'c':
	case 'C':
		glClearColor(0.0f, 1.0f, 1.0f, 0.0f); //blue 
		glutPostRedisplay();
		break;
	case 'r':
	case 'R':
		glClearColor(1.0f, 1.0f, 1.0f, 0.0f); //blue 
		glutPostRedisplay();
		break;
	case 'g':
	case 'G':
		glClearColor(0.0f, 1.0f, 0.0f, 0.0f);
		glutPostRedisplay();
		break;
	case 'k':
	case 'K':
		glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
		glutPostRedisplay();
		break;
	case 'm':
	case 'M':
		glClearColor(1.0f, 0.0f, 1.0f, 0.0f);
		glutPostRedisplay();
		break;
	default:

		wsprintf(str, TEXT("Invalid key pressed!"));
		MessageBox(NULL, str, TEXT("Key Press"), MB_OK);
		break;
	}
}

void mouse(int button, int state, int x, int y)
{
	//code
	switch (button)
	{
	case GLUT_LEFT_BUTTON:
		TCHAR str[255];
		wsprintf(str, TEXT("Left mouse button clicked!"));
		MessageBox(NULL, str, TEXT("Mouse clicks"), MB_OK);

		break;
	default:
		break;
	}
}

void resize(int width, int height)
{
	// code
	if (height == 0)
		height = 1;
	glViewport(0, 0, (GLsizei)width, (GLsizei)height);
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	gluPerspective(45.0f, (GLfloat)width / (GLfloat)height, 0.1f, 100.0f);
}

void uninitialize(void)
{
	// code
}

