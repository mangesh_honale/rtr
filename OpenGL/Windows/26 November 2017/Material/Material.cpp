#include <windows.h>
#include <gl/GL.h>
#include <gl/GLU.h>

#define WIN_WIDTH 800
#define WIN_HEIGHT 600

#pragma comment(lib,"opengl32.lib")
#pragma comment(lib,"glu32.lib")


// Global function declaration
LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);

// Global variable declaration
HWND ghwnd = NULL;
HDC ghdc = NULL;
HGLRC ghrc = NULL;
GLUquadric *quadric = NULL;
GLfloat light_ambient[] = { 0.0f, 0.0f, 0.0f, 1.0f };
GLfloat light_defuse[] = { 1.0f, 1.0f, 1.0f, 1.0f };
GLfloat light_specular[] = { 1.0f, 1.0f, 1.0f, 1.0f };
GLfloat light_position[] = { 0.0f, 0.0f, 1.0f, 0.0f };
GLfloat gfLightAngle = 0.0f;

GLfloat light_model_ambient[] = {0.2f, 0.2f, 0.2f, 0.0f};
GLfloat light_model_local_viewer[] = {0.0f};

//11
GLfloat material_emerald_ambient[] = { 0.0215f, 0.1745f, 0.0215f, 1.0f };
GLfloat material_emerald_diffuse[] = { 0.07568f, 0.61424f, 0.07568f, 1.0f };
GLfloat material_emerald_specular[] = { 0.633f, 0.727811f, 0.633f, 1.0f };
GLfloat material_emerald_shininess = 0.6f * 128;

//12
GLfloat material_jade_ambient[] = {0.135f, 0.225f, 0.1575f, 1.0f};
GLfloat material_jade_diffuse[] = {0.54f, 0.89f, 0.63f, 1.0f};
GLfloat material_jade_specular[] = {0.316228f, 0.316228f, 0.316228f, 1.0f};
GLfloat material_jade_shininess = 0.1f * 128;

//13
GLfloat material_obsidian_ambient[] = {0.05375f, 0.05f, 0.06625f, 1.0f};
GLfloat material_obsidian_diffuse[] = {0.18275f, 0.17f, 0.22525f, 1.0f};
GLfloat material_obsidian_specular[] = { 0.332741f, 0.328634f, 0.346435f, 1.0f };
GLfloat material_obsidian_shininess = 0.3f * 128;

//14
GLfloat material_pearl_ambient[] = { 0.25f, 0.20725f, 0.20725f, 1.0f };
GLfloat material_pearl_diffuse[] = { 1.0f, 0.829f, 0.829f, 1.0f };
GLfloat material_pearl_specular[] = { 0.296648f, 0.296648f, 0.296648f, 1.0f };
GLfloat material_pearl_shininess = 0.088f * 128;

//15
GLfloat material_ruby_ambient[] = { 0.1745f, 0.01175f, 0.01175f, 1.0f };
GLfloat material_ruby_diffuse[] = { 0.61424f, 0.04136f, 0.04136f, 1.0f };
GLfloat material_ruby_specular[] = { 0.727811f, 0.626959f, 0.626959f, 1.0f };
GLfloat material_ruby_shininess = 0.6f * 128;

//16
GLfloat material_turquoise_ambient[] = { 0.1f, 0.18725f, 0.1745f, 1.0f };
GLfloat material_turquoise_diffuse[] = { 0.396f, 0.74151f, 0.69102f, 1.0f };
GLfloat material_turquoise_specular[] = { 0.297254f, 0.30829f, 0.306678f, 1.0f };
GLfloat material_turquoise_shininess = 0.1 * 128;

//21
GLfloat material_brass_ambient[] = { 0.329412f, 0.223529f, 0.027451f, 1.0f };
GLfloat material_brass_diffuse[] = { 0.780392f, 0.568627f, 0.113725f, 1.0f };
GLfloat material_brass_specular[] = { 0.992157f, 0.941176f, 0.807843f, 1.0f };
GLfloat material_brass_shininess = 0.21794872f * 128;

//22
GLfloat material_bronze_ambient[] = { 0.2125f, 0.1275, 0.054f, 1.0f };
GLfloat material_bronze_diffuse[] = { 0.714f, 0.4284f, 0.18144f, 1.0f };
GLfloat material_bronze_specular[] = { 0.393548f, 0.271906f, 0.166721f, 1.0f };
GLfloat material_bronze_shininess = 0.2f * 128;

//23
GLfloat material_chrome_ambient[] = { 0.25f, 0.25f, 0.25f, 1.0f };
GLfloat material_chrome_diffuse[] = { 0.4f, 0.4f, 0.4f, 1.0f };
GLfloat material_chrome_specular[] = { 0.774597f, 0.774597f, 0.774597f, 1.0f };
GLfloat material_chrome_shininess = 0.6f * 128;

//24
GLfloat material_copper_ambient[] = { 0.19125f, 0.0735f, 0.0225f, 1.0f };
GLfloat material_copper_diffuse[] = { 0.7038f, 0.27048f, 0.0828f, 1.0f };
GLfloat material_copper_specular[] = { 0.256777f, 0.137622f, 0.86014f, 1.0f };
GLfloat material_copper_shininess = 0.1f * 128;

//25
GLfloat material_gold_ambient[] = { 0.24725f, 0.1995, 0.0745f, 1.0f };
GLfloat material_gold_diffuse[] = { 0.75164f, 0.61648f, 0.22648f, 1.0f };
GLfloat material_gold_specular[] = { 0.628281f, 0.555802f, 0.366065f, 1.0f };
GLfloat material_gold_shininess = 0.4f * 128;

//26
GLfloat material_silver_ambient[] = { 0.19225f, 0.19225f, 0.19225f, 1.0f };
GLfloat material_silver_diffuse[] = { 0.50754f, 0.50754f, 0.50754f, 1.0f };
GLfloat material_silver_specular[] = { 0.508273f, 0.508273f, 0.508273f, 1.0f };
GLfloat material_silver_shininess = 0.4f * 128;

//31
GLfloat material_black_ambient[] = { 0.0f, 0.0f, 0.0f, 1.0f };
GLfloat material_black_diffuse[] = { 0.01f, 0.01f, 0.01f, 1.0f };
GLfloat material_black_specular[] = { 0.50f, 0.50f, 0.50f, 1.0f };
GLfloat material_black_shininess = 0.25f * 128;

//32
GLfloat material_cyan_ambient[] = { 0.0f, 0.1f, 0.06f, 1.0f };
GLfloat material_cyan_diffuse[] = { 0.0f, 0.50980392f, 0.50980932f, 1.0f };
GLfloat material_cyan_specular[] = { 0.50196078f, 0.50196078f, 0.50196078f, 1.0f };
GLfloat material_cyan_shininess = 0.25f * 128;

//33
GLfloat material_green_ambient[] = { 0.0f, 0.0f, 0.0f, 1.0f };
GLfloat material_green_diffuse[] = { 0.1f, 0.35f, 0.1f, 1.0f };
GLfloat material_green_specular[] = { 0.45f, 0.45f, 0.45f, 1.0f };
GLfloat material_green_shininess = 0.25f * 128;

//34
GLfloat material_red_ambient[] = { 0.0f, 0.0f, 0.0f, 1.0f };
GLfloat material_red_diffuse[] = { 0.5f, 0.0f, 0.0f, 1.0f };
GLfloat material_red_specular[] = { 0.7f, 0.6f, 0.6f, 1.0f };
GLfloat material_red_shininess = 0.25f * 128;

//35
GLfloat material_white_ambient[] = { 0.0f, 0.0f, 0.0f, 1.0f };
GLfloat material_white_diffuse[] = { 0.55f, 0.55f, 0.55f, 1.0f };
GLfloat material_white_specular[] = { 0.70f, 0.70f, 0.70f, 1.0f };
GLfloat material_white_shininess = 0.25f * 128;

//36
GLfloat material_yellow_ambient[] = { 0.0f, 0.0f, 0.0f, 1.0f };
GLfloat material_yellow_diffuse[] = { 0.5f, 0.5f, 0.0f, 1.0f };
GLfloat material_yellow_specular[] = { 0.60f, 0.60f, 0.50f, 1.0f };
GLfloat material_yellow_shininess = 0.25f * 128;

//41
GLfloat material_black2_ambient[] = { 0.02f, 0.02f, 0.02f, 1.0f };
GLfloat material_black2_diffuse[] = { 0.01f, 0.01f, 0.01f, 1.0f };
GLfloat material_black2_specular[] = { 0.4f, 0.4f, 0.4f, 1.0f };
GLfloat material_black2_shininess = 0.078125f * 128;

//42
GLfloat material_cyan2_ambient[] = { 0.0f, 0.05f, 0.05f, 1.0f };
GLfloat material_cyan2_diffuse[] = { 0.4f, 0.5f, 0.0f, 1.0f };
GLfloat material_cyan2_specular[] = { 0.04f, 0.7f, 0.7f, 1.0f };
GLfloat material_cyan2_shininess = 0.078125f * 128;

//43
GLfloat material_green2_ambient[] = { 0.0f, 0.05f, 0.0f, 1.0f };
GLfloat material_green2_diffuse[] = { 0.4f, 0.5f, 0.4f, 1.0f };
GLfloat material_green2_specular[] = { 0.04f, 0.7f, 0.04f, 1.0f };
GLfloat material_green2_shininess = 0.078125f * 128;

//44
GLfloat material_red2_ambient[] = { 0.05f, 0.0f, 0.0f, 1.0f };
GLfloat material_red2_diffuse[] = { 0.5f, 0.4f, 0.4f, 1.0f };
GLfloat material_red2_specular[] = { 0.7f, 0.04f, 0.04f, 1.0f };
GLfloat material_red2_shininess = 0.078125f * 128;

//45
GLfloat material_white2_ambient[] = { 0.05f, 0.05f, 0.05f, 1.0f };
GLfloat material_white2_diffuse[] = { 0.5f, 0.5f, 0.5f, 1.0f };
GLfloat material_white2_specular[] = { 0.7f, 0.7f, 0.7f, 1.0f };
GLfloat material_white2_shininess = 0.078125f * 128;

//46
GLfloat material_yellow2_ambient[] = { 0.05f, 0.05f, 0.0f, 1.0f };
GLfloat material_yellow2_diffuse[] = { 0.5f, 0.5f, 0.4f, 1.0f };
GLfloat material_yellow2_specular[] = { 0.7f, 0.7f, 0.04f, 1.0f };
GLfloat material_yellow2_shininess = 0.078125f * 128;


DWORD dwStyle;
WINDOWPLACEMENT wpPrev = { sizeof(WINDOWPLACEMENT) };
BOOL isLightingOn;
int giScreenWidth;
int giScreenHeight;
int axisIdentifier = -1; //0: x-axis, 1: y-axis, 2: z-axis, -1: not revolving
bool gbActiveWindow = false;
bool gbEscapeKeyIsPressed = false;
bool gbFullScreen = false;

// main()
int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpszCmdLine, int iCmdShow) {
	// Function prototype
	void Initialize(void);
	void Uninitialize(void);
	void Display(void);
	void Update();

	// Variable declaration
	WNDCLASSEX wndClass;
	HWND hwnd;
	MSG msg;
	TCHAR szClassName[] = TEXT("RTROGL");
	bool bDone = false;
	giScreenWidth = GetSystemMetrics(SM_CXSCREEN);
	giScreenHeight = GetSystemMetrics(SM_CYSCREEN);


	// Code
	// Initializing members of struct WNDCLASSEX
	wndClass.cbSize = sizeof(WNDCLASSEX);
	wndClass.style = CS_HREDRAW | CS_VREDRAW | CS_OWNDC;
	wndClass.cbClsExtra = 0;
	wndClass.cbWndExtra = 0;
	wndClass.hInstance = hInstance;
	wndClass.hbrBackground = (HBRUSH)GetStockObject(BLACK_BRUSH);
	wndClass.hIcon = LoadIcon(NULL, IDI_APPLICATION);
	wndClass.hCursor = LoadCursor(NULL, IDC_ARROW);
	wndClass.hIconSm = LoadIcon(NULL, IDI_APPLICATION);
	wndClass.lpfnWndProc = WndProc;
	wndClass.lpszClassName = szClassName;
	wndClass.lpszMenuName = NULL;

	// Registering class
	RegisterClassEx(&wndClass);

	// Create window
	hwnd = CreateWindowEx(WS_EX_APPWINDOW,
		szClassName,
		TEXT("OpenGL Fixed Function Pipeline Using Native Windowing: Double Buffer Window"),
		WS_OVERLAPPEDWINDOW | WS_CLIPCHILDREN | WS_CLIPSIBLINGS | WS_VISIBLE,
		(giScreenWidth / 2) - 400,
		(giScreenHeight / 2) - 300,
		WIN_WIDTH,
		WIN_HEIGHT,
		NULL,
		NULL,
		hInstance,
		NULL);
	ghwnd = hwnd;

	// Initialize
	Initialize();

	ShowWindow(hwnd, SW_MAXIMIZE);
	SetForegroundWindow(hwnd);
	SetFocus(hwnd);

	// Message loop
	while (bDone == false)
	{
		if (PeekMessage(&msg, NULL, 0, 0, PM_REMOVE))
		{
			if (msg.message == WM_QUIT)
				bDone = true;
			else
			{
				TranslateMessage(&msg);
				DispatchMessage(&msg);
			}
		}
		else
		{
			if (gbActiveWindow == true)
			{
				if (gbEscapeKeyIsPressed == true)
					bDone = true;
				Update();
				Display();
			}
		}
	}
	Uninitialize();
	return((int)msg.wParam);
}

// WndProc()
LRESULT CALLBACK WndProc(HWND hwnd, UINT iMsg, WPARAM wParam, LPARAM lParam)
{
	// Function prototype
	void Resize(int, int);
	void ToggleFullscreen(void);
	void Uninitialize(void);

	// Code
	switch (iMsg)
	{
	case WM_ACTIVATE:
		if (HIWORD(wParam) == 0)
			gbActiveWindow = true;
		else
			gbActiveWindow = false;
		break;
	case WM_SIZE:
		Resize(LOWORD(lParam), HIWORD(lParam));
		break;
	case WM_KEYDOWN:
		switch (wParam)
		{
		case 0x4C:
			if (isLightingOn) {
				glDisable(GL_LIGHTING);
				isLightingOn = FALSE;
			}
			else {
				glEnable(GL_LIGHTING);
				isLightingOn = TRUE;
			}

			break;
		case 0x58: axisIdentifier = 0;
			light_position[0] = 0.0f;
			light_position[1] = 0.0f;
			gfLightAngle = 0.0f;
			break;
		case 0x59: axisIdentifier = 1;
			light_position[0] = 0.0f;
			light_position[1] = 0.0f;
			gfLightAngle = 0.0f;
			break;
		case 0x5A: axisIdentifier = 2;
			light_position[0] = 0.0f;
			light_position[1] = 0.0f;
			gfLightAngle = 0.0f;
			break;
		case VK_ESCAPE:
			gbEscapeKeyIsPressed = true;
			break;
		case 0x46:
			if (gbFullScreen == false)
			{
				ToggleFullscreen();
				gbFullScreen = true;
			}
			else
			{
				ToggleFullscreen();
				gbFullScreen = false;
			}
			break;
		}
		break;
	case WM_DESTROY:
		PostQuitMessage(0);
		break;
	default:
		break;
	}
	return(DefWindowProc(hwnd, iMsg, wParam, lParam));
}

void ToggleFullscreen(void)
{
	// Variable declaration
	MONITORINFO  mi;
	int x, y;

	// Code
	if (gbFullScreen == false)
	{
		dwStyle = GetWindowLong(ghwnd, GWL_STYLE);
		if (dwStyle & WS_OVERLAPPEDWINDOW)
		{
			mi = { sizeof(MONITORINFO) };
			if (GetWindowPlacement(ghwnd, &wpPrev) && GetMonitorInfo(MonitorFromWindow(ghwnd, MONITORINFOF_PRIMARY), &mi))
			{
				SetWindowLong(ghwnd, GWL_STYLE, dwStyle & ~WS_OVERLAPPEDWINDOW);
				SetWindowPos(ghwnd, HWND_TOP, mi.rcMonitor.left, mi.rcMonitor.top, mi.rcMonitor.right - mi.rcMonitor.left, mi.rcMonitor.bottom - mi.rcMonitor.top, SWP_NOZORDER | SWP_FRAMECHANGED);
			}
		}
		ShowCursor(FALSE);
	}
	else
	{
		// Code
		x = (giScreenWidth / 2) - ((wpPrev.rcNormalPosition.right - wpPrev.rcNormalPosition.left) / 2);
		y = (giScreenHeight / 2) - ((wpPrev.rcNormalPosition.bottom - wpPrev.rcNormalPosition.top) / 2);
		SetWindowLong(ghwnd, GWL_STYLE, dwStyle | WS_OVERLAPPEDWINDOW);
		SetWindowPlacement(ghwnd, &wpPrev);
		SetWindowPos(ghwnd, HWND_TOP, x, y, 0, 0, SWP_NOSIZE | SWP_NOOWNERZORDER | SWP_NOZORDER | SWP_FRAMECHANGED);
		ShowCursor(TRUE);
	}
}

void Initialize(void)
{
	// Function prototypes
	void Resize(int, int);
	// Variable Declarations
	PIXELFORMATDESCRIPTOR pfd;
	int iPixelFormatIndex;

	// Code
	ZeroMemory(&pfd, sizeof(PIXELFORMATDESCRIPTOR));

	// Initialization of structure 'PIXELFORMATDESCRIPTOR'
	pfd.nSize = sizeof(PIXELFORMATDESCRIPTOR);
	pfd.nVersion = 1;
	pfd.dwFlags = PFD_DRAW_TO_WINDOW | PFD_SUPPORT_OPENGL | PFD_DOUBLEBUFFER;
	pfd.iPixelType = PFD_TYPE_RGBA;
	pfd.cColorBits = 32;
	pfd.cRedBits = 8;
	pfd.cGreenBits = 8;
	pfd.cBlueBits = 8;
	pfd.cAlphaBits = 8;
	pfd.cDepthBits = 32;
	ghdc = GetDC(ghwnd);

	iPixelFormatIndex = ChoosePixelFormat(ghdc, &pfd);
	if (iPixelFormatIndex == 0)
	{
		ReleaseDC(ghwnd, ghdc);
		ghdc = NULL;
	}

	if (SetPixelFormat(ghdc, iPixelFormatIndex, &pfd) == FALSE)
	{
		ReleaseDC(ghwnd, ghdc);
		ghdc = NULL;
	}

	ghrc = wglCreateContext(ghdc);
	if (ghrc == NULL)
	{
		ReleaseDC(ghwnd, ghdc);
		ghdc = NULL;
	}

	if (wglMakeCurrent(ghdc, ghrc) == FALSE)
	{
		wglDeleteContext(ghrc);
		ghrc = NULL;
		ReleaseDC(ghwnd, ghdc);
		ghdc = NULL;
	}

	glClearColor(0.25f, 0.25f, 0.25f, 0.0f);
	glClearDepth(1.0f);
	glEnable(GL_DEPTH_TEST);
	glDepthFunc(GL_LEQUAL);
	glShadeModel(GL_SMOOTH);
	glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST);

	glEnable(GL_AUTO_NORMAL);
	glEnable(GL_NORMALIZE);
	glLightModelfv(GL_LIGHT_MODEL_AMBIENT, light_model_ambient);
	glLightModelfv(GL_LIGHT_MODEL_LOCAL_VIEWER, light_model_local_viewer);

	glLightfv(GL_LIGHT0, GL_AMBIENT, light_ambient);
	glLightfv(GL_LIGHT0, GL_DIFFUSE, light_defuse);
	glLightfv(GL_LIGHT0, GL_SPECULAR, light_specular);
	

	glEnable(GL_LIGHT0);
	Resize(WIN_WIDTH, WIN_HEIGHT);
}

void Display(void)
{
	// Code
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glPushMatrix();
	gluLookAt(0.0f, 0.0f, 0.1f, 0.0f, 0.0f, 0.0f, 0, 1, 0);
	glPushMatrix();

	if (axisIdentifier == 0) {
		
		glRotatef(gfLightAngle, 1.0f, 0.0f, 0.0f);
		light_position[1] = gfLightAngle;
	}
	if (axisIdentifier == 1) {
		
		glRotatef(gfLightAngle, 0.0f, 1.0f, 0.0f);
		light_position[0] = gfLightAngle;
	}
	if (axisIdentifier == 2) {
		
		glRotatef(gfLightAngle, 0.0f, 0.0f, 1.0f);
		light_position[0] = gfLightAngle;
	}
	glLightfv(GL_LIGHT0, GL_POSITION, light_position);
	glPopMatrix();

	//11
	glPushMatrix();
	glTranslatef(-6.0f, 5.0f, -14.0f);
	glMaterialfv(GL_FRONT, GL_AMBIENT, material_emerald_ambient);
	glMaterialfv(GL_FRONT, GL_DIFFUSE, material_emerald_diffuse);
	glMaterialfv(GL_FRONT, GL_SPECULAR, material_emerald_specular);
	glMaterialf(GL_FRONT, GL_SHININESS, material_emerald_shininess);
	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
	quadric = gluNewQuadric();
	gluSphere(quadric, 0.75, 30, 30);
	glPopMatrix();

	//12
	glPushMatrix();
	glTranslatef(-6.0f, 3.0f, -14.0f);
	glMaterialfv(GL_FRONT, GL_AMBIENT, material_jade_ambient);
	glMaterialfv(GL_FRONT, GL_DIFFUSE, material_jade_diffuse);
	glMaterialfv(GL_FRONT, GL_SPECULAR, material_jade_specular);
	glMaterialf(GL_FRONT, GL_SHININESS, material_jade_shininess);
	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
	quadric = gluNewQuadric();
	gluSphere(quadric, 0.75, 30, 30);
	glPopMatrix();
	

	//13
	glPushMatrix();
	glTranslatef(-6.0f, 1.0f, -14.0f);
	glMaterialfv(GL_FRONT, GL_AMBIENT, material_obsidian_ambient);
	glMaterialfv(GL_FRONT, GL_DIFFUSE, material_obsidian_diffuse);
	glMaterialfv(GL_FRONT, GL_SPECULAR, material_obsidian_specular);
	glMaterialf(GL_FRONT, GL_SHININESS, material_obsidian_shininess);
	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
	quadric = gluNewQuadric();
	gluSphere(quadric, 0.75, 30, 30);
	glPopMatrix();


	//14
	glPushMatrix();
	glTranslatef(-6.0f, -1.0f, -14.0f);
	glMaterialfv(GL_FRONT, GL_AMBIENT, material_pearl_ambient);
	glMaterialfv(GL_FRONT, GL_DIFFUSE, material_pearl_diffuse);
	glMaterialfv(GL_FRONT, GL_SPECULAR, material_pearl_specular);
	glMaterialf(GL_FRONT, GL_SHININESS, material_pearl_shininess);
	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
	quadric = gluNewQuadric();
	gluSphere(quadric, 0.75, 30, 30);
	glPopMatrix();
	

	//15
	glPushMatrix();
	glTranslatef(-6.0f, -3.0f, -14.0f);
	glMaterialfv(GL_FRONT, GL_AMBIENT, material_ruby_ambient);
	glMaterialfv(GL_FRONT, GL_DIFFUSE, material_ruby_diffuse);
	glMaterialfv(GL_FRONT, GL_SPECULAR, material_ruby_specular);
	glMaterialf(GL_FRONT, GL_SHININESS, material_ruby_shininess);
	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
	quadric = gluNewQuadric();
	gluSphere(quadric, 0.75, 30, 30);
	glPopMatrix();
	

	//16
	glPushMatrix();
	glTranslatef(-6.0f, -5.0f, -14.0f);
	glMaterialfv(GL_FRONT, GL_AMBIENT, material_turquoise_ambient);
	glMaterialfv(GL_FRONT, GL_DIFFUSE, material_turquoise_diffuse);
	glMaterialfv(GL_FRONT, GL_SPECULAR, material_turquoise_specular);
	glMaterialf(GL_FRONT, GL_SHININESS, material_turquoise_shininess);
	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
	quadric = gluNewQuadric();
	gluSphere(quadric, 0.75, 30, 30);
	glPopMatrix();
	
	
	//21
	glPushMatrix();
	glTranslatef(-2.0f, 5.0f, -14.0f);
	glMaterialfv(GL_FRONT, GL_AMBIENT, material_brass_ambient);
	glMaterialfv(GL_FRONT, GL_DIFFUSE, material_brass_diffuse);
	glMaterialfv(GL_FRONT, GL_SPECULAR, material_brass_specular);
	glMaterialf(GL_FRONT, GL_SHININESS, material_brass_shininess);
	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
	quadric = gluNewQuadric();
	gluSphere(quadric, 0.75, 30, 30);
	glPopMatrix();

	//22
	glPushMatrix();
	glTranslatef(-2.0f, 3.0f, -14.0f);
	glMaterialfv(GL_FRONT, GL_AMBIENT, material_bronze_ambient);
	glMaterialfv(GL_FRONT, GL_DIFFUSE, material_bronze_diffuse);
	glMaterialfv(GL_FRONT, GL_SPECULAR, material_bronze_specular);
	glMaterialf(GL_FRONT, GL_SHININESS, material_bronze_shininess);
	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
	quadric = gluNewQuadric();
	gluSphere(quadric, 0.75, 30, 30);
	glPopMatrix();

	//23
	glPushMatrix();
	glTranslatef(-2.0f, 1.0f, -14.0f);
	glMaterialfv(GL_FRONT, GL_AMBIENT, material_chrome_ambient);
	glMaterialfv(GL_FRONT, GL_DIFFUSE, material_chrome_diffuse);
	glMaterialfv(GL_FRONT, GL_SPECULAR, material_chrome_specular);
	glMaterialf(GL_FRONT, GL_SHININESS, material_chrome_shininess);
	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
	quadric = gluNewQuadric();
	gluSphere(quadric, 0.75, 30, 30);
	glPopMatrix();

	//24
	glPushMatrix();
	glTranslatef(-2.0f, -1.0f, -14.0f);
	glMaterialfv(GL_FRONT, GL_AMBIENT, material_copper_ambient);
	glMaterialfv(GL_FRONT, GL_DIFFUSE, material_copper_diffuse);
	glMaterialfv(GL_FRONT, GL_SPECULAR, material_copper_specular);
	glMaterialf(GL_FRONT, GL_SHININESS, material_copper_shininess);
	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
	quadric = gluNewQuadric();
	gluSphere(quadric, 0.75, 30, 30);
	glPopMatrix();

	//25
	glPushMatrix();
	glTranslatef(-2.0f, -3.0f, -14.0f);
	glMaterialfv(GL_FRONT, GL_AMBIENT, material_gold_ambient);
	glMaterialfv(GL_FRONT, GL_DIFFUSE, material_gold_diffuse);
	glMaterialfv(GL_FRONT, GL_SPECULAR, material_gold_specular);
	glMaterialf(GL_FRONT, GL_SHININESS, material_gold_shininess);
	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
	quadric = gluNewQuadric();
	gluSphere(quadric, 0.75, 30, 30);
	glPopMatrix();

	//26
	glPushMatrix();
	glTranslatef(-2.0f, -5.0f, -14.0f);
	glMaterialfv(GL_FRONT, GL_AMBIENT, material_silver_ambient);
	glMaterialfv(GL_FRONT, GL_DIFFUSE, material_silver_diffuse);
	glMaterialfv(GL_FRONT, GL_SPECULAR, material_silver_specular);
	glMaterialf(GL_FRONT, GL_SHININESS, material_silver_shininess);
	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
	quadric = gluNewQuadric();
	gluSphere(quadric, 0.75, 30, 30);
	glPopMatrix();

	//31
	glPushMatrix();
	glTranslatef(2.0f, 5.0f, -14.0f);
	glMaterialfv(GL_FRONT, GL_AMBIENT, material_black_ambient);
	glMaterialfv(GL_FRONT, GL_DIFFUSE, material_black_diffuse);
	glMaterialfv(GL_FRONT, GL_SPECULAR, material_black_specular);
	glMaterialf(GL_FRONT, GL_SHININESS, material_black_shininess);
	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
	quadric = gluNewQuadric();
	gluSphere(quadric, 0.75, 30, 30);
	glPopMatrix();

	//32
	glPushMatrix();
	glTranslatef(2.0f, 3.0f, -14.0f);
	glMaterialfv(GL_FRONT, GL_AMBIENT, material_cyan_ambient);
	glMaterialfv(GL_FRONT, GL_DIFFUSE, material_cyan_diffuse);
	glMaterialfv(GL_FRONT, GL_SPECULAR, material_cyan_specular);
	glMaterialf(GL_FRONT, GL_SHININESS, material_cyan_shininess);
	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
	quadric = gluNewQuadric();
	gluSphere(quadric, 0.75, 30, 30);
	glPopMatrix();

	//33
	glPushMatrix();
	glTranslatef(2.0f, 1.0f, -14.0f);
	glMaterialfv(GL_FRONT, GL_AMBIENT, material_green_ambient);
	glMaterialfv(GL_FRONT, GL_DIFFUSE, material_green_diffuse);
	glMaterialfv(GL_FRONT, GL_SPECULAR, material_green_specular);
	glMaterialf(GL_FRONT, GL_SHININESS, material_green_shininess);
	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
	quadric = gluNewQuadric();
	gluSphere(quadric, 0.75, 30, 30);
	glPopMatrix();

	//34
	glPushMatrix();
	glTranslatef(2.0f, -1.0f, -14.0f);
	glMaterialfv(GL_FRONT, GL_AMBIENT, material_red_ambient);
	glMaterialfv(GL_FRONT, GL_DIFFUSE, material_red_diffuse);
	glMaterialfv(GL_FRONT, GL_SPECULAR, material_red_specular);
	glMaterialf(GL_FRONT, GL_SHININESS, material_red_shininess);
	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
	quadric = gluNewQuadric();
	gluSphere(quadric, 0.75, 30, 30);
	glPopMatrix();

	//35
	glPushMatrix();
	glTranslatef(2.0f, -3.0f, -14.0f);
	glMaterialfv(GL_FRONT, GL_AMBIENT, material_white_ambient);
	glMaterialfv(GL_FRONT, GL_DIFFUSE, material_white_diffuse);
	glMaterialfv(GL_FRONT, GL_SPECULAR, material_white_specular);
	glMaterialf(GL_FRONT, GL_SHININESS, material_white_shininess);
	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
	quadric = gluNewQuadric();
	gluSphere(quadric, 0.75, 30, 30);
	glPopMatrix();

	//36
	glPushMatrix();
	glTranslatef(2.0f, -5.0f, -14.0f);
	glMaterialfv(GL_FRONT, GL_AMBIENT, material_yellow_ambient);
	glMaterialfv(GL_FRONT, GL_DIFFUSE, material_yellow_diffuse);
	glMaterialfv(GL_FRONT, GL_SPECULAR, material_yellow_specular);
	glMaterialf(GL_FRONT, GL_SHININESS, material_yellow_shininess);
	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
	quadric = gluNewQuadric();
	gluSphere(quadric, 0.75, 30, 30);
	glPopMatrix();

	//41
	glPushMatrix();
	glTranslatef(6.0f, 5.0f, -14.0f);
	glMaterialfv(GL_FRONT, GL_AMBIENT, material_black2_ambient);
	glMaterialfv(GL_FRONT, GL_DIFFUSE, material_black2_diffuse);
	glMaterialfv(GL_FRONT, GL_SPECULAR, material_black2_specular);
	glMaterialf(GL_FRONT, GL_SHININESS, material_black2_shininess);
	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
	quadric = gluNewQuadric();
	gluSphere(quadric, 0.75, 30, 30);
	glPopMatrix();

	//42
	glPushMatrix();
	glTranslatef(6.0f, 3.0f, -14.0f);
	glMaterialfv(GL_FRONT, GL_AMBIENT, material_cyan2_ambient);
	glMaterialfv(GL_FRONT, GL_DIFFUSE, material_cyan2_diffuse);
	glMaterialfv(GL_FRONT, GL_SPECULAR, material_cyan2_specular);
	glMaterialf(GL_FRONT, GL_SHININESS, material_cyan2_shininess);
	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
	quadric = gluNewQuadric();
	gluSphere(quadric, 0.75, 30, 30);
	glPopMatrix();

	//43
	glPushMatrix();
	glTranslatef(6.0f, 1.0f, -14.0f);
	glMaterialfv(GL_FRONT, GL_AMBIENT, material_green2_ambient);
	glMaterialfv(GL_FRONT, GL_DIFFUSE, material_green2_diffuse);
	glMaterialfv(GL_FRONT, GL_SPECULAR, material_green2_specular);
	glMaterialf(GL_FRONT, GL_SHININESS, material_green2_shininess);
	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
	quadric = gluNewQuadric();
	gluSphere(quadric, 0.75, 30, 30);
	glPopMatrix();

	//44
	glPushMatrix();
	glTranslatef(6.0f, -1.0f, -14.0f);
	glMaterialfv(GL_FRONT, GL_AMBIENT, material_red2_ambient);
	glMaterialfv(GL_FRONT, GL_DIFFUSE, material_red2_diffuse);
	glMaterialfv(GL_FRONT, GL_SPECULAR, material_red2_specular);
	glMaterialf(GL_FRONT, GL_SHININESS, material_red2_shininess);
	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
	quadric = gluNewQuadric();
	gluSphere(quadric, 0.75, 30, 30);
	glPopMatrix();

	//45
	glPushMatrix();
	glTranslatef(6.0f, -3.0f, -14.0f);
	glMaterialfv(GL_FRONT, GL_AMBIENT, material_white2_ambient);
	glMaterialfv(GL_FRONT, GL_DIFFUSE, material_white2_diffuse);
	glMaterialfv(GL_FRONT, GL_SPECULAR, material_white2_specular);
	glMaterialf(GL_FRONT, GL_SHININESS, material_white2_shininess);
	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
	quadric = gluNewQuadric();
	gluSphere(quadric, 0.75, 30, 30);
	glPopMatrix();

	//46
	glPushMatrix();
	glTranslatef(6.0f, -5.0f, -14.0f);
	glMaterialfv(GL_FRONT, GL_AMBIENT, material_yellow2_ambient);
	glMaterialfv(GL_FRONT, GL_DIFFUSE, material_yellow2_diffuse);
	glMaterialfv(GL_FRONT, GL_SPECULAR, material_yellow2_specular);
	glMaterialf(GL_FRONT, GL_SHININESS, material_yellow2_shininess);
	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
	quadric = gluNewQuadric();
	gluSphere(quadric, 0.75, 30, 30);
	glPopMatrix();
	glPopMatrix();
	SwapBuffers(ghdc);
}

void Resize(int width, int height)
{
	// Code
	if (height == 0)
		height = 1;
	glViewport(0, 0, (GLsizei)width, (GLsizei)height);
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	gluPerspective(45.0f, (GLfloat)width / (GLfloat)height, 0.1f, 100.0f);
}

void Uninitialize(void)
{
	// Uninitialization code

	if (gbFullScreen == true)
	{
		dwStyle = GetWindowLong(ghwnd, GWL_STYLE);
		SetWindowLong(ghwnd, GWL_STYLE, dwStyle | WS_OVERLAPPEDWINDOW);
		SetWindowPlacement(ghwnd, &wpPrev);
		SetWindowPos(ghwnd, HWND_TOP, 0, 0, 0, 0, SWP_NOMOVE | SWP_NOSIZE | SWP_NOZORDER | SWP_NOOWNERZORDER | SWP_FRAMECHANGED);
		ShowCursor(TRUE);
	}

	wglMakeCurrent(NULL, NULL);

	wglDeleteContext(ghrc);
	ghrc = NULL;

	ReleaseDC(ghwnd, ghdc);
	ghdc = NULL;

	DestroyWindow(ghwnd);
	ghwnd = NULL;
}

void Update() {
	gfLightAngle = (gfLightAngle + 1.0f);
	if (gfLightAngle >= 360.0f)
		gfLightAngle = 0.0f;
}
