// Headers
#import <Foundation/Foundation.h>
#import <Cocoa/Cocoa.h>
#import <Quartzcore/CVDisplayLink.h>
#import <OpenGL/gl3.h>
#import <OpenGL/gl3ext.h>
#import "vmath.h"

enum{
	VDG_ATTRIBUTE_VERTEX = 0,
    VDG_ATTRIBUTE_COLOR,
    VDG_ATTRIBUTE_NORMAL,
    VDG_ATTRIBUTE_TEXTURE0,
};

CVReturn MyDisplayLinkCallback(CVDisplayLinkRef, const CVTimeStamp *, const CVTimeStamp *, CVOptionFlags, CVOptionFlags *, void *);

// Global variables
FILE *gpFile = NULL;

// Interface declarations
@interface AppDelegate : NSObject <NSApplicationDelegate, NSWindowDelegate>
@end

@interface GLView : NSOpenGLView
@end

// Entry point function
int main(int argc, const char * argv[]){
// Code
NSAutoreleasePool *pPool = [[NSAutoreleasePool alloc]init];
NSApp = [NSApplication sharedApplication];
[NSApp setDelegate:[[AppDelegate alloc]init]];

[NSApp run];

[pPool release];

return (0);    
}

// interface implementation
@implementation AppDelegate{
@private
NSWindow *window;
GLView *glView;
}

- (void)applicationDidFinishLaunching:(NSNotification *)aNotification{
// Code
// Log file
NSBundle *mainBundle = [NSBundle mainBundle];
NSString *appDirName = [mainBundle bundlePath];
NSString *parentDirPath = [appDirName stringByDeletingLastPathComponent];
NSString *logFileNameWithPath = [NSString stringWithFormat:@"%@/Log.txt", parentDirPath];
const char *pszLogFileNameWithPath = [logFileNameWithPath cStringUsingEncoding:NSASCIIStringEncoding];
gpFile = fopen(pszLogFileNameWithPath, "w");
if(gpFile == NULL){
	printf("Can not create log file.\n");
	[self release];
	[NSApp terminate:self];
}
fprintf(gpFile, "Program started successfully");

// Window
NSRect win_rect;
win_rect = NSMakeRect(0.0, 0.0, 800.0, 600.0);

// Create simple window
   window=[[NSWindow alloc] initWithContentRect:win_rect
                                       styleMask:NSWindowStyleMaskTitled | NSWindowStyleMaskClosable | NSWindowStyleMaskMiniaturizable | NSWindowStyleMaskResizable
                                         backing:NSBackingStoreBuffered
                                           defer:NO];
[window setTitle:@"macos OpenGL Window"];
[window center];

glView=[[GLView alloc]initWithFrame:win_rect];

[window setContentView:glView];
[window setDelegate:self];
[window makeKeyAndOrderFront:self];
}

- (void)applicationWillTerminate:(NSNotification *)notification{
// code
fprintf(gpFile, "Program terminated successfully.\n");
if(gpFile){
	fclose(gpFile);
	gpFile = NULL;
}
}

- (void)windowWillClose:(NSNotification *)notification{
[NSApp terminate:self];
}

- (void)dealloc{
[glView release];
[window release];
[super dealloc];
}
@end

@implementation GLView{
@private
	CVDisplayLinkRef displayLink;
	GLuint gVertexShaderObject;
	GLuint gFragmentShaderObject;
	GLuint gShaderProgramObject;

	GLuint vao_triangle;
	GLuint vao_square;
	GLuint vbo;
	GLuint gMVPUniform;

	vmath::mat4 perspectiveProjectionMatrix;
}

- (id)initWithFrame:(NSRect)frame;{
self=[super initWithFrame:frame];

if(self){
		[[self window]setContentView:self];

		NSOpenGLPixelFormatAttribute attrs[]={
			NSOpenGLPFAOpenGLProfile,
			NSOpenGLProfileVersion4_1Core,
			NSOpenGLPFAScreenMask, CGDisplayIDToOpenGLDisplayMask(kCGDirectMainDisplay),
			NSOpenGLPFANoRecovery,
			NSOpenGLPFAAccelerated,
			NSOpenGLPFAColorSize, 24,
			NSOpenGLPFADepthSize, 24,
			NSOpenGLPFAAlphaSize, 8,
			NSOpenGLPFADoubleBuffer,
			0};

		NSOpenGLPixelFormat *pixelFormat=[[[NSOpenGLPixelFormat alloc]initWithAttributes:attrs] autorelease];

		if(pixelFormat == nil){
			fprintf(gpFile, "No Valid OpenGL Pixel is available. Exiting...");
			[self release];
			[NSApp terminate:self];
		}

		 NSOpenGLContext *glContext=[[[NSOpenGLContext alloc]initWithFormat:pixelFormat shareContext:nil]autorelease];
		[self setPixelFormat:pixelFormat];
		[self setOpenGLContext:glContext];
	}
	return (self);
}

-(CVReturn)getFrameForTime:(const CVTimeStamp *)pOutputTime{
	NSAutoreleasePool *pool = [[NSAutoreleasePool alloc]init];
	[self drawView];
	[pool release];
	return (kCVReturnSuccess);
}

-(void)prepareOpenGL{
	fprintf(gpFile, "OpenGL version: %s\n", glGetString(GL_VERSION));
	fprintf(gpFile, "GLSL verison: %s\n", glGetString(GL_SHADING_LANGUAGE_VERSION));

	[[self openGLContext]makeCurrentContext];

	GLint swapInt=1;
	[[self openGLContext]setValues:&swapInt forParameter:NSOpenGLCPSwapInterval];

	// Create shader
	gVertexShaderObject = glCreateShader(GL_VERTEX_SHADER);

	// Provide source code to shader
	const GLchar *vertexShaderSourceCode =
		"#version 410" \
		"\n" \
		"in vec4 vPosition;" \
		"uniform mat4 u_mvp_matrix;" \
		"void main(void)" \
		"{" \
		"gl_Position = u_mvp_matrix * vPosition;"
		"}";

	glShaderSource(gVertexShaderObject, 1, (const char **)&vertexShaderSourceCode, NULL);

	// Compile shader
	glCompileShader(gVertexShaderObject);
	GLint iInfoLogLength = 0;
	GLint iShaderCompiledStatus = 0;
	char *szInfoLog = NULL;
	glGetShaderiv(gVertexShaderObject, GL_COMPILE_STATUS, &iShaderCompiledStatus);

	if (iShaderCompiledStatus == GL_FALSE) {
		glGetShaderiv(gVertexShaderObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
		if (iInfoLogLength > 0) {
			szInfoLog = (char *)malloc(sizeof(iInfoLogLength));
			if (szInfoLog != NULL) {
				GLsizei written;
				glGetShaderInfoLog(gVertexShaderObject, iInfoLogLength, &written, szInfoLog);
				fprintf(gpFile, "Vertex shader compilation log: %s\n", szInfoLog);
				free(szInfoLog);
				[self release];
                [NSApp terminate:self];
			}
		}
	}
	// ** fragment shader***
	gFragmentShaderObject = glCreateShader(GL_FRAGMENT_SHADER);

	// Provide source code to shader
	const GLchar *fragmentShaderSoureCode =
		"#version 410" \
		"\n" \
		"out vec4 FragColor;" \
		"void main(void)" \
		"{" \
		"FragColor = vec4(1.0, 1.0, 1.0, 1.0);"
		"}";

	glShaderSource(gFragmentShaderObject, 1, (const GLchar **)&fragmentShaderSoureCode, NULL);
	// compile shader
	glCompileShader(gFragmentShaderObject);

	if (iShaderCompiledStatus == GL_FALSE) {
		glGetShaderiv(gFragmentShaderObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
		if (iInfoLogLength > 0) {
			szInfoLog = (char *)malloc(sizeof(iInfoLogLength));
			if (szInfoLog != NULL) {
				GLsizei written;
				glGetShaderInfoLog(gFragmentShaderObject, iInfoLogLength, &written, szInfoLog);
				fprintf(gpFile, "Fragment shader compilation log: %s\n", szInfoLog);
				free(szInfoLog);
				[self release];
                [NSApp terminate:self];
			}
		}
	}


	// *** Shader program **
	gShaderProgramObject = glCreateProgram();

	// Attach vertex shader to shader program
	glAttachShader(gShaderProgramObject, gVertexShaderObject);

	// Attach fragment shader to shader program
	glAttachShader(gShaderProgramObject, gFragmentShaderObject);
	glLinkProgram(gShaderProgramObject);
	GLint iShaderProgramLinkStatus = 0;
	glGetProgramiv(gShaderProgramObject, GL_LINK_STATUS, &iShaderProgramLinkStatus);
	if (iShaderProgramLinkStatus == GL_FALSE) {
		glGetProgramiv(gVertexShaderObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
		if (iInfoLogLength > 0) {
			szInfoLog = (char *)malloc(sizeof(iInfoLogLength));
			if (szInfoLog != NULL) {
				GLsizei written;
				glGetProgramInfoLog(gShaderProgramObject, iInfoLogLength, &written, szInfoLog);
				fprintf(gpFile, "Shader program link log: %s\n", szInfoLog);
				free(szInfoLog);
				[self release];
                [NSApp terminate:self];
			}
		}
	}

	gMVPUniform = glGetUniformLocation(gShaderProgramObject, "u_mvp_matrix");

	// *** vertices, colors, shader attribute, vbo, vao initialization ***
	const GLfloat triangleVertices[] = {
		0.0f, 1.0f, 0.0f, //apex
		-1.0f, -1.0f, 0.0f, // left bottom
		1.0f, -1.0f, 0.0f // right bottom
	};

	// ** square vertices
	const GLfloat squareVertices[] = {
		1.0f, 1.0f, 0.0f, // top-right vertex
		-1.0f, 1.0f, 0.0f, // top-left vertex
		-1.0f, -1.0f, 0.0f, // bottom-left vertex
		1.0f, -1.0f, 0.0f, // bottom-right vertex
	};
	glGenVertexArrays(1, &vao_triangle);
	glBindVertexArray(vao_triangle);

	glGenBuffers(1, &vbo);
	glBindBuffer(GL_ARRAY_BUFFER, vbo);
	glBufferData(GL_ARRAY_BUFFER, sizeof(triangleVertices), triangleVertices, GL_STATIC_DRAW);
	glVertexAttribPointer(VDG_ATTRIBUTE_VERTEX, 3, GL_FLOAT, GL_FALSE, 0, NULL);
	glEnableVertexAttribArray(VDG_ATTRIBUTE_VERTEX);

	glBindBuffer(GL_ARRAY_BUFFER, 0);
	glBindVertexArray(0);

	glGenVertexArrays(1, &vao_square);
	glBindVertexArray(vao_square);

	glGenBuffers(1, &vbo);
	glBindBuffer(GL_ARRAY_BUFFER, vbo);
	glBufferData(GL_ARRAY_BUFFER, sizeof(squareVertices), squareVertices, GL_STATIC_DRAW);
	glVertexAttribPointer(VDG_ATTRIBUTE_VERTEX, 3, GL_FLOAT, GL_FALSE, 0, NULL);
	glEnableVertexAttribArray(VDG_ATTRIBUTE_VERTEX);

	glBindBuffer(GL_ARRAY_BUFFER, 0);
	glBindVertexArray(0);


	glClearDepth(1.0f);
	glEnable(GL_DEPTH_TEST);
	glDepthFunc(GL_LEQUAL);
	glEnable(GL_CULL_FACE);

	glClearColor(0.0f, 0.0f, 0.0f, 0.0f);

	perspectiveProjectionMatrix = vmath::mat4::identity();
	CVDisplayLinkCreateWithActiveCGDisplays(&displayLink);
    CVDisplayLinkSetOutputCallback(displayLink,&MyDisplayLinkCallback,self);
    CGLContextObj cglContext=(CGLContextObj)[[self openGLContext]CGLContextObj];
    CGLPixelFormatObj cglPixelFormat=(CGLPixelFormatObj)[[self pixelFormat]CGLPixelFormatObj];
    CVDisplayLinkSetCurrentCGDisplayFromOpenGLContext(displayLink,cglContext,cglPixelFormat);
    CVDisplayLinkStart(displayLink);
}

-(void)reshape{
	CGLLockContext((CGLContextObj) [[self openGLContext]CGLContextObj]);

	NSRect rect = [self bounds];
	GLfloat width = rect.size.width;
	GLfloat height = rect.size.height;

	if(height == 0)
		height = 1;
	glViewport(0,0,(GLsizei)width,(GLsizei)height);
	
    perspectiveProjectionMatrix = vmath::perspective(45.0f, (GLfloat)width/(GLfloat)height, 0.1f, 100.0f);
	CGLUnlockContext((CGLContextObj) [[self openGLContext] CGLContextObj]);
}

-(void)drawRect:(NSRect)dirtyRect{
	[self drawView];
}

-(void)drawView{
	[[self openGLContext]makeCurrentContext];

	CGLLockContext((CGLContextObj) [[self openGLContext]CGLContextObj]);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	// start using OpenGL program object
    glUseProgram(gShaderProgramObject);
    
    // OpenGL Drawing
    // set modelview & modelviewprojection matrices to identity
    vmath::mat4 modelViewMatrix = vmath::mat4::identity();
    vmath::mat4 modelViewProjectionMatrix = vmath::mat4::identity();
    
    // multiply the modelview and orthographic matrix to get modelviewprojection matrix
    modelViewMatrix = vmath::translate(-1.5f, 0.0f, -6.0f);
    modelViewProjectionMatrix = perspectiveProjectionMatrix * modelViewMatrix; // ORDER IS IMPORTANT
    
    // pass above modelviewprojection matrix to the vertex shader in 'u_mvp_matrix' shader variable
    // whose position value we already calculated in initWithFrame() by using glGetUniformLocation()
    glUniformMatrix4fv(gMVPUniform, 1, GL_FALSE, modelViewProjectionMatrix);
    
    // *** bind vao ***
    glBindVertexArray(vao_triangle);
    
    // *** draw, either by glDrawTriangles() or glDrawArrays() or glDrawElements()
    glDrawArrays(GL_TRIANGLES, 0, 3); // 3 (each with its x,y,z ) vertices in triangleVertices array
    
    // *** unbind vao ***
    glBindVertexArray(0);

    modelViewMatrix = vmath::mat4::identity();
	modelViewProjectionMatrix = vmath::mat4::identity();

	// Multiply modelview and perspective matrix to get modelviewprojection matrix
	modelViewMatrix = vmath::translate(1.5f, 0.0f, -6.0f);
	modelViewProjectionMatrix = perspectiveProjectionMatrix * modelViewMatrix;

	// Pass above modelviewprojectionmatrix to vertex shader in 'u_mvp_matrix' shader variable whose position value we
	// already calculated in initWithFrame() by using glGetUniformLocation()
	glUniformMatrix4fv(gMVPUniform, 1, GL_FALSE, modelViewProjectionMatrix);

	// Bind vao of square
	glBindVertexArray(vao_square);

	// Draw either by glDrawTriangles or glDrawArrays or glDrawElements()
	glDrawArrays(GL_TRIANGLE_FAN, 0, 4);

	// Unbind vao
	glBindVertexArray(0);
    
    // stop using OpenGL program object
    glUseProgram(0);
	CGLFlushDrawable((CGLContextObj) [[self openGLContext]CGLContextObj]);
	CGLUnlockContext((CGLContextObj) [[self openGLContext]CGLContextObj]);
}

-(BOOL)acceptsFirstResponder{
	[[self window]makeFirstResponder:self];
	return(YES);
}

-(void)keyDown:(NSEvent *)theEvent{
	int key = (int)[[theEvent characters]characterAtIndex:0];
	switch(key){
		case 27:
		[self release];
		[NSApp terminate:self];
		break;
		case 'F':
		case 'f':
		[[self window]toggleFullScreen:self];
		break;
		default:
		break;
	}
}


-(void)mouseDown:(NSEvent *)theEvent{

}

-(void)mouseDragged:(NSEvent *)theEvent{

}

-(void)rightMouseDown:(NSEvent *)theEvent{

}

- (void) dealloc
{	
	if (vao_triangle)
    {
        glDeleteVertexArrays(1, &vao_triangle);
        vao_triangle = 0;
    }

    if (vao_square)
    {
        glDeleteVertexArrays(1, &vao_square);
        vao_square = 0;
    }
    
    // destroy vbo
    if (vbo)
    {
        glDeleteBuffers(1, &vbo);
        vbo = 0;
    }
    
    // detach vertex shader from shader program object
    glDetachShader(gShaderProgramObject, gVertexShaderObject);
    // detach fragment  shader from shader program object
    glDetachShader(gShaderProgramObject, gFragmentShaderObject);
    
    // delete vertex shader object
    glDeleteShader(gVertexShaderObject);
    gVertexShaderObject = 0;
    // delete fragment shader object
    glDeleteShader(gFragmentShaderObject);
    gFragmentShaderObject = 0;
    
    // delete shader program object
    glDeleteProgram(gShaderProgramObject);
    gShaderProgramObject = 0;
    CVDisplayLinkStop(displayLink);
    CVDisplayLinkRelease(displayLink);
    [super dealloc];
}

@end

CVReturn MyDisplayLinkCallback(CVDisplayLinkRef displayLink,const CVTimeStamp *pNow,const CVTimeStamp *pOutputTime,CVOptionFlags flagsIn,
                               CVOptionFlags *pFlagsOut,void *pDisplayLinkContext){
    CVReturn result=[(GLView *)pDisplayLinkContext getFrameForTime:pOutputTime];
    return(result);
}