// Headers
#import <Foundation/Foundation.h>
#import <Cocoa/Cocoa.h>
#import <Quartzcore/CVDisplayLink.h>
#import <OpenGL/gl3.h>
#import <OpenGL/gl3ext.h>
#import "vmath.h"
#import "Sphere.h"

enum{
	VDG_ATTRIBUTE_VERTEX = 0,
    VDG_ATTRIBUTE_COLOR,
    VDG_ATTRIBUTE_NORMAL,
    VDG_ATTRIBUTE_TEXTURE0,
};

CVReturn MyDisplayLinkCallback(CVDisplayLinkRef, const CVTimeStamp *, const CVTimeStamp *, CVOptionFlags, CVOptionFlags *, void *);

// Global variables
FILE *gpFile = NULL;

GLfloat lightAmbient[] = { 0.0f,0.0f,0.0f,1.0f };
GLfloat lightDiffuse[] = { 1.0f,1.0f,1.0f,1.0f };
GLfloat lightSpecular[] = { 1.0f,1.0f,1.0f,1.0f };
GLfloat lightPosition[] = { 100.0f,100.0f,100.0f,1.0f };

GLfloat material_ambient[] = { 0.0f,0.0f,0.0f,1.0f };
GLfloat material_diffuse[] = { 1.0f,1.0f,1.0f,1.0f };
GLfloat material_specular[] = { 1.0f,1.0f,1.0f,1.0f };
GLfloat material_shininess = 50.0f;
int gbPerVertexMode = 1;
// Interface declarations
@interface AppDelegate : NSObject <NSApplicationDelegate, NSWindowDelegate>
@end

@interface GLView : NSOpenGLView
@end

// Entry point function
int main(int argc, const char * argv[]){
// Code
NSAutoreleasePool *pPool = [[NSAutoreleasePool alloc]init];
NSApp = [NSApplication sharedApplication];
[NSApp setDelegate:[[AppDelegate alloc]init]];

[NSApp run];

[pPool release];

return (0);    
}

// interface implementation
@implementation AppDelegate{
@private
NSWindow *window;
GLView *glView;
}

- (void)applicationDidFinishLaunching:(NSNotification *)aNotification{
// Code
// Log file
NSBundle *mainBundle = [NSBundle mainBundle];
NSString *appDirName = [mainBundle bundlePath];
NSString *parentDirPath = [appDirName stringByDeletingLastPathComponent];
NSString *logFileNameWithPath = [NSString stringWithFormat:@"%@/Log.txt", parentDirPath];
const char *pszLogFileNameWithPath = [logFileNameWithPath cStringUsingEncoding:NSASCIIStringEncoding];
gpFile = fopen(pszLogFileNameWithPath, "w");
if(gpFile == NULL){
	printf("Can not create log file.\n");
	[self release];
	[NSApp terminate:self];
}
fprintf(gpFile, "Program started successfully");

// Window
NSRect win_rect;
win_rect = NSMakeRect(0.0, 0.0, 800.0, 600.0);

// Create simple window
   window=[[NSWindow alloc] initWithContentRect:win_rect
                                       styleMask:NSWindowStyleMaskTitled | NSWindowStyleMaskClosable | NSWindowStyleMaskMiniaturizable | NSWindowStyleMaskResizable
                                         backing:NSBackingStoreBuffered
                                           defer:NO];
[window setTitle:@"macos OpenGL Window"];
[window center];

glView=[[GLView alloc]initWithFrame:win_rect];

[window setContentView:glView];
[window setDelegate:self];
[window makeKeyAndOrderFront:self];
}

- (void)applicationWillTerminate:(NSNotification *)notification{
// code
fprintf(gpFile, "Program terminated successfully.\n");
if(gpFile){
	fclose(gpFile);
	gpFile = NULL;
}
}

- (void)windowWillClose:(NSNotification *)notification{
[NSApp terminate:self];
}

- (void)dealloc{
[glView release];
[window release];
[super dealloc];
}
@end

@implementation GLView{
@private
	CVDisplayLinkRef displayLink;
	

	GLuint gPerVertex_VertexShaderObject;
	GLuint gPerVertex_FragmentShaderObject;
	GLuint gPerVertexShaderProgramObject;

	GLuint gPerFragment_VertexShaderObject;
	GLuint gPerFragment_FragmentShaderObject;
	GLuint gPerFragmentShaderProgramObject;


	GLuint gVao;
	GLuint gVbo;
	GLuint gMVPUniform;
	GLuint gNumElements;
	GLuint gNumVertices;
	float sphere_vertices[1146];
	float sphere_normals[1146];
	float sphere_textures[764];
	unsigned short sphere_elements[2280];
	GLuint gVao_sphere;
	GLuint gVbo_sphere_position;
	GLuint gVbo_sphere_normal;
	GLuint gVbo_sphere_element;
	GLuint model_matrix_uniform_perVer, view_matrix_uniform_perVer, projection_matrix_uniform_perVer;
	GLuint L_KeyPressed_uniform_perVer;
	GLuint La_uniform_perVer;
	GLuint Ld_uniform_perVer;
	GLuint Ls_uniform_perVer;
	GLuint light_position_uniform_perVer;

	GLuint Ka_uniform_perVer;
	GLuint Kd_uniform_perVer;
	GLuint Ks_uniform_perVer;
	GLuint material_shininess_uniform_perVer;

	GLuint model_matrix_uniform_perFrag, view_matrix_uniform_perFrag, projection_matrix_uniform_perFrag;
	GLuint L_KeyPressed_uniform_perFrag;
	GLuint La_uniform_perFrag;
	GLuint Ld_uniform_perFrag;
	GLuint Ls_uniform_perFrag;
	GLuint light_position_uniform_perFrag;

	GLuint Ka_uniform_perFrag;
	GLuint Kd_uniform_perFrag;
	GLuint Ks_uniform_perFrag;
	GLuint material_shininess_uniform_perFrag;
	bool gbLight;
	vmath::mat4 perspectiveProjectionMatrix;
}

- (id)initWithFrame:(NSRect)frame;{
self=[super initWithFrame:frame];

if(self){
		[[self window]setContentView:self];

		NSOpenGLPixelFormatAttribute attrs[]={
			NSOpenGLPFAOpenGLProfile,
			NSOpenGLProfileVersion4_1Core,
			NSOpenGLPFAScreenMask, CGDisplayIDToOpenGLDisplayMask(kCGDirectMainDisplay),
			NSOpenGLPFANoRecovery,
			NSOpenGLPFAAccelerated,
			NSOpenGLPFAColorSize, 24,
			NSOpenGLPFADepthSize, 24,
			NSOpenGLPFAAlphaSize, 8,
			NSOpenGLPFADoubleBuffer,
			0};

		NSOpenGLPixelFormat *pixelFormat=[[[NSOpenGLPixelFormat alloc]initWithAttributes:attrs] autorelease];

		if(pixelFormat == nil){
			fprintf(gpFile, "No Valid OpenGL Pixel is available. Exiting...");
			[self release];
			[NSApp terminate:self];
		}

		 NSOpenGLContext *glContext=[[[NSOpenGLContext alloc]initWithFormat:pixelFormat shareContext:nil]autorelease];
		[self setPixelFormat:pixelFormat];
		[self setOpenGLContext:glContext];
	}
	return (self);
}

-(CVReturn)getFrameForTime:(const CVTimeStamp *)pOutputTime{
	NSAutoreleasePool *pool = [[NSAutoreleasePool alloc]init];
	[self drawView];
	[pool release];
	return (kCVReturnSuccess);
}

-(void)prepareOpenGL{
	fprintf(gpFile, "OpenGL version: %s\n", glGetString(GL_VERSION));
	fprintf(gpFile, "GLSL verison: %s\n", glGetString(GL_SHADING_LANGUAGE_VERSION));

	[[self openGLContext]makeCurrentContext];

	GLint swapInt=1;
	[[self openGLContext]setValues:&swapInt forParameter:NSOpenGLCPSwapInterval];

	// ***Per vertex VERTEX SHADER ***
	// create shader
	gPerVertex_VertexShaderObject = glCreateShader(GL_VERTEX_SHADER);

	// provide source code to shader
	const GLchar *vertexShaderSourceCode =
		"#version 410" \
		"\n" \
		"in vec4 vPosition;" \
		"in vec3 vNormal;" \
		"uniform mat4 u_model_matrix;" \
		"uniform mat4 u_view_matrix;" \
		"uniform mat4 u_projection_matrix;" \
		"uniform int u_lighting_enabled;"\
		"uniform vec3 u_La;"\
		"uniform vec3 u_Ld;"\
		"uniform vec3 u_Ls;" \
		"uniform vec4 u_light_position;"\
		"uniform vec3 u_Ka;"\
		"uniform vec3 u_Ks;"\
		"uniform vec3 u_Kd;" \
		"uniform float u_material_shininess;" \
		"out vec3 phong_ads_color;" \
		"void main(void)" \
		"{" \
		"if (u_lighting_enabled == 1)" \
		"{" \
		"vec4 eye_coordinates=u_view_matrix * u_model_matrix * vPosition;" \
		"vec3 transformed_normals=normalize(mat3(u_view_matrix * u_model_matrix) * vNormal);" \
		"vec3 light_direction = normalize(vec3(u_light_position) - eye_coordinates.xyz);" \
		"float tn_dot_ld = max(dot(transformed_normals, light_direction),0.0);" \
		"vec3 ambient = u_La * u_Ka;" \
		"vec3 diffuse = u_Ld * u_Kd * tn_dot_ld;" \
		"vec3 reflection_vector = reflect(-light_direction, transformed_normals);" \
		"vec3 viewer_vector = normalize(-eye_coordinates.xyz);" \
		"vec3 specular = u_Ls * u_Ks * pow(max(dot(reflection_vector, viewer_vector), 0.0), u_material_shininess);" \
		"phong_ads_color=ambient + diffuse + specular;" \
		"}" \
		"else" \
		"{" \
		"phong_ads_color = vec3(1.0, 1.0, 1.0);" \
		"}" \
		"gl_Position=u_projection_matrix * u_view_matrix * u_model_matrix * vPosition;" \
		"}";

	glShaderSource(gPerVertex_VertexShaderObject, 1, (const char **)&vertexShaderSourceCode, NULL);

	// Compile shader
	glCompileShader(gPerVertex_VertexShaderObject);
	GLint iInfoLogLength = 0;
	GLint iShaderCompiledStatus = 0;
	char *szInfoLog = NULL;
	glGetShaderiv(gPerVertex_VertexShaderObject, GL_COMPILE_STATUS, &iShaderCompiledStatus);

	if (iShaderCompiledStatus == GL_FALSE) {
		glGetShaderiv(gPerVertex_VertexShaderObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
		if (iInfoLogLength > 0) {
			szInfoLog = (char *)malloc(sizeof(iInfoLogLength));
			if (szInfoLog != NULL) {
				GLsizei written;
				glGetShaderInfoLog(gPerVertex_VertexShaderObject, iInfoLogLength, &written, szInfoLog);
				fprintf(gpFile, "Vertex shader compilation log: %s\n", szInfoLog);
				free(szInfoLog);
				[self release];
                [NSApp terminate:self];
			}
		}
	}
	// ** fragment shader***
	gPerVertex_FragmentShaderObject = glCreateShader(GL_FRAGMENT_SHADER);

	// provide source code to shader
	const GLchar *fragmentShaderSourceCode =
		"#version 410" \
		"\n" \
		"in vec3 phong_ads_color;" \
		"out vec4 FragColor;" \
		"void main(void)" \
		"{" \
		"FragColor = vec4(phong_ads_color, 1.0);" \
		"}";

	glShaderSource(gPerVertex_FragmentShaderObject, 1, (const GLchar **)&fragmentShaderSourceCode, NULL);
	// compile shader
	glCompileShader(gPerVertex_FragmentShaderObject);

	if (iShaderCompiledStatus == GL_FALSE) {
		glGetShaderiv(gPerVertex_FragmentShaderObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
		if (iInfoLogLength > 0) {
			szInfoLog = (char *)malloc(sizeof(iInfoLogLength));
			if (szInfoLog != NULL) {
				GLsizei written;
				glGetShaderInfoLog(gPerVertex_FragmentShaderObject, iInfoLogLength, &written, szInfoLog);
				fprintf(gpFile, "Fragment shader compilation log: %s\n", szInfoLog);
				free(szInfoLog);
				[self release];
                [NSApp terminate:self];
			}
		}
	}


	// *** Shader program **
	gPerVertexShaderProgramObject = glCreateProgram();

	// Attach vertex shader to shader program
	glAttachShader(gPerVertexShaderProgramObject, gPerVertex_VertexShaderObject);

	// Attach fragment shader to shader program
	glAttachShader(gPerVertexShaderProgramObject, gPerVertex_FragmentShaderObject);
	glBindAttribLocation(gPerVertexShaderProgramObject, VDG_ATTRIBUTE_VERTEX, "vPosition");
	glBindAttribLocation(gPerVertexShaderProgramObject, VDG_ATTRIBUTE_NORMAL, "vNormal");
	glLinkProgram(gPerVertexShaderProgramObject);
	GLint iShaderProgramLinkStatus = 0;
	glGetProgramiv(gPerVertexShaderProgramObject, GL_LINK_STATUS, &iShaderProgramLinkStatus);
	if (iShaderProgramLinkStatus == GL_FALSE) {
		glGetProgramiv(gPerVertexShaderProgramObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
		if (iInfoLogLength > 0) {
			szInfoLog = (char *)malloc(sizeof(iInfoLogLength));
			if (szInfoLog != NULL) {
				GLsizei written;
				glGetProgramInfoLog(gPerVertexShaderProgramObject, iInfoLogLength, &written, szInfoLog);
				fprintf(gpFile, "Shader program link log: %s\n", szInfoLog);
				free(szInfoLog);
				[self release];
                [NSApp terminate:self];
			}
		}
	}

	// get uniform locations
	model_matrix_uniform_perVer = glGetUniformLocation(gPerVertexShaderProgramObject, "u_model_matrix");
	view_matrix_uniform_perVer = glGetUniformLocation(gPerVertexShaderProgramObject, "u_view_matrix");
	projection_matrix_uniform_perVer = glGetUniformLocation(gPerVertexShaderProgramObject, "u_projection_matrix");
	L_KeyPressed_uniform_perVer = glGetUniformLocation(gPerVertexShaderProgramObject, "u_lighting_enabled");

	// Ambient color intensity of light
	La_uniform_perVer = glGetUniformLocation(gPerVertexShaderProgramObject, "u_La");
	// Diffuse color intensity of light
	Ld_uniform_perVer = glGetUniformLocation(gPerVertexShaderProgramObject, "u_Ld");
	// Specular color intensity of light
	Ls_uniform_perVer = glGetUniformLocation(gPerVertexShaderProgramObject, "u_Ls");

	// Position of light
	light_position_uniform_perVer = glGetUniformLocation(gPerVertexShaderProgramObject, "u_light_position");;

	// Ambient color reflective intensity of material
	Ka_uniform_perVer = glGetUniformLocation(gPerVertexShaderProgramObject, "u_Ka");
	// diffuse reflective color intensity of material
	Kd_uniform_perVer = glGetUniformLocation(gPerVertexShaderProgramObject, "u_Kd");
	// specular reflective color intensity of material
	Ks_uniform_perVer = glGetUniformLocation(gPerVertexShaderProgramObject, "u_Ks");
	// shininess of material ( value is conventionally between 1 to 200 )
	material_shininess_uniform_perVer = glGetUniformLocation(gPerVertexShaderProgramObject, "u_material_shininess");

	//********************Per Fragment****************************//
	gPerFragment_VertexShaderObject = glCreateShader(GL_VERTEX_SHADER);

	// provide source code to shader
	vertexShaderSourceCode =
		"#version 410" \
		"\n" \
		"in vec4 vPosition;" \
		"in vec3 vNormal;" \
		"uniform mat4 u_model_matrix;" \
		"uniform mat4 u_view_matrix;" \
		"uniform mat4 u_projection_matrix;" \
		"uniform vec4 u_light_position;" \
		"uniform int u_lighting_enabled;" \
		"out vec3 transformed_normals;" \
		"out vec3 light_direction;" \
		"out vec3 viewer_vector;" \
		"void main(void)" \
		"{" \
		"if(u_lighting_enabled==1)" \
		"{" \
		"vec4 eye_coordinates=u_view_matrix * u_model_matrix * vPosition;" \
		"transformed_normals=mat3(u_view_matrix * u_model_matrix) * vNormal;" \
		"light_direction = vec3(u_light_position) - eye_coordinates.xyz;" \
		"viewer_vector = -eye_coordinates.xyz;" \
		"}" \
		"gl_Position=u_projection_matrix * u_view_matrix * u_model_matrix * vPosition;" \
		"}";
	glShaderSource(gPerFragment_VertexShaderObject, 1, (const char **)&vertexShaderSourceCode, NULL);

	// Compile shader
	glCompileShader(gPerFragment_VertexShaderObject);
	iInfoLogLength = 0;
	iShaderCompiledStatus = 0;
	szInfoLog = NULL;
	glGetShaderiv(gPerFragment_VertexShaderObject, GL_COMPILE_STATUS, &iShaderCompiledStatus);

	if (iShaderCompiledStatus == GL_FALSE) {
		glGetShaderiv(gPerFragment_VertexShaderObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
		if (iInfoLogLength > 0) {
			szInfoLog = (char *)malloc(sizeof(iInfoLogLength));
			if (szInfoLog != NULL) {
				GLsizei written;
				glGetShaderInfoLog(gPerFragment_VertexShaderObject, iInfoLogLength, &written, szInfoLog);
				fprintf(gpFile, "Vertex shader compilation log: %s\n", szInfoLog);
				free(szInfoLog);
				[self release];
                [NSApp terminate:self];
			}
		}
	}

	// ** fragment shader***
	gPerFragment_FragmentShaderObject = glCreateShader(GL_FRAGMENT_SHADER);

	// Provide source code to shader
	fragmentShaderSourceCode =
		"#version 410" \
		"\n" \
		"in vec3 transformed_normals;" \
		"in vec3 light_direction;" \
		"in vec3 viewer_vector;" \
		"out vec4 FragColor;" \
		"uniform vec3 u_La;" \
		"uniform vec3 u_Ld;" \
		"uniform vec3 u_Ls;" \
		"uniform vec3 u_Ka;" \
		"uniform vec3 u_Kd;" \
		"uniform vec3 u_Ks;" \
		"uniform float u_material_shininess;" \
		"uniform int u_lighting_enabled;" \
		"void main(void)" \
		"{" \
		"vec3 phong_ads_color;" \
		"if(u_lighting_enabled==1)" \
		"{" \
		"vec3 normalized_transformed_normals=normalize(transformed_normals);" \
		"vec3 normalized_light_direction=normalize(light_direction);" \
		"vec3 normalized_viewer_vector=normalize(viewer_vector);" \
		"vec3 ambient = u_La * u_Ka;" \
		"float tn_dot_ld = max(dot(normalized_transformed_normals, normalized_light_direction),0.0);" \
		"vec3 diffuse = u_Ld * u_Kd * tn_dot_ld;" \
		"vec3 reflection_vector = reflect(-normalized_light_direction, normalized_transformed_normals);" \
		"vec3 specular = u_Ls * u_Ks * pow(max(dot(reflection_vector, normalized_viewer_vector), 0.0), u_material_shininess);" \
		"phong_ads_color=ambient + diffuse + specular;" \
		"}" \
		"else" \
		"{" \
		"phong_ads_color = vec3(1.0, 1.0, 1.0);" \
		"}" \
		"FragColor = vec4(phong_ads_color, 1.0);" \
		"}";

	glShaderSource(gPerFragment_FragmentShaderObject, 1, (const GLchar **)&fragmentShaderSourceCode, NULL);
	// compile shader
	glCompileShader(gPerFragment_FragmentShaderObject);

	if (iShaderCompiledStatus == GL_FALSE) {
		glGetShaderiv(gPerFragment_FragmentShaderObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
		if (iInfoLogLength > 0) {
			szInfoLog = (char *)malloc(sizeof(iInfoLogLength));
			if (szInfoLog != NULL) {
				GLsizei written;
				glGetShaderInfoLog(gPerFragment_FragmentShaderObject, iInfoLogLength, &written, szInfoLog);
				fprintf(gpFile, "Fragment shader compilation log: %s\n", szInfoLog);
				free(szInfoLog);
				[self release];
                [NSApp terminate:self];
			}
		}
	}


	// *** Shader program **
	gPerFragmentShaderProgramObject = glCreateProgram();

	// Attach vertex shader to shader program
	glAttachShader(gPerFragmentShaderProgramObject, gPerFragment_VertexShaderObject);

	// Attach fragment shader to shader program
	glAttachShader(gPerFragmentShaderProgramObject, gPerFragment_FragmentShaderObject);
	glBindAttribLocation(gPerFragmentShaderProgramObject, VDG_ATTRIBUTE_VERTEX, "vPosition");
	glBindAttribLocation(gPerFragmentShaderProgramObject, VDG_ATTRIBUTE_NORMAL, "vNormal");
	glLinkProgram(gPerFragmentShaderProgramObject);
	iShaderProgramLinkStatus = 0;
	glGetProgramiv(gPerFragmentShaderProgramObject, GL_LINK_STATUS, &iShaderProgramLinkStatus);
	if (iShaderProgramLinkStatus == GL_FALSE) {
		glGetProgramiv(gPerFragmentShaderProgramObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
		if (iInfoLogLength > 0) {
			szInfoLog = (char *)malloc(sizeof(iInfoLogLength));
			if (szInfoLog != NULL) {
				GLsizei written;
				glGetProgramInfoLog(gPerFragmentShaderProgramObject, iInfoLogLength, &written, szInfoLog);
				fprintf(gpFile, "Shader program link log: %s\n", szInfoLog);
				free(szInfoLog);
				[self release];
                [NSApp terminate:self];
			}
		}
	}

	// get uniform locations
	model_matrix_uniform_perFrag = glGetUniformLocation(gPerFragmentShaderProgramObject, "u_model_matrix");
	view_matrix_uniform_perFrag = glGetUniformLocation(gPerFragmentShaderProgramObject, "u_view_matrix");
	projection_matrix_uniform_perFrag = glGetUniformLocation(gPerFragmentShaderProgramObject, "u_projection_matrix");
	L_KeyPressed_uniform_perFrag = glGetUniformLocation(gPerFragmentShaderProgramObject, "u_lighting_enabled");

	// Ambient color intensity of light
	La_uniform_perFrag = glGetUniformLocation(gPerFragmentShaderProgramObject, "u_La");
	// Diffuse color intensity of light
	Ld_uniform_perFrag = glGetUniformLocation(gPerFragmentShaderProgramObject, "u_Ld");
	// Specular color intensity of light
	Ls_uniform_perFrag = glGetUniformLocation(gPerFragmentShaderProgramObject, "u_Ls");

	// Position of light
	light_position_uniform_perFrag = glGetUniformLocation(gPerFragmentShaderProgramObject, "u_light_position");;

	// Ambient color reflective intensity of material
	Ka_uniform_perFrag = glGetUniformLocation(gPerFragmentShaderProgramObject, "u_Ka");
	// diffuse reflective color intensity of material
	Kd_uniform_perFrag = glGetUniformLocation(gPerFragmentShaderProgramObject, "u_Kd");
	// specular reflective color intensity of material
	Ks_uniform_perFrag = glGetUniformLocation(gPerFragmentShaderProgramObject, "u_Ks");
	// shininess of material ( value is conventionally between 1 to 200 )
	material_shininess_uniform_perFrag = glGetUniformLocation(gPerFragmentShaderProgramObject, "u_material_shininess");
	// *** vertices, colors, shader attribute, vbo, vao initialization ***
	getSphereVertexData(sphere_vertices, sphere_normals, sphere_textures, sphere_elements);

	gNumVertices = getNumberOfSphereVertices();
	gNumElements = getNumberOfSphereElements();
	glGenVertexArrays(1, &gVao_sphere);
	glBindVertexArray(gVao_sphere);

	// position vbo
	glGenBuffers(1, &gVbo_sphere_position);
	glBindBuffer(GL_ARRAY_BUFFER, gVbo_sphere_position);
	glBufferData(GL_ARRAY_BUFFER, sizeof(sphere_vertices), sphere_vertices, GL_STATIC_DRAW);

	glVertexAttribPointer(VDG_ATTRIBUTE_VERTEX, 3, GL_FLOAT, GL_FALSE, 0, NULL);

	glEnableVertexAttribArray(VDG_ATTRIBUTE_VERTEX);

	glBindBuffer(GL_ARRAY_BUFFER, 0);

	// normal vbo
	glGenBuffers(1, &gVbo_sphere_normal);
	glBindBuffer(GL_ARRAY_BUFFER, gVbo_sphere_normal);
	glBufferData(GL_ARRAY_BUFFER, sizeof(sphere_normals), sphere_normals, GL_STATIC_DRAW);

	glVertexAttribPointer(VDG_ATTRIBUTE_NORMAL, 3, GL_FLOAT, GL_FALSE, 0, NULL);

	glEnableVertexAttribArray(VDG_ATTRIBUTE_NORMAL);

	glBindBuffer(GL_ARRAY_BUFFER, 0);

	// element vbo
	glGenBuffers(1, &gVbo_sphere_element);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, gVbo_sphere_element);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(sphere_elements), sphere_elements, GL_STATIC_DRAW);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);

	glBindVertexArray(0);


	glClearDepth(1.0f);
	glEnable(GL_DEPTH_TEST);
	glDepthFunc(GL_LEQUAL);
	glEnable(GL_CULL_FACE);

	glClearColor(0.0f, 0.0f, 0.0f, 0.0f);

	perspectiveProjectionMatrix = vmath::mat4::identity();
	CVDisplayLinkCreateWithActiveCGDisplays(&displayLink);
    CVDisplayLinkSetOutputCallback(displayLink,&MyDisplayLinkCallback,self);
    CGLContextObj cglContext=(CGLContextObj)[[self openGLContext]CGLContextObj];
    CGLPixelFormatObj cglPixelFormat=(CGLPixelFormatObj)[[self pixelFormat]CGLPixelFormatObj];
    CVDisplayLinkSetCurrentCGDisplayFromOpenGLContext(displayLink,cglContext,cglPixelFormat);
    CVDisplayLinkStart(displayLink);
}

-(void)reshape{
	CGLLockContext((CGLContextObj) [[self openGLContext]CGLContextObj]);

	NSRect rect = [self bounds];
	GLfloat width = rect.size.width;
	GLfloat height = rect.size.height;

	if(height == 0)
		height = 1;
	glViewport(0,0,(GLsizei)width,(GLsizei)height);
	
    perspectiveProjectionMatrix = vmath::perspective(45.0f, (GLfloat)width/(GLfloat)height, 0.1f, 100.0f);
	CGLUnlockContext((CGLContextObj) [[self openGLContext] CGLContextObj]);
}

-(void)drawRect:(NSRect)dirtyRect{
	[self drawView];
}

-(void)drawView{
	[[self openGLContext]makeCurrentContext];

	CGLLockContext((CGLContextObj) [[self openGLContext]CGLContextObj]);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	// start using OpenGL program object
    
    
    if (gbPerVertexMode == 1) {
		glUseProgram(gPerVertexShaderProgramObject);
		if (gbLight == true)
		{

			// set 'u_lighting_enabled' uniform
			glUniform1i(L_KeyPressed_uniform_perVer, 1);

			// setting light's properties
			glUniform3fv(La_uniform_perVer, 1, lightAmbient);
			glUniform3fv(Ld_uniform_perVer, 1, lightDiffuse);
			glUniform3fv(Ls_uniform_perVer, 1, lightSpecular);
			glUniform4fv(light_position_uniform_perVer, 1, lightPosition);

			// setting material's properties
			glUniform3fv(Ka_uniform_perVer, 1, material_ambient);
			glUniform3fv(Kd_uniform_perVer, 1, material_diffuse);
			glUniform3fv(Ks_uniform_perVer, 1, material_specular);
			glUniform1f(material_shininess_uniform_perVer, material_shininess);
		}
		else
		{
			// set 'u_lighting_enabled' uniform

			glUniform1i(L_KeyPressed_uniform_perVer, 0);
		}

		// OpenGL Drawing
		// set all matrices to identity
		vmath::mat4 modelMatrix = vmath::mat4::identity();
		vmath::mat4 viewMatrix = vmath::mat4::identity();

		// apply z axis translation to go deep into the screen by -2.0,
		// so that triangle with same fullscreen co-ordinates, but due to above translation will look small
		modelMatrix = vmath::translate(0.0f, 0.0f, -2.0f);

		glUniformMatrix4fv(model_matrix_uniform_perVer, 1, GL_FALSE, modelMatrix);
		glUniformMatrix4fv(view_matrix_uniform_perVer, 1, GL_FALSE, viewMatrix);
		glUniformMatrix4fv(projection_matrix_uniform_perVer, 1, GL_FALSE, perspectiveProjectionMatrix);

		// *** bind vao ***
		glBindVertexArray(gVao_sphere);

		// *** draw, either by glDrawTriangles() or glDrawArrays() or glDrawElements()
		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, gVbo_sphere_element);
		glDrawElements(GL_TRIANGLES, gNumElements, GL_UNSIGNED_SHORT, 0);
	}
	else {
		glUseProgram(gPerFragmentShaderProgramObject);
		if (gbLight == true)
		{

			// set 'u_lighting_enabled' uniform
			glUniform1i(L_KeyPressed_uniform_perFrag, 1);

			// setting light's properties
			glUniform3fv(La_uniform_perFrag, 1, lightAmbient);
			glUniform3fv(Ld_uniform_perFrag, 1, lightDiffuse);
			glUniform3fv(Ls_uniform_perFrag, 1, lightSpecular);
			glUniform4fv(light_position_uniform_perFrag, 1, lightPosition);

			// setting material's properties
			glUniform3fv(Ka_uniform_perFrag, 1, material_ambient);
			glUniform3fv(Kd_uniform_perFrag, 1, material_diffuse);
			glUniform3fv(Ks_uniform_perFrag, 1, material_specular);
			glUniform1f(material_shininess_uniform_perFrag, material_shininess);
		}
		else
		{
			// set 'u_lighting_enabled' uniform

			glUniform1i(L_KeyPressed_uniform_perFrag, 0);
		}

		// OpenGL Drawing
		// set all matrices to identity
		vmath::mat4 modelMatrix = vmath::mat4::identity();
		vmath::mat4 viewMatrix = vmath::mat4::identity();

		// apply z axis translation to go deep into the screen by -2.0,
		// so that triangle with same fullscreen co-ordinates, but due to above translation will look small
		modelMatrix = vmath::translate(0.0f, 0.0f, -2.0f);

		glUniformMatrix4fv(model_matrix_uniform_perFrag, 1, GL_FALSE, modelMatrix);
		glUniformMatrix4fv(view_matrix_uniform_perFrag, 1, GL_FALSE, viewMatrix);
		glUniformMatrix4fv(projection_matrix_uniform_perFrag, 1, GL_FALSE, perspectiveProjectionMatrix);

		// *** bind vao ***
		glBindVertexArray(gVao_sphere);

		// *** draw, either by glDrawTriangles() or glDrawArrays() or glDrawElements()
		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, gVbo_sphere_element);
		glDrawElements(GL_TRIANGLES, gNumElements, GL_UNSIGNED_SHORT, 0);
	}

	// *** unbind vao ***
	glBindVertexArray(0);
    
    // stop using OpenGL program object
    glUseProgram(0);
	CGLFlushDrawable((CGLContextObj) [[self openGLContext]CGLContextObj]);
	CGLUnlockContext((CGLContextObj) [[self openGLContext]CGLContextObj]);
}

-(BOOL)acceptsFirstResponder{
	[[self window]makeFirstResponder:self];
	return(YES);
}

-(void)keyDown:(NSEvent *)theEvent{
	static bool bIsLKeyPressed = false;
	int key = (int)[[theEvent characters]characterAtIndex:0];
	switch(key){
		case 27:
		[[self window]toggleFullScreen:self];
		break;
		case 'q':
		case 'Q':
		[self release];
		[NSApp terminate:self];
		break;
		case 'F':
		case 'f':
		gbPerVertexMode = 0;
		break;
		case 'v':
		case 'V':
		gbPerVertexMode = 1; //PerVertex
		break;
		case 'l':
		case 'L':
			if (bIsLKeyPressed == false)
				{
					gbLight = true;
					bIsLKeyPressed = true;
				}
				else
				{
					gbLight = false;
					bIsLKeyPressed = false;
				}
				break;
		default:
		break;
	}
}


-(void)mouseDown:(NSEvent *)theEvent{

}

-(void)mouseDragged:(NSEvent *)theEvent{

}

-(void)rightMouseDown:(NSEvent *)theEvent{

}

- (void) dealloc
{	
	if (gVao_sphere)
	{
		glDeleteVertexArrays(1, &gVao_sphere);
		gVao_sphere = 0;
	}
	// destroy vbo
	if (gVbo_sphere_position)
	{
		glDeleteBuffers(1, &gVbo_sphere_position);
		gVbo_sphere_position = 0;
	}
	if (gVbo_sphere_normal)
	{
		glDeleteBuffers(1, &gVbo_sphere_normal);
		gVbo_sphere_normal = 0;
	}
	if (gVbo_sphere_element)
	{
		glDeleteBuffers(1, &gVbo_sphere_element);
		gVbo_sphere_element = 0;
	}
    
    // Detach vertex shader from shader program object
	glDetachShader(gPerVertexShaderProgramObject, gPerVertex_VertexShaderObject);

	// Detach fragment shader from shader program object
	glDetachShader(gPerVertexShaderProgramObject, gPerVertex_FragmentShaderObject);

	// Delete vertex shader object
	glDeleteShader(gPerVertex_VertexShaderObject);
	gPerVertex_VertexShaderObject = 0;

	// Delete fragment shader object
	glDeleteShader(gPerVertex_FragmentShaderObject);
	gPerVertex_FragmentShaderObject = 0;

	// Delete shader program object
	glDeleteProgram(gPerVertexShaderProgramObject);
	gPerVertexShaderProgramObject = 0;

	glDetachShader(gPerFragmentShaderProgramObject, gPerFragment_VertexShaderObject);

	// Detach fragment shader from shader program object
	glDetachShader(gPerFragmentShaderProgramObject, gPerFragment_FragmentShaderObject);

	// Delete vertex shader object
	glDeleteShader(gPerFragment_VertexShaderObject);
	gPerFragment_VertexShaderObject = 0;

	// Delete fragment shader object
	glDeleteShader(gPerFragment_FragmentShaderObject);
	gPerFragment_FragmentShaderObject = 0;

	// Delete shader program object
	glDeleteProgram(gPerFragmentShaderProgramObject);
	gPerFragmentShaderProgramObject = 0;
    CVDisplayLinkStop(displayLink);
    CVDisplayLinkRelease(displayLink);
    [super dealloc];
}

@end

CVReturn MyDisplayLinkCallback(CVDisplayLinkRef displayLink,const CVTimeStamp *pNow,const CVTimeStamp *pOutputTime,CVOptionFlags flagsIn,
                               CVOptionFlags *pFlagsOut,void *pDisplayLinkContext){
    CVReturn result=[(GLView *)pDisplayLinkContext getFrameForTime:pOutputTime];
    return(result);
}