// Headers
#import <Foundation/Foundation.h>
#import <Cocoa/Cocoa.h>
#import <Quartzcore/CVDisplayLink.h>
#import <OpenGL/gl3.h>
#import <OpenGL/gl3ext.h>
#import "vmath.h"

enum{
	VDG_ATTRIBUTE_VERTEX = 0,
    VDG_ATTRIBUTE_COLOR,
    VDG_ATTRIBUTE_NORMAL,
    VDG_ATTRIBUTE_TEXTURE0,
};

CVReturn MyDisplayLinkCallback(CVDisplayLinkRef, const CVTimeStamp *, const CVTimeStamp *, CVOptionFlags, CVOptionFlags *, void *);

// Global variables
FILE *gpFile = NULL;
//red light arrays
GLfloat red_light_ambient[] = { 0.0f, 0.0f, 0.0f, 1.0f };
GLfloat red_light_diffuse[] = { 1.0f, 0.0f, 0.0f, 1.0f };
GLfloat red_light_specular[] = { 1.0f, 0.0f, 0.0f, 1.0f };
GLfloat red_light_position[] = { 3.0f, 0.0f, 0.0f, 1.0f };

//blue light arrays
GLfloat blue_light_ambient[] = { 0.0f, 0.0f, 0.0f, 1.0f };
GLfloat blue_light_diffuse[] = { 0.0f, 0.0f, 1.0f, 1.0f };
GLfloat blue_light_specular[] = { 0.0f, 0.0f, 1.0f, 1.0f };
GLfloat blue_light_position[] = { -3.0f, 0.0f, 0.0f, 1.0f };

//material arrays
GLfloat material_ambient[] = { 0.0f, 0.0f, 0.0f, 1.0f };
GLfloat material_diffuse[] = { 1.0f, 1.0f, 1.0f, 1.0f };
GLfloat material_specular[] = { 1.0f, 1.0f, 1.0f, 1.0f };
GLfloat material_shininess = 50.0f;
// Interface declarations
@interface AppDelegate : NSObject <NSApplicationDelegate, NSWindowDelegate>
@end

@interface GLView : NSOpenGLView
@end

// Entry point function
int main(int argc, const char * argv[]){
// Code
NSAutoreleasePool *pPool = [[NSAutoreleasePool alloc]init];
NSApp = [NSApplication sharedApplication];
[NSApp setDelegate:[[AppDelegate alloc]init]];

[NSApp run];

[pPool release];

return (0);    
}

// interface implementation
@implementation AppDelegate{
@private
NSWindow *window;
GLView *glView;
}

- (void)applicationDidFinishLaunching:(NSNotification *)aNotification{
// Code
// Log file
NSBundle *mainBundle = [NSBundle mainBundle];
NSString *appDirName = [mainBundle bundlePath];
NSString *parentDirPath = [appDirName stringByDeletingLastPathComponent];
NSString *logFileNameWithPath = [NSString stringWithFormat:@"%@/Log.txt", parentDirPath];
const char *pszLogFileNameWithPath = [logFileNameWithPath cStringUsingEncoding:NSASCIIStringEncoding];
gpFile = fopen(pszLogFileNameWithPath, "w");
if(gpFile == NULL){
	printf("Can not create log file.\n");
	[self release];
	[NSApp terminate:self];
}
fprintf(gpFile, "Program started successfully");

// Window
NSRect win_rect;
win_rect = NSMakeRect(0.0, 0.0, 800.0, 600.0);

// Create simple window
   window=[[NSWindow alloc] initWithContentRect:win_rect
                                       styleMask:NSWindowStyleMaskTitled | NSWindowStyleMaskClosable | NSWindowStyleMaskMiniaturizable | NSWindowStyleMaskResizable
                                         backing:NSBackingStoreBuffered
                                           defer:NO];
[window setTitle:@"macos OpenGL Window"];
[window center];

glView=[[GLView alloc]initWithFrame:win_rect];

[window setContentView:glView];
[window setDelegate:self];
[window makeKeyAndOrderFront:self];
}

- (void)applicationWillTerminate:(NSNotification *)notification{
// code
fprintf(gpFile, "Program terminated successfully.\n");
if(gpFile){
	fclose(gpFile);
	gpFile = NULL;
}
}

- (void)windowWillClose:(NSNotification *)notification{
[NSApp terminate:self];
}

- (void)dealloc{
[glView release];
[window release];
[super dealloc];
}
@end

@implementation GLView{
@private
	CVDisplayLinkRef displayLink;
	GLuint vertexShaderObject;
	GLuint fragmentShaderObject;
	GLuint shaderProgramObject;

	GLuint gVao_pyramid;
	GLuint gVbo_pyramid_position;
	GLuint gVbo_pyramid_normal;
	GLuint gVbo_pyramid_element;
	GLuint model_matrix_uniform, view_matrix_uniform, rotation_matrix_uniform, projection_matrix_uniform;
	GLuint L_KeyPressed_uniform;
	// Red light uniforms
	GLuint Red_La_uniform;
	GLuint Red_Ld_uniform;
	GLuint Red_Ls_uniform;
	GLuint red_light_position_uniform;

	// Blue light uniforms
	GLuint Blue_La_uniform;
	GLuint Blue_Ld_uniform;
	GLuint Blue_Ls_uniform;
	GLuint blue_light_position_uniform;

	GLuint Ka_uniform;
	GLuint Kd_uniform;
	GLuint Ks_uniform;
	GLuint material_shininess_uniform;
	
	GLfloat gfPyramidAngle;
	bool bLight;
	vmath::mat4 perspectiveProjectionMatrix;
}

- (id)initWithFrame:(NSRect)frame;{
self=[super initWithFrame:frame];

if(self){
		[[self window]setContentView:self];

		NSOpenGLPixelFormatAttribute attrs[]={
			NSOpenGLPFAOpenGLProfile,
			NSOpenGLProfileVersion4_1Core,
			NSOpenGLPFAScreenMask, CGDisplayIDToOpenGLDisplayMask(kCGDirectMainDisplay),
			NSOpenGLPFANoRecovery,
			NSOpenGLPFAAccelerated,
			NSOpenGLPFAColorSize, 24,
			NSOpenGLPFADepthSize, 24,
			NSOpenGLPFAAlphaSize, 8,
			NSOpenGLPFADoubleBuffer,
			0};

		NSOpenGLPixelFormat *pixelFormat=[[[NSOpenGLPixelFormat alloc]initWithAttributes:attrs] autorelease];

		if(pixelFormat == nil){
			fprintf(gpFile, "No Valid OpenGL Pixel is available. Exiting...");
			[self release];
			[NSApp terminate:self];
		}

		 NSOpenGLContext *glContext=[[[NSOpenGLContext alloc]initWithFormat:pixelFormat shareContext:nil]autorelease];
		[self setPixelFormat:pixelFormat];
		[self setOpenGLContext:glContext];
	}
	return (self);
}

-(CVReturn)getFrameForTime:(const CVTimeStamp *)pOutputTime{
	NSAutoreleasePool *pool = [[NSAutoreleasePool alloc]init];
	[self drawView];
	[pool release];
	return (kCVReturnSuccess);
}

-(void)prepareOpenGL{
	fprintf(gpFile, "OpenGL version: %s\n", glGetString(GL_VERSION));
	fprintf(gpFile, "GLSL verison: %s\n", glGetString(GL_SHADING_LANGUAGE_VERSION));

	[[self openGLContext]makeCurrentContext];
	gfPyramidAngle = 0.0f;
	GLint swapInt=1;
	[[self openGLContext]setValues:&swapInt forParameter:NSOpenGLCPSwapInterval];

	// Create shader
	vertexShaderObject = glCreateShader(GL_VERTEX_SHADER);

	// Provide source code to shader
	const GLchar *vertexShaderSourceCode =
		"#version 410" \
		"\n" \
		"in vec4 vPosition;" \
		"in vec3 vNormal;" \
		"uniform mat4 u_model_matrix;" \
		"uniform mat4 u_view_matrix;" \
		"uniform mat4 u_projection_matrix;" \
		"uniform int u_lighting_enabled;" \
		"uniform vec3 u_red_La;" \
		"uniform vec3 u_red_Ld;" \
		"uniform vec3 u_red_Ls;" \
		"uniform vec4 u_red_light_position;" \
		"uniform vec3 u_blue_La;" \
		"uniform vec3 u_blue_Ld;" \
		"uniform vec3 u_blue_Ls;" \
		"uniform vec4 u_blue_light_position;" \
		"uniform vec3 u_Ka;" \
		"uniform vec3 u_Kd;" \
		"uniform vec3 u_Ks;" \
		"uniform float u_material_shininess;" \
		"vec3 left_light_color;"\
		"vec3 right_light_color;"\
		"out vec3 phong_ads_color;" \
		"void main(void)" \
		"{" \
		"if(u_lighting_enabled==1)" \
		"{" \
		"vec4 eye_coordinates=u_view_matrix * u_model_matrix * vPosition;" \
		"vec3 transformed_normals=normalize(mat3(u_view_matrix * u_model_matrix) * vNormal);" \

		"vec3 left_light_direction = normalize(vec3(u_red_light_position) - eye_coordinates.xyz);" \
		"float tn_dot_ld = max(dot(transformed_normals, left_light_direction),0.0);" \
		"vec3 ambient = u_red_La * u_Ka;" \
		"vec3 diffuse = u_red_Ld * u_Kd * tn_dot_ld;" \
		"vec3 reflection_vector = reflect(-left_light_direction, transformed_normals);" \
		"vec3 viewer_vector = normalize(-eye_coordinates.xyz);" \
		"vec3 specular = u_red_Ls * u_Ks * pow(max(dot(reflection_vector, viewer_vector), 0.0), u_material_shininess);" \
		"left_light_color = ambient + diffuse + specular;"

		"vec3 right_light_direction = normalize(vec3(u_blue_light_position) - eye_coordinates.xyz);" \
		"tn_dot_ld = max(dot(transformed_normals, right_light_direction),0.0);" \
		"ambient = u_blue_La * u_Ka;" \
		"diffuse = u_blue_Ld * u_Kd * tn_dot_ld;" \
		"reflection_vector = reflect(-right_light_direction, transformed_normals);" \
		"viewer_vector = normalize(-eye_coordinates.xyz);" \
		"specular = u_blue_Ls * u_Ks * pow(max(dot(reflection_vector, viewer_vector), 0.0), u_material_shininess);" \
		"right_light_color = ambient + diffuse + specular;"

		"phong_ads_color= left_light_color + right_light_color;" \
		"}" \
		"else" \
		"{" \
		"phong_ads_color = vec3(1.0, 1.0, 1.0);" \
		"}" \
		"gl_Position=u_projection_matrix * u_view_matrix * u_model_matrix * vPosition;" \
		"}";

	glShaderSource(vertexShaderObject, 1, (const char **)&vertexShaderSourceCode, NULL);

	// Compile shader
	glCompileShader(vertexShaderObject);
	GLint iInfoLogLength = 0;
	GLint iShaderCompiledStatus = 0;
	char *szInfoLog = NULL;
	glGetShaderiv(vertexShaderObject, GL_COMPILE_STATUS, &iShaderCompiledStatus);

	if (iShaderCompiledStatus == GL_FALSE) {
		glGetShaderiv(vertexShaderObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
		if (iInfoLogLength > 0) {
			szInfoLog = (char *)malloc(sizeof(iInfoLogLength));
			if (szInfoLog != NULL) {
				GLsizei written;
				glGetShaderInfoLog(vertexShaderObject, iInfoLogLength, &written, szInfoLog);
				fprintf(gpFile, "Vertex shader compilation log: %s\n", szInfoLog);
				free(szInfoLog);
				[self release];
                [NSApp terminate:self];
			}
		}
	}
	// ** fragment shader***
	fragmentShaderObject = glCreateShader(GL_FRAGMENT_SHADER);

	// Provide source code to shader
	const GLchar *fragmentShaderSoureCode =
		"#version 410" \
		"\n" \
		"in vec3 phong_ads_color;" \
		"out vec4 FragColor;" \
		"void main(void)" \
		"{" \
		"FragColor = vec4(phong_ads_color, 1.0);" \
		"}";

	glShaderSource(fragmentShaderObject, 1, (const GLchar **)&fragmentShaderSoureCode, NULL);
	// compile shader
	glCompileShader(fragmentShaderObject);

	if (iShaderCompiledStatus == GL_FALSE) {
		glGetShaderiv(fragmentShaderObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
		if (iInfoLogLength > 0) {
			szInfoLog = (char *)malloc(sizeof(iInfoLogLength));
			if (szInfoLog != NULL) {
				GLsizei written;
				glGetShaderInfoLog(fragmentShaderObject, iInfoLogLength, &written, szInfoLog);
				fprintf(gpFile, "Fragment shader compilation log: %s\n", szInfoLog);
				free(szInfoLog);
				[self release];
                [NSApp terminate:self];
			}
		}
	}


	// *** Shader program **
	shaderProgramObject = glCreateProgram();

	// Attach vertex shader to shader program
	glAttachShader(shaderProgramObject, vertexShaderObject);

	// Attach fragment shader to shader program
	glAttachShader(shaderProgramObject, fragmentShaderObject);

	glBindAttribLocation(shaderProgramObject, VDG_ATTRIBUTE_VERTEX, "vPosition");
	glBindAttribLocation(shaderProgramObject, VDG_ATTRIBUTE_NORMAL, "vNormal");
	glLinkProgram(shaderProgramObject);
	GLint iShaderProgramLinkStatus = 0;
	glGetProgramiv(shaderProgramObject, GL_LINK_STATUS, &iShaderProgramLinkStatus);
	if (iShaderProgramLinkStatus == GL_FALSE) {
		glGetProgramiv(vertexShaderObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
		if (iInfoLogLength > 0) {
			szInfoLog = (char *)malloc(sizeof(iInfoLogLength));
			if (szInfoLog != NULL) {
				GLsizei written;
				glGetProgramInfoLog(shaderProgramObject, iInfoLogLength, &written, szInfoLog);
				fprintf(gpFile, "Shader program link log: %s\n", szInfoLog);
				free(szInfoLog);
				[self release];
                [NSApp terminate:self];
			}
		}
	}

	model_matrix_uniform = glGetUniformLocation(shaderProgramObject, "u_model_matrix");
	view_matrix_uniform = glGetUniformLocation(shaderProgramObject, "u_view_matrix");
	projection_matrix_uniform = glGetUniformLocation(shaderProgramObject, "u_projection_matrix");
	rotation_matrix_uniform = glGetUniformLocation(shaderProgramObject, "u_rotation_matrix");
	L_KeyPressed_uniform = glGetUniformLocation(shaderProgramObject, "u_lighting_enabled");

	// Ambient color intensity of light
	Red_La_uniform = glGetUniformLocation(shaderProgramObject, "u_red_La");
	// Diffuse color intensity of light
	Red_Ld_uniform = glGetUniformLocation(shaderProgramObject, "u_red_Ld");
	// Specular color intensity of light
	Red_Ls_uniform = glGetUniformLocation(shaderProgramObject, "u_red_Ls");

	// Position of light
	red_light_position_uniform = glGetUniformLocation(shaderProgramObject, "u_red_light_position");

	Blue_La_uniform = glGetUniformLocation(shaderProgramObject, "u_blue_La");
	// Diffuse color intensity of light
	Blue_Ld_uniform = glGetUniformLocation(shaderProgramObject, "u_blue_Ld");
	// Specular color intensity of light
	Blue_Ls_uniform = glGetUniformLocation(shaderProgramObject, "u_blue_Ls");

	// Position of light
	blue_light_position_uniform = glGetUniformLocation(shaderProgramObject, "u_blue_light_position");

	// Ambient color reflective intensity of material
	Ka_uniform = glGetUniformLocation(shaderProgramObject, "u_Ka");
	// diffuse reflective color intensity of material
	Kd_uniform = glGetUniformLocation(shaderProgramObject, "u_Kd");
	// specular reflective color intensity of material
	Ks_uniform = glGetUniformLocation(shaderProgramObject, "u_Ks");
	// shininess of material ( value is conventionally between 1 to 200 )
	material_shininess_uniform = glGetUniformLocation(shaderProgramObject, "u_material_shininess");

	// *** vertices, colors, shader attribute, vbo, vao initialization ***
	const GLfloat pyramidVertices[] = {
		0.0f, 1.0f, 0.0f,
		-1.0f, -1.0f, -1.0f,
		1.0f, -1.0f, -1.0f,

		0.0f,1.0f,0.0f,
		1.0f,-1.0f,-1.0f,
		1.0f,-1.0f,1.0,

		0.0f,1.0f,0.0f,
		1.0f,-1.0f,1.0f,
		-1.0f,-1.0f,1.0f,

		0.0f,1.0f,0.0f,
		-1.0f,-1.0f,1.0f,
		-1.0f,-1.0f,-1.0f
	};

	const GLfloat pyramidNormals[] = {
		0.0f,1.0f,-1.0f,
		-1.0f, -1.0f, -2.0f,
		1.0f, -1.0f, -2.0f,

		1.0f,1.0f,0.0f,
		2.0f,-1.0f,-1.0f,
		2.0f,-1.0f,1.0,

		0.0f,1.0f,1.0f,
		1.0f,-1.0f,2.0f,
		-1.0f,-1.0f,2.0f,

		-1.0f,1.0f,0.0f,
		-2.0f,-1.0f,1.0f,
		-2.0f,-1.0f,-1.0f
	};
	// *** vertices, colors, shader attribs, vbo, vao initializations ***


	// CUBE CODE
	// vao
	glGenVertexArrays(1, &gVao_pyramid);
	glBindVertexArray(gVao_pyramid);

	// position vbo
	glGenBuffers(1, &gVbo_pyramid_position);
	glBindBuffer(GL_ARRAY_BUFFER, gVbo_pyramid_position);
	glBufferData(GL_ARRAY_BUFFER, sizeof(pyramidVertices), pyramidVertices, GL_STATIC_DRAW);

	glVertexAttribPointer(VDG_ATTRIBUTE_VERTEX, 3, GL_FLOAT, GL_FALSE, 0, NULL);

	glEnableVertexAttribArray(VDG_ATTRIBUTE_VERTEX);

	glBindBuffer(GL_ARRAY_BUFFER, 0);

	// normal vbo
	glGenBuffers(1, &gVbo_pyramid_normal);
	glBindBuffer(GL_ARRAY_BUFFER, gVbo_pyramid_normal);
	glBufferData(GL_ARRAY_BUFFER, sizeof(pyramidNormals), pyramidNormals, GL_STATIC_DRAW);
	glVertexAttribPointer(VDG_ATTRIBUTE_NORMAL, 3, GL_FLOAT, GL_FALSE, 0, NULL);
	glEnableVertexAttribArray(VDG_ATTRIBUTE_NORMAL);

	glBindBuffer(GL_ARRAY_BUFFER, 0);
	glBindVertexArray(0);

	glClearDepth(1.0f);
	glEnable(GL_DEPTH_TEST);
	glDepthFunc(GL_LEQUAL);
	//glEnable(GL_CULL_FACE);

	glClearColor(0.0f, 0.0f, 0.0f, 0.0f);

	perspectiveProjectionMatrix = vmath::mat4::identity();
	CVDisplayLinkCreateWithActiveCGDisplays(&displayLink);
    CVDisplayLinkSetOutputCallback(displayLink,&MyDisplayLinkCallback,self);
    CGLContextObj cglContext=(CGLContextObj)[[self openGLContext]CGLContextObj];
    CGLPixelFormatObj cglPixelFormat=(CGLPixelFormatObj)[[self pixelFormat]CGLPixelFormatObj];
    CVDisplayLinkSetCurrentCGDisplayFromOpenGLContext(displayLink,cglContext,cglPixelFormat);
    CVDisplayLinkStart(displayLink);
}

-(void)reshape{
	CGLLockContext((CGLContextObj) [[self openGLContext]CGLContextObj]);

	NSRect rect = [self bounds];
	GLfloat width = rect.size.width;
	GLfloat height = rect.size.height;

	if(height == 0)
		height = 1;
	glViewport(0,0,(GLsizei)width,(GLsizei)height);
	
    perspectiveProjectionMatrix = vmath::perspective(45.0f, (GLfloat)width/(GLfloat)height, 0.1f, 100.0f);
	CGLUnlockContext((CGLContextObj) [[self openGLContext] CGLContextObj]);
}

-(void)drawRect:(NSRect)dirtyRect{
	[self drawView];
}

-(void)drawView{
	//static float anglePyramid = 0.0f;
	//static float angleCube = 0.0f; 
	[[self openGLContext]makeCurrentContext];

	CGLLockContext((CGLContextObj) [[self openGLContext]CGLContextObj]);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	// start using OpenGL program object
    glUseProgram(shaderProgramObject);
    
    if (bLight == true)
	{
		// set 'u_lighting_enabled' uniform
		glUniform1i(L_KeyPressed_uniform, 1);
		// setting light's properties
		glUniform3fv(Red_La_uniform, 1, red_light_ambient);
		glUniform3fv(Red_Ld_uniform, 1, red_light_diffuse);
		glUniform3fv(Red_Ls_uniform, 1, red_light_specular);
		glUniform4fv(red_light_position_uniform, 1, red_light_position);

		glUniform3fv(Blue_La_uniform, 1, blue_light_ambient);
		glUniform3fv(Blue_Ld_uniform, 1, blue_light_diffuse);
		glUniform3fv(Blue_Ls_uniform, 1, blue_light_specular);
		glUniform4fv(blue_light_position_uniform, 1, blue_light_position);

		// setting material's properties
		glUniform3fv(Ka_uniform, 1, material_ambient);
		glUniform3fv(Kd_uniform, 1, material_diffuse);
		glUniform3fv(Ks_uniform, 1, material_specular);
		glUniform1f(material_shininess_uniform, material_shininess);
	}
	else
	{
		glUniform1i(L_KeyPressed_uniform, 0);
	}
    // OpenGL Drawing
	// set all matrices to identity
	// set all matrices to identity
	vmath::mat4 modelMatrix = vmath::mat4::identity();
	vmath::mat4 viewMatrix = vmath::mat4::identity();
	vmath::mat4 rotationMatrix = vmath::mat4::identity();

	// apply z axis translation to go deep into the screen by -2.0,
	// so that triangle with same fullscreen co-ordinates, but due to above translation will look small
	modelMatrix = vmath::translate(0.0f, 0.0f, -6.0f);
	rotationMatrix = vmath::rotate(0.0f, gfPyramidAngle, 0.0f);
	modelMatrix = modelMatrix * rotationMatrix;
	glUniformMatrix4fv(model_matrix_uniform, 1, GL_FALSE, modelMatrix);
	glUniformMatrix4fv(view_matrix_uniform, 1, GL_FALSE, viewMatrix);
	
	glUniformMatrix4fv(projection_matrix_uniform, 1, GL_FALSE, perspectiveProjectionMatrix);

	// *** bind vao ***
	glBindVertexArray(gVao_pyramid);

	// *** draw, either by glDrawTriangles() or glDrawArrays() or glDrawElements()
	glDrawArrays(GL_TRIANGLES, 0, 12);

	// Unbind vao
	glBindVertexArray(0);
    
    // stop using OpenGL program object
    glUseProgram(0);
	CGLFlushDrawable((CGLContextObj) [[self openGLContext]CGLContextObj]);
	CGLUnlockContext((CGLContextObj) [[self openGLContext]CGLContextObj]);

	gfPyramidAngle = (gfPyramidAngle + 0.1f);
	if (gfPyramidAngle >= 360.0f)
		gfPyramidAngle = 0.0f;
	
}

-(BOOL)acceptsFirstResponder{
	[[self window]makeFirstResponder:self];
	return(YES);
}

-(void)keyDown:(NSEvent *)theEvent{
	static bool bIsAKeyPressed = false;
	static bool bIsLKeyPressed = false;
	int key = (int)[[theEvent characters]characterAtIndex:0];
	switch(key){
		case 27:
		[self release];
		[NSApp terminate:self];
		break;
		case 'F':
		case 'f':
		[[self window]toggleFullScreen:self];
		break;
		case 'l':
		case 'L':
		if (bIsLKeyPressed == false)
			{
				bLight = true;
				bIsLKeyPressed = true;
			}
			else
			{
				bLight = false;
				bIsLKeyPressed = false;
			}
		break;
		default:
		break;
	}
}


-(void)mouseDown:(NSEvent *)theEvent{

}

-(void)mouseDragged:(NSEvent *)theEvent{

}

-(void)rightMouseDown:(NSEvent *)theEvent{

}

- (void) dealloc
{	
    if (gVao_pyramid)
	{
		glDeleteVertexArrays(1, &gVao_pyramid);
		gVao_pyramid = 0;
	}

	// destroy vbo
	if (gVbo_pyramid_position)
	{
		glDeleteBuffers(1, &gVbo_pyramid_position);
		gVbo_pyramid_position = 0;
	}
	if (gVbo_pyramid_normal)
	{
		glDeleteBuffers(1, &gVbo_pyramid_normal);
		gVbo_pyramid_normal = 0;
	}
    
    // detach vertex shader from shader program object
    glDetachShader(shaderProgramObject, vertexShaderObject);
    // detach fragment  shader from shader program object
    glDetachShader(shaderProgramObject, fragmentShaderObject);
    
    // delete vertex shader object
    glDeleteShader(vertexShaderObject);
    vertexShaderObject = 0;
    // delete fragment shader object
    glDeleteShader(fragmentShaderObject);
    fragmentShaderObject = 0;
    
    // delete shader program object
    glDeleteProgram(shaderProgramObject);
    shaderProgramObject = 0;
    CVDisplayLinkStop(displayLink);
    CVDisplayLinkRelease(displayLink);
    [super dealloc];
}

@end

CVReturn MyDisplayLinkCallback(CVDisplayLinkRef displayLink,const CVTimeStamp *pNow,const CVTimeStamp *pOutputTime,CVOptionFlags flagsIn,
                               CVOptionFlags *pFlagsOut,void *pDisplayLinkContext){
    CVReturn result=[(GLView *)pDisplayLinkContext getFrameForTime:pOutputTime];
    return(result);
}