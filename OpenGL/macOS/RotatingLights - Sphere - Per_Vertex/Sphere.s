	.section	__TEXT,__text,regular,pure_instructions
	.macosx_version_min 10, 13
	.section	__TEXT,__literal4,4byte_literals
	.p2align	2               ## -- Begin function normalizeVector
LCPI0_0:
	.long	1065353216              ## float 1
	.section	__TEXT,__text,regular,pure_instructions
	.globl	_normalizeVector
	.p2align	4, 0x90
_normalizeVector:                       ## @normalizeVector
	.cfi_startproc
## BB#0:
	pushq	%rbp
Lcfi0:
	.cfi_def_cfa_offset 16
Lcfi1:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Lcfi2:
	.cfi_def_cfa_register %rbp
	movss	LCPI0_0(%rip), %xmm0    ## xmm0 = mem[0],zero,zero,zero
	movq	%rdi, -8(%rbp)
	movq	-8(%rbp), %rdi
	movss	(%rdi), %xmm1           ## xmm1 = mem[0],zero,zero,zero
	movq	-8(%rbp), %rdi
	mulss	(%rdi), %xmm1
	movq	-8(%rbp), %rdi
	movss	4(%rdi), %xmm2          ## xmm2 = mem[0],zero,zero,zero
	movq	-8(%rbp), %rdi
	mulss	4(%rdi), %xmm2
	addss	%xmm2, %xmm1
	movq	-8(%rbp), %rdi
	movss	8(%rdi), %xmm2          ## xmm2 = mem[0],zero,zero,zero
	movq	-8(%rbp), %rdi
	mulss	8(%rdi), %xmm2
	addss	%xmm2, %xmm1
	movss	%xmm1, -12(%rbp)
	cvtss2sd	-12(%rbp), %xmm1
	sqrtsd	%xmm1, %xmm1
	cvtsd2ss	%xmm1, %xmm1
	movss	%xmm1, -16(%rbp)
	movq	-8(%rbp), %rdi
	movaps	%xmm0, %xmm1
	mulss	(%rdi), %xmm1
	divss	-16(%rbp), %xmm1
	movq	-8(%rbp), %rdi
	movss	%xmm1, (%rdi)
	movq	-8(%rbp), %rdi
	movaps	%xmm0, %xmm1
	mulss	4(%rdi), %xmm1
	divss	-16(%rbp), %xmm1
	movq	-8(%rbp), %rdi
	movss	%xmm1, 4(%rdi)
	movq	-8(%rbp), %rdi
	mulss	8(%rdi), %xmm0
	divss	-16(%rbp), %xmm0
	movq	-8(%rbp), %rdi
	movss	%xmm0, 8(%rdi)
	popq	%rbp
	retq
	.cfi_endproc
                                        ## -- End function
	.section	__TEXT,__literal16,16byte_literals
	.p2align	4               ## -- Begin function isFoundIdentical
LCPI1_0:
	.long	2147483647              ## float NaN
	.long	2147483647              ## float NaN
	.long	2147483647              ## float NaN
	.long	2147483647              ## float NaN
	.section	__TEXT,__text,regular,pure_instructions
	.globl	_isFoundIdentical
	.p2align	4, 0x90
_isFoundIdentical:                      ## @isFoundIdentical
	.cfi_startproc
## BB#0:
	pushq	%rbp
Lcfi3:
	.cfi_def_cfa_offset 16
Lcfi4:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Lcfi5:
	.cfi_def_cfa_register %rbp
	movss	%xmm0, -8(%rbp)
	movss	%xmm1, -12(%rbp)
	movss	%xmm2, -16(%rbp)
	movss	-8(%rbp), %xmm0         ## xmm0 = mem[0],zero,zero,zero
	subss	-12(%rbp), %xmm0
	movaps	LCPI1_0(%rip), %xmm1    ## xmm1 = [nan,nan,nan,nan]
	pand	%xmm1, %xmm0
	movss	-16(%rbp), %xmm1        ## xmm1 = mem[0],zero,zero,zero
	ucomiss	%xmm0, %xmm1
	jbe	LBB1_2
## BB#1:
	movl	$1, -4(%rbp)
	jmp	LBB1_3
LBB1_2:
	movl	$0, -4(%rbp)
LBB1_3:
	movl	-4(%rbp), %eax
	popq	%rbp
	retq
	.cfi_endproc
                                        ## -- End function
	.section	__TEXT,__literal4,4byte_literals
	.p2align	2               ## -- Begin function addTriangle
LCPI2_0:
	.long	925353388               ## float 9.99999974E-6
	.section	__TEXT,__text,regular,pure_instructions
	.globl	_addTriangle
	.p2align	4, 0x90
_addTriangle:                           ## @addTriangle
	.cfi_startproc
## BB#0:
	pushq	%rbp
Lcfi6:
	.cfi_def_cfa_offset 16
Lcfi7:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Lcfi8:
	.cfi_def_cfa_register %rbp
	pushq	%rbx
	subq	$40, %rsp
Lcfi9:
	.cfi_offset %rbx, -24
	movss	LCPI2_0(%rip), %xmm0    ## xmm0 = mem[0],zero,zero,zero
	movq	%rdi, -16(%rbp)
	movq	%rsi, -24(%rbp)
	movq	%rdx, -32(%rbp)
	movss	%xmm0, -36(%rbp)
	movq	-24(%rbp), %rdi
	callq	_normalizeVector
	movq	-24(%rbp), %rdx
	addq	$12, %rdx
	movq	%rdx, %rdi
	callq	_normalizeVector
	movq	-24(%rbp), %rdx
	addq	$24, %rdx
	movq	%rdx, %rdi
	callq	_normalizeVector
	movl	$0, -40(%rbp)
LBB2_1:                                 ## =>This Loop Header: Depth=1
                                        ##     Child Loop BB2_3 Depth 2
	cmpl	$3, -40(%rbp)
	jge	LBB2_21
## BB#2:                                ##   in Loop: Header=BB2_1 Depth=1
	movl	$0, -44(%rbp)
LBB2_3:                                 ##   Parent Loop BB2_1 Depth=1
                                        ## =>  This Inner Loop Header: Depth=2
	movq	_numVertices@GOTPCREL(%rip), %rax
	movl	-44(%rbp), %ecx
	cmpl	(%rax), %ecx
	jge	LBB2_15
## BB#4:                                ##   in Loop: Header=BB2_3 Depth=2
	movq	_model_vertices@GOTPCREL(%rip), %rax
	imull	$3, -44(%rbp), %ecx
	movslq	%ecx, %rdx
	movss	(%rax,%rdx,4), %xmm0    ## xmm0 = mem[0],zero,zero,zero
	movq	-16(%rbp), %rax
	movslq	-40(%rbp), %rdx
	imulq	$12, %rdx, %rdx
	addq	%rdx, %rax
	movss	(%rax), %xmm1           ## xmm1 = mem[0],zero,zero,zero
	movss	-36(%rbp), %xmm2        ## xmm2 = mem[0],zero,zero,zero
	callq	_isFoundIdentical
	cmpl	$0, %eax
	je	LBB2_13
## BB#5:                                ##   in Loop: Header=BB2_3 Depth=2
	movq	_model_vertices@GOTPCREL(%rip), %rax
	imull	$3, -44(%rbp), %ecx
	addl	$1, %ecx
	movslq	%ecx, %rdx
	movss	(%rax,%rdx,4), %xmm0    ## xmm0 = mem[0],zero,zero,zero
	movq	-16(%rbp), %rax
	movslq	-40(%rbp), %rdx
	imulq	$12, %rdx, %rdx
	addq	%rdx, %rax
	movss	4(%rax), %xmm1          ## xmm1 = mem[0],zero,zero,zero
	movss	-36(%rbp), %xmm2        ## xmm2 = mem[0],zero,zero,zero
	callq	_isFoundIdentical
	cmpl	$0, %eax
	je	LBB2_13
## BB#6:                                ##   in Loop: Header=BB2_3 Depth=2
	movq	_model_vertices@GOTPCREL(%rip), %rax
	imull	$3, -44(%rbp), %ecx
	addl	$2, %ecx
	movslq	%ecx, %rdx
	movss	(%rax,%rdx,4), %xmm0    ## xmm0 = mem[0],zero,zero,zero
	movq	-16(%rbp), %rax
	movslq	-40(%rbp), %rdx
	imulq	$12, %rdx, %rdx
	addq	%rdx, %rax
	movss	8(%rax), %xmm1          ## xmm1 = mem[0],zero,zero,zero
	movss	-36(%rbp), %xmm2        ## xmm2 = mem[0],zero,zero,zero
	callq	_isFoundIdentical
	cmpl	$0, %eax
	je	LBB2_13
## BB#7:                                ##   in Loop: Header=BB2_3 Depth=2
	movq	_model_normals@GOTPCREL(%rip), %rax
	imull	$3, -44(%rbp), %ecx
	movslq	%ecx, %rdx
	movss	(%rax,%rdx,4), %xmm0    ## xmm0 = mem[0],zero,zero,zero
	movq	-24(%rbp), %rax
	movslq	-40(%rbp), %rdx
	imulq	$12, %rdx, %rdx
	addq	%rdx, %rax
	movss	(%rax), %xmm1           ## xmm1 = mem[0],zero,zero,zero
	movss	-36(%rbp), %xmm2        ## xmm2 = mem[0],zero,zero,zero
	callq	_isFoundIdentical
	cmpl	$0, %eax
	je	LBB2_13
## BB#8:                                ##   in Loop: Header=BB2_3 Depth=2
	movq	_model_normals@GOTPCREL(%rip), %rax
	imull	$3, -44(%rbp), %ecx
	addl	$1, %ecx
	movslq	%ecx, %rdx
	movss	(%rax,%rdx,4), %xmm0    ## xmm0 = mem[0],zero,zero,zero
	movq	-24(%rbp), %rax
	movslq	-40(%rbp), %rdx
	imulq	$12, %rdx, %rdx
	addq	%rdx, %rax
	movss	4(%rax), %xmm1          ## xmm1 = mem[0],zero,zero,zero
	movss	-36(%rbp), %xmm2        ## xmm2 = mem[0],zero,zero,zero
	callq	_isFoundIdentical
	cmpl	$0, %eax
	je	LBB2_13
## BB#9:                                ##   in Loop: Header=BB2_3 Depth=2
	movq	_model_normals@GOTPCREL(%rip), %rax
	imull	$3, -44(%rbp), %ecx
	addl	$2, %ecx
	movslq	%ecx, %rdx
	movss	(%rax,%rdx,4), %xmm0    ## xmm0 = mem[0],zero,zero,zero
	movq	-24(%rbp), %rax
	movslq	-40(%rbp), %rdx
	imulq	$12, %rdx, %rdx
	addq	%rdx, %rax
	movss	8(%rax), %xmm1          ## xmm1 = mem[0],zero,zero,zero
	movss	-36(%rbp), %xmm2        ## xmm2 = mem[0],zero,zero,zero
	callq	_isFoundIdentical
	cmpl	$0, %eax
	je	LBB2_13
## BB#10:                               ##   in Loop: Header=BB2_3 Depth=2
	movq	_model_textures@GOTPCREL(%rip), %rax
	movl	-44(%rbp), %ecx
	shll	$1, %ecx
	movslq	%ecx, %rdx
	movss	(%rax,%rdx,4), %xmm0    ## xmm0 = mem[0],zero,zero,zero
	movq	-32(%rbp), %rax
	movslq	-40(%rbp), %rdx
	movss	(%rax,%rdx,8), %xmm1    ## xmm1 = mem[0],zero,zero,zero
	movss	-36(%rbp), %xmm2        ## xmm2 = mem[0],zero,zero,zero
	callq	_isFoundIdentical
	cmpl	$0, %eax
	je	LBB2_13
## BB#11:                               ##   in Loop: Header=BB2_3 Depth=2
	movq	_model_textures@GOTPCREL(%rip), %rax
	movl	-44(%rbp), %ecx
	shll	$1, %ecx
	addl	$1, %ecx
	movslq	%ecx, %rdx
	movss	(%rax,%rdx,4), %xmm0    ## xmm0 = mem[0],zero,zero,zero
	movq	-32(%rbp), %rax
	movslq	-40(%rbp), %rdx
	movss	4(%rax,%rdx,8), %xmm1   ## xmm1 = mem[0],zero,zero,zero
	movss	-36(%rbp), %xmm2        ## xmm2 = mem[0],zero,zero,zero
	callq	_isFoundIdentical
	cmpl	$0, %eax
	je	LBB2_13
## BB#12:                               ##   in Loop: Header=BB2_1 Depth=1
	movq	_numElements@GOTPCREL(%rip), %rax
	movq	_model_elements@GOTPCREL(%rip), %rcx
	movl	-44(%rbp), %edx
	movw	%dx, %si
	movslq	(%rax), %rdi
	movw	%si, (%rcx,%rdi,2)
	movl	(%rax), %edx
	addl	$1, %edx
	movl	%edx, (%rax)
	jmp	LBB2_15
LBB2_13:                                ##   in Loop: Header=BB2_3 Depth=2
	jmp	LBB2_14
LBB2_14:                                ##   in Loop: Header=BB2_3 Depth=2
	movl	-44(%rbp), %eax
	addl	$1, %eax
	movl	%eax, -44(%rbp)
	jmp	LBB2_3
LBB2_15:                                ##   in Loop: Header=BB2_1 Depth=1
	movq	_numVertices@GOTPCREL(%rip), %rax
	movl	-44(%rbp), %ecx
	cmpl	(%rax), %ecx
	jne	LBB2_19
## BB#16:                               ##   in Loop: Header=BB2_1 Depth=1
	movq	_maxElements@GOTPCREL(%rip), %rax
	movq	_numVertices@GOTPCREL(%rip), %rcx
	movl	(%rcx), %edx
	cmpl	(%rax), %edx
	jge	LBB2_19
## BB#17:                               ##   in Loop: Header=BB2_1 Depth=1
	movq	_maxElements@GOTPCREL(%rip), %rax
	movq	_numElements@GOTPCREL(%rip), %rcx
	movl	(%rcx), %edx
	cmpl	(%rax), %edx
	jge	LBB2_19
## BB#18:                               ##   in Loop: Header=BB2_1 Depth=1
	movq	_numVertices@GOTPCREL(%rip), %rax
	movq	_numElements@GOTPCREL(%rip), %rcx
	movq	_model_elements@GOTPCREL(%rip), %rdx
	movq	_model_textures@GOTPCREL(%rip), %rsi
	movq	_model_normals@GOTPCREL(%rip), %rdi
	movq	_model_vertices@GOTPCREL(%rip), %r8
	movq	-16(%rbp), %r9
	movslq	-40(%rbp), %r10
	imulq	$12, %r10, %r10
	addq	%r10, %r9
	movss	(%r9), %xmm0            ## xmm0 = mem[0],zero,zero,zero
	imull	$3, (%rax), %r11d
	movslq	%r11d, %r9
	movss	%xmm0, (%r8,%r9,4)
	movq	-16(%rbp), %r9
	movslq	-40(%rbp), %r10
	imulq	$12, %r10, %r10
	addq	%r10, %r9
	movss	4(%r9), %xmm0           ## xmm0 = mem[0],zero,zero,zero
	imull	$3, (%rax), %r11d
	addl	$1, %r11d
	movslq	%r11d, %r9
	movss	%xmm0, (%r8,%r9,4)
	movq	-16(%rbp), %r9
	movslq	-40(%rbp), %r10
	imulq	$12, %r10, %r10
	addq	%r10, %r9
	movss	8(%r9), %xmm0           ## xmm0 = mem[0],zero,zero,zero
	imull	$3, (%rax), %r11d
	addl	$2, %r11d
	movslq	%r11d, %r9
	movss	%xmm0, (%r8,%r9,4)
	movq	-24(%rbp), %r8
	movslq	-40(%rbp), %r9
	imulq	$12, %r9, %r9
	addq	%r9, %r8
	movss	(%r8), %xmm0            ## xmm0 = mem[0],zero,zero,zero
	imull	$3, (%rax), %r11d
	movslq	%r11d, %r8
	movss	%xmm0, (%rdi,%r8,4)
	movq	-24(%rbp), %r8
	movslq	-40(%rbp), %r9
	imulq	$12, %r9, %r9
	addq	%r9, %r8
	movss	4(%r8), %xmm0           ## xmm0 = mem[0],zero,zero,zero
	imull	$3, (%rax), %r11d
	addl	$1, %r11d
	movslq	%r11d, %r8
	movss	%xmm0, (%rdi,%r8,4)
	movq	-24(%rbp), %r8
	movslq	-40(%rbp), %r9
	imulq	$12, %r9, %r9
	addq	%r9, %r8
	movss	8(%r8), %xmm0           ## xmm0 = mem[0],zero,zero,zero
	imull	$3, (%rax), %r11d
	addl	$2, %r11d
	movslq	%r11d, %r8
	movss	%xmm0, (%rdi,%r8,4)
	movq	-32(%rbp), %rdi
	movslq	-40(%rbp), %r8
	movss	(%rdi,%r8,8), %xmm0     ## xmm0 = mem[0],zero,zero,zero
	movl	(%rax), %r11d
	shll	$1, %r11d
	movslq	%r11d, %rdi
	movss	%xmm0, (%rsi,%rdi,4)
	movq	-32(%rbp), %rdi
	movslq	-40(%rbp), %r8
	movss	4(%rdi,%r8,8), %xmm0    ## xmm0 = mem[0],zero,zero,zero
	movl	(%rax), %r11d
	shll	$1, %r11d
	addl	$1, %r11d
	movslq	%r11d, %rdi
	movss	%xmm0, (%rsi,%rdi,4)
	movl	(%rax), %r11d
	movw	%r11w, %bx
	movslq	(%rcx), %rsi
	movw	%bx, (%rdx,%rsi,2)
	movl	(%rcx), %r11d
	addl	$1, %r11d
	movl	%r11d, (%rcx)
	movl	(%rax), %r11d
	addl	$1, %r11d
	movl	%r11d, (%rax)
LBB2_19:                                ##   in Loop: Header=BB2_1 Depth=1
	jmp	LBB2_20
LBB2_20:                                ##   in Loop: Header=BB2_1 Depth=1
	movl	-40(%rbp), %eax
	addl	$1, %eax
	movl	%eax, -40(%rbp)
	jmp	LBB2_1
LBB2_21:
	addq	$40, %rsp
	popq	%rbx
	popq	%rbp
	retq
	.cfi_endproc
                                        ## -- End function
	.globl	_processSphereData      ## -- Begin function processSphereData
	.p2align	4, 0x90
_processSphereData:                     ## @processSphereData
	.cfi_startproc
## BB#0:
	pushq	%rbp
Lcfi10:
	.cfi_def_cfa_offset 16
Lcfi11:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Lcfi12:
	.cfi_def_cfa_register %rbp
	subq	$144, %rsp
	movq	_maxElements@GOTPCREL(%rip), %rax
	movq	___stack_chk_guard@GOTPCREL(%rip), %rcx
	movq	(%rcx), %rcx
	movq	%rcx, -8(%rbp)
	movl	$760, -132(%rbp)        ## imm = 0x2F8
	imull	$3, -132(%rbp), %edx
	movl	%edx, (%rax)
	movl	$0, -136(%rbp)
LBB3_1:                                 ## =>This Loop Header: Depth=1
                                        ##     Child Loop BB3_3 Depth 2
	movl	-136(%rbp), %eax
	cmpl	-132(%rbp), %eax
	jge	LBB3_8
## BB#2:                                ##   in Loop: Header=BB3_1 Depth=1
	movl	$0, -140(%rbp)
LBB3_3:                                 ##   Parent Loop BB3_1 Depth=1
                                        ## =>  This Inner Loop Header: Depth=2
	cmpl	$3, -140(%rbp)
	jge	LBB3_6
## BB#4:                                ##   in Loop: Header=BB3_3 Depth=2
	leaq	_textures(%rip), %rax
	leaq	_indices(%rip), %rcx
	leaq	-96(%rbp), %rdx
	leaq	_normals(%rip), %rsi
	leaq	-48(%rbp), %rdi
	leaq	_vertices(%rip), %r8
	movslq	-136(%rbp), %r9
	imulq	$36, %r9, %r9
	movq	%rcx, %r10
	addq	%r9, %r10
	movslq	-140(%rbp), %r9
	movslq	(%r10,%r9,4), %r9
	imulq	$12, %r9, %r9
	movq	%r8, %r10
	addq	%r9, %r10
	movss	(%r10), %xmm0           ## xmm0 = mem[0],zero,zero,zero
	movslq	-140(%rbp), %r9
	imulq	$12, %r9, %r9
	movq	%rdi, %r10
	addq	%r9, %r10
	movss	%xmm0, (%r10)
	movslq	-136(%rbp), %r9
	imulq	$36, %r9, %r9
	movq	%rcx, %r10
	addq	%r9, %r10
	movslq	-140(%rbp), %r9
	movslq	(%r10,%r9,4), %r9
	imulq	$12, %r9, %r9
	movq	%r8, %r10
	addq	%r9, %r10
	movss	4(%r10), %xmm0          ## xmm0 = mem[0],zero,zero,zero
	movslq	-140(%rbp), %r9
	imulq	$12, %r9, %r9
	movq	%rdi, %r10
	addq	%r9, %r10
	movss	%xmm0, 4(%r10)
	movslq	-136(%rbp), %r9
	imulq	$36, %r9, %r9
	movq	%rcx, %r10
	addq	%r9, %r10
	movslq	-140(%rbp), %r9
	movslq	(%r10,%r9,4), %r9
	imulq	$12, %r9, %r9
	addq	%r9, %r8
	movss	8(%r8), %xmm0           ## xmm0 = mem[0],zero,zero,zero
	movslq	-140(%rbp), %r8
	imulq	$12, %r8, %r8
	addq	%r8, %rdi
	movss	%xmm0, 8(%rdi)
	movslq	-136(%rbp), %rdi
	imulq	$36, %rdi, %rdi
	movq	%rcx, %r8
	addq	%rdi, %r8
	movl	-140(%rbp), %r11d
	addl	$3, %r11d
	movslq	%r11d, %rdi
	movslq	(%r8,%rdi,4), %rdi
	imulq	$12, %rdi, %rdi
	movq	%rsi, %r8
	addq	%rdi, %r8
	movss	(%r8), %xmm0            ## xmm0 = mem[0],zero,zero,zero
	movslq	-140(%rbp), %rdi
	imulq	$12, %rdi, %rdi
	movq	%rdx, %r8
	addq	%rdi, %r8
	movss	%xmm0, (%r8)
	movslq	-136(%rbp), %rdi
	imulq	$36, %rdi, %rdi
	movq	%rcx, %r8
	addq	%rdi, %r8
	movl	-140(%rbp), %r11d
	addl	$3, %r11d
	movslq	%r11d, %rdi
	movslq	(%r8,%rdi,4), %rdi
	imulq	$12, %rdi, %rdi
	movq	%rsi, %r8
	addq	%rdi, %r8
	movss	4(%r8), %xmm0           ## xmm0 = mem[0],zero,zero,zero
	movslq	-140(%rbp), %rdi
	imulq	$12, %rdi, %rdi
	movq	%rdx, %r8
	addq	%rdi, %r8
	movss	%xmm0, 4(%r8)
	movslq	-136(%rbp), %rdi
	imulq	$36, %rdi, %rdi
	movq	%rcx, %r8
	addq	%rdi, %r8
	movl	-140(%rbp), %r11d
	addl	$3, %r11d
	movslq	%r11d, %rdi
	movslq	(%r8,%rdi,4), %rdi
	imulq	$12, %rdi, %rdi
	addq	%rdi, %rsi
	movss	8(%rsi), %xmm0          ## xmm0 = mem[0],zero,zero,zero
	movslq	-140(%rbp), %rsi
	imulq	$12, %rsi, %rsi
	addq	%rsi, %rdx
	movss	%xmm0, 8(%rdx)
	movslq	-136(%rbp), %rdx
	imulq	$36, %rdx, %rdx
	movq	%rcx, %rsi
	addq	%rdx, %rsi
	movl	-140(%rbp), %r11d
	addl	$6, %r11d
	movslq	%r11d, %rdx
	movslq	(%rsi,%rdx,4), %rdx
	movss	(%rax,%rdx,8), %xmm0    ## xmm0 = mem[0],zero,zero,zero
	movslq	-140(%rbp), %rdx
	movss	%xmm0, -128(%rbp,%rdx,8)
	movslq	-136(%rbp), %rdx
	imulq	$36, %rdx, %rdx
	addq	%rdx, %rcx
	movl	-140(%rbp), %r11d
	addl	$6, %r11d
	movslq	%r11d, %rdx
	movslq	(%rcx,%rdx,4), %rcx
	movss	4(%rax,%rcx,8), %xmm0   ## xmm0 = mem[0],zero,zero,zero
	movslq	-140(%rbp), %rax
	movss	%xmm0, -124(%rbp,%rax,8)
## BB#5:                                ##   in Loop: Header=BB3_3 Depth=2
	movl	-140(%rbp), %eax
	addl	$1, %eax
	movl	%eax, -140(%rbp)
	jmp	LBB3_3
LBB3_6:                                 ##   in Loop: Header=BB3_1 Depth=1
	leaq	-128(%rbp), %rdx
	leaq	-96(%rbp), %rsi
	leaq	-48(%rbp), %rdi
	callq	_addTriangle
## BB#7:                                ##   in Loop: Header=BB3_1 Depth=1
	movl	-136(%rbp), %eax
	addl	$1, %eax
	movl	%eax, -136(%rbp)
	jmp	LBB3_1
LBB3_8:
	movq	___stack_chk_guard@GOTPCREL(%rip), %rax
	movq	(%rax), %rax
	movq	-8(%rbp), %rcx
	cmpq	%rcx, %rax
	jne	LBB3_10
## BB#9:
	addq	$144, %rsp
	popq	%rbp
	retq
LBB3_10:
	callq	___stack_chk_fail
	.cfi_endproc
                                        ## -- End function
	.globl	_getSphereVertexData    ## -- Begin function getSphereVertexData
	.p2align	4, 0x90
_getSphereVertexData:                   ## @getSphereVertexData
	.cfi_startproc
## BB#0:
	pushq	%rbp
Lcfi13:
	.cfi_def_cfa_offset 16
Lcfi14:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Lcfi15:
	.cfi_def_cfa_register %rbp
	subq	$64, %rsp
	movq	%rdi, -8(%rbp)
	movq	%rsi, -16(%rbp)
	movq	%rdx, -24(%rbp)
	movq	%rcx, -32(%rbp)
	movl	$0, -36(%rbp)
LBB4_1:                                 ## =>This Inner Loop Header: Depth=1
	cmpl	$1146, -36(%rbp)        ## imm = 0x47A
	jge	LBB4_4
## BB#2:                                ##   in Loop: Header=BB4_1 Depth=1
	movq	_model_vertices@GOTPCREL(%rip), %rax
	movq	-8(%rbp), %rcx
	movslq	-36(%rbp), %rdx
	movss	(%rcx,%rdx,4), %xmm0    ## xmm0 = mem[0],zero,zero,zero
	movslq	-36(%rbp), %rcx
	movss	%xmm0, (%rax,%rcx,4)
## BB#3:                                ##   in Loop: Header=BB4_1 Depth=1
	movl	-36(%rbp), %eax
	addl	$1, %eax
	movl	%eax, -36(%rbp)
	jmp	LBB4_1
LBB4_4:
	movl	$0, -40(%rbp)
LBB4_5:                                 ## =>This Inner Loop Header: Depth=1
	cmpl	$1146, -40(%rbp)        ## imm = 0x47A
	jge	LBB4_8
## BB#6:                                ##   in Loop: Header=BB4_5 Depth=1
	movq	_model_normals@GOTPCREL(%rip), %rax
	movq	-16(%rbp), %rcx
	movslq	-40(%rbp), %rdx
	movss	(%rcx,%rdx,4), %xmm0    ## xmm0 = mem[0],zero,zero,zero
	movslq	-40(%rbp), %rcx
	movss	%xmm0, (%rax,%rcx,4)
## BB#7:                                ##   in Loop: Header=BB4_5 Depth=1
	movl	-40(%rbp), %eax
	addl	$1, %eax
	movl	%eax, -40(%rbp)
	jmp	LBB4_5
LBB4_8:
	movl	$0, -44(%rbp)
LBB4_9:                                 ## =>This Inner Loop Header: Depth=1
	cmpl	$764, -44(%rbp)         ## imm = 0x2FC
	jge	LBB4_12
## BB#10:                               ##   in Loop: Header=BB4_9 Depth=1
	movq	_model_textures@GOTPCREL(%rip), %rax
	movq	-24(%rbp), %rcx
	movslq	-44(%rbp), %rdx
	movss	(%rcx,%rdx,4), %xmm0    ## xmm0 = mem[0],zero,zero,zero
	movslq	-44(%rbp), %rcx
	movss	%xmm0, (%rax,%rcx,4)
## BB#11:                               ##   in Loop: Header=BB4_9 Depth=1
	movl	-44(%rbp), %eax
	addl	$1, %eax
	movl	%eax, -44(%rbp)
	jmp	LBB4_9
LBB4_12:
	movl	$0, -48(%rbp)
LBB4_13:                                ## =>This Inner Loop Header: Depth=1
	cmpl	$2280, -48(%rbp)        ## imm = 0x8E8
	jge	LBB4_16
## BB#14:                               ##   in Loop: Header=BB4_13 Depth=1
	movq	_model_elements@GOTPCREL(%rip), %rax
	movq	-32(%rbp), %rcx
	movslq	-48(%rbp), %rdx
	movw	(%rcx,%rdx,2), %si
	movslq	-48(%rbp), %rcx
	movw	%si, (%rax,%rcx,2)
## BB#15:                               ##   in Loop: Header=BB4_13 Depth=1
	movl	-48(%rbp), %eax
	addl	$1, %eax
	movl	%eax, -48(%rbp)
	jmp	LBB4_13
LBB4_16:
	callq	_processSphereData
	movl	$0, -52(%rbp)
LBB4_17:                                ## =>This Inner Loop Header: Depth=1
	cmpl	$1146, -52(%rbp)        ## imm = 0x47A
	jge	LBB4_20
## BB#18:                               ##   in Loop: Header=BB4_17 Depth=1
	movq	_model_vertices@GOTPCREL(%rip), %rax
	movslq	-52(%rbp), %rcx
	movss	(%rax,%rcx,4), %xmm0    ## xmm0 = mem[0],zero,zero,zero
	movq	-8(%rbp), %rax
	movslq	-52(%rbp), %rcx
	movss	%xmm0, (%rax,%rcx,4)
## BB#19:                               ##   in Loop: Header=BB4_17 Depth=1
	movl	-52(%rbp), %eax
	addl	$1, %eax
	movl	%eax, -52(%rbp)
	jmp	LBB4_17
LBB4_20:
	movl	$0, -56(%rbp)
LBB4_21:                                ## =>This Inner Loop Header: Depth=1
	cmpl	$1146, -56(%rbp)        ## imm = 0x47A
	jge	LBB4_24
## BB#22:                               ##   in Loop: Header=BB4_21 Depth=1
	movq	_model_normals@GOTPCREL(%rip), %rax
	movslq	-56(%rbp), %rcx
	movss	(%rax,%rcx,4), %xmm0    ## xmm0 = mem[0],zero,zero,zero
	movq	-16(%rbp), %rax
	movslq	-56(%rbp), %rcx
	movss	%xmm0, (%rax,%rcx,4)
## BB#23:                               ##   in Loop: Header=BB4_21 Depth=1
	movl	-56(%rbp), %eax
	addl	$1, %eax
	movl	%eax, -56(%rbp)
	jmp	LBB4_21
LBB4_24:
	movl	$0, -60(%rbp)
LBB4_25:                                ## =>This Inner Loop Header: Depth=1
	cmpl	$764, -60(%rbp)         ## imm = 0x2FC
	jge	LBB4_28
## BB#26:                               ##   in Loop: Header=BB4_25 Depth=1
	movq	_model_textures@GOTPCREL(%rip), %rax
	movslq	-60(%rbp), %rcx
	movss	(%rax,%rcx,4), %xmm0    ## xmm0 = mem[0],zero,zero,zero
	movq	-24(%rbp), %rax
	movslq	-60(%rbp), %rcx
	movss	%xmm0, (%rax,%rcx,4)
## BB#27:                               ##   in Loop: Header=BB4_25 Depth=1
	movl	-60(%rbp), %eax
	addl	$1, %eax
	movl	%eax, -60(%rbp)
	jmp	LBB4_25
LBB4_28:
	movl	$0, -64(%rbp)
LBB4_29:                                ## =>This Inner Loop Header: Depth=1
	cmpl	$2280, -64(%rbp)        ## imm = 0x8E8
	jge	LBB4_32
## BB#30:                               ##   in Loop: Header=BB4_29 Depth=1
	movq	_model_elements@GOTPCREL(%rip), %rax
	movslq	-64(%rbp), %rcx
	movw	(%rax,%rcx,2), %dx
	movq	-32(%rbp), %rax
	movslq	-64(%rbp), %rcx
	movw	%dx, (%rax,%rcx,2)
## BB#31:                               ##   in Loop: Header=BB4_29 Depth=1
	movl	-64(%rbp), %eax
	addl	$1, %eax
	movl	%eax, -64(%rbp)
	jmp	LBB4_29
LBB4_32:
	addq	$64, %rsp
	popq	%rbp
	retq
	.cfi_endproc
                                        ## -- End function
	.globl	_getNumberOfSphereVertices ## -- Begin function getNumberOfSphereVertices
	.p2align	4, 0x90
_getNumberOfSphereVertices:             ## @getNumberOfSphereVertices
	.cfi_startproc
## BB#0:
	pushq	%rbp
Lcfi16:
	.cfi_def_cfa_offset 16
Lcfi17:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Lcfi18:
	.cfi_def_cfa_register %rbp
	movq	_numVertices@GOTPCREL(%rip), %rax
	movl	(%rax), %eax
	popq	%rbp
	retq
	.cfi_endproc
                                        ## -- End function
	.globl	_getNumberOfSphereElements ## -- Begin function getNumberOfSphereElements
	.p2align	4, 0x90
_getNumberOfSphereElements:             ## @getNumberOfSphereElements
	.cfi_startproc
## BB#0:
	pushq	%rbp
Lcfi19:
	.cfi_def_cfa_offset 16
Lcfi20:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Lcfi21:
	.cfi_def_cfa_register %rbp
	movq	_numElements@GOTPCREL(%rip), %rax
	movl	(%rax), %eax
	popq	%rbp
	retq
	.cfi_endproc
                                        ## -- End function
	.section	__DATA,__data
	.globl	_indices                ## @indices
	.p2align	4
_indices:
	.long	0                       ## 0x0
	.long	1                       ## 0x1
	.long	2                       ## 0x2
	.long	0                       ## 0x0
	.long	1                       ## 0x1
	.long	2                       ## 0x2
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	1                       ## 0x1
	.long	3                       ## 0x3
	.long	2                       ## 0x2
	.long	1                       ## 0x1
	.long	3                       ## 0x3
	.long	2                       ## 0x2
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	2                       ## 0x2
	.long	3                       ## 0x3
	.long	4                       ## 0x4
	.long	2                       ## 0x2
	.long	3                       ## 0x3
	.long	4                       ## 0x4
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	3                       ## 0x3
	.long	5                       ## 0x5
	.long	4                       ## 0x4
	.long	3                       ## 0x3
	.long	5                       ## 0x5
	.long	4                       ## 0x4
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	4                       ## 0x4
	.long	5                       ## 0x5
	.long	6                       ## 0x6
	.long	4                       ## 0x4
	.long	5                       ## 0x5
	.long	6                       ## 0x6
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	5                       ## 0x5
	.long	7                       ## 0x7
	.long	6                       ## 0x6
	.long	5                       ## 0x5
	.long	7                       ## 0x7
	.long	6                       ## 0x6
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	6                       ## 0x6
	.long	7                       ## 0x7
	.long	8                       ## 0x8
	.long	6                       ## 0x6
	.long	7                       ## 0x7
	.long	8                       ## 0x8
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	7                       ## 0x7
	.long	9                       ## 0x9
	.long	8                       ## 0x8
	.long	7                       ## 0x7
	.long	9                       ## 0x9
	.long	8                       ## 0x8
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	8                       ## 0x8
	.long	9                       ## 0x9
	.long	10                      ## 0xa
	.long	8                       ## 0x8
	.long	9                       ## 0x9
	.long	10                      ## 0xa
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	9                       ## 0x9
	.long	11                      ## 0xb
	.long	10                      ## 0xa
	.long	9                       ## 0x9
	.long	11                      ## 0xb
	.long	10                      ## 0xa
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	10                      ## 0xa
	.long	11                      ## 0xb
	.long	12                      ## 0xc
	.long	10                      ## 0xa
	.long	11                      ## 0xb
	.long	12                      ## 0xc
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	11                      ## 0xb
	.long	13                      ## 0xd
	.long	12                      ## 0xc
	.long	11                      ## 0xb
	.long	13                      ## 0xd
	.long	12                      ## 0xc
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	12                      ## 0xc
	.long	13                      ## 0xd
	.long	14                      ## 0xe
	.long	12                      ## 0xc
	.long	13                      ## 0xd
	.long	14                      ## 0xe
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	13                      ## 0xd
	.long	15                      ## 0xf
	.long	14                      ## 0xe
	.long	13                      ## 0xd
	.long	15                      ## 0xf
	.long	14                      ## 0xe
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	14                      ## 0xe
	.long	15                      ## 0xf
	.long	16                      ## 0x10
	.long	14                      ## 0xe
	.long	15                      ## 0xf
	.long	16                      ## 0x10
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	15                      ## 0xf
	.long	17                      ## 0x11
	.long	16                      ## 0x10
	.long	15                      ## 0xf
	.long	17                      ## 0x11
	.long	16                      ## 0x10
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	16                      ## 0x10
	.long	17                      ## 0x11
	.long	18                      ## 0x12
	.long	16                      ## 0x10
	.long	17                      ## 0x11
	.long	18                      ## 0x12
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	17                      ## 0x11
	.long	19                      ## 0x13
	.long	18                      ## 0x12
	.long	17                      ## 0x11
	.long	19                      ## 0x13
	.long	18                      ## 0x12
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	18                      ## 0x12
	.long	19                      ## 0x13
	.long	20                      ## 0x14
	.long	18                      ## 0x12
	.long	19                      ## 0x13
	.long	20                      ## 0x14
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	19                      ## 0x13
	.long	21                      ## 0x15
	.long	20                      ## 0x14
	.long	19                      ## 0x13
	.long	21                      ## 0x15
	.long	20                      ## 0x14
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	20                      ## 0x14
	.long	21                      ## 0x15
	.long	22                      ## 0x16
	.long	20                      ## 0x14
	.long	21                      ## 0x15
	.long	22                      ## 0x16
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	21                      ## 0x15
	.long	23                      ## 0x17
	.long	22                      ## 0x16
	.long	21                      ## 0x15
	.long	23                      ## 0x17
	.long	22                      ## 0x16
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	22                      ## 0x16
	.long	23                      ## 0x17
	.long	24                      ## 0x18
	.long	22                      ## 0x16
	.long	23                      ## 0x17
	.long	24                      ## 0x18
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	23                      ## 0x17
	.long	25                      ## 0x19
	.long	24                      ## 0x18
	.long	23                      ## 0x17
	.long	25                      ## 0x19
	.long	24                      ## 0x18
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	24                      ## 0x18
	.long	25                      ## 0x19
	.long	26                      ## 0x1a
	.long	24                      ## 0x18
	.long	25                      ## 0x19
	.long	26                      ## 0x1a
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	25                      ## 0x19
	.long	27                      ## 0x1b
	.long	26                      ## 0x1a
	.long	25                      ## 0x19
	.long	27                      ## 0x1b
	.long	26                      ## 0x1a
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	26                      ## 0x1a
	.long	27                      ## 0x1b
	.long	28                      ## 0x1c
	.long	26                      ## 0x1a
	.long	27                      ## 0x1b
	.long	28                      ## 0x1c
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	27                      ## 0x1b
	.long	29                      ## 0x1d
	.long	28                      ## 0x1c
	.long	27                      ## 0x1b
	.long	29                      ## 0x1d
	.long	28                      ## 0x1c
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	28                      ## 0x1c
	.long	29                      ## 0x1d
	.long	30                      ## 0x1e
	.long	28                      ## 0x1c
	.long	29                      ## 0x1d
	.long	30                      ## 0x1e
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	29                      ## 0x1d
	.long	31                      ## 0x1f
	.long	30                      ## 0x1e
	.long	29                      ## 0x1d
	.long	31                      ## 0x1f
	.long	30                      ## 0x1e
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	30                      ## 0x1e
	.long	31                      ## 0x1f
	.long	32                      ## 0x20
	.long	30                      ## 0x1e
	.long	31                      ## 0x1f
	.long	32                      ## 0x20
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	31                      ## 0x1f
	.long	33                      ## 0x21
	.long	32                      ## 0x20
	.long	31                      ## 0x1f
	.long	33                      ## 0x21
	.long	32                      ## 0x20
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	32                      ## 0x20
	.long	33                      ## 0x21
	.long	34                      ## 0x22
	.long	32                      ## 0x20
	.long	33                      ## 0x21
	.long	34                      ## 0x22
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	33                      ## 0x21
	.long	35                      ## 0x23
	.long	34                      ## 0x22
	.long	33                      ## 0x21
	.long	35                      ## 0x23
	.long	34                      ## 0x22
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	34                      ## 0x22
	.long	35                      ## 0x23
	.long	36                      ## 0x24
	.long	34                      ## 0x22
	.long	35                      ## 0x23
	.long	36                      ## 0x24
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	35                      ## 0x23
	.long	37                      ## 0x25
	.long	36                      ## 0x24
	.long	35                      ## 0x23
	.long	37                      ## 0x25
	.long	36                      ## 0x24
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	36                      ## 0x24
	.long	37                      ## 0x25
	.long	38                      ## 0x26
	.long	36                      ## 0x24
	.long	37                      ## 0x25
	.long	38                      ## 0x26
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	37                      ## 0x25
	.long	39                      ## 0x27
	.long	38                      ## 0x26
	.long	37                      ## 0x25
	.long	39                      ## 0x27
	.long	38                      ## 0x26
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	38                      ## 0x26
	.long	39                      ## 0x27
	.long	0                       ## 0x0
	.long	38                      ## 0x26
	.long	39                      ## 0x27
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	39                      ## 0x27
	.long	1                       ## 0x1
	.long	0                       ## 0x0
	.long	39                      ## 0x27
	.long	1                       ## 0x1
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	27                      ## 0x1b
	.long	40                      ## 0x28
	.long	29                      ## 0x1d
	.long	27                      ## 0x1b
	.long	40                      ## 0x28
	.long	29                      ## 0x1d
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	41                      ## 0x29
	.long	40                      ## 0x28
	.long	27                      ## 0x1b
	.long	41                      ## 0x29
	.long	40                      ## 0x28
	.long	27                      ## 0x1b
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	25                      ## 0x19
	.long	41                      ## 0x29
	.long	27                      ## 0x1b
	.long	25                      ## 0x19
	.long	41                      ## 0x29
	.long	27                      ## 0x1b
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	42                      ## 0x2a
	.long	41                      ## 0x29
	.long	25                      ## 0x19
	.long	42                      ## 0x2a
	.long	41                      ## 0x29
	.long	25                      ## 0x19
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	23                      ## 0x17
	.long	42                      ## 0x2a
	.long	25                      ## 0x19
	.long	23                      ## 0x17
	.long	42                      ## 0x2a
	.long	25                      ## 0x19
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	43                      ## 0x2b
	.long	42                      ## 0x2a
	.long	23                      ## 0x17
	.long	43                      ## 0x2b
	.long	42                      ## 0x2a
	.long	23                      ## 0x17
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	21                      ## 0x15
	.long	43                      ## 0x2b
	.long	23                      ## 0x17
	.long	21                      ## 0x15
	.long	43                      ## 0x2b
	.long	23                      ## 0x17
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	44                      ## 0x2c
	.long	43                      ## 0x2b
	.long	21                      ## 0x15
	.long	44                      ## 0x2c
	.long	43                      ## 0x2b
	.long	21                      ## 0x15
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	19                      ## 0x13
	.long	44                      ## 0x2c
	.long	21                      ## 0x15
	.long	19                      ## 0x13
	.long	44                      ## 0x2c
	.long	21                      ## 0x15
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	45                      ## 0x2d
	.long	44                      ## 0x2c
	.long	19                      ## 0x13
	.long	45                      ## 0x2d
	.long	44                      ## 0x2c
	.long	19                      ## 0x13
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	17                      ## 0x11
	.long	45                      ## 0x2d
	.long	19                      ## 0x13
	.long	17                      ## 0x11
	.long	45                      ## 0x2d
	.long	19                      ## 0x13
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	46                      ## 0x2e
	.long	45                      ## 0x2d
	.long	17                      ## 0x11
	.long	46                      ## 0x2e
	.long	45                      ## 0x2d
	.long	17                      ## 0x11
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	15                      ## 0xf
	.long	46                      ## 0x2e
	.long	17                      ## 0x11
	.long	15                      ## 0xf
	.long	46                      ## 0x2e
	.long	17                      ## 0x11
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	47                      ## 0x2f
	.long	46                      ## 0x2e
	.long	15                      ## 0xf
	.long	47                      ## 0x2f
	.long	46                      ## 0x2e
	.long	15                      ## 0xf
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	13                      ## 0xd
	.long	47                      ## 0x2f
	.long	15                      ## 0xf
	.long	13                      ## 0xd
	.long	47                      ## 0x2f
	.long	15                      ## 0xf
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	48                      ## 0x30
	.long	47                      ## 0x2f
	.long	13                      ## 0xd
	.long	48                      ## 0x30
	.long	47                      ## 0x2f
	.long	13                      ## 0xd
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	11                      ## 0xb
	.long	48                      ## 0x30
	.long	13                      ## 0xd
	.long	11                      ## 0xb
	.long	48                      ## 0x30
	.long	13                      ## 0xd
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	49                      ## 0x31
	.long	48                      ## 0x30
	.long	11                      ## 0xb
	.long	49                      ## 0x31
	.long	48                      ## 0x30
	.long	11                      ## 0xb
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	9                       ## 0x9
	.long	49                      ## 0x31
	.long	11                      ## 0xb
	.long	9                       ## 0x9
	.long	49                      ## 0x31
	.long	11                      ## 0xb
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	50                      ## 0x32
	.long	49                      ## 0x31
	.long	9                       ## 0x9
	.long	50                      ## 0x32
	.long	49                      ## 0x31
	.long	9                       ## 0x9
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	7                       ## 0x7
	.long	50                      ## 0x32
	.long	9                       ## 0x9
	.long	7                       ## 0x7
	.long	50                      ## 0x32
	.long	9                       ## 0x9
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	51                      ## 0x33
	.long	50                      ## 0x32
	.long	7                       ## 0x7
	.long	51                      ## 0x33
	.long	50                      ## 0x32
	.long	7                       ## 0x7
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	5                       ## 0x5
	.long	51                      ## 0x33
	.long	7                       ## 0x7
	.long	5                       ## 0x5
	.long	51                      ## 0x33
	.long	7                       ## 0x7
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	52                      ## 0x34
	.long	51                      ## 0x33
	.long	5                       ## 0x5
	.long	52                      ## 0x34
	.long	51                      ## 0x33
	.long	5                       ## 0x5
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	3                       ## 0x3
	.long	52                      ## 0x34
	.long	5                       ## 0x5
	.long	3                       ## 0x3
	.long	52                      ## 0x34
	.long	5                       ## 0x5
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	53                      ## 0x35
	.long	52                      ## 0x34
	.long	3                       ## 0x3
	.long	53                      ## 0x35
	.long	52                      ## 0x34
	.long	3                       ## 0x3
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	1                       ## 0x1
	.long	53                      ## 0x35
	.long	3                       ## 0x3
	.long	1                       ## 0x1
	.long	53                      ## 0x35
	.long	3                       ## 0x3
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	54                      ## 0x36
	.long	53                      ## 0x35
	.long	1                       ## 0x1
	.long	54                      ## 0x36
	.long	53                      ## 0x35
	.long	1                       ## 0x1
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	39                      ## 0x27
	.long	54                      ## 0x36
	.long	1                       ## 0x1
	.long	39                      ## 0x27
	.long	54                      ## 0x36
	.long	1                       ## 0x1
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	55                      ## 0x37
	.long	54                      ## 0x36
	.long	39                      ## 0x27
	.long	55                      ## 0x37
	.long	54                      ## 0x36
	.long	39                      ## 0x27
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	37                      ## 0x25
	.long	55                      ## 0x37
	.long	39                      ## 0x27
	.long	37                      ## 0x25
	.long	55                      ## 0x37
	.long	39                      ## 0x27
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	56                      ## 0x38
	.long	55                      ## 0x37
	.long	37                      ## 0x25
	.long	56                      ## 0x38
	.long	55                      ## 0x37
	.long	37                      ## 0x25
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	35                      ## 0x23
	.long	56                      ## 0x38
	.long	37                      ## 0x25
	.long	35                      ## 0x23
	.long	56                      ## 0x38
	.long	37                      ## 0x25
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	57                      ## 0x39
	.long	56                      ## 0x38
	.long	35                      ## 0x23
	.long	57                      ## 0x39
	.long	56                      ## 0x38
	.long	35                      ## 0x23
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	33                      ## 0x21
	.long	57                      ## 0x39
	.long	35                      ## 0x23
	.long	33                      ## 0x21
	.long	57                      ## 0x39
	.long	35                      ## 0x23
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	58                      ## 0x3a
	.long	57                      ## 0x39
	.long	33                      ## 0x21
	.long	58                      ## 0x3a
	.long	57                      ## 0x39
	.long	33                      ## 0x21
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	31                      ## 0x1f
	.long	58                      ## 0x3a
	.long	33                      ## 0x21
	.long	31                      ## 0x1f
	.long	58                      ## 0x3a
	.long	33                      ## 0x21
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	59                      ## 0x3b
	.long	58                      ## 0x3a
	.long	31                      ## 0x1f
	.long	59                      ## 0x3b
	.long	58                      ## 0x3a
	.long	31                      ## 0x1f
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	29                      ## 0x1d
	.long	59                      ## 0x3b
	.long	31                      ## 0x1f
	.long	29                      ## 0x1d
	.long	59                      ## 0x3b
	.long	31                      ## 0x1f
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	40                      ## 0x28
	.long	59                      ## 0x3b
	.long	29                      ## 0x1d
	.long	40                      ## 0x28
	.long	59                      ## 0x3b
	.long	29                      ## 0x1d
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	40                      ## 0x28
	.long	60                      ## 0x3c
	.long	59                      ## 0x3b
	.long	40                      ## 0x28
	.long	60                      ## 0x3c
	.long	59                      ## 0x3b
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	61                      ## 0x3d
	.long	60                      ## 0x3c
	.long	40                      ## 0x28
	.long	61                      ## 0x3d
	.long	60                      ## 0x3c
	.long	40                      ## 0x28
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	41                      ## 0x29
	.long	61                      ## 0x3d
	.long	40                      ## 0x28
	.long	41                      ## 0x29
	.long	61                      ## 0x3d
	.long	40                      ## 0x28
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	62                      ## 0x3e
	.long	61                      ## 0x3d
	.long	41                      ## 0x29
	.long	62                      ## 0x3e
	.long	61                      ## 0x3d
	.long	41                      ## 0x29
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	42                      ## 0x2a
	.long	62                      ## 0x3e
	.long	41                      ## 0x29
	.long	42                      ## 0x2a
	.long	62                      ## 0x3e
	.long	41                      ## 0x29
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	63                      ## 0x3f
	.long	62                      ## 0x3e
	.long	42                      ## 0x2a
	.long	63                      ## 0x3f
	.long	62                      ## 0x3e
	.long	42                      ## 0x2a
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	43                      ## 0x2b
	.long	63                      ## 0x3f
	.long	42                      ## 0x2a
	.long	43                      ## 0x2b
	.long	63                      ## 0x3f
	.long	42                      ## 0x2a
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	64                      ## 0x40
	.long	63                      ## 0x3f
	.long	43                      ## 0x2b
	.long	64                      ## 0x40
	.long	63                      ## 0x3f
	.long	43                      ## 0x2b
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	44                      ## 0x2c
	.long	64                      ## 0x40
	.long	43                      ## 0x2b
	.long	44                      ## 0x2c
	.long	64                      ## 0x40
	.long	43                      ## 0x2b
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	65                      ## 0x41
	.long	64                      ## 0x40
	.long	44                      ## 0x2c
	.long	65                      ## 0x41
	.long	64                      ## 0x40
	.long	44                      ## 0x2c
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	45                      ## 0x2d
	.long	65                      ## 0x41
	.long	44                      ## 0x2c
	.long	45                      ## 0x2d
	.long	65                      ## 0x41
	.long	44                      ## 0x2c
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	66                      ## 0x42
	.long	65                      ## 0x41
	.long	45                      ## 0x2d
	.long	66                      ## 0x42
	.long	65                      ## 0x41
	.long	45                      ## 0x2d
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	46                      ## 0x2e
	.long	66                      ## 0x42
	.long	45                      ## 0x2d
	.long	46                      ## 0x2e
	.long	66                      ## 0x42
	.long	45                      ## 0x2d
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	67                      ## 0x43
	.long	66                      ## 0x42
	.long	46                      ## 0x2e
	.long	67                      ## 0x43
	.long	66                      ## 0x42
	.long	46                      ## 0x2e
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	47                      ## 0x2f
	.long	67                      ## 0x43
	.long	46                      ## 0x2e
	.long	47                      ## 0x2f
	.long	67                      ## 0x43
	.long	46                      ## 0x2e
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	68                      ## 0x44
	.long	67                      ## 0x43
	.long	47                      ## 0x2f
	.long	68                      ## 0x44
	.long	67                      ## 0x43
	.long	47                      ## 0x2f
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	48                      ## 0x30
	.long	68                      ## 0x44
	.long	47                      ## 0x2f
	.long	48                      ## 0x30
	.long	68                      ## 0x44
	.long	47                      ## 0x2f
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	69                      ## 0x45
	.long	68                      ## 0x44
	.long	48                      ## 0x30
	.long	69                      ## 0x45
	.long	68                      ## 0x44
	.long	48                      ## 0x30
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	49                      ## 0x31
	.long	69                      ## 0x45
	.long	48                      ## 0x30
	.long	49                      ## 0x31
	.long	69                      ## 0x45
	.long	48                      ## 0x30
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	70                      ## 0x46
	.long	69                      ## 0x45
	.long	49                      ## 0x31
	.long	70                      ## 0x46
	.long	69                      ## 0x45
	.long	49                      ## 0x31
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	50                      ## 0x32
	.long	70                      ## 0x46
	.long	49                      ## 0x31
	.long	50                      ## 0x32
	.long	70                      ## 0x46
	.long	49                      ## 0x31
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	71                      ## 0x47
	.long	70                      ## 0x46
	.long	50                      ## 0x32
	.long	71                      ## 0x47
	.long	70                      ## 0x46
	.long	50                      ## 0x32
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	51                      ## 0x33
	.long	71                      ## 0x47
	.long	50                      ## 0x32
	.long	51                      ## 0x33
	.long	71                      ## 0x47
	.long	50                      ## 0x32
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	72                      ## 0x48
	.long	71                      ## 0x47
	.long	51                      ## 0x33
	.long	72                      ## 0x48
	.long	71                      ## 0x47
	.long	51                      ## 0x33
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	52                      ## 0x34
	.long	72                      ## 0x48
	.long	51                      ## 0x33
	.long	52                      ## 0x34
	.long	72                      ## 0x48
	.long	51                      ## 0x33
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	73                      ## 0x49
	.long	72                      ## 0x48
	.long	52                      ## 0x34
	.long	73                      ## 0x49
	.long	72                      ## 0x48
	.long	52                      ## 0x34
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	53                      ## 0x35
	.long	73                      ## 0x49
	.long	52                      ## 0x34
	.long	53                      ## 0x35
	.long	73                      ## 0x49
	.long	52                      ## 0x34
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	74                      ## 0x4a
	.long	73                      ## 0x49
	.long	53                      ## 0x35
	.long	74                      ## 0x4a
	.long	73                      ## 0x49
	.long	53                      ## 0x35
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	54                      ## 0x36
	.long	74                      ## 0x4a
	.long	53                      ## 0x35
	.long	54                      ## 0x36
	.long	74                      ## 0x4a
	.long	53                      ## 0x35
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	75                      ## 0x4b
	.long	74                      ## 0x4a
	.long	54                      ## 0x36
	.long	75                      ## 0x4b
	.long	74                      ## 0x4a
	.long	54                      ## 0x36
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	55                      ## 0x37
	.long	75                      ## 0x4b
	.long	54                      ## 0x36
	.long	55                      ## 0x37
	.long	75                      ## 0x4b
	.long	54                      ## 0x36
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	76                      ## 0x4c
	.long	75                      ## 0x4b
	.long	55                      ## 0x37
	.long	76                      ## 0x4c
	.long	75                      ## 0x4b
	.long	55                      ## 0x37
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	56                      ## 0x38
	.long	76                      ## 0x4c
	.long	55                      ## 0x37
	.long	56                      ## 0x38
	.long	76                      ## 0x4c
	.long	55                      ## 0x37
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	77                      ## 0x4d
	.long	76                      ## 0x4c
	.long	56                      ## 0x38
	.long	77                      ## 0x4d
	.long	76                      ## 0x4c
	.long	56                      ## 0x38
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	57                      ## 0x39
	.long	77                      ## 0x4d
	.long	56                      ## 0x38
	.long	57                      ## 0x39
	.long	77                      ## 0x4d
	.long	56                      ## 0x38
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	78                      ## 0x4e
	.long	77                      ## 0x4d
	.long	57                      ## 0x39
	.long	78                      ## 0x4e
	.long	77                      ## 0x4d
	.long	57                      ## 0x39
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	58                      ## 0x3a
	.long	78                      ## 0x4e
	.long	57                      ## 0x39
	.long	58                      ## 0x3a
	.long	78                      ## 0x4e
	.long	57                      ## 0x39
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	79                      ## 0x4f
	.long	78                      ## 0x4e
	.long	58                      ## 0x3a
	.long	79                      ## 0x4f
	.long	78                      ## 0x4e
	.long	58                      ## 0x3a
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	59                      ## 0x3b
	.long	79                      ## 0x4f
	.long	58                      ## 0x3a
	.long	59                      ## 0x3b
	.long	79                      ## 0x4f
	.long	58                      ## 0x3a
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	60                      ## 0x3c
	.long	79                      ## 0x4f
	.long	59                      ## 0x3b
	.long	60                      ## 0x3c
	.long	79                      ## 0x4f
	.long	59                      ## 0x3b
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	60                      ## 0x3c
	.long	80                      ## 0x50
	.long	79                      ## 0x4f
	.long	60                      ## 0x3c
	.long	80                      ## 0x50
	.long	79                      ## 0x4f
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	81                      ## 0x51
	.long	80                      ## 0x50
	.long	60                      ## 0x3c
	.long	81                      ## 0x51
	.long	80                      ## 0x50
	.long	60                      ## 0x3c
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	61                      ## 0x3d
	.long	81                      ## 0x51
	.long	60                      ## 0x3c
	.long	61                      ## 0x3d
	.long	81                      ## 0x51
	.long	60                      ## 0x3c
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	82                      ## 0x52
	.long	81                      ## 0x51
	.long	61                      ## 0x3d
	.long	82                      ## 0x52
	.long	81                      ## 0x51
	.long	61                      ## 0x3d
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	62                      ## 0x3e
	.long	82                      ## 0x52
	.long	61                      ## 0x3d
	.long	62                      ## 0x3e
	.long	82                      ## 0x52
	.long	61                      ## 0x3d
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	83                      ## 0x53
	.long	82                      ## 0x52
	.long	62                      ## 0x3e
	.long	83                      ## 0x53
	.long	82                      ## 0x52
	.long	62                      ## 0x3e
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	63                      ## 0x3f
	.long	83                      ## 0x53
	.long	62                      ## 0x3e
	.long	63                      ## 0x3f
	.long	83                      ## 0x53
	.long	62                      ## 0x3e
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	84                      ## 0x54
	.long	83                      ## 0x53
	.long	63                      ## 0x3f
	.long	84                      ## 0x54
	.long	83                      ## 0x53
	.long	63                      ## 0x3f
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	64                      ## 0x40
	.long	84                      ## 0x54
	.long	63                      ## 0x3f
	.long	64                      ## 0x40
	.long	84                      ## 0x54
	.long	63                      ## 0x3f
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	85                      ## 0x55
	.long	84                      ## 0x54
	.long	64                      ## 0x40
	.long	85                      ## 0x55
	.long	84                      ## 0x54
	.long	64                      ## 0x40
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	65                      ## 0x41
	.long	85                      ## 0x55
	.long	64                      ## 0x40
	.long	65                      ## 0x41
	.long	85                      ## 0x55
	.long	64                      ## 0x40
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	86                      ## 0x56
	.long	85                      ## 0x55
	.long	65                      ## 0x41
	.long	86                      ## 0x56
	.long	85                      ## 0x55
	.long	65                      ## 0x41
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	66                      ## 0x42
	.long	86                      ## 0x56
	.long	65                      ## 0x41
	.long	66                      ## 0x42
	.long	86                      ## 0x56
	.long	65                      ## 0x41
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	87                      ## 0x57
	.long	86                      ## 0x56
	.long	66                      ## 0x42
	.long	87                      ## 0x57
	.long	86                      ## 0x56
	.long	66                      ## 0x42
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	67                      ## 0x43
	.long	87                      ## 0x57
	.long	66                      ## 0x42
	.long	67                      ## 0x43
	.long	87                      ## 0x57
	.long	66                      ## 0x42
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	88                      ## 0x58
	.long	87                      ## 0x57
	.long	67                      ## 0x43
	.long	88                      ## 0x58
	.long	87                      ## 0x57
	.long	67                      ## 0x43
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	68                      ## 0x44
	.long	88                      ## 0x58
	.long	67                      ## 0x43
	.long	68                      ## 0x44
	.long	88                      ## 0x58
	.long	67                      ## 0x43
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	89                      ## 0x59
	.long	88                      ## 0x58
	.long	68                      ## 0x44
	.long	89                      ## 0x59
	.long	88                      ## 0x58
	.long	68                      ## 0x44
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	69                      ## 0x45
	.long	89                      ## 0x59
	.long	68                      ## 0x44
	.long	69                      ## 0x45
	.long	89                      ## 0x59
	.long	68                      ## 0x44
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	90                      ## 0x5a
	.long	89                      ## 0x59
	.long	69                      ## 0x45
	.long	90                      ## 0x5a
	.long	89                      ## 0x59
	.long	69                      ## 0x45
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	70                      ## 0x46
	.long	90                      ## 0x5a
	.long	69                      ## 0x45
	.long	70                      ## 0x46
	.long	90                      ## 0x5a
	.long	69                      ## 0x45
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	91                      ## 0x5b
	.long	90                      ## 0x5a
	.long	70                      ## 0x46
	.long	91                      ## 0x5b
	.long	90                      ## 0x5a
	.long	70                      ## 0x46
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	71                      ## 0x47
	.long	91                      ## 0x5b
	.long	70                      ## 0x46
	.long	71                      ## 0x47
	.long	91                      ## 0x5b
	.long	70                      ## 0x46
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	92                      ## 0x5c
	.long	91                      ## 0x5b
	.long	71                      ## 0x47
	.long	92                      ## 0x5c
	.long	91                      ## 0x5b
	.long	71                      ## 0x47
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	72                      ## 0x48
	.long	92                      ## 0x5c
	.long	71                      ## 0x47
	.long	72                      ## 0x48
	.long	92                      ## 0x5c
	.long	71                      ## 0x47
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	93                      ## 0x5d
	.long	92                      ## 0x5c
	.long	72                      ## 0x48
	.long	93                      ## 0x5d
	.long	92                      ## 0x5c
	.long	72                      ## 0x48
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	73                      ## 0x49
	.long	93                      ## 0x5d
	.long	72                      ## 0x48
	.long	73                      ## 0x49
	.long	93                      ## 0x5d
	.long	72                      ## 0x48
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	94                      ## 0x5e
	.long	93                      ## 0x5d
	.long	73                      ## 0x49
	.long	94                      ## 0x5e
	.long	93                      ## 0x5d
	.long	73                      ## 0x49
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	74                      ## 0x4a
	.long	94                      ## 0x5e
	.long	73                      ## 0x49
	.long	74                      ## 0x4a
	.long	94                      ## 0x5e
	.long	73                      ## 0x49
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	95                      ## 0x5f
	.long	94                      ## 0x5e
	.long	74                      ## 0x4a
	.long	95                      ## 0x5f
	.long	94                      ## 0x5e
	.long	74                      ## 0x4a
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	75                      ## 0x4b
	.long	95                      ## 0x5f
	.long	74                      ## 0x4a
	.long	75                      ## 0x4b
	.long	95                      ## 0x5f
	.long	74                      ## 0x4a
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	96                      ## 0x60
	.long	95                      ## 0x5f
	.long	75                      ## 0x4b
	.long	96                      ## 0x60
	.long	95                      ## 0x5f
	.long	75                      ## 0x4b
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	76                      ## 0x4c
	.long	96                      ## 0x60
	.long	75                      ## 0x4b
	.long	76                      ## 0x4c
	.long	96                      ## 0x60
	.long	75                      ## 0x4b
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	97                      ## 0x61
	.long	96                      ## 0x60
	.long	76                      ## 0x4c
	.long	97                      ## 0x61
	.long	96                      ## 0x60
	.long	76                      ## 0x4c
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	77                      ## 0x4d
	.long	97                      ## 0x61
	.long	76                      ## 0x4c
	.long	77                      ## 0x4d
	.long	97                      ## 0x61
	.long	76                      ## 0x4c
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	98                      ## 0x62
	.long	97                      ## 0x61
	.long	77                      ## 0x4d
	.long	98                      ## 0x62
	.long	97                      ## 0x61
	.long	77                      ## 0x4d
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	78                      ## 0x4e
	.long	98                      ## 0x62
	.long	77                      ## 0x4d
	.long	78                      ## 0x4e
	.long	98                      ## 0x62
	.long	77                      ## 0x4d
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	99                      ## 0x63
	.long	98                      ## 0x62
	.long	78                      ## 0x4e
	.long	99                      ## 0x63
	.long	98                      ## 0x62
	.long	78                      ## 0x4e
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	79                      ## 0x4f
	.long	99                      ## 0x63
	.long	78                      ## 0x4e
	.long	79                      ## 0x4f
	.long	99                      ## 0x63
	.long	78                      ## 0x4e
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	80                      ## 0x50
	.long	99                      ## 0x63
	.long	79                      ## 0x4f
	.long	80                      ## 0x50
	.long	99                      ## 0x63
	.long	79                      ## 0x4f
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	80                      ## 0x50
	.long	100                     ## 0x64
	.long	99                      ## 0x63
	.long	80                      ## 0x50
	.long	100                     ## 0x64
	.long	99                      ## 0x63
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	101                     ## 0x65
	.long	100                     ## 0x64
	.long	80                      ## 0x50
	.long	101                     ## 0x65
	.long	100                     ## 0x64
	.long	80                      ## 0x50
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	81                      ## 0x51
	.long	101                     ## 0x65
	.long	80                      ## 0x50
	.long	81                      ## 0x51
	.long	101                     ## 0x65
	.long	80                      ## 0x50
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	102                     ## 0x66
	.long	101                     ## 0x65
	.long	81                      ## 0x51
	.long	102                     ## 0x66
	.long	101                     ## 0x65
	.long	81                      ## 0x51
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	82                      ## 0x52
	.long	102                     ## 0x66
	.long	81                      ## 0x51
	.long	82                      ## 0x52
	.long	102                     ## 0x66
	.long	81                      ## 0x51
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	103                     ## 0x67
	.long	102                     ## 0x66
	.long	82                      ## 0x52
	.long	103                     ## 0x67
	.long	102                     ## 0x66
	.long	82                      ## 0x52
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	83                      ## 0x53
	.long	103                     ## 0x67
	.long	82                      ## 0x52
	.long	83                      ## 0x53
	.long	103                     ## 0x67
	.long	82                      ## 0x52
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	104                     ## 0x68
	.long	103                     ## 0x67
	.long	83                      ## 0x53
	.long	104                     ## 0x68
	.long	103                     ## 0x67
	.long	83                      ## 0x53
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	84                      ## 0x54
	.long	104                     ## 0x68
	.long	83                      ## 0x53
	.long	84                      ## 0x54
	.long	104                     ## 0x68
	.long	83                      ## 0x53
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	105                     ## 0x69
	.long	104                     ## 0x68
	.long	84                      ## 0x54
	.long	105                     ## 0x69
	.long	104                     ## 0x68
	.long	84                      ## 0x54
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	85                      ## 0x55
	.long	105                     ## 0x69
	.long	84                      ## 0x54
	.long	85                      ## 0x55
	.long	105                     ## 0x69
	.long	84                      ## 0x54
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	106                     ## 0x6a
	.long	105                     ## 0x69
	.long	85                      ## 0x55
	.long	106                     ## 0x6a
	.long	105                     ## 0x69
	.long	85                      ## 0x55
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	86                      ## 0x56
	.long	106                     ## 0x6a
	.long	85                      ## 0x55
	.long	86                      ## 0x56
	.long	106                     ## 0x6a
	.long	85                      ## 0x55
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	107                     ## 0x6b
	.long	106                     ## 0x6a
	.long	86                      ## 0x56
	.long	107                     ## 0x6b
	.long	106                     ## 0x6a
	.long	86                      ## 0x56
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	87                      ## 0x57
	.long	107                     ## 0x6b
	.long	86                      ## 0x56
	.long	87                      ## 0x57
	.long	107                     ## 0x6b
	.long	86                      ## 0x56
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	108                     ## 0x6c
	.long	107                     ## 0x6b
	.long	87                      ## 0x57
	.long	108                     ## 0x6c
	.long	107                     ## 0x6b
	.long	87                      ## 0x57
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	88                      ## 0x58
	.long	108                     ## 0x6c
	.long	87                      ## 0x57
	.long	88                      ## 0x58
	.long	108                     ## 0x6c
	.long	87                      ## 0x57
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	109                     ## 0x6d
	.long	108                     ## 0x6c
	.long	88                      ## 0x58
	.long	109                     ## 0x6d
	.long	108                     ## 0x6c
	.long	88                      ## 0x58
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	89                      ## 0x59
	.long	109                     ## 0x6d
	.long	88                      ## 0x58
	.long	89                      ## 0x59
	.long	109                     ## 0x6d
	.long	88                      ## 0x58
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	110                     ## 0x6e
	.long	109                     ## 0x6d
	.long	89                      ## 0x59
	.long	110                     ## 0x6e
	.long	109                     ## 0x6d
	.long	89                      ## 0x59
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	90                      ## 0x5a
	.long	110                     ## 0x6e
	.long	89                      ## 0x59
	.long	90                      ## 0x5a
	.long	110                     ## 0x6e
	.long	89                      ## 0x59
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	111                     ## 0x6f
	.long	110                     ## 0x6e
	.long	90                      ## 0x5a
	.long	111                     ## 0x6f
	.long	110                     ## 0x6e
	.long	90                      ## 0x5a
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	91                      ## 0x5b
	.long	111                     ## 0x6f
	.long	90                      ## 0x5a
	.long	91                      ## 0x5b
	.long	111                     ## 0x6f
	.long	90                      ## 0x5a
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	112                     ## 0x70
	.long	111                     ## 0x6f
	.long	91                      ## 0x5b
	.long	112                     ## 0x70
	.long	111                     ## 0x6f
	.long	91                      ## 0x5b
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	92                      ## 0x5c
	.long	112                     ## 0x70
	.long	91                      ## 0x5b
	.long	92                      ## 0x5c
	.long	112                     ## 0x70
	.long	91                      ## 0x5b
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	113                     ## 0x71
	.long	112                     ## 0x70
	.long	92                      ## 0x5c
	.long	113                     ## 0x71
	.long	112                     ## 0x70
	.long	92                      ## 0x5c
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	93                      ## 0x5d
	.long	113                     ## 0x71
	.long	92                      ## 0x5c
	.long	93                      ## 0x5d
	.long	113                     ## 0x71
	.long	92                      ## 0x5c
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	114                     ## 0x72
	.long	113                     ## 0x71
	.long	93                      ## 0x5d
	.long	114                     ## 0x72
	.long	113                     ## 0x71
	.long	93                      ## 0x5d
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	94                      ## 0x5e
	.long	114                     ## 0x72
	.long	93                      ## 0x5d
	.long	94                      ## 0x5e
	.long	114                     ## 0x72
	.long	93                      ## 0x5d
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	115                     ## 0x73
	.long	114                     ## 0x72
	.long	94                      ## 0x5e
	.long	115                     ## 0x73
	.long	114                     ## 0x72
	.long	94                      ## 0x5e
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	95                      ## 0x5f
	.long	115                     ## 0x73
	.long	94                      ## 0x5e
	.long	95                      ## 0x5f
	.long	115                     ## 0x73
	.long	94                      ## 0x5e
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	116                     ## 0x74
	.long	115                     ## 0x73
	.long	95                      ## 0x5f
	.long	116                     ## 0x74
	.long	115                     ## 0x73
	.long	95                      ## 0x5f
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	96                      ## 0x60
	.long	116                     ## 0x74
	.long	95                      ## 0x5f
	.long	96                      ## 0x60
	.long	116                     ## 0x74
	.long	95                      ## 0x5f
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	117                     ## 0x75
	.long	116                     ## 0x74
	.long	96                      ## 0x60
	.long	117                     ## 0x75
	.long	116                     ## 0x74
	.long	96                      ## 0x60
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	97                      ## 0x61
	.long	117                     ## 0x75
	.long	96                      ## 0x60
	.long	97                      ## 0x61
	.long	117                     ## 0x75
	.long	96                      ## 0x60
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	118                     ## 0x76
	.long	117                     ## 0x75
	.long	97                      ## 0x61
	.long	118                     ## 0x76
	.long	117                     ## 0x75
	.long	97                      ## 0x61
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	98                      ## 0x62
	.long	118                     ## 0x76
	.long	97                      ## 0x61
	.long	98                      ## 0x62
	.long	118                     ## 0x76
	.long	97                      ## 0x61
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	119                     ## 0x77
	.long	118                     ## 0x76
	.long	98                      ## 0x62
	.long	119                     ## 0x77
	.long	118                     ## 0x76
	.long	98                      ## 0x62
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	99                      ## 0x63
	.long	119                     ## 0x77
	.long	98                      ## 0x62
	.long	99                      ## 0x63
	.long	119                     ## 0x77
	.long	98                      ## 0x62
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	100                     ## 0x64
	.long	119                     ## 0x77
	.long	99                      ## 0x63
	.long	100                     ## 0x64
	.long	119                     ## 0x77
	.long	99                      ## 0x63
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	100                     ## 0x64
	.long	120                     ## 0x78
	.long	119                     ## 0x77
	.long	100                     ## 0x64
	.long	120                     ## 0x78
	.long	119                     ## 0x77
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	121                     ## 0x79
	.long	120                     ## 0x78
	.long	100                     ## 0x64
	.long	121                     ## 0x79
	.long	120                     ## 0x78
	.long	100                     ## 0x64
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	101                     ## 0x65
	.long	121                     ## 0x79
	.long	100                     ## 0x64
	.long	101                     ## 0x65
	.long	121                     ## 0x79
	.long	100                     ## 0x64
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	122                     ## 0x7a
	.long	121                     ## 0x79
	.long	101                     ## 0x65
	.long	122                     ## 0x7a
	.long	121                     ## 0x79
	.long	101                     ## 0x65
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	102                     ## 0x66
	.long	122                     ## 0x7a
	.long	101                     ## 0x65
	.long	102                     ## 0x66
	.long	122                     ## 0x7a
	.long	101                     ## 0x65
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	123                     ## 0x7b
	.long	122                     ## 0x7a
	.long	102                     ## 0x66
	.long	123                     ## 0x7b
	.long	122                     ## 0x7a
	.long	102                     ## 0x66
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	103                     ## 0x67
	.long	123                     ## 0x7b
	.long	102                     ## 0x66
	.long	103                     ## 0x67
	.long	123                     ## 0x7b
	.long	102                     ## 0x66
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	124                     ## 0x7c
	.long	123                     ## 0x7b
	.long	103                     ## 0x67
	.long	124                     ## 0x7c
	.long	123                     ## 0x7b
	.long	103                     ## 0x67
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	104                     ## 0x68
	.long	124                     ## 0x7c
	.long	103                     ## 0x67
	.long	104                     ## 0x68
	.long	124                     ## 0x7c
	.long	103                     ## 0x67
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	125                     ## 0x7d
	.long	124                     ## 0x7c
	.long	104                     ## 0x68
	.long	125                     ## 0x7d
	.long	124                     ## 0x7c
	.long	104                     ## 0x68
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	105                     ## 0x69
	.long	125                     ## 0x7d
	.long	104                     ## 0x68
	.long	105                     ## 0x69
	.long	125                     ## 0x7d
	.long	104                     ## 0x68
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	126                     ## 0x7e
	.long	125                     ## 0x7d
	.long	105                     ## 0x69
	.long	126                     ## 0x7e
	.long	125                     ## 0x7d
	.long	105                     ## 0x69
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	106                     ## 0x6a
	.long	126                     ## 0x7e
	.long	105                     ## 0x69
	.long	106                     ## 0x6a
	.long	126                     ## 0x7e
	.long	105                     ## 0x69
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	127                     ## 0x7f
	.long	126                     ## 0x7e
	.long	106                     ## 0x6a
	.long	127                     ## 0x7f
	.long	126                     ## 0x7e
	.long	106                     ## 0x6a
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	107                     ## 0x6b
	.long	127                     ## 0x7f
	.long	106                     ## 0x6a
	.long	107                     ## 0x6b
	.long	127                     ## 0x7f
	.long	106                     ## 0x6a
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	128                     ## 0x80
	.long	127                     ## 0x7f
	.long	107                     ## 0x6b
	.long	128                     ## 0x80
	.long	127                     ## 0x7f
	.long	107                     ## 0x6b
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	108                     ## 0x6c
	.long	128                     ## 0x80
	.long	107                     ## 0x6b
	.long	108                     ## 0x6c
	.long	128                     ## 0x80
	.long	107                     ## 0x6b
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	129                     ## 0x81
	.long	128                     ## 0x80
	.long	108                     ## 0x6c
	.long	129                     ## 0x81
	.long	128                     ## 0x80
	.long	108                     ## 0x6c
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	109                     ## 0x6d
	.long	129                     ## 0x81
	.long	108                     ## 0x6c
	.long	109                     ## 0x6d
	.long	129                     ## 0x81
	.long	108                     ## 0x6c
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	130                     ## 0x82
	.long	129                     ## 0x81
	.long	109                     ## 0x6d
	.long	130                     ## 0x82
	.long	129                     ## 0x81
	.long	109                     ## 0x6d
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	110                     ## 0x6e
	.long	130                     ## 0x82
	.long	109                     ## 0x6d
	.long	110                     ## 0x6e
	.long	130                     ## 0x82
	.long	109                     ## 0x6d
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	131                     ## 0x83
	.long	130                     ## 0x82
	.long	110                     ## 0x6e
	.long	131                     ## 0x83
	.long	130                     ## 0x82
	.long	110                     ## 0x6e
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	111                     ## 0x6f
	.long	131                     ## 0x83
	.long	110                     ## 0x6e
	.long	111                     ## 0x6f
	.long	131                     ## 0x83
	.long	110                     ## 0x6e
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	132                     ## 0x84
	.long	131                     ## 0x83
	.long	111                     ## 0x6f
	.long	132                     ## 0x84
	.long	131                     ## 0x83
	.long	111                     ## 0x6f
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	112                     ## 0x70
	.long	132                     ## 0x84
	.long	111                     ## 0x6f
	.long	112                     ## 0x70
	.long	132                     ## 0x84
	.long	111                     ## 0x6f
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	133                     ## 0x85
	.long	132                     ## 0x84
	.long	112                     ## 0x70
	.long	133                     ## 0x85
	.long	132                     ## 0x84
	.long	112                     ## 0x70
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	113                     ## 0x71
	.long	133                     ## 0x85
	.long	112                     ## 0x70
	.long	113                     ## 0x71
	.long	133                     ## 0x85
	.long	112                     ## 0x70
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	134                     ## 0x86
	.long	133                     ## 0x85
	.long	113                     ## 0x71
	.long	134                     ## 0x86
	.long	133                     ## 0x85
	.long	113                     ## 0x71
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	114                     ## 0x72
	.long	134                     ## 0x86
	.long	113                     ## 0x71
	.long	114                     ## 0x72
	.long	134                     ## 0x86
	.long	113                     ## 0x71
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	135                     ## 0x87
	.long	134                     ## 0x86
	.long	114                     ## 0x72
	.long	135                     ## 0x87
	.long	134                     ## 0x86
	.long	114                     ## 0x72
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	115                     ## 0x73
	.long	135                     ## 0x87
	.long	114                     ## 0x72
	.long	115                     ## 0x73
	.long	135                     ## 0x87
	.long	114                     ## 0x72
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	136                     ## 0x88
	.long	135                     ## 0x87
	.long	115                     ## 0x73
	.long	136                     ## 0x88
	.long	135                     ## 0x87
	.long	115                     ## 0x73
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	116                     ## 0x74
	.long	136                     ## 0x88
	.long	115                     ## 0x73
	.long	116                     ## 0x74
	.long	136                     ## 0x88
	.long	115                     ## 0x73
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	137                     ## 0x89
	.long	136                     ## 0x88
	.long	116                     ## 0x74
	.long	137                     ## 0x89
	.long	136                     ## 0x88
	.long	116                     ## 0x74
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	117                     ## 0x75
	.long	137                     ## 0x89
	.long	116                     ## 0x74
	.long	117                     ## 0x75
	.long	137                     ## 0x89
	.long	116                     ## 0x74
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	138                     ## 0x8a
	.long	137                     ## 0x89
	.long	117                     ## 0x75
	.long	138                     ## 0x8a
	.long	137                     ## 0x89
	.long	117                     ## 0x75
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	118                     ## 0x76
	.long	138                     ## 0x8a
	.long	117                     ## 0x75
	.long	118                     ## 0x76
	.long	138                     ## 0x8a
	.long	117                     ## 0x75
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	139                     ## 0x8b
	.long	138                     ## 0x8a
	.long	118                     ## 0x76
	.long	139                     ## 0x8b
	.long	138                     ## 0x8a
	.long	118                     ## 0x76
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	119                     ## 0x77
	.long	139                     ## 0x8b
	.long	118                     ## 0x76
	.long	119                     ## 0x77
	.long	139                     ## 0x8b
	.long	118                     ## 0x76
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	120                     ## 0x78
	.long	139                     ## 0x8b
	.long	119                     ## 0x77
	.long	120                     ## 0x78
	.long	139                     ## 0x8b
	.long	119                     ## 0x77
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	120                     ## 0x78
	.long	140                     ## 0x8c
	.long	139                     ## 0x8b
	.long	120                     ## 0x78
	.long	140                     ## 0x8c
	.long	139                     ## 0x8b
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	141                     ## 0x8d
	.long	140                     ## 0x8c
	.long	120                     ## 0x78
	.long	141                     ## 0x8d
	.long	140                     ## 0x8c
	.long	120                     ## 0x78
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	121                     ## 0x79
	.long	141                     ## 0x8d
	.long	120                     ## 0x78
	.long	121                     ## 0x79
	.long	141                     ## 0x8d
	.long	120                     ## 0x78
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	142                     ## 0x8e
	.long	141                     ## 0x8d
	.long	121                     ## 0x79
	.long	142                     ## 0x8e
	.long	141                     ## 0x8d
	.long	121                     ## 0x79
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	122                     ## 0x7a
	.long	142                     ## 0x8e
	.long	121                     ## 0x79
	.long	122                     ## 0x7a
	.long	142                     ## 0x8e
	.long	121                     ## 0x79
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	143                     ## 0x8f
	.long	142                     ## 0x8e
	.long	122                     ## 0x7a
	.long	143                     ## 0x8f
	.long	142                     ## 0x8e
	.long	122                     ## 0x7a
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	123                     ## 0x7b
	.long	143                     ## 0x8f
	.long	122                     ## 0x7a
	.long	123                     ## 0x7b
	.long	143                     ## 0x8f
	.long	122                     ## 0x7a
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	144                     ## 0x90
	.long	143                     ## 0x8f
	.long	123                     ## 0x7b
	.long	144                     ## 0x90
	.long	143                     ## 0x8f
	.long	123                     ## 0x7b
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	124                     ## 0x7c
	.long	144                     ## 0x90
	.long	123                     ## 0x7b
	.long	124                     ## 0x7c
	.long	144                     ## 0x90
	.long	123                     ## 0x7b
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	145                     ## 0x91
	.long	144                     ## 0x90
	.long	124                     ## 0x7c
	.long	145                     ## 0x91
	.long	144                     ## 0x90
	.long	124                     ## 0x7c
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	125                     ## 0x7d
	.long	145                     ## 0x91
	.long	124                     ## 0x7c
	.long	125                     ## 0x7d
	.long	145                     ## 0x91
	.long	124                     ## 0x7c
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	146                     ## 0x92
	.long	145                     ## 0x91
	.long	125                     ## 0x7d
	.long	146                     ## 0x92
	.long	145                     ## 0x91
	.long	125                     ## 0x7d
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	126                     ## 0x7e
	.long	146                     ## 0x92
	.long	125                     ## 0x7d
	.long	126                     ## 0x7e
	.long	146                     ## 0x92
	.long	125                     ## 0x7d
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	147                     ## 0x93
	.long	146                     ## 0x92
	.long	126                     ## 0x7e
	.long	147                     ## 0x93
	.long	146                     ## 0x92
	.long	126                     ## 0x7e
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	127                     ## 0x7f
	.long	147                     ## 0x93
	.long	126                     ## 0x7e
	.long	127                     ## 0x7f
	.long	147                     ## 0x93
	.long	126                     ## 0x7e
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	148                     ## 0x94
	.long	147                     ## 0x93
	.long	127                     ## 0x7f
	.long	148                     ## 0x94
	.long	147                     ## 0x93
	.long	127                     ## 0x7f
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	128                     ## 0x80
	.long	148                     ## 0x94
	.long	127                     ## 0x7f
	.long	128                     ## 0x80
	.long	148                     ## 0x94
	.long	127                     ## 0x7f
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	149                     ## 0x95
	.long	148                     ## 0x94
	.long	128                     ## 0x80
	.long	149                     ## 0x95
	.long	148                     ## 0x94
	.long	128                     ## 0x80
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	129                     ## 0x81
	.long	149                     ## 0x95
	.long	128                     ## 0x80
	.long	129                     ## 0x81
	.long	149                     ## 0x95
	.long	128                     ## 0x80
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	150                     ## 0x96
	.long	149                     ## 0x95
	.long	129                     ## 0x81
	.long	150                     ## 0x96
	.long	149                     ## 0x95
	.long	129                     ## 0x81
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	130                     ## 0x82
	.long	150                     ## 0x96
	.long	129                     ## 0x81
	.long	130                     ## 0x82
	.long	150                     ## 0x96
	.long	129                     ## 0x81
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	151                     ## 0x97
	.long	150                     ## 0x96
	.long	130                     ## 0x82
	.long	151                     ## 0x97
	.long	150                     ## 0x96
	.long	130                     ## 0x82
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	131                     ## 0x83
	.long	151                     ## 0x97
	.long	130                     ## 0x82
	.long	131                     ## 0x83
	.long	151                     ## 0x97
	.long	130                     ## 0x82
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	152                     ## 0x98
	.long	151                     ## 0x97
	.long	131                     ## 0x83
	.long	152                     ## 0x98
	.long	151                     ## 0x97
	.long	131                     ## 0x83
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	132                     ## 0x84
	.long	152                     ## 0x98
	.long	131                     ## 0x83
	.long	132                     ## 0x84
	.long	152                     ## 0x98
	.long	131                     ## 0x83
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	153                     ## 0x99
	.long	152                     ## 0x98
	.long	132                     ## 0x84
	.long	153                     ## 0x99
	.long	152                     ## 0x98
	.long	132                     ## 0x84
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	133                     ## 0x85
	.long	153                     ## 0x99
	.long	132                     ## 0x84
	.long	133                     ## 0x85
	.long	153                     ## 0x99
	.long	132                     ## 0x84
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	154                     ## 0x9a
	.long	153                     ## 0x99
	.long	133                     ## 0x85
	.long	154                     ## 0x9a
	.long	153                     ## 0x99
	.long	133                     ## 0x85
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	134                     ## 0x86
	.long	154                     ## 0x9a
	.long	133                     ## 0x85
	.long	134                     ## 0x86
	.long	154                     ## 0x9a
	.long	133                     ## 0x85
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	155                     ## 0x9b
	.long	154                     ## 0x9a
	.long	134                     ## 0x86
	.long	155                     ## 0x9b
	.long	154                     ## 0x9a
	.long	134                     ## 0x86
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	135                     ## 0x87
	.long	155                     ## 0x9b
	.long	134                     ## 0x86
	.long	135                     ## 0x87
	.long	155                     ## 0x9b
	.long	134                     ## 0x86
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	156                     ## 0x9c
	.long	155                     ## 0x9b
	.long	135                     ## 0x87
	.long	156                     ## 0x9c
	.long	155                     ## 0x9b
	.long	135                     ## 0x87
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	136                     ## 0x88
	.long	156                     ## 0x9c
	.long	135                     ## 0x87
	.long	136                     ## 0x88
	.long	156                     ## 0x9c
	.long	135                     ## 0x87
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	157                     ## 0x9d
	.long	156                     ## 0x9c
	.long	136                     ## 0x88
	.long	157                     ## 0x9d
	.long	156                     ## 0x9c
	.long	136                     ## 0x88
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	137                     ## 0x89
	.long	157                     ## 0x9d
	.long	136                     ## 0x88
	.long	137                     ## 0x89
	.long	157                     ## 0x9d
	.long	136                     ## 0x88
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	158                     ## 0x9e
	.long	157                     ## 0x9d
	.long	137                     ## 0x89
	.long	158                     ## 0x9e
	.long	157                     ## 0x9d
	.long	137                     ## 0x89
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	138                     ## 0x8a
	.long	158                     ## 0x9e
	.long	137                     ## 0x89
	.long	138                     ## 0x8a
	.long	158                     ## 0x9e
	.long	137                     ## 0x89
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	159                     ## 0x9f
	.long	158                     ## 0x9e
	.long	138                     ## 0x8a
	.long	159                     ## 0x9f
	.long	158                     ## 0x9e
	.long	138                     ## 0x8a
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	139                     ## 0x8b
	.long	159                     ## 0x9f
	.long	138                     ## 0x8a
	.long	139                     ## 0x8b
	.long	159                     ## 0x9f
	.long	138                     ## 0x8a
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	140                     ## 0x8c
	.long	159                     ## 0x9f
	.long	139                     ## 0x8b
	.long	140                     ## 0x8c
	.long	159                     ## 0x9f
	.long	139                     ## 0x8b
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	140                     ## 0x8c
	.long	160                     ## 0xa0
	.long	159                     ## 0x9f
	.long	140                     ## 0x8c
	.long	160                     ## 0xa0
	.long	159                     ## 0x9f
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	161                     ## 0xa1
	.long	160                     ## 0xa0
	.long	140                     ## 0x8c
	.long	161                     ## 0xa1
	.long	160                     ## 0xa0
	.long	140                     ## 0x8c
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	141                     ## 0x8d
	.long	161                     ## 0xa1
	.long	140                     ## 0x8c
	.long	141                     ## 0x8d
	.long	161                     ## 0xa1
	.long	140                     ## 0x8c
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	162                     ## 0xa2
	.long	161                     ## 0xa1
	.long	141                     ## 0x8d
	.long	162                     ## 0xa2
	.long	161                     ## 0xa1
	.long	141                     ## 0x8d
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	142                     ## 0x8e
	.long	162                     ## 0xa2
	.long	141                     ## 0x8d
	.long	142                     ## 0x8e
	.long	162                     ## 0xa2
	.long	141                     ## 0x8d
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	163                     ## 0xa3
	.long	162                     ## 0xa2
	.long	142                     ## 0x8e
	.long	163                     ## 0xa3
	.long	162                     ## 0xa2
	.long	142                     ## 0x8e
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	143                     ## 0x8f
	.long	163                     ## 0xa3
	.long	142                     ## 0x8e
	.long	143                     ## 0x8f
	.long	163                     ## 0xa3
	.long	142                     ## 0x8e
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	164                     ## 0xa4
	.long	163                     ## 0xa3
	.long	143                     ## 0x8f
	.long	164                     ## 0xa4
	.long	163                     ## 0xa3
	.long	143                     ## 0x8f
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	144                     ## 0x90
	.long	164                     ## 0xa4
	.long	143                     ## 0x8f
	.long	144                     ## 0x90
	.long	164                     ## 0xa4
	.long	143                     ## 0x8f
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	165                     ## 0xa5
	.long	164                     ## 0xa4
	.long	144                     ## 0x90
	.long	165                     ## 0xa5
	.long	164                     ## 0xa4
	.long	144                     ## 0x90
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	145                     ## 0x91
	.long	165                     ## 0xa5
	.long	144                     ## 0x90
	.long	145                     ## 0x91
	.long	165                     ## 0xa5
	.long	144                     ## 0x90
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	166                     ## 0xa6
	.long	165                     ## 0xa5
	.long	145                     ## 0x91
	.long	166                     ## 0xa6
	.long	165                     ## 0xa5
	.long	145                     ## 0x91
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	146                     ## 0x92
	.long	166                     ## 0xa6
	.long	145                     ## 0x91
	.long	146                     ## 0x92
	.long	166                     ## 0xa6
	.long	145                     ## 0x91
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	167                     ## 0xa7
	.long	166                     ## 0xa6
	.long	146                     ## 0x92
	.long	167                     ## 0xa7
	.long	166                     ## 0xa6
	.long	146                     ## 0x92
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	147                     ## 0x93
	.long	167                     ## 0xa7
	.long	146                     ## 0x92
	.long	147                     ## 0x93
	.long	167                     ## 0xa7
	.long	146                     ## 0x92
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	168                     ## 0xa8
	.long	167                     ## 0xa7
	.long	147                     ## 0x93
	.long	168                     ## 0xa8
	.long	167                     ## 0xa7
	.long	147                     ## 0x93
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	148                     ## 0x94
	.long	168                     ## 0xa8
	.long	147                     ## 0x93
	.long	148                     ## 0x94
	.long	168                     ## 0xa8
	.long	147                     ## 0x93
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	169                     ## 0xa9
	.long	168                     ## 0xa8
	.long	148                     ## 0x94
	.long	169                     ## 0xa9
	.long	168                     ## 0xa8
	.long	148                     ## 0x94
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	149                     ## 0x95
	.long	169                     ## 0xa9
	.long	148                     ## 0x94
	.long	149                     ## 0x95
	.long	169                     ## 0xa9
	.long	148                     ## 0x94
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	170                     ## 0xaa
	.long	169                     ## 0xa9
	.long	149                     ## 0x95
	.long	170                     ## 0xaa
	.long	169                     ## 0xa9
	.long	149                     ## 0x95
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	150                     ## 0x96
	.long	170                     ## 0xaa
	.long	149                     ## 0x95
	.long	150                     ## 0x96
	.long	170                     ## 0xaa
	.long	149                     ## 0x95
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	171                     ## 0xab
	.long	170                     ## 0xaa
	.long	150                     ## 0x96
	.long	171                     ## 0xab
	.long	170                     ## 0xaa
	.long	150                     ## 0x96
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	151                     ## 0x97
	.long	171                     ## 0xab
	.long	150                     ## 0x96
	.long	151                     ## 0x97
	.long	171                     ## 0xab
	.long	150                     ## 0x96
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	172                     ## 0xac
	.long	171                     ## 0xab
	.long	151                     ## 0x97
	.long	172                     ## 0xac
	.long	171                     ## 0xab
	.long	151                     ## 0x97
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	152                     ## 0x98
	.long	172                     ## 0xac
	.long	151                     ## 0x97
	.long	152                     ## 0x98
	.long	172                     ## 0xac
	.long	151                     ## 0x97
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	173                     ## 0xad
	.long	172                     ## 0xac
	.long	152                     ## 0x98
	.long	173                     ## 0xad
	.long	172                     ## 0xac
	.long	152                     ## 0x98
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	153                     ## 0x99
	.long	173                     ## 0xad
	.long	152                     ## 0x98
	.long	153                     ## 0x99
	.long	173                     ## 0xad
	.long	152                     ## 0x98
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	174                     ## 0xae
	.long	173                     ## 0xad
	.long	153                     ## 0x99
	.long	174                     ## 0xae
	.long	173                     ## 0xad
	.long	153                     ## 0x99
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	154                     ## 0x9a
	.long	174                     ## 0xae
	.long	153                     ## 0x99
	.long	154                     ## 0x9a
	.long	174                     ## 0xae
	.long	153                     ## 0x99
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	175                     ## 0xaf
	.long	174                     ## 0xae
	.long	154                     ## 0x9a
	.long	175                     ## 0xaf
	.long	174                     ## 0xae
	.long	154                     ## 0x9a
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	155                     ## 0x9b
	.long	175                     ## 0xaf
	.long	154                     ## 0x9a
	.long	155                     ## 0x9b
	.long	175                     ## 0xaf
	.long	154                     ## 0x9a
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	176                     ## 0xb0
	.long	175                     ## 0xaf
	.long	155                     ## 0x9b
	.long	176                     ## 0xb0
	.long	175                     ## 0xaf
	.long	155                     ## 0x9b
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	156                     ## 0x9c
	.long	176                     ## 0xb0
	.long	155                     ## 0x9b
	.long	156                     ## 0x9c
	.long	176                     ## 0xb0
	.long	155                     ## 0x9b
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	177                     ## 0xb1
	.long	176                     ## 0xb0
	.long	156                     ## 0x9c
	.long	177                     ## 0xb1
	.long	176                     ## 0xb0
	.long	156                     ## 0x9c
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	157                     ## 0x9d
	.long	177                     ## 0xb1
	.long	156                     ## 0x9c
	.long	157                     ## 0x9d
	.long	177                     ## 0xb1
	.long	156                     ## 0x9c
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	178                     ## 0xb2
	.long	177                     ## 0xb1
	.long	157                     ## 0x9d
	.long	178                     ## 0xb2
	.long	177                     ## 0xb1
	.long	157                     ## 0x9d
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	158                     ## 0x9e
	.long	178                     ## 0xb2
	.long	157                     ## 0x9d
	.long	158                     ## 0x9e
	.long	178                     ## 0xb2
	.long	157                     ## 0x9d
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	179                     ## 0xb3
	.long	178                     ## 0xb2
	.long	158                     ## 0x9e
	.long	179                     ## 0xb3
	.long	178                     ## 0xb2
	.long	158                     ## 0x9e
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	159                     ## 0x9f
	.long	179                     ## 0xb3
	.long	158                     ## 0x9e
	.long	159                     ## 0x9f
	.long	179                     ## 0xb3
	.long	158                     ## 0x9e
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	160                     ## 0xa0
	.long	179                     ## 0xb3
	.long	159                     ## 0x9f
	.long	160                     ## 0xa0
	.long	179                     ## 0xb3
	.long	159                     ## 0x9f
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	160                     ## 0xa0
	.long	180                     ## 0xb4
	.long	179                     ## 0xb3
	.long	160                     ## 0xa0
	.long	180                     ## 0xb4
	.long	179                     ## 0xb3
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	181                     ## 0xb5
	.long	180                     ## 0xb4
	.long	160                     ## 0xa0
	.long	181                     ## 0xb5
	.long	180                     ## 0xb4
	.long	160                     ## 0xa0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	161                     ## 0xa1
	.long	181                     ## 0xb5
	.long	160                     ## 0xa0
	.long	161                     ## 0xa1
	.long	181                     ## 0xb5
	.long	160                     ## 0xa0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	182                     ## 0xb6
	.long	181                     ## 0xb5
	.long	161                     ## 0xa1
	.long	182                     ## 0xb6
	.long	181                     ## 0xb5
	.long	161                     ## 0xa1
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	162                     ## 0xa2
	.long	182                     ## 0xb6
	.long	161                     ## 0xa1
	.long	162                     ## 0xa2
	.long	182                     ## 0xb6
	.long	161                     ## 0xa1
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	183                     ## 0xb7
	.long	182                     ## 0xb6
	.long	162                     ## 0xa2
	.long	183                     ## 0xb7
	.long	182                     ## 0xb6
	.long	162                     ## 0xa2
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	163                     ## 0xa3
	.long	183                     ## 0xb7
	.long	162                     ## 0xa2
	.long	163                     ## 0xa3
	.long	183                     ## 0xb7
	.long	162                     ## 0xa2
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	184                     ## 0xb8
	.long	183                     ## 0xb7
	.long	163                     ## 0xa3
	.long	184                     ## 0xb8
	.long	183                     ## 0xb7
	.long	163                     ## 0xa3
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	164                     ## 0xa4
	.long	184                     ## 0xb8
	.long	163                     ## 0xa3
	.long	164                     ## 0xa4
	.long	184                     ## 0xb8
	.long	163                     ## 0xa3
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	185                     ## 0xb9
	.long	184                     ## 0xb8
	.long	164                     ## 0xa4
	.long	185                     ## 0xb9
	.long	184                     ## 0xb8
	.long	164                     ## 0xa4
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	165                     ## 0xa5
	.long	185                     ## 0xb9
	.long	164                     ## 0xa4
	.long	165                     ## 0xa5
	.long	185                     ## 0xb9
	.long	164                     ## 0xa4
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	186                     ## 0xba
	.long	185                     ## 0xb9
	.long	165                     ## 0xa5
	.long	186                     ## 0xba
	.long	185                     ## 0xb9
	.long	165                     ## 0xa5
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	166                     ## 0xa6
	.long	186                     ## 0xba
	.long	165                     ## 0xa5
	.long	166                     ## 0xa6
	.long	186                     ## 0xba
	.long	165                     ## 0xa5
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	187                     ## 0xbb
	.long	186                     ## 0xba
	.long	166                     ## 0xa6
	.long	187                     ## 0xbb
	.long	186                     ## 0xba
	.long	166                     ## 0xa6
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	167                     ## 0xa7
	.long	187                     ## 0xbb
	.long	166                     ## 0xa6
	.long	167                     ## 0xa7
	.long	187                     ## 0xbb
	.long	166                     ## 0xa6
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	188                     ## 0xbc
	.long	187                     ## 0xbb
	.long	167                     ## 0xa7
	.long	188                     ## 0xbc
	.long	187                     ## 0xbb
	.long	167                     ## 0xa7
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	168                     ## 0xa8
	.long	188                     ## 0xbc
	.long	167                     ## 0xa7
	.long	168                     ## 0xa8
	.long	188                     ## 0xbc
	.long	167                     ## 0xa7
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	189                     ## 0xbd
	.long	188                     ## 0xbc
	.long	168                     ## 0xa8
	.long	189                     ## 0xbd
	.long	188                     ## 0xbc
	.long	168                     ## 0xa8
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	169                     ## 0xa9
	.long	189                     ## 0xbd
	.long	168                     ## 0xa8
	.long	169                     ## 0xa9
	.long	189                     ## 0xbd
	.long	168                     ## 0xa8
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	190                     ## 0xbe
	.long	189                     ## 0xbd
	.long	169                     ## 0xa9
	.long	190                     ## 0xbe
	.long	189                     ## 0xbd
	.long	169                     ## 0xa9
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	170                     ## 0xaa
	.long	190                     ## 0xbe
	.long	169                     ## 0xa9
	.long	170                     ## 0xaa
	.long	190                     ## 0xbe
	.long	169                     ## 0xa9
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	191                     ## 0xbf
	.long	190                     ## 0xbe
	.long	170                     ## 0xaa
	.long	191                     ## 0xbf
	.long	190                     ## 0xbe
	.long	170                     ## 0xaa
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	171                     ## 0xab
	.long	191                     ## 0xbf
	.long	170                     ## 0xaa
	.long	171                     ## 0xab
	.long	191                     ## 0xbf
	.long	170                     ## 0xaa
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	192                     ## 0xc0
	.long	191                     ## 0xbf
	.long	171                     ## 0xab
	.long	192                     ## 0xc0
	.long	191                     ## 0xbf
	.long	171                     ## 0xab
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	172                     ## 0xac
	.long	192                     ## 0xc0
	.long	171                     ## 0xab
	.long	172                     ## 0xac
	.long	192                     ## 0xc0
	.long	171                     ## 0xab
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	193                     ## 0xc1
	.long	192                     ## 0xc0
	.long	172                     ## 0xac
	.long	193                     ## 0xc1
	.long	192                     ## 0xc0
	.long	172                     ## 0xac
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	173                     ## 0xad
	.long	193                     ## 0xc1
	.long	172                     ## 0xac
	.long	173                     ## 0xad
	.long	193                     ## 0xc1
	.long	172                     ## 0xac
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	194                     ## 0xc2
	.long	193                     ## 0xc1
	.long	173                     ## 0xad
	.long	194                     ## 0xc2
	.long	193                     ## 0xc1
	.long	173                     ## 0xad
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	174                     ## 0xae
	.long	194                     ## 0xc2
	.long	173                     ## 0xad
	.long	174                     ## 0xae
	.long	194                     ## 0xc2
	.long	173                     ## 0xad
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	195                     ## 0xc3
	.long	194                     ## 0xc2
	.long	174                     ## 0xae
	.long	195                     ## 0xc3
	.long	194                     ## 0xc2
	.long	174                     ## 0xae
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	175                     ## 0xaf
	.long	195                     ## 0xc3
	.long	174                     ## 0xae
	.long	175                     ## 0xaf
	.long	195                     ## 0xc3
	.long	174                     ## 0xae
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	196                     ## 0xc4
	.long	195                     ## 0xc3
	.long	175                     ## 0xaf
	.long	196                     ## 0xc4
	.long	195                     ## 0xc3
	.long	175                     ## 0xaf
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	176                     ## 0xb0
	.long	196                     ## 0xc4
	.long	175                     ## 0xaf
	.long	176                     ## 0xb0
	.long	196                     ## 0xc4
	.long	175                     ## 0xaf
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	197                     ## 0xc5
	.long	196                     ## 0xc4
	.long	176                     ## 0xb0
	.long	197                     ## 0xc5
	.long	196                     ## 0xc4
	.long	176                     ## 0xb0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	177                     ## 0xb1
	.long	197                     ## 0xc5
	.long	176                     ## 0xb0
	.long	177                     ## 0xb1
	.long	197                     ## 0xc5
	.long	176                     ## 0xb0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	198                     ## 0xc6
	.long	197                     ## 0xc5
	.long	177                     ## 0xb1
	.long	198                     ## 0xc6
	.long	197                     ## 0xc5
	.long	177                     ## 0xb1
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	178                     ## 0xb2
	.long	198                     ## 0xc6
	.long	177                     ## 0xb1
	.long	178                     ## 0xb2
	.long	198                     ## 0xc6
	.long	177                     ## 0xb1
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	199                     ## 0xc7
	.long	198                     ## 0xc6
	.long	178                     ## 0xb2
	.long	199                     ## 0xc7
	.long	198                     ## 0xc6
	.long	178                     ## 0xb2
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	179                     ## 0xb3
	.long	199                     ## 0xc7
	.long	178                     ## 0xb2
	.long	179                     ## 0xb3
	.long	199                     ## 0xc7
	.long	178                     ## 0xb2
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	180                     ## 0xb4
	.long	199                     ## 0xc7
	.long	179                     ## 0xb3
	.long	180                     ## 0xb4
	.long	199                     ## 0xc7
	.long	179                     ## 0xb3
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	180                     ## 0xb4
	.long	200                     ## 0xc8
	.long	199                     ## 0xc7
	.long	180                     ## 0xb4
	.long	200                     ## 0xc8
	.long	199                     ## 0xc7
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	201                     ## 0xc9
	.long	200                     ## 0xc8
	.long	180                     ## 0xb4
	.long	201                     ## 0xc9
	.long	200                     ## 0xc8
	.long	180                     ## 0xb4
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	181                     ## 0xb5
	.long	201                     ## 0xc9
	.long	180                     ## 0xb4
	.long	181                     ## 0xb5
	.long	201                     ## 0xc9
	.long	180                     ## 0xb4
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	202                     ## 0xca
	.long	201                     ## 0xc9
	.long	181                     ## 0xb5
	.long	202                     ## 0xca
	.long	201                     ## 0xc9
	.long	181                     ## 0xb5
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	182                     ## 0xb6
	.long	202                     ## 0xca
	.long	181                     ## 0xb5
	.long	182                     ## 0xb6
	.long	202                     ## 0xca
	.long	181                     ## 0xb5
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	203                     ## 0xcb
	.long	202                     ## 0xca
	.long	182                     ## 0xb6
	.long	203                     ## 0xcb
	.long	202                     ## 0xca
	.long	182                     ## 0xb6
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	183                     ## 0xb7
	.long	203                     ## 0xcb
	.long	182                     ## 0xb6
	.long	183                     ## 0xb7
	.long	203                     ## 0xcb
	.long	182                     ## 0xb6
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	204                     ## 0xcc
	.long	203                     ## 0xcb
	.long	183                     ## 0xb7
	.long	204                     ## 0xcc
	.long	203                     ## 0xcb
	.long	183                     ## 0xb7
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	184                     ## 0xb8
	.long	204                     ## 0xcc
	.long	183                     ## 0xb7
	.long	184                     ## 0xb8
	.long	204                     ## 0xcc
	.long	183                     ## 0xb7
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	205                     ## 0xcd
	.long	204                     ## 0xcc
	.long	184                     ## 0xb8
	.long	205                     ## 0xcd
	.long	204                     ## 0xcc
	.long	184                     ## 0xb8
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	185                     ## 0xb9
	.long	205                     ## 0xcd
	.long	184                     ## 0xb8
	.long	185                     ## 0xb9
	.long	205                     ## 0xcd
	.long	184                     ## 0xb8
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	206                     ## 0xce
	.long	205                     ## 0xcd
	.long	185                     ## 0xb9
	.long	206                     ## 0xce
	.long	205                     ## 0xcd
	.long	185                     ## 0xb9
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	186                     ## 0xba
	.long	206                     ## 0xce
	.long	185                     ## 0xb9
	.long	186                     ## 0xba
	.long	206                     ## 0xce
	.long	185                     ## 0xb9
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	207                     ## 0xcf
	.long	206                     ## 0xce
	.long	186                     ## 0xba
	.long	207                     ## 0xcf
	.long	206                     ## 0xce
	.long	186                     ## 0xba
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	187                     ## 0xbb
	.long	207                     ## 0xcf
	.long	186                     ## 0xba
	.long	187                     ## 0xbb
	.long	207                     ## 0xcf
	.long	186                     ## 0xba
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	208                     ## 0xd0
	.long	207                     ## 0xcf
	.long	187                     ## 0xbb
	.long	208                     ## 0xd0
	.long	207                     ## 0xcf
	.long	187                     ## 0xbb
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	188                     ## 0xbc
	.long	208                     ## 0xd0
	.long	187                     ## 0xbb
	.long	188                     ## 0xbc
	.long	208                     ## 0xd0
	.long	187                     ## 0xbb
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	209                     ## 0xd1
	.long	208                     ## 0xd0
	.long	188                     ## 0xbc
	.long	209                     ## 0xd1
	.long	208                     ## 0xd0
	.long	188                     ## 0xbc
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	189                     ## 0xbd
	.long	209                     ## 0xd1
	.long	188                     ## 0xbc
	.long	189                     ## 0xbd
	.long	209                     ## 0xd1
	.long	188                     ## 0xbc
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	210                     ## 0xd2
	.long	209                     ## 0xd1
	.long	189                     ## 0xbd
	.long	210                     ## 0xd2
	.long	209                     ## 0xd1
	.long	189                     ## 0xbd
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	190                     ## 0xbe
	.long	210                     ## 0xd2
	.long	189                     ## 0xbd
	.long	190                     ## 0xbe
	.long	210                     ## 0xd2
	.long	189                     ## 0xbd
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	211                     ## 0xd3
	.long	210                     ## 0xd2
	.long	190                     ## 0xbe
	.long	211                     ## 0xd3
	.long	210                     ## 0xd2
	.long	190                     ## 0xbe
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	191                     ## 0xbf
	.long	211                     ## 0xd3
	.long	190                     ## 0xbe
	.long	191                     ## 0xbf
	.long	211                     ## 0xd3
	.long	190                     ## 0xbe
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	212                     ## 0xd4
	.long	211                     ## 0xd3
	.long	191                     ## 0xbf
	.long	212                     ## 0xd4
	.long	211                     ## 0xd3
	.long	191                     ## 0xbf
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	192                     ## 0xc0
	.long	212                     ## 0xd4
	.long	191                     ## 0xbf
	.long	192                     ## 0xc0
	.long	212                     ## 0xd4
	.long	191                     ## 0xbf
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	213                     ## 0xd5
	.long	212                     ## 0xd4
	.long	192                     ## 0xc0
	.long	213                     ## 0xd5
	.long	212                     ## 0xd4
	.long	192                     ## 0xc0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	193                     ## 0xc1
	.long	213                     ## 0xd5
	.long	192                     ## 0xc0
	.long	193                     ## 0xc1
	.long	213                     ## 0xd5
	.long	192                     ## 0xc0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	214                     ## 0xd6
	.long	213                     ## 0xd5
	.long	193                     ## 0xc1
	.long	214                     ## 0xd6
	.long	213                     ## 0xd5
	.long	193                     ## 0xc1
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	194                     ## 0xc2
	.long	214                     ## 0xd6
	.long	193                     ## 0xc1
	.long	194                     ## 0xc2
	.long	214                     ## 0xd6
	.long	193                     ## 0xc1
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	215                     ## 0xd7
	.long	214                     ## 0xd6
	.long	194                     ## 0xc2
	.long	215                     ## 0xd7
	.long	214                     ## 0xd6
	.long	194                     ## 0xc2
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	195                     ## 0xc3
	.long	215                     ## 0xd7
	.long	194                     ## 0xc2
	.long	195                     ## 0xc3
	.long	215                     ## 0xd7
	.long	194                     ## 0xc2
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	216                     ## 0xd8
	.long	215                     ## 0xd7
	.long	195                     ## 0xc3
	.long	216                     ## 0xd8
	.long	215                     ## 0xd7
	.long	195                     ## 0xc3
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	196                     ## 0xc4
	.long	216                     ## 0xd8
	.long	195                     ## 0xc3
	.long	196                     ## 0xc4
	.long	216                     ## 0xd8
	.long	195                     ## 0xc3
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	217                     ## 0xd9
	.long	216                     ## 0xd8
	.long	196                     ## 0xc4
	.long	217                     ## 0xd9
	.long	216                     ## 0xd8
	.long	196                     ## 0xc4
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	197                     ## 0xc5
	.long	217                     ## 0xd9
	.long	196                     ## 0xc4
	.long	197                     ## 0xc5
	.long	217                     ## 0xd9
	.long	196                     ## 0xc4
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	218                     ## 0xda
	.long	217                     ## 0xd9
	.long	197                     ## 0xc5
	.long	218                     ## 0xda
	.long	217                     ## 0xd9
	.long	197                     ## 0xc5
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	198                     ## 0xc6
	.long	218                     ## 0xda
	.long	197                     ## 0xc5
	.long	198                     ## 0xc6
	.long	218                     ## 0xda
	.long	197                     ## 0xc5
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	219                     ## 0xdb
	.long	218                     ## 0xda
	.long	198                     ## 0xc6
	.long	219                     ## 0xdb
	.long	218                     ## 0xda
	.long	198                     ## 0xc6
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	199                     ## 0xc7
	.long	219                     ## 0xdb
	.long	198                     ## 0xc6
	.long	199                     ## 0xc7
	.long	219                     ## 0xdb
	.long	198                     ## 0xc6
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	200                     ## 0xc8
	.long	219                     ## 0xdb
	.long	199                     ## 0xc7
	.long	200                     ## 0xc8
	.long	219                     ## 0xdb
	.long	199                     ## 0xc7
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	200                     ## 0xc8
	.long	220                     ## 0xdc
	.long	219                     ## 0xdb
	.long	200                     ## 0xc8
	.long	220                     ## 0xdc
	.long	219                     ## 0xdb
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	221                     ## 0xdd
	.long	220                     ## 0xdc
	.long	200                     ## 0xc8
	.long	221                     ## 0xdd
	.long	220                     ## 0xdc
	.long	200                     ## 0xc8
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	201                     ## 0xc9
	.long	221                     ## 0xdd
	.long	200                     ## 0xc8
	.long	201                     ## 0xc9
	.long	221                     ## 0xdd
	.long	200                     ## 0xc8
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	222                     ## 0xde
	.long	221                     ## 0xdd
	.long	201                     ## 0xc9
	.long	222                     ## 0xde
	.long	221                     ## 0xdd
	.long	201                     ## 0xc9
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	202                     ## 0xca
	.long	222                     ## 0xde
	.long	201                     ## 0xc9
	.long	202                     ## 0xca
	.long	222                     ## 0xde
	.long	201                     ## 0xc9
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	223                     ## 0xdf
	.long	222                     ## 0xde
	.long	202                     ## 0xca
	.long	223                     ## 0xdf
	.long	222                     ## 0xde
	.long	202                     ## 0xca
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	203                     ## 0xcb
	.long	223                     ## 0xdf
	.long	202                     ## 0xca
	.long	203                     ## 0xcb
	.long	223                     ## 0xdf
	.long	202                     ## 0xca
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	224                     ## 0xe0
	.long	223                     ## 0xdf
	.long	203                     ## 0xcb
	.long	224                     ## 0xe0
	.long	223                     ## 0xdf
	.long	203                     ## 0xcb
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	204                     ## 0xcc
	.long	224                     ## 0xe0
	.long	203                     ## 0xcb
	.long	204                     ## 0xcc
	.long	224                     ## 0xe0
	.long	203                     ## 0xcb
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	225                     ## 0xe1
	.long	224                     ## 0xe0
	.long	204                     ## 0xcc
	.long	225                     ## 0xe1
	.long	224                     ## 0xe0
	.long	204                     ## 0xcc
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	205                     ## 0xcd
	.long	225                     ## 0xe1
	.long	204                     ## 0xcc
	.long	205                     ## 0xcd
	.long	225                     ## 0xe1
	.long	204                     ## 0xcc
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	226                     ## 0xe2
	.long	225                     ## 0xe1
	.long	205                     ## 0xcd
	.long	226                     ## 0xe2
	.long	225                     ## 0xe1
	.long	205                     ## 0xcd
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	206                     ## 0xce
	.long	226                     ## 0xe2
	.long	205                     ## 0xcd
	.long	206                     ## 0xce
	.long	226                     ## 0xe2
	.long	205                     ## 0xcd
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	227                     ## 0xe3
	.long	226                     ## 0xe2
	.long	206                     ## 0xce
	.long	227                     ## 0xe3
	.long	226                     ## 0xe2
	.long	206                     ## 0xce
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	207                     ## 0xcf
	.long	227                     ## 0xe3
	.long	206                     ## 0xce
	.long	207                     ## 0xcf
	.long	227                     ## 0xe3
	.long	206                     ## 0xce
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	228                     ## 0xe4
	.long	227                     ## 0xe3
	.long	207                     ## 0xcf
	.long	228                     ## 0xe4
	.long	227                     ## 0xe3
	.long	207                     ## 0xcf
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	208                     ## 0xd0
	.long	228                     ## 0xe4
	.long	207                     ## 0xcf
	.long	208                     ## 0xd0
	.long	228                     ## 0xe4
	.long	207                     ## 0xcf
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	229                     ## 0xe5
	.long	228                     ## 0xe4
	.long	208                     ## 0xd0
	.long	229                     ## 0xe5
	.long	228                     ## 0xe4
	.long	208                     ## 0xd0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	209                     ## 0xd1
	.long	229                     ## 0xe5
	.long	208                     ## 0xd0
	.long	209                     ## 0xd1
	.long	229                     ## 0xe5
	.long	208                     ## 0xd0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	230                     ## 0xe6
	.long	229                     ## 0xe5
	.long	209                     ## 0xd1
	.long	230                     ## 0xe6
	.long	229                     ## 0xe5
	.long	209                     ## 0xd1
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	210                     ## 0xd2
	.long	230                     ## 0xe6
	.long	209                     ## 0xd1
	.long	210                     ## 0xd2
	.long	230                     ## 0xe6
	.long	209                     ## 0xd1
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	231                     ## 0xe7
	.long	230                     ## 0xe6
	.long	210                     ## 0xd2
	.long	231                     ## 0xe7
	.long	230                     ## 0xe6
	.long	210                     ## 0xd2
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	211                     ## 0xd3
	.long	231                     ## 0xe7
	.long	210                     ## 0xd2
	.long	211                     ## 0xd3
	.long	231                     ## 0xe7
	.long	210                     ## 0xd2
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	232                     ## 0xe8
	.long	231                     ## 0xe7
	.long	211                     ## 0xd3
	.long	232                     ## 0xe8
	.long	231                     ## 0xe7
	.long	211                     ## 0xd3
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	212                     ## 0xd4
	.long	232                     ## 0xe8
	.long	211                     ## 0xd3
	.long	212                     ## 0xd4
	.long	232                     ## 0xe8
	.long	211                     ## 0xd3
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	233                     ## 0xe9
	.long	232                     ## 0xe8
	.long	212                     ## 0xd4
	.long	233                     ## 0xe9
	.long	232                     ## 0xe8
	.long	212                     ## 0xd4
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	213                     ## 0xd5
	.long	233                     ## 0xe9
	.long	212                     ## 0xd4
	.long	213                     ## 0xd5
	.long	233                     ## 0xe9
	.long	212                     ## 0xd4
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	234                     ## 0xea
	.long	233                     ## 0xe9
	.long	213                     ## 0xd5
	.long	234                     ## 0xea
	.long	233                     ## 0xe9
	.long	213                     ## 0xd5
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	214                     ## 0xd6
	.long	234                     ## 0xea
	.long	213                     ## 0xd5
	.long	214                     ## 0xd6
	.long	234                     ## 0xea
	.long	213                     ## 0xd5
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	235                     ## 0xeb
	.long	234                     ## 0xea
	.long	214                     ## 0xd6
	.long	235                     ## 0xeb
	.long	234                     ## 0xea
	.long	214                     ## 0xd6
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	215                     ## 0xd7
	.long	235                     ## 0xeb
	.long	214                     ## 0xd6
	.long	215                     ## 0xd7
	.long	235                     ## 0xeb
	.long	214                     ## 0xd6
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	236                     ## 0xec
	.long	235                     ## 0xeb
	.long	215                     ## 0xd7
	.long	236                     ## 0xec
	.long	235                     ## 0xeb
	.long	215                     ## 0xd7
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	216                     ## 0xd8
	.long	236                     ## 0xec
	.long	215                     ## 0xd7
	.long	216                     ## 0xd8
	.long	236                     ## 0xec
	.long	215                     ## 0xd7
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	237                     ## 0xed
	.long	236                     ## 0xec
	.long	216                     ## 0xd8
	.long	237                     ## 0xed
	.long	236                     ## 0xec
	.long	216                     ## 0xd8
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	217                     ## 0xd9
	.long	237                     ## 0xed
	.long	216                     ## 0xd8
	.long	217                     ## 0xd9
	.long	237                     ## 0xed
	.long	216                     ## 0xd8
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	238                     ## 0xee
	.long	237                     ## 0xed
	.long	217                     ## 0xd9
	.long	238                     ## 0xee
	.long	237                     ## 0xed
	.long	217                     ## 0xd9
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	218                     ## 0xda
	.long	238                     ## 0xee
	.long	217                     ## 0xd9
	.long	218                     ## 0xda
	.long	238                     ## 0xee
	.long	217                     ## 0xd9
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	239                     ## 0xef
	.long	238                     ## 0xee
	.long	218                     ## 0xda
	.long	239                     ## 0xef
	.long	238                     ## 0xee
	.long	218                     ## 0xda
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	219                     ## 0xdb
	.long	239                     ## 0xef
	.long	218                     ## 0xda
	.long	219                     ## 0xdb
	.long	239                     ## 0xef
	.long	218                     ## 0xda
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	220                     ## 0xdc
	.long	239                     ## 0xef
	.long	219                     ## 0xdb
	.long	220                     ## 0xdc
	.long	239                     ## 0xef
	.long	219                     ## 0xdb
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	220                     ## 0xdc
	.long	240                     ## 0xf0
	.long	239                     ## 0xef
	.long	220                     ## 0xdc
	.long	240                     ## 0xf0
	.long	239                     ## 0xef
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	241                     ## 0xf1
	.long	240                     ## 0xf0
	.long	220                     ## 0xdc
	.long	241                     ## 0xf1
	.long	240                     ## 0xf0
	.long	220                     ## 0xdc
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	221                     ## 0xdd
	.long	241                     ## 0xf1
	.long	220                     ## 0xdc
	.long	221                     ## 0xdd
	.long	241                     ## 0xf1
	.long	220                     ## 0xdc
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	242                     ## 0xf2
	.long	241                     ## 0xf1
	.long	221                     ## 0xdd
	.long	242                     ## 0xf2
	.long	241                     ## 0xf1
	.long	221                     ## 0xdd
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	222                     ## 0xde
	.long	242                     ## 0xf2
	.long	221                     ## 0xdd
	.long	222                     ## 0xde
	.long	242                     ## 0xf2
	.long	221                     ## 0xdd
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	243                     ## 0xf3
	.long	242                     ## 0xf2
	.long	222                     ## 0xde
	.long	243                     ## 0xf3
	.long	242                     ## 0xf2
	.long	222                     ## 0xde
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	223                     ## 0xdf
	.long	243                     ## 0xf3
	.long	222                     ## 0xde
	.long	223                     ## 0xdf
	.long	243                     ## 0xf3
	.long	222                     ## 0xde
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	244                     ## 0xf4
	.long	243                     ## 0xf3
	.long	223                     ## 0xdf
	.long	244                     ## 0xf4
	.long	243                     ## 0xf3
	.long	223                     ## 0xdf
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	224                     ## 0xe0
	.long	244                     ## 0xf4
	.long	223                     ## 0xdf
	.long	224                     ## 0xe0
	.long	244                     ## 0xf4
	.long	223                     ## 0xdf
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	245                     ## 0xf5
	.long	244                     ## 0xf4
	.long	224                     ## 0xe0
	.long	245                     ## 0xf5
	.long	244                     ## 0xf4
	.long	224                     ## 0xe0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	225                     ## 0xe1
	.long	245                     ## 0xf5
	.long	224                     ## 0xe0
	.long	225                     ## 0xe1
	.long	245                     ## 0xf5
	.long	224                     ## 0xe0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	246                     ## 0xf6
	.long	245                     ## 0xf5
	.long	225                     ## 0xe1
	.long	246                     ## 0xf6
	.long	245                     ## 0xf5
	.long	225                     ## 0xe1
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	226                     ## 0xe2
	.long	246                     ## 0xf6
	.long	225                     ## 0xe1
	.long	226                     ## 0xe2
	.long	246                     ## 0xf6
	.long	225                     ## 0xe1
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	247                     ## 0xf7
	.long	246                     ## 0xf6
	.long	226                     ## 0xe2
	.long	247                     ## 0xf7
	.long	246                     ## 0xf6
	.long	226                     ## 0xe2
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	227                     ## 0xe3
	.long	247                     ## 0xf7
	.long	226                     ## 0xe2
	.long	227                     ## 0xe3
	.long	247                     ## 0xf7
	.long	226                     ## 0xe2
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	248                     ## 0xf8
	.long	247                     ## 0xf7
	.long	227                     ## 0xe3
	.long	248                     ## 0xf8
	.long	247                     ## 0xf7
	.long	227                     ## 0xe3
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	228                     ## 0xe4
	.long	248                     ## 0xf8
	.long	227                     ## 0xe3
	.long	228                     ## 0xe4
	.long	248                     ## 0xf8
	.long	227                     ## 0xe3
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	249                     ## 0xf9
	.long	248                     ## 0xf8
	.long	228                     ## 0xe4
	.long	249                     ## 0xf9
	.long	248                     ## 0xf8
	.long	228                     ## 0xe4
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	229                     ## 0xe5
	.long	249                     ## 0xf9
	.long	228                     ## 0xe4
	.long	229                     ## 0xe5
	.long	249                     ## 0xf9
	.long	228                     ## 0xe4
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	250                     ## 0xfa
	.long	249                     ## 0xf9
	.long	229                     ## 0xe5
	.long	250                     ## 0xfa
	.long	249                     ## 0xf9
	.long	229                     ## 0xe5
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	230                     ## 0xe6
	.long	250                     ## 0xfa
	.long	229                     ## 0xe5
	.long	230                     ## 0xe6
	.long	250                     ## 0xfa
	.long	229                     ## 0xe5
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	251                     ## 0xfb
	.long	250                     ## 0xfa
	.long	230                     ## 0xe6
	.long	251                     ## 0xfb
	.long	250                     ## 0xfa
	.long	230                     ## 0xe6
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	231                     ## 0xe7
	.long	251                     ## 0xfb
	.long	230                     ## 0xe6
	.long	231                     ## 0xe7
	.long	251                     ## 0xfb
	.long	230                     ## 0xe6
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	252                     ## 0xfc
	.long	251                     ## 0xfb
	.long	231                     ## 0xe7
	.long	252                     ## 0xfc
	.long	251                     ## 0xfb
	.long	231                     ## 0xe7
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	232                     ## 0xe8
	.long	252                     ## 0xfc
	.long	231                     ## 0xe7
	.long	232                     ## 0xe8
	.long	252                     ## 0xfc
	.long	231                     ## 0xe7
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	253                     ## 0xfd
	.long	252                     ## 0xfc
	.long	232                     ## 0xe8
	.long	253                     ## 0xfd
	.long	252                     ## 0xfc
	.long	232                     ## 0xe8
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	233                     ## 0xe9
	.long	253                     ## 0xfd
	.long	232                     ## 0xe8
	.long	233                     ## 0xe9
	.long	253                     ## 0xfd
	.long	232                     ## 0xe8
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	254                     ## 0xfe
	.long	253                     ## 0xfd
	.long	233                     ## 0xe9
	.long	254                     ## 0xfe
	.long	253                     ## 0xfd
	.long	233                     ## 0xe9
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	234                     ## 0xea
	.long	254                     ## 0xfe
	.long	233                     ## 0xe9
	.long	234                     ## 0xea
	.long	254                     ## 0xfe
	.long	233                     ## 0xe9
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	255                     ## 0xff
	.long	254                     ## 0xfe
	.long	234                     ## 0xea
	.long	255                     ## 0xff
	.long	254                     ## 0xfe
	.long	234                     ## 0xea
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	235                     ## 0xeb
	.long	255                     ## 0xff
	.long	234                     ## 0xea
	.long	235                     ## 0xeb
	.long	255                     ## 0xff
	.long	234                     ## 0xea
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	256                     ## 0x100
	.long	255                     ## 0xff
	.long	235                     ## 0xeb
	.long	256                     ## 0x100
	.long	255                     ## 0xff
	.long	235                     ## 0xeb
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	236                     ## 0xec
	.long	256                     ## 0x100
	.long	235                     ## 0xeb
	.long	236                     ## 0xec
	.long	256                     ## 0x100
	.long	235                     ## 0xeb
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	257                     ## 0x101
	.long	256                     ## 0x100
	.long	236                     ## 0xec
	.long	257                     ## 0x101
	.long	256                     ## 0x100
	.long	236                     ## 0xec
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	237                     ## 0xed
	.long	257                     ## 0x101
	.long	236                     ## 0xec
	.long	237                     ## 0xed
	.long	257                     ## 0x101
	.long	236                     ## 0xec
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	258                     ## 0x102
	.long	257                     ## 0x101
	.long	237                     ## 0xed
	.long	258                     ## 0x102
	.long	257                     ## 0x101
	.long	237                     ## 0xed
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	238                     ## 0xee
	.long	258                     ## 0x102
	.long	237                     ## 0xed
	.long	238                     ## 0xee
	.long	258                     ## 0x102
	.long	237                     ## 0xed
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	259                     ## 0x103
	.long	258                     ## 0x102
	.long	238                     ## 0xee
	.long	259                     ## 0x103
	.long	258                     ## 0x102
	.long	238                     ## 0xee
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	239                     ## 0xef
	.long	259                     ## 0x103
	.long	238                     ## 0xee
	.long	239                     ## 0xef
	.long	259                     ## 0x103
	.long	238                     ## 0xee
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	240                     ## 0xf0
	.long	259                     ## 0x103
	.long	239                     ## 0xef
	.long	240                     ## 0xf0
	.long	259                     ## 0x103
	.long	239                     ## 0xef
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	240                     ## 0xf0
	.long	260                     ## 0x104
	.long	259                     ## 0x103
	.long	240                     ## 0xf0
	.long	260                     ## 0x104
	.long	259                     ## 0x103
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	261                     ## 0x105
	.long	260                     ## 0x104
	.long	240                     ## 0xf0
	.long	261                     ## 0x105
	.long	260                     ## 0x104
	.long	240                     ## 0xf0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	241                     ## 0xf1
	.long	261                     ## 0x105
	.long	240                     ## 0xf0
	.long	241                     ## 0xf1
	.long	261                     ## 0x105
	.long	240                     ## 0xf0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	262                     ## 0x106
	.long	261                     ## 0x105
	.long	241                     ## 0xf1
	.long	262                     ## 0x106
	.long	261                     ## 0x105
	.long	241                     ## 0xf1
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	242                     ## 0xf2
	.long	262                     ## 0x106
	.long	241                     ## 0xf1
	.long	242                     ## 0xf2
	.long	262                     ## 0x106
	.long	241                     ## 0xf1
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	263                     ## 0x107
	.long	262                     ## 0x106
	.long	242                     ## 0xf2
	.long	263                     ## 0x107
	.long	262                     ## 0x106
	.long	242                     ## 0xf2
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	243                     ## 0xf3
	.long	263                     ## 0x107
	.long	242                     ## 0xf2
	.long	243                     ## 0xf3
	.long	263                     ## 0x107
	.long	242                     ## 0xf2
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	264                     ## 0x108
	.long	263                     ## 0x107
	.long	243                     ## 0xf3
	.long	264                     ## 0x108
	.long	263                     ## 0x107
	.long	243                     ## 0xf3
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	244                     ## 0xf4
	.long	264                     ## 0x108
	.long	243                     ## 0xf3
	.long	244                     ## 0xf4
	.long	264                     ## 0x108
	.long	243                     ## 0xf3
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	265                     ## 0x109
	.long	264                     ## 0x108
	.long	244                     ## 0xf4
	.long	265                     ## 0x109
	.long	264                     ## 0x108
	.long	244                     ## 0xf4
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	245                     ## 0xf5
	.long	265                     ## 0x109
	.long	244                     ## 0xf4
	.long	245                     ## 0xf5
	.long	265                     ## 0x109
	.long	244                     ## 0xf4
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	266                     ## 0x10a
	.long	265                     ## 0x109
	.long	245                     ## 0xf5
	.long	266                     ## 0x10a
	.long	265                     ## 0x109
	.long	245                     ## 0xf5
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	246                     ## 0xf6
	.long	266                     ## 0x10a
	.long	245                     ## 0xf5
	.long	246                     ## 0xf6
	.long	266                     ## 0x10a
	.long	245                     ## 0xf5
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	267                     ## 0x10b
	.long	266                     ## 0x10a
	.long	246                     ## 0xf6
	.long	267                     ## 0x10b
	.long	266                     ## 0x10a
	.long	246                     ## 0xf6
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	247                     ## 0xf7
	.long	267                     ## 0x10b
	.long	246                     ## 0xf6
	.long	247                     ## 0xf7
	.long	267                     ## 0x10b
	.long	246                     ## 0xf6
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	268                     ## 0x10c
	.long	267                     ## 0x10b
	.long	247                     ## 0xf7
	.long	268                     ## 0x10c
	.long	267                     ## 0x10b
	.long	247                     ## 0xf7
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	248                     ## 0xf8
	.long	268                     ## 0x10c
	.long	247                     ## 0xf7
	.long	248                     ## 0xf8
	.long	268                     ## 0x10c
	.long	247                     ## 0xf7
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	269                     ## 0x10d
	.long	268                     ## 0x10c
	.long	248                     ## 0xf8
	.long	269                     ## 0x10d
	.long	268                     ## 0x10c
	.long	248                     ## 0xf8
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	249                     ## 0xf9
	.long	269                     ## 0x10d
	.long	248                     ## 0xf8
	.long	249                     ## 0xf9
	.long	269                     ## 0x10d
	.long	248                     ## 0xf8
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	270                     ## 0x10e
	.long	269                     ## 0x10d
	.long	249                     ## 0xf9
	.long	270                     ## 0x10e
	.long	269                     ## 0x10d
	.long	249                     ## 0xf9
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	250                     ## 0xfa
	.long	270                     ## 0x10e
	.long	249                     ## 0xf9
	.long	250                     ## 0xfa
	.long	270                     ## 0x10e
	.long	249                     ## 0xf9
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	271                     ## 0x10f
	.long	270                     ## 0x10e
	.long	250                     ## 0xfa
	.long	271                     ## 0x10f
	.long	270                     ## 0x10e
	.long	250                     ## 0xfa
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	251                     ## 0xfb
	.long	271                     ## 0x10f
	.long	250                     ## 0xfa
	.long	251                     ## 0xfb
	.long	271                     ## 0x10f
	.long	250                     ## 0xfa
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	272                     ## 0x110
	.long	271                     ## 0x10f
	.long	251                     ## 0xfb
	.long	272                     ## 0x110
	.long	271                     ## 0x10f
	.long	251                     ## 0xfb
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	252                     ## 0xfc
	.long	272                     ## 0x110
	.long	251                     ## 0xfb
	.long	252                     ## 0xfc
	.long	272                     ## 0x110
	.long	251                     ## 0xfb
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	273                     ## 0x111
	.long	272                     ## 0x110
	.long	252                     ## 0xfc
	.long	273                     ## 0x111
	.long	272                     ## 0x110
	.long	252                     ## 0xfc
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	253                     ## 0xfd
	.long	273                     ## 0x111
	.long	252                     ## 0xfc
	.long	253                     ## 0xfd
	.long	273                     ## 0x111
	.long	252                     ## 0xfc
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	274                     ## 0x112
	.long	273                     ## 0x111
	.long	253                     ## 0xfd
	.long	274                     ## 0x112
	.long	273                     ## 0x111
	.long	253                     ## 0xfd
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	254                     ## 0xfe
	.long	274                     ## 0x112
	.long	253                     ## 0xfd
	.long	254                     ## 0xfe
	.long	274                     ## 0x112
	.long	253                     ## 0xfd
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	275                     ## 0x113
	.long	274                     ## 0x112
	.long	254                     ## 0xfe
	.long	275                     ## 0x113
	.long	274                     ## 0x112
	.long	254                     ## 0xfe
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	255                     ## 0xff
	.long	275                     ## 0x113
	.long	254                     ## 0xfe
	.long	255                     ## 0xff
	.long	275                     ## 0x113
	.long	254                     ## 0xfe
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	276                     ## 0x114
	.long	275                     ## 0x113
	.long	255                     ## 0xff
	.long	276                     ## 0x114
	.long	275                     ## 0x113
	.long	255                     ## 0xff
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	256                     ## 0x100
	.long	276                     ## 0x114
	.long	255                     ## 0xff
	.long	256                     ## 0x100
	.long	276                     ## 0x114
	.long	255                     ## 0xff
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	277                     ## 0x115
	.long	276                     ## 0x114
	.long	256                     ## 0x100
	.long	277                     ## 0x115
	.long	276                     ## 0x114
	.long	256                     ## 0x100
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	257                     ## 0x101
	.long	277                     ## 0x115
	.long	256                     ## 0x100
	.long	257                     ## 0x101
	.long	277                     ## 0x115
	.long	256                     ## 0x100
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	278                     ## 0x116
	.long	277                     ## 0x115
	.long	257                     ## 0x101
	.long	278                     ## 0x116
	.long	277                     ## 0x115
	.long	257                     ## 0x101
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	258                     ## 0x102
	.long	278                     ## 0x116
	.long	257                     ## 0x101
	.long	258                     ## 0x102
	.long	278                     ## 0x116
	.long	257                     ## 0x101
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	279                     ## 0x117
	.long	278                     ## 0x116
	.long	258                     ## 0x102
	.long	279                     ## 0x117
	.long	278                     ## 0x116
	.long	258                     ## 0x102
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	259                     ## 0x103
	.long	279                     ## 0x117
	.long	258                     ## 0x102
	.long	259                     ## 0x103
	.long	279                     ## 0x117
	.long	258                     ## 0x102
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	260                     ## 0x104
	.long	279                     ## 0x117
	.long	259                     ## 0x103
	.long	260                     ## 0x104
	.long	279                     ## 0x117
	.long	259                     ## 0x103
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	260                     ## 0x104
	.long	280                     ## 0x118
	.long	279                     ## 0x117
	.long	260                     ## 0x104
	.long	280                     ## 0x118
	.long	279                     ## 0x117
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	281                     ## 0x119
	.long	280                     ## 0x118
	.long	260                     ## 0x104
	.long	281                     ## 0x119
	.long	280                     ## 0x118
	.long	260                     ## 0x104
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	261                     ## 0x105
	.long	281                     ## 0x119
	.long	260                     ## 0x104
	.long	261                     ## 0x105
	.long	281                     ## 0x119
	.long	260                     ## 0x104
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	282                     ## 0x11a
	.long	281                     ## 0x119
	.long	261                     ## 0x105
	.long	282                     ## 0x11a
	.long	281                     ## 0x119
	.long	261                     ## 0x105
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	262                     ## 0x106
	.long	282                     ## 0x11a
	.long	261                     ## 0x105
	.long	262                     ## 0x106
	.long	282                     ## 0x11a
	.long	261                     ## 0x105
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	283                     ## 0x11b
	.long	282                     ## 0x11a
	.long	262                     ## 0x106
	.long	283                     ## 0x11b
	.long	282                     ## 0x11a
	.long	262                     ## 0x106
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	263                     ## 0x107
	.long	283                     ## 0x11b
	.long	262                     ## 0x106
	.long	263                     ## 0x107
	.long	283                     ## 0x11b
	.long	262                     ## 0x106
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	284                     ## 0x11c
	.long	283                     ## 0x11b
	.long	263                     ## 0x107
	.long	284                     ## 0x11c
	.long	283                     ## 0x11b
	.long	263                     ## 0x107
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	264                     ## 0x108
	.long	284                     ## 0x11c
	.long	263                     ## 0x107
	.long	264                     ## 0x108
	.long	284                     ## 0x11c
	.long	263                     ## 0x107
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	285                     ## 0x11d
	.long	284                     ## 0x11c
	.long	264                     ## 0x108
	.long	285                     ## 0x11d
	.long	284                     ## 0x11c
	.long	264                     ## 0x108
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	265                     ## 0x109
	.long	285                     ## 0x11d
	.long	264                     ## 0x108
	.long	265                     ## 0x109
	.long	285                     ## 0x11d
	.long	264                     ## 0x108
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	286                     ## 0x11e
	.long	285                     ## 0x11d
	.long	265                     ## 0x109
	.long	286                     ## 0x11e
	.long	285                     ## 0x11d
	.long	265                     ## 0x109
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	266                     ## 0x10a
	.long	286                     ## 0x11e
	.long	265                     ## 0x109
	.long	266                     ## 0x10a
	.long	286                     ## 0x11e
	.long	265                     ## 0x109
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	287                     ## 0x11f
	.long	286                     ## 0x11e
	.long	266                     ## 0x10a
	.long	287                     ## 0x11f
	.long	286                     ## 0x11e
	.long	266                     ## 0x10a
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	267                     ## 0x10b
	.long	287                     ## 0x11f
	.long	266                     ## 0x10a
	.long	267                     ## 0x10b
	.long	287                     ## 0x11f
	.long	266                     ## 0x10a
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	288                     ## 0x120
	.long	287                     ## 0x11f
	.long	267                     ## 0x10b
	.long	288                     ## 0x120
	.long	287                     ## 0x11f
	.long	267                     ## 0x10b
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	268                     ## 0x10c
	.long	288                     ## 0x120
	.long	267                     ## 0x10b
	.long	268                     ## 0x10c
	.long	288                     ## 0x120
	.long	267                     ## 0x10b
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	289                     ## 0x121
	.long	288                     ## 0x120
	.long	268                     ## 0x10c
	.long	289                     ## 0x121
	.long	288                     ## 0x120
	.long	268                     ## 0x10c
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	269                     ## 0x10d
	.long	289                     ## 0x121
	.long	268                     ## 0x10c
	.long	269                     ## 0x10d
	.long	289                     ## 0x121
	.long	268                     ## 0x10c
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	290                     ## 0x122
	.long	289                     ## 0x121
	.long	269                     ## 0x10d
	.long	290                     ## 0x122
	.long	289                     ## 0x121
	.long	269                     ## 0x10d
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	270                     ## 0x10e
	.long	290                     ## 0x122
	.long	269                     ## 0x10d
	.long	270                     ## 0x10e
	.long	290                     ## 0x122
	.long	269                     ## 0x10d
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	291                     ## 0x123
	.long	290                     ## 0x122
	.long	270                     ## 0x10e
	.long	291                     ## 0x123
	.long	290                     ## 0x122
	.long	270                     ## 0x10e
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	271                     ## 0x10f
	.long	291                     ## 0x123
	.long	270                     ## 0x10e
	.long	271                     ## 0x10f
	.long	291                     ## 0x123
	.long	270                     ## 0x10e
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	292                     ## 0x124
	.long	291                     ## 0x123
	.long	271                     ## 0x10f
	.long	292                     ## 0x124
	.long	291                     ## 0x123
	.long	271                     ## 0x10f
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	272                     ## 0x110
	.long	292                     ## 0x124
	.long	271                     ## 0x10f
	.long	272                     ## 0x110
	.long	292                     ## 0x124
	.long	271                     ## 0x10f
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	293                     ## 0x125
	.long	292                     ## 0x124
	.long	272                     ## 0x110
	.long	293                     ## 0x125
	.long	292                     ## 0x124
	.long	272                     ## 0x110
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	273                     ## 0x111
	.long	293                     ## 0x125
	.long	272                     ## 0x110
	.long	273                     ## 0x111
	.long	293                     ## 0x125
	.long	272                     ## 0x110
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	294                     ## 0x126
	.long	293                     ## 0x125
	.long	273                     ## 0x111
	.long	294                     ## 0x126
	.long	293                     ## 0x125
	.long	273                     ## 0x111
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	274                     ## 0x112
	.long	294                     ## 0x126
	.long	273                     ## 0x111
	.long	274                     ## 0x112
	.long	294                     ## 0x126
	.long	273                     ## 0x111
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	295                     ## 0x127
	.long	294                     ## 0x126
	.long	274                     ## 0x112
	.long	295                     ## 0x127
	.long	294                     ## 0x126
	.long	274                     ## 0x112
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	275                     ## 0x113
	.long	295                     ## 0x127
	.long	274                     ## 0x112
	.long	275                     ## 0x113
	.long	295                     ## 0x127
	.long	274                     ## 0x112
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	296                     ## 0x128
	.long	295                     ## 0x127
	.long	275                     ## 0x113
	.long	296                     ## 0x128
	.long	295                     ## 0x127
	.long	275                     ## 0x113
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	276                     ## 0x114
	.long	296                     ## 0x128
	.long	275                     ## 0x113
	.long	276                     ## 0x114
	.long	296                     ## 0x128
	.long	275                     ## 0x113
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	297                     ## 0x129
	.long	296                     ## 0x128
	.long	276                     ## 0x114
	.long	297                     ## 0x129
	.long	296                     ## 0x128
	.long	276                     ## 0x114
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	277                     ## 0x115
	.long	297                     ## 0x129
	.long	276                     ## 0x114
	.long	277                     ## 0x115
	.long	297                     ## 0x129
	.long	276                     ## 0x114
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	298                     ## 0x12a
	.long	297                     ## 0x129
	.long	277                     ## 0x115
	.long	298                     ## 0x12a
	.long	297                     ## 0x129
	.long	277                     ## 0x115
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	278                     ## 0x116
	.long	298                     ## 0x12a
	.long	277                     ## 0x115
	.long	278                     ## 0x116
	.long	298                     ## 0x12a
	.long	277                     ## 0x115
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	299                     ## 0x12b
	.long	298                     ## 0x12a
	.long	278                     ## 0x116
	.long	299                     ## 0x12b
	.long	298                     ## 0x12a
	.long	278                     ## 0x116
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	279                     ## 0x117
	.long	299                     ## 0x12b
	.long	278                     ## 0x116
	.long	279                     ## 0x117
	.long	299                     ## 0x12b
	.long	278                     ## 0x116
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	280                     ## 0x118
	.long	299                     ## 0x12b
	.long	279                     ## 0x117
	.long	280                     ## 0x118
	.long	299                     ## 0x12b
	.long	279                     ## 0x117
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	280                     ## 0x118
	.long	300                     ## 0x12c
	.long	299                     ## 0x12b
	.long	280                     ## 0x118
	.long	300                     ## 0x12c
	.long	299                     ## 0x12b
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	301                     ## 0x12d
	.long	300                     ## 0x12c
	.long	280                     ## 0x118
	.long	301                     ## 0x12d
	.long	300                     ## 0x12c
	.long	280                     ## 0x118
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	281                     ## 0x119
	.long	301                     ## 0x12d
	.long	280                     ## 0x118
	.long	281                     ## 0x119
	.long	301                     ## 0x12d
	.long	280                     ## 0x118
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	302                     ## 0x12e
	.long	301                     ## 0x12d
	.long	281                     ## 0x119
	.long	302                     ## 0x12e
	.long	301                     ## 0x12d
	.long	281                     ## 0x119
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	282                     ## 0x11a
	.long	302                     ## 0x12e
	.long	281                     ## 0x119
	.long	282                     ## 0x11a
	.long	302                     ## 0x12e
	.long	281                     ## 0x119
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	303                     ## 0x12f
	.long	302                     ## 0x12e
	.long	282                     ## 0x11a
	.long	303                     ## 0x12f
	.long	302                     ## 0x12e
	.long	282                     ## 0x11a
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	283                     ## 0x11b
	.long	303                     ## 0x12f
	.long	282                     ## 0x11a
	.long	283                     ## 0x11b
	.long	303                     ## 0x12f
	.long	282                     ## 0x11a
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	304                     ## 0x130
	.long	303                     ## 0x12f
	.long	283                     ## 0x11b
	.long	304                     ## 0x130
	.long	303                     ## 0x12f
	.long	283                     ## 0x11b
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	284                     ## 0x11c
	.long	304                     ## 0x130
	.long	283                     ## 0x11b
	.long	284                     ## 0x11c
	.long	304                     ## 0x130
	.long	283                     ## 0x11b
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	305                     ## 0x131
	.long	304                     ## 0x130
	.long	284                     ## 0x11c
	.long	305                     ## 0x131
	.long	304                     ## 0x130
	.long	284                     ## 0x11c
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	285                     ## 0x11d
	.long	305                     ## 0x131
	.long	284                     ## 0x11c
	.long	285                     ## 0x11d
	.long	305                     ## 0x131
	.long	284                     ## 0x11c
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	306                     ## 0x132
	.long	305                     ## 0x131
	.long	285                     ## 0x11d
	.long	306                     ## 0x132
	.long	305                     ## 0x131
	.long	285                     ## 0x11d
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	286                     ## 0x11e
	.long	306                     ## 0x132
	.long	285                     ## 0x11d
	.long	286                     ## 0x11e
	.long	306                     ## 0x132
	.long	285                     ## 0x11d
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	307                     ## 0x133
	.long	306                     ## 0x132
	.long	286                     ## 0x11e
	.long	307                     ## 0x133
	.long	306                     ## 0x132
	.long	286                     ## 0x11e
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	287                     ## 0x11f
	.long	307                     ## 0x133
	.long	286                     ## 0x11e
	.long	287                     ## 0x11f
	.long	307                     ## 0x133
	.long	286                     ## 0x11e
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	308                     ## 0x134
	.long	307                     ## 0x133
	.long	287                     ## 0x11f
	.long	308                     ## 0x134
	.long	307                     ## 0x133
	.long	287                     ## 0x11f
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	288                     ## 0x120
	.long	308                     ## 0x134
	.long	287                     ## 0x11f
	.long	288                     ## 0x120
	.long	308                     ## 0x134
	.long	287                     ## 0x11f
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	309                     ## 0x135
	.long	308                     ## 0x134
	.long	288                     ## 0x120
	.long	309                     ## 0x135
	.long	308                     ## 0x134
	.long	288                     ## 0x120
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	289                     ## 0x121
	.long	309                     ## 0x135
	.long	288                     ## 0x120
	.long	289                     ## 0x121
	.long	309                     ## 0x135
	.long	288                     ## 0x120
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	310                     ## 0x136
	.long	309                     ## 0x135
	.long	289                     ## 0x121
	.long	310                     ## 0x136
	.long	309                     ## 0x135
	.long	289                     ## 0x121
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	290                     ## 0x122
	.long	310                     ## 0x136
	.long	289                     ## 0x121
	.long	290                     ## 0x122
	.long	310                     ## 0x136
	.long	289                     ## 0x121
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	311                     ## 0x137
	.long	310                     ## 0x136
	.long	290                     ## 0x122
	.long	311                     ## 0x137
	.long	310                     ## 0x136
	.long	290                     ## 0x122
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	291                     ## 0x123
	.long	311                     ## 0x137
	.long	290                     ## 0x122
	.long	291                     ## 0x123
	.long	311                     ## 0x137
	.long	290                     ## 0x122
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	312                     ## 0x138
	.long	311                     ## 0x137
	.long	291                     ## 0x123
	.long	312                     ## 0x138
	.long	311                     ## 0x137
	.long	291                     ## 0x123
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	292                     ## 0x124
	.long	312                     ## 0x138
	.long	291                     ## 0x123
	.long	292                     ## 0x124
	.long	312                     ## 0x138
	.long	291                     ## 0x123
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	313                     ## 0x139
	.long	312                     ## 0x138
	.long	292                     ## 0x124
	.long	313                     ## 0x139
	.long	312                     ## 0x138
	.long	292                     ## 0x124
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	293                     ## 0x125
	.long	313                     ## 0x139
	.long	292                     ## 0x124
	.long	293                     ## 0x125
	.long	313                     ## 0x139
	.long	292                     ## 0x124
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	314                     ## 0x13a
	.long	313                     ## 0x139
	.long	293                     ## 0x125
	.long	314                     ## 0x13a
	.long	313                     ## 0x139
	.long	293                     ## 0x125
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	294                     ## 0x126
	.long	314                     ## 0x13a
	.long	293                     ## 0x125
	.long	294                     ## 0x126
	.long	314                     ## 0x13a
	.long	293                     ## 0x125
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	315                     ## 0x13b
	.long	314                     ## 0x13a
	.long	294                     ## 0x126
	.long	315                     ## 0x13b
	.long	314                     ## 0x13a
	.long	294                     ## 0x126
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	295                     ## 0x127
	.long	315                     ## 0x13b
	.long	294                     ## 0x126
	.long	295                     ## 0x127
	.long	315                     ## 0x13b
	.long	294                     ## 0x126
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	316                     ## 0x13c
	.long	315                     ## 0x13b
	.long	295                     ## 0x127
	.long	316                     ## 0x13c
	.long	315                     ## 0x13b
	.long	295                     ## 0x127
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	296                     ## 0x128
	.long	316                     ## 0x13c
	.long	295                     ## 0x127
	.long	296                     ## 0x128
	.long	316                     ## 0x13c
	.long	295                     ## 0x127
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	317                     ## 0x13d
	.long	316                     ## 0x13c
	.long	296                     ## 0x128
	.long	317                     ## 0x13d
	.long	316                     ## 0x13c
	.long	296                     ## 0x128
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	297                     ## 0x129
	.long	317                     ## 0x13d
	.long	296                     ## 0x128
	.long	297                     ## 0x129
	.long	317                     ## 0x13d
	.long	296                     ## 0x128
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	318                     ## 0x13e
	.long	317                     ## 0x13d
	.long	297                     ## 0x129
	.long	318                     ## 0x13e
	.long	317                     ## 0x13d
	.long	297                     ## 0x129
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	298                     ## 0x12a
	.long	318                     ## 0x13e
	.long	297                     ## 0x129
	.long	298                     ## 0x12a
	.long	318                     ## 0x13e
	.long	297                     ## 0x129
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	319                     ## 0x13f
	.long	318                     ## 0x13e
	.long	298                     ## 0x12a
	.long	319                     ## 0x13f
	.long	318                     ## 0x13e
	.long	298                     ## 0x12a
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	299                     ## 0x12b
	.long	319                     ## 0x13f
	.long	298                     ## 0x12a
	.long	299                     ## 0x12b
	.long	319                     ## 0x13f
	.long	298                     ## 0x12a
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	300                     ## 0x12c
	.long	319                     ## 0x13f
	.long	299                     ## 0x12b
	.long	300                     ## 0x12c
	.long	319                     ## 0x13f
	.long	299                     ## 0x12b
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	300                     ## 0x12c
	.long	320                     ## 0x140
	.long	319                     ## 0x13f
	.long	300                     ## 0x12c
	.long	320                     ## 0x140
	.long	319                     ## 0x13f
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	321                     ## 0x141
	.long	320                     ## 0x140
	.long	300                     ## 0x12c
	.long	321                     ## 0x141
	.long	320                     ## 0x140
	.long	300                     ## 0x12c
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	301                     ## 0x12d
	.long	321                     ## 0x141
	.long	300                     ## 0x12c
	.long	301                     ## 0x12d
	.long	321                     ## 0x141
	.long	300                     ## 0x12c
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	322                     ## 0x142
	.long	321                     ## 0x141
	.long	301                     ## 0x12d
	.long	322                     ## 0x142
	.long	321                     ## 0x141
	.long	301                     ## 0x12d
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	302                     ## 0x12e
	.long	322                     ## 0x142
	.long	301                     ## 0x12d
	.long	302                     ## 0x12e
	.long	322                     ## 0x142
	.long	301                     ## 0x12d
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	323                     ## 0x143
	.long	322                     ## 0x142
	.long	302                     ## 0x12e
	.long	323                     ## 0x143
	.long	322                     ## 0x142
	.long	302                     ## 0x12e
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	303                     ## 0x12f
	.long	323                     ## 0x143
	.long	302                     ## 0x12e
	.long	303                     ## 0x12f
	.long	323                     ## 0x143
	.long	302                     ## 0x12e
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	324                     ## 0x144
	.long	323                     ## 0x143
	.long	303                     ## 0x12f
	.long	324                     ## 0x144
	.long	323                     ## 0x143
	.long	303                     ## 0x12f
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	304                     ## 0x130
	.long	324                     ## 0x144
	.long	303                     ## 0x12f
	.long	304                     ## 0x130
	.long	324                     ## 0x144
	.long	303                     ## 0x12f
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	325                     ## 0x145
	.long	324                     ## 0x144
	.long	304                     ## 0x130
	.long	325                     ## 0x145
	.long	324                     ## 0x144
	.long	304                     ## 0x130
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	305                     ## 0x131
	.long	325                     ## 0x145
	.long	304                     ## 0x130
	.long	305                     ## 0x131
	.long	325                     ## 0x145
	.long	304                     ## 0x130
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	326                     ## 0x146
	.long	325                     ## 0x145
	.long	305                     ## 0x131
	.long	326                     ## 0x146
	.long	325                     ## 0x145
	.long	305                     ## 0x131
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	306                     ## 0x132
	.long	326                     ## 0x146
	.long	305                     ## 0x131
	.long	306                     ## 0x132
	.long	326                     ## 0x146
	.long	305                     ## 0x131
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	327                     ## 0x147
	.long	326                     ## 0x146
	.long	306                     ## 0x132
	.long	327                     ## 0x147
	.long	326                     ## 0x146
	.long	306                     ## 0x132
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	307                     ## 0x133
	.long	327                     ## 0x147
	.long	306                     ## 0x132
	.long	307                     ## 0x133
	.long	327                     ## 0x147
	.long	306                     ## 0x132
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	328                     ## 0x148
	.long	327                     ## 0x147
	.long	307                     ## 0x133
	.long	328                     ## 0x148
	.long	327                     ## 0x147
	.long	307                     ## 0x133
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	308                     ## 0x134
	.long	328                     ## 0x148
	.long	307                     ## 0x133
	.long	308                     ## 0x134
	.long	328                     ## 0x148
	.long	307                     ## 0x133
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	329                     ## 0x149
	.long	328                     ## 0x148
	.long	308                     ## 0x134
	.long	329                     ## 0x149
	.long	328                     ## 0x148
	.long	308                     ## 0x134
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	309                     ## 0x135
	.long	329                     ## 0x149
	.long	308                     ## 0x134
	.long	309                     ## 0x135
	.long	329                     ## 0x149
	.long	308                     ## 0x134
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	330                     ## 0x14a
	.long	329                     ## 0x149
	.long	309                     ## 0x135
	.long	330                     ## 0x14a
	.long	329                     ## 0x149
	.long	309                     ## 0x135
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	310                     ## 0x136
	.long	330                     ## 0x14a
	.long	309                     ## 0x135
	.long	310                     ## 0x136
	.long	330                     ## 0x14a
	.long	309                     ## 0x135
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	331                     ## 0x14b
	.long	330                     ## 0x14a
	.long	310                     ## 0x136
	.long	331                     ## 0x14b
	.long	330                     ## 0x14a
	.long	310                     ## 0x136
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	311                     ## 0x137
	.long	331                     ## 0x14b
	.long	310                     ## 0x136
	.long	311                     ## 0x137
	.long	331                     ## 0x14b
	.long	310                     ## 0x136
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	332                     ## 0x14c
	.long	331                     ## 0x14b
	.long	311                     ## 0x137
	.long	332                     ## 0x14c
	.long	331                     ## 0x14b
	.long	311                     ## 0x137
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	312                     ## 0x138
	.long	332                     ## 0x14c
	.long	311                     ## 0x137
	.long	312                     ## 0x138
	.long	332                     ## 0x14c
	.long	311                     ## 0x137
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	333                     ## 0x14d
	.long	332                     ## 0x14c
	.long	312                     ## 0x138
	.long	333                     ## 0x14d
	.long	332                     ## 0x14c
	.long	312                     ## 0x138
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	313                     ## 0x139
	.long	333                     ## 0x14d
	.long	312                     ## 0x138
	.long	313                     ## 0x139
	.long	333                     ## 0x14d
	.long	312                     ## 0x138
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	334                     ## 0x14e
	.long	333                     ## 0x14d
	.long	313                     ## 0x139
	.long	334                     ## 0x14e
	.long	333                     ## 0x14d
	.long	313                     ## 0x139
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	314                     ## 0x13a
	.long	334                     ## 0x14e
	.long	313                     ## 0x139
	.long	314                     ## 0x13a
	.long	334                     ## 0x14e
	.long	313                     ## 0x139
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	335                     ## 0x14f
	.long	334                     ## 0x14e
	.long	314                     ## 0x13a
	.long	335                     ## 0x14f
	.long	334                     ## 0x14e
	.long	314                     ## 0x13a
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	315                     ## 0x13b
	.long	335                     ## 0x14f
	.long	314                     ## 0x13a
	.long	315                     ## 0x13b
	.long	335                     ## 0x14f
	.long	314                     ## 0x13a
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	336                     ## 0x150
	.long	335                     ## 0x14f
	.long	315                     ## 0x13b
	.long	336                     ## 0x150
	.long	335                     ## 0x14f
	.long	315                     ## 0x13b
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	316                     ## 0x13c
	.long	336                     ## 0x150
	.long	315                     ## 0x13b
	.long	316                     ## 0x13c
	.long	336                     ## 0x150
	.long	315                     ## 0x13b
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	337                     ## 0x151
	.long	336                     ## 0x150
	.long	316                     ## 0x13c
	.long	337                     ## 0x151
	.long	336                     ## 0x150
	.long	316                     ## 0x13c
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	317                     ## 0x13d
	.long	337                     ## 0x151
	.long	316                     ## 0x13c
	.long	317                     ## 0x13d
	.long	337                     ## 0x151
	.long	316                     ## 0x13c
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	338                     ## 0x152
	.long	337                     ## 0x151
	.long	317                     ## 0x13d
	.long	338                     ## 0x152
	.long	337                     ## 0x151
	.long	317                     ## 0x13d
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	318                     ## 0x13e
	.long	338                     ## 0x152
	.long	317                     ## 0x13d
	.long	318                     ## 0x13e
	.long	338                     ## 0x152
	.long	317                     ## 0x13d
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	339                     ## 0x153
	.long	338                     ## 0x152
	.long	318                     ## 0x13e
	.long	339                     ## 0x153
	.long	338                     ## 0x152
	.long	318                     ## 0x13e
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	319                     ## 0x13f
	.long	339                     ## 0x153
	.long	318                     ## 0x13e
	.long	319                     ## 0x13f
	.long	339                     ## 0x153
	.long	318                     ## 0x13e
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	320                     ## 0x140
	.long	339                     ## 0x153
	.long	319                     ## 0x13f
	.long	320                     ## 0x140
	.long	339                     ## 0x153
	.long	319                     ## 0x13f
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	320                     ## 0x140
	.long	340                     ## 0x154
	.long	339                     ## 0x153
	.long	320                     ## 0x140
	.long	340                     ## 0x154
	.long	339                     ## 0x153
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	341                     ## 0x155
	.long	340                     ## 0x154
	.long	320                     ## 0x140
	.long	341                     ## 0x155
	.long	340                     ## 0x154
	.long	320                     ## 0x140
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	321                     ## 0x141
	.long	341                     ## 0x155
	.long	320                     ## 0x140
	.long	321                     ## 0x141
	.long	341                     ## 0x155
	.long	320                     ## 0x140
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	342                     ## 0x156
	.long	341                     ## 0x155
	.long	321                     ## 0x141
	.long	342                     ## 0x156
	.long	341                     ## 0x155
	.long	321                     ## 0x141
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	322                     ## 0x142
	.long	342                     ## 0x156
	.long	321                     ## 0x141
	.long	322                     ## 0x142
	.long	342                     ## 0x156
	.long	321                     ## 0x141
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	343                     ## 0x157
	.long	342                     ## 0x156
	.long	322                     ## 0x142
	.long	343                     ## 0x157
	.long	342                     ## 0x156
	.long	322                     ## 0x142
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	323                     ## 0x143
	.long	343                     ## 0x157
	.long	322                     ## 0x142
	.long	323                     ## 0x143
	.long	343                     ## 0x157
	.long	322                     ## 0x142
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	344                     ## 0x158
	.long	343                     ## 0x157
	.long	323                     ## 0x143
	.long	344                     ## 0x158
	.long	343                     ## 0x157
	.long	323                     ## 0x143
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	324                     ## 0x144
	.long	344                     ## 0x158
	.long	323                     ## 0x143
	.long	324                     ## 0x144
	.long	344                     ## 0x158
	.long	323                     ## 0x143
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	345                     ## 0x159
	.long	344                     ## 0x158
	.long	324                     ## 0x144
	.long	345                     ## 0x159
	.long	344                     ## 0x158
	.long	324                     ## 0x144
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	325                     ## 0x145
	.long	345                     ## 0x159
	.long	324                     ## 0x144
	.long	325                     ## 0x145
	.long	345                     ## 0x159
	.long	324                     ## 0x144
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	346                     ## 0x15a
	.long	345                     ## 0x159
	.long	325                     ## 0x145
	.long	346                     ## 0x15a
	.long	345                     ## 0x159
	.long	325                     ## 0x145
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	326                     ## 0x146
	.long	346                     ## 0x15a
	.long	325                     ## 0x145
	.long	326                     ## 0x146
	.long	346                     ## 0x15a
	.long	325                     ## 0x145
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	347                     ## 0x15b
	.long	346                     ## 0x15a
	.long	326                     ## 0x146
	.long	347                     ## 0x15b
	.long	346                     ## 0x15a
	.long	326                     ## 0x146
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	327                     ## 0x147
	.long	347                     ## 0x15b
	.long	326                     ## 0x146
	.long	327                     ## 0x147
	.long	347                     ## 0x15b
	.long	326                     ## 0x146
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	348                     ## 0x15c
	.long	347                     ## 0x15b
	.long	327                     ## 0x147
	.long	348                     ## 0x15c
	.long	347                     ## 0x15b
	.long	327                     ## 0x147
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	328                     ## 0x148
	.long	348                     ## 0x15c
	.long	327                     ## 0x147
	.long	328                     ## 0x148
	.long	348                     ## 0x15c
	.long	327                     ## 0x147
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	349                     ## 0x15d
	.long	348                     ## 0x15c
	.long	328                     ## 0x148
	.long	349                     ## 0x15d
	.long	348                     ## 0x15c
	.long	328                     ## 0x148
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	329                     ## 0x149
	.long	349                     ## 0x15d
	.long	328                     ## 0x148
	.long	329                     ## 0x149
	.long	349                     ## 0x15d
	.long	328                     ## 0x148
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	350                     ## 0x15e
	.long	349                     ## 0x15d
	.long	329                     ## 0x149
	.long	350                     ## 0x15e
	.long	349                     ## 0x15d
	.long	329                     ## 0x149
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	330                     ## 0x14a
	.long	350                     ## 0x15e
	.long	329                     ## 0x149
	.long	330                     ## 0x14a
	.long	350                     ## 0x15e
	.long	329                     ## 0x149
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	351                     ## 0x15f
	.long	350                     ## 0x15e
	.long	330                     ## 0x14a
	.long	351                     ## 0x15f
	.long	350                     ## 0x15e
	.long	330                     ## 0x14a
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	331                     ## 0x14b
	.long	351                     ## 0x15f
	.long	330                     ## 0x14a
	.long	331                     ## 0x14b
	.long	351                     ## 0x15f
	.long	330                     ## 0x14a
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	352                     ## 0x160
	.long	351                     ## 0x15f
	.long	331                     ## 0x14b
	.long	352                     ## 0x160
	.long	351                     ## 0x15f
	.long	331                     ## 0x14b
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	332                     ## 0x14c
	.long	352                     ## 0x160
	.long	331                     ## 0x14b
	.long	332                     ## 0x14c
	.long	352                     ## 0x160
	.long	331                     ## 0x14b
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	353                     ## 0x161
	.long	352                     ## 0x160
	.long	332                     ## 0x14c
	.long	353                     ## 0x161
	.long	352                     ## 0x160
	.long	332                     ## 0x14c
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	333                     ## 0x14d
	.long	353                     ## 0x161
	.long	332                     ## 0x14c
	.long	333                     ## 0x14d
	.long	353                     ## 0x161
	.long	332                     ## 0x14c
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	354                     ## 0x162
	.long	353                     ## 0x161
	.long	333                     ## 0x14d
	.long	354                     ## 0x162
	.long	353                     ## 0x161
	.long	333                     ## 0x14d
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	334                     ## 0x14e
	.long	354                     ## 0x162
	.long	333                     ## 0x14d
	.long	334                     ## 0x14e
	.long	354                     ## 0x162
	.long	333                     ## 0x14d
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	355                     ## 0x163
	.long	354                     ## 0x162
	.long	334                     ## 0x14e
	.long	355                     ## 0x163
	.long	354                     ## 0x162
	.long	334                     ## 0x14e
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	335                     ## 0x14f
	.long	355                     ## 0x163
	.long	334                     ## 0x14e
	.long	335                     ## 0x14f
	.long	355                     ## 0x163
	.long	334                     ## 0x14e
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	356                     ## 0x164
	.long	355                     ## 0x163
	.long	335                     ## 0x14f
	.long	356                     ## 0x164
	.long	355                     ## 0x163
	.long	335                     ## 0x14f
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	336                     ## 0x150
	.long	356                     ## 0x164
	.long	335                     ## 0x14f
	.long	336                     ## 0x150
	.long	356                     ## 0x164
	.long	335                     ## 0x14f
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	357                     ## 0x165
	.long	356                     ## 0x164
	.long	336                     ## 0x150
	.long	357                     ## 0x165
	.long	356                     ## 0x164
	.long	336                     ## 0x150
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	337                     ## 0x151
	.long	357                     ## 0x165
	.long	336                     ## 0x150
	.long	337                     ## 0x151
	.long	357                     ## 0x165
	.long	336                     ## 0x150
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	358                     ## 0x166
	.long	357                     ## 0x165
	.long	337                     ## 0x151
	.long	358                     ## 0x166
	.long	357                     ## 0x165
	.long	337                     ## 0x151
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	338                     ## 0x152
	.long	358                     ## 0x166
	.long	337                     ## 0x151
	.long	338                     ## 0x152
	.long	358                     ## 0x166
	.long	337                     ## 0x151
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	359                     ## 0x167
	.long	358                     ## 0x166
	.long	338                     ## 0x152
	.long	359                     ## 0x167
	.long	358                     ## 0x166
	.long	338                     ## 0x152
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	339                     ## 0x153
	.long	359                     ## 0x167
	.long	338                     ## 0x152
	.long	339                     ## 0x153
	.long	359                     ## 0x167
	.long	338                     ## 0x152
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	340                     ## 0x154
	.long	359                     ## 0x167
	.long	339                     ## 0x153
	.long	340                     ## 0x154
	.long	359                     ## 0x167
	.long	339                     ## 0x153
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	340                     ## 0x154
	.long	360                     ## 0x168
	.long	359                     ## 0x167
	.long	340                     ## 0x154
	.long	360                     ## 0x168
	.long	359                     ## 0x167
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	361                     ## 0x169
	.long	360                     ## 0x168
	.long	340                     ## 0x154
	.long	361                     ## 0x169
	.long	360                     ## 0x168
	.long	340                     ## 0x154
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	341                     ## 0x155
	.long	361                     ## 0x169
	.long	340                     ## 0x154
	.long	341                     ## 0x155
	.long	361                     ## 0x169
	.long	340                     ## 0x154
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	362                     ## 0x16a
	.long	361                     ## 0x169
	.long	341                     ## 0x155
	.long	362                     ## 0x16a
	.long	361                     ## 0x169
	.long	341                     ## 0x155
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	342                     ## 0x156
	.long	362                     ## 0x16a
	.long	341                     ## 0x155
	.long	342                     ## 0x156
	.long	362                     ## 0x16a
	.long	341                     ## 0x155
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	363                     ## 0x16b
	.long	362                     ## 0x16a
	.long	342                     ## 0x156
	.long	363                     ## 0x16b
	.long	362                     ## 0x16a
	.long	342                     ## 0x156
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	343                     ## 0x157
	.long	363                     ## 0x16b
	.long	342                     ## 0x156
	.long	343                     ## 0x157
	.long	363                     ## 0x16b
	.long	342                     ## 0x156
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	364                     ## 0x16c
	.long	363                     ## 0x16b
	.long	343                     ## 0x157
	.long	364                     ## 0x16c
	.long	363                     ## 0x16b
	.long	343                     ## 0x157
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	344                     ## 0x158
	.long	364                     ## 0x16c
	.long	343                     ## 0x157
	.long	344                     ## 0x158
	.long	364                     ## 0x16c
	.long	343                     ## 0x157
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	365                     ## 0x16d
	.long	364                     ## 0x16c
	.long	344                     ## 0x158
	.long	365                     ## 0x16d
	.long	364                     ## 0x16c
	.long	344                     ## 0x158
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	345                     ## 0x159
	.long	365                     ## 0x16d
	.long	344                     ## 0x158
	.long	345                     ## 0x159
	.long	365                     ## 0x16d
	.long	344                     ## 0x158
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	366                     ## 0x16e
	.long	365                     ## 0x16d
	.long	345                     ## 0x159
	.long	366                     ## 0x16e
	.long	365                     ## 0x16d
	.long	345                     ## 0x159
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	346                     ## 0x15a
	.long	366                     ## 0x16e
	.long	345                     ## 0x159
	.long	346                     ## 0x15a
	.long	366                     ## 0x16e
	.long	345                     ## 0x159
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	367                     ## 0x16f
	.long	366                     ## 0x16e
	.long	346                     ## 0x15a
	.long	367                     ## 0x16f
	.long	366                     ## 0x16e
	.long	346                     ## 0x15a
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	347                     ## 0x15b
	.long	367                     ## 0x16f
	.long	346                     ## 0x15a
	.long	347                     ## 0x15b
	.long	367                     ## 0x16f
	.long	346                     ## 0x15a
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	368                     ## 0x170
	.long	367                     ## 0x16f
	.long	347                     ## 0x15b
	.long	368                     ## 0x170
	.long	367                     ## 0x16f
	.long	347                     ## 0x15b
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	348                     ## 0x15c
	.long	368                     ## 0x170
	.long	347                     ## 0x15b
	.long	348                     ## 0x15c
	.long	368                     ## 0x170
	.long	347                     ## 0x15b
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	369                     ## 0x171
	.long	368                     ## 0x170
	.long	348                     ## 0x15c
	.long	369                     ## 0x171
	.long	368                     ## 0x170
	.long	348                     ## 0x15c
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	349                     ## 0x15d
	.long	369                     ## 0x171
	.long	348                     ## 0x15c
	.long	349                     ## 0x15d
	.long	369                     ## 0x171
	.long	348                     ## 0x15c
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	370                     ## 0x172
	.long	369                     ## 0x171
	.long	349                     ## 0x15d
	.long	370                     ## 0x172
	.long	369                     ## 0x171
	.long	349                     ## 0x15d
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	350                     ## 0x15e
	.long	370                     ## 0x172
	.long	349                     ## 0x15d
	.long	350                     ## 0x15e
	.long	370                     ## 0x172
	.long	349                     ## 0x15d
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	371                     ## 0x173
	.long	370                     ## 0x172
	.long	350                     ## 0x15e
	.long	371                     ## 0x173
	.long	370                     ## 0x172
	.long	350                     ## 0x15e
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	351                     ## 0x15f
	.long	371                     ## 0x173
	.long	350                     ## 0x15e
	.long	351                     ## 0x15f
	.long	371                     ## 0x173
	.long	350                     ## 0x15e
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	372                     ## 0x174
	.long	371                     ## 0x173
	.long	351                     ## 0x15f
	.long	372                     ## 0x174
	.long	371                     ## 0x173
	.long	351                     ## 0x15f
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	352                     ## 0x160
	.long	372                     ## 0x174
	.long	351                     ## 0x15f
	.long	352                     ## 0x160
	.long	372                     ## 0x174
	.long	351                     ## 0x15f
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	373                     ## 0x175
	.long	372                     ## 0x174
	.long	352                     ## 0x160
	.long	373                     ## 0x175
	.long	372                     ## 0x174
	.long	352                     ## 0x160
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	353                     ## 0x161
	.long	373                     ## 0x175
	.long	352                     ## 0x160
	.long	353                     ## 0x161
	.long	373                     ## 0x175
	.long	352                     ## 0x160
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	374                     ## 0x176
	.long	373                     ## 0x175
	.long	353                     ## 0x161
	.long	374                     ## 0x176
	.long	373                     ## 0x175
	.long	353                     ## 0x161
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	354                     ## 0x162
	.long	374                     ## 0x176
	.long	353                     ## 0x161
	.long	354                     ## 0x162
	.long	374                     ## 0x176
	.long	353                     ## 0x161
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	375                     ## 0x177
	.long	374                     ## 0x176
	.long	354                     ## 0x162
	.long	375                     ## 0x177
	.long	374                     ## 0x176
	.long	354                     ## 0x162
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	355                     ## 0x163
	.long	375                     ## 0x177
	.long	354                     ## 0x162
	.long	355                     ## 0x163
	.long	375                     ## 0x177
	.long	354                     ## 0x162
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	376                     ## 0x178
	.long	375                     ## 0x177
	.long	355                     ## 0x163
	.long	376                     ## 0x178
	.long	375                     ## 0x177
	.long	355                     ## 0x163
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	356                     ## 0x164
	.long	376                     ## 0x178
	.long	355                     ## 0x163
	.long	356                     ## 0x164
	.long	376                     ## 0x178
	.long	355                     ## 0x163
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	377                     ## 0x179
	.long	376                     ## 0x178
	.long	356                     ## 0x164
	.long	377                     ## 0x179
	.long	376                     ## 0x178
	.long	356                     ## 0x164
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	357                     ## 0x165
	.long	377                     ## 0x179
	.long	356                     ## 0x164
	.long	357                     ## 0x165
	.long	377                     ## 0x179
	.long	356                     ## 0x164
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	378                     ## 0x17a
	.long	377                     ## 0x179
	.long	357                     ## 0x165
	.long	378                     ## 0x17a
	.long	377                     ## 0x179
	.long	357                     ## 0x165
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	358                     ## 0x166
	.long	378                     ## 0x17a
	.long	357                     ## 0x165
	.long	358                     ## 0x166
	.long	378                     ## 0x17a
	.long	357                     ## 0x165
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	379                     ## 0x17b
	.long	378                     ## 0x17a
	.long	358                     ## 0x166
	.long	379                     ## 0x17b
	.long	378                     ## 0x17a
	.long	358                     ## 0x166
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	359                     ## 0x167
	.long	379                     ## 0x17b
	.long	358                     ## 0x166
	.long	359                     ## 0x167
	.long	379                     ## 0x17b
	.long	358                     ## 0x166
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	360                     ## 0x168
	.long	379                     ## 0x17b
	.long	359                     ## 0x167
	.long	360                     ## 0x168
	.long	379                     ## 0x17b
	.long	359                     ## 0x167
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	360                     ## 0x168
	.long	380                     ## 0x17c
	.long	379                     ## 0x17b
	.long	360                     ## 0x168
	.long	380                     ## 0x17c
	.long	379                     ## 0x17b
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	379                     ## 0x17b
	.long	380                     ## 0x17c
	.long	378                     ## 0x17a
	.long	379                     ## 0x17b
	.long	380                     ## 0x17c
	.long	378                     ## 0x17a
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	378                     ## 0x17a
	.long	380                     ## 0x17c
	.long	377                     ## 0x179
	.long	378                     ## 0x17a
	.long	380                     ## 0x17c
	.long	377                     ## 0x179
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	377                     ## 0x179
	.long	380                     ## 0x17c
	.long	376                     ## 0x178
	.long	377                     ## 0x179
	.long	380                     ## 0x17c
	.long	376                     ## 0x178
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	376                     ## 0x178
	.long	380                     ## 0x17c
	.long	375                     ## 0x177
	.long	376                     ## 0x178
	.long	380                     ## 0x17c
	.long	375                     ## 0x177
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	375                     ## 0x177
	.long	380                     ## 0x17c
	.long	374                     ## 0x176
	.long	375                     ## 0x177
	.long	380                     ## 0x17c
	.long	374                     ## 0x176
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	374                     ## 0x176
	.long	380                     ## 0x17c
	.long	373                     ## 0x175
	.long	374                     ## 0x176
	.long	380                     ## 0x17c
	.long	373                     ## 0x175
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	373                     ## 0x175
	.long	380                     ## 0x17c
	.long	372                     ## 0x174
	.long	373                     ## 0x175
	.long	380                     ## 0x17c
	.long	372                     ## 0x174
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	372                     ## 0x174
	.long	380                     ## 0x17c
	.long	371                     ## 0x173
	.long	372                     ## 0x174
	.long	380                     ## 0x17c
	.long	371                     ## 0x173
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	371                     ## 0x173
	.long	380                     ## 0x17c
	.long	370                     ## 0x172
	.long	371                     ## 0x173
	.long	380                     ## 0x17c
	.long	370                     ## 0x172
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	370                     ## 0x172
	.long	380                     ## 0x17c
	.long	369                     ## 0x171
	.long	370                     ## 0x172
	.long	380                     ## 0x17c
	.long	369                     ## 0x171
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	369                     ## 0x171
	.long	380                     ## 0x17c
	.long	368                     ## 0x170
	.long	369                     ## 0x171
	.long	380                     ## 0x17c
	.long	368                     ## 0x170
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	368                     ## 0x170
	.long	380                     ## 0x17c
	.long	367                     ## 0x16f
	.long	368                     ## 0x170
	.long	380                     ## 0x17c
	.long	367                     ## 0x16f
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	367                     ## 0x16f
	.long	380                     ## 0x17c
	.long	366                     ## 0x16e
	.long	367                     ## 0x16f
	.long	380                     ## 0x17c
	.long	366                     ## 0x16e
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	366                     ## 0x16e
	.long	380                     ## 0x17c
	.long	365                     ## 0x16d
	.long	366                     ## 0x16e
	.long	380                     ## 0x17c
	.long	365                     ## 0x16d
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	365                     ## 0x16d
	.long	380                     ## 0x17c
	.long	364                     ## 0x16c
	.long	365                     ## 0x16d
	.long	380                     ## 0x17c
	.long	364                     ## 0x16c
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	364                     ## 0x16c
	.long	380                     ## 0x17c
	.long	363                     ## 0x16b
	.long	364                     ## 0x16c
	.long	380                     ## 0x17c
	.long	363                     ## 0x16b
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	363                     ## 0x16b
	.long	380                     ## 0x17c
	.long	362                     ## 0x16a
	.long	363                     ## 0x16b
	.long	380                     ## 0x17c
	.long	362                     ## 0x16a
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	362                     ## 0x16a
	.long	380                     ## 0x17c
	.long	361                     ## 0x169
	.long	362                     ## 0x16a
	.long	380                     ## 0x17c
	.long	361                     ## 0x169
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	361                     ## 0x169
	.long	380                     ## 0x17c
	.long	360                     ## 0x168
	.long	361                     ## 0x169
	.long	380                     ## 0x17c
	.long	360                     ## 0x168
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	2                       ## 0x2
	.long	381                     ## 0x17d
	.long	0                       ## 0x0
	.long	2                       ## 0x2
	.long	381                     ## 0x17d
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	4                       ## 0x4
	.long	381                     ## 0x17d
	.long	2                       ## 0x2
	.long	4                       ## 0x4
	.long	381                     ## 0x17d
	.long	2                       ## 0x2
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	6                       ## 0x6
	.long	381                     ## 0x17d
	.long	4                       ## 0x4
	.long	6                       ## 0x6
	.long	381                     ## 0x17d
	.long	4                       ## 0x4
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	8                       ## 0x8
	.long	381                     ## 0x17d
	.long	6                       ## 0x6
	.long	8                       ## 0x8
	.long	381                     ## 0x17d
	.long	6                       ## 0x6
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	10                      ## 0xa
	.long	381                     ## 0x17d
	.long	8                       ## 0x8
	.long	10                      ## 0xa
	.long	381                     ## 0x17d
	.long	8                       ## 0x8
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	12                      ## 0xc
	.long	381                     ## 0x17d
	.long	10                      ## 0xa
	.long	12                      ## 0xc
	.long	381                     ## 0x17d
	.long	10                      ## 0xa
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	14                      ## 0xe
	.long	381                     ## 0x17d
	.long	12                      ## 0xc
	.long	14                      ## 0xe
	.long	381                     ## 0x17d
	.long	12                      ## 0xc
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	16                      ## 0x10
	.long	381                     ## 0x17d
	.long	14                      ## 0xe
	.long	16                      ## 0x10
	.long	381                     ## 0x17d
	.long	14                      ## 0xe
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	18                      ## 0x12
	.long	381                     ## 0x17d
	.long	16                      ## 0x10
	.long	18                      ## 0x12
	.long	381                     ## 0x17d
	.long	16                      ## 0x10
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	20                      ## 0x14
	.long	381                     ## 0x17d
	.long	18                      ## 0x12
	.long	20                      ## 0x14
	.long	381                     ## 0x17d
	.long	18                      ## 0x12
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	22                      ## 0x16
	.long	381                     ## 0x17d
	.long	20                      ## 0x14
	.long	22                      ## 0x16
	.long	381                     ## 0x17d
	.long	20                      ## 0x14
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	24                      ## 0x18
	.long	381                     ## 0x17d
	.long	22                      ## 0x16
	.long	24                      ## 0x18
	.long	381                     ## 0x17d
	.long	22                      ## 0x16
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	26                      ## 0x1a
	.long	381                     ## 0x17d
	.long	24                      ## 0x18
	.long	26                      ## 0x1a
	.long	381                     ## 0x17d
	.long	24                      ## 0x18
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	28                      ## 0x1c
	.long	381                     ## 0x17d
	.long	26                      ## 0x1a
	.long	28                      ## 0x1c
	.long	381                     ## 0x17d
	.long	26                      ## 0x1a
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	30                      ## 0x1e
	.long	381                     ## 0x17d
	.long	28                      ## 0x1c
	.long	30                      ## 0x1e
	.long	381                     ## 0x17d
	.long	28                      ## 0x1c
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	32                      ## 0x20
	.long	381                     ## 0x17d
	.long	30                      ## 0x1e
	.long	32                      ## 0x20
	.long	381                     ## 0x17d
	.long	30                      ## 0x1e
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	34                      ## 0x22
	.long	381                     ## 0x17d
	.long	32                      ## 0x20
	.long	34                      ## 0x22
	.long	381                     ## 0x17d
	.long	32                      ## 0x20
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	36                      ## 0x24
	.long	381                     ## 0x17d
	.long	34                      ## 0x22
	.long	36                      ## 0x24
	.long	381                     ## 0x17d
	.long	34                      ## 0x22
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	38                      ## 0x26
	.long	381                     ## 0x17d
	.long	36                      ## 0x24
	.long	38                      ## 0x26
	.long	381                     ## 0x17d
	.long	36                      ## 0x24
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	381                     ## 0x17d
	.long	38                      ## 0x26
	.long	0                       ## 0x0
	.long	381                     ## 0x17d
	.long	38                      ## 0x26
	.long	0                       ## 0x0
	.long	0                       ## 0x0
	.long	0                       ## 0x0

	.globl	_vertices               ## @vertices
	.p2align	4
_vertices:
	.long	3180878147              ## float -0.0743890032
	.long	3204241695              ## float -0.493844002
	.long	3167093046              ## float -0.0241704993
	.long	3189143811              ## float -0.146945998
	.long	3203627112              ## float -0.475528002
	.long	3175321882              ## float -0.0477458015
	.long	3179387001              ## float -0.0632790998
	.long	3204241695              ## float -0.493844002
	.long	3174846536              ## float -0.0459749997
	.long	3187671040              ## float -0.125
	.long	3203627112              ## float -0.475528002
	.long	3183083183              ## float -0.0908178016
	.long	3174846536              ## float -0.0459749997
	.long	3204241695              ## float -0.493844002
	.long	3179387001              ## float -0.0632790998
	.long	3183083183              ## float -0.0908178016
	.long	3203627112              ## float -0.475528002
	.long	3187671040              ## float -0.125
	.long	3167093046              ## float -0.0241704993
	.long	3204241695              ## float -0.493844002
	.long	3180878147              ## float -0.0743890032
	.long	3175321882              ## float -0.0477458015
	.long	3203627112              ## float -0.475528002
	.long	3189143811              ## float -0.146945998
	.long	0                       ## float 0
	.long	3204241695              ## float -0.493844002
	.long	3181391972              ## float -0.0782172977
	.long	0                       ## float 0
	.long	3203627112              ## float -0.475528002
	.long	3189651355              ## float -0.154508993
	.long	1019609398              ## float 0.0241704993
	.long	3204241695              ## float -0.493844002
	.long	3180878147              ## float -0.0743890032
	.long	1027838234              ## float 0.0477458015
	.long	3203627112              ## float -0.475528002
	.long	3189143811              ## float -0.146945998
	.long	1027362888              ## float 0.0459749997
	.long	3204241695              ## float -0.493844002
	.long	3179387001              ## float -0.0632790998
	.long	1035599535              ## float 0.0908178016
	.long	3203627112              ## float -0.475528002
	.long	3187671040              ## float -0.125
	.long	1031903353              ## float 0.0632790998
	.long	3204241695              ## float -0.493844002
	.long	3174846536              ## float -0.0459749997
	.long	1040187392              ## float 0.125
	.long	3203627112              ## float -0.475528002
	.long	3183083183              ## float -0.0908178016
	.long	1033394499              ## float 0.0743890032
	.long	3204241695              ## float -0.493844002
	.long	3167093046              ## float -0.0241704993
	.long	1041660163              ## float 0.146945998
	.long	3203627112              ## float -0.475528002
	.long	3175321882              ## float -0.0477458015
	.long	1033908324              ## float 0.0782172977
	.long	3204241695              ## float -0.493844002
	.long	0                       ## float 0
	.long	1042167707              ## float 0.154508993
	.long	3203627112              ## float -0.475528002
	.long	0                       ## float 0
	.long	1033394499              ## float 0.0743890032
	.long	3204241695              ## float -0.493844002
	.long	1019609398              ## float 0.0241704993
	.long	1041660163              ## float 0.146945998
	.long	3203627112              ## float -0.475528002
	.long	1027838234              ## float 0.0477458015
	.long	1031903353              ## float 0.0632790998
	.long	3204241695              ## float -0.493844002
	.long	1027362888              ## float 0.0459749997
	.long	1040187392              ## float 0.125
	.long	3203627112              ## float -0.475528002
	.long	1035599535              ## float 0.0908178016
	.long	1027362888              ## float 0.0459749997
	.long	3204241695              ## float -0.493844002
	.long	1031903353              ## float 0.0632790998
	.long	1035599535              ## float 0.0908178016
	.long	3203627112              ## float -0.475528002
	.long	1040187392              ## float 0.125
	.long	1019609398              ## float 0.0241704993
	.long	3204241695              ## float -0.493844002
	.long	1033394499              ## float 0.0743890032
	.long	1027838234              ## float 0.0477458015
	.long	3203627112              ## float -0.475528002
	.long	1041660163              ## float 0.146945998
	.long	824193137               ## float 2.33106001E-9
	.long	3204241695              ## float -0.493844002
	.long	1033908324              ## float 0.0782172977
	.long	832452469               ## float 4.60470995E-9
	.long	3203627112              ## float -0.475528002
	.long	1042167707              ## float 0.154508993
	.long	3167093046              ## float -0.0241704993
	.long	3204241695              ## float -0.493844002
	.long	1033394499              ## float 0.0743890032
	.long	3175321855              ## float -0.0477457009
	.long	3203627112              ## float -0.475528002
	.long	1041660163              ## float 0.146945998
	.long	3174846509              ## float -0.0459748991
	.long	3204241695              ## float -0.493844002
	.long	1031903353              ## float 0.0632790998
	.long	3183083183              ## float -0.0908178016
	.long	3203627112              ## float -0.475528002
	.long	1040187392              ## float 0.125
	.long	3179387001              ## float -0.0632790998
	.long	3204241695              ## float -0.493844002
	.long	1027362861              ## float 0.0459748991
	.long	3187671040              ## float -0.125
	.long	3203627112              ## float -0.475528002
	.long	1035599535              ## float 0.0908178016
	.long	3180878147              ## float -0.0743890032
	.long	3204241695              ## float -0.493844002
	.long	1019609398              ## float 0.0241704993
	.long	3189143811              ## float -0.146945998
	.long	3203627112              ## float -0.475528002
	.long	1027838234              ## float 0.0477458015
	.long	3181391972              ## float -0.0782172977
	.long	3204241695              ## float -0.493844002
	.long	0                       ## float 0
	.long	3189651355              ## float -0.154508993
	.long	3203627112              ## float -0.475528002
	.long	0                       ## float 0
	.long	1032824932              ## float 0.0701453983
	.long	3202619640              ## float -0.445502996
	.long	1046286581              ## float 0.215884998
	.long	1040752784              ## float 0.133424997
	.long	3202619640              ## float -0.445502996
	.long	1044122857              ## float 0.183642998
	.long	1044122857              ## float 0.183642998
	.long	3202619640              ## float -0.445502996
	.long	1040752784              ## float 0.133424997
	.long	1046286581              ## float 0.215884998
	.long	3202619640              ## float -0.445502996
	.long	1032824932              ## float 0.0701453983
	.long	1047032161              ## float 0.226995006
	.long	3202619640              ## float -0.445502996
	.long	0                       ## float 0
	.long	1046286581              ## float 0.215884998
	.long	3202619640              ## float -0.445502996
	.long	3180308580              ## float -0.0701453983
	.long	1044122857              ## float 0.183642998
	.long	3202619640              ## float -0.445502996
	.long	3188236432              ## float -0.133424997
	.long	1040752784              ## float 0.133424997
	.long	3202619640              ## float -0.445502996
	.long	3191606505              ## float -0.183642998
	.long	1032824946              ## float 0.0701455027
	.long	3202619640              ## float -0.445502996
	.long	3193770229              ## float -0.215884998
	.long	0                       ## float 0
	.long	3202619640              ## float -0.445502996
	.long	3194515809              ## float -0.226995006
	.long	3180308594              ## float -0.0701455027
	.long	3202619640              ## float -0.445502996
	.long	3193770229              ## float -0.215884998
	.long	3188236432              ## float -0.133424997
	.long	3202619640              ## float -0.445502996
	.long	3191606505              ## float -0.183642998
	.long	3191606505              ## float -0.183642998
	.long	3202619640              ## float -0.445502996
	.long	3188236432              ## float -0.133424997
	.long	3193770229              ## float -0.215884998
	.long	3202619640              ## float -0.445502996
	.long	3180308594              ## float -0.0701455027
	.long	3194515809              ## float -0.226995006
	.long	3202619640              ## float -0.445502996
	.long	0                       ## float 0
	.long	3193770229              ## float -0.215884998
	.long	3202619640              ## float -0.445502996
	.long	1032824932              ## float 0.0701453983
	.long	3191606505              ## float -0.183642998
	.long	3202619640              ## float -0.445502996
	.long	1040752784              ## float 0.133424997
	.long	3188236365              ## float -0.133423999
	.long	3202619640              ## float -0.445502996
	.long	1044122857              ## float 0.183642998
	.long	3180308580              ## float -0.0701453983
	.long	3202619640              ## float -0.445502996
	.long	1046286581              ## float 0.215884998
	.long	837316987               ## float 6.7649899E-9
	.long	3202619640              ## float -0.445502996
	.long	1047032161              ## float 0.226995006
	.long	1035599535              ## float 0.0908178016
	.long	3201244110              ## float -0.404509008
	.long	1049566158              ## float 0.279509008
	.long	1043391572              ## float 0.172746003
	.long	3201244110              ## float -0.404509008
	.long	1047754856              ## float 0.237764001
	.long	1047754856              ## float 0.237764001
	.long	3201244110              ## float -0.404509008
	.long	1043391572              ## float 0.172746003
	.long	1049566158              ## float 0.279509008
	.long	3201244110              ## float -0.404509008
	.long	1035599535              ## float 0.0908178016
	.long	1050048805              ## float 0.293893009
	.long	3201244110              ## float -0.404509008
	.long	0                       ## float 0
	.long	1049566158              ## float 0.279509008
	.long	3201244110              ## float -0.404509008
	.long	3183083183              ## float -0.0908178016
	.long	1047754856              ## float 0.237764001
	.long	3201244110              ## float -0.404509008
	.long	3190875220              ## float -0.172746003
	.long	1043391572              ## float 0.172746003
	.long	3201244110              ## float -0.404509008
	.long	3195238504              ## float -0.237764001
	.long	1035599535              ## float 0.0908178016
	.long	3201244110              ## float -0.404509008
	.long	3197049806              ## float -0.279509008
	.long	0                       ## float 0
	.long	3201244110              ## float -0.404509008
	.long	3197532453              ## float -0.293893009
	.long	3183083183              ## float -0.0908178016
	.long	3201244110              ## float -0.404509008
	.long	3197049806              ## float -0.279509008
	.long	3190875220              ## float -0.172746003
	.long	3201244110              ## float -0.404509008
	.long	3195238504              ## float -0.237764001
	.long	3195238504              ## float -0.237764001
	.long	3201244110              ## float -0.404509008
	.long	3190875220              ## float -0.172746003
	.long	3197049806              ## float -0.279509008
	.long	3201244110              ## float -0.404509008
	.long	3183083183              ## float -0.0908178016
	.long	3197532453              ## float -0.293893009
	.long	3201244110              ## float -0.404509008
	.long	0                       ## float 0
	.long	3197049806              ## float -0.279509008
	.long	3201244110              ## float -0.404509008
	.long	1035599535              ## float 0.0908178016
	.long	3195238504              ## float -0.237764001
	.long	3201244110              ## float -0.404509008
	.long	1043391572              ## float 0.172746003
	.long	3190875220              ## float -0.172746003
	.long	3201244110              ## float -0.404509008
	.long	1047754856              ## float 0.237764001
	.long	3183083183              ## float -0.0908178016
	.long	3201244110              ## float -0.404509008
	.long	1049566158              ## float 0.279509008
	.long	840333600               ## float 8.75868977E-9
	.long	3201244110              ## float -0.404509008
	.long	1050048805              ## float 0.293893009
	.long	1038074000              ## float 0.109254003
	.long	3199534310              ## float -0.353552997
	.long	1051470036              ## float 0.336248994
	.long	1045744945              ## float 0.207813993
	.long	3199534310              ## float -0.353552997
	.long	1049785000              ## float 0.286031008
	.long	1049785000              ## float 0.286031008
	.long	3199534310              ## float -0.353552997
	.long	1045744945              ## float 0.207813993
	.long	1051470036              ## float 0.336248994
	.long	3199534310              ## float -0.353552997
	.long	1038074000              ## float 0.109254003
	.long	1052050662              ## float 0.353552997
	.long	3199534310              ## float -0.353552997
	.long	0                       ## float 0
	.long	1051470036              ## float 0.336248994
	.long	3199534310              ## float -0.353552997
	.long	3185557648              ## float -0.109254003
	.long	1049785000              ## float 0.286031008
	.long	3199534310              ## float -0.353552997
	.long	3193228593              ## float -0.207813993
	.long	1045744945              ## float 0.207813993
	.long	3199534310              ## float -0.353552997
	.long	3197268648              ## float -0.286031008
	.long	1038074000              ## float 0.109254003
	.long	3199534310              ## float -0.353552997
	.long	3198953684              ## float -0.336248994
	.long	0                       ## float 0
	.long	3199534310              ## float -0.353552997
	.long	3199534344              ## float -0.35355401
	.long	3185557648              ## float -0.109254003
	.long	3199534310              ## float -0.353552997
	.long	3198953684              ## float -0.336248994
	.long	3193228593              ## float -0.207813993
	.long	3199534310              ## float -0.353552997
	.long	3197268648              ## float -0.286031008
	.long	3197268648              ## float -0.286031008
	.long	3199534310              ## float -0.353552997
	.long	3193228593              ## float -0.207813993
	.long	3198953684              ## float -0.336248994
	.long	3199534310              ## float -0.353552997
	.long	3185557648              ## float -0.109254003
	.long	3199534310              ## float -0.353552997
	.long	3199534310              ## float -0.353552997
	.long	0                       ## float 0
	.long	3198953684              ## float -0.336248994
	.long	3199534310              ## float -0.353552997
	.long	1038074000              ## float 0.109254003
	.long	3197268648              ## float -0.286031008
	.long	3199534310              ## float -0.353552997
	.long	1045744945              ## float 0.207813993
	.long	3193228526              ## float -0.207812995
	.long	3199534310              ## float -0.353552997
	.long	1049785000              ## float 0.286031008
	.long	3185557648              ## float -0.109254003
	.long	3199534310              ## float -0.353552997
	.long	1051470036              ## float 0.336248994
	.long	842335462               ## float 1.05367004E-8
	.long	3199534310              ## float -0.353552997
	.long	1052050662              ## float 0.353552997
	.long	1040187392              ## float 0.125
	.long	3197532453              ## float -0.293893009
	.long	1053096118              ## float 0.384710014
	.long	1047754856              ## float 0.237764001
	.long	3197532453              ## float -0.293893009
	.long	1051168214              ## float 0.327253997
	.long	1051168214              ## float 0.327253997
	.long	3197532453              ## float -0.293893009
	.long	1047754856              ## float 0.237764001
	.long	1053096151              ## float 0.384710997
	.long	3197532453              ## float -0.293893009
	.long	1040187392              ## float 0.125
	.long	1053760462              ## float 0.404509008
	.long	3197532453              ## float -0.293893009
	.long	0                       ## float 0
	.long	1053096151              ## float 0.384710997
	.long	3197532453              ## float -0.293893009
	.long	3187671040              ## float -0.125
	.long	1051168214              ## float 0.327253997
	.long	3197532453              ## float -0.293893009
	.long	3195238504              ## float -0.237764001
	.long	1047754856              ## float 0.237764001
	.long	3197532453              ## float -0.293893009
	.long	3198651862              ## float -0.327253997
	.long	1040187392              ## float 0.125
	.long	3197532453              ## float -0.293893009
	.long	3200579799              ## float -0.384710997
	.long	0                       ## float 0
	.long	3197532453              ## float -0.293893009
	.long	3201244110              ## float -0.404509008
	.long	3187671040              ## float -0.125
	.long	3197532453              ## float -0.293893009
	.long	3200579799              ## float -0.384710997
	.long	3195238504              ## float -0.237764001
	.long	3197532453              ## float -0.293893009
	.long	3198651862              ## float -0.327253997
	.long	3198651862              ## float -0.327253997
	.long	3197532453              ## float -0.293893009
	.long	3195238504              ## float -0.237764001
	.long	3200579799              ## float -0.384710997
	.long	3197532453              ## float -0.293893009
	.long	3187671040              ## float -0.125
	.long	3201244110              ## float -0.404509008
	.long	3197532453              ## float -0.293893009
	.long	0                       ## float 0
	.long	3200579766              ## float -0.384710014
	.long	3197532453              ## float -0.293893009
	.long	1040187392              ## float 0.125
	.long	3198651862              ## float -0.327253997
	.long	3197532453              ## float -0.293893009
	.long	1047754856              ## float 0.237764001
	.long	3195238504              ## float -0.237764001
	.long	3197532453              ## float -0.293893009
	.long	1051168214              ## float 0.327253997
	.long	3187671040              ## float -0.125
	.long	3197532453              ## float -0.293893009
	.long	1053096118              ## float 0.384710014
	.long	844045253               ## float 1.20552999E-8
	.long	3197532453              ## float -0.293893009
	.long	1053760462              ## float 0.404509008
	.long	1041037527              ## float 0.137667999
	.long	3194515809              ## float -0.226995006
	.long	1054404371              ## float 0.423698992
	.long	1048973956              ## float 0.261860013
	.long	3194515809              ## float -0.226995006
	.long	1052281080              ## float 0.360419989
	.long	1052281080              ## float 0.360419989
	.long	3194515809              ## float -0.226995006
	.long	1048973956              ## float 0.261860013
	.long	1054404371              ## float 0.423698992
	.long	3194515809              ## float -0.226995006
	.long	1041037527              ## float 0.137667999
	.long	1055135992              ## float 0.445502996
	.long	3194515809              ## float -0.226995006
	.long	0                       ## float 0
	.long	1054404371              ## float 0.423698992
	.long	3194515809              ## float -0.226995006
	.long	3188521175              ## float -0.137667999
	.long	1052281080              ## float 0.360419989
	.long	3194515809              ## float -0.226995006
	.long	3196457604              ## float -0.261860013
	.long	1048973956              ## float 0.261860013
	.long	3194515809              ## float -0.226995006
	.long	3199764728              ## float -0.360419989
	.long	1041037527              ## float 0.137667999
	.long	3194515809              ## float -0.226995006
	.long	3201888019              ## float -0.423698992
	.long	0                       ## float 0
	.long	3194515809              ## float -0.226995006
	.long	3202619640              ## float -0.445502996
	.long	3188521175              ## float -0.137667999
	.long	3194515809              ## float -0.226995006
	.long	3201888019              ## float -0.423698992
	.long	3196457604              ## float -0.261860013
	.long	3194515809              ## float -0.226995006
	.long	3199764728              ## float -0.360419989
	.long	3199764728              ## float -0.360419989
	.long	3194515809              ## float -0.226995006
	.long	3196457604              ## float -0.261860013
	.long	3201888019              ## float -0.423698992
	.long	3194515809              ## float -0.226995006
	.long	3188521175              ## float -0.137667999
	.long	3202619640              ## float -0.445502996
	.long	3194515809              ## float -0.226995006
	.long	0                       ## float 0
	.long	3201888019              ## float -0.423698992
	.long	3194515809              ## float -0.226995006
	.long	1041037527              ## float 0.137667999
	.long	3199764728              ## float -0.360419989
	.long	3194515809              ## float -0.226995006
	.long	1048973956              ## float 0.261860013
	.long	3196457604              ## float -0.261860013
	.long	3194515809              ## float -0.226995006
	.long	1052281080              ## float 0.360419989
	.long	3188521175              ## float -0.137667999
	.long	3194515809              ## float -0.226995006
	.long	1054404371              ## float 0.423698992
	.long	845420765               ## float 1.32769999E-8
	.long	3194515809              ## float -0.226995006
	.long	1055135992              ## float 0.445502996
	.long	1041660163              ## float 0.146945998
	.long	3189651355              ## float -0.154508993
	.long	1055362518              ## float 0.452253997
	.long	1049566158              ## float 0.279509008
	.long	3189651355              ## float -0.154508993
	.long	1053096118              ## float 0.384710014
	.long	1053096151              ## float 0.384710997
	.long	3189651355              ## float -0.154508993
	.long	1049566158              ## float 0.279509008
	.long	1055362518              ## float 0.452253997
	.long	3189651355              ## float -0.154508993
	.long	1041660163              ## float 0.146945998
	.long	1056143464              ## float 0.475528002
	.long	3189651355              ## float -0.154508993
	.long	0                       ## float 0
	.long	1055362518              ## float 0.452253997
	.long	3189651355              ## float -0.154508993
	.long	3189143811              ## float -0.146945998
	.long	1053096151              ## float 0.384710997
	.long	3189651355              ## float -0.154508993
	.long	3197049806              ## float -0.279509008
	.long	1049566158              ## float 0.279509008
	.long	3189651355              ## float -0.154508993
	.long	3200579799              ## float -0.384710997
	.long	1041660163              ## float 0.146945998
	.long	3189651355              ## float -0.154508993
	.long	3202846166              ## float -0.452253997
	.long	0                       ## float 0
	.long	3189651355              ## float -0.154508993
	.long	3203627145              ## float -0.475528985
	.long	3189143811              ## float -0.146945998
	.long	3189651355              ## float -0.154508993
	.long	3202846200              ## float -0.452255011
	.long	3197049806              ## float -0.279509008
	.long	3189651355              ## float -0.154508993
	.long	3200579799              ## float -0.384710997
	.long	3200579799              ## float -0.384710997
	.long	3189651355              ## float -0.154508993
	.long	3197049806              ## float -0.279509008
	.long	3202846200              ## float -0.452255011
	.long	3189651355              ## float -0.154508993
	.long	3189143811              ## float -0.146945998
	.long	3203627112              ## float -0.475528002
	.long	3189651355              ## float -0.154508993
	.long	0                       ## float 0
	.long	3202846166              ## float -0.452253997
	.long	3189651355              ## float -0.154508993
	.long	1041660163              ## float 0.146945998
	.long	3200579766              ## float -0.384710014
	.long	3189651355              ## float -0.154508993
	.long	1049566158              ## float 0.279509008
	.long	3197049806              ## float -0.279509008
	.long	3189651355              ## float -0.154508993
	.long	1053096118              ## float 0.384710014
	.long	3189143811              ## float -0.146945998
	.long	3189651355              ## float -0.154508993
	.long	1055362518              ## float 0.452253997
	.long	846428220               ## float 1.41717997E-8
	.long	3189651355              ## float -0.154508993
	.long	1056143464              ## float 0.475528002
	.long	1042039999              ## float 0.152605996
	.long	3181391959              ## float -0.0782172009
	.long	1055947036              ## float 0.469673991
	.long	1049927371              ## float 0.290273994
	.long	3181391959              ## float -0.0782172009
	.long	1053593327              ## float 0.399527997
	.long	1053593327              ## float 0.399527997
	.long	3181391959              ## float -0.0782172009
	.long	1049927371              ## float 0.290273994
	.long	1055947036              ## float 0.469673991
	.long	3181391959              ## float -0.0782172009
	.long	1042039999              ## float 0.152605996
	.long	1056758047              ## float 0.493844002
	.long	3181391959              ## float -0.0782172009
	.long	0                       ## float 0
	.long	1055947036              ## float 0.469673991
	.long	3181391959              ## float -0.0782172009
	.long	3189523647              ## float -0.152605996
	.long	1053593327              ## float 0.399527997
	.long	3181391959              ## float -0.0782172009
	.long	3197411019              ## float -0.290273994
	.long	1049927371              ## float 0.290273994
	.long	3181391959              ## float -0.0782172009
	.long	3201076975              ## float -0.399527997
	.long	1042039999              ## float 0.152605996
	.long	3181391959              ## float -0.0782172009
	.long	3203430684              ## float -0.469673991
	.long	0                       ## float 0
	.long	3181391959              ## float -0.0782172009
	.long	3204241695              ## float -0.493844002
	.long	3189523647              ## float -0.152605996
	.long	3181391959              ## float -0.0782172009
	.long	3203430684              ## float -0.469673991
	.long	3197411053              ## float -0.290275007
	.long	3181391959              ## float -0.0782172009
	.long	3201077009              ## float -0.39952901
	.long	3201077009              ## float -0.39952901
	.long	3181391959              ## float -0.0782172009
	.long	3197411053              ## float -0.290275007
	.long	3203430684              ## float -0.469673991
	.long	3181391959              ## float -0.0782172009
	.long	3189523647              ## float -0.152605996
	.long	3204241695              ## float -0.493844002
	.long	3181391959              ## float -0.0782172009
	.long	0                       ## float 0
	.long	3203430684              ## float -0.469673991
	.long	3181391959              ## float -0.0782172009
	.long	1042039999              ## float 0.152605996
	.long	3201076975              ## float -0.399527997
	.long	3181391959              ## float -0.0782172009
	.long	1049927371              ## float 0.290273994
	.long	3197411019              ## float -0.290273994
	.long	3181391959              ## float -0.0782172009
	.long	1053593327              ## float 0.399527997
	.long	3189523647              ## float -0.152605996
	.long	3181391959              ## float -0.0782172009
	.long	1055947036              ## float 0.469673991
	.long	847042849               ## float 1.47176999E-8
	.long	3181391959              ## float -0.0782172009
	.long	1056758047              ## float 0.493844002
	.long	1042167707              ## float 0.154508993
	.long	0                       ## float 0
	.long	1056143464              ## float 0.475528002
	.long	1050048805              ## float 0.293893009
	.long	0                       ## float 0
	.long	1053760462              ## float 0.404509008
	.long	1053760462              ## float 0.404509008
	.long	0                       ## float 0
	.long	1050048805              ## float 0.293893009
	.long	1056143464              ## float 0.475528002
	.long	0                       ## float 0
	.long	1042167707              ## float 0.154508993
	.long	1056964608              ## float 0.5
	.long	0                       ## float 0
	.long	0                       ## float 0
	.long	1056143464              ## float 0.475528002
	.long	0                       ## float 0
	.long	3189651355              ## float -0.154508993
	.long	1053760462              ## float 0.404509008
	.long	0                       ## float 0
	.long	3197532453              ## float -0.293893009
	.long	1050048805              ## float 0.293893009
	.long	0                       ## float 0
	.long	3201244110              ## float -0.404509008
	.long	1042167707              ## float 0.154508993
	.long	0                       ## float 0
	.long	3203627145              ## float -0.475528985
	.long	0                       ## float 0
	.long	0                       ## float 0
	.long	3204448256              ## float -0.5
	.long	3189651355              ## float -0.154508993
	.long	0                       ## float 0
	.long	3203627145              ## float -0.475528985
	.long	3197532453              ## float -0.293893009
	.long	0                       ## float 0
	.long	3201244110              ## float -0.404509008
	.long	3201244110              ## float -0.404509008
	.long	0                       ## float 0
	.long	3197532453              ## float -0.293893009
	.long	3203627145              ## float -0.475528985
	.long	0                       ## float 0
	.long	3189651355              ## float -0.154508993
	.long	3204448256              ## float -0.5
	.long	0                       ## float 0
	.long	0                       ## float 0
	.long	3203627112              ## float -0.475528002
	.long	0                       ## float 0
	.long	1042167707              ## float 0.154508993
	.long	3201244110              ## float -0.404509008
	.long	0                       ## float 0
	.long	1050048805              ## float 0.293893009
	.long	3197532453              ## float -0.293893009
	.long	0                       ## float 0
	.long	1053760462              ## float 0.404509008
	.long	3189651355              ## float -0.154508993
	.long	0                       ## float 0
	.long	1056143464              ## float 0.475528002
	.long	847249430               ## float 1.49012003E-8
	.long	0                       ## float 0
	.long	1056964608              ## float 0.5
	.long	1042039999              ## float 0.152605996
	.long	1033908311              ## float 0.0782172009
	.long	1055947036              ## float 0.469673991
	.long	1049927371              ## float 0.290273994
	.long	1033908311              ## float 0.0782172009
	.long	1053593327              ## float 0.399527997
	.long	1053593327              ## float 0.399527997
	.long	1033908311              ## float 0.0782172009
	.long	1049927371              ## float 0.290273994
	.long	1055947036              ## float 0.469673991
	.long	1033908311              ## float 0.0782172009
	.long	1042039999              ## float 0.152605996
	.long	1056758047              ## float 0.493844002
	.long	1033908311              ## float 0.0782172009
	.long	0                       ## float 0
	.long	1055947036              ## float 0.469673991
	.long	1033908311              ## float 0.0782172009
	.long	3189523647              ## float -0.152605996
	.long	1053593327              ## float 0.399527997
	.long	1033908311              ## float 0.0782172009
	.long	3197411019              ## float -0.290273994
	.long	1049927371              ## float 0.290273994
	.long	1033908311              ## float 0.0782172009
	.long	3201076975              ## float -0.399527997
	.long	1042039999              ## float 0.152605996
	.long	1033908311              ## float 0.0782172009
	.long	3203430684              ## float -0.469673991
	.long	0                       ## float 0
	.long	1033908311              ## float 0.0782172009
	.long	3204241695              ## float -0.493844002
	.long	3189523647              ## float -0.152605996
	.long	1033908311              ## float 0.0782172009
	.long	3203430684              ## float -0.469673991
	.long	3197411053              ## float -0.290275007
	.long	1033908311              ## float 0.0782172009
	.long	3201077009              ## float -0.39952901
	.long	3201077009              ## float -0.39952901
	.long	1033908311              ## float 0.0782172009
	.long	3197411053              ## float -0.290275007
	.long	3203430684              ## float -0.469673991
	.long	1033908311              ## float 0.0782172009
	.long	3189523647              ## float -0.152605996
	.long	3204241695              ## float -0.493844002
	.long	1033908311              ## float 0.0782172009
	.long	0                       ## float 0
	.long	3203430684              ## float -0.469673991
	.long	1033908311              ## float 0.0782172009
	.long	1042039999              ## float 0.152605996
	.long	3201076975              ## float -0.399527997
	.long	1033908311              ## float 0.0782172009
	.long	1049927371              ## float 0.290273994
	.long	3197411019              ## float -0.290273994
	.long	1033908311              ## float 0.0782172009
	.long	1053593327              ## float 0.399527997
	.long	3189523647              ## float -0.152605996
	.long	1033908311              ## float 0.0782172009
	.long	1055947036              ## float 0.469673991
	.long	847042849               ## float 1.47176999E-8
	.long	1033908311              ## float 0.0782172009
	.long	1056758047              ## float 0.493844002
	.long	1041660163              ## float 0.146945998
	.long	1042167707              ## float 0.154508993
	.long	1055362518              ## float 0.452253997
	.long	1049566158              ## float 0.279509008
	.long	1042167707              ## float 0.154508993
	.long	1053096118              ## float 0.384710014
	.long	1053096151              ## float 0.384710997
	.long	1042167707              ## float 0.154508993
	.long	1049566158              ## float 0.279509008
	.long	1055362518              ## float 0.452253997
	.long	1042167707              ## float 0.154508993
	.long	1041660163              ## float 0.146945998
	.long	1056143464              ## float 0.475528002
	.long	1042167707              ## float 0.154508993
	.long	0                       ## float 0
	.long	1055362518              ## float 0.452253997
	.long	1042167707              ## float 0.154508993
	.long	3189143811              ## float -0.146945998
	.long	1053096151              ## float 0.384710997
	.long	1042167707              ## float 0.154508993
	.long	3197049806              ## float -0.279509008
	.long	1049566158              ## float 0.279509008
	.long	1042167707              ## float 0.154508993
	.long	3200579799              ## float -0.384710997
	.long	1041660163              ## float 0.146945998
	.long	1042167707              ## float 0.154508993
	.long	3202846166              ## float -0.452253997
	.long	0                       ## float 0
	.long	1042167707              ## float 0.154508993
	.long	3203627145              ## float -0.475528985
	.long	3189143811              ## float -0.146945998
	.long	1042167707              ## float 0.154508993
	.long	3202846200              ## float -0.452255011
	.long	3197049806              ## float -0.279509008
	.long	1042167707              ## float 0.154508993
	.long	3200579799              ## float -0.384710997
	.long	3200579799              ## float -0.384710997
	.long	1042167707              ## float 0.154508993
	.long	3197049806              ## float -0.279509008
	.long	3202846200              ## float -0.452255011
	.long	1042167707              ## float 0.154508993
	.long	3189143811              ## float -0.146945998
	.long	3203627112              ## float -0.475528002
	.long	1042167707              ## float 0.154508993
	.long	0                       ## float 0
	.long	3202846166              ## float -0.452253997
	.long	1042167707              ## float 0.154508993
	.long	1041660163              ## float 0.146945998
	.long	3200579766              ## float -0.384710014
	.long	1042167707              ## float 0.154508993
	.long	1049566158              ## float 0.279509008
	.long	3197049806              ## float -0.279509008
	.long	1042167707              ## float 0.154508993
	.long	1053096118              ## float 0.384710014
	.long	3189143811              ## float -0.146945998
	.long	1042167707              ## float 0.154508993
	.long	1055362518              ## float 0.452253997
	.long	846428220               ## float 1.41717997E-8
	.long	1042167707              ## float 0.154508993
	.long	1056143464              ## float 0.475528002
	.long	1041037527              ## float 0.137667999
	.long	1047032161              ## float 0.226995006
	.long	1054404371              ## float 0.423698992
	.long	1048973956              ## float 0.261860013
	.long	1047032161              ## float 0.226995006
	.long	1052281080              ## float 0.360419989
	.long	1052281080              ## float 0.360419989
	.long	1047032161              ## float 0.226995006
	.long	1048973956              ## float 0.261860013
	.long	1054404371              ## float 0.423698992
	.long	1047032161              ## float 0.226995006
	.long	1041037527              ## float 0.137667999
	.long	1055135992              ## float 0.445502996
	.long	1047032161              ## float 0.226995006
	.long	0                       ## float 0
	.long	1054404371              ## float 0.423698992
	.long	1047032161              ## float 0.226995006
	.long	3188521175              ## float -0.137667999
	.long	1052281080              ## float 0.360419989
	.long	1047032161              ## float 0.226995006
	.long	3196457604              ## float -0.261860013
	.long	1048973956              ## float 0.261860013
	.long	1047032161              ## float 0.226995006
	.long	3199764728              ## float -0.360419989
	.long	1041037527              ## float 0.137667999
	.long	1047032161              ## float 0.226995006
	.long	3201888019              ## float -0.423698992
	.long	0                       ## float 0
	.long	1047032161              ## float 0.226995006
	.long	3202619640              ## float -0.445502996
	.long	3188521175              ## float -0.137667999
	.long	1047032161              ## float 0.226995006
	.long	3201888019              ## float -0.423698992
	.long	3196457604              ## float -0.261860013
	.long	1047032161              ## float 0.226995006
	.long	3199764728              ## float -0.360419989
	.long	3199764728              ## float -0.360419989
	.long	1047032161              ## float 0.226995006
	.long	3196457604              ## float -0.261860013
	.long	3201888019              ## float -0.423698992
	.long	1047032161              ## float 0.226995006
	.long	3188521175              ## float -0.137667999
	.long	3202619640              ## float -0.445502996
	.long	1047032161              ## float 0.226995006
	.long	0                       ## float 0
	.long	3201888019              ## float -0.423698992
	.long	1047032161              ## float 0.226995006
	.long	1041037527              ## float 0.137667999
	.long	3199764728              ## float -0.360419989
	.long	1047032161              ## float 0.226995006
	.long	1048973956              ## float 0.261860013
	.long	3196457604              ## float -0.261860013
	.long	1047032161              ## float 0.226995006
	.long	1052281080              ## float 0.360419989
	.long	3188521175              ## float -0.137667999
	.long	1047032161              ## float 0.226995006
	.long	1054404371              ## float 0.423698992
	.long	845420765               ## float 1.32769999E-8
	.long	1047032161              ## float 0.226995006
	.long	1055135992              ## float 0.445502996
	.long	1040187392              ## float 0.125
	.long	1050048805              ## float 0.293893009
	.long	1053096118              ## float 0.384710014
	.long	1047754856              ## float 0.237764001
	.long	1050048805              ## float 0.293893009
	.long	1051168214              ## float 0.327253997
	.long	1051168214              ## float 0.327253997
	.long	1050048805              ## float 0.293893009
	.long	1047754856              ## float 0.237764001
	.long	1053096151              ## float 0.384710997
	.long	1050048805              ## float 0.293893009
	.long	1040187392              ## float 0.125
	.long	1053760462              ## float 0.404509008
	.long	1050048805              ## float 0.293893009
	.long	0                       ## float 0
	.long	1053096151              ## float 0.384710997
	.long	1050048805              ## float 0.293893009
	.long	3187671040              ## float -0.125
	.long	1051168214              ## float 0.327253997
	.long	1050048805              ## float 0.293893009
	.long	3195238504              ## float -0.237764001
	.long	1047754856              ## float 0.237764001
	.long	1050048805              ## float 0.293893009
	.long	3198651862              ## float -0.327253997
	.long	1040187392              ## float 0.125
	.long	1050048805              ## float 0.293893009
	.long	3200579799              ## float -0.384710997
	.long	0                       ## float 0
	.long	1050048805              ## float 0.293893009
	.long	3201244110              ## float -0.404509008
	.long	3187671040              ## float -0.125
	.long	1050048805              ## float 0.293893009
	.long	3200579799              ## float -0.384710997
	.long	3195238504              ## float -0.237764001
	.long	1050048805              ## float 0.293893009
	.long	3198651862              ## float -0.327253997
	.long	3198651862              ## float -0.327253997
	.long	1050048805              ## float 0.293893009
	.long	3195238504              ## float -0.237764001
	.long	3200579799              ## float -0.384710997
	.long	1050048805              ## float 0.293893009
	.long	3187671040              ## float -0.125
	.long	3201244110              ## float -0.404509008
	.long	1050048805              ## float 0.293893009
	.long	0                       ## float 0
	.long	3200579766              ## float -0.384710014
	.long	1050048805              ## float 0.293893009
	.long	1040187392              ## float 0.125
	.long	3198651862              ## float -0.327253997
	.long	1050048805              ## float 0.293893009
	.long	1047754856              ## float 0.237764001
	.long	3195238504              ## float -0.237764001
	.long	1050048805              ## float 0.293893009
	.long	1051168214              ## float 0.327253997
	.long	3187671040              ## float -0.125
	.long	1050048805              ## float 0.293893009
	.long	1053096118              ## float 0.384710014
	.long	844045253               ## float 1.20552999E-8
	.long	1050048805              ## float 0.293893009
	.long	1053760462              ## float 0.404509008
	.long	1038074000              ## float 0.109254003
	.long	1052050662              ## float 0.353552997
	.long	1051470036              ## float 0.336248994
	.long	1045744945              ## float 0.207813993
	.long	1052050662              ## float 0.353552997
	.long	1049785000              ## float 0.286031008
	.long	1049785000              ## float 0.286031008
	.long	1052050662              ## float 0.353552997
	.long	1045744945              ## float 0.207813993
	.long	1051470036              ## float 0.336248994
	.long	1052050662              ## float 0.353552997
	.long	1038074000              ## float 0.109254003
	.long	1052050662              ## float 0.353552997
	.long	1052050662              ## float 0.353552997
	.long	0                       ## float 0
	.long	1051470036              ## float 0.336248994
	.long	1052050662              ## float 0.353552997
	.long	3185557648              ## float -0.109254003
	.long	1049785000              ## float 0.286031008
	.long	1052050662              ## float 0.353552997
	.long	3193228593              ## float -0.207813993
	.long	1045744945              ## float 0.207813993
	.long	1052050662              ## float 0.353552997
	.long	3197268648              ## float -0.286031008
	.long	1038074000              ## float 0.109254003
	.long	1052050662              ## float 0.353552997
	.long	3198953684              ## float -0.336248994
	.long	0                       ## float 0
	.long	1052050662              ## float 0.353552997
	.long	3199534344              ## float -0.35355401
	.long	3185557648              ## float -0.109254003
	.long	1052050662              ## float 0.353552997
	.long	3198953684              ## float -0.336248994
	.long	3193228593              ## float -0.207813993
	.long	1052050662              ## float 0.353552997
	.long	3197268648              ## float -0.286031008
	.long	3197268648              ## float -0.286031008
	.long	1052050662              ## float 0.353552997
	.long	3193228593              ## float -0.207813993
	.long	3198953684              ## float -0.336248994
	.long	1052050662              ## float 0.353552997
	.long	3185557648              ## float -0.109254003
	.long	3199534310              ## float -0.353552997
	.long	1052050662              ## float 0.353552997
	.long	0                       ## float 0
	.long	3198953684              ## float -0.336248994
	.long	1052050662              ## float 0.353552997
	.long	1038074000              ## float 0.109254003
	.long	3197268648              ## float -0.286031008
	.long	1052050662              ## float 0.353552997
	.long	1045744945              ## float 0.207813993
	.long	3193228526              ## float -0.207812995
	.long	1052050662              ## float 0.353552997
	.long	1049785000              ## float 0.286031008
	.long	3185557648              ## float -0.109254003
	.long	1052050662              ## float 0.353552997
	.long	1051470036              ## float 0.336248994
	.long	842335462               ## float 1.05367004E-8
	.long	1052050662              ## float 0.353552997
	.long	1052050662              ## float 0.353552997
	.long	1035599535              ## float 0.0908178016
	.long	1053760462              ## float 0.404509008
	.long	1049566158              ## float 0.279509008
	.long	1043391572              ## float 0.172746003
	.long	1053760462              ## float 0.404509008
	.long	1047754856              ## float 0.237764001
	.long	1047754856              ## float 0.237764001
	.long	1053760462              ## float 0.404509008
	.long	1043391572              ## float 0.172746003
	.long	1049566158              ## float 0.279509008
	.long	1053760462              ## float 0.404509008
	.long	1035599535              ## float 0.0908178016
	.long	1050048805              ## float 0.293893009
	.long	1053760462              ## float 0.404509008
	.long	0                       ## float 0
	.long	1049566158              ## float 0.279509008
	.long	1053760462              ## float 0.404509008
	.long	3183083183              ## float -0.0908178016
	.long	1047754856              ## float 0.237764001
	.long	1053760462              ## float 0.404509008
	.long	3190875220              ## float -0.172746003
	.long	1043391572              ## float 0.172746003
	.long	1053760462              ## float 0.404509008
	.long	3195238504              ## float -0.237764001
	.long	1035599535              ## float 0.0908178016
	.long	1053760462              ## float 0.404509008
	.long	3197049806              ## float -0.279509008
	.long	0                       ## float 0
	.long	1053760462              ## float 0.404509008
	.long	3197532453              ## float -0.293893009
	.long	3183083183              ## float -0.0908178016
	.long	1053760462              ## float 0.404509008
	.long	3197049806              ## float -0.279509008
	.long	3190875220              ## float -0.172746003
	.long	1053760462              ## float 0.404509008
	.long	3195238504              ## float -0.237764001
	.long	3195238504              ## float -0.237764001
	.long	1053760462              ## float 0.404509008
	.long	3190875220              ## float -0.172746003
	.long	3197049806              ## float -0.279509008
	.long	1053760462              ## float 0.404509008
	.long	3183083183              ## float -0.0908178016
	.long	3197532453              ## float -0.293893009
	.long	1053760462              ## float 0.404509008
	.long	0                       ## float 0
	.long	3197049806              ## float -0.279509008
	.long	1053760462              ## float 0.404509008
	.long	1035599535              ## float 0.0908178016
	.long	3195238504              ## float -0.237764001
	.long	1053760462              ## float 0.404509008
	.long	1043391572              ## float 0.172746003
	.long	3190875220              ## float -0.172746003
	.long	1053760462              ## float 0.404509008
	.long	1047754856              ## float 0.237764001
	.long	3183083183              ## float -0.0908178016
	.long	1053760462              ## float 0.404509008
	.long	1049566158              ## float 0.279509008
	.long	840333600               ## float 8.75868977E-9
	.long	1053760462              ## float 0.404509008
	.long	1050048805              ## float 0.293893009
	.long	1032824932              ## float 0.0701453983
	.long	1055135992              ## float 0.445502996
	.long	1046286581              ## float 0.215884998
	.long	1040752784              ## float 0.133424997
	.long	1055135992              ## float 0.445502996
	.long	1044122857              ## float 0.183642998
	.long	1044122857              ## float 0.183642998
	.long	1055135992              ## float 0.445502996
	.long	1040752784              ## float 0.133424997
	.long	1046286581              ## float 0.215884998
	.long	1055135992              ## float 0.445502996
	.long	1032824932              ## float 0.0701453983
	.long	1047032161              ## float 0.226995006
	.long	1055135992              ## float 0.445502996
	.long	0                       ## float 0
	.long	1046286581              ## float 0.215884998
	.long	1055135992              ## float 0.445502996
	.long	3180308580              ## float -0.0701453983
	.long	1044122857              ## float 0.183642998
	.long	1055135992              ## float 0.445502996
	.long	3188236432              ## float -0.133424997
	.long	1040752784              ## float 0.133424997
	.long	1055135992              ## float 0.445502996
	.long	3191606505              ## float -0.183642998
	.long	1032824946              ## float 0.0701455027
	.long	1055135992              ## float 0.445502996
	.long	3193770229              ## float -0.215884998
	.long	0                       ## float 0
	.long	1055135992              ## float 0.445502996
	.long	3194515809              ## float -0.226995006
	.long	3180308594              ## float -0.0701455027
	.long	1055135992              ## float 0.445502996
	.long	3193770229              ## float -0.215884998
	.long	3188236432              ## float -0.133424997
	.long	1055135992              ## float 0.445502996
	.long	3191606505              ## float -0.183642998
	.long	3191606505              ## float -0.183642998
	.long	1055135992              ## float 0.445502996
	.long	3188236432              ## float -0.133424997
	.long	3193770229              ## float -0.215884998
	.long	1055135992              ## float 0.445502996
	.long	3180308594              ## float -0.0701455027
	.long	3194515809              ## float -0.226995006
	.long	1055135992              ## float 0.445502996
	.long	0                       ## float 0
	.long	3193770229              ## float -0.215884998
	.long	1055135992              ## float 0.445502996
	.long	1032824932              ## float 0.0701453983
	.long	3191606505              ## float -0.183642998
	.long	1055135992              ## float 0.445502996
	.long	1040752784              ## float 0.133424997
	.long	3188236365              ## float -0.133423999
	.long	1055135992              ## float 0.445502996
	.long	1044122857              ## float 0.183642998
	.long	3180308580              ## float -0.0701453983
	.long	1055135992              ## float 0.445502996
	.long	1046286581              ## float 0.215884998
	.long	837316987               ## float 6.7649899E-9
	.long	1055135992              ## float 0.445502996
	.long	1047032161              ## float 0.226995006
	.long	1027838234              ## float 0.0477458015
	.long	1056143464              ## float 0.475528002
	.long	1041660163              ## float 0.146945998
	.long	1035599535              ## float 0.0908178016
	.long	1056143464              ## float 0.475528002
	.long	1040187392              ## float 0.125
	.long	1040187392              ## float 0.125
	.long	1056143464              ## float 0.475528002
	.long	1035599535              ## float 0.0908178016
	.long	1041660163              ## float 0.146945998
	.long	1056143464              ## float 0.475528002
	.long	1027838234              ## float 0.0477458015
	.long	1042167707              ## float 0.154508993
	.long	1056143464              ## float 0.475528002
	.long	0                       ## float 0
	.long	1041660163              ## float 0.146945998
	.long	1056143464              ## float 0.475528002
	.long	3175321882              ## float -0.0477458015
	.long	1040187392              ## float 0.125
	.long	1056143464              ## float 0.475528002
	.long	3183083183              ## float -0.0908178016
	.long	1035599535              ## float 0.0908178016
	.long	1056143464              ## float 0.475528002
	.long	3187671040              ## float -0.125
	.long	1027838234              ## float 0.0477458015
	.long	1056143464              ## float 0.475528002
	.long	3189143811              ## float -0.146945998
	.long	0                       ## float 0
	.long	1056143464              ## float 0.475528002
	.long	3189651355              ## float -0.154508993
	.long	3175321882              ## float -0.0477458015
	.long	1056143464              ## float 0.475528002
	.long	3189143811              ## float -0.146945998
	.long	3183083183              ## float -0.0908178016
	.long	1056143464              ## float 0.475528002
	.long	3187671040              ## float -0.125
	.long	3187671040              ## float -0.125
	.long	1056143464              ## float 0.475528002
	.long	3183083183              ## float -0.0908178016
	.long	3189143811              ## float -0.146945998
	.long	1056143464              ## float 0.475528002
	.long	3175321882              ## float -0.0477458015
	.long	3189651355              ## float -0.154508993
	.long	1056143464              ## float 0.475528002
	.long	0                       ## float 0
	.long	3189143811              ## float -0.146945998
	.long	1056143464              ## float 0.475528002
	.long	1027838234              ## float 0.0477458015
	.long	3187671040              ## float -0.125
	.long	1056143464              ## float 0.475528002
	.long	1035599535              ## float 0.0908178016
	.long	3183083183              ## float -0.0908178016
	.long	1056143464              ## float 0.475528002
	.long	1040187392              ## float 0.125
	.long	3175321855              ## float -0.0477457009
	.long	1056143464              ## float 0.475528002
	.long	1041660163              ## float 0.146945998
	.long	832452469               ## float 4.60470995E-9
	.long	1056143464              ## float 0.475528002
	.long	1042167707              ## float 0.154508993
	.long	1019609398              ## float 0.0241704993
	.long	1056758047              ## float 0.493844002
	.long	1033394499              ## float 0.0743890032
	.long	1027362888              ## float 0.0459749997
	.long	1056758047              ## float 0.493844002
	.long	1031903353              ## float 0.0632790998
	.long	1031903353              ## float 0.0632790998
	.long	1056758047              ## float 0.493844002
	.long	1027362888              ## float 0.0459749997
	.long	1033394499              ## float 0.0743890032
	.long	1056758047              ## float 0.493844002
	.long	1019609398              ## float 0.0241704993
	.long	1033908324              ## float 0.0782172977
	.long	1056758047              ## float 0.493844002
	.long	0                       ## float 0
	.long	1033394499              ## float 0.0743890032
	.long	1056758047              ## float 0.493844002
	.long	3167093046              ## float -0.0241704993
	.long	1031903353              ## float 0.0632790998
	.long	1056758047              ## float 0.493844002
	.long	3174846536              ## float -0.0459749997
	.long	1027362888              ## float 0.0459749997
	.long	1056758047              ## float 0.493844002
	.long	3179387001              ## float -0.0632790998
	.long	1019609398              ## float 0.0241704993
	.long	1056758047              ## float 0.493844002
	.long	3180878147              ## float -0.0743890032
	.long	0                       ## float 0
	.long	1056758047              ## float 0.493844002
	.long	3181391972              ## float -0.0782172977
	.long	3167093046              ## float -0.0241704993
	.long	1056758047              ## float 0.493844002
	.long	3180878147              ## float -0.0743890032
	.long	3174846536              ## float -0.0459749997
	.long	1056758047              ## float 0.493844002
	.long	3179387001              ## float -0.0632790998
	.long	3179387001              ## float -0.0632790998
	.long	1056758047              ## float 0.493844002
	.long	3174846536              ## float -0.0459749997
	.long	3180878147              ## float -0.0743890032
	.long	1056758047              ## float 0.493844002
	.long	3167093046              ## float -0.0241704993
	.long	3181391972              ## float -0.0782172977
	.long	1056758047              ## float 0.493844002
	.long	0                       ## float 0
	.long	3180878147              ## float -0.0743890032
	.long	1056758047              ## float 0.493844002
	.long	1019609398              ## float 0.0241704993
	.long	3179387001              ## float -0.0632790998
	.long	1056758047              ## float 0.493844002
	.long	1027362861              ## float 0.0459748991
	.long	3174846509              ## float -0.0459748991
	.long	1056758047              ## float 0.493844002
	.long	1031903353              ## float 0.0632790998
	.long	3167093046              ## float -0.0241704993
	.long	1056758047              ## float 0.493844002
	.long	1033394499              ## float 0.0743890032
	.long	824193137               ## float 2.33106001E-9
	.long	1056758047              ## float 0.493844002
	.long	1033908324              ## float 0.0782172977
	.long	0                       ## float 0
	.long	1056964608              ## float 0.5
	.long	0                       ## float 0
	.long	0                       ## float 0
	.long	3204448256              ## float -0.5
	.long	0                       ## float 0

	.globl	_normals                ## @normals
	.p2align	4
_normals:
	.long	3190409686              ## float -0.165809005
	.long	3212586565              ## float -0.985081017
	.long	3174874158              ## float -0.0460778996
	.long	3197571879              ## float -0.295067996
	.long	3212015837              ## float -0.951062977
	.long	3183205321              ## float -0.0917278006
	.long	3188909467              ## float -0.143454
	.long	3212586565              ## float -0.985081017
	.long	3183652642              ## float -0.0950606018
	.long	3196136152              ## float -0.252279997
	.long	3212015837              ## float -0.951062977
	.long	3191255928              ## float -0.178418994
	.long	3185262906              ## float -0.107058004
	.long	3212586565              ## float -0.985081017
	.long	3188324546              ## float -0.134737998
	.long	3191684016              ## float -0.184798002
	.long	3212015837              ## float -0.951062977
	.long	3195901674              ## float -0.247646004
	.long	3178660145              ## float -0.0601818003
	.long	3212586565              ## float -0.985081017
	.long	3190102126              ## float -0.161226004
	.long	3184211806              ## float -0.0992266982
	.long	3212015837              ## float -0.951062977
	.long	3197490107              ## float -0.292631
	.long	3153262425              ## float -0.00741474004
	.long	3212586565              ## float -0.985081017
	.long	3190820593              ## float -0.171931997
	.long	3145805245              ## float -0.00394222001
	.long	3212015837              ## float -0.951062977
	.long	3198038386              ## float -0.308970988
	.long	1027390591              ## float 0.0460782014
	.long	3212586565              ## float -0.985081017
	.long	3190409686              ## float -0.165809005
	.long	1035721727              ## float 0.0917282029
	.long	3212015837              ## float -0.951062977
	.long	3197571846              ## float -0.295067012
	.long	1036169007              ## float 0.0950606986
	.long	3212586565              ## float -0.985081017
	.long	3188909467              ## float -0.143454
	.long	1043772348              ## float 0.178420007
	.long	3212015837              ## float -0.951062977
	.long	3196136152              ## float -0.252279997
	.long	1040840898              ## float 0.134737998
	.long	3212586565              ## float -0.985081017
	.long	3185262906              ## float -0.107058004
	.long	1048418026              ## float 0.247646004
	.long	3212015837              ## float -0.951062977
	.long	3191684016              ## float -0.184798002
	.long	1042618478              ## float 0.161226004
	.long	3212586565              ## float -0.985081017
	.long	3178660172              ## float -0.0601819009
	.long	1050006459              ## float 0.292631
	.long	3212015837              ## float -0.951062977
	.long	3184211806              ## float -0.0992266982
	.long	1043336945              ## float 0.171931997
	.long	3212586565              ## float -0.985081017
	.long	3153262468              ## float -0.00741476006
	.long	1050554738              ## float 0.308970988
	.long	3212015837              ## float -0.951062977
	.long	3145805309              ## float -0.00394224981
	.long	1042926038              ## float 0.165809005
	.long	3212586565              ## float -0.985081017
	.long	1027390564              ## float 0.0460781008
	.long	1050088231              ## float 0.295067996
	.long	3212015837              ## float -0.951062977
	.long	1035721713              ## float 0.0917280986
	.long	1041425819              ## float 0.143454
	.long	3212586565              ## float -0.985081017
	.long	1036168994              ## float 0.0950606018
	.long	1048652504              ## float 0.252279997
	.long	3212015837              ## float -0.951062977
	.long	1043772348              ## float 0.178420007
	.long	1037779258              ## float 0.107058004
	.long	3212586565              ## float -0.985081017
	.long	1040840898              ## float 0.134737998
	.long	1044200368              ## float 0.184798002
	.long	3212015837              ## float -0.951062977
	.long	1048418026              ## float 0.247646004
	.long	1031176524              ## float 0.0601819009
	.long	3212586565              ## float -0.985081017
	.long	1042618478              ## float 0.161226004
	.long	1036728158              ## float 0.0992266982
	.long	3212015837              ## float -0.951062977
	.long	1050006459              ## float 0.292631
	.long	1005778777              ## float 0.00741474004
	.long	3212586565              ## float -0.985081017
	.long	1043336945              ## float 0.171931997
	.long	998321597               ## float 0.00394222001
	.long	3212015837              ## float -0.951062977
	.long	1050554738              ## float 0.308970988
	.long	3174874239              ## float -0.0460782014
	.long	3212586565              ## float -0.985081017
	.long	1042926038              ## float 0.165809005
	.long	3183205361              ## float -0.0917280986
	.long	3212015837              ## float -0.951062977
	.long	1050088231              ## float 0.295067996
	.long	3183652642              ## float -0.0950606018
	.long	3212586565              ## float -0.985081017
	.long	1041425819              ## float 0.143454
	.long	3191255996              ## float -0.178420007
	.long	3212015837              ## float -0.951062977
	.long	1048652504              ## float 0.252279997
	.long	3188324546              ## float -0.134737998
	.long	3212586565              ## float -0.985081017
	.long	1037779258              ## float 0.107058004
	.long	3195901674              ## float -0.247646004
	.long	3212015837              ## float -0.951062977
	.long	1044200368              ## float 0.184798002
	.long	3190102126              ## float -0.161226004
	.long	3212586565              ## float -0.985081017
	.long	1031176524              ## float 0.0601819009
	.long	3197490107              ## float -0.292631
	.long	3212015837              ## float -0.951062977
	.long	1036728172              ## float 0.0992268025
	.long	3190820593              ## float -0.171931997
	.long	3212586565              ## float -0.985081017
	.long	1005779078              ## float 0.00741488021
	.long	3198038420              ## float -0.308972001
	.long	3212015837              ## float -0.951062977
	.long	998322306               ## float 0.00394255016
	.long	1041447428              ## float 0.143776
	.long	3211008634              ## float -0.891029
	.long	1054635159              ## float 0.43057701
	.long	1049240176              ## float 0.269793987
	.long	3211008634              ## float -0.891029
	.long	1052437243              ## float 0.365074009
	.long	1052582533              ## float 0.369403988
	.long	3211008634              ## float -0.891029
	.long	1049040226              ## float 0.263835013
	.long	1054711529              ## float 0.432853013
	.long	3211008634              ## float -0.891029
	.long	1040977263              ## float 0.136769995
	.long	1055418822              ## float 0.453931987
	.long	3211008634              ## float -0.891029
	.long	3144770136              ## float -0.00368322991
	.long	1054635159              ## float 0.43057701
	.long	3211008634              ## float -0.891029
	.long	3188931076              ## float -0.143776
	.long	1052437243              ## float 0.365074009
	.long	3211008634              ## float -0.891029
	.long	3196723824              ## float -0.269793987
	.long	1049040226              ## float 0.263835013
	.long	3211008634              ## float -0.891029
	.long	3200066181              ## float -0.369403988
	.long	1040977263              ## float 0.136769995
	.long	3211008634              ## float -0.891029
	.long	3202195177              ## float -0.432853013
	.long	3144769836              ## float -0.00368316006
	.long	3211008634              ## float -0.891029
	.long	3202902470              ## float -0.453931987
	.long	3188931009              ## float -0.143775001
	.long	3211008634              ## float -0.891029
	.long	3202118807              ## float -0.43057701
	.long	3196723824              ## float -0.269793987
	.long	3211008634              ## float -0.891029
	.long	3199920891              ## float -0.365074009
	.long	3200066148              ## float -0.369403005
	.long	3211008634              ## float -0.891029
	.long	3196523874              ## float -0.263835013
	.long	3202195177              ## float -0.432853013
	.long	3211008634              ## float -0.891029
	.long	3188460844              ## float -0.136768997
	.long	3202902470              ## float -0.453931987
	.long	3211008634              ## float -0.891029
	.long	997288292               ## float 0.00368364993
	.long	3202118807              ## float -0.43057701
	.long	3211008634              ## float -0.891029
	.long	1041447428              ## float 0.143776
	.long	3199920891              ## float -0.365074009
	.long	3211008634              ## float -0.891029
	.long	1049240176              ## float 0.269793987
	.long	3196523874              ## float -0.263835013
	.long	3211008634              ## float -0.891029
	.long	1052582533              ## float 0.369403988
	.long	3188460911              ## float -0.136769995
	.long	3211008634              ## float -0.891029
	.long	1054711529              ## float 0.432853013
	.long	997286231               ## float 0.00368317007
	.long	3211008634              ## float -0.891029
	.long	1055418822              ## float 0.453931987
	.long	1044199563              ## float 0.184786007
	.long	3209633338              ## float -0.80905497
	.long	1057936479              ## float 0.557928026
	.long	1051869401              ## float 0.348150998
	.long	3209633338              ## float -0.80905497
	.long	1056076053              ## float 0.473518997
	.long	1056207519              ## float 0.47743699
	.long	3209633338              ## float -0.80905497
	.long	1051688476              ## float 0.342759013
	.long	1057971040              ## float 0.559988022
	.long	3209633338              ## float -0.80905497
	.long	1043774159              ## float 0.178446993
	.long	1058436356              ## float 0.587723017
	.long	3209633338              ## float -0.80905497
	.long	3143264922              ## float -0.00333276996
	.long	1057936479              ## float 0.557928026
	.long	3209633338              ## float -0.80905497
	.long	3191683211              ## float -0.184786007
	.long	1056076053              ## float 0.473518997
	.long	3209633338              ## float -0.80905497
	.long	3199353049              ## float -0.348150998
	.long	1051688476              ## float 0.342759013
	.long	3209633338              ## float -0.80905497
	.long	3203691167              ## float -0.47743699
	.long	1043774159              ## float 0.178446993
	.long	3209633338              ## float -0.80905497
	.long	3205454688              ## float -0.559988022
	.long	3143264579              ## float -0.0033326901
	.long	3209633338              ## float -0.80905497
	.long	3205920004              ## float -0.587723017
	.long	3191683211              ## float -0.184786007
	.long	3209633338              ## float -0.80905497
	.long	3205420127              ## float -0.557928026
	.long	3199353049              ## float -0.348150998
	.long	3209633338              ## float -0.80905497
	.long	3203559701              ## float -0.473518997
	.long	3203691167              ## float -0.47743699
	.long	3209633338              ## float -0.80905497
	.long	3199172124              ## float -0.342759013
	.long	3205454688              ## float -0.559988022
	.long	3209633338              ## float -0.80905497
	.long	3191257740              ## float -0.178445995
	.long	3205920004              ## float -0.587723017
	.long	3209633338              ## float -0.80905497
	.long	995783765               ## float 0.00333334995
	.long	3205420127              ## float -0.557928026
	.long	3209633338              ## float -0.80905497
	.long	1044199563              ## float 0.184786007
	.long	3203559701              ## float -0.473518997
	.long	3209633338              ## float -0.80905497
	.long	1051869401              ## float 0.348150998
	.long	3199172124              ## float -0.342759013
	.long	3209633338              ## float -0.80905497
	.long	1056207519              ## float 0.47743699
	.long	3191257807              ## float -0.178446993
	.long	3209633338              ## float -0.80905497
	.long	1057971040              ## float 0.559988022
	.long	995781188               ## float 0.00333274994
	.long	3209633338              ## float -0.80905497
	.long	1058436356              ## float 0.587723017
	.long	1046646620              ## float 0.221249998
	.long	3207923791              ## float -0.707158029
	.long	1059842706              ## float 0.671548009
	.long	1054211165              ## float 0.417941004
	.long	3207923791              ## float -0.707158029
	.long	1058144214              ## float 0.570309997
	.long	1058201441              ## float 0.573720992
	.long	3207923791              ## float -0.707158029
	.long	1054053627              ## float 0.413246006
	.long	1059872787              ## float 0.673340976
	.long	3207923791              ## float -0.707158029
	.long	1046276246              ## float 0.215730995
	.long	1060438331              ## float 0.707050025
	.long	3207923791              ## float -0.707158029
	.long	3141414221              ## float -0.00290187006
	.long	1059842689              ## float 0.671546995
	.long	3207923791              ## float -0.707158029
	.long	3194130268              ## float -0.221249998
	.long	1058144214              ## float 0.570309997
	.long	3207923791              ## float -0.707158029
	.long	3201694813              ## float -0.417941004
	.long	1054053627              ## float 0.413246006
	.long	3207923791              ## float -0.707158029
	.long	3205685089              ## float -0.573720992
	.long	1046276246              ## float 0.215730995
	.long	3207923791              ## float -0.707158029
	.long	3207356435              ## float -0.673340976
	.long	3141413791              ## float -0.00290176994
	.long	3207923791              ## float -0.707158029
	.long	3207921979              ## float -0.707050025
	.long	3194130268              ## float -0.221249998
	.long	3207923791              ## float -0.707158029
	.long	3207326337              ## float -0.671546995
	.long	3201694813              ## float -0.417941004
	.long	3207923791              ## float -0.707158029
	.long	3205627862              ## float -0.570309997
	.long	3205685089              ## float -0.573720992
	.long	3207923791              ## float -0.707158029
	.long	3201537275              ## float -0.413246006
	.long	3207356435              ## float -0.673340976
	.long	3207923791              ## float -0.707158029
	.long	3193759827              ## float -0.215729997
	.long	3207921979              ## float -0.707050025
	.long	3207923791              ## float -0.707158029
	.long	993933493               ## float 0.00290254992
	.long	3207326354              ## float -0.671548009
	.long	3207923791              ## float -0.707158029
	.long	1046646620              ## float 0.221249998
	.long	3205627862              ## float -0.570309997
	.long	3207923791              ## float -0.707158029
	.long	1054211165              ## float 0.417941004
	.long	3201537275              ## float -0.413246006
	.long	3207923791              ## float -0.707158029
	.long	1058201441              ## float 0.573720992
	.long	3193759894              ## float -0.215730995
	.long	3207923791              ## float -0.707158029
	.long	1059872787              ## float 0.673340976
	.long	993930659               ## float 0.00290189008
	.long	3207923791              ## float -0.707158029
	.long	1060438331              ## float 0.707050025
	.long	1048652236              ## float 0.25227201
	.long	3205922000              ## float -0.587841988
	.long	1061471572              ## float 0.768635988
	.long	1056207821              ## float 0.47744599
	.long	3205922000              ## float -0.587841988
	.long	1059532529              ## float 0.653060019
	.long	1059579924              ## float 0.655884981
	.long	3205922000              ## float -0.587841988
	.long	1056077362              ## float 0.473558009
	.long	1061496486              ## float 0.770120978
	.long	3205922000              ## float -0.587841988
	.long	1048421717              ## float 0.247701004
	.long	1062148298              ## float 0.808972001
	.long	3205922000              ## float -0.587841988
	.long	3139271891              ## float -0.00240306999
	.long	1061471572              ## float 0.768635988
	.long	3205922000              ## float -0.587841988
	.long	3196135850              ## float -0.252270997
	.long	1059532529              ## float 0.653060019
	.long	3205922000              ## float -0.587841988
	.long	3203691469              ## float -0.47744599
	.long	1056077362              ## float 0.473558009
	.long	3205922000              ## float -0.587841988
	.long	3207063572              ## float -0.655884981
	.long	1048421717              ## float 0.247701004
	.long	3205922000              ## float -0.587841988
	.long	3208980134              ## float -0.770120978
	.long	3139271419              ## float -0.00240296009
	.long	3205922000              ## float -0.587841988
	.long	3209631946              ## float -0.808972001
	.long	3196135850              ## float -0.252270997
	.long	3205922000              ## float -0.587841988
	.long	3208955220              ## float -0.768635988
	.long	3203691469              ## float -0.47744599
	.long	3205922000              ## float -0.587841988
	.long	3207016177              ## float -0.653060019
	.long	3207063572              ## float -0.655884981
	.long	3205922000              ## float -0.587841988
	.long	3203561010              ## float -0.473558009
	.long	3208980134              ## float -0.770120978
	.long	3205922000              ## float -0.587841988
	.long	3195905298              ## float -0.247700006
	.long	3209631946              ## float -0.808972001
	.long	3205922000              ## float -0.587841988
	.long	991791636               ## float 0.00240385998
	.long	3208955220              ## float -0.768635988
	.long	3205922000              ## float -0.587841988
	.long	1048652236              ## float 0.25227201
	.long	3207016177              ## float -0.653060019
	.long	3205922000              ## float -0.587841988
	.long	1056207821              ## float 0.47744599
	.long	3203561010              ## float -0.473558009
	.long	3205922000              ## float -0.587841988
	.long	1059579924              ## float 0.655884981
	.long	3195905365              ## float -0.247701004
	.long	3205922000              ## float -0.587841988
	.long	1061496486              ## float 0.770120978
	.long	991788200               ## float 0.00240305997
	.long	3205922000              ## float -0.587841988
	.long	1062148298              ## float 0.808972001
	.long	1049484855              ## float 0.27708599
	.long	3202906229              ## float -0.454044014
	.long	1062782913              ## float 0.846798002
	.long	1057387394              ## float 0.525200009
	.long	3202906229              ## float -0.454044014
	.long	1060651032              ## float 0.719727993
	.long	1060687523              ## float 0.721903026
	.long	3202906229              ## float -0.454044014
	.long	1057337180              ## float 0.522207022
	.long	1062802089              ## float 0.847940981
	.long	3202906229              ## float -0.454044014
	.long	1049366811              ## float 0.273568004
	.long	1063524114              ## float 0.890977025
	.long	3202906229              ## float -0.454044014
	.long	3136451408              ## float -0.00184975006
	.long	1062782913              ## float 0.846798002
	.long	3202906262              ## float -0.454044998
	.long	3196968503              ## float -0.27708599
	.long	1060651032              ## float 0.719727993
	.long	3202906262              ## float -0.454044998
	.long	3204871025              ## float -0.525198996
	.long	1057337180              ## float 0.522207022
	.long	3202906229              ## float -0.454044014
	.long	3208171171              ## float -0.721903026
	.long	1049366811              ## float 0.273568004
	.long	3202906262              ## float -0.454044998
	.long	3210285737              ## float -0.847940981
	.long	3136451150              ## float -0.00184972002
	.long	3202906262              ## float -0.454044998
	.long	3211007762              ## float -0.890977025
	.long	3196968503              ## float -0.27708599
	.long	3202906262              ## float -0.454044998
	.long	3210266561              ## float -0.846798002
	.long	3204871025              ## float -0.525198996
	.long	3202906262              ## float -0.454044998
	.long	3208134680              ## float -0.719727993
	.long	3208171171              ## float -0.721903026
	.long	3202906262              ## float -0.454044998
	.long	3204820828              ## float -0.522207022
	.long	3210285737              ## float -0.847940981
	.long	3202906262              ## float -0.454044998
	.long	3196850425              ## float -0.273566991
	.long	3211007762              ## float -0.890977025
	.long	3202906229              ## float -0.454044014
	.long	988975147               ## float 0.00185061002
	.long	3210266561              ## float -0.846798002
	.long	3202906229              ## float -0.454044014
	.long	1049484855              ## float 0.27708599
	.long	3208134680              ## float -0.719727993
	.long	3202906229              ## float -0.454044014
	.long	1057387394              ## float 0.525200009
	.long	3204820828              ## float -0.522207022
	.long	3202906229              ## float -0.454044014
	.long	1060687523              ## float 0.721903026
	.long	3196850459              ## float -0.273568004
	.long	3202906229              ## float -0.454044014
	.long	1062802089              ## float 0.847940981
	.long	988966901               ## float 0.00184965006
	.long	3202906229              ## float -0.454044014
	.long	1063524114              ## float 0.890977025
	.long	1050088701              ## float 0.295082003
	.long	3198041339              ## float -0.309058994
	.long	1063744398              ## float 0.904106974
	.long	1057971644              ## float 0.560024023
	.long	3198041339              ## float -0.309058994
	.long	1061472159              ## float 0.768670976
	.long	1061496923              ## float 0.770147025
	.long	3198041339              ## float -0.309058994
	.long	1057937569              ## float 0.557992995
	.long	1063757418              ## float 0.904883027
	.long	3198041339              ## float -0.309058994
	.long	1050008573              ## float 0.292694002
	.long	1064531837              ## float 0.951041996
	.long	3198041339              ## float -0.309058994
	.long	3131348299              ## float -0.00125566998
	.long	1063744398              ## float 0.904106974
	.long	3198041339              ## float -0.309058994
	.long	3197572349              ## float -0.295082003
	.long	1061472159              ## float 0.768670976
	.long	3198041339              ## float -0.309058994
	.long	3205455292              ## float -0.560024023
	.long	1057937569              ## float 0.557992995
	.long	3198041339              ## float -0.309058994
	.long	3208980571              ## float -0.770147025
	.long	1050008573              ## float 0.292694002
	.long	3198041339              ## float -0.309058994
	.long	3211241066              ## float -0.904883027
	.long	3131347698              ## float -0.00125560001
	.long	3198041339              ## float -0.309058994
	.long	3212015485              ## float -0.951041996
	.long	3197572349              ## float -0.295082003
	.long	3198041339              ## float -0.309058994
	.long	3211228046              ## float -0.904106974
	.long	3205455292              ## float -0.560024023
	.long	3198041339              ## float -0.309058994
	.long	3208955807              ## float -0.768670976
	.long	3208980571              ## float -0.770147025
	.long	3198041339              ## float -0.309058994
	.long	3205421217              ## float -0.557992995
	.long	3211241066              ## float -0.904883027
	.long	3198041339              ## float -0.309058994
	.long	3197492187              ## float -0.292692989
	.long	3212015485              ## float -0.951041996
	.long	3198041339              ## float -0.309058994
	.long	983872554               ## float 0.00125659001
	.long	3211228046              ## float -0.904106974
	.long	3198041339              ## float -0.309058994
	.long	1050088701              ## float 0.295082003
	.long	3208955807              ## float -0.768670976
	.long	3198041339              ## float -0.309058994
	.long	1057971644              ## float 0.560024023
	.long	3205421217              ## float -0.557992995
	.long	3198041339              ## float -0.309058994
	.long	1061496923              ## float 0.770147025
	.long	3197492221              ## float -0.292694002
	.long	3198041339              ## float -0.309058994
	.long	1063757418              ## float 0.904883027
	.long	983863792               ## float 0.00125556998
	.long	3198041339              ## float -0.309058994
	.long	1064531837              ## float 0.951041996
	.long	1050448841              ## float 0.305815011
	.long	3189782084              ## float -0.156457007
	.long	1064332289              ## float 0.939148008
	.long	1058324569              ## float 0.581059992
	.long	3189782084              ## float -0.156457007
	.long	1061975644              ## float 0.798681021
	.long	1061988143              ## float 0.799426019
	.long	3189782084              ## float -0.156457007
	.long	1058307339              ## float 0.580033004
	.long	1064338866              ## float 0.939540028
	.long	3189782084              ## float -0.156457007
	.long	1050408340              ## float 0.304607987
	.long	1065146588              ## float 0.987684011
	.long	3189782084              ## float -0.156457007
	.long	3123074985              ## float -6.34545984E-4
	.long	1064332289              ## float 0.939148008
	.long	3189782084              ## float -0.156457007
	.long	3197932489              ## float -0.305815011
	.long	1061975644              ## float 0.798681021
	.long	3189782084              ## float -0.156457007
	.long	3205808217              ## float -0.581059992
	.long	1058307339              ## float 0.580033004
	.long	3189782084              ## float -0.156457007
	.long	3209471791              ## float -0.799426019
	.long	1050408340              ## float 0.304607987
	.long	3189782084              ## float -0.156457007
	.long	3211822514              ## float -0.939540028
	.long	3123073439              ## float -6.34455995E-4
	.long	3189782084              ## float -0.156457007
	.long	3212630236              ## float -0.987684011
	.long	3197932489              ## float -0.305815011
	.long	3189782084              ## float -0.156457007
	.long	3211815937              ## float -0.939148008
	.long	3205808217              ## float -0.581059992
	.long	3189782084              ## float -0.156457007
	.long	3209459292              ## float -0.798681021
	.long	3209471791              ## float -0.799426019
	.long	3189782084              ## float -0.156457007
	.long	3205790987              ## float -0.580033004
	.long	3211822514              ## float -0.939540028
	.long	3189782084              ## float -0.156457007
	.long	3197891955              ## float -0.304607004
	.long	3212630236              ## float -0.987684011
	.long	3189782084              ## float -0.156457007
	.long	975607418               ## float 6.35482021E-4
	.long	3211815937              ## float -0.939148008
	.long	3189782084              ## float -0.156457007
	.long	1050448841              ## float 0.305815011
	.long	3209459275              ## float -0.798680007
	.long	3189782084              ## float -0.156457007
	.long	1058324569              ## float 0.581059992
	.long	3205790987              ## float -0.580033004
	.long	3189782084              ## float -0.156457007
	.long	1061988143              ## float 0.799426019
	.long	3197891988              ## float -0.304607987
	.long	3189782084              ## float -0.156457007
	.long	1064338866              ## float 0.939540028
	.long	975590788               ## float 6.34514028E-4
	.long	3189782084              ## float -0.156457007
	.long	1065146588              ## float 0.987684011
	.long	1050556282              ## float 0.309017003
	.long	0                       ## float 0
	.long	1064532072              ## float 0.951056003
	.long	1058437396              ## float 0.587785006
	.long	0                       ## float 0
	.long	1062149053              ## float 0.809017002
	.long	1062149053              ## float 0.809017002
	.long	0                       ## float 0
	.long	1058437396              ## float 0.587785006
	.long	1064532072              ## float 0.951056003
	.long	0                       ## float 0
	.long	1050556282              ## float 0.309017003
	.long	1065353216              ## float 1
	.long	0                       ## float 0
	.long	0                       ## float 0
	.long	1064532089              ## float 0.951057016
	.long	0                       ## float 0
	.long	3198039930              ## float -0.309017003
	.long	1062149053              ## float 0.809017002
	.long	0                       ## float 0
	.long	3205921044              ## float -0.587785006
	.long	1058437396              ## float 0.587785006
	.long	0                       ## float 0
	.long	3209632701              ## float -0.809017002
	.long	1050556282              ## float 0.309017003
	.long	0                       ## float 0
	.long	3212015720              ## float -0.951056003
	.long	0                       ## float 0
	.long	0                       ## float 0
	.long	3212836864              ## float -1
	.long	3198039930              ## float -0.309017003
	.long	0                       ## float 0
	.long	3212015737              ## float -0.951057016
	.long	3205921044              ## float -0.587785006
	.long	0                       ## float 0
	.long	3209632701              ## float -0.809017002
	.long	3209632701              ## float -0.809017002
	.long	0                       ## float 0
	.long	3205921044              ## float -0.587785006
	.long	3212015737              ## float -0.951057016
	.long	0                       ## float 0
	.long	3198039896              ## float -0.309015989
	.long	3212836864              ## float -1
	.long	0                       ## float 0
	.long	0                       ## float 0
	.long	3212015737              ## float -0.951057016
	.long	0                       ## float 0
	.long	1050556282              ## float 0.309017003
	.long	3209632701              ## float -0.809017002
	.long	0                       ## float 0
	.long	1058437396              ## float 0.587785006
	.long	3205921044              ## float -0.587785006
	.long	0                       ## float 0
	.long	1062149053              ## float 0.809017002
	.long	3198039930              ## float -0.309017003
	.long	0                       ## float 0
	.long	1064532072              ## float 0.951056003
	.long	0                       ## float 0
	.long	0                       ## float 0
	.long	1065353216              ## float 1
	.long	1050408340              ## float 0.304607987
	.long	1042298436              ## float 0.156457007
	.long	1064338866              ## float 0.939540028
	.long	1058307339              ## float 0.580033004
	.long	1042298436              ## float 0.156457007
	.long	1061988159              ## float 0.799426972
	.long	1061975627              ## float 0.798680007
	.long	1042298436              ## float 0.156457007
	.long	1058324569              ## float 0.581059992
	.long	1064332289              ## float 0.939148008
	.long	1042298436              ## float 0.156457007
	.long	1050448841              ## float 0.305815011
	.long	1065146588              ## float 0.987684011
	.long	1042298436              ## float 0.156457007
	.long	975591337               ## float 6.34545984E-4
	.long	1064338866              ## float 0.939540028
	.long	1042298436              ## float 0.156457007
	.long	3197891988              ## float -0.304607987
	.long	1061988159              ## float 0.799426972
	.long	1042298436              ## float 0.156457007
	.long	3205790987              ## float -0.580033004
	.long	1058324569              ## float 0.581059992
	.long	1042298436              ## float 0.156457007
	.long	3209459275              ## float -0.798680007
	.long	1050448841              ## float 0.305815011
	.long	1042298436              ## float 0.156457007
	.long	3211815937              ## float -0.939148008
	.long	975593038               ## float 6.34644995E-4
	.long	1042298436              ## float 0.156457007
	.long	3212630236              ## float -0.987684011
	.long	3197891988              ## float -0.304607987
	.long	1042298436              ## float 0.156457007
	.long	3211822514              ## float -0.939540028
	.long	3205790987              ## float -0.580033004
	.long	1042298436              ## float 0.156457007
	.long	3209471807              ## float -0.799426972
	.long	3209459275              ## float -0.798680007
	.long	1042298436              ## float 0.156457007
	.long	3205808217              ## float -0.581059992
	.long	3211815937              ## float -0.939148008
	.long	1042298436              ## float 0.156457007
	.long	3197932455              ## float -0.305813998
	.long	3212630236              ## float -0.987684011
	.long	1042298436              ## float 0.156457007
	.long	3123059317              ## float -6.33633987E-4
	.long	3211822514              ## float -0.939540028
	.long	1042298436              ## float 0.156457007
	.long	1050408340              ## float 0.304607987
	.long	3209471791              ## float -0.799426019
	.long	1042298436              ## float 0.156457007
	.long	1058307339              ## float 0.580033004
	.long	3205808217              ## float -0.581059992
	.long	1042298436              ## float 0.156457007
	.long	1061975627              ## float 0.798680007
	.long	3197932489              ## float -0.305815011
	.long	1042298436              ## float 0.156457007
	.long	1064332289              ## float 0.939148008
	.long	3123075690              ## float -6.3458702E-4
	.long	1042298436              ## float 0.156457007
	.long	1065146588              ## float 0.987684011
	.long	1050008573              ## float 0.292694002
	.long	1050557691              ## float 0.309058994
	.long	1063757418              ## float 0.904883027
	.long	1057937569              ## float 0.557992995
	.long	1050557691              ## float 0.309058994
	.long	1061496923              ## float 0.770147025
	.long	1061472159              ## float 0.768670976
	.long	1050557691              ## float 0.309058994
	.long	1057971644              ## float 0.560024023
	.long	1063744398              ## float 0.904106974
	.long	1050557691              ## float 0.309058994
	.long	1050088701              ## float 0.295082003
	.long	1064531837              ## float 0.951041996
	.long	1050557691              ## float 0.309058994
	.long	983864651               ## float 0.00125566998
	.long	1063757418              ## float 0.904883027
	.long	1050557691              ## float 0.309058994
	.long	3197492221              ## float -0.292694002
	.long	1061496923              ## float 0.770147025
	.long	1050557691              ## float 0.309058994
	.long	3205421200              ## float -0.557991982
	.long	1057971660              ## float 0.560024977
	.long	1050557691              ## float 0.309058994
	.long	3208955807              ## float -0.768670976
	.long	1050088734              ## float 0.295082986
	.long	1050557691              ## float 0.309058994
	.long	3211228046              ## float -0.904106974
	.long	983865424               ## float 0.00125575997
	.long	1050557691              ## float 0.309058994
	.long	3212015485              ## float -0.951041996
	.long	3197492221              ## float -0.292694002
	.long	1050557691              ## float 0.309058994
	.long	3211241066              ## float -0.904883027
	.long	3205421217              ## float -0.557992995
	.long	1050557691              ## float 0.309058994
	.long	3208980571              ## float -0.770147025
	.long	3208955807              ## float -0.768670976
	.long	1050557691              ## float 0.309058994
	.long	3205455292              ## float -0.560024023
	.long	3211228046              ## float -0.904106974
	.long	1050557691              ## float 0.309058994
	.long	3197572315              ## float -0.29508099
	.long	3212015485              ## float -0.951041996
	.long	1050557691              ## float 0.309058994
	.long	3131339967              ## float -0.00125470001
	.long	3211241066              ## float -0.904883027
	.long	1050557691              ## float 0.309058994
	.long	1050008573              ## float 0.292694002
	.long	3208980571              ## float -0.770147025
	.long	1050557691              ## float 0.309058994
	.long	1057937569              ## float 0.557992995
	.long	3205455292              ## float -0.560024023
	.long	1050557691              ## float 0.309058994
	.long	1061472159              ## float 0.768670976
	.long	3197572349              ## float -0.295082003
	.long	1050557691              ## float 0.309058994
	.long	1063744398              ## float 0.904106974
	.long	3131348815              ## float -0.00125573005
	.long	1050557691              ## float 0.309058994
	.long	1064531837              ## float 0.951041996
	.long	1049366811              ## float 0.273568004
	.long	1055422581              ## float 0.454044014
	.long	1062802089              ## float 0.847940981
	.long	1057337180              ## float 0.522207022
	.long	1055422581              ## float 0.454044014
	.long	1060687523              ## float 0.721903026
	.long	1060651032              ## float 0.719727993
	.long	1055422581              ## float 0.454044014
	.long	1057387394              ## float 0.525200009
	.long	1062782913              ## float 0.846798002
	.long	1055422581              ## float 0.454044014
	.long	1049484855              ## float 0.27708599
	.long	1063524114              ## float 0.890977025
	.long	1055422581              ## float 0.454044014
	.long	988967760               ## float 0.00184975006
	.long	1062802089              ## float 0.847940981
	.long	1055422581              ## float 0.454044014
	.long	3196850459              ## float -0.273568004
	.long	1060687523              ## float 0.721903026
	.long	1055422581              ## float 0.454044014
	.long	3204820828              ## float -0.522207022
	.long	1057387394              ## float 0.525200009
	.long	1055422614              ## float 0.454044998
	.long	3208134680              ## float -0.719727993
	.long	1049484855              ## float 0.27708599
	.long	1055422614              ## float 0.454044998
	.long	3210266561              ## float -0.846798002
	.long	988968017               ## float 0.00184977998
	.long	1055422614              ## float 0.454044998
	.long	3211007762              ## float -0.890977025
	.long	3196850459              ## float -0.273568004
	.long	1055422614              ## float 0.454044998
	.long	3210285737              ## float -0.847940981
	.long	3204820811              ## float -0.522206008
	.long	1055422614              ## float 0.454044998
	.long	3208171171              ## float -0.721903026
	.long	3208134680              ## float -0.719727993
	.long	1055422614              ## float 0.454044998
	.long	3204871042              ## float -0.525200009
	.long	3210266561              ## float -0.846798002
	.long	1055422614              ## float 0.454044998
	.long	3196968470              ## float -0.277085006
	.long	3211007762              ## float -0.890977025
	.long	1055422581              ## float 0.454044014
	.long	3136443505              ## float -0.00184883003
	.long	3210285737              ## float -0.847940981
	.long	1055422581              ## float 0.454044014
	.long	1049366811              ## float 0.273568004
	.long	3208171171              ## float -0.721903026
	.long	1055422581              ## float 0.454044014
	.long	1057337180              ## float 0.522207022
	.long	3204871042              ## float -0.525200009
	.long	1055422581              ## float 0.454044014
	.long	1060651032              ## float 0.719727993
	.long	3196968503              ## float -0.27708599
	.long	1055422581              ## float 0.454044014
	.long	1062782913              ## float 0.846798002
	.long	3136451923              ## float -0.00184981001
	.long	1055422581              ## float 0.454044014
	.long	1063524114              ## float 0.890977025
	.long	1048421717              ## float 0.247701004
	.long	1058438352              ## float 0.587841988
	.long	1061496486              ## float 0.770120978
	.long	1056077362              ## float 0.473558009
	.long	1058438352              ## float 0.587841988
	.long	1059579924              ## float 0.655884981
	.long	1059532529              ## float 0.653060019
	.long	1058438352              ## float 0.587841988
	.long	1056207821              ## float 0.47744599
	.long	1061471572              ## float 0.768635988
	.long	1058438352              ## float 0.587841988
	.long	1048652236              ## float 0.25227201
	.long	1062148298              ## float 0.808972001
	.long	1058438352              ## float 0.587841988
	.long	991788243               ## float 0.00240306999
	.long	1061496486              ## float 0.770120978
	.long	1058438352              ## float 0.587841988
	.long	3195905365              ## float -0.247701004
	.long	1059579924              ## float 0.655884981
	.long	1058438352              ## float 0.587841988
	.long	3203561010              ## float -0.473558009
	.long	1056207821              ## float 0.47744599
	.long	1058438352              ## float 0.587841988
	.long	3207016177              ## float -0.653060019
	.long	1048652236              ## float 0.25227201
	.long	1058438352              ## float 0.587841988
	.long	3208955203              ## float -0.768634974
	.long	991788501               ## float 0.00240313006
	.long	1058438352              ## float 0.587841988
	.long	3209631946              ## float -0.808972001
	.long	3195905365              ## float -0.247701004
	.long	1058438352              ## float 0.587841988
	.long	3208980134              ## float -0.770120978
	.long	3203561010              ## float -0.473558009
	.long	1058438352              ## float 0.587841988
	.long	3207063572              ## float -0.655884981
	.long	3207016160              ## float -0.653059006
	.long	1058438352              ## float 0.587841988
	.long	3203691469              ## float -0.47744599
	.long	3208955220              ## float -0.768635988
	.long	1058438352              ## float 0.587841988
	.long	3196135850              ## float -0.252270997
	.long	3209631946              ## float -0.808972001
	.long	1058438352              ## float 0.587841988
	.long	3139268369              ## float -0.00240224996
	.long	3208980134              ## float -0.770120978
	.long	1058438352              ## float 0.587841988
	.long	1048421717              ## float 0.247701004
	.long	3207063572              ## float -0.655884981
	.long	1058438352              ## float 0.587841988
	.long	1056077362              ## float 0.473558009
	.long	3203691469              ## float -0.47744599
	.long	1058438352              ## float 0.587841988
	.long	1059532529              ## float 0.653060019
	.long	3196135884              ## float -0.25227201
	.long	1058438352              ## float 0.587841988
	.long	1061471572              ## float 0.768635988
	.long	3139272020              ## float -0.00240310002
	.long	1058438352              ## float 0.587841988
	.long	1062148298              ## float 0.808972001
	.long	1046276246              ## float 0.215730995
	.long	1060440143              ## float 0.707158029
	.long	1059872787              ## float 0.673340976
	.long	1054053627              ## float 0.413246006
	.long	1060440143              ## float 0.707158029
	.long	1058201441              ## float 0.573720992
	.long	1058144214              ## float 0.570309997
	.long	1060440143              ## float 0.707158029
	.long	1054211165              ## float 0.417941004
	.long	1059842706              ## float 0.671548009
	.long	1060440143              ## float 0.707158029
	.long	1046646620              ## float 0.221249998
	.long	1060438331              ## float 0.707050025
	.long	1060440143              ## float 0.707158029
	.long	993930573               ## float 0.00290187006
	.long	1059872787              ## float 0.673340976
	.long	1060440143              ## float 0.707158029
	.long	3193759894              ## float -0.215730995
	.long	1058201441              ## float 0.573720992
	.long	1060440143              ## float 0.707158029
	.long	3201537275              ## float -0.413246006
	.long	1054211165              ## float 0.417941004
	.long	1060440143              ## float 0.707158029
	.long	3205627845              ## float -0.570308983
	.long	1046646620              ## float 0.221249998
	.long	1060440143              ## float 0.707158029
	.long	3207326337              ## float -0.671546995
	.long	993931002               ## float 0.00290196994
	.long	1060440143              ## float 0.707158029
	.long	3207921979              ## float -0.707050025
	.long	3193759894              ## float -0.215730995
	.long	1060440143              ## float 0.707158029
	.long	3207356435              ## float -0.673340976
	.long	3201537275              ## float -0.413246006
	.long	1060440143              ## float 0.707158029
	.long	3205685089              ## float -0.573720992
	.long	3205627845              ## float -0.570308983
	.long	1060440143              ## float 0.707158029
	.long	3201694813              ## float -0.417941004
	.long	3207326354              ## float -0.671548009
	.long	1060440143              ## float 0.707158029
	.long	3194130201              ## float -0.221248999
	.long	3207921979              ## float -0.707050025
	.long	1060440143              ## float 0.707158029
	.long	3141411128              ## float -0.00290114991
	.long	3207356435              ## float -0.673340976
	.long	1060440143              ## float 0.707158029
	.long	1046276246              ## float 0.215730995
	.long	3205685089              ## float -0.573720992
	.long	1060440143              ## float 0.707158029
	.long	1054053627              ## float 0.413246006
	.long	3201694813              ## float -0.417941004
	.long	1060440143              ## float 0.707158029
	.long	1058144214              ## float 0.570309997
	.long	3194130268              ## float -0.221249998
	.long	1060440143              ## float 0.707158029
	.long	1059842706              ## float 0.671548009
	.long	3141414350              ## float -0.00290190009
	.long	1060440143              ## float 0.707158029
	.long	1060438331              ## float 0.707050025
	.long	1043774159              ## float 0.178446993
	.long	1062149690              ## float 0.80905497
	.long	1057971040              ## float 0.559988022
	.long	1051688476              ## float 0.342759013
	.long	1062149690              ## float 0.80905497
	.long	1056207519              ## float 0.47743699
	.long	1056076053              ## float 0.473518997
	.long	1062149690              ## float 0.80905497
	.long	1051869401              ## float 0.348150998
	.long	1057936479              ## float 0.557928026
	.long	1062149690              ## float 0.80905497
	.long	1044199563              ## float 0.184786007
	.long	1058436356              ## float 0.587723017
	.long	1062149690              ## float 0.80905497
	.long	995781274               ## float 0.00333276996
	.long	1057971040              ## float 0.559988022
	.long	1062149690              ## float 0.80905497
	.long	3191257807              ## float -0.178446993
	.long	1056207519              ## float 0.47743699
	.long	1062149690              ## float 0.80905497
	.long	3199172124              ## float -0.342759013
	.long	1051869401              ## float 0.348150998
	.long	1062149690              ## float 0.80905497
	.long	3203559701              ## float -0.473518997
	.long	1044199563              ## float 0.184786007
	.long	1062149690              ## float 0.80905497
	.long	3205420127              ## float -0.557928026
	.long	995781704               ## float 0.00333287008
	.long	1062149690              ## float 0.80905497
	.long	3205920004              ## float -0.587723017
	.long	3191257807              ## float -0.178446993
	.long	1062149690              ## float 0.80905497
	.long	3205454688              ## float -0.559988022
	.long	3199172124              ## float -0.342759013
	.long	1062149690              ## float 0.80905497
	.long	3203691167              ## float -0.47743699
	.long	3203559701              ## float -0.473518997
	.long	1062149690              ## float 0.80905497
	.long	3199353049              ## float -0.348150998
	.long	3205420127              ## float -0.557928026
	.long	1062149690              ## float 0.80905497
	.long	3191683211              ## float -0.184786007
	.long	3205920004              ## float -0.587723017
	.long	1062149690              ## float 0.80905497
	.long	3143262302              ## float -0.00333215995
	.long	3205454688              ## float -0.559988022
	.long	1062149690              ## float 0.80905497
	.long	1043774159              ## float 0.178446993
	.long	3203691167              ## float -0.47743699
	.long	1062149690              ## float 0.80905497
	.long	1051688476              ## float 0.342759013
	.long	3199353049              ## float -0.348150998
	.long	1062149690              ## float 0.80905497
	.long	1056076053              ## float 0.473518997
	.long	3191683211              ## float -0.184786007
	.long	1062149690              ## float 0.80905497
	.long	1057936479              ## float 0.557928026
	.long	3143264965              ## float -0.00333277998
	.long	1062149690              ## float 0.80905497
	.long	1058436356              ## float 0.587723017
	.long	1040977263              ## float 0.136769995
	.long	1063524986              ## float 0.891029
	.long	1054711529              ## float 0.432853013
	.long	1049040226              ## float 0.263835013
	.long	1063524986              ## float 0.891029
	.long	1052582533              ## float 0.369403988
	.long	1052437243              ## float 0.365074009
	.long	1063524986              ## float 0.891029
	.long	1049240176              ## float 0.269793987
	.long	1054635159              ## float 0.43057701
	.long	1063524986              ## float 0.891029
	.long	1041447428              ## float 0.143776
	.long	1055418822              ## float 0.453931987
	.long	1063524986              ## float 0.891029
	.long	997286488               ## float 0.00368322991
	.long	1054711529              ## float 0.432853013
	.long	1063524986              ## float 0.891029
	.long	3188460911              ## float -0.136769995
	.long	1052582533              ## float 0.369403988
	.long	1063524986              ## float 0.891029
	.long	3196523874              ## float -0.263835013
	.long	1049240176              ## float 0.269793987
	.long	1063524986              ## float 0.891029
	.long	3199920891              ## float -0.365074009
	.long	1041447428              ## float 0.143776
	.long	1063524986              ## float 0.891029
	.long	3202118807              ## float -0.43057701
	.long	997286746               ## float 0.00368328998
	.long	1063524986              ## float 0.891029
	.long	3202902470              ## float -0.453931987
	.long	3188460911              ## float -0.136769995
	.long	1063524986              ## float 0.891029
	.long	3202195177              ## float -0.432853013
	.long	3196523874              ## float -0.263835013
	.long	1063524986              ## float 0.891029
	.long	3200066181              ## float -0.369403988
	.long	3199920891              ## float -0.365074009
	.long	1063524986              ## float 0.891029
	.long	3196723824              ## float -0.269793987
	.long	3202118807              ## float -0.43057701
	.long	1063524986              ## float 0.891029
	.long	3188931009              ## float -0.143775001
	.long	3202902470              ## float -0.453931987
	.long	1063524986              ## float 0.891029
	.long	3144768032              ## float -0.00368274003
	.long	3202195177              ## float -0.432853013
	.long	1063524986              ## float 0.891029
	.long	1040977263              ## float 0.136769995
	.long	3200066181              ## float -0.369403988
	.long	1063524986              ## float 0.891029
	.long	1049040226              ## float 0.263835013
	.long	3196723824              ## float -0.269793987
	.long	1063524986              ## float 0.891029
	.long	1052437243              ## float 0.365074009
	.long	3188931076              ## float -0.143776
	.long	1063524986              ## float 0.891029
	.long	1054635159              ## float 0.43057701
	.long	3144770265              ## float -0.00368325994
	.long	1063524986              ## float 0.891029
	.long	1055418822              ## float 0.453931987
	.long	1035721713              ## float 0.0917280986
	.long	1064532189              ## float 0.951062977
	.long	1050088231              ## float 0.295067996
	.long	1043772280              ## float 0.178418994
	.long	1064532189              ## float 0.951062977
	.long	1048652504              ## float 0.252279997
	.long	1048418026              ## float 0.247646004
	.long	1064532189              ## float 0.951062977
	.long	1044200368              ## float 0.184798002
	.long	1050006459              ## float 0.292631
	.long	1064532189              ## float 0.951062977
	.long	1036728172              ## float 0.0992268025
	.long	1050554738              ## float 0.308970988
	.long	1064532189              ## float 0.951062977
	.long	998321661               ## float 0.00394224981
	.long	1050088231              ## float 0.295067996
	.long	1064532189              ## float 0.951062977
	.long	3183205361              ## float -0.0917280986
	.long	1048652504              ## float 0.252279997
	.long	1064532189              ## float 0.951062977
	.long	3191255928              ## float -0.178418994
	.long	1044200368              ## float 0.184798002
	.long	1064532189              ## float 0.951062977
	.long	3195901674              ## float -0.247646004
	.long	1036728172              ## float 0.0992268025
	.long	1064532189              ## float 0.951062977
	.long	3197490107              ## float -0.292631
	.long	998321790               ## float 0.00394230988
	.long	1064532189              ## float 0.951062977
	.long	3198038386              ## float -0.308970988
	.long	3183205361              ## float -0.0917280986
	.long	1064532189              ## float 0.951062977
	.long	3197571846              ## float -0.295067012
	.long	3191255928              ## float -0.178418994
	.long	1064532189              ## float 0.951062977
	.long	3196136152              ## float -0.252279997
	.long	3195901674              ## float -0.247646004
	.long	1064532189              ## float 0.951062977
	.long	3191684016              ## float -0.184798002
	.long	3197490107              ## float -0.292631
	.long	1064532189              ## float 0.951062977
	.long	3184211766              ## float -0.0992264002
	.long	3198038420              ## float -0.308972001
	.long	1064532189              ## float 0.951062977
	.long	3145804644              ## float -0.00394194014
	.long	3197571879              ## float -0.295067996
	.long	1064532189              ## float 0.951062977
	.long	1035721713              ## float 0.0917280986
	.long	3196136152              ## float -0.252279997
	.long	1064532189              ## float 0.951062977
	.long	1043772348              ## float 0.178420007
	.long	3191684016              ## float -0.184798002
	.long	1064532189              ## float 0.951062977
	.long	1048418026              ## float 0.247646004
	.long	3184211820              ## float -0.0992268025
	.long	1064532189              ## float 0.951062977
	.long	1050006459              ## float 0.292631
	.long	3145805460              ## float -0.00394232012
	.long	1064532189              ## float 0.951062977
	.long	1050554738              ## float 0.308970988
	.long	1027390564              ## float 0.0460781008
	.long	1065102917              ## float 0.985081017
	.long	1042926038              ## float 0.165809005
	.long	1036168994              ## float 0.0950606018
	.long	1065102917              ## float 0.985081017
	.long	1041425819              ## float 0.143454
	.long	1040840898              ## float 0.134737998
	.long	1065102917              ## float 0.985081017
	.long	1037779258              ## float 0.107058004
	.long	1042618478              ## float 0.161226004
	.long	1065102917              ## float 0.985081017
	.long	1031176524              ## float 0.0601819009
	.long	1043336945              ## float 0.171931997
	.long	1065102917              ## float 0.985081017
	.long	1005778820              ## float 0.00741476006
	.long	1042926038              ## float 0.165809005
	.long	1065102917              ## float 0.985081017
	.long	3174874212              ## float -0.0460781008
	.long	1041425819              ## float 0.143454
	.long	1065102917              ## float 0.985081017
	.long	3183652642              ## float -0.0950606018
	.long	1037779258              ## float 0.107058004
	.long	1065102917              ## float 0.985081017
	.long	3188324546              ## float -0.134737998
	.long	1031176524              ## float 0.0601819009
	.long	1065102917              ## float 0.985081017
	.long	3190102126              ## float -0.161226004
	.long	1005778884              ## float 0.00741478987
	.long	1065102917              ## float 0.985081017
	.long	3190820593              ## float -0.171931997
	.long	3174874212              ## float -0.0460781008
	.long	1065102917              ## float 0.985081017
	.long	3190409686              ## float -0.165809005
	.long	3183652642              ## float -0.0950606018
	.long	1065102917              ## float 0.985081017
	.long	3188909467              ## float -0.143454
	.long	3188324546              ## float -0.134737998
	.long	1065102917              ## float 0.985081017
	.long	3185262906              ## float -0.107058004
	.long	3190102126              ## float -0.161226004
	.long	1065102917              ## float 0.985081017
	.long	3178660118              ## float -0.0601816997
	.long	3190820593              ## float -0.171931997
	.long	1065102917              ## float 0.985081017
	.long	3153262017              ## float -0.00741455005
	.long	3190409686              ## float -0.165809005
	.long	1065102917              ## float 0.985081017
	.long	1027390564              ## float 0.0460781008
	.long	3188909467              ## float -0.143454
	.long	1065102917              ## float 0.985081017
	.long	1036169007              ## float 0.0950606986
	.long	3185262906              ## float -0.107058004
	.long	1065102917              ## float 0.985081017
	.long	1040840898              ## float 0.134737998
	.long	3178660172              ## float -0.0601819009
	.long	1065102917              ## float 0.985081017
	.long	1042618478              ## float 0.161226004
	.long	3153262554              ## float -0.00741480011
	.long	1065102917              ## float 0.985081017
	.long	1043336945              ## float 0.171931997
	.long	0                       ## float 0
	.long	1065353216              ## float 1
	.long	0                       ## float 0
	.long	0                       ## float 0
	.long	3212836864              ## float -1
	.long	0                       ## float 0

	.globl	_textures               ## @textures
.zerofill __DATA,__common,_textures,8,2
	.comm	_numVertices,4,2        ## @numVertices
	.comm	_model_vertices,4584,4  ## @model_vertices
	.comm	_model_normals,4584,4   ## @model_normals
	.comm	_model_textures,3056,4  ## @model_textures
	.comm	_model_elements,4560,4  ## @model_elements
	.comm	_numElements,4,2        ## @numElements
	.comm	_maxElements,4,2        ## @maxElements
	.section	__DATA,__objc_imageinfo,regular,no_dead_strip
L_OBJC_IMAGE_INFO:
	.long	0
	.long	64


.subsections_via_symbols
