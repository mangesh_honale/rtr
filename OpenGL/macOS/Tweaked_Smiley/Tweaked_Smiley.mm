// Headers
#import <Foundation/Foundation.h>
#import <Cocoa/Cocoa.h>
#import <Quartzcore/CVDisplayLink.h>
#import <OpenGL/gl3.h>
#import <OpenGL/gl3ext.h>
#import "vmath.h"

enum{
	VDG_ATTRIBUTE_VERTEX = 0,
    VDG_ATTRIBUTE_COLOR,
    VDG_ATTRIBUTE_NORMAL,
    VDG_ATTRIBUTE_TEXTURE0,
};

CVReturn MyDisplayLinkCallback(CVDisplayLinkRef, const CVTimeStamp *, const CVTimeStamp *, CVOptionFlags, CVOptionFlags *, void *);

// Global variables
FILE *gpFile = NULL;

// Interface declarations
@interface AppDelegate : NSObject <NSApplicationDelegate, NSWindowDelegate>
@end

@interface GLView : NSOpenGLView
@end

// Entry point function
int main(int argc, const char * argv[]){
// Code
NSAutoreleasePool *pPool = [[NSAutoreleasePool alloc]init];
NSApp = [NSApplication sharedApplication];
[NSApp setDelegate:[[AppDelegate alloc]init]];

[NSApp run];

[pPool release];

return (0);    
}

// interface implementation
@implementation AppDelegate{
@private
NSWindow *window;
GLView *glView;
}

- (void)applicationDidFinishLaunching:(NSNotification *)aNotification{
// Code
// Log file
NSBundle *mainBundle = [NSBundle mainBundle];
NSString *appDirName = [mainBundle bundlePath];
NSString *parentDirPath = [appDirName stringByDeletingLastPathComponent];
NSString *logFileNameWithPath = [NSString stringWithFormat:@"%@/Log.txt", parentDirPath];
const char *pszLogFileNameWithPath = [logFileNameWithPath cStringUsingEncoding:NSASCIIStringEncoding];
gpFile = fopen(pszLogFileNameWithPath, "w");
if(gpFile == NULL){
	printf("Can not create log file.\n");
	[self release];
	[NSApp terminate:self];
}
fprintf(gpFile, "Program started successfully");

// Window
NSRect win_rect;
win_rect = NSMakeRect(0.0, 0.0, 800.0, 600.0);

// Create simple window
   window=[[NSWindow alloc] initWithContentRect:win_rect
                                       styleMask:NSWindowStyleMaskTitled | NSWindowStyleMaskClosable | NSWindowStyleMaskMiniaturizable | NSWindowStyleMaskResizable
                                         backing:NSBackingStoreBuffered
                                           defer:NO];
[window setTitle:@"macos OpenGL Window"];
[window center];

glView=[[GLView alloc]initWithFrame:win_rect];

[window setContentView:glView];
[window setDelegate:self];
[window makeKeyAndOrderFront:self];
}

- (void)applicationWillTerminate:(NSNotification *)notification{
// code
fprintf(gpFile, "Program terminated successfully.\n");
if(gpFile){
	fclose(gpFile);
	gpFile = NULL;
}
}

- (void)windowWillClose:(NSNotification *)notification{
[NSApp terminate:self];
}

- (void)dealloc{
[glView release];
[window release];
[super dealloc];
}
@end

@implementation GLView{
@private
	CVDisplayLinkRef displayLink;
	GLuint gVertexShaderObject;
	GLuint gFragmentShaderObject;
	GLuint gShaderProgramObject;
	int keyPressed;
	GLuint vao_Square_1;
	GLuint vao_Square_2;
	GLuint vao_Square_3;
	GLuint vao_Square_4;

	GLuint vao_square;
	GLuint vbo_position;
	GLuint vbo_smiley_texture;
	GLuint gMVPUniform;

	GLuint smiley_texture;
	GLuint vbo_bottom_left_texture;
	GLuint vbo_complete_texture;
	GLuint vbo_four_texture;
	GLuint vbo_yellow_texture;
	GLuint texture_sampler_uniform;
	vmath::mat4 perspectiveProjectionMatrix;
}

- (id)initWithFrame:(NSRect)frame;{
self=[super initWithFrame:frame];

if(self){
		[[self window]setContentView:self];

		NSOpenGLPixelFormatAttribute attrs[]={
			NSOpenGLPFAOpenGLProfile,
			NSOpenGLProfileVersion4_1Core,
			NSOpenGLPFAScreenMask, CGDisplayIDToOpenGLDisplayMask(kCGDirectMainDisplay),
			NSOpenGLPFANoRecovery,
			NSOpenGLPFAAccelerated,
			NSOpenGLPFAColorSize, 24,
			NSOpenGLPFADepthSize, 24,
			NSOpenGLPFAAlphaSize, 8,
			NSOpenGLPFADoubleBuffer,
			0};

		NSOpenGLPixelFormat *pixelFormat=[[[NSOpenGLPixelFormat alloc]initWithAttributes:attrs] autorelease];

		if(pixelFormat == nil){
			fprintf(gpFile, "No Valid OpenGL Pixel is available. Exiting...");
			[self release];
			[NSApp terminate:self];
		}

		 NSOpenGLContext *glContext=[[[NSOpenGLContext alloc]initWithFormat:pixelFormat shareContext:nil]autorelease];
		[self setPixelFormat:pixelFormat];
		[self setOpenGLContext:glContext];
	}
	return (self);
}

-(CVReturn)getFrameForTime:(const CVTimeStamp *)pOutputTime{
	NSAutoreleasePool *pool = [[NSAutoreleasePool alloc]init];
	[self drawView];
	[pool release];
	return (kCVReturnSuccess);
}

-(void)prepareOpenGL{
	fprintf(gpFile, "OpenGL version: %s\n", glGetString(GL_VERSION));
	fprintf(gpFile, "GLSL verison: %s\n", glGetString(GL_SHADING_LANGUAGE_VERSION));

	[[self openGLContext]makeCurrentContext];

	GLint swapInt=1;
	[[self openGLContext]setValues:&swapInt forParameter:NSOpenGLCPSwapInterval];

	// Create shader
	gVertexShaderObject = glCreateShader(GL_VERTEX_SHADER);

	// Provide source code to shader
	const GLchar *vertexShaderSourceCode =
		"#version 410" \
		"\n" \
	    "in vec4 vPosition;" \
	    "in vec2 vTexture0_Coord;" \
	    "out vec2 out_texture0_coord;" \
	    "uniform mat4 u_mvp_matrix;" \
	    "void main(void)" \
	    "{" \
	    "gl_Position = u_mvp_matrix * vPosition;" \
	    "out_texture0_coord=vTexture0_Coord;" \
	    "}";

	glShaderSource(gVertexShaderObject, 1, (const char **)&vertexShaderSourceCode, NULL);

	// Compile shader
	glCompileShader(gVertexShaderObject);
	GLint iInfoLogLength = 0;
	GLint iShaderCompiledStatus = 0;
	char *szInfoLog = NULL;
	glGetShaderiv(gVertexShaderObject, GL_COMPILE_STATUS, &iShaderCompiledStatus);

	if (iShaderCompiledStatus == GL_FALSE) {
		glGetShaderiv(gVertexShaderObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
		if (iInfoLogLength > 0) {
			szInfoLog = (char *)malloc(sizeof(iInfoLogLength));
			if (szInfoLog != NULL) {
				GLsizei written;
				glGetShaderInfoLog(gVertexShaderObject, iInfoLogLength, &written, szInfoLog);
				fprintf(gpFile, "Vertex shader compilation log: %s\n", szInfoLog);
				free(szInfoLog);
				[self release];
                [NSApp terminate:self];
			}
		}
	}
	keyPressed = 4;
	// ** fragment shader***
	gFragmentShaderObject = glCreateShader(GL_FRAGMENT_SHADER);

	// Provide source code to shader
	const GLchar *fragmentShaderSoureCode =
		"#version 410" \
	    "\n" \
	    "in vec2 out_texture0_coord;" \
	    "uniform sampler2D u_texture0_sampler;" \
	    "out vec4 FragColor;" \
	    "void main(void)" \
	    "{" \
	    "vec3 tex=vec3(texture(u_texture0_sampler, out_texture0_coord));" \
	    "FragColor=vec4(tex, 1.0f);" \
	    "}";

	glShaderSource(gFragmentShaderObject, 1, (const GLchar **)&fragmentShaderSoureCode, NULL);
	// compile shader
	glCompileShader(gFragmentShaderObject);

	if (iShaderCompiledStatus == GL_FALSE) {
		glGetShaderiv(gFragmentShaderObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
		if (iInfoLogLength > 0) {
			szInfoLog = (char *)malloc(sizeof(iInfoLogLength));
			if (szInfoLog != NULL) {
				GLsizei written;
				glGetShaderInfoLog(gFragmentShaderObject, iInfoLogLength, &written, szInfoLog);
				fprintf(gpFile, "Fragment shader compilation log: %s\n", szInfoLog);
				free(szInfoLog);
				[self release];
                [NSApp terminate:self];
			}
		}
	}


	// *** Shader program **
	gShaderProgramObject = glCreateProgram();

	// Attach vertex shader to shader program
	glAttachShader(gShaderProgramObject, gVertexShaderObject);

	// Attach fragment shader to shader program
	glAttachShader(gShaderProgramObject, gFragmentShaderObject);

	glBindAttribLocation(gShaderProgramObject, VDG_ATTRIBUTE_VERTEX, "vPosition");
    glBindAttribLocation(gShaderProgramObject, VDG_ATTRIBUTE_TEXTURE0, "vTexture0_Coord");

	glLinkProgram(gShaderProgramObject);
	GLint iShaderProgramLinkStatus = 0;
	glGetProgramiv(gShaderProgramObject, GL_LINK_STATUS, &iShaderProgramLinkStatus);
	if (iShaderProgramLinkStatus == GL_FALSE) {
		glGetProgramiv(gVertexShaderObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
		if (iInfoLogLength > 0) {
			szInfoLog = (char *)malloc(sizeof(iInfoLogLength));
			if (szInfoLog != NULL) {
				GLsizei written;
				glGetProgramInfoLog(gShaderProgramObject, iInfoLogLength, &written, szInfoLog);
				fprintf(gpFile, "Shader program link log: %s\n", szInfoLog);
				free(szInfoLog);
				[self release];
                [NSApp terminate:self];
			}
		}
	}

	gMVPUniform = glGetUniformLocation(gShaderProgramObject, "u_mvp_matrix");
	texture_sampler_uniform=glGetUniformLocation(gShaderProgramObject,"u_texture0_sampler");
	smiley_texture=[self loadTextureFromBMPFile:"Smiley.bmp"];
	// *** vertices, colors, shader attribute, vbo_position, vao initialization ***


	// ** square vertices
	const GLfloat squareVertices[] = {
		1.0f, 1.0f, 0.0f, // top-right vertex
		-1.0f, 1.0f, 0.0f, // top-left vertex
		-1.0f, -1.0f, 0.0f, // bottom-left vertex
		1.0f, -1.0f, 0.0f, // bottom-right vertex
	};
	
    const GLfloat squareFullTextcoord[] = {
        1.0f, 0.0f,
        0.0f, 0.0f,
        0.0f, 1.0f,
        1.0f, 1.0f
    };
    
    const GLfloat squareBottomLeftTextcoord[] = {
        0.5f, 0.0f,
        0.0f, 0.0f,
        0.0f, 0.5f,
        0.5f, 0.5f
    };
    
    const GLfloat squareFourTextcoord[] = {
        2.0f, 0.0f,
        0.0f, 0.0f,
        0.0f, 2.0f,
        2.0f, 2.0f
    };
    
    const GLfloat squareYellpwTextcoord[] = {
        0.5f, 0.5f,
        0.5f, 0.5f,
        0.5f, 0.5f,
        0.5f, 0.5f,
    };
	glGenVertexArrays(1, &vao_Square_2);
	glBindVertexArray(vao_Square_2);

	glGenBuffers(1, &vbo_position);
	glBindBuffer(GL_ARRAY_BUFFER, vbo_position);
	glBufferData(GL_ARRAY_BUFFER, sizeof(squareVertices), squareVertices, GL_STATIC_DRAW);
	glVertexAttribPointer(VDG_ATTRIBUTE_VERTEX, 3, GL_FLOAT, GL_FALSE, 0, NULL);
	glEnableVertexAttribArray(VDG_ATTRIBUTE_VERTEX);
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	glGenBuffers(1, &vbo_complete_texture);
	glBindBuffer(GL_ARRAY_BUFFER, vbo_complete_texture);
	glBufferData(GL_ARRAY_BUFFER, sizeof(squareFullTextcoord), squareFullTextcoord, GL_STATIC_DRAW);
	glVertexAttribPointer(VDG_ATTRIBUTE_TEXTURE0, 2, GL_FLOAT, GL_FALSE, 0, NULL);
	glEnableVertexAttribArray(VDG_ATTRIBUTE_TEXTURE0);
	glBindBuffer(GL_ARRAY_BUFFER, 0);
	glBindVertexArray(0);

	// key 1 bottom left texture mapping
	glGenVertexArrays(1, &vao_Square_1);
	glBindVertexArray(vao_Square_1);

	glGenBuffers(1, &vbo_position);
	glBindBuffer(GL_ARRAY_BUFFER, vbo_position);
	glBufferData(GL_ARRAY_BUFFER, sizeof(squareVertices), squareVertices, GL_STATIC_DRAW);
	glVertexAttribPointer(VDG_ATTRIBUTE_VERTEX, 3, GL_FLOAT, GL_FALSE, 0, NULL);
	glEnableVertexAttribArray(VDG_ATTRIBUTE_VERTEX);
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	glGenBuffers(1, &vbo_bottom_left_texture);
	glBindBuffer(GL_ARRAY_BUFFER, vbo_bottom_left_texture);
	glBufferData(GL_ARRAY_BUFFER, sizeof(squareBottomLeftTextcoord), squareBottomLeftTextcoord, GL_STATIC_DRAW);
	glVertexAttribPointer(VDG_ATTRIBUTE_TEXTURE0, 2, GL_FLOAT, GL_FALSE, 0, NULL);
	glEnableVertexAttribArray(VDG_ATTRIBUTE_TEXTURE0);
	glBindBuffer(GL_ARRAY_BUFFER, 0);
	glBindVertexArray(0);

	// key 3 four smileys texture mapping
	glGenVertexArrays(1, &vao_Square_3);
	glBindVertexArray(vao_Square_3);

	glGenBuffers(1, &vbo_position);
	glBindBuffer(GL_ARRAY_BUFFER, vbo_position);
	glBufferData(GL_ARRAY_BUFFER, sizeof(squareVertices), squareVertices, GL_STATIC_DRAW);
	glVertexAttribPointer(VDG_ATTRIBUTE_VERTEX, 3, GL_FLOAT, GL_FALSE, 0, NULL);
	glEnableVertexAttribArray(VDG_ATTRIBUTE_VERTEX);
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	glGenBuffers(1, &vbo_four_texture);
	glBindBuffer(GL_ARRAY_BUFFER, vbo_four_texture);
	glBufferData(GL_ARRAY_BUFFER, sizeof(squareFourTextcoord), squareFourTextcoord, GL_STATIC_DRAW);
	glVertexAttribPointer(VDG_ATTRIBUTE_TEXTURE0, 2, GL_FLOAT, GL_FALSE, 0, NULL);
	glEnableVertexAttribArray(VDG_ATTRIBUTE_TEXTURE0);
	glBindBuffer(GL_ARRAY_BUFFER, 0);
	glBindVertexArray(0);


	// key 4 four smileys texture mapping
	glGenVertexArrays(1, &vao_Square_4);
	glBindVertexArray(vao_Square_4);

	glGenBuffers(1, &vbo_position);
	glBindBuffer(GL_ARRAY_BUFFER, vbo_position);
	glBufferData(GL_ARRAY_BUFFER, sizeof(squareVertices), squareVertices, GL_STATIC_DRAW);
	glVertexAttribPointer(VDG_ATTRIBUTE_VERTEX, 3, GL_FLOAT, GL_FALSE, 0, NULL);
	glEnableVertexAttribArray(VDG_ATTRIBUTE_VERTEX);
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	glGenBuffers(1, &vbo_yellow_texture);
	glBindBuffer(GL_ARRAY_BUFFER, vbo_yellow_texture);
	glBufferData(GL_ARRAY_BUFFER, sizeof(squareYellpwTextcoord), squareYellpwTextcoord, GL_STATIC_DRAW);
	glVertexAttribPointer(VDG_ATTRIBUTE_TEXTURE0, 2, GL_FLOAT, GL_FALSE, 0, NULL);
	glEnableVertexAttribArray(VDG_ATTRIBUTE_TEXTURE0);
	glBindBuffer(GL_ARRAY_BUFFER, 0);
	glBindVertexArray(0);


	glClearDepth(1.0f);
	glEnable(GL_DEPTH_TEST);
	glDepthFunc(GL_LEQUAL);
	glEnable(GL_CULL_FACE);

	glClearColor(0.0f, 0.0f, 0.0f, 0.0f);

	perspectiveProjectionMatrix = vmath::mat4::identity();
	CVDisplayLinkCreateWithActiveCGDisplays(&displayLink);
    CVDisplayLinkSetOutputCallback(displayLink,&MyDisplayLinkCallback,self);
    CGLContextObj cglContext=(CGLContextObj)[[self openGLContext]CGLContextObj];
    CGLPixelFormatObj cglPixelFormat=(CGLPixelFormatObj)[[self pixelFormat]CGLPixelFormatObj];
    CVDisplayLinkSetCurrentCGDisplayFromOpenGLContext(displayLink,cglContext,cglPixelFormat);
    CVDisplayLinkStart(displayLink);
}

-(GLuint)loadTextureFromBMPFile:(const char *)texFileName
{
    NSBundle *mainBundle=[NSBundle mainBundle];
    NSString *appDirName=[mainBundle bundlePath];
    NSString *parentDirPath=[appDirName stringByDeletingLastPathComponent];
    NSString *textureFileNameWithPath=[NSString stringWithFormat:@"%@/%s",parentDirPath,texFileName];
    
    NSImage *bmpImage=[[NSImage alloc]initWithContentsOfFile:textureFileNameWithPath];
    if (!bmpImage)
    {
        NSLog(@"can't find %@", textureFileNameWithPath);
        return(0);
    }
    
    CGImageRef cgImage = [bmpImage CGImageForProposedRect:nil context:nil hints:nil];
    
    int w = (int)CGImageGetWidth(cgImage);
    int h = (int)CGImageGetHeight(cgImage);
    CFDataRef imageData = CGDataProviderCopyData(CGImageGetDataProvider(cgImage));
    void* pixels = (void *)CFDataGetBytePtr(imageData);

    GLuint bmpTexture;
    glGenTextures(1, &bmpTexture);
    
    glPixelStorei(GL_UNPACK_ALIGNMENT, 1); // set 1 rather than default 4, for better performance
    glBindTexture(GL_TEXTURE_2D, bmpTexture);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
    
    glTexImage2D(GL_TEXTURE_2D,
                 0,
                 GL_RGBA,
                 w,
                 h,
                 0,
                 GL_RGBA,
                 GL_UNSIGNED_BYTE,
                 pixels);
    
    // Create mipmaps for this texture for better image quality
    glGenerateMipmap(GL_TEXTURE_2D);
    
    CFRelease(imageData);
    return(bmpTexture);
}

-(void)reshape{
	CGLLockContext((CGLContextObj) [[self openGLContext]CGLContextObj]);

	NSRect rect = [self bounds];
	GLfloat width = rect.size.width;
	GLfloat height = rect.size.height;

	if(height == 0)
		height = 1;
	glViewport(0,0,(GLsizei)width,(GLsizei)height);
	
    perspectiveProjectionMatrix = vmath::perspective(45.0f, (GLfloat)width/(GLfloat)height, 0.1f, 100.0f);
	CGLUnlockContext((CGLContextObj) [[self openGLContext] CGLContextObj]);
}

-(void)drawRect:(NSRect)dirtyRect{
	[self drawView];
}

-(void)drawView{
	[[self openGLContext]makeCurrentContext];

	CGLLockContext((CGLContextObj) [[self openGLContext]CGLContextObj]);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	// start using OpenGL program object
    glUseProgram(gShaderProgramObject);
    
    // OpenGL Drawing
    // set modelview & modelviewprojection matrices to identity
    vmath::mat4 modelViewMatrix = vmath::mat4::identity();
    vmath::mat4 modelViewProjectionMatrix = vmath::mat4::identity();
    
    // multiply the modelview and orthographic matrix to get modelviewprojection matrix
    modelViewMatrix = vmath::translate(0.0f, 0.0f, -4.0f);
    modelViewProjectionMatrix = perspectiveProjectionMatrix * modelViewMatrix; // ORDER IS IMPORTANT
    
    // pass above modelviewprojection matrix to the vertex shader in 'u_mvp_matrix' shader variable
    // whose position value we already calculated in initWithFrame() by using glGetUniformLocation()
    glUniformMatrix4fv(gMVPUniform, 1, GL_FALSE, modelViewProjectionMatrix);
    
    // *** bind vao ***
	glBindTexture(GL_TEXTURE_2D, smiley_texture);

	// Bind vao of square
	if (keyPressed == 1)
		glBindVertexArray(vao_Square_1);
	if (keyPressed == 2)
		glBindVertexArray(vao_Square_2);
	if (keyPressed == 3)
		glBindVertexArray(vao_Square_3);
	if (keyPressed == 4)
		glBindVertexArray(vao_Square_4);

	// Draw either by glDrawTriangles or glDrawArrays or glDrawElements()
	glDrawArrays(GL_TRIANGLE_FAN, 0, 4);

	// Unbind vao
	glBindVertexArray(0);
    
    // stop using OpenGL program object
    glUseProgram(0);
	CGLFlushDrawable((CGLContextObj) [[self openGLContext]CGLContextObj]);
	CGLUnlockContext((CGLContextObj) [[self openGLContext]CGLContextObj]);
}

-(BOOL)acceptsFirstResponder{
	[[self window]makeFirstResponder:self];
	return(YES);
}

-(void)keyDown:(NSEvent *)theEvent{
	int key = (int)[[theEvent characters]characterAtIndex:0];
	fprintf(gpFile, "Key: %i\n", key);
	switch(key){
		case 27:
		[self release];
		[NSApp terminate:self];
		break;
		case 49:
			keyPressed = 1;
			break;
		case 50:
			keyPressed = 2;
			break;
		case 51:
			keyPressed = 3;
			break;
		case 52:
			keyPressed = 4;
			break;
		case 'F':
		case 'f':
		[[self window]toggleFullScreen:self];
		break;
		default:
		break;
	}
}


-(void)mouseDown:(NSEvent *)theEvent{

}

-(void)mouseDragged:(NSEvent *)theEvent{

}

-(void)rightMouseDown:(NSEvent *)theEvent{

}

- (void) dealloc
{	
	

    if (vao_Square_1)
    {
        glDeleteVertexArrays(1, &vao_Square_1);
        vao_Square_1 = 0;
    }

    if (vao_Square_2)
    {
        glDeleteVertexArrays(1, &vao_Square_2);
        vao_Square_2 = 0;
    }

    if (vao_Square_3)
    {
        glDeleteVertexArrays(1, &vao_Square_3);
        vao_Square_3 = 0;
    }

    if (vao_Square_4)
    {
        glDeleteVertexArrays(1, &vao_Square_4);
        vao_Square_4 = 0;
    }
    
    // destroy vbo_position
    if (vbo_position)
    {
        glDeleteBuffers(1, &vbo_position);
        vbo_position = 0;
    }

    if (vbo_smiley_texture)
    {
        glDeleteBuffers(1, &vbo_smiley_texture);
        vbo_smiley_texture = 0;
    }
    
    // detach vertex shader from shader program object
    glDetachShader(gShaderProgramObject, gVertexShaderObject);
    // detach fragment  shader from shader program object
    glDetachShader(gShaderProgramObject, gFragmentShaderObject);
    
    // delete vertex shader object
    glDeleteShader(gVertexShaderObject);
    gVertexShaderObject = 0;
    // delete fragment shader object
    glDeleteShader(gFragmentShaderObject);
    gFragmentShaderObject = 0;
    
    // delete shader program object
    glDeleteProgram(gShaderProgramObject);
    gShaderProgramObject = 0;
    CVDisplayLinkStop(displayLink);
    CVDisplayLinkRelease(displayLink);
    [super dealloc];
}

@end

CVReturn MyDisplayLinkCallback(CVDisplayLinkRef displayLink,const CVTimeStamp *pNow,const CVTimeStamp *pOutputTime,CVOptionFlags flagsIn,
                               CVOptionFlags *pFlagsOut,void *pDisplayLinkContext){
    CVReturn result=[(GLView *)pDisplayLinkContext getFrameForTime:pOutputTime];
    return(result);
}
