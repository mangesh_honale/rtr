//
//  GLESView.m
//  OGLPPWindow
//
//  Created by user140883 on 7/18/18.
//

#import <OpenGLES/ES3/gl.h>
#import <OpenGLES/ES3/glext.h>

#import "vmath.h"
#import "Sphere.h"
#import "GLESView.h"
#define checkImageHeight 64
#define checkImageWidth 64
enum
{
    VDG_ATTRIBUTE_VERTEX = 0,
    VDG_ATTRIBUTE_COLOR,
    VDG_ATTRIBUTE_NORMAL,
    VDG_ATTRIBUTE_TEXTURE0,
};

GLfloat red_lightAmbient[] = { 0.0f, 0.0f, 0.0f, 0.0f };
GLfloat red_lightDiffuse[] = {1.0f, 0.0f, 0.0f, 0.0f };
GLfloat red_lightSpecular[] = { 1.0f, 0.0f, 0.0f, 0.0f };
GLfloat red_lightPosition[] = { 0.0f, 0.0f, 0.0f, 0.0f };

GLfloat green_lightAmbient[] = { 0.0f, 0.0f, 0.0f, 0.0f };
GLfloat green_lightDiffuse[] = { 0.0f, 1.0f, 0.0f, 0.0f };
GLfloat green_lightSpecular[] = { 0.0f, 1.0f, 0.0f, 0.0f };
GLfloat green_lightPosition[] = { 0.0f, 0.0f, 0.0f, 0.0f };

GLfloat blue_lightAmbient[] = { 0.0f, 0.0f, 0.0f, 0.0f };
GLfloat blue_lightDiffuse[] = { 0.0f, 0.0f, 1.0f, 0.0f };
GLfloat blue_lightSpecular[] = { 0.0f, 0.0f, 1.0f, 0.0f };
GLfloat blue_lightPosition[] = { 0.0f, 0.0f, 0.0f, 0.0f };

GLfloat material_ambient[] = { 0.0f,0.0f,0.0f,1.0f };
GLfloat material_diffuse[] = { 1.0f,1.0f,1.0f,1.0f };
GLfloat material_specular[] = { 1.0f,1.0f,1.0f,1.0f };
GLfloat material_shininess = 50.0f;
GLfloat gfRedLightAngle = 0.0f, gfGreenLightAngle = 0.0f, gfBlueLightAngle = 0.0f;

@implementation GLESView
{
    EAGLContext *eaglContext;
    
    GLuint defaultFramebuffer;
    GLuint colorRenderbuffer;
    GLuint depthRenderbuffer;
    
    id displayLink;
    NSInteger animationFrameInterval;
    BOOL isAnimating;
    BOOL bLight;
    
    GLuint vertexShaderObject;
    GLuint fragmentShaderObject;
    GLuint shaderProgramObject;
    
    GLuint mvpUniform;
    GLuint numElements;
    GLuint numVertices;
    float sphere_vertices[1146];
    float sphere_normals[1146];
    float sphere_textures[764];
    unsigned short sphere_elements[2280];
    GLuint vao_sphere;
    GLuint vbo_sphere_position;
    GLuint vbo_sphere_normal;
    GLuint vbo_sphere_element;
    GLuint model_matrix_uniform, view_matrix_uniform, projection_matrix_uniform;
    GLuint doubleTapUniform;
    GLuint red_La_uniform;
    GLuint red_Ld_uniform;
    GLuint red_Ls_uniform;
    GLuint red_light_position_uniform;
    
    GLuint green_La_uniform;
    GLuint green_Ld_uniform;
    GLuint green_Ls_uniform;
    GLuint green_light_position_uniform;
    
    GLuint blue_La_uniform;
    GLuint blue_Ld_uniform;
    GLuint blue_Ls_uniform;
    GLuint blue_light_position_uniform;
    
    GLuint Ka_uniform;
    GLuint Kd_uniform;
    GLuint Ks_uniform;
    GLuint material_shininess_uniform;
    
    GLfloat fPyramidAngle;
    vmath::mat4 perspectiveProjectionMatrix;
}

-(id)initWithFrame:(CGRect)frame;
{
    // code
    self=[super initWithFrame:frame];
    
    if(self)
    {
        CAEAGLLayer *eaglLayer=(CAEAGLLayer *)super.layer;
        
        eaglLayer.opaque=YES;
        eaglLayer.drawableProperties=[NSDictionary dictionaryWithObjectsAndKeys:[NSNumber numberWithBool:FALSE],
                                      kEAGLDrawablePropertyRetainedBacking,kEAGLColorFormatRGBA8,kEAGLDrawablePropertyColorFormat,nil];
        
        eaglContext=[[EAGLContext alloc]initWithAPI:kEAGLRenderingAPIOpenGLES3];
        if(eaglContext==nil)
        {
            [self release];
            return(nil);
        }
        [EAGLContext setCurrentContext:eaglContext];
        
        glGenFramebuffers(1,&defaultFramebuffer);
        glGenRenderbuffers(1,&colorRenderbuffer);
        glBindFramebuffer(GL_FRAMEBUFFER,defaultFramebuffer);
        glBindRenderbuffer(GL_RENDERBUFFER,colorRenderbuffer);
        
        [eaglContext renderbufferStorage:GL_RENDERBUFFER fromDrawable:eaglLayer];
        
        glFramebufferRenderbuffer(GL_FRAMEBUFFER,GL_COLOR_ATTACHMENT0,GL_RENDERBUFFER,colorRenderbuffer);
        
        GLint backingWidth;
        GLint backingHeight;
        
        glGetRenderbufferParameteriv(GL_RENDERBUFFER,GL_RENDERBUFFER_WIDTH,&backingWidth);
        glGetRenderbufferParameteriv(GL_RENDERBUFFER,GL_RENDERBUFFER_HEIGHT,&backingHeight);
        
        glGenRenderbuffers(1,&depthRenderbuffer);
        glBindRenderbuffer(GL_RENDERBUFFER,depthRenderbuffer);
        glRenderbufferStorage(GL_RENDERBUFFER,GL_DEPTH_COMPONENT16,backingWidth,backingHeight);
        glFramebufferRenderbuffer(GL_FRAMEBUFFER,GL_DEPTH_ATTACHMENT,GL_RENDERBUFFER,depthRenderbuffer);
        
        if(glCheckFramebufferStatus(GL_FRAMEBUFFER)!=GL_FRAMEBUFFER_COMPLETE)
        {
            printf("Failed To Create Complete Framebuffer Object %x\n",glCheckFramebufferStatus(GL_FRAMEBUFFER));
            glDeleteFramebuffers(1,&defaultFramebuffer);
            glDeleteRenderbuffers(1,&colorRenderbuffer);
            glDeleteRenderbuffers(1,&depthRenderbuffer);
            
            return(nil);
        }
        
        printf("Renderer : %s | GL Version : %s | GLSL Version : %s\n",glGetString(GL_RENDERER),glGetString(GL_VERSION),glGetString(GL_SHADING_LANGUAGE_VERSION));
        
        // hard coded initializations
        isAnimating=NO;
        animationFrameInterval=60; // default since iOS 8.2
        
        
        // *** VERTEX SHADER ***
        // create shader
        vertexShaderObject = glCreateShader(GL_VERTEX_SHADER);
        
        // provide source code to shader
        const GLchar *vertexShaderSourceCode =
        "#version 300 es" \
        "\n" \
        "in vec4 vPosition;"\
        "in vec3 vNormal;"\
        "uniform mat4 u_model_matrix;"\
        "uniform mat4 u_view_matrix;"\
        "uniform mat4 u_projection_matrix;"\
        "uniform int u_double_tap;"\
        "uniform vec3 u_La_red;"\
        "uniform vec3 u_Ld_red;"\
        "uniform vec3 u_Ls_red;"\
        "uniform vec4 u_light_position_red;"\
        
        "uniform vec3 u_La_green;"\
        "uniform vec3 u_Ld_green;"\
        "uniform vec3 u_Ls_green;"\
        "uniform vec4 u_light_position_green;"\
        
        "uniform vec3 u_La_blue;"\
        "uniform vec3 u_Ld_blue;"\
        "uniform vec3 u_Ls_blue;"\
        "uniform vec4 u_light_position_blue;"\
        
        "uniform vec3 u_Ka;"\
        "uniform vec3 u_Kd;"\
        "uniform vec3 u_Ks;"\
        "uniform float u_material_shininess;"\
        "out vec3 phong_ads_color;"\
        "void main(void)"\
        "{"\
        "if (u_double_tap == 1)"\
        "{"\
        "vec3 red_light;" \
        "vec3 blue_light;" \
        "vec3 green_light;" \
        "vec4 eye_coordinates=u_view_matrix * u_model_matrix * vPosition;"\
        "vec3 transformed_normals=normalize(mat3(u_view_matrix * u_model_matrix) * vNormal);"\
        "vec3 light_direction = normalize(vec3(u_light_position_red) - eye_coordinates.xyz);"\
        "float tn_dot_ld = max(dot(transformed_normals, light_direction),0.0);"\
        "vec3 ambient = u_La_red * u_Ka;"\
        "vec3 diffuse = u_Ld_red * u_Kd * tn_dot_ld;"\
        "vec3 reflection_vector = reflect(-light_direction, transformed_normals);"\
        "vec3 viewer_vector = normalize(-eye_coordinates.xyz);"\
        "vec3 specular = u_Ls_red * u_Ks * pow(max(dot(reflection_vector, viewer_vector), 0.0), u_material_shininess);"\
        "red_light=ambient + diffuse + specular;"\
        
        "light_direction = normalize(vec3(u_light_position_green) - eye_coordinates.xyz);"\
        "tn_dot_ld = max(dot(transformed_normals, light_direction),0.0);"\
        "ambient = u_La_green * u_Ka;"\
        "diffuse = u_Ld_green * u_Kd * tn_dot_ld;"\
        "reflection_vector = reflect(-light_direction, transformed_normals);"\
        "viewer_vector = normalize(-eye_coordinates.xyz);"\
        "specular = u_Ls_green * u_Ks * pow(max(dot(reflection_vector, viewer_vector), 0.0), u_material_shininess);"\
        "green_light=ambient + diffuse + specular;"\
        
        "light_direction = normalize(vec3(u_light_position_blue) - eye_coordinates.xyz);"\
        "tn_dot_ld = max(dot(transformed_normals, light_direction),0.0);"\
        "ambient = u_La_blue * u_Ka;"\
        "diffuse = u_Ld_blue * u_Kd * tn_dot_ld;"\
        "reflection_vector = reflect(-light_direction, transformed_normals);"\
        "viewer_vector = normalize(-eye_coordinates.xyz);"\
        "specular = u_Ls_blue * u_Ks * pow(max(dot(reflection_vector, viewer_vector), 0.0), u_material_shininess);"\
        "blue_light=ambient + diffuse + specular;"\
        
        "phong_ads_color=red_light+green_light + blue_light;"\
        "}"\
        "else"\
        "{"\
        "phong_ads_color = vec3(1.0, 1.0, 1.0);"\
        "}"\
        "gl_Position=u_projection_matrix * u_view_matrix * u_model_matrix * vPosition;"\
        "}";
        
        glShaderSource(vertexShaderObject, 1, (const GLchar **)&vertexShaderSourceCode, NULL);
        
        // compile shader
        glCompileShader(vertexShaderObject);
        GLint iInfoLogLength = 0;
        GLint iShaderCompiledStatus = 0;
        char *szInfoLog = NULL;
        glGetShaderiv(vertexShaderObject, GL_COMPILE_STATUS, &iShaderCompiledStatus);
        if (iShaderCompiledStatus == GL_FALSE)
        {
            glGetShaderiv(vertexShaderObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
            if (iInfoLogLength > 0)
            {
                szInfoLog = (char *)malloc(iInfoLogLength);
                if (szInfoLog != NULL)
                {
                    GLsizei written;
                    glGetShaderInfoLog(vertexShaderObject, iInfoLogLength, &written, szInfoLog);
                    printf("Vertex Shader Compilation Log : %s\n", szInfoLog);
                    free(szInfoLog);
                    [self release];
                }
            }
        }
        
        // *** FRAGMENT SHADER ***
        // re-initialize
        iInfoLogLength = 0;
        iShaderCompiledStatus = 0;
        szInfoLog = NULL;
        
        // create shader
        fragmentShaderObject = glCreateShader(GL_FRAGMENT_SHADER);
        
        // provide source code to shader
        const GLchar *fragmentShaderSourceCode =
        "#version 300 es" \
        "\n"\
        "precision highp float;"\
        "in vec3 phong_ads_color;" \
        "out vec4 FragColor;" \
        "void main(void)" \
        "{" \
        "FragColor = vec4(phong_ads_color, 1.0);" \
        "}";
        
        glShaderSource(fragmentShaderObject, 1, (const GLchar **)&fragmentShaderSourceCode, NULL);
        
        // compile shader
        glCompileShader(fragmentShaderObject);
        glGetShaderiv(fragmentShaderObject, GL_COMPILE_STATUS, &iShaderCompiledStatus);
        if (iShaderCompiledStatus == GL_FALSE)
        {
            glGetShaderiv(fragmentShaderObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
            if (iInfoLogLength > 0)
            {
                szInfoLog = (char *)malloc(iInfoLogLength);
                if (szInfoLog != NULL)
                {
                    GLsizei written;
                    glGetShaderInfoLog(fragmentShaderObject, iInfoLogLength, &written, szInfoLog);
                    printf("Fragment Shader Compilation Log : %s\n", szInfoLog);
                    free(szInfoLog);
                    [self release];
                }
            }
        }
        
        // *** SHADER PROGRAM ***
        // create
        shaderProgramObject = glCreateProgram();
        
        // attach vertex shader to shader program
        glAttachShader(shaderProgramObject, vertexShaderObject);
        
        // attach fragment shader to shader program
        glAttachShader(shaderProgramObject, fragmentShaderObject);
        
        // pre-link binding of shader program object with vertex shader position attribute
        glBindAttribLocation(shaderProgramObject, VDG_ATTRIBUTE_VERTEX, "vPosition");
        glBindAttribLocation(shaderProgramObject, VDG_ATTRIBUTE_NORMAL, "vNormal");
        
        // link shader
        glLinkProgram(shaderProgramObject);
        GLint iShaderProgramLinkStatus = 0;
        glGetProgramiv(shaderProgramObject, GL_LINK_STATUS, &iShaderProgramLinkStatus);
        if (iShaderProgramLinkStatus == GL_FALSE)
        {
            glGetProgramiv(shaderProgramObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
            if (iInfoLogLength>0)
            {
                szInfoLog = (char *)malloc(iInfoLogLength);
                if (szInfoLog != NULL)
                {
                    GLsizei written;
                    glGetProgramInfoLog(shaderProgramObject, iInfoLogLength, &written, szInfoLog);
                    printf("Shader Program Link Log : %s\n", szInfoLog);
                    free(szInfoLog);
                    [self release];
                }
            }
        }
        
        // get MVP uniform location
        model_matrix_uniform = glGetUniformLocation(shaderProgramObject, "u_model_matrix");
        view_matrix_uniform = glGetUniformLocation(shaderProgramObject, "u_view_matrix");
        projection_matrix_uniform = glGetUniformLocation(shaderProgramObject, "u_projection_matrix");
        
        
        doubleTapUniform = glGetUniformLocation(shaderProgramObject, "u_double_tap");
        
        // Ambient color intensity of light
        red_La_uniform = glGetUniformLocation(shaderProgramObject, "u_La_red");
        // Diffuse color intensity of light
        red_Ld_uniform = glGetUniformLocation(shaderProgramObject, "u_Ld_red");
        // Specular color intensity of light
        red_Ls_uniform = glGetUniformLocation(shaderProgramObject, "u_Ls_red");
        
        // Position of light
        red_light_position_uniform = glGetUniformLocation(shaderProgramObject, "u_light_position_red");
        
        // Ambient color intensity of light
        green_La_uniform = glGetUniformLocation(shaderProgramObject, "u_La_green");
        // Diffuse color intensity of light
        green_Ld_uniform = glGetUniformLocation(shaderProgramObject, "u_Ld_green");
        // Specular color intensity of light
        green_Ls_uniform = glGetUniformLocation(shaderProgramObject, "u_Ls_green");
        
        // Position of light
        green_light_position_uniform = glGetUniformLocation(shaderProgramObject, "u_light_position_green");
        
        blue_La_uniform = glGetUniformLocation(shaderProgramObject, "u_La_blue");
        // Diffuse color intensity of light
        blue_Ld_uniform = glGetUniformLocation(shaderProgramObject, "u_Ld_blue");
        // Specular color intensity of light
        blue_Ls_uniform = glGetUniformLocation(shaderProgramObject, "u_Ls_blue");
        
        // Position of light
        blue_light_position_uniform = glGetUniformLocation(shaderProgramObject, "u_light_position_blue");
        
        // Ambient color reflective intensity of material
        Ka_uniform = glGetUniformLocation(shaderProgramObject, "u_Ka");
        // diffuse reflective color intensity of material
        Kd_uniform = glGetUniformLocation(shaderProgramObject, "u_Kd");
        // specular reflective color intensity of material
        Ks_uniform = glGetUniformLocation(shaderProgramObject, "u_Ks");
        // shininess of material ( value is conventionally between 1 to 200 )
        material_shininess_uniform = glGetUniformLocation(shaderProgramObject, "u_material_shininess");
        
        // *** vertices, colors, shader attribute, vbo, vao initialization ***
        getSphereVertexData(sphere_vertices, sphere_normals, sphere_textures, sphere_elements);
        
        numVertices = getNumberOfSphereVertices();
        numElements = getNumberOfSphereElements();
        glGenVertexArrays(1, &vao_sphere);
        glBindVertexArray(vao_sphere);
        
        // position vbo
        glGenBuffers(1, &vbo_sphere_position);
        glBindBuffer(GL_ARRAY_BUFFER, vbo_sphere_position);
        glBufferData(GL_ARRAY_BUFFER, sizeof(sphere_vertices), sphere_vertices, GL_STATIC_DRAW);
        
        glVertexAttribPointer(VDG_ATTRIBUTE_VERTEX, 3, GL_FLOAT, GL_FALSE, 0, NULL);
        
        glEnableVertexAttribArray(VDG_ATTRIBUTE_VERTEX);
        
        glBindBuffer(GL_ARRAY_BUFFER, 0);
        
        // normal vbo
        glGenBuffers(1, &vbo_sphere_normal);
        glBindBuffer(GL_ARRAY_BUFFER, vbo_sphere_normal);
        glBufferData(GL_ARRAY_BUFFER, sizeof(sphere_normals), sphere_normals, GL_STATIC_DRAW);
        
        glVertexAttribPointer(VDG_ATTRIBUTE_NORMAL, 3, GL_FLOAT, GL_FALSE, 0, NULL);
        
        glEnableVertexAttribArray(VDG_ATTRIBUTE_NORMAL);
        
        glBindBuffer(GL_ARRAY_BUFFER, 0);
        
        // element vbo
        glGenBuffers(1, &vbo_sphere_element);
        glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vbo_sphere_element);
        glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(sphere_elements), sphere_elements, GL_STATIC_DRAW);
        glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
        glBindVertexArray(0);
        // ==================
        
        // enable depth testing
        glEnable(GL_DEPTH_TEST);
        // depth test to do
        glDepthFunc(GL_LEQUAL);
        // We will always cull back faces for better performance
        //glEnable(GL_CULL_FACE);
        
        // set background color
        glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
        // set projection matrix to identity matrix
        perspectiveProjectionMatrix = vmath::mat4::identity();
        
        // GESTURE RECOGNITION
        // Tap gesture code
        UITapGestureRecognizer *singleTapGestureRecognizer=[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(onSingleTap:)];
        [singleTapGestureRecognizer setNumberOfTapsRequired:1];
        [singleTapGestureRecognizer setNumberOfTouchesRequired:1]; // touch of 1 finger
        [singleTapGestureRecognizer setDelegate:self];
        [self addGestureRecognizer:singleTapGestureRecognizer];
        
        UITapGestureRecognizer *doubleTapGestureRecognizer=[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(onDoubleTap:)];
        [doubleTapGestureRecognizer setNumberOfTapsRequired:2];
        [doubleTapGestureRecognizer setNumberOfTouchesRequired:1]; // touch of 1 finger
        [doubleTapGestureRecognizer setDelegate:self];
        [self addGestureRecognizer:doubleTapGestureRecognizer];
        
        // this will allow to differentiate between single tap and double tap
        [singleTapGestureRecognizer requireGestureRecognizerToFail:doubleTapGestureRecognizer];
        
        // swipe gesture
        UISwipeGestureRecognizer *swipeGestureRecognizer=[[UISwipeGestureRecognizer alloc]initWithTarget:self action:@selector(onSwipe:)];
        [self addGestureRecognizer:swipeGestureRecognizer];
        
        // long-press gesture
        UILongPressGestureRecognizer *longPressGestureRecognizer=[[UILongPressGestureRecognizer alloc]initWithTarget:self action:@selector(onLongPress:)];
        [self addGestureRecognizer:longPressGestureRecognizer];
    }
    return(self);
}



+(Class)layerClass{
    return([CAEAGLLayer class]);
}

-(void)drawView:(id)sender{
    
    [EAGLContext setCurrentContext:eaglContext];
    
    glBindFramebuffer(GL_FRAMEBUFFER,defaultFramebuffer);
    
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT | GL_STENCIL_BUFFER_BIT);
    
    // start using OpenGL program object
    glUseProgram(shaderProgramObject);
    
    // OpenGL Drawing
    // set modelview & modelviewprojection matrices to identity
    if (bLight == YES)
    {
        // set 'u_lighting_enabled' uniform
        glUniform1i(doubleTapUniform, 1);
        // setting light's properties
        glUniform3fv(red_La_uniform, 1, red_lightAmbient);
        glUniform3fv(red_Ld_uniform, 1, red_lightDiffuse);
        glUniform3fv(red_Ls_uniform, 1, red_lightSpecular);
        red_lightPosition[1] = sinf(gfRedLightAngle);
        red_lightPosition[2] = cosf(gfRedLightAngle) - 2;
        glUniform4fv(red_light_position_uniform, 1, red_lightPosition);
        
        glUniform3fv(blue_La_uniform, 1, blue_lightAmbient);
        glUniform3fv(blue_Ld_uniform, 1, blue_lightDiffuse);
        glUniform3fv(blue_Ls_uniform, 1, blue_lightSpecular);
        blue_lightPosition[0] = sinf(gfBlueLightAngle);
        blue_lightPosition[1] = cosf(gfBlueLightAngle);
        glUniform4fv(blue_light_position_uniform, 1, blue_lightPosition);
        
        glUniform3fv(green_La_uniform, 1, green_lightAmbient);
        glUniform3fv(green_Ld_uniform, 1, green_lightDiffuse);
        glUniform3fv(green_Ls_uniform, 1, green_lightSpecular);
        green_lightPosition[0] = sinf(gfGreenLightAngle);
        green_lightPosition[2] = cosf(gfGreenLightAngle) - 2;
        glUniform4fv(green_light_position_uniform, 1, green_lightPosition);
        
        // setting material's properties
        glUniform3fv(Ka_uniform, 1, material_ambient);
        glUniform3fv(Kd_uniform, 1, material_diffuse);
        glUniform3fv(Ks_uniform, 1, material_specular);
        glUniform1f(material_shininess_uniform, material_shininess);
    }
    else
    {
        glUniform1i(doubleTapUniform, 0);
    }
    // OpenGL Drawing
    // set all matrices to identity
    vmath::mat4 modelMatrix = vmath::mat4::identity();
    vmath::mat4 viewMatrix = vmath::mat4::identity();
    
    // apply z axis translation to go deep into the screen by -2.0,
    // so that triangle with same fullscreen co-ordinates, but due to above translation will look small
    modelMatrix = vmath::translate(0.0f, 0.0f, -2.0f);
    
    glUniformMatrix4fv(model_matrix_uniform, 1, GL_FALSE, modelMatrix);
    glUniformMatrix4fv(view_matrix_uniform, 1, GL_FALSE, viewMatrix);
    glUniformMatrix4fv(projection_matrix_uniform, 1, GL_FALSE, perspectiveProjectionMatrix);
    
    // *** bind vao ***
    glBindVertexArray(vao_sphere);
    
    // *** draw, either by glDrawTriangles() or glDrawArrays() or glDrawElements()
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vbo_sphere_element);
    glDrawElements(GL_TRIANGLES, numElements, GL_UNSIGNED_SHORT, 0);
    // stop using OpenGL program object
    glUseProgram(0);
    
    glBindRenderbuffer(GL_RENDERBUFFER,colorRenderbuffer);
    [eaglContext presentRenderbuffer:GL_RENDERBUFFER];
    gfRedLightAngle = (gfRedLightAngle + 0.01f);
    if (gfRedLightAngle >= 360.0f)
        gfRedLightAngle = 0.0f;
    
    gfBlueLightAngle = (gfBlueLightAngle + 0.01f);
    if (gfBlueLightAngle >= 360.0f)
        gfBlueLightAngle = 0.0f;
    
    
    gfGreenLightAngle = (gfGreenLightAngle + 0.01f);
    if (gfGreenLightAngle >= 360.0f)
        gfGreenLightAngle = 0.0f;
    
}

-(void)layoutSubviews
{
    // code
    GLint width;
    GLint height;
    
    glBindRenderbuffer(GL_RENDERBUFFER,colorRenderbuffer);
    [eaglContext renderbufferStorage:GL_RENDERBUFFER fromDrawable:(CAEAGLLayer*)self.layer];
    glGetRenderbufferParameteriv(GL_RENDERBUFFER,GL_RENDERBUFFER_WIDTH,&width);
    glGetRenderbufferParameteriv(GL_RENDERBUFFER,GL_RENDERBUFFER_HEIGHT,&height);
    
    glGenRenderbuffers(1,&depthRenderbuffer);
    glBindRenderbuffer(GL_RENDERBUFFER,depthRenderbuffer);
    glRenderbufferStorage(GL_RENDERBUFFER,GL_DEPTH_COMPONENT16,width,height);
    glFramebufferRenderbuffer(GL_FRAMEBUFFER,GL_DEPTH_ATTACHMENT,GL_RENDERBUFFER,depthRenderbuffer);
    
    glViewport(0,0,width,height);
    
    perspectiveProjectionMatrix = vmath::perspective(45.0f, (GLfloat)width/(GLfloat)height, 0.1f, 100.0f);
    
    if(glCheckFramebufferStatus(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE)
    {
        printf("Failed To Create Complete Framebuffer Object %x", glCheckFramebufferStatus(GL_FRAMEBUFFER));
    }
    
    [self drawView:nil];
}

-(void)startAnimation
{
    if (!isAnimating) {
        displayLink=[NSClassFromString(@"CADisplayLink")displayLinkWithTarget:self selector:@selector(drawView:)];
        [displayLink setPreferredFramesPerSecond:animationFrameInterval];
        [displayLink addToRunLoop:[NSRunLoop currentRunLoop] forMode:NSDefaultRunLoopMode];
        isAnimating=YES;
    }
}

-(void)stopAnimation{
    if(isAnimating){
        [displayLink invalidate];
        displayLink=nil;
        isAnimating=NO;
    }
}

// to become first responder
-(BOOL)acceptsFirstResponder
{
    // code
    return(YES);
}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    
}

-(void)onSingleTap:(UITapGestureRecognizer *)gr
{
    if(bLight == YES)
        bLight = NO;
    else
        bLight = YES;
    
}

-(void)onDoubleTap:(UITapGestureRecognizer *)gr
{
    
    
}

-(void)onSwipe:(UISwipeGestureRecognizer *)gr
{
    // code
    [self release];
    exit(0);
}

-(void)onLongPress:(UILongPressGestureRecognizer *)gr
{
    
}

- (void)dealloc
{
    // code
    // destroy vao
    if (vao_sphere)
    {
        glDeleteVertexArrays(1, &vao_sphere);
        vao_sphere = 0;
    }
    // destroy vbo
    if (vbo_sphere_position)
    {
        glDeleteBuffers(1, &vbo_sphere_position);
        vbo_sphere_position = 0;
    }
    if (vbo_sphere_normal)
    {
        glDeleteBuffers(1, &vbo_sphere_normal);
        vbo_sphere_normal = 0;
    }
    if (vbo_sphere_element)
    {
        glDeleteBuffers(1, &vbo_sphere_element);
        vbo_sphere_element = 0;
    }
    
    // detach vertex shader from shader program object
    glDetachShader(shaderProgramObject, vertexShaderObject);
    // detach fragment  shader from shader program object
    glDetachShader(shaderProgramObject, fragmentShaderObject);
    
    // delete vertex shader object
    glDeleteShader(vertexShaderObject);
    vertexShaderObject = 0;
    // delete fragment shader object
    glDeleteShader(fragmentShaderObject);
    fragmentShaderObject = 0;
    
    // delete shader program object
    glDeleteProgram(shaderProgramObject);
    shaderProgramObject = 0;
    
    if(depthRenderbuffer)
    {
        glDeleteRenderbuffers(1,&depthRenderbuffer);
        depthRenderbuffer=0;
    }
    
    if(colorRenderbuffer)
    {
        glDeleteRenderbuffers(1,&colorRenderbuffer);
        colorRenderbuffer=0;
    }
    
    if(defaultFramebuffer)
    {
        glDeleteFramebuffers(1,&defaultFramebuffer);
        defaultFramebuffer=0;
    }
    
    if([EAGLContext currentContext]==eaglContext)
    {
        [EAGLContext setCurrentContext:nil];
    }
    [eaglContext release];
    eaglContext=nil;
    
    [super dealloc];
}

@end

