//
//  GLESView.m
//  OGLPPWindow
//
//  Created by user140883 on 7/18/18.
//

#import <OpenGLES/ES3/gl.h>
#import <OpenGLES/ES3/glext.h>

#import "vmath.h"
#import "Sphere.h"
#import "GLESView.h"
#define checkImageHeight 64
#define checkImageWidth 64
enum
{
    VDG_ATTRIBUTE_VERTEX = 0,
    VDG_ATTRIBUTE_COLOR,
    VDG_ATTRIBUTE_NORMAL,
    VDG_ATTRIBUTE_TEXTURE0,
};

GLfloat lightAmbient[] = { 0.0f,0.0f,0.0f,1.0f };
GLfloat lightDiffuse[] = { 1.0f,1.0f,1.0f,1.0f };
GLfloat lightSpecular[] = { 1.0f,1.0f,1.0f,1.0f };
GLfloat lightPosition[] = { 100.0f,100.0f,100.0f,1.0f };

GLfloat material_ambient[] = { 0.0f,0.0f,0.0f,1.0f };
GLfloat material_diffuse[] = { 1.0f,1.0f,1.0f,1.0f };
GLfloat material_specular[] = { 1.0f,1.0f,1.0f,1.0f };
GLfloat material_shininess = 50.0f;

@implementation GLESView
{
    EAGLContext *eaglContext;
    
    GLuint defaultFramebuffer;
    GLuint colorRenderbuffer;
    GLuint depthRenderbuffer;
    
    id displayLink;
    NSInteger animationFrameInterval;
    BOOL isAnimating;
    BOOL bLight;
    
    GLuint perVertex_VertexShaderObject;
    GLuint perVertex_FragmentShaderObject;
    GLuint perVertexShaderProgramObject;
    
    GLuint perFragment_VertexShaderObject;
    GLuint perFragment_FragmentShaderObject;
    GLuint perFragmentShaderProgramObject;
    
    GLuint mvpUniform;
    GLuint numElements;
    GLuint numVertices;
    float sphere_vertices[1146];
    float sphere_normals[1146];
    float sphere_textures[764];
    unsigned short sphere_elements[2280];
    GLuint vao_sphere;
    GLuint vbo_sphere_position;
    GLuint vbo_sphere_normal;
    GLuint vbo_sphere_element;
    GLuint model_matrix_uniform_perVer, view_matrix_uniform_perVer, projection_matrix_uniform_perVer;
    GLuint doubleTapUniform_perVertex;
    GLuint La_uniform_perVer;
    GLuint Ld_uniform_perVer;
    GLuint Ls_uniform_perVer;
    GLuint light_position_uniform_perVer;
    
    GLuint Ka_uniform_perVer;
    GLuint Kd_uniform_perVer;
    GLuint Ks_uniform_perVer;
    GLuint material_shininess_uniform_perVer;
    
    GLuint model_matrix_uniform_perFrag, view_matrix_uniform_perFrag, projection_matrix_uniform_perFrag;
    GLuint doubleTapUniform_perFragment;
    GLuint La_uniform_perFrag;
    GLuint Ld_uniform_perFrag;
    GLuint Ls_uniform_perFrag;
    GLuint light_position_uniform_perFrag;
    
    GLuint Ka_uniform_perFrag;
    GLuint Kd_uniform_perFrag;
    GLuint Ks_uniform_perFrag;
    GLuint material_shininess_uniform_perFrag;
    BOOL vertexMode;
    vmath::mat4 perspectiveProjectionMatrix;
}

-(id)initWithFrame:(CGRect)frame;
{
    // code
    self=[super initWithFrame:frame];
    
    if(self)
    {
        CAEAGLLayer *eaglLayer=(CAEAGLLayer *)super.layer;
        
        eaglLayer.opaque=YES;
        eaglLayer.drawableProperties=[NSDictionary dictionaryWithObjectsAndKeys:[NSNumber numberWithBool:FALSE],
                                      kEAGLDrawablePropertyRetainedBacking,kEAGLColorFormatRGBA8,kEAGLDrawablePropertyColorFormat,nil];
        
        eaglContext=[[EAGLContext alloc]initWithAPI:kEAGLRenderingAPIOpenGLES3];
        if(eaglContext==nil)
        {
            [self release];
            return(nil);
        }
        [EAGLContext setCurrentContext:eaglContext];
        
        glGenFramebuffers(1,&defaultFramebuffer);
        glGenRenderbuffers(1,&colorRenderbuffer);
        glBindFramebuffer(GL_FRAMEBUFFER,defaultFramebuffer);
        glBindRenderbuffer(GL_RENDERBUFFER,colorRenderbuffer);
        
        [eaglContext renderbufferStorage:GL_RENDERBUFFER fromDrawable:eaglLayer];
        
        glFramebufferRenderbuffer(GL_FRAMEBUFFER,GL_COLOR_ATTACHMENT0,GL_RENDERBUFFER,colorRenderbuffer);
        
        GLint backingWidth;
        GLint backingHeight;
        vertexMode = YES;
        glGetRenderbufferParameteriv(GL_RENDERBUFFER,GL_RENDERBUFFER_WIDTH,&backingWidth);
        glGetRenderbufferParameteriv(GL_RENDERBUFFER,GL_RENDERBUFFER_HEIGHT,&backingHeight);
        
        glGenRenderbuffers(1,&depthRenderbuffer);
        glBindRenderbuffer(GL_RENDERBUFFER,depthRenderbuffer);
        glRenderbufferStorage(GL_RENDERBUFFER,GL_DEPTH_COMPONENT16,backingWidth,backingHeight);
        glFramebufferRenderbuffer(GL_FRAMEBUFFER,GL_DEPTH_ATTACHMENT,GL_RENDERBUFFER,depthRenderbuffer);
        
        if(glCheckFramebufferStatus(GL_FRAMEBUFFER)!=GL_FRAMEBUFFER_COMPLETE)
        {
            printf("Failed To Create Complete Framebuffer Object %x\n",glCheckFramebufferStatus(GL_FRAMEBUFFER));
            glDeleteFramebuffers(1,&defaultFramebuffer);
            glDeleteRenderbuffers(1,&colorRenderbuffer);
            glDeleteRenderbuffers(1,&depthRenderbuffer);
            
            return(nil);
        }
        
        printf("Renderer : %s | GL Version : %s | GLSL Version : %s\n",glGetString(GL_RENDERER),glGetString(GL_VERSION),glGetString(GL_SHADING_LANGUAGE_VERSION));
        
        // hard coded initializations
        isAnimating=NO;
        animationFrameInterval=60; // default since iOS 8.2
        
        
        // *** VERTEX SHADER ***
        // create shader
        perVertex_VertexShaderObject = glCreateShader(GL_VERTEX_SHADER);
        
        // provide source code to shader
        const GLchar *vertexShaderSourceCode =
        "#version 300 es" \
        "\n" \
        "in vec4 vPosition;" \
        "in vec3 vNormal;" \
        "uniform mat4 u_model_matrix;" \
        "uniform mat4 u_view_matrix;" \
        "uniform mat4 u_projection_matrix;" \
        "uniform mediump int u_double_tap;"\
        "uniform vec3 u_La;"\
        "uniform vec3 u_Ld;"\
        "uniform vec3 u_Ls;" \
        "uniform vec4 u_light_position;"\
        "uniform vec3 u_Ka;"\
        "uniform vec3 u_Ks;"\
        "uniform vec3 u_Kd;" \
        "uniform float u_material_shininess;" \
        "out vec3 phong_ads_color;" \
        "void main(void)" \
        "{" \
        "if (u_double_tap == 1)" \
        "{" \
        "vec4 eye_coordinates=u_view_matrix * u_model_matrix * vPosition;" \
        "vec3 transformed_normals=normalize(mat3(u_view_matrix * u_model_matrix) * vNormal);" \
        "vec3 light_direction = normalize(vec3(u_light_position) - eye_coordinates.xyz);" \
        "float tn_dot_ld = max(dot(transformed_normals, light_direction),0.0);" \
        "vec3 ambient = u_La * u_Ka;" \
        "vec3 diffuse = u_Ld * u_Kd * tn_dot_ld;" \
        "vec3 reflection_vector = reflect(-light_direction, transformed_normals);" \
        "vec3 viewer_vector = normalize(-eye_coordinates.xyz);" \
        "vec3 specular = u_Ls * u_Ks * pow(max(dot(reflection_vector, viewer_vector), 0.0), u_material_shininess);" \
        "phong_ads_color=ambient + diffuse + specular;" \
        "}" \
        "else" \
        "{" \
        "phong_ads_color = vec3(1.0, 1.0, 1.0);" \
        "}" \
        "gl_Position=u_projection_matrix * u_view_matrix * u_model_matrix * vPosition;" \
        "}";
        
        glShaderSource(perVertex_VertexShaderObject, 1, (const char **)&vertexShaderSourceCode, NULL);
        
        // Compile shader
        glCompileShader(perVertex_VertexShaderObject);
        GLint iInfoLogLength = 0;
        GLint iShaderCompiledStatus = 0;
        char *szInfoLog = NULL;
        glGetShaderiv(perVertex_VertexShaderObject, GL_COMPILE_STATUS, &iShaderCompiledStatus);
        if (iShaderCompiledStatus == GL_FALSE)
        {
            glGetShaderiv(perVertex_VertexShaderObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
            if (iInfoLogLength > 0)
            {
                szInfoLog = (char *)malloc(iInfoLogLength);
                if (szInfoLog != NULL)
                {
                    GLsizei written;
                    glGetShaderInfoLog(perVertex_VertexShaderObject, iInfoLogLength, &written, szInfoLog);
                    printf("Vertex Shader Compilation Log : %s\n", szInfoLog);
                    free(szInfoLog);
                    [self release];
                }
            }
        }
        
        // *** FRAGMENT SHADER ***
        // re-initialize
        iInfoLogLength = 0;
        iShaderCompiledStatus = 0;
        szInfoLog = NULL;
        
        // create shader
        perVertex_FragmentShaderObject = glCreateShader(GL_FRAGMENT_SHADER);
        
        // provide source code to shader
        const GLchar *fragmentShaderSourceCode =
        "#version 300 es" \
        "\n"\
        "precision highp float;"\
        "in vec3 phong_ads_color;" \
        "out vec4 FragColor;" \
        "void main(void)" \
        "{" \
        "FragColor = vec4(phong_ads_color, 1.0);" \
        "}";
        
        glShaderSource(perVertex_FragmentShaderObject, 1, (const GLchar **)&fragmentShaderSourceCode, NULL);
        // compile shader
        glCompileShader(perVertex_FragmentShaderObject);
        glGetShaderiv(perVertex_FragmentShaderObject, GL_COMPILE_STATUS, &iShaderCompiledStatus);
        if (iShaderCompiledStatus == GL_FALSE)
        {
            glGetShaderiv(perVertex_FragmentShaderObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
            if (iInfoLogLength > 0)
            {
                szInfoLog = (char *)malloc(iInfoLogLength);
                if (szInfoLog != NULL)
                {
                    GLsizei written;
                    glGetShaderInfoLog(perVertex_FragmentShaderObject, iInfoLogLength, &written, szInfoLog);
                    printf("Fragment Shader Compilation Log : %s\n", szInfoLog);
                    free(szInfoLog);
                    [self release];
                }
            }
        }
        
        // *** SHADER PROGRAM ***
        // create
        perVertexShaderProgramObject = glCreateProgram();
        
        // Attach vertex shader to shader program
        glAttachShader(perVertexShaderProgramObject, perVertex_VertexShaderObject);
        
        // Attach fragment shader to shader program
        glAttachShader(perVertexShaderProgramObject, perVertex_FragmentShaderObject);
        glBindAttribLocation(perVertexShaderProgramObject, VDG_ATTRIBUTE_VERTEX, "vPosition");
        glBindAttribLocation(perVertexShaderProgramObject, VDG_ATTRIBUTE_NORMAL, "vNormal");
        glLinkProgram(perVertexShaderProgramObject);
        GLint iShaderProgramLinkStatus = 0;
        glGetProgramiv(perVertexShaderProgramObject, GL_LINK_STATUS, &iShaderProgramLinkStatus);
        if (iShaderProgramLinkStatus == GL_FALSE)
        {
            glGetProgramiv(perVertexShaderProgramObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
            if (iInfoLogLength>0)
            {
                szInfoLog = (char *)malloc(iInfoLogLength);
                if (szInfoLog != NULL)
                {
                    GLsizei written;
                    glGetProgramInfoLog(perVertexShaderProgramObject, iInfoLogLength, &written, szInfoLog);
                    printf("Shader Program Link Log : %s\n", szInfoLog);
                    free(szInfoLog);
                    [self release];
                }
            }
        }
        
        // get MVP uniform location
        model_matrix_uniform_perVer = glGetUniformLocation(perVertexShaderProgramObject, "u_model_matrix");
        view_matrix_uniform_perVer = glGetUniformLocation(perVertexShaderProgramObject, "u_view_matrix");
        projection_matrix_uniform_perVer = glGetUniformLocation(perVertexShaderProgramObject, "u_projection_matrix");
        doubleTapUniform_perVertex = glGetUniformLocation(perVertexShaderProgramObject, "u_double_tap");
        
        // Ambient color intensity of light
        La_uniform_perVer = glGetUniformLocation(perVertexShaderProgramObject, "u_La");
        // Diffuse color intensity of light
        Ld_uniform_perVer = glGetUniformLocation(perVertexShaderProgramObject, "u_Ld");
        // Specular color intensity of light
        Ls_uniform_perVer = glGetUniformLocation(perVertexShaderProgramObject, "u_Ls");
        
        // Position of light
        light_position_uniform_perVer = glGetUniformLocation(perVertexShaderProgramObject, "u_light_position");;
        
        // Ambient color reflective intensity of material
        Ka_uniform_perVer = glGetUniformLocation(perVertexShaderProgramObject, "u_Ka");
        // diffuse reflective color intensity of material
        Kd_uniform_perVer = glGetUniformLocation(perVertexShaderProgramObject, "u_Kd");
        // specular reflective color intensity of material
        Ks_uniform_perVer = glGetUniformLocation(perVertexShaderProgramObject, "u_Ks");
        // shininess of material ( value is conventionally between 1 to 200 )
        material_shininess_uniform_perVer = glGetUniformLocation(perVertexShaderProgramObject, "u_material_shininess");
        
        perFragment_VertexShaderObject = glCreateShader(GL_VERTEX_SHADER);
        
        // provide source code to shader
        vertexShaderSourceCode =
        "#version 300 es" \
        "\n"\
        "in vec4 vPosition;"\
        "in vec3 vNormal;"\
        "uniform mat4 u_model_matrix;"\
        "uniform mat4 u_view_matrix;"\
        "uniform mat4 u_projection_matrix;"\
        "uniform mediump int u_double_tap;"\
        "uniform vec4 u_light_position;"\
        "out vec3 transformed_normals;"\
        "out vec3 light_direction;"\
        "out vec3 viewer_vector;"\
        "void main(void)"\
        "{"\
        "if (u_double_tap == 1)"\
        "{"\
        "vec4 eye_coordinates=u_view_matrix * u_model_matrix * vPosition;"\
        "transformed_normals=mat3(u_view_matrix * u_model_matrix) * vNormal;"\
        "light_direction = vec3(u_light_position) - eye_coordinates.xyz;"\
        "viewer_vector = -eye_coordinates.xyz;"\
        "}"\
        "gl_Position=u_projection_matrix * u_view_matrix * u_model_matrix * vPosition;"\
        "}";
        glShaderSource(perFragment_VertexShaderObject, 1, (const char **)&vertexShaderSourceCode, NULL);
        
        // Compile shader
        glCompileShader(perFragment_VertexShaderObject);
        iInfoLogLength = 0;
        iShaderCompiledStatus = 0;
        szInfoLog = NULL;
        glGetShaderiv(perFragment_VertexShaderObject, GL_COMPILE_STATUS, &iShaderCompiledStatus);
        if (iShaderCompiledStatus == GL_FALSE)
        {
            glGetShaderiv(perFragment_VertexShaderObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
            if (iInfoLogLength > 0)
            {
                szInfoLog = (char *)malloc(iInfoLogLength);
                if (szInfoLog != NULL)
                {
                    GLsizei written;
                    glGetShaderInfoLog(perFragment_VertexShaderObject, iInfoLogLength, &written, szInfoLog);
                    printf("Vertex Shader Compilation Log : %s\n", szInfoLog);
                    free(szInfoLog);
                    [self release];
                }
            }
        }
        
        // ** fragment shader***
        perFragment_FragmentShaderObject = glCreateShader(GL_FRAGMENT_SHADER);
        
        // Provide source code to shader
        fragmentShaderSourceCode =
        "#version 300 es" \
        "\n"\
        "precision highp float;"\
        "in vec3 transformed_normals;"\
        "in vec3 light_direction;"\
        "in vec3 viewer_vector;"\
        "out vec4 FragColor;"\
        "uniform vec3 u_La;"\
        "uniform vec3 u_Ld;"\
        "uniform vec3 u_Ls;"\
        "uniform vec3 u_Ka;"\
        "uniform vec3 u_Kd;"\
        "uniform vec3 u_Ks;"\
        "uniform float u_material_shininess;"\
        "uniform int u_double_tap;"\
        "void main(void)"\
        "{"\
        "vec3 phong_ads_color;"\
        "if(u_double_tap==1)"\
        "{"\
        "vec3 normalized_transformed_normals=normalize(transformed_normals);"\
        "vec3 normalized_light_direction=normalize(light_direction);"\
        "vec3 normalized_viewer_vector=normalize(viewer_vector);"\
        "vec3 ambient = u_La * u_Ka;"\
        "float tn_dot_ld = max(dot(normalized_transformed_normals, normalized_light_direction),0.0);"\
        "vec3 diffuse = u_Ld * u_Kd * tn_dot_ld;"\
        "vec3 reflection_vector = reflect(-normalized_light_direction, normalized_transformed_normals);"\
        "vec3 specular = u_Ls * u_Ks * pow(max(dot(reflection_vector, normalized_viewer_vector), 0.0), u_material_shininess);"\
        "phong_ads_color=ambient + diffuse + specular;"\
        "}"\
        "else"\
        "{"\
        "phong_ads_color = vec3(1.0, 1.0, 1.0);"\
        "}"\
        "FragColor = vec4(phong_ads_color, 1.0);"\
        "}";
        
        glShaderSource(perFragment_FragmentShaderObject, 1, (const GLchar **)&fragmentShaderSourceCode, NULL);
        // compile shader
        glCompileShader(perFragment_FragmentShaderObject);
        if (iShaderCompiledStatus == GL_FALSE)
        {
            glGetShaderiv(perFragment_FragmentShaderObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
            if (iInfoLogLength > 0)
            {
                szInfoLog = (char *)malloc(iInfoLogLength);
                if (szInfoLog != NULL)
                {
                    GLsizei written;
                    glGetShaderInfoLog(perFragment_FragmentShaderObject, iInfoLogLength, &written, szInfoLog);
                    printf("Fragment Shader Compilation Log : %s\n", szInfoLog);
                    free(szInfoLog);
                    [self release];
                }
            }
        }
        
        perFragmentShaderProgramObject = glCreateProgram();
        
        // Attach vertex shader to shader program
        glAttachShader(perFragmentShaderProgramObject, perFragment_VertexShaderObject);
        
        // Attach fragment shader to shader program
        glAttachShader(perFragmentShaderProgramObject, perFragment_FragmentShaderObject);
        glBindAttribLocation(perFragmentShaderProgramObject, VDG_ATTRIBUTE_VERTEX, "vPosition");
        glBindAttribLocation(perFragmentShaderProgramObject, VDG_ATTRIBUTE_NORMAL, "vNormal");
        glLinkProgram(perFragmentShaderProgramObject);
        iShaderProgramLinkStatus = 0;
        glGetProgramiv(perFragmentShaderProgramObject, GL_LINK_STATUS, &iShaderProgramLinkStatus);
        if (iShaderProgramLinkStatus == GL_FALSE) {
            glGetProgramiv(perFragmentShaderProgramObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
            if (iInfoLogLength > 0) {
                szInfoLog = (char *)malloc(sizeof(iInfoLogLength));
                if (szInfoLog != NULL) {
                    GLsizei written;
                    glGetProgramInfoLog(perFragmentShaderProgramObject, iInfoLogLength, &written, szInfoLog);
                    printf("Linking Log : %s\n", szInfoLog);
                    free(szInfoLog);
                    [self release];
                }
            }
        }
        model_matrix_uniform_perFrag = glGetUniformLocation(perFragmentShaderProgramObject, "u_model_matrix");
        view_matrix_uniform_perFrag = glGetUniformLocation(perFragmentShaderProgramObject, "u_view_matrix");
        projection_matrix_uniform_perFrag = glGetUniformLocation(perFragmentShaderProgramObject, "u_projection_matrix");
        doubleTapUniform_perFragment = glGetUniformLocation(perFragmentShaderProgramObject, "u_double_tap");
        
        // Ambient color intensity of light
        La_uniform_perFrag = glGetUniformLocation(perFragmentShaderProgramObject, "u_La");
        // Diffuse color intensity of light
        Ld_uniform_perFrag = glGetUniformLocation(perFragmentShaderProgramObject, "u_Ld");
        // Specular color intensity of light
        Ls_uniform_perFrag = glGetUniformLocation(perFragmentShaderProgramObject, "u_Ls");
        
        // Position of light
        light_position_uniform_perFrag = glGetUniformLocation(perFragmentShaderProgramObject, "u_light_position");;
        
        // Ambient color reflective intensity of material
        Ka_uniform_perFrag = glGetUniformLocation(perFragmentShaderProgramObject, "u_Ka");
        // diffuse reflective color intensity of material
        Kd_uniform_perFrag = glGetUniformLocation(perFragmentShaderProgramObject, "u_Kd");
        // specular reflective color intensity of material
        Ks_uniform_perFrag = glGetUniformLocation(perFragmentShaderProgramObject, "u_Ks");
        // shininess of material ( value is conventionally between 1 to 200 )
        material_shininess_uniform_perFrag = glGetUniformLocation(perFragmentShaderProgramObject, "u_material_shininess");
        // *** vertices, colors, shader attribute, vbo, vao initialization ***
        getSphereVertexData(sphere_vertices, sphere_normals, sphere_textures, sphere_elements);
        
        numVertices = getNumberOfSphereVertices();
        numElements = getNumberOfSphereElements();
        glGenVertexArrays(1, &vao_sphere);
        glBindVertexArray(vao_sphere);
        
        // position vbo
        glGenBuffers(1, &vbo_sphere_position);
        glBindBuffer(GL_ARRAY_BUFFER, vbo_sphere_position);
        glBufferData(GL_ARRAY_BUFFER, sizeof(sphere_vertices), sphere_vertices, GL_STATIC_DRAW);
        
        glVertexAttribPointer(VDG_ATTRIBUTE_VERTEX, 3, GL_FLOAT, GL_FALSE, 0, NULL);
        
        glEnableVertexAttribArray(VDG_ATTRIBUTE_VERTEX);
        
        glBindBuffer(GL_ARRAY_BUFFER, 0);
        
        // normal vbo
        glGenBuffers(1, &vbo_sphere_normal);
        glBindBuffer(GL_ARRAY_BUFFER, vbo_sphere_normal);
        glBufferData(GL_ARRAY_BUFFER, sizeof(sphere_normals), sphere_normals, GL_STATIC_DRAW);
        
        glVertexAttribPointer(VDG_ATTRIBUTE_NORMAL, 3, GL_FLOAT, GL_FALSE, 0, NULL);
        
        glEnableVertexAttribArray(VDG_ATTRIBUTE_NORMAL);
        
        glBindBuffer(GL_ARRAY_BUFFER, 0);
        
        // element vbo
        glGenBuffers(1, &vbo_sphere_element);
        glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vbo_sphere_element);
        glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(sphere_elements), sphere_elements, GL_STATIC_DRAW);
        glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
        glBindVertexArray(0);
        // ==================
        
        // enable depth testing
        glEnable(GL_DEPTH_TEST);
        // depth test to do
        glDepthFunc(GL_LEQUAL);
        // We will always cull back faces for better performance
        //glEnable(GL_CULL_FACE);
        
        // set background color
        glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
        // set projection matrix to identity matrix
        perspectiveProjectionMatrix = vmath::mat4::identity();
        
        // GESTURE RECOGNITION
        // Tap gesture code
        UITapGestureRecognizer *singleTapGestureRecognizer=[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(onSingleTap:)];
        [singleTapGestureRecognizer setNumberOfTapsRequired:1];
        [singleTapGestureRecognizer setNumberOfTouchesRequired:1]; // touch of 1 finger
        [singleTapGestureRecognizer setDelegate:self];
        [self addGestureRecognizer:singleTapGestureRecognizer];
        
        UITapGestureRecognizer *doubleTapGestureRecognizer=[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(onDoubleTap:)];
        [doubleTapGestureRecognizer setNumberOfTapsRequired:2];
        [doubleTapGestureRecognizer setNumberOfTouchesRequired:1]; // touch of 1 finger
        [doubleTapGestureRecognizer setDelegate:self];
        [self addGestureRecognizer:doubleTapGestureRecognizer];
        
        // this will allow to differentiate between single tap and double tap
        [singleTapGestureRecognizer requireGestureRecognizerToFail:doubleTapGestureRecognizer];
        
        // swipe gesture
        UISwipeGestureRecognizer *swipeGestureRecognizer=[[UISwipeGestureRecognizer alloc]initWithTarget:self action:@selector(onSwipe:)];
        [self addGestureRecognizer:swipeGestureRecognizer];
        
        // long-press gesture
        UILongPressGestureRecognizer *longPressGestureRecognizer=[[UILongPressGestureRecognizer alloc]initWithTarget:self action:@selector(onLongPress:)];
        [self addGestureRecognizer:longPressGestureRecognizer];
    }
    return(self);
}



+(Class)layerClass{
    return([CAEAGLLayer class]);
}

-(void)drawView:(id)sender{
    
    [EAGLContext setCurrentContext:eaglContext];
    
    glBindFramebuffer(GL_FRAMEBUFFER,defaultFramebuffer);
    
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT | GL_STENCIL_BUFFER_BIT);
    
    // start using OpenGL program object
    
    
    // OpenGL Drawing
    // set modelview & modelviewprojection matrices to identity
    if (vertexMode == YES) {
        glUseProgram(perVertexShaderProgramObject);
        if (bLight == YES)
        {
            
            // set 'u_lighting_enabled' uniform
            glUniform1i(doubleTapUniform_perVertex, 1);
            
            // setting light's properties
            glUniform3fv(La_uniform_perVer, 1, lightAmbient);
            glUniform3fv(Ld_uniform_perVer, 1, lightDiffuse);
            glUniform3fv(Ls_uniform_perVer, 1, lightSpecular);
            glUniform4fv(light_position_uniform_perVer, 1, lightPosition);
            
            // setting material's properties
            glUniform3fv(Ka_uniform_perVer, 1, material_ambient);
            glUniform3fv(Kd_uniform_perVer, 1, material_diffuse);
            glUniform3fv(Ks_uniform_perVer, 1, material_specular);
            glUniform1f(material_shininess_uniform_perVer, material_shininess);
        }
        else
        {
            // set 'u_lighting_enabled' uniform
            
            glUniform1i(doubleTapUniform_perVertex, 0);
        }
        
        // OpenGL Drawing
        // set all matrices to identity
        vmath::mat4 modelMatrix = vmath::mat4::identity();
        vmath::mat4 viewMatrix = vmath::mat4::identity();
        
        // apply z axis translation to go deep into the screen by -2.0,
        // so that triangle with same fullscreen co-ordinates, but due to above translation will look small
        modelMatrix = vmath::translate(0.0f, 0.0f, -2.0f);
        
        glUniformMatrix4fv(model_matrix_uniform_perVer, 1, GL_FALSE, modelMatrix);
        glUniformMatrix4fv(view_matrix_uniform_perVer, 1, GL_FALSE, viewMatrix);
        glUniformMatrix4fv(projection_matrix_uniform_perVer, 1, GL_FALSE, perspectiveProjectionMatrix);
        
        // *** bind vao ***
        glBindVertexArray(vao_sphere);
        
        // *** draw, either by glDrawTriangles() or glDrawArrays() or glDrawElements()
        glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vbo_sphere_element);
        glDrawElements(GL_TRIANGLES, numElements, GL_UNSIGNED_SHORT, 0);
    }
    else {
        glUseProgram(perFragmentShaderProgramObject);
        if (bLight == YES)
        {
            
            // set 'u_lighting_enabled' uniform
            glUniform1i(doubleTapUniform_perFragment, 1);
            
            // setting light's properties
            glUniform3fv(La_uniform_perFrag, 1, lightAmbient);
            glUniform3fv(Ld_uniform_perFrag, 1, lightDiffuse);
            glUniform3fv(Ls_uniform_perFrag, 1, lightSpecular);
            glUniform4fv(light_position_uniform_perFrag, 1, lightPosition);
            
            // setting material's properties
            glUniform3fv(Ka_uniform_perFrag, 1, material_ambient);
            glUniform3fv(Kd_uniform_perFrag, 1, material_diffuse);
            glUniform3fv(Ks_uniform_perFrag, 1, material_specular);
            glUniform1f(material_shininess_uniform_perFrag, material_shininess);
        }
        else
        {
            // set 'u_lighting_enabled' uniform
            
            glUniform1i(doubleTapUniform_perFragment, 0);
        }
        
        // OpenGL Drawing
        // set all matrices to identity
        vmath::mat4 modelMatrix = vmath::mat4::identity();
        vmath::mat4 viewMatrix = vmath::mat4::identity();
        
        // apply z axis translation to go deep into the screen by -2.0,
        // so that triangle with same fullscreen co-ordinates, but due to above translation will look small
        modelMatrix = vmath::translate(0.0f, 0.0f, -2.0f);
        
        glUniformMatrix4fv(model_matrix_uniform_perFrag, 1, GL_FALSE, modelMatrix);
        glUniformMatrix4fv(view_matrix_uniform_perFrag, 1, GL_FALSE, viewMatrix);
        glUniformMatrix4fv(projection_matrix_uniform_perFrag, 1, GL_FALSE, perspectiveProjectionMatrix);
        
        // *** bind vao ***
        glBindVertexArray(vao_sphere);
        
        // *** draw, either by glDrawTriangles() or glDrawArrays() or glDrawElements()
        glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vbo_sphere_element);
        glDrawElements(GL_TRIANGLES, numElements, GL_UNSIGNED_SHORT, 0);
    }
    
    
    // stop using OpenGL program object
    glUseProgram(0);
    
    glBindRenderbuffer(GL_RENDERBUFFER,colorRenderbuffer);
    [eaglContext presentRenderbuffer:GL_RENDERBUFFER];
}

-(void)layoutSubviews
{
    // code
    GLint width;
    GLint height;
    
    glBindRenderbuffer(GL_RENDERBUFFER,colorRenderbuffer);
    [eaglContext renderbufferStorage:GL_RENDERBUFFER fromDrawable:(CAEAGLLayer*)self.layer];
    glGetRenderbufferParameteriv(GL_RENDERBUFFER,GL_RENDERBUFFER_WIDTH,&width);
    glGetRenderbufferParameteriv(GL_RENDERBUFFER,GL_RENDERBUFFER_HEIGHT,&height);
    
    glGenRenderbuffers(1,&depthRenderbuffer);
    glBindRenderbuffer(GL_RENDERBUFFER,depthRenderbuffer);
    glRenderbufferStorage(GL_RENDERBUFFER,GL_DEPTH_COMPONENT16,width,height);
    glFramebufferRenderbuffer(GL_FRAMEBUFFER,GL_DEPTH_ATTACHMENT,GL_RENDERBUFFER,depthRenderbuffer);
    
    glViewport(0,0,width,height);
    
    perspectiveProjectionMatrix = vmath::perspective(45.0f, (GLfloat)width/(GLfloat)height, 0.1f, 100.0f);
    
    if(glCheckFramebufferStatus(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE)
    {
        printf("Failed To Create Complete Framebuffer Object %x", glCheckFramebufferStatus(GL_FRAMEBUFFER));
    }
    
    [self drawView:nil];
}

-(void)startAnimation
{
    if (!isAnimating) {
        displayLink=[NSClassFromString(@"CADisplayLink")displayLinkWithTarget:self selector:@selector(drawView:)];
        [displayLink setPreferredFramesPerSecond:animationFrameInterval];
        [displayLink addToRunLoop:[NSRunLoop currentRunLoop] forMode:NSDefaultRunLoopMode];
        isAnimating=YES;
    }
}

-(void)stopAnimation{
    if(isAnimating){
        [displayLink invalidate];
        displayLink=nil;
        isAnimating=NO;
    }
}

// to become first responder
-(BOOL)acceptsFirstResponder
{
    // code
    return(YES);
}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    
}

-(void)onSingleTap:(UITapGestureRecognizer *)gr
{
    if(bLight == YES)
        bLight = NO;
    else
        bLight = YES;
    
}

-(void)onDoubleTap:(UITapGestureRecognizer *)gr
{
    
    if(vertexMode == YES)
        vertexMode = NO;
    else
        vertexMode = YES;
}

-(void)onSwipe:(UISwipeGestureRecognizer *)gr
{
    // code
    [self release];
    exit(0);
}

-(void)onLongPress:(UILongPressGestureRecognizer *)gr
{
    
}

- (void)dealloc
{
    // code
    // destroy vao
    if (vao_sphere)
    {
        glDeleteVertexArrays(1, &vao_sphere);
        vao_sphere = 0;
    }
    // destroy vbo
    if (vbo_sphere_position)
    {
        glDeleteBuffers(1, &vbo_sphere_position);
        vbo_sphere_position = 0;
    }
    if (vbo_sphere_normal)
    {
        glDeleteBuffers(1, &vbo_sphere_normal);
        vbo_sphere_normal = 0;
    }
    if (vbo_sphere_element)
    {
        glDeleteBuffers(1, &vbo_sphere_element);
        vbo_sphere_element = 0;
    }
    
    // Detach vertex shader from shader program object
    glDetachShader(perVertexShaderProgramObject, perVertex_VertexShaderObject);
    
    // Detach fragment shader from shader program object
    glDetachShader(perVertexShaderProgramObject, perVertex_FragmentShaderObject);
    
    // Delete vertex shader object
    glDeleteShader(perVertex_VertexShaderObject);
    perVertex_VertexShaderObject = 0;
    
    // Delete fragment shader object
    glDeleteShader(perVertex_FragmentShaderObject);
    perVertex_FragmentShaderObject = 0;
    
    // Delete shader program object
    glDeleteProgram(perVertexShaderProgramObject);
    perVertexShaderProgramObject = 0;
    
    glDetachShader(perFragmentShaderProgramObject, perFragment_VertexShaderObject);
    
    // Detach fragment shader from shader program object
    glDetachShader(perFragmentShaderProgramObject, perFragment_FragmentShaderObject);
    
    // Delete vertex shader object
    glDeleteShader(perFragment_VertexShaderObject);
    perFragment_VertexShaderObject = 0;
    
    // Delete fragment shader object
    glDeleteShader(perFragment_FragmentShaderObject);
    perFragment_FragmentShaderObject = 0;
    
    // Delete shader program object
    glDeleteProgram(perFragmentShaderProgramObject);
    perFragmentShaderProgramObject = 0;
    
    if(depthRenderbuffer)
    {
        glDeleteRenderbuffers(1,&depthRenderbuffer);
        depthRenderbuffer=0;
    }
    
    if(colorRenderbuffer)
    {
        glDeleteRenderbuffers(1,&colorRenderbuffer);
        colorRenderbuffer=0;
    }
    
    if(defaultFramebuffer)
    {
        glDeleteFramebuffers(1,&defaultFramebuffer);
        defaultFramebuffer=0;
    }
    
    if([EAGLContext currentContext]==eaglContext)
    {
        [EAGLContext setCurrentContext:nil];
    }
    [eaglContext release];
    eaglContext=nil;
    
    [super dealloc];
}

@end

