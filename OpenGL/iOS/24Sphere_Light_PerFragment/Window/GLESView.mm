//
//  GLESView.m
//  OGLPPWindow
//
//  Created by user140883 on 7/18/18.
//

#import <OpenGLES/ES3/gl.h>
#import <OpenGLES/ES3/glext.h>

#import "vmath.h"
#import "Sphere.h"
#import "GLESView.h"
#define checkImageHeight 64
#define checkImageWidth 64
enum
{
    VDG_ATTRIBUTE_VERTEX = 0,
    VDG_ATTRIBUTE_COLOR,
    VDG_ATTRIBUTE_NORMAL,
    VDG_ATTRIBUTE_TEXTURE0,
};

int axisIdentifier = 0;
GLfloat gfLightAngle;
GLfloat lightAmbient[] = { 0.0f,0.0f,0.0f,1.0f };
GLfloat lightDiffuse[] = { 1.0f,1.0f,1.0f,1.0f };
GLfloat lightSpecular[] = { 1.0f,1.0f,1.0f,1.0f };
GLfloat lightPosition[] = { 100.0f,100.0f,100.0f,1.0f };

//11
GLfloat material_emerald_ambient[] = { 0.0215f, 0.1745f, 0.0215f, 1.0f };
GLfloat material_emerald_diffuse[] = { 0.07568f, 0.61424f, 0.07568f, 1.0f };
GLfloat material_emerald_specular[] = { 0.633f, 0.727811f, 0.633f, 1.0f };
GLfloat material_emerald_shininess = 0.6f * 128;

//12
GLfloat material_jade_ambient[] = { 0.135f, 0.225f, 0.1575f, 1.0f };
GLfloat material_jade_diffuse[] = { 0.54f, 0.89f, 0.63f, 1.0f };
GLfloat material_jade_specular[] = { 0.316228f, 0.316228f, 0.316228f, 1.0f };
GLfloat material_jade_shininess = 0.1f * 128;

//13
GLfloat material_obsidian_ambient[] = { 0.05375f, 0.05f, 0.06625f, 1.0f };
GLfloat material_obsidian_diffuse[] = { 0.18275f, 0.17f, 0.22525f, 1.0f };
GLfloat material_obsidian_specular[] = { 0.332741f, 0.328634f, 0.346435f, 1.0f };
GLfloat material_obsidian_shininess = 0.3f * 128;

//14
GLfloat material_pearl_ambient[] = { 0.25f, 0.20725f, 0.20725f, 1.0f };
GLfloat material_pearl_diffuse[] = { 1.0f, 0.829f, 0.829f, 1.0f };
GLfloat material_pearl_specular[] = { 0.296648f, 0.296648f, 0.296648f, 1.0f };
GLfloat material_pearl_shininess = 0.088f * 128;

//15
GLfloat material_ruby_ambient[] = { 0.1745f, 0.01175f, 0.01175f, 1.0f };
GLfloat material_ruby_diffuse[] = { 0.61424f, 0.04136f, 0.04136f, 1.0f };
GLfloat material_ruby_specular[] = { 0.727811f, 0.626959f, 0.626959f, 1.0f };
GLfloat material_ruby_shininess = 0.6f * 128;

//16
GLfloat material_turquoise_ambient[] = { 0.1f, 0.18725f, 0.1745f, 1.0f };
GLfloat material_turquoise_diffuse[] = { 0.396f, 0.74151f, 0.69102f, 1.0f };
GLfloat material_turquoise_specular[] = { 0.297254f, 0.30829f, 0.306678f, 1.0f };
GLfloat material_turquoise_shininess = 0.1 * 128;

//21
GLfloat material_brass_ambient[] = { 0.329412f, 0.223529f, 0.027451f, 1.0f };
GLfloat material_brass_diffuse[] = { 0.780392f, 0.568627f, 0.113725f, 1.0f };
GLfloat material_brass_specular[] = { 0.992157f, 0.941176f, 0.807843f, 1.0f };
GLfloat material_brass_shininess = 0.21794872f * 128;

//22
GLfloat material_bronze_ambient[] = { 0.2125f, 0.1275, 0.054f, 1.0f };
GLfloat material_bronze_diffuse[] = { 0.714f, 0.4284f, 0.18144f, 1.0f };
GLfloat material_bronze_specular[] = { 0.393548f, 0.271906f, 0.166721f, 1.0f };
GLfloat material_bronze_shininess = 0.2f * 128;

//23
GLfloat material_chrome_ambient[] = { 0.25f, 0.25f, 0.25f, 1.0f };
GLfloat material_chrome_diffuse[] = { 0.4f, 0.4f, 0.4f, 1.0f };
GLfloat material_chrome_specular[] = { 0.774597f, 0.774597f, 0.774597f, 1.0f };
GLfloat material_chrome_shininess = 0.6f * 128;

//24
GLfloat material_copper_ambient[] = { 0.19125f, 0.0735f, 0.0225f, 1.0f };
GLfloat material_copper_diffuse[] = { 0.7038f, 0.27048f, 0.0828f, 1.0f };
GLfloat material_copper_specular[] = { 0.256777f, 0.137622f, 0.86014f, 1.0f };
GLfloat material_copper_shininess = 0.1f * 128;

//25
GLfloat material_gold_ambient[] = { 0.24725f, 0.1995, 0.0745f, 1.0f };
GLfloat material_gold_diffuse[] = { 0.75164f, 0.61648f, 0.22648f, 1.0f };
GLfloat material_gold_specular[] = { 0.628281f, 0.555802f, 0.366065f, 1.0f };
GLfloat material_gold_shininess = 0.4f * 128;

//26
GLfloat material_silver_ambient[] = { 0.19225f, 0.19225f, 0.19225f, 1.0f };
GLfloat material_silver_diffuse[] = { 0.50754f, 0.50754f, 0.50754f, 1.0f };
GLfloat material_silver_specular[] = { 0.508273f, 0.508273f, 0.508273f, 1.0f };
GLfloat material_silver_shininess = 0.4f * 128;

//31
GLfloat material_black_ambient[] = { 0.0f, 0.0f, 0.0f, 1.0f };
GLfloat material_black_diffuse[] = { 0.01f, 0.01f, 0.01f, 1.0f };
GLfloat material_black_specular[] = { 0.50f, 0.50f, 0.50f, 1.0f };
GLfloat material_black_shininess = 0.25f * 128;

//32
GLfloat material_cyan_ambient[] = { 0.0f, 0.1f, 0.06f, 1.0f };
GLfloat material_cyan_diffuse[] = { 0.0f, 0.50980392f, 0.50980932f, 1.0f };
GLfloat material_cyan_specular[] = { 0.50196078f, 0.50196078f, 0.50196078f, 1.0f };
GLfloat material_cyan_shininess = 0.25f * 128;

//33
GLfloat material_green_ambient[] = { 0.0f, 0.0f, 0.0f, 1.0f };
GLfloat material_green_diffuse[] = { 0.1f, 0.35f, 0.1f, 1.0f };
GLfloat material_green_specular[] = { 0.45f, 0.45f, 0.45f, 1.0f };
GLfloat material_green_shininess = 0.25f * 128;

//34
GLfloat material_red_ambient[] = { 0.0f, 0.0f, 0.0f, 1.0f };
GLfloat material_red_diffuse[] = { 0.5f, 0.0f, 0.0f, 1.0f };
GLfloat material_red_specular[] = { 0.7f, 0.6f, 0.6f, 1.0f };
GLfloat material_red_shininess = 0.25f * 128;

//35
GLfloat material_white_ambient[] = { 0.0f, 0.0f, 0.0f, 1.0f };
GLfloat material_white_diffuse[] = { 0.55f, 0.55f, 0.55f, 1.0f };
GLfloat material_white_specular[] = { 0.70f, 0.70f, 0.70f, 1.0f };
GLfloat material_white_shininess = 0.25f * 128;

//36
GLfloat material_yellow_ambient[] = { 0.0f, 0.0f, 0.0f, 1.0f };
GLfloat material_yellow_diffuse[] = { 0.5f, 0.5f, 0.0f, 1.0f };
GLfloat material_yellow_specular[] = { 0.60f, 0.60f, 0.50f, 1.0f };
GLfloat material_yellow_shininess = 0.25f * 128;

//41
GLfloat material_black2_ambient[] = { 0.02f, 0.02f, 0.02f, 1.0f };
GLfloat material_black2_diffuse[] = { 0.01f, 0.01f, 0.01f, 1.0f };
GLfloat material_black2_specular[] = { 0.4f, 0.4f, 0.4f, 1.0f };
GLfloat material_black2_shininess = 0.078125f * 128;

//42
GLfloat material_cyan2_ambient[] = { 0.0f, 0.05f, 0.05f, 1.0f };
GLfloat material_cyan2_diffuse[] = { 0.4f, 0.5f, 0.0f, 1.0f };
GLfloat material_cyan2_specular[] = { 0.04f, 0.7f, 0.7f, 1.0f };
GLfloat material_cyan2_shininess = 0.078125f * 128;

//43
GLfloat material_green2_ambient[] = { 0.0f, 0.05f, 0.0f, 1.0f };
GLfloat material_green2_diffuse[] = { 0.4f, 0.5f, 0.4f, 1.0f };
GLfloat material_green2_specular[] = { 0.04f, 0.7f, 0.04f, 1.0f };
GLfloat material_green2_shininess = 0.078125f * 128;

//44
GLfloat material_red2_ambient[] = { 0.05f, 0.0f, 0.0f, 1.0f };
GLfloat material_red2_diffuse[] = { 0.5f, 0.4f, 0.4f, 1.0f };
GLfloat material_red2_specular[] = { 0.7f, 0.04f, 0.04f, 1.0f };
GLfloat material_red2_shininess = 0.078125f * 128;

//45
GLfloat material_white2_ambient[] = { 0.05f, 0.05f, 0.05f, 1.0f };
GLfloat material_white2_diffuse[] = { 0.5f, 0.5f, 0.5f, 1.0f };
GLfloat material_white2_specular[] = { 0.7f, 0.7f, 0.7f, 1.0f };
GLfloat material_white2_shininess = 0.078125f * 128;

//46
GLfloat material_yellow2_ambient[] = { 0.05f, 0.05f, 0.0f, 1.0f };
GLfloat material_yellow2_diffuse[] = { 0.5f, 0.5f, 0.4f, 1.0f };
GLfloat material_yellow2_specular[] = { 0.7f, 0.7f, 0.04f, 1.0f };
GLfloat material_yellow2_shininess = 0.078125f * 128;
GLfloat material_ambient[] = { 0.0f,0.0f,0.0f,1.0f };
GLfloat material_diffuse[] = { 1.0f,1.0f,1.0f,1.0f };
GLfloat material_specular[] = { 1.0f,1.0f,1.0f,1.0f };
GLfloat material_shininess = 50.0f;

@implementation GLESView
{
    EAGLContext *eaglContext;
    
    GLuint defaultFramebuffer;
    GLuint colorRenderbuffer;
    GLuint depthRenderbuffer;
    
    id displayLink;
    NSInteger animationFrameInterval;
    BOOL isAnimating;
    BOOL bLight;
    
    GLuint vertexShaderObject;
    GLuint fragmentShaderObject;
    GLuint shaderProgramObject;
    
    GLuint mvpUniform;
    GLuint numElements;
    GLuint numVertices;
    float sphere_vertices[1146];
    float sphere_normals[1146];
    float sphere_textures[764];
    unsigned short sphere_elements[2280];
    GLuint vao_sphere;
    GLuint vbo_sphere_position;
    GLuint vbo_sphere_normal;
    GLuint vbo_sphere_element;
    GLuint model_matrix_uniform, view_matrix_uniform, projection_matrix_uniform;
    GLuint doubleTapUniform;
    GLuint La_uniform;
    GLuint Ld_uniform;
    GLuint Ls_uniform;
    GLuint light_position_uniform;
    
    GLuint Ka_uniform;
    GLuint Kd_uniform;
    GLuint Ks_uniform;
    GLuint material_shininess_uniform;
    
    GLfloat fPyramidAngle;
    vmath::mat4 perspectiveProjectionMatrix;
}

-(id)initWithFrame:(CGRect)frame;
{
    // code
    self=[super initWithFrame:frame];
    
    if(self)
    {
        CAEAGLLayer *eaglLayer=(CAEAGLLayer *)super.layer;
        
        eaglLayer.opaque=YES;
        eaglLayer.drawableProperties=[NSDictionary dictionaryWithObjectsAndKeys:[NSNumber numberWithBool:FALSE],
                                      kEAGLDrawablePropertyRetainedBacking,kEAGLColorFormatRGBA8,kEAGLDrawablePropertyColorFormat,nil];
        
        eaglContext=[[EAGLContext alloc]initWithAPI:kEAGLRenderingAPIOpenGLES3];
        if(eaglContext==nil)
        {
            [self release];
            return(nil);
        }
        [EAGLContext setCurrentContext:eaglContext];
        
        glGenFramebuffers(1,&defaultFramebuffer);
        glGenRenderbuffers(1,&colorRenderbuffer);
        glBindFramebuffer(GL_FRAMEBUFFER,defaultFramebuffer);
        glBindRenderbuffer(GL_RENDERBUFFER,colorRenderbuffer);
        
        [eaglContext renderbufferStorage:GL_RENDERBUFFER fromDrawable:eaglLayer];
        
        glFramebufferRenderbuffer(GL_FRAMEBUFFER,GL_COLOR_ATTACHMENT0,GL_RENDERBUFFER,colorRenderbuffer);
        
        GLint backingWidth;
        GLint backingHeight;
        
        glGetRenderbufferParameteriv(GL_RENDERBUFFER,GL_RENDERBUFFER_WIDTH,&backingWidth);
        glGetRenderbufferParameteriv(GL_RENDERBUFFER,GL_RENDERBUFFER_HEIGHT,&backingHeight);
        
        glGenRenderbuffers(1,&depthRenderbuffer);
        glBindRenderbuffer(GL_RENDERBUFFER,depthRenderbuffer);
        glRenderbufferStorage(GL_RENDERBUFFER,GL_DEPTH_COMPONENT16,backingWidth,backingHeight);
        glFramebufferRenderbuffer(GL_FRAMEBUFFER,GL_DEPTH_ATTACHMENT,GL_RENDERBUFFER,depthRenderbuffer);
        
        if(glCheckFramebufferStatus(GL_FRAMEBUFFER)!=GL_FRAMEBUFFER_COMPLETE)
        {
            printf("Failed To Create Complete Framebuffer Object %x\n",glCheckFramebufferStatus(GL_FRAMEBUFFER));
            glDeleteFramebuffers(1,&defaultFramebuffer);
            glDeleteRenderbuffers(1,&colorRenderbuffer);
            glDeleteRenderbuffers(1,&depthRenderbuffer);
            
            return(nil);
        }
        
        printf("Renderer : %s | GL Version : %s | GLSL Version : %s\n",glGetString(GL_RENDERER),glGetString(GL_VERSION),glGetString(GL_SHADING_LANGUAGE_VERSION));
        
        // hard coded initializations
        isAnimating=NO;
        animationFrameInterval=60; // default since iOS 8.2
        
        
        // *** VERTEX SHADER ***
        // create shader
        vertexShaderObject = glCreateShader(GL_VERTEX_SHADER);
        
        // provide source code to shader
        const GLchar *vertexShaderSourceCode =
        "#version 300 es" \
        "\n"\
        "in vec4 vPosition;"\
        "in vec3 vNormal;"\
        "uniform mat4 u_model_matrix;"\
        "uniform mat4 u_view_matrix;"\
        "uniform mat4 u_projection_matrix;"\
        "uniform mediump int u_double_tap;"\
        "uniform vec4 u_light_position;"\
        "out vec3 transformed_normals;"\
        "out vec3 light_direction;"\
        "out vec3 viewer_vector;"\
        "void main(void)"\
        "{"\
        "if (u_double_tap == 1)"\
        "{"\
        "vec4 eye_coordinates=u_view_matrix * u_model_matrix * vPosition;"\
        "transformed_normals=mat3(u_view_matrix * u_model_matrix) * vNormal;"\
        "light_direction = vec3(u_light_position) - eye_coordinates.xyz;"\
        "viewer_vector = -eye_coordinates.xyz;"\
        "}"\
        "gl_Position=u_projection_matrix * u_view_matrix * u_model_matrix * vPosition;"\
        "}";
        
        glShaderSource(vertexShaderObject, 1, (const GLchar **)&vertexShaderSourceCode, NULL);
        
        // compile shader
        glCompileShader(vertexShaderObject);
        GLint iInfoLogLength = 0;
        GLint iShaderCompiledStatus = 0;
        char *szInfoLog = NULL;
        glGetShaderiv(vertexShaderObject, GL_COMPILE_STATUS, &iShaderCompiledStatus);
        if (iShaderCompiledStatus == GL_FALSE)
        {
            glGetShaderiv(vertexShaderObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
            if (iInfoLogLength > 0)
            {
                szInfoLog = (char *)malloc(iInfoLogLength);
                if (szInfoLog != NULL)
                {
                    GLsizei written;
                    glGetShaderInfoLog(vertexShaderObject, iInfoLogLength, &written, szInfoLog);
                    printf("Vertex Shader Compilation Log : %s\n", szInfoLog);
                    free(szInfoLog);
                    [self release];
                }
            }
        }
        
        // *** FRAGMENT SHADER ***
        // re-initialize
        iInfoLogLength = 0;
        iShaderCompiledStatus = 0;
        szInfoLog = NULL;
        
        // create shader
        fragmentShaderObject = glCreateShader(GL_FRAGMENT_SHADER);
        
        // provide source code to shader
        const GLchar *fragmentShaderSourceCode =
        "#version 300 es" \
        "\n"\
        "precision highp float;"\
        "in vec3 transformed_normals;"\
        "in vec3 light_direction;"\
        "in vec3 viewer_vector;"\
        "out vec4 FragColor;"\
        "uniform vec3 u_La;"\
        "uniform vec3 u_Ld;"\
        "uniform vec3 u_Ls;"\
        "uniform vec3 u_Ka;"\
        "uniform vec3 u_Kd;"\
        "uniform vec3 u_Ks;"\
        "uniform float u_material_shininess;"\
        "uniform int u_double_tap;"\
        "void main(void)"\
        "{"\
        "vec3 phong_ads_color;"\
        "if(u_double_tap==1)"\
        "{"\
        "vec3 normalized_transformed_normals=normalize(transformed_normals);"\
        "vec3 normalized_light_direction=normalize(light_direction);"\
        "vec3 normalized_viewer_vector=normalize(viewer_vector);"\
        "vec3 ambient = u_La * u_Ka;"\
        "float tn_dot_ld = max(dot(normalized_transformed_normals, normalized_light_direction),0.0);"\
        "vec3 diffuse = u_Ld * u_Kd * tn_dot_ld;"\
        "vec3 reflection_vector = reflect(-normalized_light_direction, normalized_transformed_normals);"\
        "vec3 specular = u_Ls * u_Ks * pow(max(dot(reflection_vector, normalized_viewer_vector), 0.0), u_material_shininess);"\
        "phong_ads_color=ambient + diffuse + specular;"\
        "}"\
        "else"\
        "{"\
        "phong_ads_color = vec3(1.0, 1.0, 1.0);"\
        "}"\
        "FragColor = vec4(phong_ads_color, 1.0);"\
        "}";
        
        glShaderSource(fragmentShaderObject, 1, (const GLchar **)&fragmentShaderSourceCode, NULL);
        
        // compile shader
        glCompileShader(fragmentShaderObject);
        glGetShaderiv(fragmentShaderObject, GL_COMPILE_STATUS, &iShaderCompiledStatus);
        if (iShaderCompiledStatus == GL_FALSE)
        {
            glGetShaderiv(fragmentShaderObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
            if (iInfoLogLength > 0)
            {
                szInfoLog = (char *)malloc(iInfoLogLength);
                if (szInfoLog != NULL)
                {
                    GLsizei written;
                    glGetShaderInfoLog(fragmentShaderObject, iInfoLogLength, &written, szInfoLog);
                    printf("Fragment Shader Compilation Log : %s\n", szInfoLog);
                    free(szInfoLog);
                    [self release];
                }
            }
        }
        
        // *** SHADER PROGRAM ***
        // create
        shaderProgramObject = glCreateProgram();
        
        // attach vertex shader to shader program
        glAttachShader(shaderProgramObject, vertexShaderObject);
        
        // attach fragment shader to shader program
        glAttachShader(shaderProgramObject, fragmentShaderObject);
        
        // pre-link binding of shader program object with vertex shader position attribute
        glBindAttribLocation(shaderProgramObject, VDG_ATTRIBUTE_VERTEX, "vPosition");
        glBindAttribLocation(shaderProgramObject, VDG_ATTRIBUTE_NORMAL, "vNormal");
        
        // link shader
        glLinkProgram(shaderProgramObject);
        GLint iShaderProgramLinkStatus = 0;
        glGetProgramiv(shaderProgramObject, GL_LINK_STATUS, &iShaderProgramLinkStatus);
        if (iShaderProgramLinkStatus == GL_FALSE)
        {
            glGetProgramiv(shaderProgramObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
            if (iInfoLogLength>0)
            {
                szInfoLog = (char *)malloc(iInfoLogLength);
                if (szInfoLog != NULL)
                {
                    GLsizei written;
                    glGetProgramInfoLog(shaderProgramObject, iInfoLogLength, &written, szInfoLog);
                    printf("Shader Program Link Log : %s\n", szInfoLog);
                    free(szInfoLog);
                    [self release];
                }
            }
        }
        
        // get MVP uniform location
        model_matrix_uniform = glGetUniformLocation(shaderProgramObject, "u_model_matrix");
        view_matrix_uniform = glGetUniformLocation(shaderProgramObject, "u_view_matrix");
        projection_matrix_uniform = glGetUniformLocation(shaderProgramObject, "u_projection_matrix");
        doubleTapUniform = glGetUniformLocation(shaderProgramObject, "u_double_tap");
        
        // Ambient color intensity of light
        La_uniform = glGetUniformLocation(shaderProgramObject, "u_La");
        // Diffuse color intensity of light
        Ld_uniform = glGetUniformLocation(shaderProgramObject, "u_Ld");
        // Specular color intensity of light
        Ls_uniform = glGetUniformLocation(shaderProgramObject, "u_Ls");
        
        // Position of light
        light_position_uniform = glGetUniformLocation(shaderProgramObject, "u_light_position");;
        
        // Ambient color reflective intensity of material
        Ka_uniform = glGetUniformLocation(shaderProgramObject, "u_Ka");
        // diffuse reflective color intensity of material
        Kd_uniform = glGetUniformLocation(shaderProgramObject, "u_Kd");
        // specular reflective color intensity of material
        Ks_uniform = glGetUniformLocation(shaderProgramObject, "u_Ks");
        // shininess of material ( value is conventionally between 1 to 200 )
        material_shininess_uniform = glGetUniformLocation(shaderProgramObject, "u_material_shininess");
        
        // *** vertices, colors, shader attribute, vbo, vao initialization ***
        getSphereVertexData(sphere_vertices, sphere_normals, sphere_textures, sphere_elements);
        
        numVertices = getNumberOfSphereVertices();
        numElements = getNumberOfSphereElements();
        glGenVertexArrays(1, &vao_sphere);
        glBindVertexArray(vao_sphere);
        
        // position vbo
        glGenBuffers(1, &vbo_sphere_position);
        glBindBuffer(GL_ARRAY_BUFFER, vbo_sphere_position);
        glBufferData(GL_ARRAY_BUFFER, sizeof(sphere_vertices), sphere_vertices, GL_STATIC_DRAW);
        
        glVertexAttribPointer(VDG_ATTRIBUTE_VERTEX, 3, GL_FLOAT, GL_FALSE, 0, NULL);
        
        glEnableVertexAttribArray(VDG_ATTRIBUTE_VERTEX);
        
        glBindBuffer(GL_ARRAY_BUFFER, 0);
        
        // normal vbo
        glGenBuffers(1, &vbo_sphere_normal);
        glBindBuffer(GL_ARRAY_BUFFER, vbo_sphere_normal);
        glBufferData(GL_ARRAY_BUFFER, sizeof(sphere_normals), sphere_normals, GL_STATIC_DRAW);
        
        glVertexAttribPointer(VDG_ATTRIBUTE_NORMAL, 3, GL_FLOAT, GL_FALSE, 0, NULL);
        
        glEnableVertexAttribArray(VDG_ATTRIBUTE_NORMAL);
        
        glBindBuffer(GL_ARRAY_BUFFER, 0);
        
        // element vbo
        glGenBuffers(1, &vbo_sphere_element);
        glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vbo_sphere_element);
        glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(sphere_elements), sphere_elements, GL_STATIC_DRAW);
        glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
        glBindVertexArray(0);
        // ==================
        
        // enable depth testing
        glEnable(GL_DEPTH_TEST);
        // depth test to do
        glDepthFunc(GL_LEQUAL);
        // We will always cull back faces for better performance
        //glEnable(GL_CULL_FACE);
        
        // set background color
        glClearColor(0.25f, 0.25f, 0.25f, 0.0f);
        // set projection matrix to identity matrix
        perspectiveProjectionMatrix = vmath::mat4::identity();
        
        // GESTURE RECOGNITION
        // Tap gesture code
        UITapGestureRecognizer *singleTapGestureRecognizer=[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(onSingleTap:)];
        [singleTapGestureRecognizer setNumberOfTapsRequired:1];
        [singleTapGestureRecognizer setNumberOfTouchesRequired:1]; // touch of 1 finger
        [singleTapGestureRecognizer setDelegate:self];
        [self addGestureRecognizer:singleTapGestureRecognizer];
        
        UITapGestureRecognizer *doubleTapGestureRecognizer=[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(onDoubleTap:)];
        [doubleTapGestureRecognizer setNumberOfTapsRequired:2];
        [doubleTapGestureRecognizer setNumberOfTouchesRequired:1]; // touch of 1 finger
        [doubleTapGestureRecognizer setDelegate:self];
        [self addGestureRecognizer:doubleTapGestureRecognizer];
        
        // this will allow to differentiate between single tap and double tap
        [singleTapGestureRecognizer requireGestureRecognizerToFail:doubleTapGestureRecognizer];
        
        // swipe gesture
        UISwipeGestureRecognizer *swipeGestureRecognizer=[[UISwipeGestureRecognizer alloc]initWithTarget:self action:@selector(onSwipe:)];
        [self addGestureRecognizer:swipeGestureRecognizer];
        
        // long-press gesture
        UILongPressGestureRecognizer *longPressGestureRecognizer=[[UILongPressGestureRecognizer alloc]initWithTarget:self action:@selector(onLongPress:)];
        [self addGestureRecognizer:longPressGestureRecognizer];
    }
    return(self);
}



+(Class)layerClass{
    return([CAEAGLLayer class]);
}

-(void)drawView:(id)sender{
    
    [EAGLContext setCurrentContext:eaglContext];
    
    glBindFramebuffer(GL_FRAMEBUFFER,defaultFramebuffer);
    
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT | GL_STENCIL_BUFFER_BIT);
    
    // start using OpenGL program object
    glUseProgram(shaderProgramObject);
    GLint width;
    GLint height;
    glGetRenderbufferParameteriv(GL_RENDERBUFFER,GL_RENDERBUFFER_WIDTH,&width);
    glGetRenderbufferParameteriv(GL_RENDERBUFFER,GL_RENDERBUFFER_HEIGHT,&height);
    
    // OpenGL Drawing
    if (bLight == true)
    {
        
        // set 'u_lighting_enabled' uniform
        glUniform1i(doubleTapUniform, 1);
        
        // setting light's properties
        glUniform3fv(La_uniform, 1, lightAmbient);
        glUniform3fv(Ld_uniform, 1, lightDiffuse);
        glUniform3fv(Ls_uniform, 1, lightSpecular);
        if (axisIdentifier == 0) {
            lightPosition[1] = sinf(gfLightAngle);
            lightPosition[2] = cosf(gfLightAngle) - 2;
        }
        if (axisIdentifier == 1) {
            
            lightPosition[0] = sinf(gfLightAngle);
            lightPosition[2] = cosf(gfLightAngle) - 2;
        }
        if (axisIdentifier == 2) {
            lightPosition[2] = -2.0f;
            lightPosition[0] = cosf(gfLightAngle);
            lightPosition[1] = sinf(gfLightAngle);
        }
        glUniform4fv(light_position_uniform, 1, lightPosition);
        
        // setting material's properties
        glUniform3fv(Ka_uniform, 1, material_ambient);
        glUniform3fv(Kd_uniform, 1, material_diffuse);
        glUniform3fv(Ks_uniform, 1, material_specular);
        glUniform1f(material_shininess_uniform, material_shininess);
    }
    else
    {
        // set 'u_lighting_enabled' uniform
        glUniform1i(doubleTapUniform, 0);
    }
    // OpenGL Drawing
    vmath::mat4 modelMatrix = vmath::mat4::identity();
    vmath::mat4 viewMatrix = vmath::mat4::identity();
    
    [self MapViewport:width/6 :5*height/6 :width/6 :height/6];
    //MapViewport(width/6, 5*height/6,width / 6, height / 6);
    // apply z axis translation to go deep into the screen by -2.0,
    // so that triangle with same fullscreen co-ordinates, but due to above translation will look small
    if (bLight == true) {
        glUniform3fv(Ka_uniform, 1, material_emerald_ambient);
        glUniform3fv(Kd_uniform, 1, material_emerald_diffuse);
        glUniform3fv(Ks_uniform, 1, material_emerald_specular);
        glUniform1f(material_shininess_uniform, material_emerald_shininess);
    }
    modelMatrix = vmath::translate(0.0f, 0.0f, -2.0f);
    
    //modelMatrix = modelMatrix * scale(0.3f, 0.3f, 0.3f);
    glUniformMatrix4fv(model_matrix_uniform, 1, GL_FALSE, modelMatrix);
    glUniformMatrix4fv(view_matrix_uniform, 1, GL_FALSE, viewMatrix);
    glUniformMatrix4fv(projection_matrix_uniform, 1, GL_FALSE, perspectiveProjectionMatrix);
    
    // *** bind vao ***
    glBindVertexArray(vao_sphere);
    
    // *** draw, either by glDrawTriangles() or glDrawArrays() or glDrawElements()
    // 11
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vbo_sphere_element);
    glDrawElements(GL_TRIANGLES, numElements, GL_UNSIGNED_SHORT, 0);
    
    [self MapViewport:width/6 :4*height/6 :width/6 :height/6];
    //MapViewport(width / 6, 4 * height / 6, width / 6, height / 6);
    // 12
    if (bLight == true) {
        glUniform3fv(Ka_uniform, 1, material_jade_ambient);
        glUniform3fv(Kd_uniform, 1, material_jade_diffuse);
        glUniform3fv(Ks_uniform, 1, material_jade_specular);
        glUniform1f(material_shininess_uniform, material_jade_shininess);
    }
    modelMatrix = vmath::mat4::identity();
    modelMatrix = vmath::translate(0.0f, 0.0f, -2.0f);
    //modelMatrix = modelMatrix * scale(0.3f, 0.3f, 0.3f);
    glUniformMatrix4fv(model_matrix_uniform, 1, GL_FALSE, modelMatrix);
    glUniformMatrix4fv(view_matrix_uniform, 1, GL_FALSE, viewMatrix);
    glUniformMatrix4fv(projection_matrix_uniform, 1, GL_FALSE, perspectiveProjectionMatrix);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vbo_sphere_element);
    glDrawElements(GL_TRIANGLES, numElements, GL_UNSIGNED_SHORT, 0);
    
    [self MapViewport:width/6 :height/2 :width/6 :height/6];
    //MapViewport(width / 6,   height / 2, width / 6, height / 6);
    // 13
    if (bLight == true) {
        glUniform3fv(Ka_uniform, 1, material_obsidian_ambient);
        glUniform3fv(Kd_uniform, 1, material_obsidian_diffuse);
        glUniform3fv(Ks_uniform, 1, material_obsidian_specular);
        glUniform1f(material_shininess_uniform, material_obsidian_shininess);
    }
    modelMatrix = vmath::mat4::identity();
    modelMatrix = vmath::translate(0.0f, 0.0f, -2.0f);
    
    glUniformMatrix4fv(model_matrix_uniform, 1, GL_FALSE, modelMatrix);
    glUniformMatrix4fv(view_matrix_uniform, 1, GL_FALSE, viewMatrix);
    glUniformMatrix4fv(projection_matrix_uniform, 1, GL_FALSE, perspectiveProjectionMatrix);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vbo_sphere_element);
    glDrawElements(GL_TRIANGLES, numElements, GL_UNSIGNED_SHORT, 0);
    
    [self MapViewport:width/6 :height/3 :width/6 :height/6];
    //MapViewport(width / 6, height / 3, width / 6, height / 6);
    // 14
    if (bLight == true) {
        glUniform3fv(Ka_uniform, 1, material_pearl_ambient);
        glUniform3fv(Kd_uniform, 1, material_pearl_diffuse);
        glUniform3fv(Ks_uniform, 1, material_pearl_specular);
        glUniform1f(material_shininess_uniform, material_pearl_shininess);
    }
    modelMatrix = vmath::mat4::identity();
    modelMatrix = vmath::translate(0.0f, 0.0f, -2.0f);
    glUniformMatrix4fv(model_matrix_uniform, 1, GL_FALSE, modelMatrix);
    glUniformMatrix4fv(view_matrix_uniform, 1, GL_FALSE, viewMatrix);
    glUniformMatrix4fv(projection_matrix_uniform, 1, GL_FALSE, perspectiveProjectionMatrix);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vbo_sphere_element);
    glDrawElements(GL_TRIANGLES, numElements, GL_UNSIGNED_SHORT, 0);
    
    [self MapViewport:width/6 :height/6 :width/6 :height/6];
    //MapViewport(width / 6, height / 6, width / 6, height / 6);
    // 15
    if (bLight == true) {
        glUniform3fv(Ka_uniform, 1, material_ruby_ambient);
        glUniform3fv(Kd_uniform, 1, material_ruby_diffuse);
        glUniform3fv(Ks_uniform, 1, material_ruby_specular);
        glUniform1f(material_shininess_uniform, material_ruby_shininess);
    }
    modelMatrix = vmath::mat4::identity();
    modelMatrix = vmath::translate(0.0f, 0.0f, -2.0f);
    glUniformMatrix4fv(model_matrix_uniform, 1, GL_FALSE, modelMatrix);
    glUniformMatrix4fv(view_matrix_uniform, 1, GL_FALSE, viewMatrix);
    glUniformMatrix4fv(projection_matrix_uniform, 1, GL_FALSE, perspectiveProjectionMatrix);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vbo_sphere_element);
    glDrawElements(GL_TRIANGLES, numElements, GL_UNSIGNED_SHORT, 0);
    
    [self MapViewport:width/6 :0 :width/6 :height/6];
    //MapViewport(width / 6, 0, width / 6, height / 6);
    // 16
    if (bLight == true) {
        glUniform3fv(Ka_uniform, 1, material_turquoise_ambient);
        glUniform3fv(Kd_uniform, 1, material_turquoise_diffuse);
        glUniform3fv(Ks_uniform, 1, material_turquoise_specular);
        glUniform1f(material_shininess_uniform, material_turquoise_shininess);
    }
    modelMatrix = vmath::mat4::identity();
    modelMatrix = vmath::translate(0.0f, 0.0f, -2.0f);
    glUniformMatrix4fv(model_matrix_uniform, 1, GL_FALSE, modelMatrix);
    glUniformMatrix4fv(view_matrix_uniform, 1, GL_FALSE, viewMatrix);
    glUniformMatrix4fv(projection_matrix_uniform, 1, GL_FALSE, perspectiveProjectionMatrix);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vbo_sphere_element);
    glDrawElements(GL_TRIANGLES, numElements, GL_UNSIGNED_SHORT, 0);
    
    [self MapViewport:2*width/6 :5*height/6 :width/6 :height/6];
    //MapViewport(2*width / 6, 5*height / 6, width / 6, height / 6);
    // 21
    if (bLight == true) {
        glUniform3fv(Ka_uniform, 1, material_brass_ambient);
        glUniform3fv(Kd_uniform, 1, material_brass_diffuse);
        glUniform3fv(Ks_uniform, 1, material_brass_specular);
        glUniform1f(material_shininess_uniform, material_brass_shininess);
    }
    modelMatrix = vmath::mat4::identity();
    modelMatrix = vmath::translate(0.0f, 0.0f, -2.0f);
    glUniformMatrix4fv(model_matrix_uniform, 1, GL_FALSE, modelMatrix);
    glUniformMatrix4fv(view_matrix_uniform, 1, GL_FALSE, viewMatrix);
    glUniformMatrix4fv(projection_matrix_uniform, 1, GL_FALSE, perspectiveProjectionMatrix);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vbo_sphere_element);
    glDrawElements(GL_TRIANGLES, numElements, GL_UNSIGNED_SHORT, 0);
    
    [self MapViewport:2*width/6 :4*height/6 :width/6 :height/6];
    //MapViewport(2 * width / 6, 4 * height / 6, width / 6, height / 6);
    // 22
    if (bLight == true) {
        glUniform3fv(Ka_uniform, 1, material_bronze_ambient);
        glUniform3fv(Kd_uniform, 1, material_bronze_diffuse);
        glUniform3fv(Ks_uniform, 1, material_bronze_specular);
        glUniform1f(material_shininess_uniform, material_bronze_shininess);
    }
    modelMatrix = vmath::mat4::identity();
    modelMatrix = vmath::translate(0.0f, 0.0f, -2.0f);
    glUniformMatrix4fv(model_matrix_uniform, 1, GL_FALSE, modelMatrix);
    glUniformMatrix4fv(view_matrix_uniform, 1, GL_FALSE, viewMatrix);
    glUniformMatrix4fv(projection_matrix_uniform, 1, GL_FALSE, perspectiveProjectionMatrix);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vbo_sphere_element);
    glDrawElements(GL_TRIANGLES, numElements, GL_UNSIGNED_SHORT, 0);
    
    [self MapViewport:2*width/6 :3*height/6 :width/6 :height/6];
    //MapViewport(2 * width / 6, 3 * height / 6, width / 6, height / 6);
    // 23
    if (bLight == true) {
        glUniform3fv(Ka_uniform, 1, material_chrome_ambient);
        glUniform3fv(Kd_uniform, 1, material_chrome_diffuse);
        glUniform3fv(Ks_uniform, 1, material_chrome_specular);
        glUniform1f(material_shininess_uniform, material_chrome_shininess);
    }
    modelMatrix = vmath::mat4::identity();
    modelMatrix = vmath::translate(0.0f, 0.0f, -2.0f);
    glUniformMatrix4fv(model_matrix_uniform, 1, GL_FALSE, modelMatrix);
    glUniformMatrix4fv(view_matrix_uniform, 1, GL_FALSE, viewMatrix);
    glUniformMatrix4fv(projection_matrix_uniform, 1, GL_FALSE, perspectiveProjectionMatrix);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vbo_sphere_element);
    glDrawElements(GL_TRIANGLES, numElements, GL_UNSIGNED_SHORT, 0);
    
    [self MapViewport:2*width/6 :2*height/6 :width/6 :height/6];
    //MapViewport(2 * width / 6, 2 * height / 6, width / 6, height / 6);
    // 24
    if (bLight == true) {
        glUniform3fv(Ka_uniform, 1, material_copper_ambient);
        glUniform3fv(Kd_uniform, 1, material_copper_diffuse);
        glUniform3fv(Ks_uniform, 1, material_copper_specular);
        glUniform1f(material_shininess_uniform, material_copper_shininess);
    }
    modelMatrix = vmath::mat4::identity();
    modelMatrix = vmath::translate(0.0f, 0.0f, -2.0f);
    glUniformMatrix4fv(model_matrix_uniform, 1, GL_FALSE, modelMatrix);
    glUniformMatrix4fv(view_matrix_uniform, 1, GL_FALSE, viewMatrix);
    glUniformMatrix4fv(projection_matrix_uniform, 1, GL_FALSE, perspectiveProjectionMatrix);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vbo_sphere_element);
    glDrawElements(GL_TRIANGLES, numElements, GL_UNSIGNED_SHORT, 0);
    
    [self MapViewport:2*width/6 :height/6 :width/6 :height/6];
    //MapViewport(2 * width / 6,  height / 6, width / 6, height / 6);
    // 25
    if (bLight == true) {
        glUniform3fv(Ka_uniform, 1, material_gold_ambient);
        glUniform3fv(Kd_uniform, 1, material_gold_diffuse);
        glUniform3fv(Ks_uniform, 1, material_gold_specular);
        glUniform1f(material_shininess_uniform, material_gold_shininess);
    }
    modelMatrix = vmath::mat4::identity();
    modelMatrix = vmath::translate(0.0f, 0.0f, -2.0f);
    glUniformMatrix4fv(model_matrix_uniform, 1, GL_FALSE, modelMatrix);
    glUniformMatrix4fv(view_matrix_uniform, 1, GL_FALSE, viewMatrix);
    glUniformMatrix4fv(projection_matrix_uniform, 1, GL_FALSE, perspectiveProjectionMatrix);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vbo_sphere_element);
    glDrawElements(GL_TRIANGLES, numElements, GL_UNSIGNED_SHORT, 0);
    
    [self MapViewport:2*width/6 :0 :width/6 :height/6];
    //MapViewport(2 * width / 6,  0, width / 6, height / 6);
    // 26
    if (bLight == true) {
        glUniform3fv(Ka_uniform, 1, material_silver_ambient);
        glUniform3fv(Kd_uniform, 1, material_silver_diffuse);
        glUniform3fv(Ks_uniform, 1, material_silver_specular);
        glUniform1f(material_shininess_uniform, material_silver_shininess);
    }
    modelMatrix = vmath::mat4::identity();
    modelMatrix = vmath::translate(0.0f, 0.0f, -2.0f);
    glUniformMatrix4fv(model_matrix_uniform, 1, GL_FALSE, modelMatrix);
    glUniformMatrix4fv(view_matrix_uniform, 1, GL_FALSE, viewMatrix);
    glUniformMatrix4fv(projection_matrix_uniform, 1, GL_FALSE, perspectiveProjectionMatrix);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vbo_sphere_element);
    glDrawElements(GL_TRIANGLES, numElements, GL_UNSIGNED_SHORT, 0);
    
    [self MapViewport:3*width/6 :5*height/6 :width/6 :height/6];
    //MapViewport(3 * width / 6, 5*height / 6, width / 6, height / 6);
    // 31
    if (bLight == true) {
        glUniform3fv(Ka_uniform, 1, material_black_ambient);
        glUniform3fv(Kd_uniform, 1, material_black_diffuse);
        glUniform3fv(Ks_uniform, 1, material_black_specular);
        glUniform1f(material_shininess_uniform, material_black_shininess);
    }
    modelMatrix = vmath::mat4::identity();
    modelMatrix = vmath::translate(0.0f, 0.0f, -2.0f);
    glUniformMatrix4fv(model_matrix_uniform, 1, GL_FALSE, modelMatrix);
    glUniformMatrix4fv(view_matrix_uniform, 1, GL_FALSE, viewMatrix);
    glUniformMatrix4fv(projection_matrix_uniform, 1, GL_FALSE, perspectiveProjectionMatrix);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vbo_sphere_element);
    glDrawElements(GL_TRIANGLES, numElements, GL_UNSIGNED_SHORT, 0);
    
    [self MapViewport:3*width/6 :4*height/6 :width/6 :height/6];
    //MapViewport(3 * width / 6, 4 * height / 6, width / 6, height / 6);
    // 32
    if (bLight == true) {
        glUniform3fv(Ka_uniform, 1, material_cyan_ambient);
        glUniform3fv(Kd_uniform, 1, material_cyan_diffuse);
        glUniform3fv(Ks_uniform, 1, material_cyan_specular);
        glUniform1f(material_shininess_uniform, material_cyan_shininess);
    }
    modelMatrix = vmath::mat4::identity();
    modelMatrix = vmath::translate(0.0f, 0.0f, -2.0f);
    glUniformMatrix4fv(model_matrix_uniform, 1, GL_FALSE, modelMatrix);
    glUniformMatrix4fv(view_matrix_uniform, 1, GL_FALSE, viewMatrix);
    glUniformMatrix4fv(projection_matrix_uniform, 1, GL_FALSE, perspectiveProjectionMatrix);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vbo_sphere_element);
    glDrawElements(GL_TRIANGLES, numElements, GL_UNSIGNED_SHORT, 0);
    
    [self MapViewport:3*width/6 :3*height/6 :width/6 :height/6];
    //MapViewport(3 * width / 6, 3 * height / 6, width / 6, height / 6);
    // 33
    if (bLight == true) {
        glUniform3fv(Ka_uniform, 1, material_green_ambient);
        glUniform3fv(Kd_uniform, 1, material_green_diffuse);
        glUniform3fv(Ks_uniform, 1, material_green_specular);
        glUniform1f(material_shininess_uniform, material_green_shininess);
    }
    modelMatrix = vmath::mat4::identity();
    modelMatrix = vmath::translate(0.0f, 0.0f, -2.0f);
    glUniformMatrix4fv(model_matrix_uniform, 1, GL_FALSE, modelMatrix);
    glUniformMatrix4fv(view_matrix_uniform, 1, GL_FALSE, viewMatrix);
    glUniformMatrix4fv(projection_matrix_uniform, 1, GL_FALSE, perspectiveProjectionMatrix);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vbo_sphere_element);
    glDrawElements(GL_TRIANGLES, numElements, GL_UNSIGNED_SHORT, 0);
    
    [self MapViewport:3*width/6 :2*height/6 :width/6 :height/6];
    //MapViewport(3 * width / 6, 2 * height / 6, width / 6, height / 6);
    // 34
    if (bLight == true) {
        glUniform3fv(Ka_uniform, 1, material_red_ambient);
        glUniform3fv(Kd_uniform, 1, material_red_diffuse);
        glUniform3fv(Ks_uniform, 1, material_red_specular);
        glUniform1f(material_shininess_uniform, material_red_shininess);
    }
    modelMatrix = vmath::mat4::identity();
    modelMatrix = vmath::translate(0.0f, 0.0f, -2.0f);
    glUniformMatrix4fv(model_matrix_uniform, 1, GL_FALSE, modelMatrix);
    glUniformMatrix4fv(view_matrix_uniform, 1, GL_FALSE, viewMatrix);
    glUniformMatrix4fv(projection_matrix_uniform, 1, GL_FALSE, perspectiveProjectionMatrix);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vbo_sphere_element);
    glDrawElements(GL_TRIANGLES, numElements, GL_UNSIGNED_SHORT, 0);
    
    [self MapViewport:3*width/6 :height/6 :width/6 :height/6];
    //MapViewport(3 * width / 6, height / 6, width / 6, height / 6);
    // 35
    if (bLight == true) {
        glUniform3fv(Ka_uniform, 1, material_white_ambient);
        glUniform3fv(Kd_uniform, 1, material_white_diffuse);
        glUniform3fv(Ks_uniform, 1, material_white_specular);
        glUniform1f(material_shininess_uniform, material_white_shininess);
    }
    modelMatrix = vmath::mat4::identity();
    modelMatrix = vmath::translate(0.0f, 0.0f, -2.0f);
    glUniformMatrix4fv(model_matrix_uniform, 1, GL_FALSE, modelMatrix);
    glUniformMatrix4fv(view_matrix_uniform, 1, GL_FALSE, viewMatrix);
    glUniformMatrix4fv(projection_matrix_uniform, 1, GL_FALSE, perspectiveProjectionMatrix);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vbo_sphere_element);
    glDrawElements(GL_TRIANGLES, numElements, GL_UNSIGNED_SHORT, 0);
    
    [self MapViewport:3*width/6 :0 :width/6 :height/6];
    //MapViewport(3 * width / 6, 0, width / 6, height / 6);
    // 36
    if (bLight == true) {
        glUniform3fv(Ka_uniform, 1, material_yellow_ambient);
        glUniform3fv(Kd_uniform, 1, material_yellow_diffuse);
        glUniform3fv(Ks_uniform, 1, material_yellow_specular);
        glUniform1f(material_shininess_uniform, material_yellow_shininess);
    }
    modelMatrix = vmath::mat4::identity();
    modelMatrix = vmath::translate(0.0f, 0.0f, -2.0f);
    glUniformMatrix4fv(model_matrix_uniform, 1, GL_FALSE, modelMatrix);
    glUniformMatrix4fv(view_matrix_uniform, 1, GL_FALSE, viewMatrix);
    glUniformMatrix4fv(projection_matrix_uniform, 1, GL_FALSE, perspectiveProjectionMatrix);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vbo_sphere_element);
    glDrawElements(GL_TRIANGLES, numElements, GL_UNSIGNED_SHORT, 0);
    
    [self MapViewport:4*width/6 :5*height/6 :width/6 :height/6];
    //MapViewport(4 * width / 6, 5*height / 6, width / 6, height / 6);
    // 41
    if (bLight == true) {
        glUniform3fv(Ka_uniform, 1, material_black2_ambient);
        glUniform3fv(Kd_uniform, 1, material_black2_diffuse);
        glUniform3fv(Ks_uniform, 1, material_black2_specular);
        glUniform1f(material_shininess_uniform, material_black2_shininess);
    }
    modelMatrix = vmath::mat4::identity();
    modelMatrix = vmath::translate(0.0f, 0.0f, -2.0f);
    glUniformMatrix4fv(model_matrix_uniform, 1, GL_FALSE, modelMatrix);
    glUniformMatrix4fv(view_matrix_uniform, 1, GL_FALSE, viewMatrix);
    glUniformMatrix4fv(projection_matrix_uniform, 1, GL_FALSE, perspectiveProjectionMatrix);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vbo_sphere_element);
    glDrawElements(GL_TRIANGLES, numElements, GL_UNSIGNED_SHORT, 0);
    
    [self MapViewport:4*width/6 :4*height/6 :width/6 :height/6];
    //MapViewport(4 * width / 6, 4 * height / 6, width / 6, height / 6);
    // 42
    if (bLight == true) {
        glUniform3fv(Ka_uniform, 1, material_cyan2_ambient);
        glUniform3fv(Kd_uniform, 1, material_cyan2_diffuse);
        glUniform3fv(Ks_uniform, 1, material_cyan2_specular);
        glUniform1f(material_shininess_uniform, material_cyan2_shininess);
    }
    modelMatrix = vmath::mat4::identity();
    modelMatrix = vmath::translate(0.0f, 0.0f, -2.0f);
    glUniformMatrix4fv(model_matrix_uniform, 1, GL_FALSE, modelMatrix);
    glUniformMatrix4fv(view_matrix_uniform, 1, GL_FALSE, viewMatrix);
    glUniformMatrix4fv(projection_matrix_uniform, 1, GL_FALSE, perspectiveProjectionMatrix);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vbo_sphere_element);
    glDrawElements(GL_TRIANGLES, numElements, GL_UNSIGNED_SHORT, 0);
    
    [self MapViewport:4*width/6 :3*height/6 :width/6 :height/6];
    //MapViewport(4 * width / 6, 3 * height / 6, width / 6, height / 6);
    // 43
    if (bLight == true) {
        glUniform3fv(Ka_uniform, 1, material_green2_ambient);
        glUniform3fv(Kd_uniform, 1, material_green2_diffuse);
        glUniform3fv(Ks_uniform, 1, material_green2_specular);
        glUniform1f(material_shininess_uniform, material_green2_shininess);
    }
    modelMatrix = vmath::mat4::identity();
    modelMatrix = vmath::translate(0.0f, 0.0f, -2.0f);
    glUniformMatrix4fv(model_matrix_uniform, 1, GL_FALSE, modelMatrix);
    glUniformMatrix4fv(view_matrix_uniform, 1, GL_FALSE, viewMatrix);
    glUniformMatrix4fv(projection_matrix_uniform, 1, GL_FALSE, perspectiveProjectionMatrix);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vbo_sphere_element);
    glDrawElements(GL_TRIANGLES, numElements, GL_UNSIGNED_SHORT, 0);
    
    [self MapViewport:4*width/6 :2*height/6 :width/6 :height/6];
    //MapViewport(4 * width / 6, 2 * height / 6, width / 6, height / 6);
    // 44
    if (bLight == true) {
        glUniform3fv(Ka_uniform, 1, material_red2_ambient);
        glUniform3fv(Kd_uniform, 1, material_red2_diffuse);
        glUniform3fv(Ks_uniform, 1, material_red2_specular);
        glUniform1f(material_shininess_uniform, material_red2_shininess);
    }
    modelMatrix = vmath::mat4::identity();
    modelMatrix = vmath::translate(0.0f, 0.0f, -2.0f);
    glUniformMatrix4fv(model_matrix_uniform, 1, GL_FALSE, modelMatrix);
    glUniformMatrix4fv(view_matrix_uniform, 1, GL_FALSE, viewMatrix);
    glUniformMatrix4fv(projection_matrix_uniform, 1, GL_FALSE, perspectiveProjectionMatrix);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vbo_sphere_element);
    glDrawElements(GL_TRIANGLES, numElements, GL_UNSIGNED_SHORT, 0);
    
    [self MapViewport:4*width/6 :height/6 :width/6 :height/6];
    //MapViewport(4 * width / 6, height / 6, width / 6, height / 6);
    // 45
    if (bLight == true) {
        glUniform3fv(Ka_uniform, 1, material_white2_ambient);
        glUniform3fv(Kd_uniform, 1, material_white2_diffuse);
        glUniform3fv(Ks_uniform, 1, material_white2_specular);
        glUniform1f(material_shininess_uniform, material_white2_shininess);
    }
    modelMatrix = vmath::mat4::identity();
    modelMatrix = vmath::translate(0.0f, 0.0f, -2.0f);
    glUniformMatrix4fv(model_matrix_uniform, 1, GL_FALSE, modelMatrix);
    glUniformMatrix4fv(view_matrix_uniform, 1, GL_FALSE, viewMatrix);
    glUniformMatrix4fv(projection_matrix_uniform, 1, GL_FALSE, perspectiveProjectionMatrix);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vbo_sphere_element);
    glDrawElements(GL_TRIANGLES, numElements, GL_UNSIGNED_SHORT, 0);
    
    [self MapViewport:4*width/6 :0 :width/6 :height/6];
    //MapViewport(4 * width / 6, 0, width / 6, height / 6);
    // 46
    if (bLight == true) {
        glUniform3fv(Ka_uniform, 1, material_yellow2_ambient);
        glUniform3fv(Kd_uniform, 1, material_yellow2_diffuse);
        glUniform3fv(Ks_uniform, 1, material_yellow2_specular);
        glUniform1f(material_shininess_uniform, material_yellow2_shininess);
    }
    modelMatrix = vmath::mat4::identity();
    modelMatrix = vmath::translate(0.0f, 0.0f, -2.0f);
    glUniformMatrix4fv(model_matrix_uniform, 1, GL_FALSE, modelMatrix);
    glUniformMatrix4fv(view_matrix_uniform, 1, GL_FALSE, viewMatrix);
    glUniformMatrix4fv(projection_matrix_uniform, 1, GL_FALSE, perspectiveProjectionMatrix);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vbo_sphere_element);
    glDrawElements(GL_TRIANGLES, numElements, GL_UNSIGNED_SHORT, 0);
    // stop using OpenGL program object
    glUseProgram(0);
    
    glBindRenderbuffer(GL_RENDERBUFFER,colorRenderbuffer);
    [eaglContext presentRenderbuffer:GL_RENDERBUFFER];
    gfLightAngle += 0.2f;
    if(gfLightAngle > 360.0f)
        gfLightAngle = 0.0f;
    
}

-(void)MapViewport:(int)x :(int)y :(GLfloat)width :(GLfloat)height
{
    
    //code
    if (height == 0)
        height = 1;
    glViewport(x, y, (GLsizei)width, (GLsizei)height);
    perspectiveProjectionMatrix = vmath::perspective(45.0f, (GLfloat)width/(GLfloat)height, 0.1f, 100.0f);
}

-(void)layoutSubviews
{
    // code
    GLint width;
    GLint height;
    
    glBindRenderbuffer(GL_RENDERBUFFER,colorRenderbuffer);
    [eaglContext renderbufferStorage:GL_RENDERBUFFER fromDrawable:(CAEAGLLayer*)self.layer];
    glGetRenderbufferParameteriv(GL_RENDERBUFFER,GL_RENDERBUFFER_WIDTH,&width);
    glGetRenderbufferParameteriv(GL_RENDERBUFFER,GL_RENDERBUFFER_HEIGHT,&height);
    
    glGenRenderbuffers(1,&depthRenderbuffer);
    glBindRenderbuffer(GL_RENDERBUFFER,depthRenderbuffer);
    glRenderbufferStorage(GL_RENDERBUFFER,GL_DEPTH_COMPONENT16,width,height);
    glFramebufferRenderbuffer(GL_FRAMEBUFFER,GL_DEPTH_ATTACHMENT,GL_RENDERBUFFER,depthRenderbuffer);
    
    glViewport(0,0,width,height);
    
    perspectiveProjectionMatrix = vmath::perspective(45.0f, (GLfloat)width/(GLfloat)height, 0.1f, 100.0f);
    
    if(glCheckFramebufferStatus(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE)
    {
        printf("Failed To Create Complete Framebuffer Object %x", glCheckFramebufferStatus(GL_FRAMEBUFFER));
    }
    
    [self drawView:nil];
}

-(void)startAnimation
{
    if (!isAnimating) {
        displayLink=[NSClassFromString(@"CADisplayLink")displayLinkWithTarget:self selector:@selector(drawView:)];
        [displayLink setPreferredFramesPerSecond:animationFrameInterval];
        [displayLink addToRunLoop:[NSRunLoop currentRunLoop] forMode:NSDefaultRunLoopMode];
        isAnimating=YES;
    }
}

-(void)stopAnimation{
    if(isAnimating){
        [displayLink invalidate];
        displayLink=nil;
        isAnimating=NO;
    }
}

// to become first responder
-(BOOL)acceptsFirstResponder
{
    // code
    return(YES);
}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    
}

-(void)onSingleTap:(UITapGestureRecognizer *)gr
{
    axisIdentifier++;
    if(axisIdentifier > 2)
        axisIdentifier = 0;
    lightPosition[0] = 0.0f;
    lightPosition[1] = 0.0f;
    lightPosition[2] = 0.0f;
    gfLightAngle = 0.0f;
}

-(void)onDoubleTap:(UITapGestureRecognizer *)gr
{
    if(bLight == YES)
        bLight = NO;
    else
        bLight = YES;
    
    
}

-(void)onSwipe:(UISwipeGestureRecognizer *)gr
{
    // code
    [self release];
    exit(0);
}

-(void)onLongPress:(UILongPressGestureRecognizer *)gr
{
    
}

- (void)dealloc
{
    // code
    // destroy vao
    if (vao_sphere)
    {
        glDeleteVertexArrays(1, &vao_sphere);
        vao_sphere = 0;
    }
    // destroy vbo
    if (vbo_sphere_position)
    {
        glDeleteBuffers(1, &vbo_sphere_position);
        vbo_sphere_position = 0;
    }
    if (vbo_sphere_normal)
    {
        glDeleteBuffers(1, &vbo_sphere_normal);
        vbo_sphere_normal = 0;
    }
    if (vbo_sphere_element)
    {
        glDeleteBuffers(1, &vbo_sphere_element);
        vbo_sphere_element = 0;
    }
    
    // detach vertex shader from shader program object
    glDetachShader(shaderProgramObject, vertexShaderObject);
    // detach fragment  shader from shader program object
    glDetachShader(shaderProgramObject, fragmentShaderObject);
    
    // delete vertex shader object
    glDeleteShader(vertexShaderObject);
    vertexShaderObject = 0;
    // delete fragment shader object
    glDeleteShader(fragmentShaderObject);
    fragmentShaderObject = 0;
    
    // delete shader program object
    glDeleteProgram(shaderProgramObject);
    shaderProgramObject = 0;
    
    if(depthRenderbuffer)
    {
        glDeleteRenderbuffers(1,&depthRenderbuffer);
        depthRenderbuffer=0;
    }
    
    if(colorRenderbuffer)
    {
        glDeleteRenderbuffers(1,&colorRenderbuffer);
        colorRenderbuffer=0;
    }
    
    if(defaultFramebuffer)
    {
        glDeleteFramebuffers(1,&defaultFramebuffer);
        defaultFramebuffer=0;
    }
    
    if([EAGLContext currentContext]==eaglContext)
    {
        [EAGLContext setCurrentContext:nil];
    }
    [eaglContext release];
    eaglContext=nil;
    
    [super dealloc];
}

@end

